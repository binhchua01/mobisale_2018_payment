v3.6.0
- Build chức năng xác nhận thiết bị
- Build chức năng quà tặng

v3.7.0
- Build chức năng bán Extra OTT
- Thay đổi luồng bán dịch vụ cũ

v3.8.0
- Build lại module bán Camera
- Build tính năng bán Foxy

v3.9.0
- Build lại chức năng bán thiết bị lẻ(bán mới, bán thêm)
- Tính tiền thiết bị bằng API mới(bán mới, bán thêm)
- Cập nhật UI
- Build chức năng RP, E-Voucher ở module Internet/IPTV/Thiết bị (bán mới)
- Tích hợp tính tiền Internet bằng API(bán mới)
- Tích hợp tính tiền IPTV bằng API version V2(bán mới)
- Hiển thị thông tin Quận/Huyện, Phường/Xã, tên Đường, Chung Cư bằng tiếng việt có dấu
- Fix lỗi không nhận được Notifications trên Android 10
- Bán combo camera:  cho phép chỉnh sửa ngày sinh, email, phone type 1,phone type 2,
 phone 1, phone 2, contact 1, contact 2, note(ghi chú địa chỉ), description IBB(ghi chú triển khai)

v3.10.0
- Đánh dấu HĐ Camera Combo khi thực hiện bán thêm
- Mở gông các thông tin được phép cập nhật trên PĐK HĐ Camera liên kết Loại hình, Loại khách hàng, Địa chỉ trên CMND
- Bán mới camera có notification khi chọn hình thức triển khai tại quầy
- Bán Box OTT cho phép chọn MAC
- Điều chỉnh API đăng lý notification
- Camera Fcitizen
- Thay đổi API scan CMND

v3.11.0
- Recare gói giải trí đa nền tảng + Active code tự động
- Quà tặng cho OTT
- [Bán thêm] Foxy không kèm Box
v3.19
- Add camera combo internet and iptv
v3.21
- Add Foxpay payment
- Remove Chữ ký
- Fix 1 số lỗi ở camera
v3.22
- Hợp nhất dịch vụ FPT PLay + Truyền hình => FPTPLay
- Xử lý một số lỗi khác
- điều chỉnh param voucher
