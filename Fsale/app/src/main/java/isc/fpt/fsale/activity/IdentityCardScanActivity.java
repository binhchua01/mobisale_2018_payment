package isc.fpt.fsale.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.IdentityCard;
import isc.fpt.fsale.model.ParseJsonModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Common.scaleBitmapSmaller;

// màn hình scan cmnd
public class IdentityCardScanActivity extends BaseActivitySecond {
    private String pathImageCapture;
    private Uri selectedImageUri;
    private ImageView ibMainIdentityCard;
    private final String ERROR_CODE = "errorCode";
    private final String ERROR_MESSAGE = "errorMessage";
    private final String DATA = "data";
    // file nhận được sau khi chụp ảnh cmnd
    private File tempFile;
    private Bitmap bitmapIdentityCard;
    private Button btnCaptureIdentityCard, btnOpenLocalIdentityCard, btnConfirmScanIdentityCard;

    @Override
    protected void initEvent() {
        btnCaptureIdentityCard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                            || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
                    } else {
                        tempFile = Common.capture(IdentityCardScanActivity.this);
                    }
                } else {
                    tempFile = Common.capture(IdentityCardScanActivity.this);
                }
            }
        });
        btnOpenLocalIdentityCard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                    } else {
                        openGallery();
                    }
                } else {
                    openGallery();
                }
            }
        });
        btnConfirmScanIdentityCard.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View v) {
                if (pathImageCapture == null || pathImageCapture.equals("")) {
                    Common.alertDialog(
                            getString(R.string.lbl_status_un_selected_identity_card_image_document_message),
                            IdentityCardScanActivity.this
                    );
                } else {
                    if (selectedImageUri == null) {
                        bitmapIdentityCard = scaleBitmapSmaller(new File(pathImageCapture));
                    } else {
                        bitmapIdentityCard = scaleBitmapSmaller(
                                new File(
                                        Common.getRealPathUrlImage(
                                                IdentityCardScanActivity.this,
                                                selectedImageUri
                                        )
                                )
                        );
                    }
                    scanIdentityCard();
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_identity_card_scan;
    }

    @Override
    protected void initView() {
        ibMainIdentityCard = (ImageView) findViewById(R.id.ib_main_identity_card);
        btnCaptureIdentityCard = (Button) findViewById(R.id.btn_capture_identity_card);
        btnOpenLocalIdentityCard = (Button) findViewById(R.id.btn_open_local_identity_card);
        btnConfirmScanIdentityCard = (Button) findViewById(R.id.btn_confirm_scan_identity_card);
        getIntentData();
    }

    // mở file ảnh từ thiết bị
    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (data != null) {
                selectedImageUri = data.getData();
                pathImageCapture = Common.getRealPathUrlImage(IdentityCardScanActivity.this, selectedImageUri);
                if (pathImageCapture == null || pathImageCapture.equals("")) {
                    Common.alertDialog(getResources().getString(R.string.lbl_error_image_document_select_message), this);
                } else {
                    setMainBackgroundIdentityCardView(pathImageCapture);
                }
            }
        } else if (resultCode == RESULT_OK) {
            int REQUEST_CAMERA = 0;
            if (requestCode == REQUEST_CAMERA) {
                if (tempFile != null) {
                    pathImageCapture = tempFile.toString();
                    ibMainIdentityCard.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    setMainBackgroundIdentityCardView(pathImageCapture);
                } else {
                    Common.alertDialogNotTitle(getResources().getString(R.string.lbl_fail_capture_picture_message), IdentityCardScanActivity.this);
                }
            }
        } else {
            pathImageCapture = null;
        }
    }

    private void getIntentData() {
        Intent intent = getIntent();
        pathImageCapture = intent.getStringExtra(Constants.TAG_LOCAL_PATH_IDENTITY_CARD);
        setMainBackgroundIdentityCardView(pathImageCapture);
    }

    //hiển thị ảnh cmnd
    private void setMainBackgroundIdentityCardView(String pathImageCapture) {
        ibMainIdentityCard.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        ibMainIdentityCard.setImageBitmap(Common.rotateBitmap(BitmapFactory.decodeFile(pathImageCapture), pathImageCapture));
    }

    @SuppressLint("StaticFieldLeak")
    private void scanIdentityCard() {
        final ProgressDialog pd = Common.showProgressBar(
                IdentityCardScanActivity.this,
                getResources().getString(R.string.lbl_process_scan_info_identity_card
                )
        );
        new AsyncTask<Object, String, String>() {
            @Override
            protected void onPostExecute(String result) {
                IdentityCard identityCard;
                if (result != null) {
                    JSONObject jsObj;
                    try {
                        jsObj = new JSONObject(result);
                        int errorCode = -1;
                        String errorMessage = null;
                        if (jsObj.has(ERROR_CODE)) {
                            errorCode = jsObj.getInt(ERROR_CODE);
                        }
                        if (jsObj.has(ERROR_MESSAGE)) {
                            errorMessage = jsObj.getString(ERROR_MESSAGE);
                        }
                        if (errorCode == 0) {
                            if (jsObj.has(DATA)) {
                                JSONArray jsonArrayData = jsObj.getJSONArray(DATA);
                                JSONObject jsonData = jsonArrayData.getJSONObject(0);
                                identityCard = ParseJsonModel.parse(jsonData, IdentityCard.class);
                                if (identityCard == null) {
                                    Common.alertDialog(result, IdentityCardScanActivity.this);
                                } else {
                                    Intent intent = new Intent();
                                    intent.putExtra(Constants.TAG_INFO_IDENTITY_CARD, identityCard);
                                    intent.putExtra(Constants.TAG_LOCAL_PATH_IDENTITY_CARD, pathImageCapture);
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }
                            } else {
                                Common.alertDialog(errorMessage, IdentityCardScanActivity.this);
                            }
                        } else {
                            Common.alertDialog(errorMessage, IdentityCardScanActivity.this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Common.alertDialog(result, IdentityCardScanActivity.this);
                    }
                }
                try {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Object... params) {
                String result = null;
                try {
                    // kết nối API quét CMND  điền tự động thông tin khách hàng
                    result = Services.getInfoIdentityCard("vnm", bitmapIdentityCard);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return result;
            }
        }.execute(null, null, null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(IdentityCardScanActivity.this, message);
                return;
            }
        }
        switch (requestCode) {
            case 2:
                tempFile = Common.capture(IdentityCardScanActivity.this);
                break;
            case 3:
                openGallery();
                break;
        }
    }
}