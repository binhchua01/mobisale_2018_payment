package isc.fpt.fsale.adapter;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportPotentialSurveyDetailItemModel;
import isc.fpt.fsale.model.ReportPotentialSurveyDetailModel;
import isc.fpt.fsale.utils.Common;
import java.util.ArrayList;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import com.danh32.fontify.TextView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;


public class ReportPotentialSurveyDetailAdapter  extends BaseAdapter{
	private List<ReportPotentialSurveyDetailModel> mList;	
	private Context mContext;
	
	public ReportPotentialSurveyDetailAdapter(Context context, ArrayList<ReportPotentialSurveyDetailModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportPotentialSurveyDetailModel item = mList.get(position);		

		if (convertView == null) {			
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_sbi_total, null);
		}
		try {
			TextView rowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			TextView saleName = (TextView)convertView.findViewById(R.id.lbl_report_sale_name);
			LinearLayout frmData = (LinearLayout)convertView.findViewById(R.id.frm_report_data);
			if(frmData.getChildCount()>0){
				frmData.removeAllViews();
			}			
			if(item != null){					
				rowNumber.setText(String.valueOf(item.getRowNumber()));
				saleName.setText(item.getSaleName());		
				if(item.getItems() != null && item.getItems().size() >0){
					frmData.addView(initRowItem("Họ tên: ", item.getFullName()));
					for (ReportPotentialSurveyDetailItemModel reportPotentialSurveyModel : item.getItems()) {
						frmData.addView(initRowItem(reportPotentialSurveyModel.getSurvey(), reportPotentialSurveyModel.getSurveyValue()));
					}					
				}
			}				
		} catch (Exception e) {

			Common.alertDialog("ReportPotentialObjTotalAdapter.getView():" + e.getMessage(), mContext);
		}
		
		return convertView;
	}
	
	@SuppressLint("InflateParams")
	private LinearLayout initRowItem(String label, final String value){
		try {
			View viewChild = LayoutInflater.from(mContext).inflate( R.layout.sub_row_report_value_hor, null );
			LinearLayout frmDataRow = (LinearLayout)viewChild.findViewById(R.id.frm_report_data_row);
			frmDataRow.setVisibility(View.VISIBLE);		
			TextView lblTitle = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_title);
			TextView lblTotal = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_total);
			lblTitle.setText(label);
			lblTotal.setText(value);			
			return frmDataRow;
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			return null;
		}		
	}
	
}
