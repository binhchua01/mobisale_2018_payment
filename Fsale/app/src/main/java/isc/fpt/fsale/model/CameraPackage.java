package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hau Le on 2019-01-02.
 */
public class CameraPackage implements Parcelable {
    private int PackID;
    private String PackName;
    private int MinCam;
    private int MaxCam;
    private int Cost;
    private int ServiceType;
    private int Discount;
    private int TypePromotion;
    private boolean isSelected = false;

    public int getDiscount() {
        return Discount;
    }

    public void setDiscount(int discount) {
        Discount = discount;
    }

    public int getTypePromotion() {
        return TypePromotion;
    }

    public void setTypePromotion(int typePromotion) {
        TypePromotion = typePromotion;
    }

    public CameraPackage() {
    }


    protected CameraPackage(Parcel in) {
        PackID = in.readInt();
        PackName = in.readString();
        MinCam = in.readInt();
        MaxCam = in.readInt();
        Cost = in.readInt();
        ServiceType = in.readInt();
        Discount = in.readInt();
        TypePromotion = in.readInt();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(PackID);
        dest.writeString(PackName);
        dest.writeInt(MinCam);
        dest.writeInt(MaxCam);
        dest.writeInt(Cost);
        dest.writeInt(ServiceType);
        dest.writeInt(Discount);
        dest.writeInt(TypePromotion);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CameraPackage> CREATOR = new Creator<CameraPackage>() {
        @Override
        public CameraPackage createFromParcel(Parcel in) {
            return new CameraPackage(in);
        }

        @Override
        public CameraPackage[] newArray(int size) {
            return new CameraPackage[size];
        }
    };

    public int getPackID() {
        return PackID;
    }

    public void setPackID(int packID) {
        PackID = packID;
    }

    public String getPackName() {
        return PackName;
    }

    public void setPackName(String packName) {
        PackName = packName;
    }

    public int getMinCam() {
        return MinCam;
    }

    public void setMinCam(int minCam) {
        MinCam = minCam;
    }

    public int getMaxCam() {
        return MaxCam;
    }

    public void setMaxCam(int maxCam) {
        MaxCam = maxCam;
    }

    public int getCost() {
        return Cost;
    }

    public void setCost(int cost) {
        Cost = cost;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


}
