package isc.fpt.fsale.action;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.AutoCreatePreCheckListActivity;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/*
 * Created by HCM.TUANTT14 on 7/4/2018.
 */

public class AutoPreCheckListInsertCL implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public AutoPreCheckListInsertCL(Context context, String[] paramsValue) {
        this.mContext = context;
        String[] arrParamName = new String[]{
                "ObjID",
                "UserName",
                "iInit_Status",
                "Location_Name",
                "Location_Phone",
                "Description",
                "DivisionID",
                "Supporter",
                "SubSupporter",
                "DeptID",
                "OwnerType",
                "Timezone",
                "AppointmentDate",
                "AbilityDesc"
        };
        String message = mContext.getResources().getString(R.string.msg_process_insert_auto_pre_checklist);
        String AUTO_PRE_CHECK_LIST_INSERT_CL = "AutoPreCheckList_InsertCL";
        CallServiceTask service = new CallServiceTask(mContext, AUTO_PRE_CHECK_LIST_INSERT_CL,
                arrParamName, paramsValue, Services.JSON_POST, message, AutoPreCheckListInsertCL.
                this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                String TAG_RESULT_ID = "ResultID";
                if (jsObj.has(TAG_RESULT_ID)) {
                    String TAG_RESULT = "Result";
                    new AlertDialog.Builder(mContext).setTitle(mContext.getResources()
                            .getString(R.string.title_notification)).setMessage(jsObj.getString(TAG_RESULT))
                            .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (mContext.getClass().getSimpleName().equals(AutoCreatePreCheckListActivity.class.getSimpleName())) {
                                        AutoCreatePreCheckListActivity activity = (AutoCreatePreCheckListActivity) mContext;
                                        activity.finish();
                                    }
                                }
                            })
                            .setCancelable(false).create().show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}