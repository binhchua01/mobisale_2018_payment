package isc.fpt.fsale.activity.device_price_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

/**
 * Created by haulc3 on 21,October,2019
 */
public class DevicePriceListAdapter extends RecyclerView.Adapter<DevicePriceListAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<DevicePrice> mList;
    private Device mDeviceSelected;
    private OnItemClickListener mListener;

    public DevicePriceListAdapter(Context mContext, List<DevicePrice> mList,
                                  Device mDeviceSelected, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mDeviceSelected = mDeviceSelected;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_key_value, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewHolder.setDevicePriceInfo(mList.get(mViewHolder.getAdapterPosition()));
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position), mDeviceSelected);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public void notifyData(List<DevicePrice> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }

        void setDevicePriceInfo(DevicePrice mDevicePrice) {
            mDeviceSelected.setDeviceID(mDevicePrice.getDeviceID());
            mDeviceSelected.setCustomerStatus(mDevicePrice.getCustomerStatus());
            mDeviceSelected.setEFStatus(mDevicePrice.getEFStatus());
            mDeviceSelected.setNumMax(mDevicePrice.getNumMax());
            mDeviceSelected.setNumMin(mDevicePrice.getNumMin());
            mDeviceSelected.setNumber(mDevicePrice.getNumMin());
            mDeviceSelected.setPrice(mDevicePrice.getPrice());
            mDeviceSelected.setPriceID(mDevicePrice.getPriceID());
            mDeviceSelected.setPriceName(mDevicePrice.getPriceName());
            mDeviceSelected.setPriceText(mDevicePrice.getPriceText());
            mDeviceSelected.setServiceCode(mDevicePrice.getServiceCode());
            mDeviceSelected.setReturnStatus(mDevicePrice.getReturnStatus());

            //callback to activity
            mListener.onItemClick(mDeviceSelected);
        }

        public void bindView(DevicePrice mDevicePrice, Device mDevice) {
            tvName.setText(mDevicePrice.getPriceText());
            if(mDevicePrice.getPriceID() == mDevice.getPriceID()){
                tvName.setBackgroundResource(R.drawable.background_radius_selected);
            }
        }
    }
}
