package isc.fpt.fsale.ui.fpt_camera.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.model.camera319.PromoCloud;

/**
 * Created by nhannh26 on 22/3/2021
 */
public class ObjectCameraOfNet implements Parcelable {
    private List<CameraDetail> CameraDetail;
    private List<CloudDetail> CloudDetail;
    private SetupDetail SetupDetail;
    private PromoCloud PromotionDetail;
    private int CameraTotal;

    public ObjectCameraOfNet(List<isc.fpt.fsale.ui.fpt_camera.model.CameraDetail> cameraDetail, List<isc.fpt.fsale.ui.fpt_camera.model.CloudDetail> cloudDetail, isc.fpt.fsale.ui.fpt_camera.model.SetupDetail setupDetail, PromoCloud promotionDetail, int cameraTotal) {
        CameraDetail = cameraDetail;
        CloudDetail = cloudDetail;
        SetupDetail = setupDetail;
        PromotionDetail = promotionDetail;
        CameraTotal = cameraTotal;
    }

    public ObjectCameraOfNet() {
    }

    public List<isc.fpt.fsale.ui.fpt_camera.model.CameraDetail> getCameraDetail() {
        return CameraDetail;
    }

    public void setCameraDetail(List<isc.fpt.fsale.ui.fpt_camera.model.CameraDetail> cameraDetail) {
        CameraDetail = cameraDetail;
    }

    public List<isc.fpt.fsale.ui.fpt_camera.model.CloudDetail> getCloudDetail() {
        return CloudDetail;
    }

    public void setCloudDetail(List<isc.fpt.fsale.ui.fpt_camera.model.CloudDetail> cloudDetail) {
        CloudDetail = cloudDetail;
    }

    public isc.fpt.fsale.ui.fpt_camera.model.SetupDetail getSetupDetail() {
        return SetupDetail;
    }

    public void setSetupDetail(isc.fpt.fsale.ui.fpt_camera.model.SetupDetail setupDetail) {
        SetupDetail = setupDetail;
    }

    public PromoCloud getPromotionDetail() {
        return PromotionDetail;
    }

    public void setPromotionDetail(PromoCloud promotionDetail) {
        PromotionDetail = promotionDetail;
    }

    public int getCameraTotal() {
        return CameraTotal;
    }

    public void setCameraTotal(int cameraTotal) {
        CameraTotal = cameraTotal;
    }

    public static Creator<ObjectCameraOfNet> getCREATOR() {
        return CREATOR;
    }

    protected ObjectCameraOfNet(Parcel in) {
        CameraDetail = in.createTypedArrayList(isc.fpt.fsale.ui.fpt_camera.model.CameraDetail.CREATOR);
        CloudDetail = in.createTypedArrayList(isc.fpt.fsale.ui.fpt_camera.model.CloudDetail.CREATOR);
        SetupDetail = in.readParcelable(isc.fpt.fsale.ui.fpt_camera.model.SetupDetail.class.getClassLoader());
        PromotionDetail = in.readParcelable(PromoCloud.class.getClassLoader());
        CameraTotal = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(CameraDetail);
        dest.writeTypedList(CloudDetail);
        dest.writeParcelable(SetupDetail, flags);
        dest.writeParcelable(PromotionDetail, flags);
        dest.writeInt(CameraTotal);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObjectCameraOfNet> CREATOR = new Creator<ObjectCameraOfNet>() {
        @Override
        public ObjectCameraOfNet createFromParcel(Parcel in) {
            return new ObjectCameraOfNet(in);
        }

        @Override
        public ObjectCameraOfNet[] newArray(int size) {
            return new ObjectCameraOfNet[size];
        }
    };

    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArrayCamera = new JSONArray();
        for (CameraDetail item : getCameraDetail()) {
            jsonArrayCamera.put(item.toJsonObjectCamera());
        }
        JSONArray jsonArrayCloud = new JSONArray();
        for (CloudDetail item : getCloudDetail()) {
            jsonArrayCloud.put(item.toJsonObjectCloud());
        }
        try {
            jsonObject.put("CameraDetail", jsonArrayCamera);
            jsonObject.put("CloudDetail", jsonArrayCloud);
            jsonObject.put("SetupDetail", getSetupDetail() == null
                    ? new JSONObject() : getSetupDetail().toJsonObject());
            jsonObject.put("PromotionDetail", getPromotionDetail().toJsonObject() != null
                    ? getPromotionDetail().toJsonObject() : new JSONObject());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public JSONObject toJsonObjectUpdate() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArrayCamera = new JSONArray();
        for (CameraDetail item : getCameraDetail()) {
            jsonArrayCamera.put(item.toJsonObjectCamera());
        }
        JSONArray jsonArrayCloud = new JSONArray();
        for (CloudDetail item : getCloudDetail()) {
            jsonArrayCloud.put(item.toJsonObjectCloud());
        }
        try {
            jsonObject.put("CameraDetail", jsonArrayCamera);
            jsonObject.put("CloudDetail", jsonArrayCloud);
            jsonObject.put("SetupDetail", getSetupDetail() == null ?
                    new JSONObject() : getSetupDetail().toJsonObject());
            jsonObject.put("PromotionDetail", getPromotionDetail().toJsonObject());
            jsonObject.put("CameraTotal", getCameraTotal());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}