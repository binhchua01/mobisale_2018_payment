package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.OTTInventory;
import isc.fpt.fsale.model.PhoneRecare;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.extra_ott.fragment.FragmentExtraOttCusInfo;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class CheckPhoneRecare implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentExtraOttCusInfo activity;

    public CheckPhoneRecare(Context mContext, FragmentExtraOttCusInfo activity, String phone, String username) {
        this.mContext = mContext;
        this.activity = activity;
        String[] arrParamName = new String[]{"Phone", "UserName"};
        String[] arrParamValue = new String[]{phone, username};
        String message = mContext.getResources().getString(R.string.msg_progress_check_phone_recare);
        String CHECK_PHONE_RECARE = "CheckPhoneRecare";
        CallServiceTask service = new CallServiceTask(mContext, CHECK_PHONE_RECARE, arrParamName,
                arrParamValue, Services.JSON_POST, message, CheckPhoneRecare.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<PhoneRecare> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    Log.e("Item", jsonArray.get(i).toString());
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), PhoneRecare.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if(lst.size() > 0)
                        activity.setRecare(lst.get(0).getIsRecare());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
