package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.GiftBox;

/**
 * Created by haulc3 on 20,June,2019
 */
public class SpGiftBoxAdapter extends ArrayAdapter<GiftBox> {
    private List<GiftBox> list;
    private Context mContext;

    public SpGiftBoxAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull ArrayList<GiftBox> list) {
        super(context, resource, textViewResourceId, list);
        this.list = list;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Nullable
    @Override
    public GiftBox getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getGiftID();
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_spinner, null);
        }
        TextView labelView = (TextView) convertView.findViewById(R.id.text_view_0);
        TextView labelDropdown = (TextView) convertView.findViewById(R.id.text_view_1);
        labelDropdown.setVisibility(View.GONE);
        labelView.setVisibility(View.VISIBLE);
        labelView.setGravity(Gravity.LEFT);
        labelView.setTextColor(Color.BLACK);
        labelView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Bold.ttf"));
        labelView.setText(list.get(position).getGiftName());
        return convertView;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_spinner, null);
        }
        TextView labelView = (TextView) convertView.findViewById(R.id.text_view_0);
        TextView labelDropdown = (TextView) convertView.findViewById(R.id.text_view_1);
        labelDropdown.setVisibility(View.VISIBLE);
        labelView.setVisibility(View.GONE);
        labelDropdown.setGravity(Gravity.CENTER);
        labelDropdown.setTextColor(Color.BLACK);
        labelDropdown.setText(list.get(position).getGiftName());
        return convertView;
    }

    public void notifyData(List<GiftBox> mList) {
        this.list = mList;
        notifyDataSetChanged();
    }
}
