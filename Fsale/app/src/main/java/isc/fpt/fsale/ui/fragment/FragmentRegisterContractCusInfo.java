package isc.fpt.fsale.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.SellMoreCameraModel;
import isc.fpt.fsale.utils.Common;

public class FragmentRegisterContractCusInfo extends Fragment {
    private TextView lblFullName, lblContract, lblAddress;
    public EditText txtEmail, txtPhone1, txtContact1;
    public Spinner spTypePhone1;
    private Context mContext;
    private ObjectDetailModel mObject;
    private RegistrationDetailModel mRegister;
    private SellMoreCameraModel mRegisterCamera;

    public static FragmentRegisterContractCusInfo newInstance() {
        FragmentRegisterContractCusInfo fragment = new FragmentRegisterContractCusInfo();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentRegisterContractCusInfo() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_contract_cus_info, container, false);
        try {
            Common.setupUI(getActivity(), rootView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mContext = getActivity();
        lblFullName = (TextView) rootView.findViewById(R.id.lbl_full_name);
        lblContract = (TextView) rootView.findViewById(R.id.lbl_contract);
        lblAddress = (TextView) rootView.findViewById(R.id.lbl_address);
        txtEmail = (EditText) rootView.findViewById(R.id.txt_email);
        txtPhone1 = (EditText) rootView.findViewById(R.id.txt_phone_1);
        txtContact1 = (EditText) rootView.findViewById(R.id.txt_contact_1);
        spTypePhone1 = (Spinner) rootView.findViewById(R.id.sp_phone1);
        removeErrorEdiText();
        getObject();
        return rootView;
    }

    private void removeErrorEdiText() {
        txtContact1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtContact1.setError(null);
            }
        });

        txtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtEmail.setError(null);
            }
        });

        txtPhone1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtPhone1.setError(null);
            }
        });
    }

    private void getObject() {
        if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
            RegisterContractActivity activity = (RegisterContractActivity) mContext;
            mObject = activity.getObject();
            mRegister = activity.getRegister();
            mRegisterCamera = activity.getRegisterCamera();
            loadData();
        }
    }

    private void loadData() {
        try {
            if (mRegister == null) {
                if (mObject != null) {
                    lblContract.setText(mObject.getContract());
                    lblFullName.setText(mObject.getFullName());
                    lblAddress.setText(mObject.getAddress());
                    txtPhone1.setText(mObject.getPhone_1());
                    txtContact1.setText(mObject.getContact_1());
                    txtEmail.setText(mObject.getEmail());
                }
            } else {
                lblFullName.setText(mRegister.getFullName());
                lblContract.setText(mRegister.getContract());
                lblAddress.setText(mRegister.getAddress());
                txtEmail.setText(mRegister.getEmail());
                txtPhone1.setText(mRegister.getPhone_1());
                txtContact1.setText(mRegister.getContact_1());
            }
            loadTypePhone1();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadTypePhone1() {
        ArrayList<KeyValuePairModel> lstPhone = new ArrayList<>();
        lstPhone.add(new KeyValuePairModel(0, "[ Vui lòng chọn loại điện thoại ]"));
        lstPhone.add(new KeyValuePairModel(1, "Cơ quan"));
        lstPhone.add(new KeyValuePairModel(2, "Fax"));
        lstPhone.add(new KeyValuePairModel(3, "Nhà riêng"));
        lstPhone.add(new KeyValuePairModel(4, "Di động"));
        lstPhone.add(new KeyValuePairModel(5, "Nhắn tin"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPhone, Gravity.RIGHT);
        spTypePhone1.setAdapter(adapter);
        if (mRegister != null) {
            spTypePhone1.setSelection(Common.getIndex(spTypePhone1, mRegister.getType_1()), true);
        } else if (mObject != null) {
            spTypePhone1.setSelection(Common.getIndex(spTypePhone1, mObject.getType_1()), true);
        }
    }

    public void updateRegister() {
        try {
            if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
                RegisterContractActivity activity = (RegisterContractActivity) mContext;
                mRegister = activity.getRegister();
                if (mRegister == null) {
                    mRegister = new RegistrationDetailModel();
                    if (mObject != null) {
                        mRegister.setContract(mObject.getContract());
                        mRegister.setFullName(mObject.getFullName());
                    }
                }
                String email = "", phone1, contact1;
                int phone1Type;
                if (!Common.isEmpty(txtEmail))
                    email = txtEmail.getText().toString();
                phone1Type = ((KeyValuePairModel) spTypePhone1.getSelectedItem()).getID();
                phone1 = txtPhone1.getText().toString();
                contact1 = txtContact1.getText().toString();
                mRegister.setEmail(email);
                mRegister.setPhone_1(phone1);
                mRegister.setContact_1(contact1);
                mRegister.setContact(contact1);
                mRegister.setType_1(phone1Type);

                activity.setRegister(mRegister);
                //update obj sell more camera
                mRegisterCamera.setEmail(email);
                mRegisterCamera.setPhone_1(phone1);
                mRegisterCamera.setContact_1(contact1);
                mRegisterCamera.setType_1(phone1Type);
                activity.setRegisterCamera(mRegisterCamera);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
