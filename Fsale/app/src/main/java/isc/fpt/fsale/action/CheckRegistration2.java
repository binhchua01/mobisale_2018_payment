package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

/*
 * @author DuHK - 10/05/2016
 * @see: Kiem tra trung thong tin PDK
 * Api cập nhật phiếu đăng ký
 */

public class CheckRegistration2 implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private RegistrationDetailModel modelDetail;

    public CheckRegistration2(Context mContext, RegistrationDetailModel mModelDetail) {
        this.mContext = mContext;
        this.modelDetail = mModelDetail;
        String[] arrParamName = new String[]{
                "Passport",
                "TaxID",
                "Number",
                "Street",
                "Ward",
                "District",
                "NameVilla",
                "RegCode",
                "UserName",
                "PhoneNumber",
                "PayTVStatus",
                "LocalType",
                "PromotionID",
                "IPTVPackpage",
                "IPTVPromotionID",
                "TypeHouse",
                "HousePosition",
                "Lot",
                "Floor",
                "Room"
        };
        String[] arrParamValues = new String[]{
                mModelDetail.getPassport(),
                mModelDetail.getTaxId(),
                mModelDetail.getBillTo_Number(),
                mModelDetail.getBillTo_Street(),
                mModelDetail.getBillTo_Ward(),
                mModelDetail.getBillTo_District(),
                mModelDetail.getNameVilla(),
                mModelDetail.getRegCode(),
                Constants.USERNAME,
                mModelDetail.getPhone_1(),
                String.valueOf(mModelDetail.getIPTVStatus()),
                String.valueOf(mModelDetail.getLocalType()),
                String.valueOf(mModelDetail.getPromotionID()),
                mModelDetail.getIPTVPackage(),
                String.valueOf(mModelDetail.getIPTVPromotionID()),
                String.valueOf(mModelDetail.getTypeHouse()),
                String.valueOf(mModelDetail.getPosition()),
                mModelDetail.getLot(), mModelDetail.getFloor(),
                mModelDetail.getRoom()
        };

        String message = mContext.getResources().getString(
                R.string.msg_pd_checkRegistration);
        String CHECK_REGISTRATION = "CheckRegistration2";
        CallServiceTask service = new CallServiceTask(mContext, CHECK_REGISTRATION, arrParamName,
                arrParamValues, Services.JSON_POST, message, CheckRegistration2.this);
        service.execute();
    }

    private void handleCheckRegistration(String json) {
        try {
            List<UpdResultModel> lst = null;
            boolean isError = false;
            if (Common.jsonObjectValidate(json)) {
                JSONObject jsObj = new JSONObject(json);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(
                        jsObj, UpdResultModel.class);
                if (resultObject.getErrorCode() == 0) {// OK not Error
                    lst = resultObject.getListObject();
                } else {// ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError)
                    if (lst != null && lst.size() > 0) {
                        if (lst.get(0).getResultID() != 0) {
                            confirmToUpdate(lst.get(0));
                        } else {
                            // cập nhật 1 pdk vào hệ thống
                            JSONObject jsonObject = modelDetail.toJSONObject();
                            new UpdateRegistration(mContext, jsonObject);
                        }
                    } else {
                        Common.alertDialog("Không có dữ liệu trả về!", mContext);
                    }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleCheckRegistration(result);
    }

    private void confirmToUpdate(UpdResultModel result) {
        if (result != null) {
            AlertDialog.Builder builder;
            Dialog dialog;
            builder = new AlertDialog.Builder(mContext);
            builder.setTitle(
                    mContext.getResources().getString(
                            R.string.msg_confirm_update))
                    .setMessage(result.getResult())
                    .setCancelable(false)
                    .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                new UpdateRegistration(mContext, modelDetail.toJSONObject());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            dialog = builder.create();
            dialog.show();
            if (result.getDupID() > 0) {
                UpdateHasShowDuplicate task = new UpdateHasShowDuplicate(mContext, result.getDupID());
                task.execute();
            }
        }
    }
}