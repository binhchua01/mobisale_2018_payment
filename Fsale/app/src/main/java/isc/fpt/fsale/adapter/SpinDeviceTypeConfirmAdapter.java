package isc.fpt.fsale.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.DeviceTypeConfirm;

/**
 * Created by haulc3 on 18,April,2019
 */
public class SpinDeviceTypeConfirmAdapter extends ArrayAdapter<DeviceTypeConfirm> {
    private ArrayList<DeviceTypeConfirm> list;

    protected SpinDeviceTypeConfirmAdapter(@NonNull Context context, int resource, int textViewResourceId,
                                           @NonNull ArrayList<DeviceTypeConfirm> list) {
        super(context, resource, textViewResourceId, list);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Nullable
    @Override
    public DeviceTypeConfirm getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getTypeID();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = (TextView) super.getView(position, convertView, parent).findViewById(R.id.text_view_0);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(list.get(position).getTypeName());
        label.setPadding(10, 15, 10, 15);

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent).findViewById(R.id.text_view_0);
        label.setTextColor(Color.BLACK);
        label.setText(list.get(position).getTypeName());
        label.setPadding(10, 15, 10, 15);
        return label;
    }
}
