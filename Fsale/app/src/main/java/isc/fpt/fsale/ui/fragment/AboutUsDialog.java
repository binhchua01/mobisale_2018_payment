package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckVersion;
import isc.fpt.fsale.action.TestNotification;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.MyApp;


@SuppressLint("ResourceAsColor")
//màn hình Thông tin ứng dụng
public class AboutUsDialog extends DialogFragment {
    private Button btnRegGCM;
    private Context mContext;
    String sUrl = null;
    private ProgressDialog pd;

    public AboutUsDialog() { }

    @SuppressLint("ValidFragment")
    public AboutUsDialog(Context mContext) {
        this.mContext = mContext;
    }

    @SuppressLint({"NewApi", "SetTextI18n"})
    @SuppressWarnings("static-access")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_about_us, container);
        TextView txtDeviceIMEI = (TextView) view.findViewById(R.id.lbl_device);
        TextView txtSimIMEI = (TextView) view.findViewById(R.id.lbl_sim);
        Button btnSupportFixError = (Button) view.findViewById(R.id.btn_support_fix_error);
        Button btnTestNotification = (Button) view.findViewById(R.id.btn_test_notify);
        DeviceInfo info = new DeviceInfo(mContext);
        txtDeviceIMEI.setText("Device imei: " + info.DEVICE_IMEI.toUpperCase());
        txtSimIMEI.setText("Sim imei: " + info.SIM_IMEI.toUpperCase());
        Button btnVersion = (Button) view.findViewById(R.id.btn_about_version);
        String service = "";
        try {
            if (Services.SERVICE_URL.equals(Constants.WS_URL_STAGING) || Services.SERVICE_URL.equals(Constants.WS_URL_BETA)) {
                service = "(TEST)";
            } else if (Services.SERVICE_URL.equals(Constants.WS_URL_ORACLE_SIGNATURE_PRODUCT)) {
                service = "(ORACLE PRODUCT)";
            } else if (Services.SERVICE_URL.equals(Constants.WS_URL_BETA_ORACLE_SIGNATURE_STAGING)) {
                service = "(ORACLE STAGGING)";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        btnVersion.setText("V" + Common.GetAppVersion(mContext) + service);
        // nút ĐĂNG KÝ NOTIFICATION
        btnRegGCM = (Button) view.findViewById(R.id.btn_reg_gcm);
        try {
            // kiểm tra user đã đăng nhập hệ thống
            if (((MyApp) mContext.getApplicationContext()).getIsLogIn())
                // hiển thị nút ĐĂNG KÝ NOTIFICATION
                btnRegGCM.setVisibility(View.VISIBLE);
            else
                //ẩn nút ĐĂNG KÝ NOTIFICATION
                btnRegGCM.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // khởi tạo sự kiện nhấn nút ĐĂNG KÝ NOTIFICATION
        btnRegGCM.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.savePreference(mContext, Constants.SHARE_PRE_GCM,
                        Constants.SHARE_PRE_GCM_SHOW_TOAST, true);
                Common.RegGCM(getActivity(), true, 1);
            }
        });
        // khởi tạo sự kiện nhấn nút HD XỬ LÝ LỖI
        btnSupportFixError.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://mobisaleguide.fpt.vn/"));
                startActivity(browserIntent);
            }
        });
        //khởi tạo sự kiện nhấn nút TEST NOTIFY
        btnTestNotification.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new TestNotification(mContext);
            }
        });
        if (CheckVersion.LINK != null) {
            sUrl = CheckVersion.LINK;
        }
        // khởi tạo sự kiện nhấn nút version V....
//        btnVersion.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    DeviceInfo info = new DeviceInfo(mContext);
//                    String sCurrentVersion = Common.GetAppVersion(mContext);
//                    String deviceIMEI = info.DEVICE_IMEI;
//                    String APP_TYPE = "6";
//                    String ANDROID_PLATFORM = "1";
//                    txtDeviceIMEI.setText("Device imei: " + info.DEVICE_IMEI);
//                    txtSimIMEI.setText("Sim imei: " + info.SIM_IMEI);
//                    new CheckVersion(mContext, new String[]{deviceIMEI, sCurrentVersion,
//                            APP_TYPE, ANDROID_PLATFORM}, AboutUsDialog.this);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
        // lấy regid từ cache thiết bị
        String tokenDevice = Common.loadPreference(mContext, Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_REG_ID);
        if (!tokenDevice.equals("")){
            checkTokenGCMActive(tokenDevice);
        }
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        return dialog;
    }

    //Kết nối API kiểm tra tình trang RegID kích hoạt từ Google
    @SuppressLint("StaticFieldLeak")
    private void checkTokenGCMActive(String token) {
        new AsyncTask<String, String, Boolean>() {

            @Override
            protected void onPreExecute() {
                pd = Common.showProgressBar(mContext, "Đang kiểm tra tình trạng nhận thông báo của thiết bị.");
            }

            @Override
            protected void onProgressUpdate(String... values) {
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Boolean status) {
                try {
                    if (pd != null && pd.isShowing()) {
                        pd.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
                if (!status) {
                    btnRegGCM.setBackgroundColor(Color.RED);
                }
            }

            @Override
            protected Boolean doInBackground(String... params) {
                boolean status = false;
                HttpClient httpclient = new DefaultHttpClient();
                // make GET request to the given URL
                HttpGet httpGet = new HttpGet("https://iid.googleapis.com/iid/info/" + params[0] + "?details=true");
                httpGet.addHeader("Authorization", Constants.GCM_SENDER_APP_ID);
                try {
                    HttpResponse httpResponse = httpclient.execute(httpGet);
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
                    JSONObject resultObject = new JSONObject(reader.readLine());
                    status = resultObject.has("appSigner");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return status;
            }
        }.execute(token, null, null);
    }
}