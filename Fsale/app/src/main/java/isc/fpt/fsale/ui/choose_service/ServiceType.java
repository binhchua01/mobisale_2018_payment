package isc.fpt.fsale.ui.choose_service;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import isc.fpt.fsale.model.CategoryServiceList;

/**
 * Created by haulc3 on 15,July,2019
 */
public class ServiceType implements Parcelable {
    @SerializedName("CategoryServiceGroupDesc")
    @Expose
    private String categoryServiceGroupDesc;
    @SerializedName("CategoryServiceGroupID")
    @Expose
    private Integer categoryServiceGroupID;
    @SerializedName("CategoryServiceGroupLogo")
    @Expose
    private String categoryServiceGroupLogo;
    @SerializedName("CategoryServiceGroupName")
    @Expose
    private String categoryServiceGroupName;
    @SerializedName("CategoryServiceList")
    @Expose
    private List<CategoryServiceList> categoryServiceList;

    protected ServiceType(Parcel in) {
        categoryServiceGroupDesc = in.readString();
        if (in.readByte() == 0) {
            categoryServiceGroupID = null;
        } else {
            categoryServiceGroupID = in.readInt();
        }
        categoryServiceGroupLogo = in.readString();
        categoryServiceGroupName = in.readString();
        categoryServiceList = in.createTypedArrayList(CategoryServiceList.CREATOR);
    }

    public static final Creator<ServiceType> CREATOR = new Creator<ServiceType>() {
        @Override
        public ServiceType createFromParcel(Parcel in) {
            return new ServiceType(in);
        }

        @Override
        public ServiceType[] newArray(int size) {
            return new ServiceType[size];
        }
    };

    public String getCategoryServiceGroupDesc() {
        return categoryServiceGroupDesc;
    }

    public void setCategoryServiceGroupDesc(String categoryServiceGroupDesc) {
        this.categoryServiceGroupDesc = categoryServiceGroupDesc;
    }

    public Integer getCategoryServiceGroupID() {
        return categoryServiceGroupID;
    }

    public void setCategoryServiceGroupID(Integer categoryServiceGroupID) {
        this.categoryServiceGroupID = categoryServiceGroupID;
    }

    public String getCategoryServiceGroupLogo() {
        return categoryServiceGroupLogo;
    }

    public void setCategoryServiceGroupLogo(String categoryServiceGroupLogo) {
        this.categoryServiceGroupLogo = categoryServiceGroupLogo;
    }

    public String getCategoryServiceGroupName() {
        return categoryServiceGroupName;
    }

    public void setCategoryServiceGroupName(String categoryServiceGroupName) {
        this.categoryServiceGroupName = categoryServiceGroupName;
    }

    public List<CategoryServiceList> getCategoryServiceList() {
        return categoryServiceList;
    }

    public void setCategoryServiceList(List<CategoryServiceList> categoryServiceList) {
        this.categoryServiceList = categoryServiceList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoryServiceGroupDesc);
        if (categoryServiceGroupID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(categoryServiceGroupID);
        }
        dest.writeString(categoryServiceGroupLogo);
        dest.writeString(categoryServiceGroupName);
        dest.writeTypedList(categoryServiceList);
    }
}
