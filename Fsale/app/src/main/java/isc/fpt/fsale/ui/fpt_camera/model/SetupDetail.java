package isc.fpt.fsale.ui.fpt_camera.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class SetupDetail implements Parcelable {
    private int Qty;
    private int Cost;
    private int CostBackup;
    private int PackID;
    private String PackName;
    private int ServiceType;
    //type = 0* tại quầy, type = 1* triển khai
    private int Type;

    public SetupDetail() { }

    private SetupDetail(Parcel in) {
        Qty = in.readInt();
        Cost = in.readInt();
        CostBackup = in.readInt();
        PackID = in.readInt();
        PackName = in.readString();
        ServiceType = in.readInt();
        Type = in.readInt();
    }

    public static final Creator<SetupDetail> CREATOR = new Creator<SetupDetail>() {
        @Override
        public SetupDetail createFromParcel(Parcel in) {
            return new SetupDetail(in);
        }

        @Override
        public SetupDetail[] newArray(int size) {
            return new SetupDetail[size];
        }
    };

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        this.Qty = qty;
    }

    public int getCost() {
        return Cost;
    }

    public void setCost(int cost) {
        this.Cost = cost;
    }

    public int getCostBackup() {
        return CostBackup;
    }

    public void setCostBackup(int costBackup) {
        CostBackup = costBackup;
    }

    public int getPackID() {
        return PackID;
    }

    public void setPackID(int packID) {
        this.PackID = packID;
    }

    public String getPackName() {
        return PackName;
    }

    public void setPackName(String packName) {
        PackName = packName;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        this.ServiceType = serviceType;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public JSONObject toJsonObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("Type", getType());
            obj.put("PackID", getPackID());
            obj.put("PackName", getPackName());
            obj.put("Cost", getCost());
            obj.put("Qty", getQty());
            obj.put("ServiceType", getServiceType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(Qty);
        parcel.writeInt(Cost);
        parcel.writeInt(CostBackup);
        parcel.writeInt(PackID);
        parcel.writeString(PackName);
        parcel.writeInt(ServiceType);
        parcel.writeInt(Type);
    }
}
