package isc.fpt.fsale.utils.popup_menu;

/**
 * CLASS: 			PopupItem
 * @description:	model class for pop up menu
 * @author: 		vandn
 * @created on:		12/11/2013
 * */
public class PopupItem {
	private int itemId;
	private String titleText;
	
	public PopupItem(int itemId, String titleText) {
		super();
		this.itemId = itemId;
		this.titleText = titleText;
	}
 
	public int getItemId() {
		return itemId;
	}
 
	public String getTitleText() {
		return titleText;
	}
 
	/*public Class<?> getActivityClassName() {
		return activityClassName;
	}*/
}
