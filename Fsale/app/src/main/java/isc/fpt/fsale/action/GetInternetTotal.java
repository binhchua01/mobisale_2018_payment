package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.InternetTotal;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 05,November,2019
 */
public class GetInternetTotal implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterStep4 fragment;

    public
    GetInternetTotal(Context mContext, RegistrationDetailModel
            mRegistrationDetailModel, FragmentRegisterStep4 fragment) {
        this.fragment = fragment;
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.msg_pd_get_internet_total);
        String GET_INTERNET_TOTAL = "GetInternetTotal";
        CallServiceTask service = new CallServiceTask(mContext, GET_INTERNET_TOTAL,
                mRegistrationDetailModel.toJSONObjectGetInternetTotal(), Services.JSON_POST_OBJECT,
                message, GetInternetTotal.this);
        service.execute();
    }


    @Override
    public void onTaskComplete(String result) {
        try {
            List<InternetTotal> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), InternetTotal.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (fragment != null) {
                        fragment.loadInternetPrice(lst.get(0));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
