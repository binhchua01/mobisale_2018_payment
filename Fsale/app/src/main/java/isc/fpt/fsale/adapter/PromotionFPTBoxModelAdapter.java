package isc.fpt.fsale.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionFPTBoxModel;

/**
 * Created by HCM.TUANTT14 on 1/18/2018.
 */

public class PromotionFPTBoxModelAdapter extends BaseAdapter {
    private ArrayList<PromotionFPTBoxModel> lstObj;
    private Context mContext;

    public PromotionFPTBoxModelAdapter(Context context, ArrayList<PromotionFPTBoxModel> lstObj) {
        this.mContext = context;
        this.lstObj = lstObj;
    }

    public int getCount() {
        if (lstObj != null)
            return lstObj.size();
        return 0;
    }

    public PromotionFPTBoxModel getItem(int position) {
        if (lstObj != null && getCount() > 0)
            return lstObj.get(position);
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_auto_complete, null);
        }
        PromotionFPTBoxModel item = lstObj.get(position);
        TextView label = (TextView) convertView.findViewById(R.id.lbl_desc);
        label.setGravity(Gravity.CENTER);
        if (item != null)
            label.setText(item.getPromotionName());
        return label;
    }
}
