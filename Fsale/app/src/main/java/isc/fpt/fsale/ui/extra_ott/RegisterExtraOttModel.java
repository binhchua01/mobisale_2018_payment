package isc.fpt.fsale.ui.extra_ott;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.model.CartExtraOtt;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 15,July,2019
 */
public class RegisterExtraOttModel implements Parcelable {
    private int ID;
    private int ObjID;
    private String Contract;
    private int LocalType;
    private String LocalTypeName;
    private String LocationID;
    private String FullName;
    private String Contact;
    private String BillTo_City;
    private String BillTo_District;
    private String BillTo_Ward;
    private String BillTo_Street;
    private int TypeHouse;
    private String Lot;
    private String Floor;
    private String Room;
    private String NameVilla;
    private String NameVillaDes;
    private int Position;
    private String BillTo_Number;
    private String Address;
    private String Note;
    private String Passport;
    private String TaxId;
    private String Phone_1;
    private String Phone_2;
    private int Type_1;
    private int Type_2;
    private String Contact_1;
    private String Contact_2;
    private String CusTypeDetail;
    private String CurrentHouse;
    private String Supporter;
    private int Total;
    private String UserName;
    private int CusType;
    private String Email;
    private String DescriptionIBB;
    private int PotentialID;
    private int Payment;
    private String AddressPassport;
    private String Birthday;
    private CartExtraOtt cartExtraOtt;
    private List<CategoryServiceList> categoryServiceList;
    private String BillTo_CityVN;
    private String BillTo_DistrictVN;
    private String BillTo_StreetVN;
    private String BillTo_WardVN;
    private String NameVillaVN;
    private int SourceType;

    public RegisterExtraOttModel() {
    }

    protected RegisterExtraOttModel(Parcel in) {
        ID = in.readInt();
        ObjID = in.readInt();
        Contract = in.readString();
        LocalType = in.readInt();
        LocalTypeName = in.readString();
        LocationID = in.readString();
        FullName = in.readString();
        Contact = in.readString();
        BillTo_City = in.readString();
        BillTo_District = in.readString();
        BillTo_Ward = in.readString();
        TypeHouse = in.readInt();
        BillTo_Street = in.readString();
        Lot = in.readString();
        Floor = in.readString();
        Room = in.readString();
        NameVilla = in.readString();
        NameVillaDes = in.readString();
        Position = in.readInt();
        BillTo_Number = in.readString();
        Address = in.readString();
        Note = in.readString();
        Passport = in.readString();
        TaxId = in.readString();
        Phone_1 = in.readString();
        Phone_2 = in.readString();
        Type_1 = in.readInt();
        Type_2 = in.readInt();
        Contact_1 = in.readString();
        Contact_2 = in.readString();
        CusTypeDetail = in.readString();
        CurrentHouse = in.readString();
        Supporter = in.readString();
        Total = in.readInt();
        UserName = in.readString();
        CusType = in.readInt();
        Email = in.readString();
        DescriptionIBB = in.readString();
        PotentialID = in.readInt();
        Payment = in.readInt();
        AddressPassport = in.readString();
        Birthday = in.readString();
        cartExtraOtt = in.readParcelable(CartExtraOtt.class.getClassLoader());
        categoryServiceList = new ArrayList<>();
        in.readList(categoryServiceList, CategoryServiceList.class.getClassLoader());
        BillTo_CityVN = in.readString();
        BillTo_DistrictVN = in.readString();
        BillTo_StreetVN = in.readString();
        BillTo_WardVN = in.readString();
        NameVillaVN = in.readString();
        SourceType = in.readInt();
    }

    public static final Creator<RegisterExtraOttModel> CREATOR = new Creator<RegisterExtraOttModel>() {
        @Override
        public RegisterExtraOttModel createFromParcel(Parcel in) {
            return new RegisterExtraOttModel(in);
        }

        @Override
        public RegisterExtraOttModel[] newArray(int size) {
            return new RegisterExtraOttModel[size];
        }
    };

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getObjID() {
        return ObjID;
    }

    public void setObjID(int objID) {
        ObjID = objID;
    }

    public String getContract() {
        return Contract;
    }

    public void setContract(String contract) {
        Contract = contract;
    }

    public int getLocalType() {
        return LocalType;
    }

    public void setLocalType(int localType) {
        LocalType = localType;
    }

    public String getLocalTypeName() {
        return LocalTypeName;
    }

    public void setLocalTypeName(String localTypeName) {
        LocalTypeName = localTypeName;
    }

    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String locationID) {
        LocationID = locationID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }

    public String getBillTo_City() {
        return BillTo_City;
    }

    public void setBillTo_City(String billTo_City) {
        BillTo_City = billTo_City;
    }

    public String getBillTo_District() {
        return BillTo_District;
    }

    public void setBillTo_District(String billTo_District) {
        BillTo_District = billTo_District;
    }

    public String getBillTo_Ward() {
        return BillTo_Ward;
    }

    public void setBillTo_Ward(String billTo_Ward) {
        BillTo_Ward = billTo_Ward;
    }

    public int getTypeHouse() {
        return TypeHouse;
    }

    public void setTypeHouse(int typeHouse) {
        TypeHouse = typeHouse;
    }

    public String getBillTo_Street() {
        return BillTo_Street;
    }

    public void setBillTo_Street(String billTo_Street) {
        BillTo_Street = billTo_Street;
    }

    public String getLot() {
        return Lot;
    }

    public void setLot(String lot) {
        Lot = lot;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getRoom() {
        return Room;
    }

    public void setRoom(String room) {
        Room = room;
    }

    public String getNameVilla() {
        return NameVilla;
    }

    public void setNameVilla(String nameVilla) {
        NameVilla = nameVilla;
    }

    public String getNameVillaDes() {
        return NameVillaDes;
    }

    public void setNameVillaDes(String nameVillaDes) {
        NameVillaDes = nameVillaDes;
    }

    public int getPosition() {
        return Position;
    }

    public void setPosition(int position) {
        Position = position;
    }

    public String getBillTo_Number() {
        return BillTo_Number;
    }

    public void setBillTo_Number(String billTo_Number) {
        BillTo_Number = billTo_Number;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getPassport() {
        return Passport;
    }

    public void setPassport(String passport) {
        Passport = passport;
    }

    public String getTaxId() {
        return TaxId;
    }

    public void setTaxId(String taxId) {
        TaxId = taxId;
    }

    public String getPhone_1() {
        return Phone_1;
    }

    public void setPhone_1(String phone_1) {
        Phone_1 = phone_1;
    }

    public String getPhone_2() {
        return Phone_2;
    }

    public void setPhone_2(String phone_2) {
        Phone_2 = phone_2;
    }

    public int getType_1() {
        return Type_1;
    }

    public void setType_1(int type_1) {
        Type_1 = type_1;
    }

    public int getType_2() {
        return Type_2;
    }

    public void setType_2(int type_2) {
        Type_2 = type_2;
    }

    public String getContact_1() {
        return Contact_1;
    }

    public void setContact_1(String contact_1) {
        Contact_1 = contact_1;
    }

    public String getContact_2() {
        return Contact_2;
    }

    public void setContact_2(String contact_2) {
        Contact_2 = contact_2;
    }

    public String getCusTypeDetail() {
        return CusTypeDetail;
    }

    public void setCusTypeDetail(String cusTypeDetail) {
        CusTypeDetail = cusTypeDetail;
    }

    public String getCurrentHouse() {
        return CurrentHouse;
    }

    public void setCurrentHouse(String currentHouse) {
        CurrentHouse = currentHouse;
    }

    public String getSupporter() {
        return Supporter;
    }

    public void setSupporter(String supporter) {
        Supporter = supporter;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public int getCusType() {
        return CusType;
    }

    public void setCusType(int cusType) {
        CusType = cusType;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getDescriptionIBB() {
        return DescriptionIBB;
    }

    public void setDescriptionIBB(String descriptionIBB) {
        DescriptionIBB = descriptionIBB;
    }

    public int getPotentialID() {
        return PotentialID;
    }

    public void setPotentialID(int potentialID) {
        PotentialID = potentialID;
    }

    public int getPayment() {
        return Payment;
    }

    public void setPayment(int payment) {
        Payment = payment;
    }

    public String getAddressPassport() {
        return AddressPassport;
    }

    public void setAddressPassport(String addressPassport) {
        AddressPassport = addressPassport;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public CartExtraOtt getCartExtraOtt() {
        return cartExtraOtt;
    }

    public void setCartExtraOtt(CartExtraOtt cartExtraOtt) {
        this.cartExtraOtt = cartExtraOtt;
    }

    public List<CategoryServiceList> getCategoryServiceList() {
        return categoryServiceList;
    }

    public void setCategoryServiceList(List<CategoryServiceList> categoryServiceList) {
        this.categoryServiceList = categoryServiceList;
    }

    public String getBillTo_CityVN() {
        return BillTo_CityVN;
    }

    public void setBillTo_CityVN(String billTo_CityVN) {
        BillTo_CityVN = billTo_CityVN;
    }

    public String getBillTo_DistrictVN() {
        return BillTo_DistrictVN;
    }

    public void setBillTo_DistrictVN(String billTo_DistrictVN) {
        BillTo_DistrictVN = billTo_DistrictVN;
    }

    public String getBillTo_StreetVN() {
        return BillTo_StreetVN;
    }

    public void setBillTo_StreetVN(String billTo_StreetVN) {
        BillTo_StreetVN = billTo_StreetVN;
    }

    public String getBillTo_WardVN() {
        return BillTo_WardVN;
    }

    public void setBillTo_WardVN(String billTo_WardVN) {
        BillTo_WardVN = billTo_WardVN;
    }

    public String getNameVillaVN() {
        return NameVillaVN;
    }

    public void setNameVillaVN(String nameVillaVN) {
        NameVillaVN = nameVillaVN;
    }

    public int getSourceType() {
        return SourceType;
    }

    public void setSourceType(int sourceType) {
        SourceType = sourceType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeInt(ObjID);
        dest.writeString(Contract);
        dest.writeInt(LocalType);
        dest.writeString(LocalTypeName);
        dest.writeString(LocationID);
        dest.writeString(FullName);
        dest.writeString(Contact);
        dest.writeString(BillTo_City);
        dest.writeString(BillTo_District);
        dest.writeString(BillTo_Ward);
        dest.writeInt(TypeHouse);
        dest.writeString(BillTo_Street);
        dest.writeString(Lot);
        dest.writeString(Floor);
        dest.writeString(Room);
        dest.writeString(NameVilla);
        dest.writeString(NameVillaDes);
        dest.writeInt(Position);
        dest.writeString(BillTo_Number);
        dest.writeString(Address);
        dest.writeString(Note);
        dest.writeString(Passport);
        dest.writeString(TaxId);
        dest.writeString(Phone_1);
        dest.writeString(Phone_2);
        dest.writeInt(Type_1);
        dest.writeInt(Type_2);
        dest.writeString(Contact_1);
        dest.writeString(Contact_2);
        dest.writeString(CusTypeDetail);
        dest.writeString(CurrentHouse);
        dest.writeString(Supporter);
        dest.writeInt(Total);
        dest.writeString(UserName);
        dest.writeInt(CusType);
        dest.writeString(Email);
        dest.writeString(DescriptionIBB);
        dest.writeInt(PotentialID);
        dest.writeInt(Payment);
        dest.writeString(AddressPassport);
        dest.writeString(Birthday);
        dest.writeParcelable(cartExtraOtt, flags);
        dest.writeList(categoryServiceList);
        dest.writeString(BillTo_CityVN);
        dest.writeString(BillTo_DistrictVN);
        dest.writeString(BillTo_StreetVN);
        dest.writeString(BillTo_WardVN);
        dest.writeString(NameVillaVN);
        dest.writeInt(SourceType);
    }

    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ID", getID());
            jsonObject.put("ObjID", getObjID());
            jsonObject.put("Contract", getContract() == null ? "" : getContract());
            jsonObject.put("LocalType", getLocalType());
            jsonObject.put("LocationID", getLocationID() == null ? "" : getLocationID());
            jsonObject.put("FullName", getFullName() == null ? "" : getFullName());
            jsonObject.put("Contact", getContact() == null ? "" : getContact());
            jsonObject.put("BillTo_City", getBillTo_City() == null ? "" : getBillTo_City());
            jsonObject.put("BillTo_District", getBillTo_District() == null ? "" : getBillTo_District());
            jsonObject.put("BillTo_Ward", getBillTo_Ward() == null ? "" : getBillTo_Ward());
            jsonObject.put("TypeHouse", getTypeHouse());
            jsonObject.put("BillTo_Street", getBillTo_Street() == null ? "" : getBillTo_Street());
            jsonObject.put("Lot", getLot() == null ? "" : getLot());
            jsonObject.put("Floor", getFloor() == null ? "" : getFloor());
            jsonObject.put("Room", getRoom() == null ? "" : getRoom());
            jsonObject.put("NameVilla", getNameVilla() == null ? "" : getNameVilla());
            jsonObject.put("Position", getPosition());
            jsonObject.put("BillTo_Number", getBillTo_Number() == null ? "" : getBillTo_Number());
            jsonObject.put("Address", getAddress() == null ? "" : getAddress());
            jsonObject.put("Note", getNote() == null ? "" : getNote());
            jsonObject.put("Passport", getPassport() == null ? "" : getPassport());
            jsonObject.put("TaxId", getTaxId() == null ? "" : getTaxId());
            jsonObject.put("Phone_1", getPhone_1() == null ? "" : getPhone_1());
            jsonObject.put("Phone_2", getPhone_2() == null ? "" : getPhone_2());
            jsonObject.put("Type_1", getType_1());
            jsonObject.put("Type_2", getType_2());
            jsonObject.put("Contact_1", getContact_1() == null ? "" : getContact_1());
            jsonObject.put("Contact_2", getContact_2() == null ? "" : getContact_2());
            jsonObject.put("CusTypeDetail", getCusTypeDetail() == null ? "" : getCusTypeDetail());
            jsonObject.put("CurrentHouse", getCurrentHouse() == null ? "" : getCurrentHouse());
            jsonObject.put("Supporter", getSupporter() == null ? "" : getSupporter());
            jsonObject.put("Total", getTotal());
            jsonObject.put("UserName", getUserName() == null ? "" : getUserName());
            jsonObject.put("CusType", getCusType());
            jsonObject.put("Email", getEmail() == null ? "" : getEmail());
            jsonObject.put("DescriptionIBB", getDescriptionIBB() == null ? "" : getDescriptionIBB());
            jsonObject.put("PotentialID", getPotentialID());
            jsonObject.put("Payment", getPayment());
            jsonObject.put("AddressPassport", getAddressPassport() == null ? "" : getAddressPassport());
            jsonObject.put("Birthday", getBirthday() == null ? "" :
                    Common.getSimpleDateFormat(getBirthday(), Constants.DATE_FORMAT));
            jsonObject.put("Cart", getCartExtraOtt().toJsonObject());
            jsonObject.put("SourceType", getSourceType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
