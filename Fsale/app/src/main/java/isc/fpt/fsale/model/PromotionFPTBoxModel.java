package isc.fpt.fsale.model;

/**
 * Created by HCM.TUANTT14 on 1/18/2018.
 */

public class PromotionFPTBoxModel{
    private int PromotionID;
    private String PromotionName;

    public PromotionFPTBoxModel() {

    }

    public PromotionFPTBoxModel(int promotionID, String promotionName) {
        PromotionID = promotionID;
        PromotionName = promotionName;
    }

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int promotionID) {
        PromotionID = promotionID;
    }

    public String getPromotionName() {
        return PromotionName;
    }

    public void setPromotionName(String promotionName) {
        PromotionName = promotionName;
    }
}
