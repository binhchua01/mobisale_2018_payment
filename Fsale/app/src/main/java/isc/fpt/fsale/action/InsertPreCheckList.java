package isc.fpt.fsale.action;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

//API tạo Create CheckList thủ công
public class InsertPreCheckList implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public InsertPreCheckList(Context mContext, String[] paramsValue) {
        // TODO Auto-generated constructor stub
        this.mContext = mContext;
        Insert(paramsValue);
    }

    private void Insert(String[] paramsValue) {
//        String[] params = new String[]{"ObjID", "Location_Name",
//                "Location_Phone", "FirstStatus", "Description", "DivisionID",
//                "UserName", "AppointmentDate", "Timezone",
//                "Contract", "Supporter", "SubID"};
        String[] params = new String[]{
                "Location_Name",
                "Location_Phone",
                "FirstStatus",
                "Description",
                "DivisionID",
                "UserName",
                "ObjID",
                "Contract",
                "Supporter",
                "SubID",
                "AppointmentDate",
                "Timezone",
                "AbilityDesc"
        };
        String message = mContext.getResources().getString(
                R.string.msg_pd_update);
        String INSERT_PRECHECKLIST = "InsertPreCheckList";
        CallServiceTask service = new CallServiceTask(mContext,
                INSERT_PRECHECKLIST, params, paramsValue, Services.JSON_POST,
                message, InsertPreCheckList.this);
        service.execute();
    }

    private void handleInsert(String json) {
        if (Common.jsonObjectValidate(json)) {
            try {
                JSONArray jsArr;
                JSONObject jsObj = JSONParsing.getJsonObj(json);
                String TAG_INSERT_RESULT = "InsertPreCheckListResult";
                jsArr = jsObj.getJSONArray(TAG_INSERT_RESULT);
                int l = jsArr.length();
                if (l > 0) {
                    String TAG_ERROR = "ErrorService";
                    String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                    if (error.equals("null")) {
                        String TAG_RESULT = "Result";
                        String result = jsArr.getJSONObject(0).getString(TAG_RESULT);
                        String TAG_RESULT_ID = "ResultID";
                        int resultID = 0;
                        if (jsArr.getJSONObject(0).has(TAG_RESULT_ID))
                            resultID = jsArr.getJSONObject(0).getInt(TAG_RESULT_ID);
                        //Show thông báo thành công và đóng activity tạo Pre-Checklist
                        if (resultID > 0) {
                            new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(result)
                                    .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (mContext.getClass().getSimpleName().equals(CreatePreChecklistActivity.class.getSimpleName())) {
                                                CreatePreChecklistActivity activity = (CreatePreChecklistActivity) mContext;
                                                activity.finish();
                                            }
                                        }
                                    })
                                    .setCancelable(false).create().show();
                        } else
                            Common.alertDialogResult(result, mContext);
                    } else {
                        Common.alertDialog("Lỗi WS:" + error, mContext);
                    }

                } else {
                    Common.alertDialog("Không tìm thấy dữ liệu", mContext);
                }

            } catch (Exception e) {
                e.printStackTrace();
                Common.alertDialog(
                        mContext.getResources().getString(
                                R.string.msg_error_data), mContext);
            }
        } else
            Common.alertDialog(
                    mContext.getResources().getString(
                            R.string.msg_connectServer), mContext);
    }

    @Override
    public void onTaskComplete(String result) {
        handleInsert(result);
    }
}
