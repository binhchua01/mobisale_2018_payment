package isc.fpt.fsale.adapter;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.upsell.response.ProgramUpgradeModel;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/*
 * ADAPTER:			KeyValuePairSpinnerAdapter
 *
 * @description: customize abc for DDL KeyValuePaisModel
 * @author: vandn
 * @create on: 		16/08/2013
 */
public class KeyValuePairAdapter extends ArrayAdapter<KeyValuePairModel> {
    private ArrayList<KeyValuePairModel> lstObj;
    private Context mContext;
    private boolean hasInitText = false;
    private int iColor = 2;
    private Typeface tf;

    public KeyValuePairAdapter(Context context, int textViewResourceId, ArrayList<KeyValuePairModel> lstObj) {
        super(context, textViewResourceId);
        this.mContext = context;
        this.lstObj = lstObj;
        this.tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    public KeyValuePairAdapter(Context context, int textViewResourceId, ArrayList<KeyValuePairModel> lstObj, int color) {
        super(context, textViewResourceId);
        this.mContext = context;
        this.lstObj = lstObj;
        this.tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Bold.ttf");
        this.iColor = color;
    }

    public KeyValuePairAdapter(Context context, int textViewResourceId, ArrayList<KeyValuePairModel> lstObj, boolean hasInitText) {
        super(context, textViewResourceId);
        this.mContext = context;
        this.lstObj = lstObj;
        this.hasInitText = true;
    }


    public ArrayList<KeyValuePairModel> getList() {
        return lstObj;
    }

    public int getCount() {
        if (lstObj != null)
            return lstObj.size();
        return 0;
    }

    public KeyValuePairModel getItem(int position) {
        if (lstObj != null)
            return lstObj.get(position);
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(final int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setGravity(Gravity.CENTER);
        if (hasInitText && position == 0) {
            label.setVisibility(View.GONE);
        } else {
            label.setText(lstObj.get(position).getDescription());
            label.setTypeface(tf);
            int padding = (int) mContext.getResources().getDimension(R.dimen.padding_medium);
            label.setPadding(padding, padding, padding, padding);
            if (lstObj.get(position).getColor() != -1) {
                label.setBackgroundColor(lstObj.get(position).getColor());
                label.setTextColor(Color.parseColor("#ffffff"));
            }

            //không cho chọn item khi ability < 0
            if (lstObj.get(position).getAbility() < 0) {
                label.setEnabled(false);
                label.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
            }
        }
        return label;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        switch (iColor) {
            case Gravity.LEFT:
                label.setGravity(Gravity.LEFT);
                label.setTextColor(mContext.getResources().getColor(R.color.black));
                break;
            case Gravity.CENTER:
                label.setGravity(Gravity.CENTER);
                label.setTextColor(mContext.getResources().getColor(R.color.black));
                break;
            case Gravity.RIGHT:
                label.setGravity(Gravity.RIGHT);
                label.setTextColor(mContext.getResources().getColor(R.color.black));
                break;
            default:
                if (lstObj.get(position).getColor() != -1) {
                    label.setTextColor(lstObj.get(position).getColor());//set color for text
                }
                if (this.iColor != 2) {
                    label.setTextColor(ColorStateList.valueOf(this.iColor));
                }
                break;
        }
        label.setTypeface(tf);
        label.setText(lstObj.get(position).getDescription());
        return label;
    }
}
