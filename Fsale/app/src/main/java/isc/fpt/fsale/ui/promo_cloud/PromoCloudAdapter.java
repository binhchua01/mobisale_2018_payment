package isc.fpt.fsale.ui.promo_cloud;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.camera319.PromoCloud;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;

/**
 * Created by Hau Le on 2018-12-25.
 */
public class PromoCloudAdapter extends RecyclerView.Adapter<PromoCloudAdapter.SimpleViewHolder> {
    private Context context;
    private List<PromoCloud> mList;
    private OnItemClickListener mListener;

    PromoCloudAdapter(Context context, List<PromoCloud> mList, OnItemClickListener mListener) {
        this.context = context;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_dialog_camera_device, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressServiceWhenClick(mList.get(mViewHolder.getAdapterPosition()), mViewHolder);
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.textView.setText(mList.get(position).getPromoName());
        if (mList.get(position).isSelected()) {
            holder.layout.setBackgroundResource(R.drawable.background_radius_selected);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        private LinearLayout layout;

        SimpleViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv_item_name);
            layout = (LinearLayout) itemView.findViewById(R.id.layout_camera_package);
        }
    }

    private void progressServiceWhenClick(PromoCloud mDetail, SimpleViewHolder mViewHolder) {
        if (mDetail.isSelected()) {
            mDetail.setSelected(false);
            mViewHolder.layout.setBackgroundResource(R.drawable.background_radius);
        } else {
            mDetail.setSelected(true);
            mViewHolder.layout.setBackgroundResource(R.drawable.background_radius_selected);
        }
        mListener.onItemClick(mDetail);
    }

    public void notifyData(List<PromoCloud> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
