package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ReportSubscriberGrowthForManagerActivity;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportSubscriberGrowthForManagerModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class ReportSubscriberGrowthForManagerAction implements AsyncTaskCompleteListener<String> {
    private final String METHOD_NAME = "ReportSubscriberGrowthForManager";
    private Context mContext;
    private final String TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error", TAG_LIST_OBJECT = "ListObject";
    private final String TAG_SALE_NAME = "SaleName", TAG_TITLE = "Title", TAG_TOTAL = "Total", TAG_ROW_LIST = "Rows";
    private final String TAG_LOCAL_TYPE = "Type", TAG_CURRENT_PAGE = "CurrentPage", TAG_TOTAL_ROW = "TotalRow", TAG_TOTAL_PAGE = "TotalPage";
    private final String TAG_SUM_TOTAL = "SumTotal";

    public ReportSubscriberGrowthForManagerAction(Context context, String UserName, String fromDate, String toDate, int Agent, String AgentName, int PageNumber, int Dept) {
        this.mContext = context;
        String[] paramNames = {"UserName", "FromDate", "ToDate", "Agent", "AgentName", "PageNumber", "Dept"};
        String[] paramValues = {UserName, fromDate, toDate, String.valueOf(Agent), AgentName, String.valueOf(PageNumber), String.valueOf(Dept)};
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, ReportSubscriberGrowthForManagerAction.this);
        service.execute();
    }


    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<ReportSubscriberGrowthForManagerModel> lstReport = new ArrayList<ReportSubscriberGrowthForManagerModel>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                    JSONArray jsArr = jsObj.getJSONArray(TAG_LIST_OBJECT);
                    if (jsArr != null & jsArr.length() > 0) {
                        for (int index = 0; index < jsArr.length(); index++) {
                            JSONObject item = jsArr.getJSONObject(index);
                            ReportSubscriberGrowthForManagerModel newItem = new ReportSubscriberGrowthForManagerModel();
                            if (item.has(TAG_SALE_NAME))
                                newItem.setSaleName(item.getString(TAG_SALE_NAME));
                            if (item.has(TAG_CURRENT_PAGE))
                                newItem.setCurrentPage(item.getInt(TAG_CURRENT_PAGE));
                            if (item.has(TAG_TOTAL_PAGE))
                                newItem.setTotalPage(item.getInt(TAG_TOTAL_PAGE));
                            if (item.has(TAG_TOTAL_ROW))
                                newItem.setTotalRow(item.getInt(TAG_TOTAL_ROW));
                            newItem.setReportList(new ArrayList<KeyValuePairModel>());
                            if (item.has(TAG_ROW_LIST)) {
                                JSONArray rowsArr = item.getJSONArray(TAG_ROW_LIST);
                                for (int i = 0; i < rowsArr.length(); i++) {
                                    JSONObject subItem = rowsArr.getJSONObject(i);
                                    KeyValuePairModel newSubItem = new KeyValuePairModel(0, "", "");
                                    if (subItem.has(TAG_LOCAL_TYPE))
                                        newSubItem.setID(subItem.getInt(TAG_LOCAL_TYPE));
                                    if (subItem.has(TAG_TITLE))
                                        newSubItem.setDescription(subItem.getString(TAG_TITLE));
                                    if (subItem.has(TAG_TOTAL))
                                        newSubItem.setHint(subItem.getString(TAG_TOTAL));
                                    newItem.getReportList().add(newSubItem);
                                }
                            }
                            //Add by: DuHK, tổng số HĐ lấy được
                            if (item.has(TAG_SUM_TOTAL))
                                newItem.setSumTotal(item.getInt(TAG_SUM_TOTAL));
                            lstReport.add(newItem);
                        }
                    }
                } else {
                    String message = "Serrivce error";
                    if (jsObj.has(TAG_ERROR))
                        message = jsObj.getString(TAG_ERROR);
                    Common.alertDialog(message, mContext);
                }
                loadData(lstReport);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(ArrayList<ReportSubscriberGrowthForManagerModel> lstReport) {
        try {
            if (mContext.getClass().getSimpleName().equals(ReportSubscriberGrowthForManagerActivity.class.getSimpleName())) {
                ReportSubscriberGrowthForManagerActivity activity = (ReportSubscriberGrowthForManagerActivity) mContext;
                activity.loadData(lstReport);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
