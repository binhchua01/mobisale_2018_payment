package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportSuspendCustomerActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportSuspendCustomerModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetReportSuspendCustomer implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "ReportSuspendCustomer";
	private String[] paramValues;
	public static final String[] paramNames = new String[]{"UserName", "Month", "Year", "Agent", "AgentName", "Dept", "PageNumber"};
	//
	private int mMonth, mYear;
	
	public GetReportSuspendCustomer(Context context, int Month, int Year, int Agent, String AgentName, int Dept, int PageNumber) {	
		mContext = context;
		String UserName = "";
		this.mMonth = Month;
		this.mYear = Year;
		if(mContext != null)
			UserName = ((MyApp)mContext.getApplicationContext()).getUserName();
		this.paramValues = new String[]{UserName, String.valueOf(Month), String.valueOf(Year), String.valueOf(Agent), AgentName, String.valueOf(Dept), String.valueOf(PageNumber)};			
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportSuspendCustomer.this);
		service.execute();		
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			ArrayList<ReportSuspendCustomerModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ReportSuspendCustomerModel> resultObject = 
						 new WSObjectsModel<ReportSuspendCustomerModel>(jsObj, ReportSuspendCustomerModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();					
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetReportSuspendCustomer:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	
	
	private void loadData(ArrayList<ReportSuspendCustomerModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListReportSuspendCustomerActivity.class.getSimpleName())){
				ListReportSuspendCustomerActivity activity = (ListReportSuspendCustomerActivity)mContext;
				activity.loadData(lst, mMonth, mYear);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetCustomerCareList.loadRpCodeInfo()", e.getMessage());
		}
	}
}
