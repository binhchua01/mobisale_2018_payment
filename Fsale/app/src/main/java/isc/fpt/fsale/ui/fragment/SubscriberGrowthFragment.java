package isc.fpt.fsale.ui.fragment;



import isc.fpt.fsale.R;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

public class SubscriberGrowthFragment extends DialogFragment{
	
	//private Context mContext;	
	private String CountObject;
	private String Norm;
	private String Turnover;
	private String TurnoverObject;
	private ImageButton btn_Close;
	
	private TextView txt_CountObject, txt_Norm, txt_TurnOver, txt_TurnOverObject;

	public SubscriberGrowthFragment(){ }

	@SuppressLint("ValidFragment")
	public SubscriberGrowthFragment(Context mContext,String CountObject, String Norm, String Turnover, String TurnoverObject){
		//this.mContext = mContext;
		this.CountObject = CountObject;
		this.Norm = Norm;
		this.Turnover = Turnover;
		this.TurnoverObject = TurnoverObject;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	    // Inflate the layout to use as dialog or embedded fragment
		  View view = inflater.inflate(R.layout.diaglog_subscriber_growth, container);
		  txt_CountObject = (TextView)view.findViewById(R.id.txt_count_object);
		  txt_Norm = (TextView)view.findViewById(R.id.txt_norm);
		  txt_TurnOver = (TextView)view.findViewById(R.id.txt_turn_over);
		  txt_TurnOverObject = (TextView)view.findViewById(R.id.txt_turn_over_object);
		  btn_Close = (ImageButton)view.findViewById(R.id.btn_close);
		  
		  txt_CountObject.setText(CountObject);
		  txt_Norm.setText(Norm);
		  txt_TurnOver.setText(Turnover);
		  txt_TurnOverObject.setText(TurnoverObject);
		  
			  
		  btn_Close.setOnClickListener(new View.OnClickListener() {     
		    @Override
		    public void onClick(View v) {
		     getDialog().dismiss();
		    }
		  });
		  
		  return view;
	}
	
	/** The system calls this only when creating the layout in a dialog. */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {	   
	    Dialog dialog = super.onCreateDialog(savedInstanceState);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    return dialog;
	}
	
}
