package isc.fpt.fsale.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetObjectList;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ListObjectAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ListObjectModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//màn hình BÁN THÊM DỊCH VỤ
public class ListObjectSearchActivity extends BaseActivity {
    private Spinner spAgent;
    private ListView lvResult;
    private EditText txtAgentName;
    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)

    public ListObjectSearchActivity() {
        super(R.string.lbl_screen_name_paid_new_service);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_paid_new_service));
        setContentView(R.layout.activity_list_object);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            e.printStackTrace();
        }
        lvResult = (ListView) findViewById(R.id.lv_report);
        txtAgentName = (EditText) findViewById(R.id.txt_agent_name);
        final ImageView btnFind = (ImageView) findViewById(R.id.img_find);
        btnFind.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FLAG_FIRST_LOAD > 0)
                    FLAG_FIRST_LOAD = 0;
                getData();
            }
        });


        Spinner spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getData();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        spAgent = (Spinner) findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() > 0) {
                    txtAgentName.setEnabled(true);
                    txtAgentName.requestFocus();
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        initSpinnerAgent();

        lvResult.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListObjectModel selectedItem = (ListObjectModel) parent.getItemAtPosition(position);
                Intent intent = new Intent(ListObjectSearchActivity.this, ObjectDetailActivity.class);
                intent.putExtra(Constants.TAG_CONTRACT, selectedItem.getContract());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ListObjectSearchActivity.this.startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void initSpinnerAgent() {
        ArrayList<KeyValuePairModel> listAgent = new ArrayList<>();
        listAgent.add(new KeyValuePairModel(1, "Số HĐ"));
        listAgent.add(new KeyValuePairModel(2, "Họ tên"));
        listAgent.add(new KeyValuePairModel(3, "Số điện thoại"));
        listAgent.add(new KeyValuePairModel(4, "Địa chỉ"));
        KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(this, R.layout.my_spinner_style, listAgent, Gravity.CENTER);
        spAgent.setAdapter(adapterStatus);
    }

    private boolean checkValidDate() {
        if (Common.isEmpty(txtAgentName)) {
            Common.alertDialog("Chưa nhập nội dung", this);
            return false;
        }
        return true;
    }

    private void getData() {
        if (checkValidDate()) {
            int agent;
            String agentName;
            agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
            agentName = txtAgentName.getText().toString();
            new GetObjectList(this, agent, agentName);
        }
    }

    public void loadData(List<ListObjectModel> lst) {
        if (lst != null && lst.size() > 0) {
            ListObjectAdapter adapter = new ListObjectAdapter(this, lst);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
            lvResult.setAdapter(new ListObjectAdapter(this, new ArrayList<ListObjectModel>()));
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
