package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.UploadRegistrationDocumentActivity;
import isc.fpt.fsale.model.ImageDocument;
import isc.fpt.fsale.utils.Common;

import static isc.fpt.fsale.utils.Common.mapListDocumentRest;

/**
 * Created by HCM.TUANTT14 on 10/16/2018.
 */
//abc chứa danh sách ảnh đã chọn
public class ImageDocumentAdapter extends BaseAdapter {
    private HashMap<Integer, List<ImageDocument>> listDocument;
    private Context context;
    private UploadRegistrationDocumentActivity uploadRegistrationDocumentActivity;
    private int count = 1;

    public ImageDocumentAdapter(Context context, HashMap<Integer, List<ImageDocument>> listDocument) {
        this.context = context;
        this.uploadRegistrationDocumentActivity = (UploadRegistrationDocumentActivity) context;
        this.listDocument = listDocument;
    }

    @Override
    public int getCount() {
        if (listDocument != null)
            return this.listDocument.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (listDocument != null && getCount() > 0)
            return this.listDocument.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final List<ImageDocument> listImageDocument = listDocument.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.list_row_image_document, null);
        }
        ImageButton ibFirstImage = (ImageButton) convertView
                .findViewById(R.id.ib_first_image);
        ImageView ivStatusFirstUpload = (ImageView) convertView.findViewById(R.id.iv_status_first_upload);
        if (listImageDocument != null) {
            ImageDocument firstImageDocument = listImageDocument.get(0);
            String path = firstImageDocument.getPath();
            String pathCache = firstImageDocument.getPathCache();
            ibFirstImage.setBackgroundColor(Color.TRANSPARENT);
            if (path != null) {
                ibFirstImage.setImageBitmap(Common.rotateBitmap(BitmapFactory.decodeFile(path), path));
            } else if (pathCache != null) {
                ibFirstImage.setImageBitmap(Common.rotateBitmap(BitmapFactory.decodeFile(pathCache), pathCache));
            } else {
                ibFirstImage.setImageDrawable(uploadRegistrationDocumentActivity.getResources().getDrawable(R.drawable.error404));
            }
            if (firstImageDocument.getStatusUpload() == 1) {
                ivStatusFirstUpload.setBackground(context.getResources().getDrawable(R.drawable.sync_success));
            } else if (firstImageDocument.getStatusUpload() == 2) {
                ivStatusFirstUpload.setBackground(context.getResources().getDrawable(R.drawable.sync_problem));
            }
        }
        ibFirstImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //chọn ảnh mới thay thế ảnh đã chọn ban đầu
                uploadRegistrationDocumentActivity.openGallerySetKeyIndex(position, 0);
            }
        });
        final ImageView ivDeleteFirstImage = (ImageView) convertView
                .findViewById(R.id.iv_delete_first_image);
        //sự kiện nhấn nút xóa ảnh thứ nhất trên 1 hàng 2 ảnh
        ivDeleteFirstImage.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (listImageDocument != null)
                    deleteImage(listImageDocument, 0, ivDeleteFirstImage);
            }
        });
        ImageButton ibSecondImage = (ImageButton) convertView
                .findViewById(R.id.ib_second_image);
        ImageView ivStatusSecondUpload = (ImageView) convertView.findViewById(R.id.iv_status_second_upload);
        // set border cho các item hình ảnh
        LinearLayout frmParentContainerSecondImage = (LinearLayout) convertView.findViewById(R.id.frm_parent_container_second_image);
        LinearLayout linearLayout = ((LinearLayout) (frmParentContainerSecondImage.getParent()).getParent());
        LinearLayout linearLayout1 = (LinearLayout) linearLayout.getChildAt(0);
        View view = linearLayout1.getChildAt(1);
        if (view != null && count < 3) {
            view.setBackground(context.getResources().getDrawable(R.drawable.border_rectangle_small));
            count++;
        }
        LinearLayout frmContainerSecondImage = (LinearLayout) convertView.findViewById(R.id.frm_container_second_image);
        ibSecondImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadRegistrationDocumentActivity.openGallerySetKeyIndex(position, 1);
            }
        });
        final ImageView ivDeleteSecondImage = (ImageView) convertView.findViewById(R.id.iv_delete_second_image);
        //sự kiện nhấn nút xóa ảnh thứ 2 trên 1 hàng 2 ảnh
        ivDeleteSecondImage.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (listImageDocument != null)
                    deleteImage(listImageDocument, 1, ivDeleteSecondImage);
            }
        });
        ImageView ivUploadImageDocument = (ImageView) convertView.findViewById(R.id.iv_upload_document);
        LinearLayout frmContainerUpLoadDocumentBottom = (LinearLayout) convertView.findViewById(R.id.frm_container_ib_upload_document_bottom);
        ImageButton ibUpLoadDocumentBottom = (ImageButton) convertView.findViewById(R.id.iv_upload_document_bottom);
        if (listDocument.size() == 3) {
            ivUploadImageDocument.setVisibility(View.GONE);
            ibUpLoadDocumentBottom.setVisibility(View.GONE);
        }
        if (listImageDocument != null) {
            if (listImageDocument.size() == 2) {
                ivUploadImageDocument.setVisibility(View.GONE);
                frmContainerSecondImage.setVisibility(View.VISIBLE);
                ImageDocument secondImageDocument = listImageDocument.get(1);
                String path = secondImageDocument.getPath();
                String pathCache = secondImageDocument.getPathCache();
                ibSecondImage.setBackgroundColor(Color.TRANSPARENT);
                if (path != null) {
                    ibSecondImage.setImageBitmap(Common.rotateBitmap(BitmapFactory.decodeFile(path), path));
                } else if (pathCache != null) {
                    ibSecondImage.setImageBitmap(Common.rotateBitmap(BitmapFactory.decodeFile(pathCache), pathCache));
                } else {
                    ibSecondImage.setImageDrawable(uploadRegistrationDocumentActivity.getResources().getDrawable(R.drawable.error404));
                }
                if (secondImageDocument.getStatusUpload() == 1) {
                    ivStatusSecondUpload.setBackground(context.getResources().getDrawable(R.drawable.sync_success));
                } else if (secondImageDocument.getStatusUpload() == 2) {
                    ivStatusSecondUpload.setBackground(context.getResources().getDrawable(R.drawable.sync_problem));
                }
                if (position < 2 && listDocument.size() == position + 1) {
                    frmContainerUpLoadDocumentBottom.setVisibility(View.VISIBLE);
                    ibUpLoadDocumentBottom.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // mở ảnh từ thiết bị sự kiện nhấn nút mở ảnh thứ 1 hàng thứ 2
                            uploadRegistrationDocumentActivity.openGallery(true);
                        }
                    });
                } else {
                    frmContainerUpLoadDocumentBottom.setVisibility(View.GONE);
                }
            } else {
                ivUploadImageDocument.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // mở ảnh từ thiết bị sự kiện nhấn nút mở ảnh thứ 2 hàng trên
                        uploadRegistrationDocumentActivity.openGallery(true);
                    }
                });
            }
        }
        return convertView;
    }

    // hiện dialog xóa 1 ảnh
    public void deleteImage(final List<ImageDocument> listImageDocument, final int index, final ImageView imageSelected) {
        try {
            // confirm log out
            AlertDialog.Builder builder = null;
            Dialog dialog = null;
            builder = new AlertDialog.Builder(context);
            builder.setMessage("Bạn muốn xóa ảnh này?")
                    .setCancelable(false)
                    .setPositiveButton("Có",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    try {
                                        listImageDocument.remove(index);
                                        List<ImageDocument> listDocumentRest = Common.listRestImageDocument(listDocument);
                                        uploadRegistrationDocumentActivity.refreshAdapterImageDocument(mapListDocumentRest(listDocumentRest), true);
                                    } catch (Exception e) {
                                    }
                                }
                            })
                    .setNegativeButton("Không",
                            new DialogInterface.OnClickListener() {
                                @SuppressLint("NewApi")
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });
            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            return;
        }
    }

}
