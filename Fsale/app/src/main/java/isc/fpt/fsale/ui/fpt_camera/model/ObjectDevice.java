package isc.fpt.fsale.ui.fpt_camera.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.model.Device;

/**
 * Created by haulc3 on 05,September,2019
 */
public class ObjectDevice implements Parcelable {
    private List<Device> DeviceDetail;

    public ObjectDevice() {
    }

    public ObjectDevice(List<Device> deviceDetail) {
        DeviceDetail = deviceDetail;
    }

    public List<Device> getDeviceDetail() {
        return DeviceDetail;
    }

    public void setDeviceDetail(List<Device> deviceDetail) {
        DeviceDetail = deviceDetail;
    }

    protected ObjectDevice(Parcel in) {
        DeviceDetail = in.createTypedArrayList(Device.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(DeviceDetail);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObjectDevice> CREATOR = new Creator<ObjectDevice>() {
        @Override
        public ObjectDevice createFromParcel(Parcel in) {
            return new ObjectDevice(in);
        }

        @Override
        public ObjectDevice[] newArray(int size) {
            return new ObjectDevice[size];
        }
    };

    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArrayDevice = new JSONArray();
        for (Device item : getDeviceDetail()) {
            jsonArrayDevice.put(item.toJSONObjectRegister());
        }
        try {
            jsonObject.put("CameraDetail", jsonArrayDevice);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}