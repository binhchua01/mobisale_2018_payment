package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by haulc3 on 21,July,2019
 */
public class CartExtraOttTotal implements Parcelable {
    private Integer id;
    private Integer quantity;

    protected CartExtraOttTotal(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            quantity = null;
        } else {
            quantity = in.readInt();
        }
    }

    public static final Creator<CartExtraOttTotal> CREATOR = new Creator<CartExtraOttTotal>() {
        @Override
        public CartExtraOttTotal createFromParcel(Parcel in) {
            return new CartExtraOttTotal(in);
        }

        @Override
        public CartExtraOttTotal[] newArray(int size) {
            return new CartExtraOttTotal[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public CartExtraOttTotal(Integer id, Integer quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (quantity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(quantity);
        }
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ID", getId());
            jsonObject.put("Quantity", getQuantity());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
