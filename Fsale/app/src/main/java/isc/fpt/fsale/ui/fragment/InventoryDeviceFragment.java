package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.ConfirmDevice;
import isc.fpt.fsale.action.GetOTTInventory;
import isc.fpt.fsale.activity.ReceiveDeviceActivity;
import isc.fpt.fsale.adapter.SpinDeviceTypeInventoryAdapter;
import isc.fpt.fsale.model.ListDeviceOTT;
import isc.fpt.fsale.model.OTTInventory;
import isc.fpt.fsale.utils.MyApp;

public class InventoryDeviceFragment extends Fragment {
    private Spinner spnDeviceTypeInventory;
    private ReceiveDeviceActivity activity;
    private SpinDeviceTypeInventoryAdapter spnAdapter;
    private ListDeviceOTT mListDeviceOTT;
    public int ottId;
    private WebView wvShowInventoryDevice;
    private Button btnSearch;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ReceiveDeviceActivity) {
            this.activity = (ReceiveDeviceActivity) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inventory_device, container, false);
        bindView(view);
        return view;
    }

    private void bindView(View view) {
        spnDeviceTypeInventory = (Spinner) view.findViewById(R.id.spinner_type_search_inventory);
        wvShowInventoryDevice = (WebView) view.findViewById(R.id.wv_show_inventory_device);
        btnSearch = (Button) view.findViewById(R.id.btn_search);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initEvent();
    }

    private void initData() {
        //init data to spinner device type inventory
        spnAdapter = new SpinDeviceTypeInventoryAdapter(activity,
                R.layout.item_spinner, R.id.text_view_0, activity.listInventory) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textview = (TextView) view;
                if (position == 0) {
                    textview.setTextColor(Color.GRAY);
                } else {
                    textview.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spnDeviceTypeInventory.setAdapter(spnAdapter);

        spnDeviceTypeInventory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                mListDeviceOTT = spnAdapter.getItem(position);
                assert mListDeviceOTT != null;
                ottId = mListDeviceOTT.getOttId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    private void initEvent() {
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getOTTInventory();
            }
        });
    }

    public void getOTTInventory() {
        //chưa chọn loại thiết bị
        if (ottId == -1) return;

        //call api get OTT inventory by ott id
        new GetOTTInventory(activity, InventoryDeviceFragment.this,
                ((MyApp) getActivity().getApplication()).getUserName(), ottId);
    }

    public void ottInventory(ArrayList<OTTInventory> list) {
        if (list.size() != 0) {
            wvShowInventoryDevice.loadDataWithBaseURL(null, list.get(0).getHTMLDetail(),
                    "text/html", "utf-8", null);
        }
    }
}
