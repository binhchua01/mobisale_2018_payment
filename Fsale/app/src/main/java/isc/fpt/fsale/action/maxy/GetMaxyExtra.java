package isc.fpt.fsale.action.maxy;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.ui.maxytv.model.MaxyExtra;
import isc.fpt.fsale.ui.maxytv.model.MaxyGeneral;
import isc.fpt.fsale.ui.maxytv.ui.MaxyDeviceActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyExtraActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetMaxyExtra implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private int RegType;
    private String contract;

    public GetMaxyExtra(Context mContext, int regType, String contract, MaxyGeneral maxyGeneral) {
        this.mContext = mContext;
        this.RegType = regType;
        this.contract = contract;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Contract", contract);
            jsonObject.put("RegType",RegType);
            jsonObject.put("LocationID", Constants.LST_REGION.get(0).getsID());
            jsonObject.put("BranchCode", Constants.BRANCH_CODE);
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("TypeDeployID", maxyGeneral.getTypeDeployID());
            jsonObject.put("MaxyPromotionID", maxyGeneral.getPromotionID());
            jsonObject.put("MaxyPromotionType", maxyGeneral.getPromotionType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.msg_progress_get_maxy_extra);
        String GET_MAXY_EXTRA = "GetMaxyExtra";
        CallServiceTask service = new CallServiceTask(mContext, GET_MAXY_EXTRA, jsonObject,
                Services.JSON_POST_OBJECT, message, GetMaxyExtra.this);
        service.execute();
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<MaxyExtra> resultObject = new WSObjectsModel<>(jsObj, MaxyExtra.class);
                List<MaxyExtra> lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (mContext != null) {
                    if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                        Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                    } else {
                        ((MaxyExtraActivity) mContext).loadExtraList(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
