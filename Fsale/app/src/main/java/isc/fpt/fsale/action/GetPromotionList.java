package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PromotionFilterActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.UpdateInternetReceiptActivity;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractInternet;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3v2;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// API lấy danh sách câu lệnh khuyến mãi
public class GetPromotionList implements AsyncTaskCompleteListener<String> {
    private final String GET_PROMOTION_LIST_POST = "GetListPromotionPost";
    private Context mContext;
    private String[] arrParamName, arrParamValue;
    private FragmentRegisterContractInternet fragmentRegister;
    private FragmentRegisterStep3 fragmentRegisterStep2;
    private FragmentRegisterStep3v2 fragmentRegisterStep2v2;

    public GetPromotionList(Context mContext, String LocationId, int LocalType, int ServiceType) {
        this.mContext = mContext;
        GetPromotionsPost(LocationId, LocalType, ServiceType);
    }

    public GetPromotionList(Context mContext, FragmentRegisterStep3 fragment,
                            String LocationId, int LocalType, int ServiceType) {
        this.mContext = mContext;
        this.fragmentRegisterStep2 = fragment;
        GetPromotionsPost(LocationId, LocalType, ServiceType);
    }

    public GetPromotionList(Context mContext, FragmentRegisterStep3v2 fragment,
                            String LocationId, int LocalType, int ServiceType) {
        this.mContext = mContext;
        this.fragmentRegisterStep2v2 = fragment;
        GetPromotionsPost(LocationId, LocalType, ServiceType);
    }

    public GetPromotionList(Context context, FragmentRegisterContractInternet fragment,
                            String LocationId, int LocalType, int ServiceType, String Contract) {
        fragmentRegister = fragment;
        mContext = context;
        arrParamName = new String[]{"LocationID", "LocalType", "UserName", "ServiceType", "Contract"};
        this.arrParamValue = new String[]{LocationId, String.valueOf(LocalType),
                Constants.USERNAME, String.valueOf(ServiceType), Contract};
        String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_PROMOTION_LIST_POST,
                arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionList.this);
        service.execute();
    }


    public GetPromotionList(Context context, String LocationId, int LocalType, int ServiceType, String Contract) {
        mContext = context;
        arrParamName = new String[]{"LocationID", "LocalType", "UserName", "ServiceType", "Contract"};
        this.arrParamValue = new String[]{LocationId, String.valueOf(LocalType),
                Constants.USERNAME, String.valueOf(ServiceType), Contract};
        String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_PROMOTION_LIST_POST,
                arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionList.this);
        service.execute();
    }

    private void GetPromotionsPost(String LocationId, int LocalType, int ServiceType) {
        arrParamName = new String[]{"LocationID", "LocalType", "UserName", "ServiceType"};
        this.arrParamValue = new String[]{LocationId, String.valueOf(LocalType),
                Constants.USERNAME, String.valueOf(ServiceType)};
        String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_PROMOTION_LIST_POST,
                arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionList.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<PromotionModel> lst = null;
            boolean isError = false;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<PromotionModel> resultObject = new WSObjectsModel<>(jsObj, PromotionModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getArrayListObject();
                } else {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError) {
                    loadData(lst);
                }
            }
        } catch (JSONException e) {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
        }
    }

    private void loadData(ArrayList<PromotionModel> lst) {
        if (mContext.getClass().getSimpleName().equals(UpdateInternetReceiptActivity.class.getSimpleName())) {
            // màn hình cập nhật phiếu thu điện tử
            UpdateInternetReceiptActivity activity = (UpdateInternetReceiptActivity) mContext;
            activity.loadPromotion(lst);
        } else if (mContext.getClass().getSimpleName().equals(PromotionFilterActivity.class.getSimpleName())) {
            //màn hình Lọc câu lệnh khuyến mãi
            PromotionFilterActivity activity = (PromotionFilterActivity) mContext;
            activity.updatePromotionList(lst);
        }
        if (fragmentRegister != null) {
            // màn hình phiếu đăng ký bán thêm
            fragmentRegister.updatePromotion(lst);
        } else if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
            // màn hình phiếu đăng ký bán mới
            fragmentRegisterStep2v2.setAutoCompletePromotionAdapter(lst);
            //fragmentRegisterStep2.setAutoCompletePromotionAdapter(lst);
        }
    }
}
