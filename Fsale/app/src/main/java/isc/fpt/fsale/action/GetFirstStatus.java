package isc.fpt.fsale.action;

import android.content.Context;
import android.view.Gravity;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

public class GetFirstStatus implements AsyncTaskCompleteListener<String> {
    private ArrayList<KeyValuePairModel> lstStatus = null;
    private Spinner spFirstStatus;

    private Context mContext;

    public GetFirstStatus(Context _mContext, Spinner firstStatus) {
        this.mContext = _mContext;
        this.spFirstStatus = firstStatus;
        String[] params = new String[]{};
        String[] arrParams = new String[]{""};
        String message = mContext.getResources().getString(R.string.msg_pd_get_first_status);
        String GET_FIRST_STATUS = "GetFirstStatus";
        CallServiceTask service = new CallServiceTask(mContext, GET_FIRST_STATUS, params, arrParams,
                Services.JSON_POST, message, GetFirstStatus.this);
        service.execute();
    }

    private void handleGetFirstStatus(String json) {
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            bindData(jsObj);
        }
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {
            String TAG_FIRST_STATUS_RESULT = "GetFirstStatusMethodPostResult";
            jsArr = jsObj.getJSONArray(TAG_FIRST_STATUS_RESULT);
            int l = jsArr.length();
            if (l > 0) {
                String TAG_ERROR = "ErrorService";
                String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                if (error.equals("null")) {
                    lstStatus = new ArrayList<>();
                    lstStatus.add(new KeyValuePairModel("", "[ Vui lòng chọn tình trạng ]", "", true));
                    for (int i = 0; i < l; i++) {
                        JSONObject iObj = jsArr.getJSONObject(i);
                        String TAG_ACTION = "Action";
                        String TAG_STATUS = "FirstStatus";
                        String TAG_ID = "ID";
                        lstStatus.add(new KeyValuePairModel(iObj.getString(TAG_ID), iObj.getString(TAG_STATUS),
                                iObj.getString(TAG_ACTION), true));
                    }
                } else {
                    Common.alertDialog("Lỗi WS:" + error, mContext);
                }

                KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, android.R.layout.simple_spinner_item, lstStatus, Gravity.LEFT);
                spFirstStatus.setAdapter(adapter);

            } else {
                Common.alertDialog("Không tìm thấy dữ liệu", mContext);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleGetFirstStatus(result);
    }
}
