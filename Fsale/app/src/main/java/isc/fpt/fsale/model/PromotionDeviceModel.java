package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PromotionDeviceModel implements Parcelable {
    private int PromotionID;
    private String PromotionName;
    private int PromotionPrice;

    public PromotionDeviceModel() {
        // TODO Auto-generated constructor stub
    }

    public PromotionDeviceModel(int promotionID, String promotionName,
                                int promotionPrice) {
        super();
        this.PromotionID = promotionID;
        this.PromotionName = promotionName;
        this.PromotionPrice = promotionPrice;
    }

    public static final Creator<PromotionDeviceModel> CREATOR = new Creator<PromotionDeviceModel>() {
        @Override
        public PromotionDeviceModel createFromParcel(Parcel in) {
            return new PromotionDeviceModel(in);
        }

        @Override
        public PromotionDeviceModel[] newArray(int size) {
            return new PromotionDeviceModel[size];
        }
    };

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int promotionID) {
        PromotionID = promotionID;
    }

    public String getPromotionName() {
        return PromotionName;
    }

    public void setPromotionName(String promotionName) {
        PromotionName = promotionName;
    }

    public int getPromotionPrice() {
        return PromotionPrice;
    }

    public void setPromotionPrice(int promotionPrice) {
        PromotionPrice = promotionPrice;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public PromotionDeviceModel(Parcel in) {
        this.PromotionID = in.readInt();
        this.PromotionName = in.readString();
        this.PromotionPrice = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        // TODO Auto-generated method stub
        out.writeInt(this.PromotionID);
        out.writeString(this.PromotionName);
        out.writeInt(this.PromotionPrice);
    }
}
