package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.MainActivity;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/*
 * ACTION: 		GetLocationList
 * @description: - call service api "GetLocationList" and get location list
 * - handle response result: show location list
 * @author: vandn
 * @created on: 14/08/2013
 */

public class GetLocationList implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    GetLocationList(Context mContext) {
        this.mContext = mContext;
        String[] arrParamName = new String[]{"Username"};
        String[] arrParamValue = new String[]{Constants.USERNAME};
        String message = mContext.getResources().getString(R.string.msg_pd_get_location_list);
        String GET_INFO = "GetInfo";
        CallServiceTask service = new CallServiceTask(mContext, GET_INFO, arrParamName, arrParamValue,
                Services.JSON_POST, message, GetLocationList.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    private void handleLocationListResult(String json) {
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = getJsonObject(json);
            if (jsObj != null) {
                try {
                    if (Constants.LST_REGION != null)
                        Constants.LST_REGION.clear();
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);

                    String TAG_ERROR_CODE = "ErrorCode";
                    if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        String LIST_OBJECT = "ListObject";
                        JSONArray jsnArray = jsObj.getJSONArray(LIST_OBJECT);
                        for (int index = 0; index < jsnArray.length(); index++) {
                            JSONObject item = jsnArray.getJSONObject(index);
                            String TAG_LOCATION_ID = "LocationID";
                            if (item.has(TAG_LOCATION_ID)) {
                                Constants.LOCATION_ID = item.getString(TAG_LOCATION_ID);
                                try {
                                    ((MyApp) mContext.getApplicationContext()).setLocationID(item.getInt(TAG_LOCATION_ID));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                            String TAG_LOCATION_NAME = "LocationName";
                            String TAG_MANAGER_NAME = "ManagerName";
                            if (item.has(TAG_MANAGER_NAME)) {
                                Constants.LST_REGION.add(new KeyValuePairModel(Constants.LOCATION_ID, item.getString(TAG_LOCATION_NAME), ""));
                                try {
                                    ((MyApp) mContext.getApplicationContext()).setCityName(item.getString(TAG_LOCATION_NAME));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            String IS_BUY_CAMERA = "IsBuyCamera";
                            if (item.has(IS_BUY_CAMERA))
                                Constants.IS_BUY_CAMERA = item.getInt(IS_BUY_CAMERA);
                            String TAG_BRACH_CODE = "BrachCode";
                            if (item.has(TAG_BRACH_CODE))
                                Constants.BRANCH_CODE = item.getString(TAG_BRACH_CODE);
                            String TAG_IS_MANAGER = "IsManager";
                            if (item.has(TAG_IS_MANAGER) && item.getString(TAG_IS_MANAGER).equals("1"))
                                Constants.IS_MANAGER = true;
                            if (item.has(TAG_MANAGER_NAME))
                                Constants.MANAGER_NAME = item.getString(TAG_MANAGER_NAME);
                            //----ver 3.21 Sale COD -------//
                            String IS_SALE_COD = "IsSaleCOD";
                            if (item.has(IS_SALE_COD))
                                Constants.IS_SALE_COD = item.getBoolean(IS_SALE_COD);
                            //----------------------------//
                            String TAG_ACCOUNT_PORT = "AccountPort";
                            if (item.has(TAG_ACCOUNT_PORT))
                                Constants.MANAGER_NAME = item.getString(TAG_ACCOUNT_PORT);
                            String TAG_PORT_LOCATION_ID = "PortLocationID";
                            if (item.has(TAG_PORT_LOCATION_ID))
                                Constants.PORT_LOCATION_ID = item.getString(TAG_PORT_LOCATION_ID);
                            if (item.has(TAG_LOCATION_NAME))
                                Constants.LOCATION_NAME = item.getString(TAG_LOCATION_NAME);
                            //Lay khu vuc cua Sale de kiem tra dia chi
                            String TAG_LOCATION_ID_PARENT = "LocationParent";
                            if (item.has(TAG_LOCATION_ID_PARENT))
                                ((MyApp) mContext.getApplicationContext()).getUser().setLocationParent(item.getString(TAG_LOCATION_ID_PARENT));
                            //Tinh trang su dung MPOS
                            String TAG_USE_MPOS = "UseMPOS";
                            if (item.has(TAG_USE_MPOS))
                                ((MyApp) mContext.getApplicationContext()).getUser().setUseMPOS(item.getInt(TAG_USE_MPOS));
                            // trạng thái bookport tự động hay bằng tay
                            String TAG_AUTO_BOOKPORT = "AutoBookPort";
                            if (item.has(TAG_AUTO_BOOKPORT)) {
                                Constants.AutoBookPort = item.getInt(TAG_AUTO_BOOKPORT);
                            }
                            // bán kính kéo marker
                            String TAG_AUTO_BOOKPORT_IR = "AutoBookPort_iR";
                            if (item.has(TAG_AUTO_BOOKPORT_IR)) {
                                Constants.AutoBookPort_iR = item.getInt(TAG_AUTO_BOOKPORT_IR);
                            }
                            //số lần tối đa book port tự động
                            String TAG_AUTO_BOOKPORT_RETRY_CONNECT = "AutoBookPort_RetryConnect";
                            if (item.has(TAG_AUTO_BOOKPORT_RETRY_CONNECT)) {
                                Constants.AutoBookPort_RetryConnect = item.getInt(TAG_AUTO_BOOKPORT_RETRY_CONNECT);
                            }
                            // thời gian time out connect đến server
                            String TAG_TIME_OUT_BOOK_PORT_AUTO = "AutoBookPort_Timeout";
                            if (item.has(TAG_TIME_OUT_BOOK_PORT_AUTO)) {
                                Constants.AutoBookPort_Timeout = item.getInt(TAG_TIME_OUT_BOOK_PORT_AUTO);
                            }
                            // đường dẫn chăm sóc khách hàng pay tv
                            String TAG_LINK_CSKH_PAYTV = "link_cskh_paytv";
                            if (item.has(TAG_LINK_CSKH_PAYTV)) {
                                Constants.Link_CSKH_PayTv = item.getString(TAG_LINK_CSKH_PAYTV);
                            }
                            //IBBMemberID
                            String TAG_IBB_MEMBER_ID = "IBBMemberID";
                            if (item.has(TAG_IBB_MEMBER_ID)) {
                                Constants.IBB_MEMBER_ID = item.getString(TAG_IBB_MEMBER_ID);
                            }
                            //danh sách hạ tầng book port
                            String TAG_TYPE_PORT_OF_SALE = "TypePortOfSale";
                            if (item.has(TAG_TYPE_PORT_OF_SALE)) {
                                ArrayList<KeyValuePairModel> listTypePortOfSale = new ArrayList<>();
                                JSONArray jsArr = item.getJSONArray(TAG_TYPE_PORT_OF_SALE);
                                for (int i = 0; i < jsArr.length(); i++) {
                                    int id = 0;
                                    JSONObject typePortOfSale = jsArr.getJSONObject(i);
                                    String TAG_ID_PORT_OF_SALE = "Id";
                                    if (typePortOfSale.has(TAG_ID_PORT_OF_SALE))
                                        id = typePortOfSale.getInt(TAG_ID_PORT_OF_SALE);
                                    String name = "";
                                    String TAG_NAME_PORT_OF_SALE = "Name";
                                    if (typePortOfSale.has(TAG_NAME_PORT_OF_SALE))
                                        name = typePortOfSale.getString(TAG_NAME_PORT_OF_SALE);
                                    listTypePortOfSale.add(new KeyValuePairModel(id, name));
                                }
                                Constants.TypePortOfSale = listTypePortOfSale;
                            }
                            // key quét thông tin cmnd
                            String TAG_RECOGNIZE_ID_CARD_API_KEY = "RecognizeIDCardAPIKey";
                            if (item.has(TAG_RECOGNIZE_ID_CARD_API_KEY)) {
                                Constants.KEY_SCAN_IDENTITY_CARD = item.getString(TAG_RECOGNIZE_ID_CARD_API_KEY);
                            }

                            //CHECK ENABLE MGT
                            String TAG_IS_ENABLED_REFERRAL = "IsEnableReferral";
                            if(item.has(TAG_IS_ENABLED_REFERRAL)){
                                Constants.IS_ENABLE_REFERRAL = item.getInt(TAG_IS_ENABLED_REFERRAL);
                            }

                        }
                    } else {
                        Constants.LST_REGION.add(new KeyValuePairModel("-1", "-1", ""));
                        String TAG_ERROR = "Error";
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            boolean createLocalSchedule = true;
            new GetAllPotentialObjSchedule(mContext, Constants.USERNAME, null,
                    null, null, String.valueOf(1), createLocalSchedule);
            registerGCM();
            gotoMain();
        }
    }

    private void gotoMain() {
        try {
            mContext.startActivity(new Intent(mContext, MainActivity.class));
            ((Activity) mContext).finish();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            return JSONParsing.getJsonObj(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //đăng ký regID GCM mới và cập nhật regID nếu regID thay đổi.
    private void registerGCM() {
        try {
            String currentRegID = Common.loadPreference(mContext,
                    Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_REG_ID);
            if (currentRegID.equals("")) {
                Common.RegGCM(mContext, false, 0);
            } else {
                new GCM_RegisterPushNotification(mContext, currentRegID, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleLocationListResult(result);
    }
}
