package isc.fpt.fsale.adapter;

import isc.fpt.fsale.activity.ListReportPayTVDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportItemModel;
import isc.fpt.fsale.model.ReportPayTVModel;
import isc.fpt.fsale.utils.Common;
import java.util.ArrayList;
import java.util.List;

import com.danh32.fontify.TextView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;


public class ReportPayTVAdapter extends BaseAdapter{
	public static final String TAG_SALE_NAME = "TAG_SALE_NAME";
	public static final String TAG_FROMDATE = "TAG_FROMDATE";
	public static final String TAG_TODATE = "TAG_TODATE";
	public static final String TAG_MONTH = "TAG_MONTH";
	public static final String TAG_YEAR = "TAG_YEAR";
	public static final String TAG_LOCAL_TYPE = "TAG_LOCAL_TYPE";
	public static final String TAG_PACKAGE = "TAG_PACKAGE";
	
	private List<ReportPayTVModel> mList;	
	private Context mContext;
	private int month, year;
	private String fromDate, toDate;
	public ReportPayTVAdapter(Context context, ArrayList<ReportPayTVModel> lst, int month, int year){
		this.mContext = context;
		this.mList = lst;		
		this.month = month;
		this.year = year;
	}
	public ReportPayTVAdapter(Context context, ArrayList<ReportPayTVModel> lst,String fromDate, String toDate, int month, int year){
		this.mContext = context;
		this.mList = lst;		
		this.month = month;
		this.year = year;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportPayTVModel item = mList.get(position);		

		if (convertView == null) {			
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_sbi_total, null);
		}
		try {
			TextView rowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			TextView saleName = (TextView)convertView.findViewById(R.id.lbl_report_sale_name);
			LinearLayout frmData = (LinearLayout)convertView.findViewById(R.id.frm_report_data);
			if(frmData.getChildCount()>0){
				frmData.removeAllViews();
			}			
			if(item != null){					
				rowNumber.setText(String.valueOf(item.getRowNumber()));
				saleName.setText(item.getSaleName());		
				if(item.getItems() != null && item.getItems().size() >0){
					for (ReportItemModel subItem : item.getItems()) {
						frmData.addView(initRowItem(item.getSaleName(), subItem));
					}					
				}
			}				
		} catch (Exception e) {

			Common.alertDialog("ReportPotentialObjTotalAdapter.getView():" + e.getMessage(), mContext);
		}
		
		return convertView;
	}
	
	@SuppressLint("InflateParams")
	private LinearLayout initRowItem(final String saleName,  ReportItemModel item){
		try {
			final int value = item.getTotal();
			final ReportItemModel tempItem = item;
			View viewChild = LayoutInflater.from(mContext).inflate( R.layout.sub_row_report_total, null );
			LinearLayout frmDataRow = (LinearLayout)viewChild.findViewById(R.id.frm_report_data_row);
			frmDataRow.setVisibility(View.VISIBLE);		
			TextView lblTitle = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_title);
			TextView lblTotal = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_total);
			lblTitle.setText(tempItem.getTitle());
			lblTotal.setText(String.valueOf(value));
			lblTotal.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub					
					try {
						if(value > 0)
							startAcitivity(saleName, tempItem);	
					} catch (Exception e) {

						e.printStackTrace();
					}
									
				}
			});
			return frmDataRow;
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			return null;
		}		
	}
	
	private void startAcitivity(String saleName, ReportItemModel item){
		Intent intent = new Intent(mContext, ListReportPayTVDetailActivity.class);
		if(saleName != null && !saleName.equals(""))
			intent.putExtra(TAG_SALE_NAME, saleName);
		if(this.month >0)
			intent.putExtra(TAG_MONTH, this.month);
		if(this.year >0)
			intent.putExtra(TAG_YEAR, this.year);
		if(this.fromDate != null)
			intent.putExtra(TAG_FROMDATE , this.fromDate);
		if(this.toDate != null)
			intent.putExtra(TAG_TODATE, this.toDate);
		if(item != null){
			intent.putExtra(TAG_LOCAL_TYPE, item.getLocalType());
			intent.putExtra(TAG_PACKAGE, item.getPackage());
		}
		mContext.startActivity(intent);
	}
}
