package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class GetListObjectHDBoxAction implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public GetListObjectHDBoxAction(Context mContext, String Contract, String searchType, String pageNumber) {
        this.mContext = mContext;

        String[] arrParamName = new String[]{"UserName", "Agent", "AgentName", "PageNumber"};
        String[] arrParamValue = new String[]{Constants.USERNAME, searchType, Contract, pageNumber};
        String message = mContext.getResources().getString(R.string.msg_pd_get_search_contact);
        String TAG_GET_OBJECT_HD_BOX_LIST = "GetObjectHDBoxList";
        CallServiceTask service = new CallServiceTask(mContext, TAG_GET_OBJECT_HD_BOX_LIST, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetListObjectHDBoxAction.this);
        service.execute();
    }

    private void HandleResultSearch(String json) {
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if (!Common.isEmptyJSONObject(jsObj)){
                bindData(jsObj);
            }
        } else {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
        }
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {
            String TAG_GET_OBJECT_HD_BOX_LIST_RESULT = "GetObjectHDBoxListResult";
            jsArr = jsObj.getJSONArray(TAG_GET_OBJECT_HD_BOX_LIST_RESULT);
            int l = jsArr.length();
            if (l > 0) {
			/*	ArrayList<ObjectModel> lstObject = new ArrayList<ObjectModel>();
			 * String error = jsArr.toJSONObject(0).getString(TAG_ERROR);
				String totalPage="1", currentPage="1",rownumber, contract, fullname, address, objid,  SupportINFID ="", PortalObjID="",TryingStatus="",
						TryingStatusID = "", phone = "", comment="", depName="",subDepID="",
						NotTrialNote ="",ReasonID="";
				if(error.equals("null")){
						for(int i=0; i<l;i++){
							JSONObject iObj = jsArr.toJSONObject(i);

							rownumber = iObj.getString(TAG_ROWNUMBER);
							contract = iObj.getString(TAG_CONTRACT);
							fullname = iObj.getString(TAG_FULLNAME);
							address = iObj.getString(TAG_ADDRESS);
							objid = iObj.getString(TAG_ObjID);

							if(iObj.has(TAG_SUPPORTINFID))
								SupportINFID = iObj.getString(TAG_SUPPORTINFID);
							if(iObj.has(TAG_PORTALOBJID))
								PortalObjID = iObj.getString(TAG_PORTALOBJID);
							if(iObj.has(TAG_TRYINGSTATUS))
								TryingStatus = iObj.getString(TAG_TRYINGSTATUS);
							if(iObj.has(TAG_TRYINGSTATUSID))
								TryingStatusID = iObj.getString(TAG_TRYINGSTATUSID);
							if(iObj.has(TAG_PHONE))
								phone = iObj.getString(TAG_PHONE);
							if(iObj.has(TAG_COMMENT))
								comment = iObj.getString(TAG_COMMENT);
							if(iObj.has(TAG_DEPNAME))
								depName = iObj.getString(TAG_DEPNAME);
							if(iObj.has(TAG_SUPDEPID))
								subDepID = iObj.getString(TAG_SUPDEPID);
							if(iObj.has(TAG_TOTALPAGE))
								totalPage = iObj.getString(TAG_TOTALPAGE);
							if(iObj.has(TAG_CURRENTPAGE))
								currentPage = iObj.getString(TAG_CURRENTPAGE);
							if(iObj.has(TAG_NOTTRIALNOTE))
								NotTrialNote = iObj.getString(TAG_NOTTRIALNOTE);
							if(iObj.has(TAG_REASONID))
								ReasonID = iObj.getString(TAG_REASONID);

							//lstObject.add(new ObjectModel(rownumber,contract,fullname,address, objid, SupportINFID, PortalObjID,TryingStatus, TryingStatusID, phone, comment, depName, subDepID,NotTrialNote,ReasonID));

						}
						String pageSize = currentPage+";"+ totalPage;
						UpdateSpinnerPage(pageSize);
						lvObjectList.setAdapter(new ObjectListIPTVAdapter(mContext, lstObject));
						lvObjectList.setVisibility(View.VISIBLE);
				}

				else{
						Common.alertDialog("Lỗi WS:" + error, mContext);
					}*/
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        HandleResultSearch(result);
    }
}