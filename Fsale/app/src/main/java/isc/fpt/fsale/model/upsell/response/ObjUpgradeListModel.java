package isc.fpt.fsale.model.upsell.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import isc.fpt.fsale.services.Services;

public class ObjUpgradeListModel implements Parcelable {

    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Contract")
    @Expose
    private String contract;
    @SerializedName("ErrorService")
    @Expose
    private Object errorService;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("LocalTypeName")
    @Expose
    private String localTypeName;
    @SerializedName("ObjID")
    @Expose
    private Integer objID;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Promotion")
    @Expose
    private Integer promotion;
    @SerializedName("PromotionName")
    @Expose
    private String promotionName;

    public ObjUpgradeListModel(String address, String contract, Object errorService, String fullName, String localTypeName, Integer objID, String phone, Integer promotion, String promotionName) {
        this.address = address;
        this.contract = contract;
        this.errorService = errorService;
        this.fullName = fullName;
        this.localTypeName = localTypeName;
        this.objID = objID;
        this.phone = phone;
        this.promotion = promotion;
        this.promotionName = promotionName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public Object getErrorService() {
        return errorService;
    }

    public void setErrorService(Object errorService) {
        this.errorService = errorService;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLocalTypeName() {
        return localTypeName;
    }

    public void setLocalTypeName(String localTypeName) {
        this.localTypeName = localTypeName;
    }

    public Integer getObjID() {
        return objID;
    }

    public void setObjID(Integer objID) {
        this.objID = objID;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getPromotion() {
        return promotion;
    }

    public void setPromotion(Integer promotion) {
        this.promotion = promotion;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    protected ObjUpgradeListModel(Parcel in) {
        address = in.readString();
        contract = in.readString();
        fullName = in.readString();
        localTypeName = in.readString();
        if (in.readByte() == 0) {
            objID = null;
        } else {
            objID = in.readInt();
        }
        phone = in.readString();
        if (in.readByte() == 0) {
            promotion = null;
        } else {
            promotion = in.readInt();
        }
        promotionName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeString(contract);
        dest.writeString(fullName);
        dest.writeString(localTypeName);
        if (objID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(objID);
        }
        dest.writeString(phone);
        if (promotion == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(promotion);
        }
        dest.writeString(promotionName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObjUpgradeListModel> CREATOR = new Creator<ObjUpgradeListModel>() {
        @Override
        public ObjUpgradeListModel createFromParcel(Parcel in) {
            return new ObjUpgradeListModel(in);
        }

        @Override
        public ObjUpgradeListModel[] newArray(int size) {
            return new ObjUpgradeListModel[size];
        }
    };
}

