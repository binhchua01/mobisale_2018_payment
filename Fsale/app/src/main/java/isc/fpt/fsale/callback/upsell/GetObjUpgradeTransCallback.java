package isc.fpt.fsale.callback.upsell;

import java.util.ArrayList;

import isc.fpt.fsale.model.upsell.response.ObjUpgradeTransListModel;

public interface GetObjUpgradeTransCallback {
    void loadListSuccess(ArrayList<ObjUpgradeTransListModel> lst );
}
