package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListReportSurveyManagerTotalAction;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ReportSurveyManegerTotalAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportSurveyManagerTotalModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

//màn hình BÁO CÁO KHẢO SÁT
public class ListReportSurveyManagerTotalActivity extends BaseActivity implements OnItemClickListener {

    private Spinner spDay, spMonth, spYear, spAgent;
    private ListView lvResult;
    private LinearLayout frmFind;
    private ImageView imgNavigation;
    private Spinner spPage;
    private EditText txtAgentName;

    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
    private int Day = 0, Month = 0, Year = 0, Agent = 0;
    private String AgentName;
    //================================

    public ListReportSurveyManagerTotalActivity() {
        super(R.string.lbl_ReportSurvey_Manger);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        Common.hideSoftKeyboard(this);
        return true;
    }

    @SuppressWarnings("static-access")
    @Override
    public void startActivity(Intent intent) {
        // TODO Auto-generated method stub
        //Finish this activity after start another activity without Sub Report

        if (!intent.getComponent().getClassName().equals(ListReportSurveyManagerActivity.class.getName()))
            finish();
        else {
            if (isShowLeftMenu() && Constants.SLIDING_MENU != null)
                toggle(Constants.SLIDING_MENU.LEFT);
        }
        super.startActivity(intent);
    }

    @SuppressLint("InflateParams")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_report_survey_manager_total_activity));

        setContentView(R.layout.activity_report_survey_total);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        spDay = (Spinner) findViewById(R.id.sp_day);
        spMonth = (Spinner) findViewById(R.id.sp_month);
        spYear = (Spinner) findViewById(R.id.sp_year);

        lvResult = (ListView) findViewById(R.id.lv_report);
        txtAgentName = (EditText) findViewById(R.id.txt_agent_name);

        final Button btnFind = (Button) findViewById(R.id.btn_find);
        imgNavigation = (ImageView) findViewById(R.id.img_navigation_drop_down);
        frmFind = (LinearLayout) findViewById(R.id.frm_find);
        btnFind.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (FLAG_FIRST_LOAD > 0)
                    FLAG_FIRST_LOAD = 0;
                getReport(1);
            }
        });

        imgNavigation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dropDownNavigation();
            }
        });
        spYear.setOnItemSelectedListener(new OnItemSelectedListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                int month = 0, year = 0;
                try {
                    year = item.getID();
                    month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
                } catch (Exception e) {

                    Log.i("ContractFilterActivity_spYear.setOnItemSelectedListener:", e.getMessage());
                }
                //common.
                Common.initDaySpinner(ListReportSurveyManagerTotalActivity.this, spDay, month, year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spMonth.setOnItemSelectedListener(new OnItemSelectedListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                int month = 0, year = 0;
                try {
                    month = item.getID();
                    year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();
                } catch (Exception e) {

                    Log.i("ContractFilterActivity_spMonth.setOnItemSelectedListener:", e.getMessage());
                }
                Common.initDaySpinner(ListReportSurveyManagerTotalActivity.this, spDay, month, year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        lvResult.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            }
        });

        lvResult.setItemsCanFocus(true);
        lvResult.setOnItemClickListener(this);
        lvResult.setTextFilterEnabled(true);
        lvResult.setSelection(0);

        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getReport(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spAgent = (Spinner) findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() > 0) {
                    txtAgentName.setEnabled(true);
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        Common.initYearSpinner(this, spYear);
        Common.initMonthSpinner(this, spMonth);
        //initSpinnerStatus();
        initSpinnerAgent();

        // super.addRight(new MenuRightReport());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    /*@Override
    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_right) {
            toggle(SlidingMenu.RIGHT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //MenuInflater inflater = getSupportMenuInflater();
        //inflater.inflate(R.menu.main, menu);
        return false;
    }


    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        ReportSurveyManagerTotalModel selectedItem = (ReportSurveyManagerTotalModel) parentView.getItemAtPosition(position);
        Intent intent = new Intent(this, ListReportSurveyManagerActivity.class);
        int Day = ((KeyValuePairModel) spDay.getSelectedItem()).getID();
        int Month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
        int Year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();
        intent.putExtra("DAY", Day);
        intent.putExtra("MONTH", Month);
        intent.putExtra("YEAR", Year);
        intent.putExtra("SALE_NAME", selectedItem.getSaleName());
        this.startActivity(intent);
		
			/*ReportSurveyManagerTotalModel selectedItem = (ReportSurveyManagerTotalModel)parentView.getItemAtPosition(position);
			
			int Day = ((KeyValuePairModel)spDay.getSelectedItem()).getID();
			int Month = ((KeyValuePairModel)spMonth.getSelectedItem()).getID();
			int Year = ((KeyValuePairModel)spYear.getSelectedItem()).getID();
			
			//String params = Services.getParams(new String[]{Constants.USERNAME,Month,Year,"1",selectedItem.getSaleName(),Page});
			
			new GetListReportSurveyManagerAction(mContext, Day, Month, Year, "1", selectedItem.getSaleName(), 1);*/

    }


    //====================================== Get/Load data ======================
    private void initSpinnerAgent() {
        //Loai Tim kiem
        ArrayList<KeyValuePairModel> ListSpAgent = new ArrayList<KeyValuePairModel>();
        ListSpAgent.add(new KeyValuePairModel(0, "Tất cả"));
        ListSpAgent.add(new KeyValuePairModel(1, "Nhân viên"));
        KeyValuePairAdapter adapterAgent = new KeyValuePairAdapter(this, R.layout.my_spinner_style, ListSpAgent, Gravity.LEFT);
        spAgent.setAdapter(adapterAgent);

    }

    public void LoadData(ArrayList<ReportSurveyManagerTotalModel> lst) {
        if (lst != null && lst.size() > 0) {
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                dropDownNavigation();
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
                //adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            ReportSurveyManegerTotalAdapter adapter = new ReportSurveyManegerTotalAdapter(this, lst);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
            lvResult.setAdapter(new ReportSurveyManegerTotalAdapter(this, new ArrayList<ReportSurveyManagerTotalModel>()));
        }

    }

    private void getReport(int pageNumber) {

        Day = ((KeyValuePairModel) spDay.getSelectedItem()).getID();
        Month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
        Year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();
        Agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
        AgentName = txtAgentName.getText().toString();
        if (Agent > 0 && AgentName.equals("")) {
            Common.alertDialog(getString(R.string.msg_search_value_empty), this);
        } else {
            new GetListReportSurveyManagerTotalAction(this, Day, Month, Year, Agent, AgentName, pageNumber);
        }

    }

    //TODO: dropDown Search frm
    private void dropDownNavigation() {
        if (frmFind.getVisibility() == View.VISIBLE) {
            frmFind.setVisibility(View.GONE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
        } else {
            frmFind.setVisibility(View.VISIBLE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
        }
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }
}
