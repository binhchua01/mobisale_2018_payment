package isc.fpt.fsale.action;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.DetailPackage;
import isc.fpt.fsale.model.PromotionExtraOtt;
import isc.fpt.fsale.model.ServiceListExtraOtt;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.extra_ott.RegisterExtraOttModel;
import isc.fpt.fsale.ui.extra_ott.promotion_extra_ott.PromotionExtraOttActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 20,July,2019
 */
public class GetPromotionExtraOtt implements AsyncTaskCompleteListener<String> {
    private PromotionExtraOttActivity activity;

    public GetPromotionExtraOtt(PromotionExtraOttActivity activity,
                                RegisterExtraOttModel object, ServiceListExtraOtt mService) {
        this.activity = activity;
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            List<DetailPackage> lst = mService.getPromoCommand().getDetailPackage();
            for(DetailPackage item: lst){
                if(item.getiD() != null){
                    jsonArray.put(item.getiD());
                }
            }
            jsonObject.put("ListPackage", jsonArray);
            jsonObject.put("SaleTypeID", object.getCartExtraOtt().getSaleType());
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("BillTo_District", object.getBillTo_District());
            jsonObject.put("BillTo_Ward", object.getBillTo_Ward());
            String message = activity.getResources().getString(R.string.txt_message_get_extra_ott_promotion);
            String GET_PACKAGE_LIST_EXTRA_OTT = "PromotionList_ExOTT";
            CallServiceTask service = new CallServiceTask(activity, GET_PACKAGE_LIST_EXTRA_OTT, jsonObject,
                    Services.JSON_POST_OBJECT, message, GetPromotionExtraOtt.this);
            service.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<PromotionExtraOtt> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), PromotionExtraOtt.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), activity);
                } else {
                    activity.loadPromotionList(lst);
                }
            }
        } catch (JSONException e) {
            Common.alertDialog(activity.getResources().getString(R.string.msg_error_data), activity);
        }
    }
}
