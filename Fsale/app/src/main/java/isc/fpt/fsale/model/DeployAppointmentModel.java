package isc.fpt.fsale.model;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table DEPLOY_APPOINTMENT_MODEL.
 */
public class DeployAppointmentModel {

    private Integer ID;
    private Integer SupID;
    private Integer PartnerID;
    private String Partner;
    private Integer SubID;
    private String Timezone;
    private String Date;
    private String Account;
    private String AppointmentDate;
    private Integer ObjID;
    private String RegCode;
    private String Contract;

    public DeployAppointmentModel() {
    }

    public DeployAppointmentModel(Integer ID, Integer SupID, Integer PartnerID, String Partner, Integer SubID, String Timezone, String Date, String Account, String AppointmentDate, Integer ObjID, String RegCode, String Contract) {
        this.ID = ID;
        this.SupID = SupID;
        this.PartnerID = PartnerID;
        this.Partner = Partner;
        this.SubID = SubID;
        this.Timezone = Timezone;
        this.Date = Date;
        this.Account = Account;
        this.AppointmentDate = AppointmentDate;
        this.ObjID = ObjID;
        this.RegCode = RegCode;
        this.Contract = Contract;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Integer getSupID() {
        return SupID;
    }

    public void setSupID(Integer SupID) {
        this.SupID = SupID;
    }

    public Integer getPartnerID() {
        return PartnerID;
    }

    public void setPartnerID(Integer PartnerID) {
        this.PartnerID = PartnerID;
    }

    public String getPartner() {
        return Partner;
    }

    public void setPartner(String Partner) {
        this.Partner = Partner;
    }

    public Integer getSubID() {
        return SubID;
    }

    public void setSubID(Integer SubID) {
        this.SubID = SubID;
    }

    public String getTimezone() {
        return Timezone;
    }

    public void setTimezone(String Timezone) {
        this.Timezone = Timezone;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getAccount() {
        return Account;
    }

    public void setAccount(String Account) {
        this.Account = Account;
    }

    public String getAppointmentDate() {
        return AppointmentDate;
    }

    public void setAppointmentDate(String AppointmentDate) {
        this.AppointmentDate = AppointmentDate;
    }

    public Integer getObjID() {
        return ObjID;
    }

    public void setObjID(Integer ObjID) {
        this.ObjID = ObjID;
    }

    public String getRegCode() {
        return RegCode;
    }

    public void setRegCode(String RegCode) {
        this.RegCode = RegCode;
    }

    public String getContract() {
        return Contract;
    }

    public void setContract(String Contract) {
        this.Contract = Contract;
    }

}
