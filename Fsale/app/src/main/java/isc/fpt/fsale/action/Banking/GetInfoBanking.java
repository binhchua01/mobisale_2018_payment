package isc.fpt.fsale.action.Banking;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.callback.banking.InfoBankingCallback;
import isc.fpt.fsale.callback.banking.ListBankingCallback;
import isc.fpt.fsale.model.Banking.InfoBankingModel;
import isc.fpt.fsale.model.Banking.ListBankingModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetInfoBanking implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    InfoBankingCallback mCallBack;

    public GetInfoBanking(Context mContext, String[] value, InfoBankingCallback mCallBack) {
        this.mContext = mContext;
        this.mCallBack = mCallBack;
        String[] params = new String[]{"SaleName", "BankId"};
        String message = "Vui lòng chờ ...";
        String GET_INFO_BANKING= "GetInfoBanking";
        CallServiceTask service = new CallServiceTask(mContext, GET_INFO_BANKING, params, value, Services.JSON_POST, message, GetInfoBanking.this);
        service.execute();
    }


    @Override
    public void onTaskComplete(String result) throws JSONException {
        try {
            List<InfoBankingModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), InfoBankingModel.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        mCallBack.onGetInfoSuccess(lst.get(0));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}


