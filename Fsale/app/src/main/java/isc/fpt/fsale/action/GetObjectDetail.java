package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ObjectDetailActivity;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.FptCameraSelectComboActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.google.gson.Gson;

// API lấy thông tin hợp đồng
public class GetObjectDetail implements AsyncTaskCompleteListener<String> {
    public static final String METHOD_NAME = "GetObjectDetail";
    public static final String[] paramNames = {"UserName", "Contract"};
    private Context mContext;

    //Add by: DuHK
    public GetObjectDetail(Context mContext, String Contract) {
        this.mContext = mContext;
        String[] paramValues = {Constants.USERNAME, Contract};
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, GetObjectDetail.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            //haulc3 modify
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                ObjectDetailModel obj = new Gson().fromJson(
                        jsObj.getJSONArray("ListObject").get(0).toString(), ObjectDetailModel.class);
                loadData(obj);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //this function modify by haulc3
    private void loadData(ObjectDetailModel obj) {
        try {
            if (obj != null) {
                if (mContext.getClass().getSimpleName().equals(ObjectDetailActivity.class.getSimpleName())) {
                    ObjectDetailActivity activity = (ObjectDetailActivity) mContext;
                    activity.loadData(obj);
                } else if (mContext.getClass().getSimpleName().equals(FptCameraSelectComboActivity.class.getSimpleName())) {
                    FptCameraSelectComboActivity activity = (FptCameraSelectComboActivity) mContext;
                    activity.loadDataObjectDetail(obj);
                }
            } else {
                Common.alertDialog("Không có dữ liệu.", mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
