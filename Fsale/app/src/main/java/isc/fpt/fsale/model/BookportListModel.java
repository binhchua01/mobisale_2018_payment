package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;


public class BookportListModel implements Parcelable {

	
	private String ID;
	private String Name;
	private String IsEmptyPort;
	private String Distance;
	private int Type;
	
	public BookportListModel(String _ID,String _Name,String _IsEmptyPort,String _Distance, int _Type)
	{
		this.ID=_ID;
		this.Name=_Name;
		this.IsEmptyPort=_IsEmptyPort;
		this.Distance=_Distance;
		this.Type=_Type;
	}
	
	
	public String getID()
	{
		return this.ID;
	}
	
	public void setID(String _ID)
	{
		this.ID=_ID;
	}
	
	
	public String getName()
	{
		return this.Name;
	}
	
	public void setName(String _Name)
	{
		this.Name=_Name;
	}
	
	public String getIsEmptyPort()
	{
		return this.IsEmptyPort;
	}
	
	public void setIsEmptyPort(String _IsEmptyPort)
	{
		this.IsEmptyPort=_IsEmptyPort;
	}
	
	public String getDistance()
	{
		return this.Distance;
	}
	
	public void setDistance(String _Distance)
	{
		this.Distance=_Distance;
	}
	
	public int getType()
	{
		return this.Type;
	}
	
	public void setType(int _Type)
	{
		this.Type=_Type;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel pc, int flags) {
		// TODO Auto-generated method stub
		pc.writeString(ID);
		pc.writeString(Name);
		pc.writeString(IsEmptyPort);
		pc.writeInt(Type);
		
	}
	public BookportListModel(Parcel source){
		
		ID = source.readString();
		Name = source.readString();
		IsEmptyPort = source.readString();
		Type = source.readInt();
	}
	/** Static field used to regenerate object, individually or as arrays */
	public static final Creator<BookportListModel> CREATOR = new Creator<BookportListModel>() {
		@Override     
		public BookportListModel createFromParcel(Parcel source) {
	             return new BookportListModel(source);
		}
		@Override
		public BookportListModel[] newArray(int size) {
             return new BookportListModel[size];
        }
	};
}
