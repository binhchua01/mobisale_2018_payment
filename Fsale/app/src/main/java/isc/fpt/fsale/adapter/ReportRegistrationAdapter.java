package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportRegistrationModel;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReportRegistrationAdapter  extends BaseAdapter {

	private ArrayList<ReportRegistrationModel> mList;	
	private Context mContext;
	
	public ReportRegistrationAdapter(Context context, ArrayList<ReportRegistrationModel> lst){
		this.mContext = context;
		this.mList = lst;	
		
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportRegistrationModel ReportRegistration = mList.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_registration, null);
		}
		
		TextView txtRowNumber = (TextView) convertView.findViewById(R.id.txt_row_number);
		txtRowNumber.setText(ReportRegistration.getRowNumber());
		
		// Ten sale
		TextView txtSaleName = (TextView) convertView.findViewById(R.id.txt_SaleName);
		txtSaleName.setText(ReportRegistration.getSaleName());
		
		// So luong tong
		TextView txtTotal = (TextView) convertView.findViewById(R.id.txt_Total);
		txtTotal.setText(String.valueOf(ReportRegistration.getTotal()));
		
		// So luong phieu dang ky da len hop dong
		TextView txtTotalComplete = (TextView) convertView.findViewById(R.id.txt_TotalComplete);
		txtTotalComplete.setText(String.valueOf(ReportRegistration.getTotalComplete()));
		
		// So luong phieu dang ky chua len hop dong
		TextView txtTotalInComplete = (TextView) convertView.findViewById(R.id.txt_TotalInComplete);
		txtTotalInComplete.setText(String.valueOf(ReportRegistration.getTotalInComplete()));
		
		return convertView;
	}

}
