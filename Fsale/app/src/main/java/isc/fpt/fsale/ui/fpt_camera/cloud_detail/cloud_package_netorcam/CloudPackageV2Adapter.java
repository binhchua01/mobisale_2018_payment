package isc.fpt.fsale.ui.fpt_camera.cloud_detail.cloud_package_netorcam;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;

/**
 * Created by haulc3 on 01,April,2019
 */
public class CloudPackageV2Adapter extends RecyclerView.Adapter<CloudPackageV2Adapter.SimpleViewHolder> {
    private Context mContext;
    private List<CloudDetail> mList;
    private OnItemClickListener mListener;

    CloudPackageV2Adapter(Context mContext, List<CloudDetail> mList, OnItemClickListener mListener) {
        this.mList = mList;
        this.mContext = mContext;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_dialog_cloud_type, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        private View layout;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv_item_name);
            layout = itemView.findViewById(R.id.layout_background);
        }

        void bindView(final CloudDetail mCloudDetail) {
            textView.setText(mCloudDetail.getPackName());
            if (mCloudDetail.isSelected()) {
                layout.setBackgroundResource(R.drawable.background_radius_selected);
            }

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!mCloudDetail.isSelected()) {
                        whenItemClicked(mCloudDetail);
                    }
                }
            });
        }
    }

    private void whenItemClicked(CloudDetail mCloudDetail) {
        mCloudDetail.setPackID(mCloudDetail.getPackID());
        mCloudDetail.setPackName(mCloudDetail.getPackName());
        mCloudDetail.setTypeID(mCloudDetail.getTypeID());
        mCloudDetail.setTypeName(mCloudDetail.getTypeName());
        mCloudDetail.setCost(mCloudDetail.getCost());
        mCloudDetail.setQty(1);
        mCloudDetail.setServiceType(mCloudDetail.getServiceType());

        mListener.onItemClick(mCloudDetail);
    }

    public void notifyData(List<CloudDetail> mList){
        this.mList = mList;
        notifyDataSetChanged();
    }
}
