package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CreateObject_2018POST implements Parcelable {
    private String userName;
    private String regCode;
    private String total;
    private String imageSignature;

    /**
     * Constructor
     */
    public CreateObject_2018POST() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getImageSignature() {
        return imageSignature;
    }

    public void setImageSignature(String imageSignature) {
        this.imageSignature = imageSignature;
    }


    protected CreateObject_2018POST(Parcel in) {
        userName = in.readString();
        regCode = in.readString();
        total = in.readString();
        imageSignature = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(regCode);
        dest.writeString(total);
        dest.writeString(imageSignature);
    }
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CreateObject_2018POST> CREATOR = new Creator<CreateObject_2018POST>() {
        @Override
        public CreateObject_2018POST createFromParcel(Parcel in) {
            return new CreateObject_2018POST(in);
        }

        @Override
        public CreateObject_2018POST[] newArray(int size) {
            return new CreateObject_2018POST[size];
        }
    };

}
