package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by haulc3 on 01,April,2019
 */
public class AllCloudType implements Parcelable {
    private int TypeID;
    private String TypeName;
    private boolean isSelected = false;

    public AllCloudType() {
    }

    private AllCloudType(Parcel in) {
        TypeID = in.readInt();
        TypeName = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<AllCloudType> CREATOR = new Creator<AllCloudType>() {
        @Override
        public AllCloudType createFromParcel(Parcel in) {
            return new AllCloudType(in);
        }

        @Override
        public AllCloudType[] newArray(int size) {
            return new AllCloudType[size];
        }
    };

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getTypeID() {
        return TypeID;
    }

    public void setTypeID(int typeID) {
        this.TypeID = typeID;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        this.TypeName = typeName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(TypeID);
        parcel.writeString(TypeName);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
    }
}
