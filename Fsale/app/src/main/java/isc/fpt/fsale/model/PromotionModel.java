package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "PROMOTION_MODEL".
 */
// model câu lệnh khuyến mãi internet
public class PromotionModel implements Parcelable{

    private int PromotionID;
    private String PromotionName;
    private int RealPrepaid;
    private String ImageUrl;
    private String VideoUrl;
    private String TextNote;

    public PromotionModel() {
    }

    public PromotionModel(int PromotionID, String PromotionName, int RealPrepaid,
                          String ImageUrl, String VideoUrl, String TextNote) {
        this.PromotionID = PromotionID;
        this.PromotionName = PromotionName;
        this.RealPrepaid = RealPrepaid;
        this.ImageUrl = ImageUrl;
        this.VideoUrl = VideoUrl;
        this.TextNote = TextNote;
    }

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int PromotionID) {
        this.PromotionID = PromotionID;
    }

    public String getPromotionName() {
        return PromotionName;
    }

    public void setPromotionName(String PromotionName) {
        this.PromotionName = PromotionName;
    }

    public int getRealPrepaid() {
        return RealPrepaid;
    }

    public void setRealPrepaid(int RealPrepaid) {
        this.RealPrepaid = RealPrepaid;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String ImageUrl) {
        this.ImageUrl = ImageUrl;
    }

    public String getVideoUrl() {
        return VideoUrl;
    }

    public void setVideoUrl(String VideoUrl) {
        this.VideoUrl = VideoUrl;
    }

    public String getTextNote() {
        return TextNote;
    }

    public void setTextNote(String TextNote) {
        this.TextNote = TextNote;
    }

    @Override
	public void writeToParcel(Parcel pc, int flags) {
		// TODO Auto-generated method stub   	    	
		pc.writeInt(this.PromotionID);
		pc.writeString(this.PromotionName);
		pc.writeInt(this.RealPrepaid);
		pc.writeString(this.ImageUrl);
		pc.writeString(this.VideoUrl);
		pc.writeString(this.TextNote);
	}
	
	public PromotionModel(Parcel source) {
		PromotionID = source.readInt();
		PromotionName = source.readString();
		RealPrepaid = source.readInt();
		ImageUrl = source.readString();
		VideoUrl = source.readString();
		TextNote = source.readString();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public static final Creator<PromotionModel> CREATOR = new Creator<PromotionModel>() {
		@Override     
		public PromotionModel createFromParcel(Parcel source) {
	             return new PromotionModel(source);
		}
		@Override
		public PromotionModel[] newArray(int size) {
             return new PromotionModel[size];
        }
	};

}
