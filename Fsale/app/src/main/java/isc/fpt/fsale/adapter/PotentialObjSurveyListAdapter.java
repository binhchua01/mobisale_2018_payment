package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.DatePickerDialog;
import isc.fpt.fsale.ui.fragment.FragmentPotentialObjSurveyList;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjSurveyModel;
import isc.fpt.fsale.model.PotentialObjSurveyValueModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class PotentialObjSurveyListAdapter extends BaseAdapter {
    private static final int TAG_SURVEY_TYPE_SPINNER = 1;
    private static final int TAG_SURVEY_TYPE_RADIO = 2;
    private static final int TAG_SURVEY_TYPE_CHECK_BOX = 3;
    public static final int TAG_SURVEY_TYPE_EDIT_TEXT = 4;
    private static final int TAG_SURVEY_TYPE_DATE_TIME = 5;

    private List<PotentialObjSurveyModel> mList;
    private Context mContext;
    private FragmentPotentialObjSurveyList frmSurvey;

    public PotentialObjSurveyListAdapter(Context mContext, FragmentPotentialObjSurveyList frmSurvey, List<PotentialObjSurveyModel> mList) {
        this.frmSurvey = frmSurvey;
        this.mContext = mContext;
        this.mList = mList;
    }

    public void setListData(List<PotentialObjSurveyModel> mList) {
        this.mList.clear();
        this.mList.addAll(mList);
        this.notifyDataSetChanged();
    }

    public void addAll(List<PotentialObjSurveyModel> mList) {
        this.mList.addAll(mList);
        this.notifyDataSetChanged();
    }

    public List<PotentialObjSurveyModel> getList() {
        return this.mList;
    }

    @Override
    public int getCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public PotentialObjSurveyModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        PotentialObjSurveyModel item = null;
        if (mList != null)
            item = mList.get(position);
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_survey_template, null);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.lbl_question);
            holder.frmAnswer = (LinearLayout) convertView.findViewById(R.id.frm_answer);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (item != null) {
            holder.textView.setText(item.getRowNumber() + ". " + item.getSurveyDesc());
            if (holder.frmAnswer.getChildCount() > 0) {
                holder.frmAnswer.removeAllViews();
            }
            try {
                switch (item.getType()) {
                    case TAG_SURVEY_TYPE_CHECK_BOX:
                        addCheckBoxAnswer(mContext, holder.frmAnswer, item.getValues());
                        break;
                    case TAG_SURVEY_TYPE_RADIO:
                        addRadioButtonAnswer(mContext, holder.frmAnswer, item.getValues(), position);
                        break;
                    case TAG_SURVEY_TYPE_SPINNER:
                        addSpinnerAnswer(mContext, holder.frmAnswer, item.getValues());
                        break;
                    case TAG_SURVEY_TYPE_EDIT_TEXT:
                        addEditTextAnswer(mContext, holder.frmAnswer, item);
                        break;
                    case TAG_SURVEY_TYPE_DATE_TIME:
                        addDateTimeTextView(mContext, holder.frmAnswer, item);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return convertView;
    }

    private void addCheckBoxAnswer(Context mContext, LinearLayout frmAnswer, final List<PotentialObjSurveyValueModel> values) {
        if (values != null && values.size() > 0) {
            for (final PotentialObjSurveyValueModel item : values) {
                View viewChild = LayoutInflater.from(mContext).inflate(R.layout.row_survey_sub_item_check_box, null);
                LinearLayout frm = (LinearLayout) viewChild.findViewById(R.id.frm_group);
                CheckBox chb = (CheckBox) viewChild.findViewById(R.id.chb_answer);
                if (item.getSelected() > 0) {
                    chb.setChecked(true);
                }
                chb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            for (PotentialObjSurveyValueModel subItem : values) {
                                subItem.setSelected(0);
                            }
                            item.setSelected(1);
                            frmSurvey.updateCount();
                        }
                    }
                });
                chb.setText(item.getSurveyValue());
                frmAnswer.addView(frm);
            }
        }
    }

    private void addRadioButtonAnswer(Context mContext, LinearLayout frmAnswer, List<PotentialObjSurveyValueModel> values, int Position) {
        if (values != null && values.size() > 0) {
            View viewChild = LayoutInflater.from(mContext).inflate(R.layout.row_survey_sub_item_radio, null);
            LinearLayout frm = (LinearLayout) viewChild.findViewById(R.id.frm_group);
            RadioGroup radGroup = (RadioGroup) viewChild.findViewById(R.id.rad_group);
            for (final PotentialObjSurveyValueModel item : values) {
                final RadioButton rad = new RadioButton(mContext);
                radGroup.addView(rad);
                rad.setText(item.getSurveyValue());
                rad.setTag(Position);
                if (item.getSelected() > 0) {
                    rad.setChecked(true);
                }

                rad.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rad.setChecked(true);
                        int selectedPosition = (Integer) view.getTag();
                        unSelectAll(getItem(selectedPosition).getValues());
                        item.setSelected(1);
                        frmSurvey.updateCount();
                    }
                });
            }
            frmAnswer.addView(frm);
        }
    }

    private void unSelectAll(List<PotentialObjSurveyValueModel> values) {
        for (PotentialObjSurveyValueModel item : values)
            item.setSelected(0);
    }

    private void addSpinnerAnswer(Context mContext, LinearLayout frmAnswer, List<PotentialObjSurveyValueModel> values) {
        if (values != null && values.size() > 0) {
            View viewChild = LayoutInflater.from(mContext).inflate(R.layout.row_survey_sub_item_spinner, null);
            LinearLayout frm = (LinearLayout) viewChild.findViewById(R.id.frm_group);
            Spinner spinner = (Spinner) viewChild.findViewById(R.id.sp_answer);
            ArrayList<KeyValuePairModel> lst = new ArrayList<>();
            for (PotentialObjSurveyValueModel item : values) {
                lst.add(new KeyValuePairModel(item.getSurveyValueID(), item.getSurveyValue()));
            }
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lst, Gravity.LEFT);
            spinner.setAdapter(adapter);
            frmAnswer.addView(frm);
        }
    }

    private void addEditTextAnswer(Context mContext, LinearLayout frmAnswer, final PotentialObjSurveyModel item) {
        if (item != null) {
            View viewChild = LayoutInflater.from(mContext).inflate(R.layout.row_survey_sub_item_edit_text, null);
            LinearLayout frm = (LinearLayout) viewChild.findViewById(R.id.frm_group);
            final EditText txtValue = (EditText) viewChild.findViewById(R.id.txt_answer);
            if (item.getValues() == null) {
                item.setValues(new ArrayList<PotentialObjSurveyValueModel>());
            }
            if (item.getValues().size() == 0) {
                item.getValues().add(new PotentialObjSurveyValueModel());
            }
            if (item.getValues().size() > 0) {
                txtValue.setText(item.getValues().get(0).getSurveyValue());
            }
            txtValue.setInputType(InputType.TYPE_CLASS_TEXT);
            txtValue.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().equals("")) {
                        item.getValues().get(0).setSelected(1);
                        item.getValues().get(0).setSurveyValue(s.toString());
                        frmSurvey.updateCount();
                    }
                }
            });

            frmAnswer.addView(frm);
        }
    }

    private void addDateTimeTextView(Context mContext, LinearLayout frmAnswer, final PotentialObjSurveyModel question) {
        if (question != null) {
            final String dialogTitle = question.getSurveyDesc();
            if (question.getValues() == null) {
                question.setValues(new ArrayList<PotentialObjSurveyValueModel>());
            }
            if (question.getValues().size() == 0) {
                question.getValues().add(new PotentialObjSurveyValueModel());
            }

            View viewChild = LayoutInflater.from(mContext).inflate(R.layout.row_survey_sub_item_text_view, null);
            LinearLayout frm = (LinearLayout) viewChild.findViewById(R.id.frm_group);
            final TextView lbl = (TextView) viewChild.findViewById(R.id.lbl_answer);
            lbl.setText((question.getValues().get(0).getSurveyValue()));
            lbl.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (PotentialObjSurveyListAdapter.this.frmSurvey != null) {
                        Calendar minDate = Calendar.getInstance(), maxDate = Calendar.getInstance();
                        minDate.add(Calendar.YEAR, -20);
                        DialogFragment newFragment = new DatePickerDialog(lbl, minDate, maxDate, dialogTitle);
                        newFragment.setCancelable(false);
                        newFragment.show(PotentialObjSurveyListAdapter.this.frmSurvey.getFragmentManager(), "datePicker");
                    }
                }
            });

            lbl.setText(question.getValues().get(0).getSurveyValue());
            lbl.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().equals("")) {
                        question.getValues().get(0).setSelected(1);
                        question.getValues().get(0).setSurveyValue(s.toString());
                        frmSurvey.updateCount();
                    }
                }
            });

            frmAnswer.addView(frm);
        }
    }

    public static class ViewHolder {
        TextView textView;
        LinearLayout frmAnswer;
    }
}
