package isc.fpt.fsale.action;

import android.annotation.SuppressLint;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.choose_service.ChooseServiceActivity;
import isc.fpt.fsale.ui.choose_service.ServiceType;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 15,July,2019
 */

public class GetListCategoryService implements AsyncTaskCompleteListener<String> {
    private ChooseServiceActivity activity;

    public GetListCategoryService(ChooseServiceActivity activity, String userName) {
        this.activity = activity;
        String[] arrParamName = new String[]{"UserName"};
        String[] arrParamValue = new String[]{userName};
        String message = activity.getResources().getString(R.string.msg_progress_get_cat_service);
        String GET_LIST_DEVICE_CONFIRM = "GetListCategoryService";
        CallServiceTask service = new CallServiceTask(activity, GET_LIST_DEVICE_CONFIRM, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetListCategoryService.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<ServiceType> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ServiceType.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), activity);
                } else {
                    if (activity != null) {
                        activity.loadListCatService(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
