package isc.fpt.fsale.model.camera319;

import android.os.Parcel;
import android.os.Parcelable;

public class CameraService implements Parcelable {

    private String UserName;
    private objectCameraOfNet cameraOfNet;

    public CameraService(String userName, objectCameraOfNet cameraOfNet) {
        UserName = userName;
        this.cameraOfNet = cameraOfNet;
    }

    protected CameraService(Parcel in) {
        UserName = in.readString();
        cameraOfNet = in.readParcelable(objectCameraOfNet.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(UserName);
        dest.writeParcelable(cameraOfNet, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CameraService> CREATOR = new Creator<CameraService>() {
        @Override
        public CameraService createFromParcel(Parcel in) {
            return new CameraService(in);
        }

        @Override
        public CameraService[] newArray(int size) {
            return new CameraService[size];
        }
    };

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public objectCameraOfNet getCameraOfNet() {
        return cameraOfNet;
    }

    public void setCameraOfNet(objectCameraOfNet cameraOfNet) {
        this.cameraOfNet = cameraOfNet;
    }
}
