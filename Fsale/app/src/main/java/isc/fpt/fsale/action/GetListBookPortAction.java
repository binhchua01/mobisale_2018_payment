package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.map.activity.BookPortManualActivity;
import isc.fpt.fsale.map.activity.MapSearchTDActivity;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Toast;

// API lấy danh sách tập điểm Bookport bằng tay
public class GetListBookPortAction implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private List<RowBookPortModel> ListPopLocation;

    public GetListBookPortAction(Context mContext, String ID, int bookPortType, String LatLng) {
        this.mContext = mContext;
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        String[] params = new String[]{"UserName", "Latlng", "Type"};
        String[] arrParams = new String[]{UserName, LatLng, String.valueOf(bookPortType)};
        String message = "Xin vui long cho giay lat...";
        String GET_POP_LOCATION = "GetTapDiemLocation";
        CallServiceTask service = new CallServiceTask(this.mContext, GET_POP_LOCATION, params, arrParams,
                Services.JSON_POST, message, GetListBookPortAction.this);
        service.execute();
    }

    public void handleGetDistrictsResult(String json) {
        if (Common.jsonObjectValidate(json)) {
            ListPopLocation = new ArrayList<>();
            JSONObject jsObj = JSONParsing.getJsonObj(json);

            if (jsObj == null) {
                showListBookporActivity();
                return;
            }
            bindData(jsObj);
        } else
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {
            String TAG_GET_POP_LOCATION_RESULT = "GetTapdiemLocationMethodPostResult";
            jsArr = jsObj.getJSONArray(TAG_GET_POP_LOCATION_RESULT);
            if (jsArr.length() > 0) {
                String error = jsArr.getJSONObject(0).getString("ErrorService");
                if (error.equals("null")) {
                    int l = jsArr.length();
                    if (l > 0) {
                        for (int i = 0; i < l; i++) {
                            JSONObject iObj = jsArr.getJSONObject(i);
                            String Chuoi = iObj.getString("Latlng");
                            Chuoi = Chuoi.substring(1, Chuoi.length() - 1);
                            String Address = null, distance = null, technology = null;
                            double PortFreeRatio = 0;
                            String TAG_ADDRESS = "Address";
                            if (iObj.has(TAG_ADDRESS))
                                Address = iObj.getString(TAG_ADDRESS);
                            String TAG_DISTANCE = "Distance";
                            if (iObj.has(TAG_DISTANCE))
                                distance = iObj.getString(TAG_DISTANCE);
                            String TAG_PORT_FREE_RATIO = "PortFreeRatio";
                            if (iObj.has(TAG_PORT_FREE_RATIO))
                                PortFreeRatio = iObj.getDouble(TAG_PORT_FREE_RATIO);
                            String TAG_TECHNOLOGY = "Technology";
                            if (iObj.has(TAG_TECHNOLOGY))
                                technology = iObj.getString(TAG_TECHNOLOGY);

                            RowBookPortModel team = new RowBookPortModel(
                                    i + 1,
                                    0,
                                    0,
                                    iObj.getInt("Cabtype"),
                                    Chuoi,
                                    iObj.getInt("PortFree"),
                                    iObj.getInt("PortTotal"),
                                    iObj.getInt("PortUsed"),
                                    iObj.getString("TDName"),
                                    iObj.getString("UserName"),
                                    iObj.getInt("TDType"),
                                    null,
                                    null,
                                    null,
                                    0,
                                    Address,
                                    distance
                            );
                            team.setPortFreeRatio(PortFreeRatio);
                            team.setTechnology(technology);
                            ListPopLocation.add(team);
                        }
                    }
                    showListBookporActivity();
                } else Common.alertDialog("Lỗi WS: " + error, mContext);
            } else {
                showListBookporActivity();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void showListBookporActivity() {
        if (ListPopLocation == null || ListPopLocation.size() == 0)
            Toast.makeText(mContext, "Không có tập điểm!", Toast.LENGTH_SHORT).show();
        else {
            if (mContext.getClass().getSimpleName().equals(BookPortManualActivity.class.getSimpleName())) {
                BookPortManualActivity activity = (BookPortManualActivity) mContext;
                activity.loadTDList(ListPopLocation);
            } else if (mContext.getClass().getSimpleName().equals(MapSearchTDActivity.class.getSimpleName())) {
                MapSearchTDActivity activity = (MapSearchTDActivity) mContext;
                activity.loadTDList(ListPopLocation);
            }
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleGetDistrictsResult(result);
    }
}