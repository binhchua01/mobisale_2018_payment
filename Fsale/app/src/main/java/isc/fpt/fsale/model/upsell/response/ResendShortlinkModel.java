package isc.fpt.fsale.model.upsell.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResendShortlinkModel implements Parcelable {

    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("ResultID")
    @Expose
    private Integer resultID;


    public ResendShortlinkModel(String result, Integer resultID) {
        this.result = result;
        this.resultID = resultID;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getResultID() {
        return resultID;
    }

    public void setResultID(Integer resultID) {
        this.resultID = resultID;
    }

    protected ResendShortlinkModel(Parcel in) {
        result = in.readString();
        if (in.readByte() == 0) {
            resultID = null;
        } else {
            resultID = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(result);
        if (resultID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(resultID);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResendShortlinkModel> CREATOR = new Creator<ResendShortlinkModel>() {
        @Override
        public ResendShortlinkModel createFromParcel(Parcel in) {
            return new ResendShortlinkModel(in);
        }

        @Override
        public ResendShortlinkModel[] newArray(int size) {
            return new ResendShortlinkModel[size];
        }
    };

}
