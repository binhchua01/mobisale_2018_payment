package isc.fpt.fsale.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import isc.fpt.fsale.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * CLASS:			DateTimeHelper 
 * @description: 	common Date time functions
 * @author: 		vandn 
 * @created on:		12/08/2013
 * */
public class DateTimeHelper {
	/**
	 * get current date with string format dd-mm-yyyy
	 * 
	 * */
	public static String getCurrentDate(){
		String currentDate="";
		Calendar c= Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		currentDate =  df.format(c.getTime());
		return currentDate;
	}
	/**
	 * get current day with int datatype 
	 */  
	public static int getCurrentDay(String sCurrentDate){
		return Integer.parseInt((String)sCurrentDate.subSequence(0, 2));
	}
	/**
	 * get current month with int datatype 
	 */  
	public static int getCurrentMonth(String sCurrentDate){
		return Integer.parseInt((String) sCurrentDate.subSequence(3, 5));
	}
	/**
	 * get current year with int datatype
	 */  
	public static int getCurrentYear(String sCurrentDate){
		return Integer.parseInt((String) sCurrentDate.subSequence(6, 10));
	}	
	/**
	 * Set ddl value for Spinner Month (added by vandn, on 16/08/2013)
	 */  
	public static ArrayAdapter<Integer> getDdlMonthAdapter(final Context mContext){
		//Spinner spMonth;
		Integer[] lstMonth = {1,2,3,4,5,6,7,8,9,10,11,12};
		ArrayAdapter<Integer> adapterMonth = new ArrayAdapter<Integer>(mContext,R.layout.my_spinner_style, lstMonth)
		{
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
             // set OpenSans font for toast
				Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");
				((TextView) v).setTypeface(tf);                       
                return v;
			}
	    };		
		adapterMonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		return adapterMonth;
	}
	/**
	 * Set ddl value for Spinner Year(added by vandn, on 16/08/2013)
	 */  
	public static ArrayAdapter<Integer> getDdlYearAdapter(final Context mContext){		
		int iYear = getCurrentYear(getCurrentDate());
		Integer[] lstYear = {iYear, (iYear - 1)};
		ArrayAdapter<Integer> adapterYear = new ArrayAdapter<Integer>(mContext, R.layout.my_spinner_style, lstYear){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");
				((TextView) v).setTypeface(tf);              
                return v;
			}
		};
		adapterYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);	
		return adapterYear;
	}

	//format date to dd/MM/yyyy
	public static String formatDateOfBirthRegister(String dateOriginal){
		@SuppressLint("SimpleDateFormat")
		SimpleDateFormat patternOriginal = new SimpleDateFormat("dd-MM-yyyy");
		@SuppressLint("SimpleDateFormat")
		SimpleDateFormat patternNew = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = patternOriginal.parse(dateOriginal);
			return patternNew.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateOriginal;
	}
}
