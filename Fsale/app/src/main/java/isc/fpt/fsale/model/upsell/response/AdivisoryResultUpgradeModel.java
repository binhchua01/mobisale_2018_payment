package isc.fpt.fsale.model.upsell.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdivisoryResultUpgradeModel implements Parcelable {
    @SerializedName("Enable")
    @Expose
    private Integer enable;
    @SerializedName("ErrorService")
    @Expose
    private Object errorService;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ParentID")
    @Expose
    private Integer parentID;
    @SerializedName("ShortName")
    @Expose
    private String shortName;

    public AdivisoryResultUpgradeModel(Integer enable, Object errorService, Integer iD, String name, Integer parentID, String shortName) {
        this.enable = enable;
        this.errorService = errorService;
        this.iD = iD;
        this.name = name;
        this.parentID = parentID;
        this.shortName = shortName;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Object getErrorService() {
        return errorService;
    }

    public void setErrorService(Object errorService) {
        this.errorService = errorService;
    }

    public Integer getiD() {
        return iD;
    }

    public void setiD(Integer iD) {
        this.iD = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    protected AdivisoryResultUpgradeModel(Parcel in) {
        if (in.readByte() == 0) {
            enable = null;
        } else {
            enable = in.readInt();
        }
        if (in.readByte() == 0) {
            iD = null;
        } else {
            iD = in.readInt();
        }
        name = in.readString();
        if (in.readByte() == 0) {
            parentID = null;
        } else {
            parentID = in.readInt();
        }
        shortName = in.readString();
    }

    public static final Creator<AdivisoryResultUpgradeModel> CREATOR = new Creator<AdivisoryResultUpgradeModel>() {
        @Override
        public AdivisoryResultUpgradeModel createFromParcel(Parcel in) {
            return new AdivisoryResultUpgradeModel(in);
        }

        @Override
        public AdivisoryResultUpgradeModel[] newArray(int size) {
            return new AdivisoryResultUpgradeModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (enable == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(enable);
        }
        if (iD == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(iD);
        }
        dest.writeString(name);
        if (parentID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(parentID);
        }
        dest.writeString(shortName);
    }
}
