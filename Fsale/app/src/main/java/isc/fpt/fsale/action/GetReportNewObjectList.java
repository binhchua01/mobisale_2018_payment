package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportNewObjectActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportNewObjectModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

public class GetReportNewObjectList implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	private final String TAG_METHOD_NAME = "GetSubscriberGrowthObjectList";
	private final String TAG_ERROR = "Error";
	private final String TAG_ERROR_CODE = "ErrorCode", TAG_LIST_OBJECT = "ListObject";
	
	
	private ListReportNewObjectActivity acitvity = null;
	//For test
	
	public GetReportNewObjectList(Context context, String UserName, int Day, int Year, int Month, int Agent, String AgentName, int LocalType, int PageNumber) {
	
		mContext = context;
		try {
			acitvity = (ListReportNewObjectActivity)mContext;
		} catch (Exception e) {

			acitvity = null;
		}
		String[] paramNames = new String[]{"UserName", "Day", "Month", "Year", "Agent", "AgentName", "LocalType", "PageNumber"};
		String[] paramValues = new String[]{UserName, String.valueOf(Day),  String.valueOf(Month), String.valueOf(Year), String.valueOf(Agent), AgentName, 
				String.valueOf(LocalType), String.valueOf(PageNumber)};			
		String message = "Đang lấy danh sách hợp đồng...";
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportNewObjectList.this);
		service.execute();
			
	}
	
	public GetReportNewObjectList(Context context, String UserName, String FromDate, String ToDate, int LocalType, int PageNumber) {
		
		mContext = context;
		try {
			acitvity = (ListReportNewObjectActivity)mContext;
		} catch (Exception e) {

			acitvity = null;
		}
		String[] paramNames = new String[]{"UserName", "FromDate", "ToDate", "LocalType", "PageNumber"};
		String[] paramValues = new String[]{UserName, FromDate, ToDate, String.valueOf(LocalType), String.valueOf(PageNumber)};			
		String message = "Đang lấy danh sách hợp đồng...";
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportNewObjectList.this);
		service.execute();
			
	}
		
	public void handleUpdateRegistration(String result){
		if(result != null && Common.jsonObjectValidate(result)){
		JSONObject jsObj = getJsonObject(result);
		if(jsObj != null && jsObj.has(Constants.RESPONSE_RESULT)){							
			try{	
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);	//Get sub object json
				if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
					JSONArray jsArr = jsObj.getJSONArray(TAG_LIST_OBJECT);	
					ArrayList<ReportNewObjectModel> lstNewObject = new ArrayList<ReportNewObjectModel>();
					for(int i=0; i< jsArr.length(); i++){
						lstNewObject.add(ReportNewObjectModel.Parse(jsArr.getJSONObject(i)));
					}				
					if(acitvity != null)
						acitvity.LoadData(lstNewObject);
				}else{
					String message = "Lỗi ServiceType.";
					if(jsObj.has(TAG_ERROR))
						message = jsObj.getString(TAG_ERROR);
					Common.alertDialog(message, mContext);	
				}
			}catch (Exception e) {

				e.printStackTrace();					
				Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + "-" + TAG_METHOD_NAME, mContext);
				//setSpinnerEmpty();
			}	
			
	}
		}
	}
	
	private JSONObject getJsonObject(String result){
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);		
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception

		}
		return null;
	}
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);			
	}
	

		
	

}
