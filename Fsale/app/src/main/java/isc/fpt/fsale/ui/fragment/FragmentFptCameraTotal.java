package isc.fpt.fsale.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.danh32.fontify.Button;
import com.danh32.fontify.TextView;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.UpdateRegistrationContract;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.SellMoreCameraModel;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.utils.Common;

/**
 * Created by Hau Le on 2018-12-27.
 */
public class FragmentFptCameraTotal extends BaseFragment {
    public Context mContext;
    public TextView tvCameraTotal, tvDeviceTotal, tvTotalPrice;
    private SellMoreCameraModel mRegisterCamera;
    private Button btnUpdate;
    public RegistrationDetailModel mRegister;


    public static FragmentFptCameraTotal newInstance() {
        FragmentFptCameraTotal fragment = new FragmentFptCameraTotal();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        RegisterContractActivity activity = (RegisterContractActivity) context;
        mRegisterCamera = activity.getRegisterCamera();
    }

    @Override
    protected void initEvent() {
        btnUpdate.setOnClickListener(view -> {
            //call api update registration
            if (mRegisterCamera.toJsonObjectV2() != null) {
                new UpdateRegistrationContract(getActivity(), mRegisterCamera.toJsonObjectV2());
            }
        });
    }

    @Override
    protected void bindData() {

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_fpt_camera_total;
    }

    @Override
    protected void initView(View view) {
        tvCameraTotal = (TextView) view.findViewById(R.id.tv_camera_total_sell_more);
        tvDeviceTotal = view.findViewById(R.id.tv_total_device);
        tvTotalPrice = view.findViewById(R.id.tv_total_price);
        tvCameraTotal.setText(String.format(getString(R.string.txt_unit_price), Common.formatNumber(0)));
        tvDeviceTotal.setText(String.format(getString(R.string.txt_unit_price), Common.formatNumber(0)));
        tvTotalPrice.setText(String.format(getString(R.string.txt_unit_price), Common.formatNumber(0)));
        btnUpdate = (Button) view.findViewById(R.id.btn_update);
    }
}
