package isc.fpt.fsale.ui.fragment;

import java.util.ArrayList;

import org.json.JSONObject;

import isc.fpt.fsale.action.GetImage;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ImageObjectModel;
import isc.fpt.fsale.model.PromotionBrochureModel;
import isc.fpt.fsale.model.TouchImageView;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FragmentPromotionAdImageDetail extends Fragment {

    private static final int ANIM_DURATION = 600;
    //public static final String ARG_BUNDLE = "ARG_BUNDLE";
    public static final String ARG_TOP = "TAG_TOP";
    public static final String ARG_LEFT = "TAG_LEFT";
    public static final String ARG_WIDTH= "TAG_WIDTH";
    public static final String ARG_HEIGHT = "TAG_HEIGHT";
    public static final String ARG_PROMOTION_OBJECT = "TAG_PROMOTION_OBJECT";

    private TextView txtPromDesc;
    private TouchImageView imageView;
    private ProgressBar procBar;

    private int mLeftDelta;
    private int mTopDelta;
    private float mWidthScale;
    private float mHeightScale;

    private FrameLayout frameLayout;
    private ColorDrawable colorDrawable;

    /*private int thumbnailTop;
    private int thumbnailLeft;
    private int thumbnailWidth;
    private int thumbnailHeight;*/

    //private String title = "", image ="";
    private PromotionBrochureModel mPromotionObj;
    private Context mContext;

    /**
     * Returns a new instance of this fragment for the given section number.
     */
    public static FragmentPromotionAdImageDetail newInstance(Bundle args) {
        FragmentPromotionAdImageDetail fragment = new FragmentPromotionAdImageDetail();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentPromotionAdImageDetail() {

    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();//.getBundle(ARG_BUNDLE);
        if(bundle != null){
	        /*thumbnailTop = bundle.getInt(ARG_TOP);
	        thumbnailLeft = bundle.getInt(ARG_LEFT);
	        thumbnailWidth = bundle.getInt(ARG_WIDTH);
	        thumbnailHeight = bundle.getInt(ARG_HEIGHT);*/
            mPromotionObj = bundle.getParcelable(ARG_PROMOTION_OBJECT);

        }
    }

    //TouchImageView img ;
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity();
        View rootView = inflater.inflate(R.layout.fragment_promotion_image_detail, container, false);
        txtPromDesc = (TextView) rootView.findViewById(R.id.title);
        imageView = (TouchImageView) rootView.findViewById(R.id.grid_item_image);
        frameLayout = (FrameLayout) rootView.findViewById(R.id.main_background);
        colorDrawable = new ColorDrawable(Color.BLACK);
        procBar = (ProgressBar)rootView.findViewById(R.id.progress_bar);

        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            frameLayout.setBackgroundDrawable(colorDrawable);
        } else {
            frameLayout.setBackground(colorDrawable);
        }

        if(mPromotionObj != null)
            txtPromDesc.setText(mPromotionObj.getTitle());

		/*if(mPromotionObj != null){
 	        title = mPromotionObj.getPromotionName();
 	        image = mPromotionObj.getImageUrl();
 	        titleTextView.setText(Html.fromHtml(title));
 	        Picasso.with(getActivity()).load(image).into(imageView);
        }*/
        // Only run the animation if we're coming from the parent activity, not if
        // we're recreated automatically by the window manager (e.g., device rotation)
	        /*if (savedInstanceState == null) {
	            ViewTreeObserver observer = imageView.getViewTreeObserver();
	            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

	                @Override
	                public boolean onPreDraw() {
	                    imageView.getViewTreeObserver().removeOnPreDrawListener(this);

	                    // Figure out where the thumbnail and full size versions are, relative
	                    // to the screen and each other
	                    int[] screenLocation = new int[2];
	                    imageView.getLocationOnScreen(screenLocation);
	                    mLeftDelta = thumbnailLeft - screenLocation[0];
	                    mTopDelta = thumbnailTop - screenLocation[1];

	                    // Scale factors to make the large version the same size as the thumbnail
	                    mWidthScale = (float) thumbnailWidth / imageView.getWidth();
	                    mHeightScale = (float) thumbnailHeight / imageView.getHeight();
	                    enterAnimation();
	                    return true;
	                }
	            });
	        }*/
		/*img = new TouchImageView(getActivity());
        //img.setImageResource(R.drawable.ice_age_2);
        img.setMaxZoom(4f);  */

        getData();
        return rootView;
    }

    private void getData(){
		/*if(mPromotionObj != null){
			new GetImage(getActivity(), this, mPromotionObj.getImageUrl());
		}*/
        //new DownloadImage().execute(mPromotionObj.getImageUrl());
        if(imageView.getDrawable() == null && mPromotionObj!= null){
            //new DownloadImage().execute(mPromotionObj.getImageUrl());
            new DownloadImage().execute(mPromotionObj.getMediaUrl());
        }
    }


    public void enterAnimation() {

        // Set starting values for properties we're going to animate. These
        // values scale and position the full size version down to the thumbnail
        // size/location, from which we'll animate it back up
        imageView.setPivotX(0);
        imageView.setPivotY(0);
        imageView.setScaleX(mWidthScale);
        imageView.setScaleY(mHeightScale);
        imageView.setTranslationX(mLeftDelta);
        imageView.setTranslationY(mTopDelta);

        // interpolator where the rate of change starts out quickly and then decelerates.
        TimeInterpolator sDecelerator = new DecelerateInterpolator();

        // Animate scale and translation to go from thumbnail to full size
        imageView.animate().setDuration(ANIM_DURATION).scaleX(1).scaleY(1).
                translationX(0).translationY(0).setInterpolator(sDecelerator);

        // Fade in the black background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(colorDrawable, "alpha", 0, 255);
        bgAnim.setDuration(ANIM_DURATION);
        bgAnim.start();

    }

    /**
     * The exit animation is basically a reverse of the enter animation.
     * This Animate image back to thumbnail size/location as relieved from bundle.
     *
     * @param endAction This action gets run after the animation completes (this is
     *                  when we actually switch activities)
     */
    /*@SuppressLint("NewApi")
	public void exitAnimation(final Runnable endAction) {

    	final int sdk = android.os.Build.VERSION.SDK_INT;

 		if(sdk >= 16) {
 			TimeInterpolator sInterpolator = new AccelerateInterpolator();
 	        imageView.animate().setDuration(ANIM_DURATION).scaleX(mWidthScale).scaleY(mHeightScale).
 	                translationX(mLeftDelta).translationY(mTopDelta)
 	                .setInterpolator(sInterpolator).withEndAction(endAction);
 		} else {

 		}


        // Fade out background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(colorDrawable, "alpha", 0);
        bgAnim.setDuration(ANIM_DURATION);
        bgAnim.start();
    }

    @Override
    public void onBackPressed() {
        exitAnimation(new Runnable() {
            public void run() {
                finish();
            }
        });
    }*/


    private class DownloadImage extends AsyncTask<String, Integer, Bitmap> {
        protected Bitmap doInBackground(String... urls) {
            String result = null;
            String[] paramsValue = new String[]{urls[0]};
            ArrayList<ImageObjectModel> lst = null;
            try {
                result = Services.postJsonUpload(GetImage.GET_IMAGE_METHOD, GetImage.paramNames, paramsValue, mContext);
                boolean isError = false;
                JSONObject jsObj = new JSONObject(result);
                if(jsObj != null){
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<ImageObjectModel> resultObject = new WSObjectsModel<ImageObjectModel>(jsObj, ImageObjectModel.class);
                    if(resultObject != null){
                        if(resultObject.getErrorCode() == 0){//OK not Error
                            lst = resultObject.getArrayListObject();
                        }else{//ServiceType Error
                            isError = true;
                            Common.alertDialog( resultObject.getError(), getActivity());
                        }
                    }
                }
                if(!isError){
                    if(lst != null && lst.size() > 0){
                        return Common.StringToBitMap(lst.get(0).getImage());
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block

                e.printStackTrace();
            }

            return null;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Bitmap bmp) {
            procBar.setVisibility(View.GONE);
            try {
                if(bmp!= null){
                    //Picasso.with(getActivity()).load(image).into(imageView);
                    imageView.setImageBitmap(bmp);
                }
            } catch (Exception e) {
                // TODO: handle exception

                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
