package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportPotentialObjActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetPotentialObjCEMList implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    public final String TAG_METHOD_NAME = "GetPotentialObjList";
    private String[] paramNames, paramValues;

    public GetPotentialObjCEMList(Context context, String UserName, int Agent,
                                  String AgentName, int PageNumber, int Source) {
        this.mContext = context;
        this.paramNames = new String[]{"UserName", "Agent", "AgentName", "PageNumber", "Source"};
        this.paramValues = new String[]{UserName, String.valueOf(Agent), AgentName,
                String.valueOf(PageNumber), String.valueOf(Source)};

    }

    public void execute() {
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, GetPotentialObjCEMList.this);
        service.execute();
    }

    public String[] getParamName() {
        return this.paramNames;
    }

    public String[] getParamValue() {
        return this.paramValues;
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<PotentialObjModel> lst = null;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<PotentialObjModel> resultObject = new WSObjectsModel<PotentialObjModel>(jsObj, PotentialObjModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getArrayListObject();
                } else {//ServiceType Error
                    Common.alertDialog(resultObject.getError(), mContext);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }


    public WSObjectsModel<PotentialObjModel> getData(String json) {
        WSObjectsModel<PotentialObjModel> resultObject = null;
        JSONObject jsObj;
        try {
            jsObj = new JSONObject(json);
            jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
            resultObject = new WSObjectsModel<>(jsObj, PotentialObjModel.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resultObject;
    }

    private void loadData(ArrayList<PotentialObjModel> lst) {
        try {
            if (mContext.getClass().getSimpleName().equals(ListReportPotentialObjActivity.class.getSimpleName())) {
                ListReportPotentialObjActivity activity = (ListReportPotentialObjActivity) mContext;
                activity.loadData(lst);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
