package isc.fpt.fsale.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV4;
import isc.fpt.fsale.utils.Common;

public class DeviceCameraAdapter extends RecyclerView.Adapter<DeviceCameraAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<Device> mList;
    private OnItemClickListenerV4 mListener;

    public DeviceCameraAdapter(Context mContext, List<Device> mList, OnItemClickListenerV4 mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_row_device, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDeviceName, tvLess, tvPlus, tvCount, tvTotal, tvPriceDes;
        private View mDevicePrice, mLayoutPromotion;
        private ImageView imgDelete;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvDeviceName = (TextView) itemView.findViewById(R.id.txt_device_name);
            mDevicePrice = itemView.findViewById(R.id.layout_choose_price);
            tvLess = (TextView) itemView.findViewById(R.id.tv_device_charge_time_less);
            tvPlus = (TextView) itemView.findViewById(R.id.tv_device_charge_time_plus);
            tvCount = (TextView) itemView.findViewById(R.id.txt_total_count_device);
            tvTotal = (TextView) itemView.findViewById(R.id.tv_total_price_device);
            mLayoutPromotion = itemView.findViewById(R.id.layout_promotion);
            tvPriceDes = (TextView) itemView.findViewById(R.id.txt_device_price);
            imgDelete = (ImageView) itemView.findViewById(R.id.img_delete);
        }

        public void bindView(final Device mDevice, final int position) {
            tvDeviceName.setText(mDevice.getDeviceName() != null ? mDevice.getDeviceName() : "");
            tvPriceDes.setText(mDevice.getPriceText() != null ? mDevice.getPriceText() : "");
            tvCount.setText(String.valueOf(mDevice.getNumber()));
            tvTotal.setText(String.format(mContext.getString(R.string.txt_unit_price), Common.formatNumber(calculatorPrice(mDevice))));
            imgDelete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClickV4(0, mDevice, position);//delete
                }
            });
            tvPriceDes.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClickV4(1, mDevice, position);//get price
                }
            });
            tvLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = mDevice.getNumber();
                    if (quantity == mDevice.getNumMin()) {
                        return;
                    }
                    quantity--;
                    mDevice.setNumber(quantity);
                    tvCount.setText(String.valueOf(mDevice.getNumber()));
                    tvTotal.setText(String.format(mContext.getString(R.string.txt_unit_price), Common.formatNumber(calculatorPrice(mDevice))));
                }
            });
            tvPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = mDevice.getNumber();
                    if (quantity == mDevice.getNumMax()) {
                        return;
                    }
                    quantity++;
                    mDevice.setNumber(quantity);
                    tvCount.setText(String.valueOf(mDevice.getNumber()));
                    tvTotal.setText(String.format(mContext.getString(R.string.txt_unit_price), Common.formatNumber(calculatorPrice(mDevice))));

                    if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
                        ((RegisterContractActivity) mContext).mRegisterCamera.setDevice(mList);
                    }
//                    if(mContext.getClass().getSimpleName().equals(FptCameraActivity.class.getSimpleName())){
//                        ((FptCameraActivity) mContext).mRegisterFptCameraModel.getObjectCamera().setCameraDetail(mList);
//                    }else if(mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())){
//                        ((RegisterContractActivity) mContext).mRegisterCamera.getObjectCamera().setCameraDetail(mList);
//                    }
                    //mUpdatePromotion.onItemClick(true);
//                    int quantity = mDevice.getNumber();
//                    if(quantity == mDevice.getNumMax()){
//                        return;
//                    }
//                    quantity++;
//                    mDevice.setNumber(quantity);
//                    tvCount.setText(String.valueOf(mDevice.getNumber()));
//                    tvTotal.setText(String.format(mContext.getString(R.string.txt_unit_price), Common.formatNumber(calculatorPrice(mDevice))));
                }
            });
            if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
                ((RegisterContractActivity) mContext).mRegisterCamera.setDevice(mList);
            }
            mDevicePrice.setVisibility(View.VISIBLE);
            tvTotal.setVisibility(View.VISIBLE);
            mLayoutPromotion.setVisibility(View.GONE);
        }

        private double calculatorPrice(Device mDevice) {
            return (mDevice.getNumber() * mDevice.getPrice());
        }
    }

    public void notifyData(List<Device> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}