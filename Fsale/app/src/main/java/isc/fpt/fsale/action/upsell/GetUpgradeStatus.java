package isc.fpt.fsale.action.upsell;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.model.upsell.response.ProgramUpgradeStatusModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetUpgradeStatus implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private OnGetUpgradeStatus mCallBack;

    public GetUpgradeStatus(Context mContext, String[] value, OnGetUpgradeStatus mCallBack) {
        this.mContext = mContext;
        this.mCallBack = mCallBack;
        //String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        String[] params = new String[]{"objID","programUpgradeID"};
        String message = "Đang lấy danh sách trạng thái ...";
        String GET_UPGRADE_STATUS = "GetUpgradeStatus";
        CallServiceTask service = new CallServiceTask(mContext, GET_UPGRADE_STATUS, params, value,
                Services.JSON_POST, message, GetUpgradeStatus.this);
        service.execute();
    }

    public interface OnGetUpgradeStatus{
        void onGetUpgradeStatusSuccess(List<ProgramUpgradeStatusModel> mList);
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<ProgramUpgradeStatusModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ProgramUpgradeStatusModel.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        mCallBack.onGetUpgradeStatusSuccess(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
