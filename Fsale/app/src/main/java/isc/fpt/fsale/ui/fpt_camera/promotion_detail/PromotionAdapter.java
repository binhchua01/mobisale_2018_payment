package isc.fpt.fsale.ui.fpt_camera.promotion_detail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.model.PromotionDetail;

/**
 * Created by haulc3 on 10,September,2019
 */
public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<PromotionDetail> mList;
    private OnItemClickListener mListener;

    PromotionAdapter(Context mContext, List<PromotionDetail> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_dialog_camera_device, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private View layout;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_item_name);
            layout = itemView.findViewById(R.id.layout_camera_package);
        }

        void bindView(final PromotionDetail mPromotionDetail) {
            tvName.setText(mPromotionDetail.getPromoName());
            if(mPromotionDetail.isSelected()){
                layout.setBackgroundResource(R.drawable.background_radius_selected);
            }
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mPromotionDetail.isSelected()) {
                        mPromotionDetail.setSelected(false);
                        layout.setBackgroundResource(R.drawable.background_radius);
                    } else {
                        mPromotionDetail.setSelected(true);
                        layout.setBackgroundResource(R.drawable.background_radius_selected);
                        mListener.onItemClick(mPromotionDetail);
                    }
                }
            });
        }
    }

    public void notifyData(List<PromotionDetail> mList){
        this.mList = mList;
        notifyDataSetChanged();
    }
}
