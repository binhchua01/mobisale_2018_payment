package isc.fpt.fsale.callback.banking;

import java.util.List;

import isc.fpt.fsale.model.Banking.InfoBankingModel;

public interface InfoBankingCallback {

    void onGetInfoSuccess(InfoBankingModel models);
}
