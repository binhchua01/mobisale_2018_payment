package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.CustomerCareDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentCustomerCareToDoList;
import isc.fpt.fsale.model.ToDoListModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

public class GetToDoList implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetToDoList";
	private String[] paramNames, paramValues;
	private Fragment fragment;
	
	public GetToDoList(Context context, String UserName, String Contract, int Agent, String AgentName, int PageNumber){	
		mContext = context;
		this.paramNames = new String[]{"UserName", "Contract", "Agent", "AgentName", "PageNumber"};
		this.paramValues = new String[]{UserName, Contract, String.valueOf(Agent), AgentName, String.valueOf(PageNumber)};			
		execute();
	}
	
	public GetToDoList(Context context, Fragment fragment, String UserName, String Contract, int Agent, String AgentName, int PageNumber){	
		mContext = context;
		this.paramNames = new String[]{"UserName", "Contract", "Agent", "AgentName", "PageNumber"};
		this.paramValues = new String[]{UserName, Contract, String.valueOf(Agent), AgentName, String.valueOf(PageNumber)};	
		this.fragment = fragment;
		execute();
	}
	
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetToDoList.this);
		service.execute();		
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			List<ToDoListModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ToDoListModel> resultObject = new WSObjectsModel<ToDoListModel>(jsObj, ToDoListModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetCustomerCareToDoList:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	
	
	private void loadData(List<ToDoListModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(CustomerCareDetailActivity.class.getSimpleName())){
				if(this.fragment != null){
					FragmentCustomerCareToDoList fmg = (FragmentCustomerCareToDoList)fragment;
					if(fmg != null){
						fmg.loadData(lst);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetCustomerCareToDoList.loadRpCodeInfo():", e.getMessage());
		}
	}
}
