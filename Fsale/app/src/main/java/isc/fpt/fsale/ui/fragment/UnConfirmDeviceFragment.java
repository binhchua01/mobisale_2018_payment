package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.ConfirmDevice;
import isc.fpt.fsale.action.GetListDeviceConfirm;
import isc.fpt.fsale.activity.ReceiveDeviceActivity;
import isc.fpt.fsale.adapter.AdapterConfirmDevice;
import isc.fpt.fsale.adapter.SpinDeviceTypeConfirmAdapter;
import isc.fpt.fsale.model.DeviceConfirm;
import isc.fpt.fsale.model.DeviceTypeConfirm;
import isc.fpt.fsale.model.ListDeviceConfirm;
import isc.fpt.fsale.utils.MyApp;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

/**
 * Created by haulc3 on 16,April,2019
 */
public class UnConfirmDeviceFragment extends Fragment
        implements AdapterConfirmDevice.CallbackDeviceConfirm {
    private RecyclerView mRecyclerView;
    private Spinner spnDeviceTypeConfirm;
    private ReceiveDeviceActivity activity;
    private SpinDeviceTypeConfirmAdapter spnAdapter;
    private DeviceTypeConfirm mDeviceTypeConfirm;
    private int typeId;
    private Button btnConfirm, btnUnConfirm, btnSelectAll;
    private JSONArray listDeviceSelected;
    private AdapterConfirmDevice mAdapter;
    private boolean isSelectAll = true;
    private List<ListDeviceConfirm> list;
    private LinearLayoutManager mLinearLayoutManager;
    private boolean isLastPage = false;
    private int pageCurrent = 1;
    private boolean isRefresh = false;
    private boolean isLoadMore = true;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ReceiveDeviceActivity) {
            this.activity = (ReceiveDeviceActivity) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_un_confirm_device, container, false);
        bindView(view);
        return view;
    }

    private void bindView(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_un_confirm_device);
        spnDeviceTypeConfirm = (Spinner) view.findViewById(R.id.spinner_type_search_un_confirm);
        btnConfirm = (Button) view.findViewById(R.id.btn_confirm);
        btnUnConfirm = (Button) view.findViewById(R.id.btn_cancel);
        btnSelectAll = (Button) view.findViewById(R.id.btn_select_all);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initEvent();
    }

    private void initEvent() {
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //chưa chọn loại thiết bị cần xác nhận
                if (typeId == -1 || listDeviceSelected == null || listDeviceSelected.length() == 0)
                    return;
                new ConfirmDevice(activity,
                        UnConfirmDeviceFragment.this, putDataToJson(1));
            }
        });

        btnUnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (typeId == -1 || listDeviceSelected == null || listDeviceSelected.length() == 0)
                    return;
                new ConfirmDevice(activity,
                        UnConfirmDeviceFragment.this, putDataToJson(0));
            }
        });

        btnSelectAll.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                if (list == null) return;
                mAdapter.selectAllItem(isSelectAll);
                if (isSelectAll) {
                    btnSelectAll.setText(getResources().getString(R.string.label_un_select_all));
                } else {
                    btnSelectAll.setText(getResources().getString(R.string.label_select_all));
                }
                isSelectAll = !isSelectAll;
            }
        });
    }

    private JSONObject putDataToJson(int status) {
        //put data to json obj and call api
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("SaleName", ((MyApp) getActivity().getApplication()).getUserName());
            jsonObject.put("TypeID", typeId);
            jsonObject.put("ListDevice", listDeviceSelected);
            jsonObject.put("Status", status); // 0 - không xác nhận, 1 - xác nhận
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initData() {
        //init data to spinner device type confirm
        spnAdapter = new SpinDeviceTypeConfirmAdapter(activity,
                R.layout.item_spinner, R.id.text_view_0, activity.list) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textview = (TextView) view;
                if (position == 0) {
                    textview.setTextColor(Color.GRAY);
                } else {
                    textview.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spnDeviceTypeConfirm.setAdapter(spnAdapter);

        spnDeviceTypeConfirm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                mDeviceTypeConfirm = spnAdapter.getItem(position);
                assert mDeviceTypeConfirm != null;
                typeId = mDeviceTypeConfirm.getTypeID();

                //chưa chọn loại thiết bị cần xác nhận
                if (typeId == -1) return;

                //call api get list device confirm by type id
                new GetListDeviceConfirm(activity, UnConfirmDeviceFragment.this,
                        ((MyApp) getActivity().getApplication()).getUserName(),
                        mDeviceTypeConfirm.getTypeID(), 0, pageCurrent);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        list = new ArrayList<>();
        mAdapter = new AdapterConfirmDevice(getContext(), (ArrayList<ListDeviceConfirm>) list,
                this, 1);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        //load more
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLinearLayoutManager.getChildCount();
                int totalItemCount = mLinearLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();

                if (!isLastPage && isLoadMore) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {
                        pageCurrent++;
                        isRefresh = false;
                        isLoadMore = false;
                        //call api get list device confirm by type id
                        new GetListDeviceConfirm(activity, UnConfirmDeviceFragment.this,
                                ((MyApp) getActivity().getApplication()).getUserName(),
                                mDeviceTypeConfirm.getTypeID(), 0, pageCurrent);
                    }
                }
            }
        });
    }

    public void listDeviceConfirm(ArrayList<ListDeviceConfirm> list) {
        if (list.size() == 0) {
            isLastPage = true;
        }
        isLoadMore = true;
        if (!isRefresh) {
            this.list.addAll(list);
        } else {
            this.list = list;
        }
        mAdapter.notifyDataChanged((ArrayList<ListDeviceConfirm>) this.list);
    }

    @Override
    public void deviceConfirm(ArrayList<DeviceConfirm> listDevice) {
        if (listDevice == null) return;
        listDeviceSelected = new JSONArray();
        //convert list obj to json arr
        if (listDevice.size() != 0) {
            for (DeviceConfirm item : listDevice) {
                listDeviceSelected.put(item.getMAC());
            }
        }
    }

    public void deviceConfirmResult() {
        isRefresh = true;
        //refresh list has selected
        mAdapter.getRefresh().refresh();
        listDeviceSelected = new JSONArray();
        //call api get list device confirm by type id
        //this api called when confirm device success to refresh list device
        new GetListDeviceConfirm(activity, UnConfirmDeviceFragment.this,
                ((MyApp) getActivity().getApplication()).getUserName(),
                mDeviceTypeConfirm.getTypeID(), 0, 1);
    }
}

