package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;
// model chứa thông tin 1 tập điểm cụ thể
public class RowBookPortModel implements Parcelable {
    private int STT;
    private int BranchID;
    private int LocationID;
    private int Cabtype;
    private String LatlngPort;//Toa do Port
    private int PortFree;//Port trong
    private int PortTotal;//Tong port
    private int PortUsed;//Port da su dung
    private String TDName;//Ten cum Port
    private String UserName;//Nguoi tao
    private int TypeService;//Loai dich vu
    private String LocationName;
    private String NameBranch;
    private String PopName;
    private int TDID;
    private String Address;
    private String Distance;
    private double PortFreeRatio;
    private String Technology;
    public RowBookPortModel() {

    }

    public RowBookPortModel(int _STT, int _BranchID, int _LocationID, int _Cabtype, String _LatlngPort, int _PortFree, int _PortTotal, int _PortUsed,
                            String _TDName, String _UserName, int _TypeService, String _LocationName, String _NameBranch, String _PopName, int _TDID) {
        this.STT = _STT;
        this.BranchID = _BranchID;
        this.LocationID = _LocationID;
        this.Cabtype = _Cabtype;
        this.LatlngPort = _LatlngPort;
        this.PortFree = _PortFree;
        this.PortTotal = _PortTotal;
        this.PortUsed = _PortUsed;
        this.TDName = _TDName;
        this.UserName = _UserName;
        this.TypeService = _TypeService;
        this.LocationName = _LocationName;
        this.NameBranch = _NameBranch;
        this.PopName = _PopName;
        this.TDID = _TDID;
    }

    //
    public RowBookPortModel(int _STT, int _BranchID, int _LocationID, int _Cabtype, String _LatlngPort, int _PortFree, int _PortTotal, int _PortUsed,
                            String _TDName, String _UserName, int _TypeService, String _LocationName, String _NameBranch, String _PopName, int _TDID,
                            String Address, String Distance) {
        this.STT = _STT;
        this.BranchID = _BranchID;
        this.LocationID = _LocationID;
        this.Cabtype = _Cabtype;
        this.LatlngPort = _LatlngPort;
        this.PortFree = _PortFree;
        this.PortTotal = _PortTotal;
        this.PortUsed = _PortUsed;
        this.TDName = _TDName;
        this.UserName = _UserName;
        this.TypeService = _TypeService;
        this.LocationName = _LocationName;
        this.NameBranch = _NameBranch;
        this.PopName = _PopName;
        this.TDID = _TDID;
        this.Address = Address;
        this.Distance = Distance;
    }

    public int getSTT() {
        return this.STT;
    }

    public void setSTT(int _STT) {
        this.STT = _STT;
    }

    //BranchID

    public int getBranchID() {
        return this.BranchID;
    }

    public void setBranchID(int _BranchID) {
        this.BranchID = _BranchID;
    }

    //LocationID

    public int getLocationID() {
        return this.LocationID;
    }

    public void setLocationID(int _LocationID) {
        this.LocationID = _LocationID;
    }


    //Cabtype
    public int getCabtype() {
        return this.Cabtype;
    }

    public void setCabtype(int _Cabtype) {
        this.Cabtype = _Cabtype;
    }

    //LatlngPort
    public String getLatlngPort() {
        return this.LatlngPort;
    }

    public void setLatlngPort(String _LatlngPort) {
        this.LatlngPort = _LatlngPort;
    }

    //PortFree
    public int getPortFree() {
        return this.PortFree;
    }

    public void setPortFree(int _PortFree) {
        this.PortFree = _PortFree;
    }


    //PortTotal
    public int getPortTotal() {
        return this.PortTotal;
    }

    public void setPortTotal(int _PortTotal) {
        this.PortTotal = _PortTotal;
    }


    //PortUsed
    public int getPortUsed() {
        return this.PortUsed;
    }

    public void setPortUsed(int _PortUsed) {
        this.PortUsed = _PortUsed;
    }

    //TDName
    public String getTDName() {
        return this.TDName;
    }

    public void setTDName(String _TDName) {
        this.TDName = _TDName;
    }

    //UserName
    public String getUserName() {
        return this.UserName;
    }

    public void setUserName(String _UserName) {
        this.UserName = _UserName;
    }

    public int GetTypeService() {
        return this.TypeService;
    }

    public void SetTypeService(int _TypeService) {
        this.TypeService = _TypeService;
    }


    public String GetLocationName() {
        return this.LocationName;
    }

    public void SetLocationName(String _LocationName) {
        this.LocationName = _LocationName;
    }

    public String GetNameBranch() {
        return this.NameBranch;
    }

    public void SetNameBranch(String _NameBranch) {
        this.NameBranch = _NameBranch;
    }

    public String GetPopName() {
        return this.PopName;
    }

    public void SetPopName(String _PopName) {
        this.PopName = _PopName;
    }


    public int getTDID() {
        return this.TDID;
    }

    public void setTDID(int _TDID) {
        this.TDID = _TDID;
    }

    // Địa chỉ tập điểm

    public String getAddress() {
        return this.Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }
    //khoản cách từ TD đến vị trí

    public String getDistance() {
        return this.Distance;
    }

    public void setDistance(String Distance) {
        this.Distance = Distance;
    }
    //Tỷ lệ port trống
    public void setPortFreeRatio(double PortFreeRatio) {
        this.PortFreeRatio = PortFreeRatio;
    }
    public double getPortFreeRatio() {
        return this.PortFreeRatio;
    }
    // technology
    public String getTechnology() {
        return Technology;
    }

    public void setTechnology(String technology) {
        Technology = technology;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel pc, int flags) {
        // TODO Auto-generated method stub
        pc.writeInt(STT);
        pc.writeInt(BranchID);
        pc.writeInt(LocationID);
        pc.writeInt(Cabtype);
        pc.writeString(LatlngPort);
        pc.writeInt(PortFree);
        pc.writeInt(PortTotal);
        pc.writeInt(PortUsed);
        pc.writeString(TDName);
        pc.writeString(UserName);
        pc.writeInt(TypeService);
        pc.writeString(LocationName);
        pc.writeString(NameBranch);
        pc.writeString(PopName);
        pc.writeInt(TDID);
        pc.writeString(Address);
        pc.writeString(Distance);
        pc.writeDouble(PortFreeRatio);
        pc.writeString(Technology);
    }

    public RowBookPortModel(Parcel source) {
        STT = source.readInt();
        BranchID = source.readInt();
        LocationID = source.readInt();
        Cabtype = source.readInt();
        LatlngPort = source.readString();
        PortFree = source.readInt();
        PortTotal = source.readInt();
        PortUsed = source.readInt();
        TDName = source.readString();
        UserName = source.readString();
        TypeService = source.readInt();
        LocationName = source.readString();
        NameBranch = source.readString();
        PopName = source.readString();
        TDID = source.readInt();
        Address = source.readString();
        Distance = source.readString();
        PortFreeRatio = source.readDouble();
        Technology = source.readString();
    }

    /**
     * Static field used to regenerate object, individually or as arrays
     */
    public static final Creator<RowBookPortModel> CREATOR = new Creator<RowBookPortModel>() {
        @Override
        public RowBookPortModel createFromParcel(Parcel source) {
            return new RowBookPortModel(source);
        }

        @Override
        public RowBookPortModel[] newArray(int size) {
            return new RowBookPortModel[size];
        }
    };
}
