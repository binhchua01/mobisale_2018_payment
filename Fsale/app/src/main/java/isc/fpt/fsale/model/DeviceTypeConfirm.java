package isc.fpt.fsale.model;

/**
 * Created by haulc3 on 18,April,2019
 */
public class DeviceTypeConfirm {
    private int TypeID;
    private String TypeName;

    public DeviceTypeConfirm() {
    }

    public DeviceTypeConfirm(int typeID, String typeName) {
        TypeID = typeID;
        TypeName = typeName;
    }

    public int getTypeID() {
        return TypeID;
    }

    public void setTypeID(int typeID) {
        TypeID = typeID;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }
}
