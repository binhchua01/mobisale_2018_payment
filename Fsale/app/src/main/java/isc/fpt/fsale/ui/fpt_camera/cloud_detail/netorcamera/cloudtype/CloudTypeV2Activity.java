package isc.fpt.fsale.ui.fpt_camera.cloud_detail.netorcamera.cloudtype;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetAllCloudTypeV2;
import isc.fpt.fsale.model.AllCloudType;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.cloud_detail.cloud_package_netorcam.CloudPackageV2Activity;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.utils.Common;

import static isc.fpt.fsale.utils.Constants.CLASS_NAME;
import static isc.fpt.fsale.utils.Constants.CLOUD_DETAIL;
import static isc.fpt.fsale.utils.Constants.CUSTYPE;
import static isc.fpt.fsale.utils.Constants.LIST_CLOUD_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.REGTYPE;

public class CloudTypeV2Activity extends BaseActivitySecond implements OnItemClickListener<CloudDetail> {
    private View loBack, loChoosePackage;
    private List<CloudDetail> mList;
    private List<CloudDetail> mListSelected;
    private CloudTypeV2Adapter mAdapter;
    private RecyclerView mRecyclerViewCloud;

    private Button btnUpdate;
    private TextView tvCloudTypeName, tvCloudServiceName, tvCloudServiceLess,
            tvCloudServicePlus, tvCloudServiceCount, tvTotal;
    private CloudDetail mCloudDetail;
    private final int CODE_CLOUD_PACKAGE = 4;
    private int cusType;
    private int regType;
    private String className;

    @Override
    protected void onResume() {
        super.onResume();
        new GetAllCloudTypeV2(this, cusType, regType);
    }

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCloudDetail.getPackID() == 0) {
                    return;
                }
                Intent returnIntent = new Intent();
                returnIntent.putExtra(CLOUD_DETAIL, mCloudDetail);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        tvCloudServiceName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(LIST_CLOUD_DETAIL_SELECTED,
                        (ArrayList<? extends Parcelable>) mListSelected);
                bundle.putParcelable(CLOUD_DETAIL, mCloudDetail);
                bundle.putInt(CUSTYPE, cusType);
                bundle.putInt(REGTYPE, regType);
                bundle.putString(CLASS_NAME, className);
                Intent intent = new Intent(CloudTypeV2Activity.this, CloudPackageV2Activity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_CLOUD_PACKAGE);
            }
        });

        tvCloudServiceLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(mCloudDetail.getPackName())) {
                    return;
                }
                int quantity = mCloudDetail.getQty();
                if (quantity == 1) {
                    return;
                }
                quantity--;
                mCloudDetail.setQty(quantity);
                tvCloudServiceCount.setText(String.valueOf(mCloudDetail.getQty()));
                tvTotal.setText(calculatorCloudServiceTotal(mCloudDetail.getCost(), mCloudDetail.getQty()));
            }
        });

        tvCloudServicePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(mCloudDetail.getPackName())) {
                    return;
                }
                int quantity = mCloudDetail.getQty();
                quantity++;
                mCloudDetail.setQty(quantity);
                tvCloudServiceCount.setText(String.valueOf(mCloudDetail.getQty()));
                tvTotal.setText(calculatorCloudServiceTotal(mCloudDetail.getCost(), mCloudDetail.getQty()));
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_cloud_type;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        loChoosePackage = findViewById(R.id.layout_choose_package);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.title_cloud_type));
        mRecyclerViewCloud = (RecyclerView) findViewById(R.id.recycler_view_cloud_type_list);
        mList = new ArrayList<>();
        mAdapter = new CloudTypeV2Adapter(this, mList, this);
        mRecyclerViewCloud.setAdapter(mAdapter);
        getDataIntent();

        btnUpdate = (Button) findViewById(R.id.button_update);
        tvCloudTypeName = (TextView) findViewById(R.id.tv_cloud_type);
        tvCloudServiceName = (TextView) findViewById(R.id.tv_cloud_service_name);
        tvCloudServiceLess = (TextView) findViewById(R.id.tv_cloud_service_less);
        tvCloudServiceCount = (TextView) findViewById(R.id.lbl_cloud_service_quantity);
        tvCloudServicePlus = (TextView) findViewById(R.id.tv_cloud_service_plus);
        tvTotal = (TextView) findViewById(R.id.tv_cloud_service_total);
    }

    public void loadCloudDetail(List<AllCloudType> mList) {
        if (mList == null || mList.size() == 0) {
            return;
        }
        this.mList.clear();
        for (AllCloudType item : mList) {
            this.mList.add(new CloudDetail(item.getTypeID(), item.getTypeName(),
                    0, "", 0, 0, 0, false));
        }

        mAdapter.notifyData(this.mList);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelableArrayList(LIST_CLOUD_DETAIL_SELECTED) != null) {
            mListSelected = bundle.getParcelableArrayList(LIST_CLOUD_DETAIL_SELECTED);
            cusType = bundle.getInt(CUSTYPE);
            regType = bundle.getInt(REGTYPE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                switch (requestCode) {
                    case CODE_CLOUD_PACKAGE:
                        mCloudDetail = data.getParcelableExtra(CLOUD_DETAIL);
                        tvCloudServiceName.setText(mCloudDetail.getPackName());
                        tvCloudServiceCount.setText(String.valueOf(mCloudDetail.getQty()));
                        tvTotal.setText(calculatorCloudServiceTotal(mCloudDetail.getCost(), mCloudDetail.getQty()));
                        btnUpdate.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    @Override
    public void onItemClick(CloudDetail mCloudDetail) {
        this.mCloudDetail = mCloudDetail;
        tvCloudTypeName.setText(mCloudDetail.getTypeName());
        tvCloudServiceCount.setText(String.valueOf(0));
        tvTotal.setText(String.format(getString(R.string.txt_unit_price), Common.formatNumber(0)));
        mRecyclerViewCloud.setVisibility(View.GONE);
        loChoosePackage.setVisibility(View.VISIBLE);
    }

    private String calculatorCloudServiceTotal(int cost, int quantity) {
        return String.format(getString(R.string.txt_unit_price), Common.formatNumber((cost * quantity)));
    }
}
