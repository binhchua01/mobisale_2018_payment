package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_type.CameraTypeActivity;
import isc.fpt.fsale.model.CameraType;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by Hau Le on 2018-12-27.
 */

public class GetAllCameraType implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public GetAllCameraType(Context mContext, int combo, boolean isSellMore) {
        this.mContext = mContext;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("Combo", combo);
            jsonObject.put("RegType", isSellMore ? 1 : 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.msg_progress_get_all_camera_type);
        String GET_ALL_CAMERA_TYPE = "GetAllCameraType";
        CallServiceTask service = new CallServiceTask(mContext, GET_ALL_CAMERA_TYPE, jsonObject,
                Services.JSON_POST_OBJECT, message, GetAllCameraType.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<CameraType> resultObject = new WSObjectsModel<>(jsObj, CameraType.class);
                List<CameraType> lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (mContext != null) {
                    if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                        Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                    } else {
                        ((CameraTypeActivity) mContext).loadCameraList(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
