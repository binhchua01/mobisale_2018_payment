package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.UnConfirmDeviceFragment;
import isc.fpt.fsale.model.ConfirmDeviceResult;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 19,April,2019
 */

public class ConfirmDevice implements AsyncTaskCompleteListener<String> {
    private Context context;
    private UnConfirmDeviceFragment unConfirmDeviceFragment;

    public ConfirmDevice(Context context, UnConfirmDeviceFragment unConfirmDeviceFragment, JSONObject jsonObject) {
        this.context = context;
        this.unConfirmDeviceFragment = unConfirmDeviceFragment;
        String message = context.getResources().getString(R.string.msg_progress_get_all_camera_package);
        String CONFIRM_CAMERA = "ConfirmDevice";
        CallServiceTask service = new CallServiceTask(context, CONFIRM_CAMERA, jsonObject,
                Services.JSON_POST_OBJECT, message, ConfirmDevice.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String json) {
        try {
            ConfirmDeviceResult resultObj;
            if (Common.jsonObjectValidate(json)) {
                JSONObject jsObj = new JSONObject(json);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<ConfirmDeviceResult> resultObject = new WSObjectsModel<>(jsObj, ConfirmDeviceResult.class);
                resultObj = resultObject.getListObject().get(0);
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), context);
                } else {
                    unConfirmDeviceFragment.deviceConfirmResult();
                    Common.alertDialog(String.valueOf(resultObj.getResult()), context);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
