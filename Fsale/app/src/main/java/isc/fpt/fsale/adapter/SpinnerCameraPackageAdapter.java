package isc.fpt.fsale.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CameraPackage;
import isc.fpt.fsale.model.CameraSelected;
import isc.fpt.fsale.model.CameraType;

/**
 * Created by Hau Le on 2019-01-02.
 */
public class SpinnerCameraPackageAdapter extends ArrayAdapter<CameraPackage> {
    private Context context;
    private ArrayList<CameraPackage> list;

    public SpinnerCameraPackageAdapter(Context context, int resource, ArrayList<CameraPackage> list) {
        super(context, resource, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount(){
        return list.size();
    }

    @Override
    public CameraPackage getItem(int position){
        return list.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(list.get(position).getPackName());
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(list.get(position).getPackName());
        return label;
    }
}
