package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionModel;

// abc chứa danh sách clkm internet
public class PromotionAutoCompleteAdapter extends ArrayAdapter<PromotionModel> implements Filterable {
    private ArrayList<PromotionModel> lstObj, resultList;
    private Activity activity;

    public PromotionAutoCompleteAdapter(Context context, int textViewResourceId, ArrayList<PromotionModel> lstObj) {
        super(context, textViewResourceId);
        this.activity = (Activity) context;
        this.resultList = lstObj;
        this.lstObj = lstObj;
    }

    public int getCount() {
        if (resultList != null)
            return resultList.size();
        return 0;
    }

    public PromotionModel getItem(int position) {
        // Tuấn cập nhật code 25092017 kiểm tra size của arraylist.
        if (resultList != null && getCount() > 0)
            return resultList.get(position);
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public ArrayList<PromotionModel> getList() {
        return resultList;
    }

    public ArrayList<PromotionModel> getListFull() {
        return lstObj;
    }


    @SuppressLint("InflateParams")
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_auto_complete, null);
        }

        TextView label = (TextView) convertView.findViewById(R.id.lbl_desc);
        PromotionModel item = lstObj.get(position);
        if (item != null) {

            label.setText(lstObj.get(position).getPromotionName());
        } else
            label.setVisibility(View.GONE);
        int padding = (int) getContext().getResources().getDimension(R.dimen.padding_medium);
        convertView.setPadding(padding, padding, padding, padding);
        return convertView;

    }


    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_auto_complete, null);
        }
        PromotionModel item = getItem(position);
        TextView label = (TextView) convertView.findViewById(R.id.lbl_desc);
        if (item != null)
            label.setText(item.getPromotionName());
        else
            label.setVisibility(View.GONE);
        return convertView;
    }

    @Override
    public Filter getFilter() {
        // TODO Auto-generated method stub
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            resultList = getPromotionFilterList(constraint.toString());
                        }
                    });
                }
                filterResults.values = resultList;
                filterResults.count = resultList.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, final FilterResults results) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (results != null && results.count > 0) {
                            notifyDataSetChanged();
                        } else {
                            notifyDataSetInvalidated();
                        }
                    }
                });
            }
        };

        return filter;
    }

    @SuppressLint("DefaultLocale")
    private ArrayList<PromotionModel> getPromotionFilterList(String constraint) {
        String[] subStrs = constraint.split("\\*");
        ArrayList<PromotionModel> results = new ArrayList<PromotionModel>();
        for (int i = 0; i < lstObj.size(); i++) {
            PromotionModel item = lstObj.get(i);
            String parentStr = item.getPromotionName().toUpperCase();
            boolean exist = true;
            if (subStrs.length > 0) {
                for (int j = 0; j < subStrs.length; j++) {
                    String s = subStrs[j].toUpperCase();
                    if (s != null && !s.trim().equals(""))
                        if (!parentStr.contains(s))
                            exist = false;
                }
                if (exist) {
                    results.add(item);
                }
            } else
                results = lstObj;

        }
        return results;
    }
}
