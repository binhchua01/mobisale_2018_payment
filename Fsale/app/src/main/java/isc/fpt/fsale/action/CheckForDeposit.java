package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.DepositActivity;

import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONObject;

import android.content.Context;

/*
 * @author DuHK - 12/05/2016
 * @see: Kiem tra PDK truoc khi cap nhat
 */

public class CheckForDeposit implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public CheckForDeposit(Context mContext, RegistrationDetailModel modelDetail, String sbiInternet, String sbiIPTV) {
        this.mContext = mContext;
        String message = mContext.getResources().getString(
                R.string.msg_pd_checkRegistration);
        String CHECK_DEPOSIT = "CheckForDeposit";
        String[] arrParamName = new String[]{"UserName", "RegCode", "Total", "sbiInternet", "sbiIPTV"};
        String[] arrParamValues = new String[]{Constants.USERNAME, modelDetail.getRegCode(),
                String.valueOf(modelDetail.getTotal()), sbiInternet, sbiIPTV};
        CallServiceTask service = new CallServiceTask(mContext, CHECK_DEPOSIT, arrParamName, arrParamValues,
                Services.JSON_POST, message, CheckForDeposit.this);
        service.execute();
    }

    private void handleCheckRegistration(String json) {
        try {
            List<UpdResultModel> lst = null;
            boolean isError = false;
            if (Common.jsonObjectValidate(json)) {
                JSONObject jsObj = new JSONObject(json);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(jsObj, UpdResultModel.class);
                if (resultObject.getErrorCode() == 0) {
                    lst = resultObject.getListObject();
                } else {
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError) {
                    returnResult(lst);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleCheckRegistration(result);
    }

    private void returnResult(List<UpdResultModel> lst) {
        if (mContext.getClass().getSimpleName().equals(DepositActivity.class.getSimpleName())) {
            DepositActivity activity = (DepositActivity) mContext;
            activity.loadResultCheckForDeposit(lst);
        }
    }
}
