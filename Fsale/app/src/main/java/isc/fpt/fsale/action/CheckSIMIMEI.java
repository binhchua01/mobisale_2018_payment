package isc.fpt.fsale.action;

import android.content.Context;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.LoginActivity;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

/*
 * ACTION: CheckSIMIMEI
 *
 * @description: - call service and check if device's sim imei is active -
 * handle response result: if sim isn't active, show message
 * dialog
 * @author: vandn, on 13/08/2013
 */

public class CheckSIMIMEI implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public CheckSIMIMEI(Context mContext, String[] arrParams) {
        this.mContext = mContext;
        String message = mContext.getResources().getString(
                R.string.msg_pd_checkimei);
        String[] params = new String[]{"SimIMEI", "DeviceIMEI", "AndroidVersion", "ModelNumber"};
        String CHECK_IMEI = "CheckImei";
        CallServiceTask service = new CallServiceTask(mContext, CHECK_IMEI,
                params, arrParams, Services.JSON_POST, message, CheckSIMIMEI.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = JSONParsing.getJsonObj(result);
                String TAG_CHECK_IMEI_RESULT = "CheckIMEIMethodPostResult";
                JSONObject obj = jsObj.getJSONObject(TAG_CHECK_IMEI_RESULT);
                String TAG_ERROR = "ErrorService";
                String error = obj.getString(TAG_ERROR);
                if (!TextUtils.isEmpty(error)) {
                    String TAG_IMEI_ACTIVE = "IsActive";
                    if ((obj.getInt(TAG_IMEI_ACTIVE) == 1) && obj.getString("UserID") != null) {
                        ((LoginActivity) mContext).bindUsername(obj.getString("UserID"));
                    } else {
                        Common.alertDialog(mContext.getResources().getString(R.string.msg_isnot_active), mContext);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}