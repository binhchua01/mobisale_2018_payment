package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class InsertInvestiageAction implements AsyncTaskCompleteListener<String>{ 

	private final String InsertInvestiage = "UpdateSurvey";
	private final String InsertInvestiage_RESULT = "UpdateSurveyResult";
	private final String ERRORSERVICE = "ErrorService";
	private final String RESULT = "Result";
	
	private Context mContext;
	private int RegID;
	
	public InsertInvestiageAction(Context mContext, int RegID, String RegCode, int OutDType, int OutDoor, int InDType, int InDoor, String Image, String MapCode, int Modem, String TDName, String UserName, String Latlng) 
	{
		this.mContext = mContext;
		this.RegID = RegID;
		Insert(new String[]{RegCode, String.valueOf(OutDType), String.valueOf(OutDoor), String.valueOf(InDType), String.valueOf(InDoor), Image, MapCode, String.valueOf(Modem), TDName, UserName, Latlng});
	}
	
	public void Insert(String[] paramsValue)
	{		
		String[] params = new String[]{"RegCode","OutDType","OutDoor","InDType","InDoor","Image","MapCode","Modem","TDName","UserName","Latlng"};
		String message = "Vui lòng đợi giây lát";
		CallServiceTask service = new CallServiceTask(mContext,InsertInvestiage,params,paramsValue, Services.JSON_POST, message, InsertInvestiageAction.this);
		service.execute();
	}
	
	public void handleUpdateRegistration(String json){		
		if(json != null && Common.jsonObjectValidate(json)){			
			 try {	
				   JSONArray jsArr;
				    JSONObject jsObj = JSONParsing.getJsonObj(json);
				    jsArr = jsObj.getJSONArray(InsertInvestiage_RESULT);
				    int l = jsArr.length();
				    if (l > 0) 
				    {
					     String error = jsArr.getJSONObject(0).getString(ERRORSERVICE);
					     if (error.equals("null")) 
					     {
					    	 String result = jsArr.getJSONObject(0).getString(RESULT);
						      int ResultID=jsArr.getJSONObject(0).getInt("ResultID");
						      if(ResultID==1)
						      {
						    	  new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(mContext.getResources().getString(R.string.msg_result_investiage_success))
					 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
					 				    @Override
					 				    public void onClick(DialogInterface dialog, int which) {
					 				        dialog.cancel();
					 				      
					 				       new GetRegistrationDetail(mContext,Constants.USERNAME, RegID);					 				     
					 				    }
					 				}).show();
						    	 
						      }
						      else
						      {
						    	 // new GetRegistrationDetail(mContext,"oanh254",this.ID);
						    	 Common.alertDialog(result, mContext);
						      }
					     }
					     else  Common.alertDialog(error, mContext);
					    	 
				    }
			 }
			 catch (Exception e)
			 {

				e.printStackTrace();
				Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
			 }
		}		
		else Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);		
	}
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);
	}

}
