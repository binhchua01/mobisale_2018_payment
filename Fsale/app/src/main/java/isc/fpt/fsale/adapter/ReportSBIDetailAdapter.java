package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportSBIDetailModel;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReportSBIDetailAdapter  extends BaseAdapter{
	private List<ReportSBIDetailModel> mList;	
	private Context mContext;
	
	public ReportSBIDetailAdapter(Context context, List<ReportSBIDetailModel> lst){
		this.mContext = context;
		this.mList = lst;	
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public ReportSBIDetailModel getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportSBIDetailModel item = mList.get(position);		
		if (view == null) {
			view = LayoutInflater.from(mContext).inflate(R.layout.row_report_sbi_detail, null);
		}
		if(item != null){
			if(getCount()> 0){
				TextView lable = (TextView)view.findViewById(R.id.lbl_row_number);
				lable.setText(String.valueOf(position+1));
			}
			if(!item.getContract().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_report_sbi_detail_contract);
				lable.setText(item.getContract());
			}
			if(!item.getRegCode().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_report_sbi_detail_reg_code);
				lable.setText(item.getRegCode());
			}
			if(!item.getReIssuedDate().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_report_sbi_detail_re_issued_date);
				lable.setText(item.getReIssuedDate());
			}
			if(!item.getSBI().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_report_sbi_detail_sbi);
				lable.setText(item.getSBI());
			}
			if(!item.getStatusName().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_report_sbi_detail_status_name);
				lable.setText(item.getStatusName());
			}
			if(!item.getTypeName().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_report_sbi_detail_type_name);
				lable.setText(item.getTypeName());
			}

		}
		
		
		return view;
	}

}
