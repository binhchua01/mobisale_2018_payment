package isc.fpt.fsale.ui.fpt_camera.promotion_detail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPromotionFptCamera;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.model.GetAllPromotionPackage;
import isc.fpt.fsale.ui.fpt_camera.model.PromotionDetail;

import static isc.fpt.fsale.utils.Constants.CAMERA_PROMOTION;
import static isc.fpt.fsale.utils.Constants.CAMERA_PROMOTION_SELECTED;

public class PromotionDetailActivity extends BaseActivitySecond implements OnItemClickListener<PromotionDetail> {
    private View loBack;
    private GetAllPromotionPackage mObject;
    private RecyclerView mRecyclerView;
    private List<PromotionDetail> mList;
    private PromotionAdapter mAdapter;
    private PromotionDetail mPromotionDetailSelected;

    @Override
    protected void onResume() {
        super.onResume();
        if(mObject != null){
            new GetPromotionFptCamera(this, mObject);
        }
    }

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_promotion_detail;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.lbl_screen_name_promotion_ad_list));
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_promotion_list);
        mList = new ArrayList<>();
        mAdapter = new PromotionAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);

        getDataIntent();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(CAMERA_PROMOTION) != null) {
            mObject = bundle.getParcelable(CAMERA_PROMOTION);
            mPromotionDetailSelected = bundle.getParcelable(CAMERA_PROMOTION_SELECTED);
        }
    }

    public void loadPromotion(List<PromotionDetail> mList){
        this.mList = mList;
        if(mPromotionDetailSelected != null){
            if(mPromotionDetailSelected.getPromoID() != 0){
                for(PromotionDetail item : mList){
                    if(item.getPromoID() == mPromotionDetailSelected.getPromoID()){
                        item.setSelected(true);
                    }
                }
            }
        }
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(PromotionDetail mPromotionDetail) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(CAMERA_PROMOTION, mPromotionDetail);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
