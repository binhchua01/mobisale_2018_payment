package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.Gravity;
import android.widget.Spinner;

public class GetPercentTDFTTH implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private Spinner spCoreType;
    private ArrayList<KeyValuePairModel> ListCore;
    private int iIndex = -1;

    public GetPercentTDFTTH(Context _mContext, String[] arrParams, Spinner sp, int LocationID) {
        this.mContext = _mContext;
        this.spCoreType = sp;
        int HO_CHI_MINH = 36;
        if (LocationID == HO_CHI_MINH) {
            //call service
            CallService(arrParams);
        } else {
            SetListCoreType(iIndex);
        }

    }

    private void CallService(String[] arrParams) {
        String message = "Xin cho trong giay lat";
        String[] params = new String[]{"TDName"};
        String GET_PERCENT_FTTH = "GetPercenTDFTTH";
        CallServiceTask service = new CallServiceTask(mContext, GET_PERCENT_FTTH, params, arrParams,
                Services.JSON_POST, message, GetPercentTDFTTH.this);
        service.execute();

    }

    public void handleGetDistrictsResult(String json) {
        ListCore = new ArrayList<>();
        ListCore.add(new KeyValuePairModel(-1, "[ Vui lòng chọn loại ]"));
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if (Common.isEmptyJSONObject(jsObj)) {
                KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListCore, Gravity.LEFT);
                spCoreType.setAdapter(adapter);
                return;
            }
            bindData(jsObj);
            SetListCoreType(iIndex);
        } else {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListCore, Gravity.LEFT);
            spCoreType.setAdapter(adapter);
        }
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {
            String TAG_GET_PERCENT_FTTH = "GetPercenTDFTTHMethodPostResult";
            jsArr = jsObj.getJSONArray(TAG_GET_PERCENT_FTTH);
            String error = jsArr.getJSONObject(0).getString("ErrorService");
            if (error.equals("null")) {
                int l = jsArr.length();
                if (l > 0) {
                    JSONObject iObj = jsArr.getJSONObject(0);
                    int iPercent;
                    try {
                        iPercent = iObj.getInt("Result");
                        int INDEX_ONE_CORE = 0;
                        int INDEX_TWO_CORE = 1;
                        if (iPercent < 46)
                            iIndex = INDEX_ONE_CORE;
                        else
                            iIndex = INDEX_TWO_CORE;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleGetDistrictsResult(result);
    }

    private void SetListCoreType(int index) {
        try {
            ListCore = new ArrayList<>();
            ListCore.add(new KeyValuePairModel(2, "Một Core"));
            ListCore.add(new KeyValuePairModel(1, "Hai Core"));
            if (index > -1) {
                ListCore.remove(index);
            }
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListCore, Gravity.CENTER);
            spCoreType.setAdapter(adapter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
