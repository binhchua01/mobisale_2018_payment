package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportCreateObjectTotalActivity;
import isc.fpt.fsale.activity.ListReportPotentialObjTotalActivity;

import isc.fpt.fsale.activity.ReportSubscriberGrowthForManagerActivity;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class GetDeptList implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    //Add by: DuHK
    public GetDeptList(Context mContext, String UserName) {
        this.mContext = mContext;
        String[] paramNames = {"UserName"};
        String[] paramValues = {UserName};
        String message = "Đang lấy dữ liệu...";
        String METHOD_NAME = "GetDeptList";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, GetDeptList.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            boolean isError = false;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<KeyValuePairModel> resultObject = new WSObjectsModel<>(jsObj, KeyValuePairModel.class);
                if (resultObject.getErrorCode() != 0) {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError) {
                    loadData(result);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(String result) {
        try {
            if (mContext != null) {
                Common.savePreference(mContext, Constants.SHARE_PRE_OBJECT,
                        Constants.SHARE_PRE_OBJECT_DEPT_LIST, result);
                if (mContext.getClass().getSimpleName().equals(
                        ReportSubscriberGrowthForManagerActivity.class.getSimpleName())) {
                    ReportSubscriberGrowthForManagerActivity activity =
                            (ReportSubscriberGrowthForManagerActivity) mContext;
                    activity.loadSpinnerDept();
                } else if (mContext.getClass().getSimpleName()
                        .equals(ListReportPotentialObjTotalActivity.class.getSimpleName())) {
                    ListReportPotentialObjTotalActivity activity =
                            (ListReportPotentialObjTotalActivity) mContext;
                    activity.loadSpinnerDept();
                } else if (mContext.getClass().getSimpleName()
                        .equals(ListReportCreateObjectTotalActivity.class.getSimpleName())) {
                    ListReportCreateObjectTotalActivity activity =
                            (ListReportCreateObjectTotalActivity) mContext;
                    activity.loadSpinnerDept();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}