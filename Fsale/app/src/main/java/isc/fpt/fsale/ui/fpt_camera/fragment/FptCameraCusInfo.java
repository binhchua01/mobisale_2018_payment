package isc.fpt.fsale.ui.fpt_camera.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.DetailRegistrationDocumentActivity;
import isc.fpt.fsale.activity.IdentityCardScanActivity;
import isc.fpt.fsale.activity.UploadRegistrationDocumentActivity;
import isc.fpt.fsale.activity.cus_type.CusTypeActivity;
import isc.fpt.fsale.activity.cus_type_detail.CusTypeDetailActivity;
import isc.fpt.fsale.activity.district.DistrictActivity;
import isc.fpt.fsale.activity.house_position.HousePositionActivity;
import isc.fpt.fsale.activity.house_type.HouseTypeActivity;
import isc.fpt.fsale.activity.phone_type.PhoneTypeActivity;
import isc.fpt.fsale.activity.source_type.SourceTypeActivity;
import isc.fpt.fsale.activity.street_or_condo.StreetOrCondoActivity;
import isc.fpt.fsale.activity.ward.WardActivity;
import isc.fpt.fsale.model.District;
import isc.fpt.fsale.model.IdentityCard;
import isc.fpt.fsale.model.ImageDocument;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.StreetOrCondo;
import isc.fpt.fsale.model.Ward;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.ui.fpt_camera.FptCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.model.RegisterFptCameraModel;
import isc.fpt.fsale.ui.fragment.DatePickerReportDialog;
import isc.fpt.fsale.ui.fragment.HouseNumFormatRuleDialog;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.activity.RegisterDetailActivity.TAG_REG_ID;
import static isc.fpt.fsale.utils.Constants.TAG_IMAGE_DOCUMENT_IDENTITY_CARD_SELECTED;
import static isc.fpt.fsale.utils.Constants.TAG_LIST_IMAGE_DOCUMENT;
import static isc.fpt.fsale.utils.Constants.TAG_LIST_IMAGE_DOCUMENT_SELECTED;
import static isc.fpt.fsale.utils.Constants.TAG_REGISTRATION_CREATE_FORM;
import static isc.fpt.fsale.utils.Constants.TAG_UPDATE_REGISTRATION;
import static isc.fpt.fsale.utils.Constants.TAG_UPLOAD_IMAGE_TYPE;
import static isc.fpt.fsale.utils.DateTimeHelper.formatDateOfBirthRegister;

/**
 * Created by haulc3 on 28,August,2019
 */
public class FptCameraCusInfo extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    public Context mContext;
    private FptCameraActivity activity;
    public ScrollView scrollMain;
    public View taxNumLayout, phoneNumberLayout1, mViewButtonImage, mViewChildScroll;
    public EditText txtCustomerName, txtIdentityCard, txtAddressId, txtTaxNum,
            txtEmail, txtLot, txtFloor, txtRoom, txtHouseNum, txtPhone1,
            txtContactPhone1, txtPhone2, txtContactPhone2, txtHouseDesc, txtDescriptionIBB,
            txtBirthDay;
    /**
     * ver3.23
     * author : NhanNH26
     * Add SourceType
     * Day: 28/09/2021
     */
    public TextView tvPhone1, tvPhone2, tvDistrict, txtCusTypeDetail, txtCusType,
            tvWard, tvHouseType, tvStreet, tvApartment, tvHousePosition, tvSourceType;
    public ViewGroup apartmentPanel, housePositionPanel, houseNumberPanel;
    public Button btnShowHouseNumFormat, btnTakePhoto, btnOpenGallery, btnUploadImageDocument, btnDetailImageDocument;
    private LinearLayout loParent;
    private Calendar myCalendar;
    private RegisterFptCameraModel mRegisterFptCameraModel;
    // file ảnh sau khi chụp ảnh cmnd
    public File fileIdentityCardImageDocument;
    public DatePickerReportDialog mDateDialog;

    private final int CODE_PHONE_TYPE_1 = 102;
    private final int CODE_PHONE_TYPE_2 = 103;
    private final int CODE_DISTRICT = 104;
    private final int CODE_WARD = 105;
    private final int CODE_HOUSE_TYPE = 106;
    private final int CODE_STREET = 107;
    private final int CODE_CONDO = 108;
    private final int CODE_HOUSE_POSITION = 109;
    private final int CODE_OPEN_GALLERY = 110;
    private final int CODE_REQUEST_OPEN_GALLERY = 111;
    private final int CODE_REQUEST_TAKE_PHOTO = 112;
    private final int CODE_SCAN_IDENTITY = 113;
    private final int CODE_CUS_TYPE = 114;
    private final int CODE_CUS_TYPE_DETAIL = 115;
    private final int REQUEST_CAMERA_TAKE_PHOTO = 0;
    private final int CODE_SOURCE_TYPE= 116;

    // danh sách ảnh hồ sơ
    public List<ImageDocument> listImageDocument = new ArrayList<>();
    // danh sách ảnh cmnd
    private List<ImageDocument> listImageDocumentIdentityCard = new ArrayList<>();
    //ID các ảnh đã upload
    public String strListImageDocumentInfo = "";
    public String pathImageIdentityCard;
    private boolean statusUpdateRegistration;

    // thiết lập đường dẫn ảnh cmnd và lưu ảnh cmnd mới vào danh sách ảnh cmnd
    public void setPathImageDocument(String imagePath) {
        this.pathImageIdentityCard = imagePath;
        listImageDocumentIdentityCard.clear();
        listImageDocumentIdentityCard.add(new ImageDocument(pathImageIdentityCard, true, 2));
    }

    // cập nhật danh sách hồ sơ khách hàng
    public void setListImageDocument(ArrayList<ImageDocument> listImageDocumentInput) {
        statusUpdateRegistration = listImageDocumentInput.size() > 0;
        this.listImageDocument = listImageDocumentInput;
        this.strListImageDocumentInfo = Common.covertListImageDocumentToString((ArrayList<ImageDocument>)
                listImageDocument, ",");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
        activity = (FptCameraActivity) getActivity();
        mRegisterFptCameraModel = activity.getRegisterModel();
    }

    @Override
    protected void initView(View view) {
        scrollMain = view.findViewById(R.id.frm_main);
        mViewChildScroll = view.findViewById(R.id.layout_child);
        mViewButtonImage = view.findViewById(R.id.layout_button);
        taxNumLayout = view.findViewById(R.id.frm_tax_num);
        phoneNumberLayout1 = view.findViewById(R.id.frm_phone_number_1);
        apartmentPanel = view.findViewById(R.id.frm_apartment);// Apartment Panel
        housePositionPanel = view.findViewById(R.id.frm_house_position); // Position
        houseNumberPanel = view.findViewById(R.id.frm_house_num);// House Number
        txtCustomerName = view.findViewById(R.id.txt_customer_name); //Họ tên khách hàng
        txtCustomerName.requestFocus();
        txtIdentityCard = view.findViewById(R.id.txt_identity_card);//CMND
        txtBirthDay = view.findViewById(R.id.txt_birthday);// Ngay sinh
        txtAddressId = view.findViewById(R.id.txt_address_in_id);//Địa chỉ trên CMND
        txtTaxNum = view.findViewById(R.id.txt_tax_num);//Mã số thuế
        txtEmail = view.findViewById(R.id.txt_email);
        tvApartment = view.findViewById(R.id.tv_apartment_name);
        txtRoom = view.findViewById(R.id.txt_room);
        txtFloor = view.findViewById(R.id.txt_floor);
        txtLot = view.findViewById(R.id.txt_apartment_group);
        txtHouseNum = view.findViewById(R.id.txt_house_num);
        txtPhone1 = view.findViewById(R.id.txt_phone_1);
        txtContactPhone1 = view.findViewById(R.id.txt_contact_phone_1);
        txtHouseDesc = view.findViewById(R.id.txt_note_address);
        txtDescriptionIBB = view.findViewById(R.id.txt_description_ibb);
        txtPhone2 = view.findViewById(R.id.txt_phone_2);
        txtContactPhone2 = view.findViewById(R.id.txt_contact_phone_2);
        tvPhone1 = view.findViewById(R.id.tv_phone_1);
        tvPhone2 = view.findViewById(R.id.tv_phone_2);
        tvDistrict = view.findViewById(R.id.tv_districts);
        tvWard = view.findViewById(R.id.tv_wards);
        tvStreet = view.findViewById(R.id.tv_streets);
        tvHouseType = view.findViewById(R.id.tv_house_types);
        tvHousePosition = view.findViewById(R.id.tv_house_position);
        btnShowHouseNumFormat = view.findViewById(R.id.btn_show_house_num_format);
        btnTakePhoto = view.findViewById(R.id.btn_capture_identity_card);
        btnOpenGallery = view.findViewById(R.id.btn_open_local_identity_card);
        txtCusTypeDetail = view.findViewById(R.id.txt_customer_type_detail);
        txtCusType = view.findViewById(R.id.txt_customer_type);
        btnUploadImageDocument = view.findViewById(R.id.btn_upload_image_document);
        btnDetailImageDocument = view.findViewById(R.id.btn_detail_image_document);
        loParent = view.findViewById(R.id.container);
        //ver3.23
        tvSourceType = view.findViewById(R.id.txt_source_type);//hình thức bán

        txtCusType.setText(getCusType(1));
        mRegisterFptCameraModel.setCusType(1);
        txtCusTypeDetail.setText(getCusTypeDetail(12));
        mRegisterFptCameraModel.setCusTypeDetail(String.valueOf(12));

        if (mRegisterFptCameraModel != null) {
            if (mRegisterFptCameraModel.getCusType() != 0) {
                txtCusType.setText(getCusType(mRegisterFptCameraModel.getCusType()));
            } else {
                txtCusType.setText(getCusType(1));
                mRegisterFptCameraModel.setCusType(1);
            }
            if(mRegisterFptCameraModel.getCusTypeDetail() == null){
                txtCusTypeDetail.setText(getCusTypeDetail(12));
                mRegisterFptCameraModel.setCusTypeDetail(String.valueOf(12));
            }
            else {
                txtCusTypeDetail.setText(getCusTypeDetail(Integer.parseInt(mRegisterFptCameraModel.getCusTypeDetail())));
            }
        }
        //set lại dịch vụ triển khai mặc định khi chọn lại loại khách hàng
        //activity.setUpdateCusType(true);
    }

    private void disableEnableControls(ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);

            if (!TextUtils.isEmpty(mRegisterFptCameraModel.getBaseImageInfo()) &&
                    mRegisterFptCameraModel.getBaseRegIDImageInfo() != 0) {
                if (child.getId() == R.id.container || child.getId() == R.id.frm_main ||
                        child.getId() == R.id.layout_child || child.getId() == R.id.frm_address_input ||
                        child.getId() == R.id.layout_input || child.getId() == R.id.layout_input_birthday ||
                        child.getId() == R.id.label_input_birthday || child.getId() == R.id.txt_description_ibb ||
                        child.getId() == R.id.txt_birthday || child.getId() == R.id.img_input_birthday ||
                        child.getId() == R.id.layout_input_email || child.getId() == R.id.label_input_email ||
                        child.getId() == R.id.txt_email || child.getId() == R.id.frm_phone_number_1 ||
                        child.getId() == R.id.label_phone_type_1 || child.getId() == R.id.tv_phone_1 ||
                        child.getId() == R.id.img_phone_type_1 || child.getId() == R.id.layout_phone_number_1 ||
                        child.getId() == R.id.txt_phone_1 || child.getId() == R.id.txt_contact_phone_1 ||
                        child.getId() == R.id.layout_phone_type_2 || child.getId() == R.id.label_phone_type_2 ||
                        child.getId() == R.id.tv_phone_2 || child.getId() == R.id.img_phone_type_2 ||
                        child.getId() == R.id.layout_phone_number_2 || child.getId() == R.id.txt_phone_2 ||
                        child.getId() == R.id.txt_contact_phone_2 || child.getId() == R.id.label_note_address ||
                        child.getId() == R.id.txt_note_address || child.getId() == R.id.label_description_ibb ||
                        child.getId() == R.id.layout_cus_type || child.getId() == R.id.label_cus_type ||
                        child.getId() == R.id.txt_customer_type || child.getId() == R.id.image_cus_type ||
                        child.getId() == R.id.layout_cus_type_detail || child.getId() == R.id.label_cus_type_detail ||
                        child.getId() == R.id.txt_customer_type_detail || child.getId() == R.id.image_cus_type_detail ||
                        child.getId() == R.id.layout_identify_address || child.getId() == R.id.label_identify_address ||
                        child.getId() == R.id.txt_address_in_id || child.getId() == R.id.label_info ||
                        child.getId() == R.id.layout_source_type || child.getId() == R.id.label_source_type ||
                        child.getId() == R.id.txt_source_type || child.getId() == R.id.image_source_type) {
                    child.setEnabled(true);
                    child.setAlpha(1F);
                } else {
                    child.setEnabled(false);
                    child.setAlpha(0.5F);
                }
            } else {//cho phép xem, cập nhật hình ảnh PDK combo, birthday,
                // email, phone1, phone2, note, descriptionIBB, loại KH, loại hình, ĐC trên CMND
                if (child.getId() == R.id.container || child.getId() == R.id.frm_main ||
                        child.getId() == R.id.layout_child || child.getId() == R.id.frm_address_input ||
                        child.getId() == R.id.layout_input || child.getId() == R.id.layout_button ||
                        child.getId() == R.id.btn_upload_image_document ||
                        child.getId() == R.id.btn_detail_image_document ||
                        child.getId() == R.id.layout_input_birthday || child.getId() == R.id.label_input_birthday ||
                        child.getId() == R.id.txt_birthday || child.getId() == R.id.img_input_birthday ||
                        child.getId() == R.id.layout_input_email || child.getId() == R.id.label_input_email ||
                        child.getId() == R.id.txt_email || child.getId() == R.id.frm_phone_number_1 ||
                        child.getId() == R.id.label_phone_type_1 || child.getId() == R.id.tv_phone_1 ||
                        child.getId() == R.id.img_phone_type_1 || child.getId() == R.id.layout_phone_number_1 ||
                        child.getId() == R.id.txt_phone_1 || child.getId() == R.id.txt_contact_phone_1 ||
                        child.getId() == R.id.layout_phone_type_2 || child.getId() == R.id.label_phone_type_2 ||
                        child.getId() == R.id.tv_phone_2 || child.getId() == R.id.img_phone_type_2 ||
                        child.getId() == R.id.layout_phone_number_2 || child.getId() == R.id.txt_phone_2 ||
                        child.getId() == R.id.txt_contact_phone_2 || child.getId() == R.id.label_note_address ||
                        child.getId() == R.id.txt_note_address || child.getId() == R.id.label_description_ibb ||
                        child.getId() == R.id.txt_description_ibb ||
                        child.getId() == R.id.layout_cus_type || child.getId() == R.id.label_cus_type ||
                        child.getId() == R.id.txt_customer_type || child.getId() == R.id.image_cus_type ||
                        child.getId() == R.id.layout_cus_type_detail || child.getId() == R.id.label_cus_type_detail ||
                        child.getId() == R.id.txt_customer_type_detail || child.getId() == R.id.image_cus_type_detail ||
                        child.getId() == R.id.layout_identify_address || child.getId() == R.id.label_identify_address ||
                        child.getId() == R.id.txt_address_in_id || child.getId() == R.id.label_info ||
                        child.getId() == R.id.layout_source_type || child.getId() == R.id.label_source_type ||
                        child.getId() == R.id.txt_source_type || child.getId() == R.id.image_source_type) {
                    child.setEnabled(true);
                    child.setAlpha(1F);
                } else {
                    child.setEnabled(false);
                    child.setAlpha(0.5F);
                }
            }
            if (child instanceof ViewGroup) {
                disableEnableControls((ViewGroup) child);
            }
        }
    }

    private void checkSaleCombo() {
        if (activity.getObjectDetailModel() != null || mRegisterFptCameraModel.getBaseObjID() > 0) {
            disableEnableControls(loParent);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initEvent() {
        mDateDialog = initDatePickerDialog(txtBirthDay, getString(R.string.title_dialog_birth_day));
        tvPhone1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.isEnabled()) {
                    startActivityForResult(new Intent(getActivity(), PhoneTypeActivity.class), CODE_PHONE_TYPE_1);
                }
            }
        });
        tvPhone2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), PhoneTypeActivity.class), CODE_PHONE_TYPE_2);
            }
        });
        tvDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), DistrictActivity.class), CODE_DISTRICT);
            }
        });
        tvWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mRegisterFptCameraModel.getBillTo_District())) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DISTRICT_ID, mRegisterFptCameraModel.getBillTo_District());
                    Intent intent = new Intent(getActivity(), WardActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_WARD);
                } else {
                    showError(getString(R.string.txt_message_choose_district));
                }
            }
        });
        tvHouseType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), HouseTypeActivity.class), CODE_HOUSE_TYPE);
            }
        });
        tvStreet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mRegisterFptCameraModel.getBillTo_District()) &&
                        !TextUtils.isEmpty(mRegisterFptCameraModel.getBillTo_Ward())) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DISTRICT_ID, mRegisterFptCameraModel.getBillTo_District());
                    bundle.putString(Constants.WARD_ID, mRegisterFptCameraModel.getBillTo_Ward());
                    bundle.putInt(Constants.TYPE, 0); //get street
                    Intent intent = new Intent(getActivity(), StreetOrCondoActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_STREET);
                } else {
                    if (TextUtils.isEmpty(mRegisterFptCameraModel.getBillTo_District())) {
                        showError(getString(R.string.txt_message_choose_district));
                    } else {
                        showError(getString(R.string.txt_message_choose_ward));
                    }
                }
            }
        });
        tvApartment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mRegisterFptCameraModel.getBillTo_District()) &&
                        !TextUtils.isEmpty(mRegisterFptCameraModel.getBillTo_Ward())) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DISTRICT_ID, mRegisterFptCameraModel.getBillTo_District());
                    bundle.putString(Constants.WARD_ID, mRegisterFptCameraModel.getBillTo_Ward());
                    bundle.putInt(Constants.TYPE, 1); //get condo
                    Intent intent = new Intent(getActivity(), StreetOrCondoActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_CONDO);
                } else {
                    if (TextUtils.isEmpty(mRegisterFptCameraModel.getBillTo_District())) {
                        showError(getString(R.string.txt_message_choose_district));
                    } else {
                        showError(getString(R.string.txt_message_choose_ward));
                    }
                }
            }
        });
        tvHousePosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), HousePositionActivity.class), CODE_HOUSE_POSITION);
            }
        });
        myCalendar = Calendar.getInstance();
        txtBirthDay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (getActivity() != null) {
                        mDateDialog.setCancelable(false);
                        mDateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
                    }
                    txtBirthDay.setError(null);
                    return true;
                }
                return false;
            }
        });
        btnShowHouseNumFormat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HouseNumFormatRuleDialog dialog = new HouseNumFormatRuleDialog();
                Common.showFragmentDialog(getFragmentManager(), dialog, "fragment_house_num_format_dialog");
            }
        });
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mContext.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                            || mContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, CODE_REQUEST_TAKE_PHOTO);
                    } else {
                        fileIdentityCardImageDocument = Common.capture();
                        takePhoto(fileIdentityCardImageDocument);
                    }
                } else {
                    fileIdentityCardImageDocument = Common.capture();
                    takePhoto(fileIdentityCardImageDocument);
                }
            }
        });
        btnOpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mở file ảnh từ thiết bị
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                            PackageManager.PERMISSION_GRANTED ||
                            mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) !=
                                    PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, CODE_REQUEST_OPEN_GALLERY);
                    } else {
                        openGallery();
                    }
                } else {
                    openGallery();
                }
            }
        });
        txtCusType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), CusTypeActivity.class), CODE_CUS_TYPE);
            }
        });

        txtCusTypeDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), CusTypeDetailActivity.class), CODE_CUS_TYPE_DETAIL);
            }
        });
        //upload ảnh hồ sơ khách hàng lên server
        btnUploadImageDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, UploadRegistrationDocumentActivity.class);
                intent.putExtra(TAG_IMAGE_DOCUMENT_IDENTITY_CARD_SELECTED, pathImageIdentityCard);
                intent.putExtra(TAG_UPDATE_REGISTRATION, statusUpdateRegistration);
                intent.putExtra(TAG_REGISTRATION_CREATE_FORM, true);
                if (mRegisterFptCameraModel != null) {
                    intent.putExtra(TAG_REG_ID, mRegisterFptCameraModel.getID());
                }
                // biến định danh upload hồ sơ (3)
                intent.putExtra(TAG_UPLOAD_IMAGE_TYPE, 3);
                intent.putParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT_SELECTED,
                        (ArrayList<? extends Parcelable>) listImageDocument);
                startActivityForResult(intent, CODE_SCAN_IDENTITY);
            }
        });
        btnDetailImageDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailRegistrationDocumentActivity.class);
                intent.putExtra(TAG_UPDATE_REGISTRATION, statusUpdateRegistration);
                intent.putParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT_SELECTED,
                        (ArrayList<? extends Parcelable>) listImageDocument);
                startActivityForResult(intent, CODE_SCAN_IDENTITY);
            }
        });

        tvSourceType.setOnClickListener(v ->
                startActivityForResult(new Intent(getActivity(),
                        SourceTypeActivity.class),
                        CODE_SOURCE_TYPE));
    }

    private DatePickerReportDialog initDatePickerDialog(EditText txtValue, String dialogTitle) {
        Calendar minDate = Calendar.getInstance(), starDate = Calendar.getInstance(), maxDate = Calendar.getInstance();
        starDate.set(Calendar.DAY_OF_MONTH, 1);
        starDate.set(Calendar.MONTH, 0);
        starDate.set(Calendar.YEAR, 1990);
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        minDate.set(Calendar.MONTH, 0);
        minDate.set(Calendar.YEAR, 1900);
        return new DatePickerReportDialog(this, starDate, minDate, maxDate,
                dialogTitle, txtValue, true);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                switch (requestCode) {
                    case CODE_PHONE_TYPE_1:
                        KeyValuePairModel modelPhoneType1 = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        if (modelPhoneType1 == null) return;
                        tvPhone1.setText(modelPhoneType1.getDescription());
                        mRegisterFptCameraModel.setType_1(modelPhoneType1.getID());
                        break;
                    case CODE_PHONE_TYPE_2:
                        KeyValuePairModel modelPhoneType2 = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        if (modelPhoneType2 == null) return;
                        tvPhone2.setText(modelPhoneType2.getDescription());
                        mRegisterFptCameraModel.setType_2(modelPhoneType2.getID());
                        break;
                    case CODE_DISTRICT:
                        District district = (District) data.getSerializableExtra(Constants.DISTRICT);
                        if (district == null) return;
                        tvDistrict.setText(district.getFullNameVN());
                        mRegisterFptCameraModel.setBillTo_District(district.getName());
                        //xóa data ward, street khi chọn lại district
                        tvWard.setText("");
                        tvStreet.setText("");
                        txtFloor.setText("");
                        txtLot.setText("");
                        txtRoom.setText("");
                        tvApartment.setText("");
                        txtHouseNum.setText("");
                        mRegisterFptCameraModel.setBillTo_Number("");
                        mRegisterFptCameraModel.setBillTo_Ward("");
                        mRegisterFptCameraModel.setBillTo_Street("");
                        mRegisterFptCameraModel.setNameVilla("");
                        break;
                    case CODE_WARD:
                        tvDistrict.setError(null);
                        Ward ward = (Ward) data.getSerializableExtra(Constants.WARD);
                        if (ward == null) return;
                        tvWard.setText(ward.getNameVN());
                        mRegisterFptCameraModel.setBillTo_Ward(ward.getName());
                        //xóa data  street khi chọn lại district
                        tvStreet.setText("");
                        txtFloor.setText("");
                        txtLot.setText("");
                        txtRoom.setText("");
                        tvApartment.setText("");
                        txtHouseNum.setText("");
                        mRegisterFptCameraModel.setBillTo_Number("");
                        mRegisterFptCameraModel.setBillTo_Street("");
                        mRegisterFptCameraModel.setNameVilla("");
                        break;
                    case CODE_HOUSE_TYPE:
                        KeyValuePairModel modelHouseType = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        if (modelHouseType == null) return;
                        tvHouseType.setText(modelHouseType.getDescription());
                        mRegisterFptCameraModel.setTypeHouse(modelHouseType.getID());
                        int id = modelHouseType.getID();
                        switch (id) {
                            case 1:// chọn nhà phố
                                //vùng chứa control Số chung cư
                                apartmentPanel.setVisibility(View.GONE);
                                //vùng chứa control Vị trí nhà
                                housePositionPanel.setVisibility(View.GONE);
                                //vùng chứa control Số nhà
                                houseNumberPanel.setVisibility(View.VISIBLE);
                                // Tầng, Lô, Phòng
                                txtFloor.setText("");
                                txtLot.setText("");
                                txtRoom.setText("");
                                tvApartment.setText("");
                                mRegisterFptCameraModel.setNameVilla("");
                                break;
                            case 2:// cu xa, chung cu, cho, villa, thuong xa
                                apartmentPanel.setVisibility(View.VISIBLE);
                                housePositionPanel.setVisibility(View.GONE);
                                houseNumberPanel.setVisibility(View.GONE);
                                txtHouseNum.setText("");
                                txtHouseNum.setText("");
                                mRegisterFptCameraModel.setBillTo_Number("");
                                break;
                            case 3://Nhà không địa chỉ
                                apartmentPanel.setVisibility(View.GONE);
                                housePositionPanel.setVisibility(View.VISIBLE);
                                houseNumberPanel.setVisibility(View.VISIBLE);
                                txtFloor.setText("");
                                txtLot.setText("");
                                txtRoom.setText("");
                                tvApartment.setText("");
                                mRegisterFptCameraModel.setNameVilla("");
                                break;
                        }
                        break;
                    case CODE_STREET:
                        StreetOrCondo modelStreet = data.getParcelableExtra(Constants.STREET_OR_CONDO);
                        if (modelStreet == null) return;
                        tvStreet.setText(modelStreet.getNameVN());
                        txtFloor.setText("");
                        txtLot.setText("");
                        txtRoom.setText("");
                        tvApartment.setText("");
                        mRegisterFptCameraModel.setBillTo_Street(modelStreet.getId());
                        mRegisterFptCameraModel.setNameVilla("");
                        break;
                    case CODE_CONDO:
                        StreetOrCondo modelCondo = data.getParcelableExtra(Constants.STREET_OR_CONDO);
                        if (modelCondo == null) return;
                        tvApartment.setText(modelCondo.getNameVN());
                        mRegisterFptCameraModel.setNameVilla(modelCondo.getName());
                        break;
                    case CODE_HOUSE_POSITION:
                        KeyValuePairModel modelHousePosition = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        if (modelHousePosition == null) return;
                        tvHousePosition.setText(modelHousePosition.getDescription());
                        mRegisterFptCameraModel.setPosition(modelHousePosition.getID());
                        break;
                    case CODE_OPEN_GALLERY:
                        Uri selectedImageUri = data.getData();
                        String selectedImagePath = Common.getRealPathUrlImage(mContext, selectedImageUri);
                        if (selectedImagePath == null || selectedImagePath.equals("")) {
                            Common.alertDialog(
                                    getResources().getString(R.string.lbl_error_image_document_select_message),
                                    mContext
                            );
                        } else {
                            Intent intent = new Intent(mContext, IdentityCardScanActivity.class);
                            intent.putExtra(Constants.TAG_LOCAL_PATH_IDENTITY_CARD, selectedImagePath);
                            startActivityForResult(intent, CODE_SCAN_IDENTITY);
                        }
                        break;
                    case CODE_SCAN_IDENTITY:
                        // nhận kết quả đường dẫn các ảnh upload thành công
                        if (data.hasExtra(TAG_LIST_IMAGE_DOCUMENT)) {
                            ArrayList<ImageDocument> listImageDocument =
                                    data.getParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT);
                            if (listImageDocument == null) return;
                            setListImageDocument(listImageDocument);
                        }
                        // nhận thông tin quét cmnd
                        if (data.hasExtra(Constants.TAG_INFO_IDENTITY_CARD)) {
                            IdentityCard identityCard = data.getParcelableExtra(Constants.TAG_INFO_IDENTITY_CARD);
                            String pathFileCapture = data.getStringExtra(Constants.TAG_LOCAL_PATH_IDENTITY_CARD);
                            setPathImageDocument(pathFileCapture);
                            autoFieldDataIdentityCard(identityCard);
                        }
                        break;
                    case CODE_CUS_TYPE:
                        KeyValuePairModel modelCusType = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        if (modelCusType == null) return;
                        txtCusType.setText(modelCusType.getDescription());
                        mRegisterFptCameraModel.setCusType(modelCusType.getID());

                        //set lại dịch vụ triển khai mặc định khi chọn lại loại khách hàng
                        activity.setUpdateCusType(true);
                        break;
                    case CODE_CUS_TYPE_DETAIL:
                        KeyValuePairModel modelCusTypeDetail = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        if (modelCusTypeDetail == null) return;
                        txtCusTypeDetail.setText(modelCusTypeDetail.getDescription());
                        mRegisterFptCameraModel.setCusTypeDetail(String.valueOf(modelCusTypeDetail.getID()));
                        break;
                    case CODE_SOURCE_TYPE:
                        KeyValuePairModel modelSourceType = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        if (modelSourceType == null) return;
                        tvSourceType.setText(modelSourceType.getDescription());
                        mRegisterFptCameraModel.setSourceType(modelSourceType.getID());
                }
            } else {
                // file ảnh trả về sau khi chụp ảnh cmnd ở bước 1
                if (requestCode == REQUEST_CAMERA_TAKE_PHOTO) {
                    File fileIdentityCardCapture = fileIdentityCardImageDocument;
                    if (fileIdentityCardCapture != null) {
                        String pathFileCapture = fileIdentityCardCapture.toString();
                        Intent intent = new Intent(mContext, IdentityCardScanActivity.class);
                        intent.putExtra(Constants.TAG_LOCAL_PATH_IDENTITY_CARD, pathFileCapture);
                        startActivityForResult(intent, CODE_SCAN_IDENTITY);
                    }
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = mContext.getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(mContext, message);
                return;
            }
        }
        switch (requestCode) {
            case CODE_REQUEST_TAKE_PHOTO:
                fileIdentityCardImageDocument = Common.capture();
                takePhoto(fileIdentityCardImageDocument);
                break;
            case CODE_REQUEST_OPEN_GALLERY:
                openGallery();
                break;
        }
    }

    // mở file ảnh từ thiết bị
    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), CODE_OPEN_GALLERY);
    }

    private void takePhoto(File tempFile) {
        Uri selectedImageUri = Uri.fromFile(tempFile);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
        startActivityForResult(cameraIntent, REQUEST_CAMERA_TAKE_PHOTO);
    }

    // điền thông tin chứng minh nhân dân tự động
    public void autoFieldDataIdentityCard(IdentityCard identityCard) {
        if (identityCard != null) {
            txtCustomerName.setText(identityCard.getName() == null ? "" : identityCard.getName());
            txtIdentityCard.setText(identityCard.getId() == null ? "" : identityCard.getId());
            txtBirthDay.setText(identityCard.getDob() == null ? "" : formatDateOfBirthRegister(identityCard.getDob()));
            txtAddressId.setText(identityCard.getAddress() == null ? "" : identityCard.getAddress());
        } else {
            Common.alertDialogNotTitle(getResources().getString(R.string.title_empty_data), mContext);
        }
    }

    @Override
    protected void bindData() {
        tvSourceType.setText(getSourceType(mRegisterFptCameraModel.getSourceType()));
        txtCusType.setText(getCusType(mRegisterFptCameraModel.getCusType()));
        txtCusTypeDetail.setText(getCusTypeDetail(Integer.parseInt(
                mRegisterFptCameraModel.getCusTypeDetail() != null ? mRegisterFptCameraModel.getCusTypeDetail() : "0")));
        txtCustomerName.setText(mRegisterFptCameraModel.getFullName() != null ?
                mRegisterFptCameraModel.getFullName() : "");
        txtIdentityCard.setText(mRegisterFptCameraModel.getPassport() != null ?
                mRegisterFptCameraModel.getPassport() : "");
        txtBirthDay.setText(mRegisterFptCameraModel.getBirthday() == null ? "" :
                Common.getSimpleDateFormat(mRegisterFptCameraModel.getBirthday(), Constants.DATE_FORMAT_VN));
        txtAddressId.setText(mRegisterFptCameraModel.getAddressPassport() != null ?
                mRegisterFptCameraModel.getAddressPassport() : "");
        txtTaxNum.setText(mRegisterFptCameraModel.getTaxId() != null ?
                mRegisterFptCameraModel.getTaxId() : "");
        txtEmail.setText(mRegisterFptCameraModel.getEmail() != null ?
                mRegisterFptCameraModel.getEmail() : "");
        if (mRegisterFptCameraModel.getType_1() == 0) {//set default when create registration
            tvPhone1.setText(getPhone(4));
            mRegisterFptCameraModel.setType_1(4);
        } else {
            tvPhone1.setText(getPhone(mRegisterFptCameraModel.getType_1()));
        }

        tvPhone2.setText(getPhone(mRegisterFptCameraModel.getType_2()));
        txtPhone1.setText(mRegisterFptCameraModel.getPhone_1() != null ?
                mRegisterFptCameraModel.getPhone_1() : "");
        txtPhone2.setText(mRegisterFptCameraModel.getPhone_2() != null ?
                mRegisterFptCameraModel.getPhone_2() : "");
        txtContactPhone1.setText(mRegisterFptCameraModel.getContact_1() != null ?
                mRegisterFptCameraModel.getContact_1() : "");
        txtContactPhone2.setText(mRegisterFptCameraModel.getContact_2() != null ?
                mRegisterFptCameraModel.getContact_2() : "");
        tvDistrict.setText(mRegisterFptCameraModel.getBillTo_DistrictVN() != null ?
                mRegisterFptCameraModel.getBillTo_DistrictVN() : "");
        tvWard.setText(mRegisterFptCameraModel.getBillTo_WardVN() != null ?
                mRegisterFptCameraModel.getBillTo_WardVN() : "");
        if (mRegisterFptCameraModel.getTypeHouse() == 0) {//set default when create registration
            tvHouseType.setText(getTypeHouse(1));
            mRegisterFptCameraModel.setTypeHouse(1);
        } else {
            tvHouseType.setText(getTypeHouse(mRegisterFptCameraModel.getTypeHouse()));
            if (mRegisterFptCameraModel.getTypeHouse() == 2) {
                apartmentPanel.setVisibility(View.VISIBLE);
                houseNumberPanel.setVisibility(View.GONE);
                mRegisterFptCameraModel.setBillTo_Number("");
                txtLot.setText(mRegisterFptCameraModel.getLot() != null ?
                        mRegisterFptCameraModel.getLot() : "");
                txtFloor.setText(mRegisterFptCameraModel.getFloor() != null ?
                        mRegisterFptCameraModel.getFloor() : "");
                txtRoom.setText(mRegisterFptCameraModel.getRoom() != null ?
                        mRegisterFptCameraModel.getRoom() : "");
                tvApartment.setText(mRegisterFptCameraModel.getNameVillaDes());
            } else if (mRegisterFptCameraModel.getTypeHouse() == 3) {
                housePositionPanel.setVisibility(View.VISIBLE);
                tvHousePosition.setText(getHousePosition(mRegisterFptCameraModel.getPosition()));
            }
        }
        tvStreet.setText(mRegisterFptCameraModel.getBillTo_StreetVN() != null ?
                mRegisterFptCameraModel.getBillTo_StreetVN() : "");
        txtHouseNum.setText(mRegisterFptCameraModel.getBillTo_Number() != null ?
                mRegisterFptCameraModel.getBillTo_Number() : "");
        txtHouseDesc.setText(mRegisterFptCameraModel.getNote() != null ?
                mRegisterFptCameraModel.getNote() : "");
        txtDescriptionIBB.setText(mRegisterFptCameraModel.getDescriptionIBB() != null ?
                mRegisterFptCameraModel.getDescriptionIBB() : "");
        setListImageDocument(Common.covertStringToListImageDocument(mRegisterFptCameraModel.getImageInfo()));
        checkSaleCombo();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_fpt_camera_cus_info;
    }
    //ver 3.23
    private String getSourceType(int type){
        if (type == 0) {
            return "";
        }
        ArrayList<KeyValuePairModel> lstSourceType = new ArrayList<>();
        lstSourceType.add(new KeyValuePairModel(1, "Nguồn D2D"));
        lstSourceType.add(new KeyValuePairModel(2, "Nguồn Online"));
        for (KeyValuePairModel item : lstSourceType) {
            if (item.getID() == type) {
                return item.getDescription();
            }
        }
        return "";
    }

    private String getPhone(int type) {
        if (type == 0) {
            return "";
        }
        ArrayList<KeyValuePairModel> lstPhone = new ArrayList<>();
        lstPhone.add(new KeyValuePairModel(1, "Cơ quan"));
        lstPhone.add(new KeyValuePairModel(2, "Fax"));
        lstPhone.add(new KeyValuePairModel(3, "Nhà riêng"));
        lstPhone.add(new KeyValuePairModel(4, "Di động"));
        lstPhone.add(new KeyValuePairModel(5, "Nhắn tin"));
        for (KeyValuePairModel item : lstPhone) {
            if (item.getID() == type) {
                return item.getDescription();
            }
        }
        return "";
    }

    private String getCusType(int type) {
        if (type == 0) {
            return "";
        }
        ArrayList<KeyValuePairModel> lst = new ArrayList<>();
        lst.add(new KeyValuePairModel(1, "Cá nhân Việt Nam"));
        lst.add(new KeyValuePairModel(2, "Cá nhân nước ngoài"));
        lst.add(new KeyValuePairModel(3, "Cơ quan Việt Nam"));
        lst.add(new KeyValuePairModel(4, "Cơ quan nước ngoài"));
        lst.add(new KeyValuePairModel(5, "Công ty tư nhân"));
        lst.add(new KeyValuePairModel(6, "Nhân viên FPT"));
        lst.add(new KeyValuePairModel(7, "Nhà nước"));
        lst.add(new KeyValuePairModel(8, "Nước ngoài"));
        lst.add(new KeyValuePairModel(9, "Công ty"));
        lst.add(new KeyValuePairModel(10, "Giáo dục"));
        lst.add(new KeyValuePairModel(11, "Nhân viên FTEL"));
        lst.add(new KeyValuePairModel(12, "Nhân viên tập đoàn FPT"));
        lst.add(new KeyValuePairModel(13, "Nhân viên TIN/PNC"));
        for (KeyValuePairModel item : lst) {
            if (item.getID() == type) {
                return item.getDescription();
            }
        }
        return "";
    }

    private String getCusTypeDetail(int type) {
        ArrayList<KeyValuePairModel> lstObjType = new ArrayList<>();
        lstObjType.add(new KeyValuePairModel(12, "Hộ gia đình"));
        lstObjType.add(new KeyValuePairModel(10, "Công ty"));
        lstObjType.add(new KeyValuePairModel(11, "Học sinh, sinh viên"));
        lstObjType.add(new KeyValuePairModel(13, "Hộ kinh doanh"));
        lstObjType.add(new KeyValuePairModel(14, "Khách hàng nước ngoài"));
        lstObjType.add(new KeyValuePairModel(15, "KXD"));
        lstObjType.add(new KeyValuePairModel(16, "Ký túc xá"));
        lstObjType.add(new KeyValuePairModel(17, "Đại lý Internet"));
        for (KeyValuePairModel item : lstObjType) {
            if (item.getID() == type) {
                return item.getDescription();
            }
        }
        if (type == 0) {
            return lstObjType.get(0).getDescription();
        }
        return "";
    }

    private String getTypeHouse(int typeHouse) {
        if (typeHouse == 0) {
            return "";
        }
        ArrayList<KeyValuePairModel> lstHouseTypes = new ArrayList<>();
        lstHouseTypes.add(new KeyValuePairModel(1, "Nhà phố"));
        lstHouseTypes.add(new KeyValuePairModel(2, "C.Cư, C.xá, Villa, Chợ, Thương Xá"));
        lstHouseTypes.add(new KeyValuePairModel(3, "Nhà không địa chỉ"));
        for (KeyValuePairModel item : lstHouseTypes) {
            if (item.getID() == typeHouse) {
                return item.getDescription();
            }
        }
        return "";
    }

    private String getHousePosition(int id) {
        if (id == 0) {
            return "";
        }
        ArrayList<KeyValuePairModel> lstHousePositions = new ArrayList<>();
        lstHousePositions.add(new KeyValuePairModel(1, "Kế bên phải"));
        lstHousePositions.add(new KeyValuePairModel(2, "Kế bên trái"));
        lstHousePositions.add(new KeyValuePairModel(3, "Đối diện"));
        lstHousePositions.add(new KeyValuePairModel(4, "Phía sau"));
        lstHousePositions.add(new KeyValuePairModel(5, "Cách"));
        for (KeyValuePairModel item : lstHousePositions) {
            if (item.getID() == id) {
                return item.getDescription();
            }
        }
        return "";
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar date = Calendar.getInstance();
        date.set(year, month, dayOfMonth);
        if (mDateDialog.getEditText() == txtBirthDay && txtBirthDay != null) {
            myCalendar = date;
            txtBirthDay.setText(Common.getSimpleDateFormat(myCalendar, Constants.DATE_FORMAT_VN));
        }
    }
}
