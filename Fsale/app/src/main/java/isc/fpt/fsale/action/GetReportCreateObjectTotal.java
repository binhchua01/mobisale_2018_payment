package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Bao cao HĐ tự động
 * @author: 		DuHK
 * @create date: 	15/03/2016
 * */
import isc.fpt.fsale.activity.ListReportCreateObjectTotalActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportCreateObjectTotalModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class GetReportCreateObjectTotal implements AsyncTaskCompleteListener<String> {

	public static final String METHOD_NAME = "ReportCreateObjectTotal";
	public static final String[] paramNames = {"UserName", "FromDate", "ToDate", "Status", "Agent", "AgentName", "PageNumber", "Dept"};
	private Context mContext;	
	private String fromDate, toDate;
	private int status;	
	
	public GetReportCreateObjectTotal(Context mContext, String UserName, String FromDate, String ToDate, int Status, int Agent, String AgentName, int PageNumber, int Dept){
		this.mContext = mContext;	
		this.fromDate = FromDate;
		this.toDate = ToDate;
		this.status = Status;
		
		String[] paramValues = {UserName, FromDate, ToDate, String.valueOf(Status), String.valueOf(Agent), AgentName, String.valueOf(PageNumber), String.valueOf(Dept)};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportCreateObjectTotal.this);
		service.execute();	
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<ReportCreateObjectTotalModel> lst = null;
			boolean isError = false;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ReportCreateObjectTotalModel> resultObject = new WSObjectsModel<ReportCreateObjectTotalModel>(jsObj, ReportCreateObjectTotalModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();								
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			e.printStackTrace();
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(List<ReportCreateObjectTotalModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListReportCreateObjectTotalActivity.class.getSimpleName())){
				ListReportCreateObjectTotalActivity activity = (ListReportCreateObjectTotalActivity)mContext;
				activity.LoadData(lst, this.fromDate, this.toDate, this.status);
			}
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
	}
	

}
