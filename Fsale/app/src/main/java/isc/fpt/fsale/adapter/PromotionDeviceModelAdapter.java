package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.Mac;
import isc.fpt.fsale.model.PromotionDeviceModel;
import isc.fpt.fsale.model.PromotionFPTBoxModel;

/**
 * Created by HCM.TUANTT14 on 1/18/2018.
 */

public class PromotionDeviceModelAdapter extends ArrayAdapter<PromotionDeviceModel> {
    private ArrayList<PromotionDeviceModel> lstObj;
    private Context mContext;
    private boolean hasInitText = false;
    private int iColor = 2;
    private Typeface tf;

    public PromotionDeviceModelAdapter(Context context, int textViewResourceId, ArrayList<PromotionDeviceModel> lstObj) {
        super(context, textViewResourceId);
        this.mContext = context;
        this.lstObj = lstObj;
        this.tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    public ArrayList<PromotionDeviceModel> getList(){
        return lstObj;
    }
    public int getCount(){
        if(lstObj != null)
            return lstObj.size();
        return 0;
    }

    public PromotionDeviceModel getItem(int position){
        if(lstObj != null)
            return lstObj.get(position);
        return null;
    }

    public long getItemId(int position){
        return position;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(mContext);
        if(hasInitText && position == 0){
            label.setVisibility(View.GONE);
        }
        else{
            label.setText(lstObj.get(position).getPromotionName());
            label.setTypeface(tf);
            int padding = (int) mContext.getResources().getDimension(R.dimen.padding_medium);
            label.setPadding(padding, padding, padding, padding);
        }
        return label;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setTypeface(tf);
        label.setText(lstObj.get(position).getPromotionName());
        if(this.iColor != 2)
            label.setTextColor(ColorStateList.valueOf(this.iColor));
        return label;
    }
}
