package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.SBIManageDialog;
import isc.fpt.fsale.model.SBIModel;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SBIAdapter  extends BaseAdapter{
	private ArrayList<SBIModel> mList;	
	private Context mContext;
	@SuppressWarnings("unused")
	private SBIManageDialog sbiDialog = null;
	
	public SBIAdapter(Context context, ArrayList<SBIModel> lst){
		this.mContext = context;
		this.mList = lst;	
	}
	
	public SBIAdapter(Context context, ArrayList<SBIModel> lst, SBIManageDialog dialog){
		this.mContext = context;
		this.mList = lst;	
		this.sbiDialog = dialog;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		SBIModel item = mList.get(position);		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_sbi, null);
		}
		
		TextView  lblSBINumber = (TextView)convertView.findViewById(R.id.lbl_sbi_number);
		lblSBINumber.setText(item.getSBI());
		TextView  lblSBITypeName = (TextView)convertView.findViewById(R.id.lbl_sbi_type_name);
		if(item.getType() == 1)//ADSL
			lblSBITypeName.setText("ADSL");
		if(item.getType() == 2)
			lblSBITypeName.setText("FTTH");
		if(item.getType() == 8)
			lblSBITypeName.setText("IPTV");
		//lblSBITypeName.setText(item.getStatusName());
		ImageView imgRequestToCancelSBI = (ImageView)convertView.findViewById(R.id.img_btn_request_cancel_sbi);
		 /* 
		 Status:
		 0: Hết hạn sử dụng
		 1: Còn hạn sử dụng
		 2: Đã sử dụng
		 3: Hủy
	    */
		/*if(item.getStatus() != 1)//Chi duoc phep huy cac SBI con han sd*/
		imgRequestToCancelSBI.setVisibility(View.GONE);
		/*final String sbi = item.getSBI();*/
		/*imgRequestToCancelSBI.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = null;
				Dialog dialog = null;
				builder = new AlertDialog.Builder(mContext);
				builder.setMessage("Bạn có muốn hủy SBI").setCancelable(false).setPositiveButton("Có",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {		
	  					new RequestToCancelSBI(mContext, sbi, sbiDialog);
					}
				}).setNegativeButton("Không",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int id) {
						dialog.cancel();
					}
				});
				dialog = builder.create();
				dialog.show();
			
			}
		});*/
		
		return convertView;
	}

}
