package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Report Potential Obj Total
 * @author: 		DuHK
 * @create date: 	16/02/2016
 * */
import isc.fpt.fsale.activity.ListReportPotentialObjTotalActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportPotentialObjTotalModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetReportPotentialObjTotal implements AsyncTaskCompleteListener<String> {

	private final String METHOD_NAME = "ReportPotentialObjTotal";
	private Context mContext;	
	private String fromDate, toDate;
	private int status;
	
	//Add by: DuHK
	public GetReportPotentialObjTotal(Context mContext, String UserName, int Status, int Day, int Month, int Year, int Agent, String AgentName, int PageNumber){
		this.mContext = mContext;	
		
		String[] paramNames = {"UserName", "Status", "Day", "Month", "Year", "Agent", "AgentName", "PageNumber"};
		String[] paramValues = {UserName, String.valueOf(Status), String.valueOf(Day), String.valueOf(Month), String.valueOf(Year), String.valueOf(Agent), AgentName, String.valueOf(PageNumber)};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportPotentialObjTotal.this);
		service.execute();	
	}
	
	public GetReportPotentialObjTotal(Context mContext, String UserName, String FromDate, String ToDate, int Status, int Agent, String AgentName, int PageNumber, int Dept){
		this.mContext = mContext;	
		this.fromDate = FromDate;
		this.toDate = ToDate;
		this.status = Status;
		String[] paramNames = {"UserName", "FromDate", "ToDate", "Status", "Agent", "AgentName", "PageNumber", "Dept"};
		String[] paramValues = {UserName, FromDate, ToDate, String.valueOf(Status), String.valueOf(Agent), AgentName, String.valueOf(PageNumber), String.valueOf(Dept)};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportPotentialObjTotal.this);
		service.execute();	
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<ReportPotentialObjTotalModel> lst = null;
			boolean isError = false;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ReportPotentialObjTotalModel> resultObject = new WSObjectsModel<ReportPotentialObjTotalModel>(jsObj, ReportPotentialObjTotalModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();								
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(List<ReportPotentialObjTotalModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListReportPotentialObjTotalActivity.class.getSimpleName())){
				ListReportPotentialObjTotalActivity activity = (ListReportPotentialObjTotalActivity)mContext;
				activity.LoadData(lst, this.fromDate, this.toDate, this.status);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("ReportSBIDetail.loadRpCodeInfo()", e.getMessage());
		}
	}
	

}
