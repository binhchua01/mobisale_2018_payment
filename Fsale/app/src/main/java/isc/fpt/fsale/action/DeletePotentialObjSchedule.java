package isc.fpt.fsale.action;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PotentialObjScheduleList;
import isc.fpt.fsale.model.PotentialSchedule;
import isc.fpt.fsale.services.AlarmReceiver;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 10/11/2017.
 * API xóa lịch hẹn KHTN
 */

public class DeletePotentialObjSchedule implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private PotentialObjScheduleList potentialObjScheduleList;
    private int positionRemoved, potentialObjScheduleID;

    public DeletePotentialObjSchedule(Context mContext, int potentialObjScheduleID, int positionRemoved) {
        this.mContext = mContext;
        this.potentialObjScheduleList = (PotentialObjScheduleList) mContext;
        this.positionRemoved = positionRemoved;
        this.potentialObjScheduleID = potentialObjScheduleID;
        String[] arrParamName = new String[]{"PotentialObjScheduleID"};
        String[] arrParamValue = new String[]{String.valueOf(potentialObjScheduleID)};
        String message = mContext.getResources().getString(R.string.msg_pd_delete_potential_schedule);
        String UPDATE_POTENTIAL_OBJ_SCHEDULE = "DeletePotentialObjSchedule";
        CallServiceTask service = new CallServiceTask(mContext, UPDATE_POTENTIAL_OBJ_SCHEDULE,
                arrParamName, arrParamValue, Services.JSON_POST, message, DeletePotentialObjSchedule.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        if (Common.jsonObjectValidate(result)) {
            JSONObject jsObj = JSONParsing.getJsonObj(result);
            if (Common.isEmptyJSONObject(jsObj))
                return;
            try {
                String TAG_RESULT = "ResponseResult";
                JSONObject obj = jsObj.getJSONObject(TAG_RESULT);
                String TAG_ERROR_CODE = "ErrorCode";
                int error = obj.getInt(TAG_ERROR_CODE);
                if (error >= 0) {
                    List<PotentialSchedule> listPotentialSchedule = potentialObjScheduleList.getListPotentialSchedule();
                    AlarmManager alarmManager = ((MyApp) mContext.getApplicationContext()).getAlarmManager();
                    listPotentialSchedule.remove(positionRemoved);
                    potentialObjScheduleList.getLblTotalSchedule().setText(String.valueOf(listPotentialSchedule.size()));
                    potentialObjScheduleList.getAdapterPotentialSchedule().notifyDataSetChanged();
                    if (listPotentialSchedule.size() == 0) {
                        potentialObjScheduleList.setNumberPage(listPotentialSchedule);
                    }
                    Intent intent = new Intent(mContext, AlarmReceiver.class);
                    PendingIntent sender = PendingIntent.getBroadcast(
                            mContext, potentialObjScheduleID, intent,
                            PendingIntent.FLAG_CANCEL_CURRENT);
                    alarmManager.cancel(sender);
                    String TAG_ERROR = "Error";
                    Common.alertDialog(obj.getString(TAG_ERROR), mContext);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Common.alertDialog(mContext.getResources().getString(R.string.lbl_return_result_potential_schedule_list_fail), mContext);
            }
        } else {
            Common.alertDialog(mContext.getResources().getString(R.string.lbl_return_result_potential_schedule_list_fail), mContext);
        }
    }
}
