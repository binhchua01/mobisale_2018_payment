package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.UpdateInternetReceiptActivity;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.model.DepositValueModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

/*
 * @Description: Get list value contract deposit
 * @author: DuHK
 * @create date: 	06/08/2015
 * API lấy danh sách đặt cọc thuê bao
 */

public class GetContractDepositList implements AsyncTaskCompleteListener<String> {

    private final String METHOD_NAME = "GetContractDepositList";
    private Context mContext;
    private FragmentRegisterStep4 fragmentRegisterStep4;

    public GetContractDepositList(Context mContext, String UserName, String RegCode) {
        this.mContext = mContext;
        String[] paramNames = {"UserName", "RegCode"};
        String[] paramValues = {UserName, RegCode};
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, GetContractDepositList.this);
        service.execute();
    }

    public GetContractDepositList(Context mContext, FragmentRegisterStep4 fragment, String UserName, String RegCode) {
        this.mContext = mContext;
        this.fragmentRegisterStep4 = fragment;
        String[] paramNames = {"UserName", "RegCode"};
        String[] paramValues = {UserName, RegCode};
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, GetContractDepositList.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            List<DepositValueModel> lst = null;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<DepositValueModel> resultObject = new WSObjectsModel<>(jsObj, DepositValueModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                }
                loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("LongLogTag")
    private void loadData(List<DepositValueModel> lst) {
        try {
            if (lst == null || lst.size() <= 0) {
                lst = new ArrayList<>();
                lst.add(new DepositValueModel("0", 0));
                lst.add(new DepositValueModel("330,000", 330000));
                lst.add(new DepositValueModel("660,000", 660000));
                lst.add(new DepositValueModel("1,100,000", 1100000));
                lst.add(new DepositValueModel("2,200,000", 2200000));
            }
            if (mContext.getClass().getSimpleName().equals(UpdateInternetReceiptActivity.class.getSimpleName())) {
                UpdateInternetReceiptActivity activity = (UpdateInternetReceiptActivity) mContext;
                activity.loadDepositContract(lst);
            } else if (fragmentRegisterStep4 != null) {
                fragmentRegisterStep4.updateSpContractDeposit(lst);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
