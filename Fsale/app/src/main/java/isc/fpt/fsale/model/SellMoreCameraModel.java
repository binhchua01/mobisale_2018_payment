package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 21,January,2019
 */
public class SellMoreCameraModel implements Parcelable {
    private String UserName;
    private String RegCode;
    private String Contract;
    private String Email;
    private String Phone_1;
    private int Type_1;
    private String Contact_1;
    private int Payment;
    private int LocalType;
    private String LocationID;
    private ObjectCamera objectCamera;
    private List<Device> device;
    private String cameraTotal;
    private int Total;
    private int ObjID;
    private int CusType;
    private int BaseObjID;
    private String BaseContract;
    private String Supporter;
    private double DeviceTotal;


    public SellMoreCameraModel() {
    }

    public SellMoreCameraModel(String userName, String regCode, String contract, String email, String phone_1, int type_1,
                               String contact_1, int payment, int localType, String locationID, ObjectCamera objectCamera,
                               List<Device> device, String cameraTotal, int total, int objID, int cusType, int baseObjID,
                               String baseContract, String supporter, double deviceTotal) {
        UserName = userName;
        RegCode = regCode;
        Contract = contract;
        Email = email;
        Phone_1 = phone_1;
        Type_1 = type_1;
        Contact_1 = contact_1;
        Payment = payment;
        LocalType = localType;
        LocationID = locationID;
        this.objectCamera = objectCamera;
        this.device = device;
        this.cameraTotal = cameraTotal;
        Total = total;
        ObjID = objID;
        CusType = cusType;
        BaseObjID = baseObjID;
        BaseContract = baseContract;
        Supporter = supporter;
        DeviceTotal = deviceTotal;
    }

    protected SellMoreCameraModel(Parcel in) {
        UserName = in.readString();
        RegCode = in.readString();
        Contract = in.readString();
        Email = in.readString();
        Phone_1 = in.readString();
        Type_1 = in.readInt();
        Contact_1 = in.readString();
        Payment = in.readInt();
        LocalType = in.readInt();
        LocationID = in.readString();
        objectCamera = in.readParcelable(ObjectCamera.class.getClassLoader());
        device = in.createTypedArrayList(Device.CREATOR);
        cameraTotal = in.readString();
        Total = in.readInt();
        ObjID = in.readInt();
        CusType = in.readInt();
        BaseObjID = in.readInt();
        BaseContract = in.readString();
        Supporter = in.readString();
        DeviceTotal = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(UserName);
        dest.writeString(RegCode);
        dest.writeString(Contract);
        dest.writeString(Email);
        dest.writeString(Phone_1);
        dest.writeInt(Type_1);
        dest.writeString(Contact_1);
        dest.writeInt(Payment);
        dest.writeInt(LocalType);
        dest.writeString(LocationID);
        dest.writeParcelable(objectCamera, flags);
        dest.writeTypedList(device);
        dest.writeString(cameraTotal);
        dest.writeInt(Total);
        dest.writeInt(ObjID);
        dest.writeInt(CusType);
        dest.writeInt(BaseObjID);
        dest.writeString(BaseContract);
        dest.writeString(Supporter);
        dest.writeDouble(DeviceTotal);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SellMoreCameraModel> CREATOR = new Creator<SellMoreCameraModel>() {
        @Override
        public SellMoreCameraModel createFromParcel(Parcel in) {
            return new SellMoreCameraModel(in);
        }

        @Override
        public SellMoreCameraModel[] newArray(int size) {
            return new SellMoreCameraModel[size];
        }
    };

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getRegCode() {
        return RegCode;
    }

    public void setRegCode(String regCode) {
        RegCode = regCode;
    }

    public String getContract() {
        return Contract;
    }

    public void setContract(String contract) {
        Contract = contract;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone_1() {
        return Phone_1;
    }

    public void setPhone_1(String phone_1) {
        Phone_1 = phone_1;
    }

    public int getType_1() {
        return Type_1;
    }

    public void setType_1(int type_1) {
        Type_1 = type_1;
    }

    public String getContact_1() {
        return Contact_1;
    }

    public void setContact_1(String contact_1) {
        Contact_1 = contact_1;
    }

    public int getPayment() {
        return Payment;
    }

    public void setPayment(int payment) {
        Payment = payment;
    }

    public int getLocalType() {
        return LocalType;
    }

    public void setLocalType(int localType) {
        LocalType = localType;
    }

    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String locationID) {
        LocationID = locationID;
    }

    public ObjectCamera getObjectCamera() {
        return objectCamera;
    }

    public void setObjectCamera(ObjectCamera objectCamera) {
        this.objectCamera = objectCamera;
    }

    public List<Device> getDevice() {
        return device;
    }

    public void setDevice(List<Device> device) {
        this.device = device;
    }

    public String getCameraTotal() {
        return cameraTotal;
    }

    public void setCameraTotal(String cameraTotal) {
        this.cameraTotal = cameraTotal;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

    public int getObjID() {
        return ObjID;
    }

    public void setObjID(int objID) {
        ObjID = objID;
    }

    public int getCusType() {
        return CusType;
    }

    public void setCusType(int cusType) {
        CusType = cusType;
    }

    public int getBaseObjID() {
        return BaseObjID;
    }

    public void setBaseObjID(int baseObjID) {
        BaseObjID = baseObjID;
    }

    public String getBaseContract() {
        return BaseContract;
    }

    public void setBaseContract(String baseContract) {
        BaseContract = baseContract;
    }

    public String getSupporter() {
        return Supporter;
    }

    public void setSupporter(String supporter) {
        Supporter = supporter;
    }

    public double getDeviceTotal() {
        return DeviceTotal;
    }

    public void setDeviceTotal(double deviceTotal) {
        DeviceTotal = deviceTotal;
    }

    // Convert Object to Json bán thêm
    public JSONObject toJsonObject() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("UserName", this.getUserName());
            jsonObj.put("RegCode", this.getRegCode());
            jsonObj.put("Contract", this.getContract());
            jsonObj.put("Email", this.getEmail());
            jsonObj.put("Phone_1", this.getPhone_1());
            jsonObj.put("Type_1", this.getType_1());
            jsonObj.put("Contact_1", this.getContact_1());
            jsonObj.put("Payment", this.getPayment());
            jsonObj.put("LocalType", this.getLocalType());
            jsonObj.put("LocationID", this.getLocationID());
            jsonObj.put("cameraServices", this.getObjectCamera().toJsonObject());
            jsonObj.put("cameraTotal", this.getCameraTotal());
            jsonObj.put("Total", this.getTotal());
            jsonObj.put("ObjID", this.getObjID());
            jsonObj.put("Supporter", this.getSupporter());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    public JSONObject toJsonObjectV2() {

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("UserName", this.getUserName());
            jsonObj.put("RegCode", this.getRegCode());
            jsonObj.put("Contract", this.getContract());
            jsonObj.put("Email", this.getEmail());
            jsonObj.put("Phone_1", this.getPhone_1());
            jsonObj.put("Type_1", this.getType_1());
            jsonObj.put("Contact_1", this.getContact_1());
            jsonObj.put("Payment", this.getPayment());
            jsonObj.put("LocalType", this.getLocalType());
            jsonObj.put("LocationID", this.getLocationID());
            jsonObj.put("cameraServices", this.getObjectCamera().toJsonObject());
            jsonObj.put("cameraTotal", this.getCameraTotal());
            jsonObj.put("Total", this.getTotal());
            jsonObj.put("ObjID", this.getObjID());
            jsonObj.put("Supporter", this.getSupporter());
            JSONArray jsonArrayDevice = new JSONArray();
            for (Device item : getDevice()) {
                jsonArrayDevice.put(item.toJSONObjectRegisterV2());
            }
            jsonObj.put("ListDevice", jsonArrayDevice);
            jsonObj.put("DeviceTotal", this.getDeviceTotal());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }


    // Convert Object to Json lấy giá set up camera
    public JSONObject toJsonObjectCostSetupCamera() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("Combo", getBaseObjID() == 0 && TextUtils.isEmpty(getBaseContract()) ? 0 : 1);
            jsonObject.put("RegType", 1);
            jsonObject.put("Contract", getContract());
            jsonObject.put("CusType", getCusType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
