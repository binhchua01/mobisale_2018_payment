package isc.fpt.fsale.callback;

import android.os.Parcelable;

public interface RegisterCallback extends Parcelable {
	void updateOnclick();
	void changePage(int pos);
}
