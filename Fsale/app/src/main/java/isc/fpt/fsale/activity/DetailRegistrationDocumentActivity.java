package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ImageDocument;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//import static isc.fpt.fsale.ui.fragment.FragmentRegisterStep1.TAG_LIST_IMAGE_DOCUMENT_SELECTED;
//import static isc.fpt.fsale.ui.fragment.FragmentRegisterStep1.TAG_UPDATE_REGISTRATION;
//import static isc.fpt.fsale.ui.fragment.FragmentRegisterStep1.TAG_VIEW_SIGNATURE;
import static isc.fpt.fsale.utils.Common.showScreenActionBar;
import static isc.fpt.fsale.utils.Constants.TAG_LIST_IMAGE_DOCUMENT;
import static isc.fpt.fsale.utils.Constants.TAG_LIST_IMAGE_DOCUMENT_SELECTED;
import static isc.fpt.fsale.utils.Constants.TAG_UPDATE_REGISTRATION;
import static isc.fpt.fsale.utils.Constants.TAG_VIEW_SIGNATURE;
import static isc.fpt.fsale.utils.Constants.TOKEN;

//màn hình chi tiết ảnh hồ sơ thông tin khách hàng
public class DetailRegistrationDocumentActivity extends AppCompatActivity {
    private LinearLayout frmContainerListImageDocument;
    private View frmListImageDocument;
    private TextView tvEmptyImageDocument;
    private ArrayList<ImageDocument> listImageDocument = new ArrayList<>();
    private ImageView ibMainImageDocument;
    private Context context;
    private boolean updateRegistration, statusViewSignature;
    //tên API download hình ảnh
    private LinearLayout frmContainerImageDocument;
    private final String methodDownload = "Download";
    private int currentPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_registration_document);
        //hiển thị actionbar
        showScreenActionBar(this);
        this.context = this;
        initView();
        getDataFromIntent();
    }

    // khởi tạo wiget
    private void initView() {
        frmListImageDocument = findViewById(R.id.frm_list_image_document);
        frmContainerListImageDocument = (LinearLayout) findViewById(R.id.frm_container_list_image_document);
        tvEmptyImageDocument = (TextView) findViewById(R.id.tv_empty_image_document);
        ibMainImageDocument = (ImageView) findViewById(R.id.ib_main_image_document);
    }

    // lấy data intent
    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(TAG_VIEW_SIGNATURE))
                this.statusViewSignature = intent.getBooleanExtra(TAG_VIEW_SIGNATURE, false);
            if (intent.hasExtra(TAG_LIST_IMAGE_DOCUMENT_SELECTED)) {
                listImageDocument = intent.getParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT_SELECTED);
                updateRegistration = intent.getBooleanExtra(TAG_UPDATE_REGISTRATION, false);
                if (updateRegistration && listImageDocument.size() > 0) {
                    frmListImageDocument.setVisibility(View.VISIBLE);
                    tvEmptyImageDocument.setVisibility(View.GONE);
                    frmContainerImageDocument = new LinearLayout(this);
                    frmContainerImageDocument.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    if (listImageDocument.get(0).getPathCache() != null || listImageDocument.get(0).getPath() != null) {
                        for (ImageDocument imageDocument : listImageDocument) {
                            generatorListImageDocumentFrame(imageDocument);
                        }
                    } else {
                        String[] param = new String[]{"Id", "Token"};
                        String[] paramValues = new String[2];
                        paramValues[1] = TOKEN;
                        ExecutorService executorService = Executors.newFixedThreadPool(listImageDocument.size());
                        for (int index = 0; index < listImageDocument.size(); index++) {
                            String message = statusViewSignature ? context.getResources().getString(R.string.lbl_process_load_signature_image_document_message) : context.getResources().getString(R.string.lbl_process_load_image_document_message);
                            final ProgressDialog pd = Common.showProgressBar(context, message);
                            newThreadDownloadFile(executorService, index, param, paramValues, pd);
                        }
                    }
                } else {
                    frmListImageDocument.setVisibility(View.GONE);
                    tvEmptyImageDocument.setVisibility(View.VISIBLE);
                }
                if (statusViewSignature) {
                    setTitle(getResources().getString(R.string.lbl_customer_signature));
                }
            }
        }
    }

    private void newThreadDownloadFile(ExecutorService executorService, final int index, final String[] param, final String[] paramValues, final ProgressDialog pd) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                downloadFileImageDocument(methodDownload, listImageDocument.get(index), param, paramValues, pd);
            }
        });
    }

    // tạo giao diện danh sách ảnh hồ sơ
    public void generatorListImageDocumentFrame(final ImageDocument imageDocument) {
        int padding = (int) getResources().getDimension(R.dimen.padding_small);
        frmContainerImageDocument.setPadding(padding, padding, padding, padding);
        final ImageButton ibImageDocument = new ImageButton(this);
        int width = (int) getResources().getDimension(R.dimen.width_image_document);
        int height = (int) getResources().getDimension(R.dimen.heigth_image_document);
        LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(width, height);
        buttonLayoutParams.setMargins(padding, 0, padding, 0);
        ibImageDocument.setLayoutParams(buttonLayoutParams);
        final String pathCache = imageDocument.getPathCache();
        final String path = imageDocument.getPath();
        ibImageDocument.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (path != null) {
//                    Picasso.with(context).load(new File(path)).fit().into(ibMainImageDocument);
                    Picasso.get().load(new File(path)).into(ibMainImageDocument);
                } else if (pathCache != null) {
//                    Picasso.with(context).load(new File(pathCache)).fit().into(ibMainImageDocument);
                    Picasso.get().load(new File(pathCache)).into(ibMainImageDocument);
                } else {
                    Picasso.get().load(R.drawable.error404).fit().centerInside().into(ibMainImageDocument);
                }
            }
        });
        ibImageDocument.setBackgroundColor(Color.TRANSPARENT);
        if (path != null) {
            Picasso.get().load(new File(path)).fit().into(ibImageDocument);
//            Picasso.with(context).load(new File(path)).into(ibImageDocument);
        } else if (pathCache != null) {
            Picasso.get().load(new File(pathCache)).fit().into(ibImageDocument);
//            Picasso.with(context).load(new File(pathCache)).into(ibImageDocument);
        } else {
            Picasso.get().load(R.drawable.error404).fit().centerInside().into(ibImageDocument);
        }
        if (currentPosition == 0) {
            if (path != null) {
//                Picasso.with(context).load(new File(path)).fit().into(ibMainImageDocument);
                Picasso.get().load(new File(path)).into(ibMainImageDocument);
            } else if (pathCache != null) {
//                Picasso.with(context).load(new File(pathCache)).fit().into(ibMainImageDocument);
                Picasso.get().load(new File(pathCache)).into(ibMainImageDocument);
            } else {
                Picasso.get().load(R.drawable.error404).fit().centerInside().into(ibMainImageDocument);
            }
            frmContainerListImageDocument.addView(frmContainerImageDocument);
            currentPosition++;
        }
        frmContainerImageDocument.addView(ibImageDocument);
    }

    // menu nút back để thoát màn hình
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            sendIntentBack();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // khởi tạo đối tượng tham chiếu nhận code KHTN
    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    public void onBackPressed() {
        sendIntentBack();
    }

    // truyền dữ liệu sang màn hình tạo TTKH hoặc chi tiết TTKH
    private void sendIntentBack() {
        Intent intent = new Intent();
        if (statusViewSignature) {
            intent.putExtra(TAG_VIEW_SIGNATURE, statusViewSignature);
        } else if (updateRegistration) {
            intent.putExtra(TAG_UPDATE_REGISTRATION, updateRegistration);
        }
        intent.putExtra(TAG_LIST_IMAGE_DOCUMENT, listImageDocument);
        setResult(RESULT_OK, intent);
        finish();
    }

    // Kết nối API tải danh sách ảnh sau khi đã upload
    @SuppressLint("StaticFieldLeak")
    private void downloadFileImageDocument(final String methodName, final ImageDocument imageDocument,
                                           final String[] params, final String[] paramsValue, final ProgressDialog pd) {
        new AsyncTask<Object, String, ImageDocument>() {
            @Override
            protected void onPostExecute(ImageDocument imageDocument) {
                generatorListImageDocumentFrame(imageDocument);
                if (pd != null && pd.isShowing())
                    pd.dismiss();
            }

            @Override
            protected ImageDocument doInBackground(Object... object) {
                try {
                    // kết nối API load ảnh
                    paramsValue[0] = String.valueOf(imageDocument.getID());
                    // Kết nối API load ảnh
                    imageDocument.setPathCache(Common.getPathCacheImageFile(context, Services.getBitmapImageDocument(methodName, params, paramsValue)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return imageDocument;
            }
        }.execute(null, null, null);
    }
}