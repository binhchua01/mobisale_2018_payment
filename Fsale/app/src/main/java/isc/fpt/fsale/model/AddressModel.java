package isc.fpt.fsale.model;

import isc.fpt.fsale.utils.Common;

/**
 * @MODEL 			AddressModel
 * @author			vandn
 * @created on: 	05/12/2013
 * */
public class AddressModel {
	
	private String bill_to_city;
	public String getBillToCity() {
		return bill_to_city;
	}
	public void setBillToCity(String billToCity) {
		this.bill_to_city = Common.checkIsEmpty(billToCity);
	}
	public String getInstallAtCity() {
		return install_at_city;
	}
	public void setInstallAtCity(String installAtCity) {
		this.install_at_city = Common.checkIsEmpty(installAtCity);
	}
	private String install_at_city;
	private String wards;
	private int house_type;
	private String street_name;
	private String house_num;
	
	//house type: C.cu, C.Xa
	private String floor;
	private String group;
	private String room;
	private String apparment_name;
	
	//house type: nha khong dia chi
	private String house_position;
	private String house_desc;
	
	public String getHouse_desc() {
		return house_desc;
	}
	public void setHouse_desc(String house_desc) {
		this.house_desc = Common.checkIsEmpty(house_desc);
	}
	public AddressModel(){
		this.district = "-1";
		this.wards = "-1";
		this.street_name = "-1";
		this.house_num = "-1";
		this.floor = "-1";
		this.group = "-1";
		this.room = "-1";
		this.apparment_name = "-1";
		this.house_position = "-1";
		this.house_desc = "-1";
		this.bill_to_city = "-1";
		this.install_at_city = "-1";
	}
	public String getApparmentName() {
		return apparment_name;
	}
	public void setApparmentName(String apparmentName) {
		this.apparment_name = Common.checkIsEmpty(apparmentName);	
	}	
	private String district;
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getWards() {
		return wards;
	}
	public void setWards(String wards) {
		this.wards = wards;
	}
	public int getHouse_type() {
		return house_type;
	}
	public void setHouse_type(int house_type) {
		this.house_type = house_type;
	}
	public String getStreet_name() {
		return street_name;
	}
	public void setStreet_name(String street_name) {
		this.street_name = street_name;
	}
	public String getHouse_num() {
		return house_num;
	}
	public void setHouse_num(String house_num) {
		this.house_num = Common.checkIsEmpty(house_num);	
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = Common.checkIsEmpty(floor);	
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = Common.checkIsEmpty(group);	
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = Common.checkIsEmpty(room);		
	}
	public String getHouse_position() {
		return house_position;
	}
	public void setHouse_position(String house_position) {
		this.house_position = house_position;
	}
	
	
}
