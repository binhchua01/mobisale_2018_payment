package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.AutoCreatePreCheckListActivity;
import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.model.TimeZone;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by HCM.TUANTT14 on 7/4/2018.
 */

public class AutoPreCheckListGetTimezone implements AsyncTaskCompleteListener<String> {
    private String[] arrParamName;
    private Context mContext;
    private final String AUTO_PRE_CHECK_LIST_GET_TIMEZONE = "AutoPreCheckList_GetTimezone";
    private CreatePreChecklistActivity createPreChecklistActivity;
    private AutoCreatePreCheckListActivity autoCreatePreCheckListActivity;

    public AutoPreCheckListGetTimezone(Context context,
                                       CreatePreChecklistActivity activity, String[] paramsValue) {
        this.mContext = context;
        this.createPreChecklistActivity = activity;
        this.arrParamName = new String[]{"iInit_Status", "Supporter", "SubID", "AppointmentDate", "UserName", "ObjID"};
        String message = mContext.getResources().getString(R.string.msg_process_auto_create_pre_checklist_get_time_zone);
        CallServiceTask service = new CallServiceTask(mContext, AUTO_PRE_CHECK_LIST_GET_TIMEZONE,
                arrParamName, paramsValue, Services.JSON_POST, message, AutoPreCheckListGetTimezone.this);
        service.execute();
    }

    public AutoPreCheckListGetTimezone(Context context,
                                       AutoCreatePreCheckListActivity activity, String[] paramsValue) {
        this.mContext = context;
        this.autoCreatePreCheckListActivity = activity;
        this.arrParamName = new String[]{"iInit_Status", "Supporter", "SubID", "AppointmentDate", "UserName", "ObjID"};
        String message = mContext.getResources().getString(R.string.msg_process_auto_create_pre_checklist_get_time_zone);
        CallServiceTask service = new CallServiceTask(mContext, AUTO_PRE_CHECK_LIST_GET_TIMEZONE,
                arrParamName, paramsValue, Services.JSON_POST, message, AutoPreCheckListGetTimezone.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<TimeZone> lstTimeZone = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                String TAG_RESULT = "Result";
                String TAG_RESULT_ID = "ResultID";
                if (jsObj.has(TAG_RESULT_ID) && jsObj.getInt(TAG_RESULT_ID) < 0) {
                    Common.alertDialog(jsObj.getString(TAG_RESULT), mContext);
                } else {
                    if (jsObj.has(TAG_RESULT_ID) && jsObj.getInt(TAG_RESULT_ID) > 0) {
                        JSONArray arr = jsObj.getJSONArray("ListObject");
                        for (int i = 0; i < arr.length(); i++) {
                            lstTimeZone.add(new Gson().fromJson(String.valueOf(arr.get(i)), TimeZone.class));
                        }
                        if (lstTimeZone.size() > 0) {
                            if (createPreChecklistActivity != null) {
                                createPreChecklistActivity.loadTimeZone(lstTimeZone);
                            }

                            if (autoCreatePreCheckListActivity != null) {
                                autoCreatePreCheckListActivity.loadTimeZone(lstTimeZone);
                            }
                        } else {
                            Common.alertDialog(jsObj.getString(TAG_RESULT), mContext);
                        }
                    } else {
                        Common.alertDialog(jsObj.getString(TAG_RESULT), mContext);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}