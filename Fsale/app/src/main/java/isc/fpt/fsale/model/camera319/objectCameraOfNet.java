package isc.fpt.fsale.model.camera319;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.ui.fpt_camera.model.PromotionDetail;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;

public class objectCameraOfNet implements Parcelable {

    private List<CameraDetail> CameraDetail;
    private List<CloudDetail> CloudDetail;
    private SetupDetail SetupDetail;
    private PromoCloud PromotionDetail;
    private int CameraTotal;

    protected objectCameraOfNet(Parcel in) {
        CameraDetail = in.createTypedArrayList(isc.fpt.fsale.ui.fpt_camera.model.CameraDetail.CREATOR);
        CloudDetail = in.createTypedArrayList(isc.fpt.fsale.ui.fpt_camera.model.CloudDetail.CREATOR);
        SetupDetail = in.readParcelable(SetupDetail.class.getClassLoader());
        PromotionDetail = in.readParcelable(PromotionDetail.class.getClassLoader());
        CameraTotal = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(CameraDetail);
        dest.writeTypedList(CloudDetail);
        dest.writeParcelable(SetupDetail, flags);
        dest.writeParcelable(PromotionDetail, flags);
        dest.writeInt(CameraTotal);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<objectCameraOfNet> CREATOR = new Creator<objectCameraOfNet>() {
        @Override
        public objectCameraOfNet createFromParcel(Parcel in) {
            return new objectCameraOfNet(in);
        }

        @Override
        public objectCameraOfNet[] newArray(int size) {
            return new objectCameraOfNet[size];
        }
    };

    public objectCameraOfNet(List<isc.fpt.fsale.ui.fpt_camera.model.CameraDetail> cameraDetail, List<isc.fpt.fsale.ui.fpt_camera.model.CloudDetail> cloudDetail, isc.fpt.fsale.ui.fpt_camera.model.SetupDetail setupDetail, PromoCloud promotionDetail, int cameraTotal) {
        CameraDetail = cameraDetail;
        CloudDetail = cloudDetail;
        SetupDetail = setupDetail;
        PromotionDetail = promotionDetail;
        CameraTotal = cameraTotal;
    }

    public List<isc.fpt.fsale.ui.fpt_camera.model.CameraDetail> getCameraDetail() {
        return CameraDetail;
    }

    public void setCameraDetail(List<isc.fpt.fsale.ui.fpt_camera.model.CameraDetail> cameraDetail) {
        CameraDetail = cameraDetail;
    }

    public List<isc.fpt.fsale.ui.fpt_camera.model.CloudDetail> getCloudDetail() {
        return CloudDetail;
    }

    public void setCloudDetail(List<isc.fpt.fsale.ui.fpt_camera.model.CloudDetail> cloudDetail) {
        CloudDetail = cloudDetail;
    }

    public isc.fpt.fsale.ui.fpt_camera.model.SetupDetail getSetupDetail() {
        return SetupDetail;
    }

    public void setSetupDetail(isc.fpt.fsale.ui.fpt_camera.model.SetupDetail setupDetail) {
        SetupDetail = setupDetail;
    }

    public PromoCloud getPromotionDetail() {
        return PromotionDetail;
    }

    public void setPromotionDetail(PromoCloud promotionDetail) {
        PromotionDetail = promotionDetail;
    }

    public int getCameraTotal() {
        return CameraTotal;
    }

    public void setCameraTotal(int cameraTotal) {
        CameraTotal = cameraTotal;
    }

    public static Creator<objectCameraOfNet> getCREATOR() {
        return CREATOR;
    }
}
