package isc.fpt.fsale.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPotentialByCode;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class GetByCodeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_by_code);
        Intent intent = getIntent();
        final String PotentialObjID = intent.getStringExtra("PotentialObjID");
        final String UserName = ((MyApp) getApplication()).getUserName();
        final String Code = intent.getStringExtra("Code");
        if (Constants.contextLocal == null) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
        else{
            if (Constants.contextLocal.getClass().getSimpleName().equals(LoginActivity.class.getSimpleName())) {
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            } else {
                AlertDialog.Builder builder;
                Dialog dialog;
                builder = new AlertDialog.Builder(this);
                builder.setMessage(this.getResources().getString(R.string.msg_confirm_active_code))
                        .setCancelable(false)
                        .setPositiveButton(this.getResources().getString(R.string.msg_button_yes),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        new GetPotentialByCode(Constants.contextLocal, UserName, Code, PotentialObjID);
                                        finish();
                                    }
                                })
                        .setNegativeButton(this.getResources().getString(R.string.msg_button_no),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        finish();
                                    }
                                });
                dialog = builder.create();
                dialog.show();
            }
        }
    }
}
