package isc.fpt.fsale.ui.extra_ott.promotion_extra_ott;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionExtraOtt;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

/**
 * Created by haulc3 on 20,July,2019
 */
public class PromotionExtraOttAdapter extends RecyclerView.Adapter<PromotionExtraOttAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<PromotionExtraOtt> mList;
    private OnItemClickListener mListener;

    PromotionExtraOttAdapter(Context mContext, List<PromotionExtraOtt> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_key_value, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(mList.get(mViewHolder.getAdapterPosition()));
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindData(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }

        public void bindData(PromotionExtraOtt promotionExtraOtt) {
            tvName.setText(promotionExtraOtt.getCommandText());
        }
    }

    public void notifyData(List<PromotionExtraOtt> mList){
        this.mList = mList;
        notifyDataSetChanged();
    }
}
