package isc.fpt.fsale.ui.maxytv.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.ui.maxytv.model.MaxyExtra;

public class MaxyExtraAdapter extends RecyclerView.Adapter<MaxyExtraAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<MaxyExtra> mList;
    private OnItemClickListener mListener;

    public MaxyExtraAdapter(Context mContext, List<MaxyExtra> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public MaxyExtraAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_key_value, parent, false);
        final MaxyExtraAdapter.SimpleViewHolder mViewHolder = new MaxyExtraAdapter.SimpleViewHolder(view);
        view.setOnClickListener(view1 -> mListener.onItemClick(mList.get(mViewHolder.getAdapterPosition())));
        mViewHolder.setIsRecyclable(false);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(MaxyExtraAdapter.SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }

        public void bindView(MaxyExtra mBox) {
            tvName.setText(mBox.getName());
            if (mBox.isSelected()) {
                tvName.setBackgroundResource(R.drawable.background_radius_selected);
            }
        }
    }

    public void notifyData(List<MaxyExtra> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
