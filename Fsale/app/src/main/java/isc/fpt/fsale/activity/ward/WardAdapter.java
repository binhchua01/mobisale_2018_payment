package isc.fpt.fsale.activity.ward;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.Ward;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

/**
 * Created by haulc3 on 17,July,2019
 */
public class WardAdapter extends RecyclerView.Adapter<WardAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<Ward> mList;
    private OnItemClickListener mListener;

    WardAdapter(Context mContext, List<Ward> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_key_value, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(mList.get(mViewHolder.getAdapterPosition()));
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.tvName.setText(mList.get(position).getNameVN());
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }
    }

    public void notifyData(List<Ward> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
