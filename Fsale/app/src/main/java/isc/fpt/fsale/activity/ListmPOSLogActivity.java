package isc.fpt.fsale.activity;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.database.mPOSLogTable;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.mPOSLogModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

public class ListmPOSLogActivity extends BaseActivity {
    // Phan trang
    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
    private TextView lblSaleName;
    private Spinner spPage;
    private ListView lvResult;


    public ListmPOSLogActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.lbl_screen_name_list_mpos_log);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_mpos_log));
        setContentView(R.layout.activity_list_mpos_log);
        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem != null) {
                    FLAG_FIRST_LOAD++;
                    if (FLAG_FIRST_LOAD > 1) {
                        if (mPage != selectedItem.getID()) {
                            mPage = selectedItem.getID();
                            getData(mPage);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        lvResult = (ListView) findViewById(R.id.lv_report_sbi_detail);
        lblSaleName = (TextView) findViewById(R.id.lbl_sale);
        lblSaleName.setVisibility(View.GONE);
        getData(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
	/*private void getDataFromIntent() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		if(intent != null){
			this.fromDate = intent.getStringExtra("FROM_DATE");
			this.toDate = intent.getStringExtra("TO_DATE");
			this.saleName = intent.getStringExtra("SALE_NAME");
			this.status = intent.getIntExtra("STATUS", 2);
			lblSaleName.setText(this.saleName);
			getData(0);
		}
	}*/

    private void getData(int PageNumber) {
        mPOSLogTable table = new mPOSLogTable(this);
        List<mPOSLogModel> lst = new ArrayList<mPOSLogModel>();
        lst = table.getAll();
        mPOSLogAdapter adapter = new mPOSLogAdapter(lst);
        lvResult.setAdapter(adapter);
    }


    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    private class mPOSLogAdapter extends BaseAdapter {
        private List<mPOSLogModel> lst;

        public mPOSLogAdapter(List<mPOSLogModel> lst) {
            this.lst = lst;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            if (lst != null)
                return lst.size();
            return 0;
        }

        @Override
        public mPOSLogModel getItem(int position) {
            // TODO Auto-generated method stub
            if (lst != null)
                return lst.get(position);
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            mPOSLogModel item = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(ListmPOSLogActivity.this).inflate(R.layout.row_mpos_log, null);
            }
            TextView lblLog = (TextView) convertView.findViewById(R.id.lbl_log);
            lblLog.setText(item.toString());
            TextView lblRowNum = (TextView) convertView.findViewById(R.id.lbl_row_number);
            lblRowNum.setText(String.valueOf(position + 1));
            return convertView;
        }

    }
}
