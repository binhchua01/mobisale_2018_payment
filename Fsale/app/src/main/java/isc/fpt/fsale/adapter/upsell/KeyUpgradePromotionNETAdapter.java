package isc.fpt.fsale.adapter.upsell;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.upsell.response.AdivisoryResultUpgradeModel;
import isc.fpt.fsale.model.upsell.response.GetPromotionNetUpgradeModel;


public class KeyUpgradePromotionNETAdapter extends ArrayAdapter<GetPromotionNetUpgradeModel> {
    private List<GetPromotionNetUpgradeModel> lstObj;
    private Context mContext;
    private boolean hasInitText = false;
    private int iColor = 2;
    private Typeface tf;



    public KeyUpgradePromotionNETAdapter(Context context, int my_spinner_style, List<GetPromotionNetUpgradeModel> mProgram, int color) {
        super(context, my_spinner_style);
        this.mContext = context;
        this.lstObj = mProgram;
        this.tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Bold.ttf");
        this.iColor = color;
    }


    public List<GetPromotionNetUpgradeModel> getList() {
        return lstObj;
    }

    public int getCount() {
        if (lstObj != null)
            return lstObj.size();
        return 0;
    }

    public GetPromotionNetUpgradeModel getItem(int position) {
        if (lstObj != null)
            return lstObj.get(position);
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(final int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setGravity(Gravity.CENTER);
        if (hasInitText && position == 0) {
            label.setVisibility(View.GONE);
        } else {
            label.setText(lstObj.get(position).getPromotionName());
            label.setTypeface(tf);
            label.setSingleLine(false);
            int padding = (int) mContext.getResources().getDimension(R.dimen.padding_medium);
            label.setPadding(padding, padding, padding, padding);
            label.setTextColor(Color.parseColor("#000000"));
        }
        return label;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setTypeface(tf);
        label.setGravity(Gravity.RIGHT);
        label.setText(lstObj.get(position).getPromotionName());
        label.setTextColor(Color.parseColor("#000000"));
        return label;
    }
}
