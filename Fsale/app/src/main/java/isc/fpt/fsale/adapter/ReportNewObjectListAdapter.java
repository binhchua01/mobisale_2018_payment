package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportNewObjectModel;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ReportNewObjectListAdapter  extends BaseAdapter{
	private ArrayList<ReportNewObjectModel> mList;	
	private Context mContext;
	
	public ReportNewObjectListAdapter(Context context, ArrayList<ReportNewObjectModel> lst){
		this.mContext = context;
		this.mList = lst;	
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportNewObjectModel item = mList.get(position);		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_new_object, null);
		}
		
		TextView txtRowNumber = (TextView) convertView.findViewById(R.id.txt_row_number);
		txtRowNumber.setText(String.valueOf(item.getRowNumber()));
		
		TextView txtContract = (TextView) convertView.findViewById(R.id.txt_contract);
		String contract = item.getContract();
		try {
			SpannableString content = new SpannableString(contract);
			content.setSpan(new UnderlineSpan(), 0, contract.length(), 0);
			txtContract.setText(content);
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			txtContract.setText(String.valueOf(contract));
		}
		
		
		
		TextView txtFullName = (TextView) convertView.findViewById(R.id.tv_full_name);
		txtFullName.setText(String.valueOf(item.getFullName()));
		
		TextView txtLocalTypeName = (TextView)convertView.findViewById(R.id.txt_local_type_name);
		txtLocalTypeName.setText(String.valueOf(item.getLocalTypeName()));
		
		TextView txtCreateDate = (TextView) convertView.findViewById(R.id.txt_create_date);		
		try {
			txtCreateDate.setText(item.getDate().subSequence(0, item.getDate().indexOf(' ')));
		} catch (Exception e) {

			txtCreateDate.setText(item.getDate());
		}
		
		
		TextView txtAddress = (TextView) convertView.findViewById(R.id.txt_address);
		txtAddress.setText(String.valueOf(item.getAddress()));
		
		//
		LinearLayout frmPhone = (LinearLayout)convertView.findViewById(R.id.frm_phone);
		TextView lblPhone = (TextView)convertView.findViewById(R.id.lbl_phone);
		if(item.getPhoneNumber() != null && !item.getPhoneNumber().equals("")){
			lblPhone.setText(item.getPhoneNumber());
		}else
			frmPhone.setVisibility(View.GONE);
		
		return convertView;
	}

}
