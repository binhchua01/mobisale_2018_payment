package isc.fpt.fsale.utils;

import java.util.ArrayList;

import isc.fpt.fsale.model.CameraSelected;
import isc.fpt.fsale.model.CameraType;

/**
 * Created by Hau Le on 2018-12-25.
 */
public interface OnItemCameraClickListener {
    void CallbackListDevice(ArrayList<CameraSelected> list, int position);
}
