package isc.fpt.fsale.utils;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import net.hockeyapp.android.ExceptionHandler;

import android.util.Base64;
import android.util.Log;
 
/**
 * CLASS: 			EnCrypt
 * @description: 	encrypt input string to md5/utf8 code
 * @author: 		vandn, on 07/10/2013
 * */
public class EnCrypt {
	
	/**
	 * @FUNCTION: 		md5Encrypt
	 * @description:	generate key for post request
	 * 					- encrypt string(params + secret key) to md5 code
	 * @return:			substring of md5 code - index 0 to 16
	 * @author:	 		vandn 
	 */
	 public static String md5Encrypt(String input) {
		 String secretKey = "fpay@rad.fpt.com.vn";
		 String output = "null";
		 input += secretKey;
		 input = input.replaceAll("/", "");
	     try
	     {
		     MessageDigest md = MessageDigest.getInstance("MD5");
		     byte[] hash = md.digest(input.getBytes("UTF-8"));
		     StringBuilder sb = new StringBuilder(2*hash.length);	
		     for(byte b : hash)
		     {
		        sb.append(String.format("%02x", b&0xff));
		     }	
		     output = sb.toString();
		     output = output.substring(0,16);
	     }
	     catch (Exception e)
	     {
	    	 e.printStackTrace();
		     Log.d("LOG_GENERATE_KEY_STRING:", e.getMessage());
		 }
	     return output;		
	 }
	 
	 /**
		 * @FUNCTION: 		uft8Encrypt
		 * @description:	- encrypt string(user password) to utf8 code
		 * @return:			uft8 code string with % is replaced by [percent]
		 * @author:	 		vandn 
		 */
	 @SuppressWarnings("deprecation")
	public static String uft8Encrypt(String input){
		 String out = null;	
		 try{
			 out = URLEncoder.encode(input);
			 
			 out = out.replaceAll("\\.","%2E");
			 out = out.replaceAll("\\-", "%2D");
			 out = out.replaceAll("\\*", "%2A");
			 out = out.replaceAll("\\_", "%5F");
			 out = out.replaceAll("\\%", "(percent)");
		}
		catch(Exception e){

			 e.printStackTrace();
		}
		return out;
	 }
	 
	 /**
		 * @FUNCTION: 		SHA-512Encrypt
		 * @description:	- encrypt string(user password) 
		 * @return:			
		 * @author:	 		GiauTQ 
		 */
	public static String SHA512Encrypt(String password) {
	    MessageDigest md = null;
	    try {
	        md = MessageDigest.getInstance("SHA-512");
	    } catch (NoSuchAlgorithmException e) {

	        e.printStackTrace();
	    }
	    if (md != null) {
	        md.update(password.getBytes());
	        byte byteData[] = md.digest();
	        String base64 = byteArrayToHex(byteData);
//	        Base64.encodeToString(byteData, Base64.DEFAULT);
	        return base64;
	    }
	    return password;
	}
	
	public static String byteArrayToHex(byte[] a) {
	   StringBuilder sb = new StringBuilder(a.length * 2);
	   for(byte b: a)
	      sb.append(String.format("%02x", b & 0xff));
	   return sb.toString();
	}
}