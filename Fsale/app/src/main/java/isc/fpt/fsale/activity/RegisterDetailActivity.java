package isc.fpt.fsale.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CreateObject;
import isc.fpt.fsale.ui.fragment.MenuListRegister_RightSide;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.activity.BookPortManualActivity;
import isc.fpt.fsale.model.ImageDocument;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.ui.fragment.FragmentRegisterStep1.TAG_LIST_IMAGE_DOCUMENT_SELECTED;
import static isc.fpt.fsale.ui.fragment.FragmentRegisterStep1.TAG_UPDATE_REGISTRATION;
import static isc.fpt.fsale.ui.fragment.FragmentRegisterStep1.TAG_UPLOAD_IMAGE_TYPE;
import static isc.fpt.fsale.ui.fragment.FragmentRegisterStep1.TAG_VIEW_SIGNATURE;
import static isc.fpt.fsale.utils.Constants.TAG_LIST_IMAGE_DOCUMENT;

/*
 * @author ISC-HUNGLQ9
 * màn hình chi tiết phiếu đăng ký
 */

public class RegisterDetailActivity extends BaseActivity {
    public static final String TAG_REGISTER = "TAG_REGISTER";
    public static final String TAG_REG_ID = "TAG_REG_ID";
    private Button btnCollectMoney, btnCreateContract, btnDetailImageDocument,
            btnDetailImageSignature, btnUploadImageDocument;
    private WebView wbDetailRegistration;

    private RegistrationDetailModel mRegister;
    private MenuListRegister_RightSide menuRight;
    private ArrayList<ImageDocument> listImageDocument;
    private ArrayList<ImageDocument> listImageDocumentSignature;

    public RegisterDetailActivity() {
        super(R.string.lbl_detail_registration);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_register_detail_activity));
        setContentView(R.layout.activity_register_detail);
        initView();
        initEventView();
        initMenuRight();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
//        getDataFromIntent();
//        updateMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
        getDataFromIntent();
        updateMenu();
    }

    private void initMenuRight() {
        menuRight = new MenuListRegister_RightSide(mRegister);
        super.addRight(menuRight);
    }

    private void initEventView() {
        btnCollectMoney.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnCollectMoney.getText().toString().equals(getString(R.string.menu_book_port))) {
                    goToBookPort();
                } else if (btnCollectMoney.getText().toString().equals(getString(R.string.menu_invest))) {
                    gotoInvest();
                } else if (btnCollectMoney.getText().toString().equals(getString(R.string.create_contract_header_activity))) {
                    // tạo hợp đồng pdk
                    Intent intent = new Intent(RegisterDetailActivity.this, CreateContractActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                    startActivity(intent);
                } else if (btnCollectMoney.getText().toString().equals(getString(R.string.pay_contract_header_activity))) {
                    // thanh toán pdk
                    if (mRegister.getCheckListRegistration().getTrangThaiThanhToan() != 0) {
                        if (mRegister.getRegType() > 0) {
                            //Thanh toán pdk bán thêm
                            Intent intent = new Intent(RegisterDetailActivity.this,
                                    PaymentRetailAdditionalActivity.class);
                            intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else {
                            // Thanh toán pdk bán mới
                            Intent intent = new Intent(RegisterDetailActivity.this, PayContractActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                            startActivity(intent);
                        }
                    } else {
                        Common.alertDialog("TTKH đã thu tiền", RegisterDetailActivity.this);
                    }
                }
            }
        });

        // nút Lên hợp đồng
        btnCreateContract.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkToCreateObject()) {
                    confirmToCreateObject();
                }
            }
        });

        btnUploadImageDocument.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterDetailActivity.this,
                        UploadRegistrationDocumentActivity.class);
                intent.putExtra(TAG_UPLOAD_IMAGE_TYPE, 3);
                intent.putExtra(TAG_REG_ID, mRegister.getID());
                boolean tagUpdate = listImageDocument.size() > 0;
                intent.putExtra(TAG_UPDATE_REGISTRATION, tagUpdate);
                intent.putParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT_SELECTED, listImageDocument);
                startActivityForResult(intent, 2);
            }
        });

        btnDetailImageDocument.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRegister != null) {
                    Intent intent = new Intent(RegisterDetailActivity.this,
                            DetailRegistrationDocumentActivity.class);
                    intent.putExtra(TAG_UPDATE_REGISTRATION, true);
                    intent.putParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT_SELECTED, listImageDocument);
                    startActivityForResult(intent, 2);
                }
            }
        });

        btnDetailImageSignature.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRegister != null) {
                    Intent intent = new Intent(RegisterDetailActivity.this,
                            DetailRegistrationDocumentActivity.class);
                    intent.putExtra(TAG_VIEW_SIGNATURE, true);
                    intent.putExtra(TAG_UPDATE_REGISTRATION, true);
                    intent.putParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT_SELECTED, listImageDocumentSignature);
                    startActivityForResult(intent, 2);
                }
            }
        });
    }

    private void initView() {
        listImageDocument = new ArrayList<>();
        listImageDocumentSignature = new ArrayList<>();
        wbDetailRegistration = (WebView) findViewById(R.id.web_view_detail);
        btnCollectMoney = (Button) findViewById(R.id.btn_collect_money);
        btnUploadImageDocument = (Button) findViewById(R.id.btn_register_detail_upload_image);
        btnCreateContract = (Button) findViewById(R.id.btn_create_contract);
        btnDetailImageDocument = (Button) findViewById(R.id.btn_detail_image_document);
        btnDetailImageSignature = (Button) findViewById(R.id.btn_detail_image_signature);
    }

    private void goToBookPort() {
        try {
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.getApplicationContext());
            if (resultCode == ConnectionResult.SUCCESS) {
                GPSTracker gpsTracker = new GPSTracker(this);
                if (gpsTracker.canGetLocation()) {
                    Intent intent;
                    if (Constants.AutoBookPort == 0) {
                        intent = new Intent(this, BookPortManualActivity.class);
                    } else {
                        intent = new Intent(this, MapBookPortAuto.class);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                    startActivity(intent);
                }
            } else if (resultCode == ConnectionResult.SERVICE_MISSING ||
                    resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED ||
                    resultCode == ConnectionResult.SERVICE_DISABLED) {
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 1);
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gotoInvest() {
        try {
            // Kiểm tra xem có bookport chưa. Nếu chưa bookport thì không cho
            // TDName là tập điểm ví dụ HCMP034.041/HO
            if (mRegister.getTDName() != null
                    && !mRegister.getTDName().equals("")) {
                Intent intent = new Intent(this, InvestiageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                startActivity(intent);
            } else {
                Common.alertDialog("Vui lòng bookport trước khi khảo sát.", this);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // kiểm tra thông tin pdk sau khi bấm nút Lên hợp đồng
    private boolean checkToCreateObject() {
        if (mRegister == null) {
            Common.alertDialog("TTKH không có thông tin!", this);
            return false;
        } else {
            if (mRegister.getODCCableType().trim().equals("")) {
                Common.alertDialog("TTKH chưa book port!", this);
                return false;
            } else if (mRegister.getRegCode().trim().equals("")) {
                Common.alertDialog("Không có mã PĐK!", this);
                return false;
            }
        }
        return true;
    }

    // xử lý bấm nút Lên hợp đồng
    private void confirmToCreateObject() {
        if (mRegister != null && !mRegister.getRegCode().trim().equals("")) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.title_notification))
                    .setMessage(getString(R.string.lbl_register_lbl_comfirm_create_object))
                    .setPositiveButton(R.string.lbl_ok,
                            new Dialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    createObject();
                                    dialog.cancel();
                                }
                            })
                    .setNeutralButton(R.string.lbl_cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    private void createObject() {
        new CreateObject(this, Constants.USERNAME, mRegister.getID(), mRegister.getRegCode());
    }

    private void getDataFromIntent() {
        try {
            Intent myIntent = getIntent();
            if (myIntent != null) {
                if (myIntent.getExtras() != null) {
                    mRegister = myIntent.getParcelableExtra(Constants.MODEL_REGISTER);
                    if (mRegister != null) {
                        Log.d("TAG", "onItemClickRegister: " + mRegister.getID());
                        //Check trạng thái HĐ
                        if (mRegister.getCheckListRegistration() != null) {
                            if (mRegister.getRegType() > 0) {
                                if (mRegister.getCheckListRegistration().getTrangThaiThanhToan() != 0) {
                                    btnCollectMoney.setText(getString(R.string.pay_contract_header_activity));
                                } else {
                                    btnCollectMoney.setVisibility(View.GONE);
                                }
                            } else {
                                if (!mRegister.getCheckListRegistration().isBookPort()) {
                                    btnCollectMoney.setText(getString(R.string.menu_book_port));
                                } else if (!mRegister.getCheckListRegistration().isKhaoSat()) {
                                    btnCollectMoney.setText(getString(R.string.menu_invest));
                                } else if (!mRegister.getCheckListRegistration().isHopDong()) {
                                    btnCollectMoney.setText(R.string.create_contract_header_activity);
                                } else if (mRegister.getCheckListRegistration().getTrangThaiThanhToan() != 0) {
                                    btnCollectMoney.setText(getString(R.string.pay_contract_header_activity));
                                } else {
                                    btnCollectMoney.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            Common.alertDialog(getString(R.string.msg_emty_data_container_status_for_register), this);
                        }
                        wbDetailRegistration.loadDataWithBaseURL(null, mRegister.getHTMLDetail(), "text/html", "UTF-8", null);

                        if (mRegister.getRegType() == 0 &&
                                TextUtils.isEmpty(mRegister.getContract()) &&
                                mRegister.getBaseRegIDImageInfo() == 0 &&
                                TextUtils.isEmpty(mRegister.getBaseImageInfo())) {
                            btnUploadImageDocument.setVisibility(View.VISIBLE);
                        } else {
                            btnUploadImageDocument.setVisibility(View.GONE);
                        }

                        if (mRegister.getImageInfo().length() == 0) {
                            btnDetailImageDocument.setVisibility(View.GONE);
                        } else {
                            if (listImageDocument.size() == 0) {
                                listImageDocument = Common.covertStringToListImageDocument(mRegister.getImageInfo());
                            }
                            btnDetailImageDocument.setVisibility(View.VISIBLE);
                        }

                        if (mRegister.getImageSignature().length() == 0) {
                            btnDetailImageSignature.setVisibility(View.GONE);
                        } else {
                            if (listImageDocumentSignature.size() == 0) {
                                listImageDocumentSignature.add(new ImageDocument(mRegister.getImageSignature()));
                            }
                            btnDetailImageSignature.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
            updateMenu();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateMenu() {
        if (mRegister != null && menuRight != null) {
            menuRight.initAdapterFromRegister(mRegister);
        }
    }

    @Override
    public boolean onOptionsItemSelected(
            MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else if (itemId == R.id.action_right) {
            toggle(SlidingMenu.RIGHT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        getDataFromIntent();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (data.hasExtra(TAG_LIST_IMAGE_DOCUMENT)) {
                ArrayList<ImageDocument> listImageDocumentResult = data.getParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT);
                if (data.hasExtra(TAG_VIEW_SIGNATURE)) {
                    listImageDocumentSignature = listImageDocumentResult;
                } else if (data.hasExtra(TAG_UPDATE_REGISTRATION)) {
                    this.listImageDocument = listImageDocumentResult;
                }
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Common.clearAllFilesCache(this);
    }
}