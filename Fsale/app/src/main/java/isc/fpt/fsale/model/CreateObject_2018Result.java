package isc.fpt.fsale.model;

public class CreateObject_2018Result {
    //Declares variables
    private String ClientID;
    private String ErrorService;
    private int Failure;
    private int IgnoreAction;
    private int LogId;
    private String Message;
    private String Result;
    private int ResultID;

    //Constructor
    public CreateObject_2018Result(){}

    //Getter and Setter function
    public String getClientID() {
        return ClientID;
    }

    public void setClientID(String clientID) {
        this.ClientID = clientID;
    }

    public String getErrorService() {
        return ErrorService;
    }

    public void setErrorService(String errorService) {
        this.ErrorService = errorService;
    }

    public int getFailure() {
        return Failure;
    }

    public void setFailure(int failure) {
        this.Failure = failure;
    }

    public int getIgnoreAction() {
        return IgnoreAction;
    }

    public void setIgnoreAction(int ignoreAction) {
        this.IgnoreAction = ignoreAction;
    }

    public int getLogId() {
        return LogId;
    }

    public void setLogId(int logId) {
        this.LogId = logId;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        this.Result = result;
    }

    public int getResultID() {
        return ResultID;
    }

    public void setResultID(int resultID) {
        this.ResultID = resultID;
    }
}
