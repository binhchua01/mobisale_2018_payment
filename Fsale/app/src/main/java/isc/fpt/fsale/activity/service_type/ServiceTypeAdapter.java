package isc.fpt.fsale.activity.service_type;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

/**
 * Created by haulc3 on 24,July,2019
 */
public class ServiceTypeAdapter extends RecyclerView.Adapter<ServiceTypeAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<CategoryServiceList> mList;
    private OnItemClickListener mListener;

    public ServiceTypeAdapter(Context mContext, List<CategoryServiceList> mList,
                              OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_key_value, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.bindData(mList.get(position));
        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(position);
            }
        });
        holder.tvName.setText(mList.get(position).getCategoryServiceName());
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
        }

        public void bindData(CategoryServiceList object) {
            tvName.setText(object.getCategoryServiceName());
            if (object.isSelected()) {
                tvName.setBackgroundResource(R.drawable.background_radius_selected);
            } else {
                tvName.setBackgroundResource(R.drawable.background_radius);
            }
        }
    }

    private void onItemClick(int position) {
        if (mList.get(position).isSelected()) {
            mList.get(position).setSelected(false);
        } else {
            mList.get(position).setSelected(true);
        }
        mListener.onItemClick(mList);
        notifyData(this.mList);
    }

    public void notifyData(List<CategoryServiceList> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
