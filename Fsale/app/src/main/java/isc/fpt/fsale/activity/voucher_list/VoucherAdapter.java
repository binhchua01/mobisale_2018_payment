package isc.fpt.fsale.activity.voucher_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.Voucher;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

/**
 * Created by haulc3 on 29,October,2019
 */
public class VoucherAdapter extends RecyclerView.Adapter<VoucherAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<Voucher> mList;
    private OnItemClickListener mListener;

    public VoucherAdapter(Context mContext, List<Voucher> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_key_value, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClick(mList.get(mViewHolder.getAdapterPosition()));
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }

        public void bindView(Voucher mVoucher) {
            tvName.setText(mVoucher.getDescription());
        }
    }

    public void notifyData(List<Voucher> mList){
        this.mList = mList;
        notifyDataSetChanged();
    }
}
