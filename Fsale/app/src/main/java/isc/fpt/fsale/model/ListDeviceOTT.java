package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListDeviceOTT {
    @SerializedName("OTTID")
    @Expose
    private int OTTId;
    @SerializedName("OTTName")
    @Expose
    private String OTTName;

    public ListDeviceOTT(int ottId, String ottName) {
        OTTId = ottId;
        OTTName = ottName;
    }

    public int getOttId() {
        return OTTId;
    }

    public void setOttId(int ottId) {
        this.OTTId = ottId;
    }

    public String getOttName() {
        return OTTName;
    }

    public void setOttName(String ottName) {
        this.OTTName = ottName;
    }
}
