package isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_type;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetAllCameraType;
import isc.fpt.fsale.model.CameraType;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.CLASS_NAME;
import static isc.fpt.fsale.utils.Constants.LIST_CAMERA_DETAIL_SELECTED;

public class CameraTypeActivity extends BaseActivitySecond implements OnItemClickListener<List<CameraDetail>> {
    private View loBack;
    private Button btnUpdate;
    private CameraTypeAdapter mCameraAdapter;
    private List<CameraDetail> mList;
    private List<CameraDetail> mListSelected;
    private int COMBO_TYPE = 0;
    private boolean isSellMore = false;

    @Override
    protected void onResume() {
        super.onResume();
        new GetAllCameraType(this, COMBO_TYPE, isSellMore);
    }

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putParcelableArrayListExtra(Constants.OBJECT_CAMERA_DETAIL,
                        (ArrayList<? extends Parcelable>) mListSelected);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_camera_type;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.title_camera_device));
        RecyclerView mRecyclerViewCamera = (RecyclerView) findViewById(R.id.recycler_view_camera_list);
        btnUpdate = (Button) findViewById(R.id.button_update);
        mListSelected = new ArrayList<>();
        mList = new ArrayList<>();
        mCameraAdapter = new CameraTypeAdapter(this, mList, this);
        mRecyclerViewCamera.setAdapter(mCameraAdapter);
        getDataIntent();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelableArrayList(LIST_CAMERA_DETAIL_SELECTED) != null) {
            mListSelected = bundle.getParcelableArrayList(LIST_CAMERA_DETAIL_SELECTED);
            COMBO_TYPE = bundle.getInt(Constants.COMBO);
            String className = bundle.getString(CLASS_NAME);
            assert className != null;
            switch (className) {
                case "FptCameraService":
                    isSellMore = false;
                    break;
                case "FragmentFptCamera":
                    isSellMore = true;
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onItemClick(List<CameraDetail> mListSelected) {
        this.mListSelected.clear();
        for (CameraDetail item : mListSelected) {
            if (item.isSelected()) {
                this.mListSelected.add(item);
            }
        }
    }

    public void loadCameraList(List<CameraType> mList) {
        this.mList.clear();
        if (mList == null || mList.size() == 0) {
            return;
        }

        for (CameraType item : mList) {
            this.mList.add(new CameraDetail(item.getTypeID(), item.getTypeName(),
                    0, "", 0, 0, 0, 0, 0, 0, 0, false));
        }
        if (mListSelected == null) {
            return;
        }

        for (int i = 0; i < mList.size(); i++) {
            for (CameraDetail item1 : mListSelected) {
                if (mList.get(i).getTypeID() == item1.getTypeID()) {
                    this.mList.get(i).setPackID(item1.getPackID());
                    this.mList.get(i).setPackName(item1.getPackName());
                    this.mList.get(i).setTypeID(item1.getTypeID());
                    this.mList.get(i).setTypeName(item1.getTypeName());
                    this.mList.get(i).setMinCam(item1.getMinCam());
                    this.mList.get(i).setMaxCam(item1.getMaxCam());
                    this.mList.get(i).setCost(item1.getCost());
                    this.mList.get(i).setQty(item1.getQty());
                    this.mList.get(i).setServiceType(item1.getServiceType());
                    this.mList.get(i).setSelected(true);
                }
            }
        }
        mCameraAdapter.notifyData(this.mList);
    }
}
