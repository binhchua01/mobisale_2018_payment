package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportSalaryModel;
import isc.fpt.fsale.utils.Common;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import net.hockeyapp.android.ExceptionHandler;

import com.danh32.fontify.TextView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;


public class ReportSalaryAdapter  extends BaseAdapter{
	private ArrayList<ReportSalaryModel> mList;	
	private Context mContext;
	
	public ReportSalaryAdapter(Context context, ArrayList<ReportSalaryModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportSalaryModel item = mList.get(position);
		/*
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_salary, null);
		}
		TextView txtRowNumber, txtUserName, txtFullName, txtCN, txtDept, txtDateSalary, txtBasicSalary, txtMonthCommission, txtPrepaidNew, txtIncomeSalary;
		TextView txtTPlusTotal, txtTMinTotal, txtBoxTPlusTotal, txtBoxTMinTotal, txtNetSalary;
		
		txtRowNumber = (TextView)convertView.findViewById(R.id.txt_report_salary_row_number);
		txtUserName = (TextView) convertView.findViewById(R.id.txt_report_salary_username);
		txtFullName = (TextView) convertView.findViewById(R.id.txt_report_salary_full_name);
		txtCN = (TextView) convertView.findViewById(R.id.txt_report_salary_cn);
		txtDept = (TextView) convertView.findViewById(R.id.txt_report_salary_dept);
		txtDateSalary = (TextView) convertView.findViewById(R.id.txt_report_salary_date_salary);
		txtBasicSalary = (TextView) convertView.findViewById(R.id.txt_report_salary_basic_salary); 
		txtMonthCommission = (TextView) convertView.findViewById(R.id.txt_report_salary_month_commission); 
		txtPrepaidNew = (TextView) convertView.findViewById(R.id.txt_report_salary_prepaid_new); 
		txtIncomeSalary = (TextView) convertView.findViewById(R.id.txt_report_salary_income_salary);
		txtTPlusTotal = (TextView) convertView.findViewById(R.id.txt_report_salary_t_plus_total); 
		txtTMinTotal = (TextView) convertView.findViewById(R.id.txt_report_salary_t_min_total); 
		txtBoxTPlusTotal = (TextView) convertView.findViewById(R.id.txt_report_salary_box_t_plus_total); 
		txtBoxTMinTotal = (TextView) convertView.findViewById(R.id.txt_report_salary_box_t_min_total); 
		txtNetSalary = (TextView) convertView.findViewById(R.id.txt_report_salary_net_salary);
		//==========================================================================================
		
		txtRowNumber.setText(String.valueOf(item.getRowNumber()));
		txtUserName.setText(item.getName());
		txtFullName.setText(item.getFullName());
		txtCN.setText(item.getCN());
		txtDept.setText(item.getDept());
		txtDateSalary.setText(item.getDateSalary());
		txtBasicSalary.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBasicSalary()));
		txtMonthCommission.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getMonthCommission()));
		txtPrepaidNew.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getPrepaidNew()));
		txtIncomeSalary.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getIncomeSalary()));
		txtTPlusTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getTPlusTotal()));
		txtTMinTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getTMinTotal()));
		txtBoxTPlusTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBoxTPlusTotal()));
		txtBoxTMinTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBoxTMinTotal()));
		txtNetSalary.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getNetSalary()));		
		
		*/
		

		if (convertView == null) {			
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_sbi_total, null);
		}
		try {
			TextView rowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			TextView saleName = (TextView)convertView.findViewById(R.id.lbl_report_sale_name);
			LinearLayout frmData = (LinearLayout)convertView.findViewById(R.id.frm_report_data);
			if(frmData.getChildCount()>0){
				frmData.removeAllViews();
			}
			
			if(item != null){	
				
				rowNumber.setText(String.valueOf(item.getRowNumber()));
				saleName.setText(item.getName());				
				frmData.addView(initRowItem("Họ tên: ", item.getFullName()));
				//frmData.addView(initRowItem("Chi nhánh: ", item.getCN()));
				//frmData.addView(initRowItem("Phòng ban: ", item.getDept()));
				frmData.addView(initRowItem("Ngày tính lương: ", item.getDateSalary()));
				frmData.addView(initRowItem("Lương cơ bản: ", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBasicSalary())));
				frmData.addView(initRowItem("Hoa hồng bán hàng: ", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getMonthCommission())));
				frmData.addView(initRowItem("Lương doanh thu(KH-TT): ", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getPrepaidNew())));
				frmData.addView(initRowItem("Tổng lương doanh thu: ", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getIncomeSalary())));
				frmData.addView(initRowItem("Tổng lương T+: ", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getTPlusTotal())));
				frmData.addView(initRowItem("Tổng lương T-: ", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getTMinTotal())));
				frmData.addView(initRowItem("Tổng lương T+ Box: ", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBoxTPlusTotal())));
				frmData.addView(initRowItem("Tổng lương T- Box: ", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBoxTMinTotal())));
				frmData.addView(initRowItem("Lương theo công thức: ", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getNetSalary())));
				/*frmData.addView(initRowItem("Lương cơ bản: ", String.format("%,.0f", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBasicSalary()))));
				frmData.addView(initRowItem("Lương cơ bản: ", String.format("%,.0f", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBasicSalary()))));
				frmData.addView(initRowItem("Lương cơ bản: ", String.format("%,.0f", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBasicSalary()))));
				frmData.addView(initRowItem("Lương cơ bản: ", String.format("%,.0f", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBasicSalary()))));
				frmData.addView(initRowItem("Lương cơ bản: ", String.format("%,.0f", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBasicSalary()))));
				frmData.addView(initRowItem("Lương cơ bản: ", String.format("%,.0f", NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getBasicSalary()))));*/
				
			}
				
		} catch (Exception e) {

			Common.alertDialog("ReportSalarydapter.getView():" + e.getMessage(), mContext);
		}
		
		return convertView;
	}
	
	@SuppressLint("InflateParams")
	private LinearLayout initRowItem(String label, String value){
		try {
			View viewChild = LayoutInflater.from(mContext).inflate( R.layout.sub_row_report_salary, null );
			LinearLayout frmDataRow = (LinearLayout)viewChild.findViewById(R.id.frm_report_data_row);
			frmDataRow.setVisibility(View.VISIBLE);		
			TextView lblTitle = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_title);
			TextView lblTotal = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_total);
			lblTitle.setText(label);
			lblTotal.setText(value);
			return frmDataRow;
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			return null;
		}
		
		
		
	}
}
