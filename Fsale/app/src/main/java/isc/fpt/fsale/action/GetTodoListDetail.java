package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListTodoListDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ToDoListDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetTodoListDetail implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetToDoListDetail";
	private String[] paramNames, paramValues;
	
	public GetTodoListDetail(Context context, String UserName, String Contract, int Agent, String AgentName) {	
		mContext = context;
		this.paramNames = new String[]{"UserName", "Contract", "Agent", "AgentName"};
		this.paramValues = new String[]{UserName, Contract, String.valueOf(Agent), AgentName};			
		execute();
	}
	
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetTodoListDetail.this);
		service.execute();		
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			List<ToDoListDetailModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ToDoListDetailModel> resultObject = new WSObjectsModel<ToDoListDetailModel>(jsObj, ToDoListDetailModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetTodoListDetail:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	
	
	private void loadData(List<ToDoListDetailModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListTodoListDetailActivity.class.getSimpleName())){
				ListTodoListDetailActivity activity = (ListTodoListDetailActivity)mContext;
				activity.loadData(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetTodoListDetail.loadRpCodeInfo()", e.getMessage());
		}
	}
}
