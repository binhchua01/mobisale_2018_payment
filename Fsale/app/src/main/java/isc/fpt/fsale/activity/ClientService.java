package isc.fpt.fsale.activity;


import isc.fpt.fsale.action.WriteLogMPos;
import isc.fpt.fsale.database.mPOSLogTable;
import isc.fpt.fsale.model.mPOSLogModel;

import java.util.List;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class ClientService extends Service {

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        updateLogs();
        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
    }

    /*TODO: Cap nhat Log*/
    private void updateLogs() {
        try {
            mPOSLogTable table = new mPOSLogTable(this);
            List<mPOSLogModel> logs = table.getAll();
            if (logs != null && logs.size() > 0) {
                //Update Server DB
                WriteLogMPos task = new WriteLogMPos(this, logs);
                task.execute();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
