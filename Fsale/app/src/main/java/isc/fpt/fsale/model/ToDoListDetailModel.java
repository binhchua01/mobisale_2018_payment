package isc.fpt.fsale.model;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "TO_DO_LIST_DETAIL_MODEL".
 */
public class ToDoListDetailModel {

    private int ID;
    private String ToDoListID;
    private String CareType;
    private String Description;
    private String CreateBy;
    private String Date;
    private int Status;
    private int RowNumber ;
    private int TotalRow ;
    private int TotalPage ;
    private int CurrentPage;
    
    public ToDoListDetailModel() {
    }

    public ToDoListDetailModel(Integer ID, String ToDoListID, String CareType, String Description, String CreateBy, 
    		String Date, Integer Status, int RowNumber, int TotalRow, int TotalPage, int CurrentPage) {
        this.ID = ID;
        this.ToDoListID = ToDoListID;
        this.CareType = CareType;
        this.Description = Description;
        this.CreateBy = CreateBy;
        this.Date = Date;
        this.Status = Status;
        this.RowNumber = RowNumber;
        this.TotalRow = TotalRow;
        this.TotalPage = TotalPage;
        this.CurrentPage = CurrentPage;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getToDoListID() {
        return ToDoListID;
    }

    public void setToDoListID(String ToDoListID) {
        this.ToDoListID = ToDoListID;
    }

    public String getCareType() {
        return CareType;
    }

    public void setCareType(String CareType) {
        this.CareType = CareType;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String CreateBy) {
        this.CreateBy = CreateBy;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public int getRowNumber() {
        return RowNumber;
    }

    public void setRowNumber(int RowNumber) {
        this.RowNumber = RowNumber;
    }
    
    public int getTotalRow() {
        return TotalRow;
    }

    public void setTotalRow(int TotalRow) {
        this.TotalRow = TotalRow;
    }
    
    public int getTotalPage() {
        return TotalPage;
    }

    public void setTotalPage(int TotalPage) {
        this.TotalPage = TotalPage;
    }
    
    public int getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(int CurrentPage) {
        this.CurrentPage = CurrentPage;
    }
}
