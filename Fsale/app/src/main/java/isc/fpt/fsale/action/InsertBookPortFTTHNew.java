package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

// API BookPort bằng tay FTTH_NEW
public class InsertBookPortFTTHNew implements AsyncTaskCompleteListener<String> {

    private final String InsertBookPortFTTHNEW = "BookPortFTTHNEW";
    private final String InsertBookPortFTTHNEW_RESULT = "BookPortFTTHNEWResult";
    private final String ERRORSERVICE = "ErrorService";
    private final String RESULT = "Result";

    private Context mContext;
    private String[] ParamsValuse;
    private int ID;

    public InsertBookPortFTTHNew(Context mContext, String[] _paramsValue, String _ID) {
        this.mContext = mContext;
        this.ParamsValuse = _paramsValue;
        try {
            this.ID = Integer.valueOf(_ID);
        } catch (Exception e) {
            // TODO: handle exception

            ID = 0;
        }

        InsertFTTHNEW(ParamsValuse);
    }

    public void InsertFTTHNEW(String[] paramsValue) {
        //Port (INT) :IDPort
        //RegCode (STRING) : SOPHIEUDK
        //TDName (STRING) : TDName
        //UserName (STRING) :UserName
        //IP (STRING) :String IP="192.168.123.123";
        String[] params = new String[]{"Port", "RegCode", "TDName", "UserName", "IP", "Latlng", "LatlngDevice", "LatlngMarker"};
        String message = "Vui lòng đợi giây lát";
        CallServiceTask service = new CallServiceTask(mContext, InsertBookPortFTTHNEW, params, paramsValue, Services.JSON_POST, message, InsertBookPortFTTHNew.this);
        service.execute();
    }

    public void handleUpdateRegistration(String json) {
        if (json != null && Common.jsonObjectValidate(json)) {
            try {
                JSONArray jsArr;
                JSONObject jsObj = JSONParsing.getJsonObj(json);
                jsArr = jsObj.getJSONArray(InsertBookPortFTTHNEW_RESULT);
                int l = jsArr.length();
                if (l > 0) {
                    String error = jsArr.getJSONObject(0).getString(ERRORSERVICE);
                    if (error.equals("null")) {
                        String result = jsArr.getJSONObject(0).getString(RESULT);
                        int ResultID = jsArr.getJSONObject(0).getInt("ResultID");
                        if (ResultID == 1) {
                            new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage("BookPort thành công !")
                                    .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            try {
                                                new GetRegistrationDetail(mContext, Constants.USERNAME, ID);
                                                dialog.cancel();

                                            } catch (Exception e) {

                                                Log.i("InsertBookPortFTTHNew", "_handleUpdateRegistration=" + e.getMessage());
                                            }

                                        }
                                    })
                                    .setCancelable(false).create().show();


                        } else {
                            // new GetRegistrationDetail(mContext,"oanh254",this.ID);
                            Common.alertDialog(result, mContext);
                        }
                    } else
                        Common.alertDialog(error, mContext);
                }
            } catch (Exception e) {

                e.printStackTrace();
                Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
            }
        } else
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
    }

    @Override
    public void onTaskComplete(String result) {
        handleUpdateRegistration(result);
    }

}
