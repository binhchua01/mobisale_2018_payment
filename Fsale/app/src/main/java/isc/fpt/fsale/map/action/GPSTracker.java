package isc.fpt.fsale.map.action;

import net.hockeyapp.android.ExceptionHandler;
import isc.fpt.fsale.R;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.utils.Common;
import com.google.android.gms.maps.model.LatLng;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;


/****
 * CLASS:			GPS Tracker 
 * @description:
 * @author: 		vandn
 * @created on: 	09/08/2013
 * */

public class GPSTracker implements LocationListener {

	private final Context mContext;
	// flag for GPS status
	public boolean isGPSEnabled = false;
	// flag for network status
	public boolean isNetworkEnabled = false;
	// declaring a Location Manager
	public static LocationManager locationManager;
	//declaring gps best provider
	public static String bestProvider;
	public static Location location;
	public static LatLng coordinate;
	// The minimum distance to change Updates in meters
	public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 0 meters
	// The minimum time between updates in milliseconds
	public static final long MIN_TIME_BW_UPDATES = 0;//(1000 * 60 * 1; // 1 minute)

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}
	public GPSTracker(Context context) {
		this.mContext = context;
		getLocation();
	}

	/**
	 * Function to check GPS/wifi enabled
	 * @return boolean
	 * */
	public boolean canGetLocation(){
		boolean canGet = false;
		// getting GPS status
		isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		// getting network status
		isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		if (!isGPSEnabled && !isNetworkEnabled)
			canGet= false;
		else canGet = true;
		return canGet;
	}

	/**
	 * start GPS tracker
	 */
	public Location getLocation() {
		try {
			// Register the listener with the Location Manager to receive location updates
		 	locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
		 	bestProvider = locationManager.getBestProvider(MapCommon.initGPS_Settings(), true);
			if (!canGetLocation()) {
				showSettingsAlert();
			} else {
				if (isNetworkEnabled) {
					if(getNetWorkLocation() != null);
					else getGPSLocation();
				}else if(isGPSEnabled)
					getGPSLocation();
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		return location;
	}

	public Location getLocation(boolean netWorkFirst) {
		try {
			/*// Register the listener with the Location Manager to receive location updates
			 	locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
			 	bestProvider = locationManager.getBestProvider(MapCommon.initGPS_Settings(), true);				
			if (!canGetLocation()) {			
				showSettingsAlert();
			} else {		
				if (isNetworkEnabled) {
					if(getNetWorkLocation() != null);
					else getGPSLocation();					
				}else if(isGPSEnabled)
					getGPSLocation();
			}*/
			locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
		 	bestProvider = locationManager.getBestProvider(MapCommon.initGPS_Settings(), true);
//			if (!canGetLocation()) {			
//				showSettingsAlert();
//			} else {
				if(netWorkFirst){
					if(getNetWorkLocation() == null)
						getGPSLocation();
				}else{
					if(getGPSLocation() == null)
						getNetWorkLocation();
//				}
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		return location;
	}

	/**
	 * when GPS isn't enable: switch on network tracker to find current location
	 * */
	public void callNetWorkTracker(){
		if (isNetworkEnabled) {
			 locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
			 location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			 if(location == null){
					Common.alertDialog(mContext.getResources().getString(R.string.msg_unable_get_location), mContext);
			 }
			 else{
				double lat = location.getLatitude();
				double lng = location.getLongitude();
				coordinate = new LatLng(lat, lng);
			 }
		}
		else showSettingsAlert();
	}

	public Location getGPSLocation(){
		if(isGPSEnabled){
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
			location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if (location != null){
				double lat = location.getLatitude();
				double lng = location.getLongitude();
				coordinate = new LatLng(lat, lng);
			}
		}
		return location;
	}

	private Location getNetWorkLocation(){
		if (isNetworkEnabled) {
			 locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
			 location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			/* if(location == null){
					Common.alertDialog(mContext.getResources().getString(R.string.msg_unable_get_location), mContext);
			 	*/
			 if(location != null){
				double lat = location.getLatitude();
				double lng = location.getLongitude();
				coordinate = new LatLng(lat, lng);
			 }
		}
		return location;
	}

	/**
	 * Stop using GPS listener
	 * Calling this function will stop using GPS in your app
	 * */
	public void stopUsingGPS(){
		if(locationManager != null){
			locationManager.removeUpdates(this);

		}
	}

	/**
	 * Function to show settings alert dialog
	 * On pressing Settings button will lauch Settings Options
	 * added by vandn
	 * */
	public void showSettingsAlert(){
		location = null;
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle(R.string.title_settings_gps);

        // Setting Dialog Message
        alertDialog.setMessage(R.string.msg_gps_off);

        // On pressing Settings button
        alertDialog.setPositiveButton(R.string.lbl_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            	mContext.startActivity(intent);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.lbl_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
	}

	/**
	 * my location change listener
	 */
	@Override
	public void onLocationChanged(Location newLocation) {
		if(newLocation!=null){
			//set new value
			location = newLocation;
			double lat = location.getLatitude();
			double lng = location.getLongitude();
			coordinate = new LatLng(lat, lng);
			stopUsingGPS();
		}
	}
}
