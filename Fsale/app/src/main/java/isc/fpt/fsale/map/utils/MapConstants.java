package isc.fpt.fsale.map.utils;

import com.google.android.gms.maps.GoogleMap;

/**
 * CLASS: 			Map constants
 * @description: 	define global variables for map functions
 * @author: 		vandn, on 07/08/2013
 * */
public class MapConstants {
	public static GoogleMap MAP = null;	
	public static boolean IS_SATELLITE = false;//false: normal, true: satellite
	
	//google place auto complete variables
	public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
	public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
	public static final String OUT_JSON = "/json";

}
