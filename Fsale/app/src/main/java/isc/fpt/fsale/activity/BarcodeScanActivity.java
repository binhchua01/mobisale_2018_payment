package isc.fpt.fsale.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import com.google.zxing.Result;

import isc.fpt.fsale.R;
import isc.fpt.fsale.utils.Common;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class BarcodeScanActivity extends BaseActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    public BarcodeScanActivity() {
        super(R.string.lbl_screen_name_scan_mac);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_scan_mac));
        setContentView(R.layout.activity_barcode_scan);
        mScannerView = (ZXingScannerView) findViewById(R.id.scannerZxing);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(BarcodeScanActivity.this);
        mScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.stopCamera();
        finish();
    }

    @Override
    public void handleResult(Result result) {
        Intent intent = new Intent();
        if (result != null) {
            intent.putExtra("result", result.getText());
            setResult(RESULT_OK, intent);
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
