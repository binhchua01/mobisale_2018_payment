package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.Gravity;
import android.widget.Spinner;

public class GetPortFreeFTTHNewAction implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private Spinner spPortFree;
    private ArrayList<KeyValuePairModel> LIST_PORT;

    public GetPortFreeFTTHNewAction(Context _mContext, String TDName, Spinner sp) {
        this.mContext = _mContext;
        this.spPortFree = sp;
        String message = "Xin cho trong giay lat";
        String[] params = new String[]{TDName};
        String[] paramNames = new String[]{"TDName"};
        String GET_PORT_FREE_FTTH_New = "GetPortFreeFTTHNEW";
        CallServiceTask service = new CallServiceTask(mContext, GET_PORT_FREE_FTTH_New, paramNames, params,
                Services.JSON_POST, message, GetPortFreeFTTHNewAction.this);
        service.execute();
    }

    public void handleGetDistrictsResult(String json) {
        LIST_PORT = new ArrayList<>();
        LIST_PORT.add(new KeyValuePairModel(-1, "[ Vui lòng chọn port ]"));
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if (jsObj == null) {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_service_404) + " " + mContext.getResources().getString(R.string.msg_connectServer), mContext);
                KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LIST_PORT, Gravity.LEFT);
                spPortFree.setAdapter(adapter);
                return;
            }
            bindData(jsObj);
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LIST_PORT, Gravity.LEFT);
            spPortFree.setAdapter(adapter);
        } else {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
        }
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {
            String TAG_GET_PORT_FREE_RESULT_FTTH_New = "GetPortFreeFTTHNEWResult";
            jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_RESULT_FTTH_New);
            String error = jsArr.getJSONObject(0).getString("ErrorService");
            if (error.equals("null")) {
                int l = jsArr.length();
                if (l > 0) {
                    LIST_PORT = new ArrayList<>();
                    for (int i = 0; i < l; i++) {
                        JSONObject iObj = jsArr.getJSONObject(i);
                        LIST_PORT.add(new KeyValuePairModel(iObj.getInt("ID"), iObj.getString("PortID")));
                    }
                }
            } else {
                Common.alertDialog("Lỗi WS: " + error, mContext);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleGetDistrictsResult(result);
    }
}