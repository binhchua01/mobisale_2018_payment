package isc.fpt.fsale.utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import isc.fpt.fsale.R;
// màn hình chọn loại dịch vụ
@SuppressLint("AppCompatCustomView")
public class MultiSelectionSpinner extends Spinner implements DialogInterface.OnMultiChoiceClickListener {
    String[] _items = null;
    boolean[] mSelection = null;
    boolean[] mSelectionPre = null;
    ArrayAdapter<String> simple_adapter;
    MultiSpinnerListener listener;
    AlertDialog.Builder builder;
    Context context;
    ArrayAdapter<String> adapter;
    List<String> listitem;
    ListView list;

    public void setOnOkListener(MultiSpinnerListener listener) {
        this.listener = listener;
    }

    public MultiSelectionSpinner(Context context) {
        super(context);
        this.context = context;
        simple_adapter = new ArrayAdapter<>(context,
                R.layout.simple_spinner_item_new);
        super.setAdapter(simple_adapter);
    }

    public MultiSelectionSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        simple_adapter = new ArrayAdapter<>(context,
                R.layout.simple_spinner_item_new);
        super.setAdapter(simple_adapter);
    }
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        if (mSelection != null && which < mSelection.length) {
            mSelection[which] = isChecked;
        } else {
            throw new IllegalArgumentException(
                    "Argument 'which' is out of bounds.");
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean performClick() {
        mSelectionPre = mSelection.clone();
        builder = new AlertDialog.Builder(getContext());
        list = new ListView(context);
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listitem = new ArrayList<>();
        Collections.addAll(listitem, _items);
        adapter = new ArrayAdapter<>(context,
                android.R.layout.simple_list_item_multiple_choice, listitem);
        list.setAdapter(adapter);
        if (!mSelection[0] && !mSelection[1] && listitem.size() == 5) {
            listitem.remove(listitem.size() - 1);
            adapter.notifyDataSetChanged();
        }

        for (int i = 0; i < listitem.size(); i++) {
            list.setItemChecked(i, mSelection[i]);
        }

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View view, int pos, long id) {
                CheckedTextView ch = (CheckedTextView) view;
                boolean checked = ch.isChecked();
                if (checked) {
                    mSelection[pos] = true;
                } else {
                    mSelection[pos] = false;
                }
                if(Constants.IS_BUY_CAMERA == 1){
                    if (pos == 0 || pos == 1) {
                        if (checked && listitem.size() < 5) {
                            listitem.add("Đăng ký thiết bị");
                            if (mSelection[0] || mSelection[1]) {
                                if (listitem.size() == 5) {
                                    list.setItemChecked(listitem.size() - 1, false);
                                }
                            }
                        } else {
                            if (listitem.size() == 5 && !mSelection[0]
                                    && !mSelection[1]) {
                                listitem.remove(listitem.size() - 1);
                                mSelection[mSelection.length - 1] = false;
                            }
                        }
                    } else {
                        if (listitem.size() == 5 && pos != 4) {
                            if (!mSelection[0] && !mSelection[1])
                                listitem.remove(listitem.size() - 1);
                        }
                    }
                }else{
                    if (pos == 0 || pos == 1) {
                        if (checked && listitem.size() < 4) {
                            listitem.add("Đăng ký thiết bị");
                            if (mSelection[0] || mSelection[1]) {
                                if (listitem.size() == 4) {
                                    list.setItemChecked(listitem.size() - 1, false);
                                }
                            }
                        } else {
                            if (listitem.size() == 4 && !mSelection[0]
                                    && !mSelection[1]) {
                                listitem.remove(listitem.size() - 1);
                                mSelection[mSelection.length - 1] = false;
                            }
                        }
                    } else {
                        if (listitem.size() == 4 && pos != 3) {
                            if (!mSelection[0] && !mSelection[1])
                                listitem.remove(listitem.size() - 1);
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });

        builder.setView(list);
        builder.setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                if (buildSelectedItemString().length() >= 1) {
                    // check fpt play && fcamera
                    if (mSelection[2]) {
                        // check others is false
                        if (mSelection[0] || mSelection[1] || mSelection[3]) {
                            mSelection = mSelectionPre.clone();
                            listener.onSelectFPTPlay();
                        } else {
                            simple_adapter.clear();
                            simple_adapter.add(buildSelectedItemString());
                            listener.onOKClickMultiSpinner();
                        }
                    }else if(Constants.IS_BUY_CAMERA == 1 && mSelection[3]){
                        // check others is false
                        if (mSelection[0] || mSelection[1] || mSelection[2]) {
                            mSelection = mSelectionPre.clone();
                            listener.onSelectFPTCamera();
                        } else {
                            simple_adapter.clear();
                            simple_adapter.add(buildSelectedItemString());
                            listener.onOKClickMultiSpinner();
                        }
                    }else {
                        simple_adapter.clear();
                        simple_adapter.add(buildSelectedItemString());
                        listener.onOKClickMultiSpinner();
                    }
                } else {
                    mSelection = mSelectionPre.clone();
                    listener.onNotChoose();
                }
            }
        });
        builder.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub
                mSelection = mSelectionPre.clone();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
        return true;
    }

    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new RuntimeException(
                "setAdapter is not supported by MultiSelectSpinner.");
    }

    public void setItems(String[] items) {
        _items = items;
        mSelection = new boolean[_items.length];
        simple_adapter.clear();
        simple_adapter.add(_items[0]);
        Arrays.fill(mSelection, false);

        if (items.length > 0) {
            mSelection[0] = true;
        }
    }

    // khởi tạo các mục chọn ban đầu
    public void setItems(List<String> items) {
        _items = items.toArray(new String[items.size()]);
        mSelection = new boolean[_items.length];
        simple_adapter.clear();
        simple_adapter.add(_items[0]);
        Arrays.fill(mSelection, false);
        //chọn mặc định cái đầu tiên
        if (items.size() > 0) {
            mSelection[0] = true;
        }
    }

    public void setSelection(String[] selection) {
        for (String cell : selection) {
            for (int j = 0; j < _items.length; ++j) {
                if (_items[j].equals(cell)) {
                    mSelection[j] = true;
                }
            }
        }
    }

    public void setSelection(List<String> selection) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        for (String sel : selection) {
            for (int j = 0; j < _items.length; ++j) {
                if (_items[j].equals(sel)) {
                    mSelection[j] = true;
                }
            }
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public void setSelection(int index) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        if (index >= 0 && index < mSelection.length) {
            mSelection[index] = true;
        } else {
            throw new IllegalArgumentException("Index " + index
                    + " is out of bounds.");
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public void setSelectionBooleanList(boolean[] data) {
        mSelection = data.clone();
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public void setSelectionListInt(List<Integer> indexs) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        for (Integer index : indexs) {
            if (index >= 0 && index < mSelection.length) {
                mSelection[index] = true;
            } else {
                throw new IllegalArgumentException("Index " + index
                        + " is out of bounds.");
            }
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public void setSelection(int[] selectedIndicies) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        for (int index : selectedIndicies) {
            if (index >= 0 && index < mSelection.length) {
                mSelection[index] = true;
            } else {
                throw new IllegalArgumentException("Index " + index
                        + " is out of bounds.");
            }
        }
        simple_adapter.clear();
        simple_adapter.add(buildSelectedItemString());
    }

    public List<String> getSelectedStrings() {
        List<String> selection = new LinkedList<String>();
        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                selection.add(_items[i]);
            }
        }
        return selection;
    }

    public List<Integer> getSelectedIndicies() {
        List<Integer> selection = new LinkedList<Integer>();
        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                selection.add(i);
            }
        }
        return selection;
    }

    private String buildSelectedItemString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;

                sb.append(_items[i]);
            }
        }
        return sb.toString();
    }

    public String getSelectedItemsAsString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < _items.length; ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;
                sb.append(_items[i]);
            }
        }
        return sb.toString();
    }

    public boolean[] getBoolean() {
        return mSelection;
    }

    public interface MultiSpinnerListener {
        public void onOKClickMultiSpinner();

        public void onNotChoose();

        public void onSelectFPTPlay();

        //in after, haulc3 has modify
        void onSelectFPTCamera();
    }
}