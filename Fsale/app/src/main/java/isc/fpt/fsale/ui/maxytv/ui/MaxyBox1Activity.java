package isc.fpt.fsale.ui.maxytv.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.maxy.GetMaxyDevice;
import isc.fpt.fsale.action.maxy.GetMaxyPackage;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyPackageAdapter;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyTypeAdapter;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.ui.maxytv.model.MaxyGeneral;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.BOX_FIRST;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.LOCAL_TYPE;
import static isc.fpt.fsale.utils.Constants.MAXY_PROMOTION_ID;
import static isc.fpt.fsale.utils.Constants.MAXY_PROMOTION_TYPE;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_PACKAGE_DETAIL;
import static isc.fpt.fsale.utils.Constants.REGTYPE;
import static isc.fpt.fsale.utils.Constants.TAG_CONTRACT;

public class MaxyBox1Activity extends BaseActivitySecond implements OnItemClickListener<MaxyBox> {
    private View loBack;
    private MaxyTypeAdapter mAdapter;
    private List<MaxyBox> mList;
    private MaxyBox mListBoxSelected;
    private int RegType, localtype, box, maxyProID,maxyProType;
    private String contract;
    private String mClassName;
    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(mClassName)) {
            new GetMaxyDevice(this, RegType, box, localtype, maxyProID,maxyProType, contract);
        }
    }

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_device_list;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText("Chọn danh sách Box thứ nhất");
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_device_list);
        mList = new ArrayList<>();
        mAdapter = new MaxyTypeAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);
        getDataIntent();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mListBoxSelected = bundle.getParcelable(LIST_MAXY_DETAIL_SELECTED);
            RegType = bundle.getInt(REGTYPE);
            localtype = bundle.getInt(LOCAL_TYPE);
            box = bundle.getInt(BOX_FIRST);
            maxyProID = bundle.getInt(MAXY_PROMOTION_ID);
            contract = bundle.getString(TAG_CONTRACT);
            maxyProType = bundle.getInt(MAXY_PROMOTION_TYPE);
        }
        if (bundle != null && bundle.getString(Constants.CLASS_NAME) != null) {
            mClassName = bundle.getString(Constants.CLASS_NAME);
        }
    }

    @Override
    public void onItemClick(MaxyBox object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.OBJECT_MAXY_DEVICE_BOX_1_DETAIL, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    public void loadDeviceList(List<MaxyBox> lst) {
        for (MaxyBox item : mList) {
                if (item.getBoxID() == mListBoxSelected.getPromotionID()) {
                    item.setSelected(true);
                }
        }
        this.mList = lst;
        mAdapter.notifyData(this.mList);
    }
}
