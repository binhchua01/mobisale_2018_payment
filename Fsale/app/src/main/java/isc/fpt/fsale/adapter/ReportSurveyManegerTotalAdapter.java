package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportSurveyManagerTotalModel;
import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReportSurveyManegerTotalAdapter extends BaseAdapter {
	 
	private ArrayList<ReportSurveyManagerTotalModel> mlist;
	private Context mContext;
	
	
	public ReportSurveyManegerTotalAdapter(Context mContext,ArrayList<ReportSurveyManagerTotalModel> mlist)
	{
		this.mContext=mContext;
		this.mlist=mlist;
	}
	
	@Override
	public int getCount()
	{
		return this.mlist.size();
	}
	
	@Override
	public Object getItem(int position)
	{
		return this.mlist.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	
		ReportSurveyManagerTotalModel ItemBookPort = mlist.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_survey_manger_total,null);
		}		
		
		TextView txtStt = (TextView) convertView.findViewById(R.id.txt_stt);
		txtStt.setText(ItemBookPort.getRowNumber());
		
		TextView txtSaleName = (TextView) convertView.findViewById(R.id.txt_sale_name);
		txtSaleName.setText(ItemBookPort.getSaleName());
		
		TextView txtTotalObject = (TextView) convertView.findViewById(R.id.txt_total_object);
		txtTotalObject.setText(ItemBookPort.getTotalObject());
		
		TextView txtCabPlus = (TextView) convertView.findViewById(R.id.txt_cab_plus);
		txtCabPlus.setText(ItemBookPort.getCabPlus());
		
		TextView txtObjectCabPlus = (TextView) convertView.findViewById(R.id.txt_object_cab_plus);
		txtObjectCabPlus.setText(ItemBookPort.getObjectCabPlus());
		
		TextView txtCabSub = (TextView) convertView.findViewById(R.id.txt_cab_sub);
		txtCabSub.setText(ItemBookPort.getCabSub());
		
		TextView txtObjectCabSub = (TextView) convertView.findViewById(R.id.txt_object_cab_sub);
		txtObjectCabSub.setText(ItemBookPort.getObjectCabSub());
		
		return convertView;
	}
	
}
