package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentPromotionAdImageList;
import isc.fpt.fsale.ui.fragment.FragmentPromotionAdTextList;
import isc.fpt.fsale.model.PromotionBrochureModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetPromotionBrochure implements AsyncTaskCompleteListener<String>{ 

	public final static String METHOD_NAME = "GetBrochure";
	public static String[] paramNames = new String[]{"UserName", "Type"};
	public static int TYPE_VIDEO = 2, TYPE_IMAGE = 1, TYPE_TEXT = 3;
	private Context mContext; 
	private FragmentPromotionAdImageList fragmentMediaList;
	private FragmentPromotionAdTextList fragmentTextList;
	
	public GetPromotionBrochure(Context context, FragmentPromotionAdImageList fragment, int Type ) {
		this.mContext = context;
		this.fragmentMediaList = fragment;
		String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
		String[] paramValues = new String[]{userName, String.valueOf(Type)};		
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST_UPLOAD, message, GetPromotionBrochure.this);
		service.execute();	
	}
	
	public GetPromotionBrochure(Context context, FragmentPromotionAdTextList fragmentTextList, int Type ) {
		this.mContext = context;
		this.fragmentTextList = fragmentTextList;
		String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
		String[] paramValues = new String[]{userName, String.valueOf(Type)};		
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST_UPLOAD, message, GetPromotionBrochure.this);
		service.execute();	
	}
		
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			ArrayList<PromotionBrochureModel> lst = null;
			boolean isError = false;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<PromotionBrochureModel> resultObject = new WSObjectsModel<PromotionBrochureModel>(jsObj, PromotionBrochureModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();								
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(ArrayList<PromotionBrochureModel> lst){
		/*if(lst != null && lst.size() >0){
			Intent intent = new Intent(mContext, ShowImageActivity.class);
			String s = lst.get(0).getImage();
			intent.putExtra("IMAGE", s);
			mContext.startActivity(intent);
			if(mContext.getClass().getSimpleName().equals(ShowImageActivity.class.getSimpleName())){
				ShowImageActivity activity = (ShowImageActivity)mContext;
				activity.loadRpCodeInfo(lst.get(0).getImage());
			}			
		}else{
			Common.alertDialog("Không có dữ liệu!", mContext);
		}*/
		
		if(fragmentMediaList != null){
			fragmentMediaList.loadData(lst);
		}
		if(fragmentTextList != null){
			fragmentTextList.loadData(lst);
		}
	}
	
	

}
