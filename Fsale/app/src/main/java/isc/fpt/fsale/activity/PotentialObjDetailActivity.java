package isc.fpt.fsale.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPotentialByCode;
import isc.fpt.fsale.action.UpdateSaleCallTime;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.ui.fragment.MenuListPotentialObjDetail;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * @author ISC-HUNGLQ9
 */
// màn hình KHÁCH HÀNG TIỀM NĂNG
public class PotentialObjDetailActivity extends BaseActivity {
    private PotentialObjModel potentialObj;
    private MenuListPotentialObjDetail menu;
    private TextView lblFullName, lblPassport, lblTaxID, lblPhone1, lblEmail, lblFacebook,
            lblAddress, lblLocation, lblCreateDate, lblNote, lblRegCode, lblServiceType,
            lblISP, lblISPStartDate, lblISPEndDate, lblIspIpTv;
    private Button btnUpdate, btnAccept, btnShowphone;
    private String supporter;
    private String phone1;
    public static final String TAG_POTENTIAL_OBJECT = "TAG_POTENTIAL_OBJECT";
    public static final String TAG_SUPPORTER = "SUPPORTER";
    private Context mContext;
    private boolean is_click_phone = false;

    public PotentialObjDetailActivity(int titleRes) {
        super(titleRes);
    }

    public PotentialObjDetailActivity() {
        super(R.string.lbl_screen_name_potential_obj_detail);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_potential_obj_detail));
        setContentView(R.layout.activity_potential_obj_detail);
        this.mContext = this;
        lblFullName = (TextView) findViewById(R.id.lbl_full_name);
        lblPassport = (TextView) findViewById(R.id.lbl_passport);
        lblTaxID = (TextView) findViewById(R.id.lbl_tax_id);
        lblPhone1 = (TextView) findViewById(R.id.lbl_phone_1);
        lblEmail = (TextView) findViewById(R.id.lbl_email);
        lblFacebook = (TextView) findViewById(R.id.lbl_face_book);
        lblAddress = (TextView) findViewById(R.id.lbl_address);
        lblLocation = (TextView) findViewById(R.id.lbl_location);
        lblCreateDate = (TextView) findViewById(R.id.lbl_create_date);
        lblNote = (TextView) findViewById(R.id.lbl_note);
        lblRegCode = (TextView) findViewById(R.id.lbl_reg_code);
        lblServiceType = (TextView) findViewById(R.id.lbl_service_type);
        lblIspIpTv = (TextView) findViewById(R.id.lbl_isp_ip_tv);
        lblISP = (TextView) findViewById(R.id.lbl_isp);
        lblISPStartDate = (TextView) findViewById(R.id.lbl_isp_start_date);
        lblISPEndDate = (TextView) findViewById(R.id.lbl_isp_end_date);
        btnUpdate = (Button) findViewById(R.id.btn_update);
        ImageView btnCallPhonePotential = (ImageView) findViewById(R.id.ib_call_phone);
        btnShowphone = (Button) findViewById(R.id.btn_show_phone);
        btnCallPhonePotential.setVisibility(View.GONE);
        lblPhone1.setText("Vui lòng đọc ghi chú");
        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (potentialObj != null) {
                    Intent intent = new Intent(PotentialObjDetailActivity.this, UpdatePotentialAdvisoryResultActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("TAG_POTENTIAL_OBJECT", potentialObj);
                    PotentialObjDetailActivity.this.startActivity(intent);
                }
            }
        });
        lblPhone1.setOnClickListener(new OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                if (is_click_phone) {
                    new UpdateSaleCallTime(mContext, potentialObj.getID());
                }
            }
        });
        btnCallPhonePotential.setOnClickListener(new OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mContext.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 2);
                    } else {
                        new UpdateSaleCallTime(mContext, potentialObj.getID());
                    }
                } else {
                    new UpdateSaleCallTime(mContext, potentialObj.getID());
                }

            }
        });
        btnShowphone.setOnClickListener(new OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                btnCallPhonePotential.setVisibility(View.VISIBLE);
                lblPhone1.setText(phone1);
                is_click_phone = true;
                btnShowphone.setEnabled(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    btnShowphone.setBackground(getDrawable(R.drawable.background_button_v2_disable));
                }
            }
        });
        btnAccept = (Button) findViewById(R.id.btn_accept);
        btnAccept.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                Dialog dialog;
                builder = new AlertDialog.Builder(PotentialObjDetailActivity.this);
                builder.setMessage(getResources().getString(R.string.msg_confirm_update))
                        .setCancelable(false)
                        .setPositiveButton("Có",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        String UserName = ((MyApp) getApplication()).getUserName();
                                        new GetPotentialByCode(PotentialObjDetailActivity.this, UserName, null, String.valueOf(potentialObj.getID()));
                                    }
                                })
                        .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
                dialog = builder.create();
                dialog.show();
            }
        });
        getDataFromIntent();
        menu = new MenuListPotentialObjDetail(this.potentialObj, this.supporter);
        super.addRight(menu);
        if (this.potentialObj != null) {
            if (this.potentialObj.getCodeStatus() == 0) {
                btnAccept.setVisibility(View.VISIBLE);
                btnUpdate.setVisibility(View.GONE);
                setPropertyLayoutButtonAccept();
            } else {
                btnAccept.setVisibility(View.GONE);
                btnUpdate.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        try {
            Intent intent = getIntent();
            if (intent != null) {
                this.potentialObj = intent.getParcelableExtra("POTENTIAL_OBJECT");
                this.supporter = intent.getStringExtra(PotentialObjDetailActivity.TAG_SUPPORTER);
                boolean flagNotify = intent.getBooleanExtra("FLAG_NOTIFY", false);
                String Code = intent.getStringExtra("Code");
                if (flagNotify) {
                    String UserName = ((MyApp) getApplication()).getUserName();
                    this.potentialObj.setCodeStatus(1);
                    new GetPotentialByCode(PotentialObjDetailActivity.this, UserName, String.valueOf(Code), String.valueOf(potentialObj.getID()));
                }
                if (menu != null)
                    menu.setPotentialObj(this.potentialObj);
                if (this.potentialObj != null && !flagNotify) {
                    this.lblAddress.setText(potentialObj.getAddress());
                    lblCreateDate.setText(potentialObj.getCreateDate());
                    lblFacebook.setText(potentialObj.getFacebook());
                    lblFullName.setText(potentialObj.getFullName());
                    lblLocation.setText(potentialObj.getLatlng());
                    lblNote.setText(potentialObj.getNote());
                    lblPassport.setText(potentialObj.getPassport());
                    lblTaxID.setText(potentialObj.getTaxID());
                    lblRegCode.setText(potentialObj.getRegCode());
                    if (potentialObj.getPhone1() != null && !potentialObj.getPhone1().trim().equals("")) {
                        //lblPhone1.setText(potentialObj.getPhone1());
                        phone1 = potentialObj.getPhone1();
                    }
                    if (potentialObj.getEmail() != null && !potentialObj.getEmail().trim().equals("")) {
                        if (Common.checkMailValid(potentialObj.getEmail())) {
                            String a = "<a href=\"mailto:" + potentialObj.getEmail().trim() + "\">" + potentialObj.getEmail() + "</a>";
                            lblEmail.setText(Html.fromHtml(a));
                            lblEmail.setMovementMethod(LinkMovementMethod.getInstance());
                        }
                    }
                    if (potentialObj.getDisplayPhone() > 0) {
                        lblPhone1.setVisibility(View.VISIBLE);
                    }
                    mapName();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mapName() {
        if (potentialObj != null) {
            //Show/Hide ngày bắt đầu/kết thúc dịch vụ của nhà CC khác
            if (potentialObj.getISPStartDate() != null && !potentialObj.getISPStartDate().trim().equals("")) {
                lblISPStartDate.setText(potentialObj.getISPStartDate());
            }

            if (potentialObj.getISPEndDate() != null && !potentialObj.getISPEndDate().trim().equals("")) {
                lblISPEndDate.setText(potentialObj.getISPEndDate());
            }

            if (!potentialObj.getServiceTypeName().trim().equals("")) {
                lblServiceType.setText(potentialObj.getServiceTypeName());
            }

            if (!potentialObj.getISPIPTV().trim().equals("")) {
                lblIspIpTv.setText(potentialObj.getISPIPTV());
            }

//            boolean isExist = false;
            if (potentialObj.getISPType() > 0) {
                //ISP Type
                ArrayList<KeyValuePairModel> lstISPType = new ArrayList<>();
                lstISPType.add(new KeyValuePairModel(0, getString(R.string.lbl_hint_spinner_none)));
                lstISPType.add(new KeyValuePairModel(1, "Viettel"));
                lstISPType.add(new KeyValuePairModel(2, "VNPT"));
                lstISPType.add(new KeyValuePairModel(3, "Netnam"));
                lstISPType.add(new KeyValuePairModel(4, "CMC"));
                lstISPType.add(new KeyValuePairModel(5, "SCTV"));
                lstISPType.add(new KeyValuePairModel(6, "HCTV"));
                lstISPType.add(new KeyValuePairModel(7, "Khác"));
                for (KeyValuePairModel item : lstISPType) {
                    if (potentialObj.getISPType() == item.getID()) {
                        lblISP.setText(item.getDescription());
//                        isExist = true;
                        break;
                    }
                }
            }
//            if (!isExist) {
//                frmISPInternet.setVisibility(View.GONE);
//            } else {
//                frmISPInternet.setVisibility(View.VISIBLE);
//            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else if (itemId == R.id.action_right) {
            toggle(SlidingMenu.RIGHT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        getDataFromIntent();
    }

    public void hideButtonAccept(boolean flag) {
        if (flag) {
            btnAccept.setVisibility(View.GONE);
            btnUpdate.setVisibility(View.VISIBLE);
            this.potentialObj.setAcceptStatus(1);
            menu.initAdapterFromRegister(this.potentialObj);
        }
    }

    private void setPropertyLayoutButtonAccept() {
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        btnAccept.setLayoutParams(params);
    }

    public void callPhonePotential() {
        if (potentialObj != null) {
            if (potentialObj.getPhone1() != null && potentialObj.getPhone1().length() > 0) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:".concat(potentialObj.getPhone1())));
                if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = mContext.getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(mContext, message);
                return;
            }
        }
        if (requestCode == 2) {
            new UpdateSaleCallTime(mContext, potentialObj.getID());
        }
    }
}