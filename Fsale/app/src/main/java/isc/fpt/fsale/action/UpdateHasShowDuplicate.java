package isc.fpt.fsale.action;

import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.MyApp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

public class UpdateHasShowDuplicate extends AsyncTask<Void, Void, String> {
	@SuppressLint("StaticFieldLeak")
	private Context mContext;
	public final String TAG_METHOD_NAME = "UpdateHasShowDuplicate";
	public static String[] paramNames = new String[]{"UserName", "ID"};
	private int dupID;
	
	UpdateHasShowDuplicate(Context context, int DupID) {
		mContext = context;	
		this.dupID = DupID;
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
	}
	
	@Override
	protected String doInBackground(Void...voids) {
		try {
			String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
			String[] paramsValue = new String[]{userName, String.valueOf(dupID)};
			return Services.postJson(TAG_METHOD_NAME, paramNames, paramsValue, mContext);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
	}
}
