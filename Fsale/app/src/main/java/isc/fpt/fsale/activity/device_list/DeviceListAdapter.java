package isc.fpt.fsale.activity.device_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

/**
 * Created by haulc3 on 18,October,2019
 */
public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<Device> mList;
    private OnItemClickListener mListener;

    public DeviceListAdapter(Context mContext, List<Device> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_key_value, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClick(mList.get(mViewHolder.getAdapterPosition()));
            }
        });
        mViewHolder.setIsRecyclable(false);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }

        public void bindView(Device mDevice) {
            tvName.setText(mDevice.getDeviceName());
            if(mDevice.isSelected()){
                tvName.setBackgroundResource(R.drawable.background_radius_selected);
            }
        }
    }

    public void notifyData(List<Device> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
