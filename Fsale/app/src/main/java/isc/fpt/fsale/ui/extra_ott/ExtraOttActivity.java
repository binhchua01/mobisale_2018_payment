package isc.fpt.fsale.ui.extra_ott;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetTotalExtraOtt;
import isc.fpt.fsale.action.UpdateRegistrationExtraOtt;
import isc.fpt.fsale.model.CartExtraOtt;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.ExtraOttTotalResponse;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.ServiceListExtraOtt;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.extra_ott.adapter.TabAdapter;
import isc.fpt.fsale.ui.extra_ott.fragment.FragmentExtraOttCusInfo;
import isc.fpt.fsale.ui.extra_ott.fragment.FragmentExtraOttService;
import isc.fpt.fsale.ui.extra_ott.fragment.FragmentExtraOttTotal;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class ExtraOttActivity extends BaseActivitySecond implements
        ViewPager.OnPageChangeListener {
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RelativeLayout rltBack;
    public RegisterExtraOttModel mRegisterExtraOttModel;
    public List<CategoryServiceList> mListService;
    private RegistrationDetailModel mRegistrationModel;
    private PotentialObjModel mPotentialObjModel;

    private FragmentExtraOttCusInfo fragCusInfo;
    public FragmentExtraOttService fragService;
    private FragmentExtraOttTotal fragTotal;

    private boolean is_show_aleart = false;
    private int[] tabIcons = {
            R.drawable.tab_customer_info,
            R.drawable.tab_service,
            R.drawable.tab_money_total
    };

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_extra_ott;
    }

    @Override
    public void showError(String errorMessage) {
        Common.alertDialog(errorMessage, this);
    }

    @Override
    protected void initView() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);

        mRegisterExtraOttModel = new RegisterExtraOttModel();
        adapter = new TabAdapter(getSupportFragmentManager(), this);
        fragCusInfo = new FragmentExtraOttCusInfo();
        fragService = new FragmentExtraOttService();
        fragTotal = new FragmentExtraOttTotal();

        adapter.addFragment(fragCusInfo, getResources().getString(R.string.lbl_tab_cus_info), tabIcons[0]);
        adapter.addFragment(fragService, getResources().getString(R.string.lbl_tab_service), tabIcons[1]);
        adapter.addFragment(fragTotal, getResources().getString(R.string.lbl_tab_total), tabIcons[2]);

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOnPageChangeListener(this);
        highLightCurrentTab(0);
        getDataIntent();
        bindDataRegistrationModelToRegister();
        bindDataPotentialCustomer();
    }

    public RegisterExtraOttModel getRegisterModel() {
        return mRegisterExtraOttModel != null ? mRegisterExtraOttModel : new RegisterExtraOttModel();
    }

    public List<CategoryServiceList> getListService() {
        return mListService != null ? mListService : new ArrayList<CategoryServiceList>();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelableArrayList(Constants.LIST_CAT_SERVICE) != null) {//lấy TT PĐK khi tạo mới
            mListService = bundle.getParcelableArrayList(Constants.LIST_CAT_SERVICE);
        }
        if (bundle != null && bundle.getParcelable(Constants.POTENTIAL_OBJECT) != null) {//lấy thông tin từ KHTN
            mPotentialObjModel = bundle.getParcelable(Constants.POTENTIAL_OBJECT);
        }
        if (getIntent().getParcelableExtra(Constants.MODEL_REGISTER) != null) {//lấy thông tin PĐK khi cập nhật
            mRegistrationModel = getIntent().getParcelableExtra(Constants.MODEL_REGISTER);
        }
    }

    private void highLightCurrentTab(int position) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(adapter.getTabView(i));
        }
        TabLayout.Tab tab = tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(adapter.getSelectedTabView(position));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        highLightCurrentTab(position);
        switch (position) {
            case 0://frag cus info
                break;
            case 1:// frag service
                if (!validationFragCusInfo()) {//validation fragment customer info
                    setPageViewPager(0);
                }
                break;
            case 2://frag total
                if (!validationFragCusInfo()) {
                    setPageViewPager(0);
                    return;
                }

                if (!validationFragService()) {//validation fragment service
                    setPageViewPager(1);
                    return;
                }
                //put data to object
                submitDataToRegistration();
                //tính tổng tiền
                new GetTotalExtraOtt(this, mRegisterExtraOttModel);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void submitDataToRegistration() {
        mRegisterExtraOttModel.setLocationID(Constants.LOCATION_ID);
        mRegisterExtraOttModel.setBillTo_City(Constants.LOCATION_NAME);
        mRegisterExtraOttModel.setUserName(Constants.USERNAME);
        mRegisterExtraOttModel.setSupporter(Constants.USERNAME);
        mRegisterExtraOttModel.setFullName(fragCusInfo.txtCustomerName.getText().toString());
        mRegisterExtraOttModel.setPassport(fragCusInfo.txtIdentityCard.getText().toString());
        mRegisterExtraOttModel.setBirthday(fragCusInfo.txtBirthDay.getText().toString());
        mRegisterExtraOttModel.setAddressPassport(fragCusInfo.txtAddressId.getText().toString());
        mRegisterExtraOttModel.setTaxId(fragCusInfo.txtTaxNum.getText().toString());
        mRegisterExtraOttModel.setEmail(fragCusInfo.txtEmail.getText().toString());
        mRegisterExtraOttModel.setPhone_1(fragCusInfo.txtPhone1.getText().toString());
        mRegisterExtraOttModel.setContact_1(fragCusInfo.txtContactPhone1.getText().toString());
        mRegisterExtraOttModel.setPhone_2(fragCusInfo.txtPhone2.getText().toString());
        mRegisterExtraOttModel.setContact_2(fragCusInfo.txtContactPhone2.getText().toString());
        mRegisterExtraOttModel.setLot(fragCusInfo.txtLot.getText().toString());
        mRegisterExtraOttModel.setFloor(fragCusInfo.txtFloor.getText().toString());
        mRegisterExtraOttModel.setRoom(fragCusInfo.txtRoom.getText().toString());
        mRegisterExtraOttModel.setBillTo_Number(fragCusInfo.txtHouseNum.getText().toString());
        mRegisterExtraOttModel.setNote(fragCusInfo.txtHouseDesc.getText().toString());
        mRegisterExtraOttModel.setDescriptionIBB(fragCusInfo.txtDescriptionIBB.getText().toString());
        mRegisterExtraOttModel.setAddress(getAddress());
        mRegisterExtraOttModel.setCusType(1);//ca nhan viet nam
        mRegisterExtraOttModel.setCusTypeDetail("12"); //
    }

    private boolean validationFragCusInfo() {
        boolean isValidate = false;
        if (fragCusInfo.txtIdentityCard != null &&
                TextUtils.isEmpty(fragCusInfo.txtIdentityCard.getText().toString())) {
            showError(getString(R.string.txt_message_enter_identity_card));
        } else if (fragCusInfo.txtAddressId != null &&
                TextUtils.isEmpty(fragCusInfo.txtAddressId.getText().toString())) {
            showError(getString(R.string.txt_message_enter_address_id));
        } else if (fragCusInfo.txtEmail != null &&
                !TextUtils.isEmpty(fragCusInfo.txtEmail.getText().toString()) &&
                !Common.checkMailValid(fragCusInfo.txtEmail.getText().toString())) {
            // CHECK EMAIL
            showError(getString(R.string.txt_message_invalid_email));
        } else if (fragCusInfo.txtCustomerName != null &&
                TextUtils.isEmpty(fragCusInfo.txtCustomerName.getText().toString())) {
            showError(getString(R.string.txt_message_enter_cus_name));
        } else if (fragCusInfo.txtBirthDay != null &&
                TextUtils.isEmpty(fragCusInfo.txtBirthDay.getText().toString())) {
            showError(getString(R.string.txt_message_choose_birthday));
        } else if (fragCusInfo.tvPhone1 != null &&
                TextUtils.isEmpty(fragCusInfo.tvPhone1.getText().toString())) {
            showError(getString(R.string.txt_message_choose_phone_type));
        } else if (fragCusInfo.txtPhone1 != null &&
                TextUtils.isEmpty(fragCusInfo.txtPhone1.getText().toString())) {
            showError(getString(R.string.txt_message_enter_phone_number));
        } else if (fragCusInfo.txtContactPhone1 != null &&
                TextUtils.isEmpty(fragCusInfo.txtContactPhone1.getText().toString())) {
            showError(getString(R.string.txt_message_enter_contact_people));
        } else if (fragCusInfo.tvDistrict != null &&
                TextUtils.isEmpty(fragCusInfo.tvDistrict.getText().toString())) {
            showError(getString(R.string.txt_message_choose_district));
        } else if (fragCusInfo.tvWard != null &&
                TextUtils.isEmpty(fragCusInfo.tvWard.getText().toString())) {
            showError(getString(R.string.txt_message_choose_ward));
        } else if (fragCusInfo.tvHouseType != null &&
                TextUtils.isEmpty(fragCusInfo.tvHouseType.getText().toString())) {
            showError(getString(R.string.txt_message_choose_house_type));
        } else if (fragCusInfo.tvHouseType != null &&
                !TextUtils.isEmpty(fragCusInfo.tvHouseType.getText().toString())) {
            if (mRegisterExtraOttModel.getTypeHouse() == 1) {//nhà phố
                if (fragCusInfo.tvStreet != null &&
                        TextUtils.isEmpty(fragCusInfo.tvStreet.getText().toString())) {
                    showError(getString(R.string.txt_message_choose_street));
                } else if (fragCusInfo.txtHouseNum != null &&
                        TextUtils.isEmpty(fragCusInfo.txtHouseNum.getText().toString())) {
                    showError(getString(R.string.txt_message_enter_house_number));
                } else {
                    isValidate = true;
                }
            } else if (mRegisterExtraOttModel.getTypeHouse() == 2) {// cu xa, chung cu, cho, villa, thuong xa
                if (fragCusInfo.tvStreet != null &&
                        TextUtils.isEmpty(fragCusInfo.tvStreet.getText().toString())) {
                    showError(getString(R.string.txt_message_choose_street));
                }else if (fragCusInfo.txtFloor != null &&
                        TextUtils.isEmpty(fragCusInfo.txtFloor.getText().toString())) {
                    showError(getString(R.string.txt_message_enter_floor));
                } else if (fragCusInfo.txtRoom != null &&
                        TextUtils.isEmpty(fragCusInfo.txtRoom.getText().toString())) {
                    showError(getString(R.string.txt_message_enter_room));
                } else if (fragCusInfo.tvApartment != null &&
                        TextUtils.isEmpty(fragCusInfo.tvApartment.getText().toString())) {
                    showError(getString(R.string.txt_message_choose_apartment));
                } else {
                    isValidate = true;
                }
            } else if (mRegisterExtraOttModel.getTypeHouse() == 3) {//Nhà không địa chỉ
                if (fragCusInfo.tvStreet != null &&
                        TextUtils.isEmpty(fragCusInfo.tvStreet.getText().toString())) {
                    showError(getString(R.string.txt_message_choose_street));
                } else if (fragCusInfo.tvHousePosition != null &&
                        TextUtils.isEmpty(fragCusInfo.tvHousePosition.getText().toString())) {
                    showError(getString(R.string.txt_message_choose_house_position));
                } else if (fragCusInfo.txtHouseNum != null &&
                        TextUtils.isEmpty(fragCusInfo.txtHouseNum.getText().toString())) {
                    showError(getString(R.string.txt_message_enter_house_number));
                } else {
                    isValidate = true;
                }
            } else {
                isValidate = true;
            }
        }else if(fragCusInfo.tvSourceType != null &&
                TextUtils.isEmpty(fragCusInfo.tvSourceType.getText().toString())){
                showError(getString(R.string.txt_message_choose_source_type));
        } else {
            isValidate = true;
        }
        return isValidate;
    }

    private boolean validationFragService() {
        boolean isValidate = false;

        if (fragService.tvLocalType != null &&
                TextUtils.isEmpty(fragService.tvLocalType.getText().toString())) {//chưa chọn local type
            showError(getString(R.string.txt_message_choose_package_service_extra_ott));
        } else if (mRegisterExtraOttModel.getCartExtraOtt().getServiceList() != null) {
            if (mRegisterExtraOttModel.getCartExtraOtt().getServiceList().size() != 0) {//đã chọn gói dịch vụ
                for (ServiceListExtraOtt item : mRegisterExtraOttModel.getCartExtraOtt().getServiceList()) {
                    if (item.getPromoCommand().getCommandText() == null) {
                        is_show_aleart = true;
                        //showError(getString(R.string.txt_message_choose_promotion_extra_ott));//chưa chọn CLKM
                        isValidate = false;
                    } else if (item.getQuantity() != null && item.getQuantity() == 0) {
                        showError(getString(R.string.txt_message_choose_quantity_extra_ott));//chưa chọn SL
                        isValidate = false;
                        break;
                    } else {
                        isValidate = true;
                    }
                }
                if (is_show_aleart == true) {
                    showError(getString(R.string.txt_message_choose_promotion_extra_ott));//chưa chọn CLKM
                    is_show_aleart = false;
                }
            } else {//chưa chọn gói dịch vụ
                showError(getString(R.string.txt_message_choose_package_service_name_extra_ott));
            }
        } else {
            isValidate = true;
        }
        return isValidate;
    }

    public void setPageViewPager(final int index) {
        viewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(index);
            }
        }, 10);
    }

    public void onUpdateRegistration(Integer isAutoActive) {
        if(mRegisterExtraOttModel.getCartExtraOtt() == null) mRegisterExtraOttModel.setCartExtraOtt(new CartExtraOtt());
        mRegisterExtraOttModel.getCartExtraOtt().setIsAutoActive(isAutoActive);
        new UpdateRegistrationExtraOtt(this, mRegisterExtraOttModel);
    }

    public void resultExtraOttTotal(List<ExtraOttTotalResponse> mList) {
        if (mList == null || mList.size() == 0) {
            return;
        }
        if (fragTotal.tvEntertainmentTotal != null) {
            Integer total = 0;
            for (ExtraOttTotalResponse item : mList) {
                total += item.getTotalAmountVAT();
            }
            fragTotal.tvEntertainmentTotal.setText(Common.formatNumber(total));
        }
        if (fragTotal.tvTotal != null) {
            Integer total = 0;
            for (ExtraOttTotalResponse item : mList) {
                total += item.getTotalAmountVAT();
            }
            fragTotal.tvTotal.setText(Common.formatNumber(total));

            //set total to object update
            mRegisterExtraOttModel.setTotal(total);
        }
    }

    private void bindDataPotentialCustomer() {
        if (mPotentialObjModel != null) {
            mRegisterExtraOttModel = new RegisterExtraOttModel();
            mRegisterExtraOttModel.setCusType(1);//ca nhan viet nam
            mRegisterExtraOttModel.setCusTypeDetail("12"); // ho gia dinh
            mRegisterExtraOttModel.setAddress(mPotentialObjModel.getAddress() != null ?
                    mPotentialObjModel.getAddress() : "");
            mRegisterExtraOttModel.setBillTo_City(mPotentialObjModel.getBillTo_City() != null ?
                    mPotentialObjModel.getBillTo_City() : "");
            mRegisterExtraOttModel.setBillTo_CityVN(mPotentialObjModel.getBillTo_CityVN() != null ?
                    mPotentialObjModel.getBillTo_CityVN() : "");
            mRegisterExtraOttModel.setBillTo_District(mPotentialObjModel.getBillTo_District() != null ?
                    mPotentialObjModel.getBillTo_District() : "");
            mRegisterExtraOttModel.setBillTo_DistrictVN(mPotentialObjModel.getBillTo_DistrictVN() != null ?
                    mPotentialObjModel.getBillTo_DistrictVN() : "");
            mRegisterExtraOttModel.setBillTo_Number(mPotentialObjModel.getBillTo_Number() != null ?
                    mPotentialObjModel.getBillTo_Number() : "");
            mRegisterExtraOttModel.setBillTo_Street(mPotentialObjModel.getBillTo_Street() != null ?
                    mPotentialObjModel.getBillTo_Street() : "");
            mRegisterExtraOttModel.setBillTo_StreetVN(mPotentialObjModel.getBillTo_StreetVN() != null ?
                    mPotentialObjModel.getBillTo_StreetVN() : "");
            mRegisterExtraOttModel.setBillTo_Ward(mPotentialObjModel.getBillTo_Ward() != null ?
                    mPotentialObjModel.getBillTo_Ward() : "");
            mRegisterExtraOttModel.setBillTo_WardVN(mPotentialObjModel.getBillTo_WardVN() != null ?
                    mPotentialObjModel.getBillTo_WardVN() : "");
            mRegisterExtraOttModel.setContact_1(mPotentialObjModel.getContact1() != null ?
                    mPotentialObjModel.getContact1() : "");
            mRegisterExtraOttModel.setContact_2(mPotentialObjModel.getContact2() != null ?
                    mPotentialObjModel.getContact2() : "");
            mRegisterExtraOttModel.setNote(mPotentialObjModel.getNote() != null ?
                    mPotentialObjModel.getNote() : "");
            mRegisterExtraOttModel.setEmail(mPotentialObjModel.getEmail() != null ?
                    mPotentialObjModel.getEmail() : "");
            mRegisterExtraOttModel.setFloor(mPotentialObjModel.getFloor() != null ?
                    mPotentialObjModel.getFloor() : "");
            mRegisterExtraOttModel.setFullName(mPotentialObjModel.getFullName() != null ?
                    mPotentialObjModel.getFullName() : "");
            mRegisterExtraOttModel.setLocationID(String.valueOf(mPotentialObjModel.getLocationID()));
            mRegisterExtraOttModel.setLot(mPotentialObjModel.getLot() != null ?
                    mPotentialObjModel.getLot() : "");
            mRegisterExtraOttModel.setNameVilla(mPotentialObjModel.getNameVilla() != null ?
                    mPotentialObjModel.getNameVilla() : "");
            mRegisterExtraOttModel.setNameVillaVN(mPotentialObjModel.getNameVillaVN() != null ?
                    mPotentialObjModel.getNameVillaVN() : "");
            mRegisterExtraOttModel.setNameVillaDes(mPotentialObjModel.getNameVillaDes() != null ?
                    mPotentialObjModel.getNameVillaDes() : "");
            mRegisterExtraOttModel.setPassport(mPotentialObjModel.getPassport() != null ?
                    mPotentialObjModel.getPassport() : "");
            if (!mPotentialObjModel.getPhone1().trim().equals("")) {
                mRegisterExtraOttModel.setPhone_1(mPotentialObjModel.getPhone1() != null ?
                        mPotentialObjModel.getPhone1() : "");
                mRegisterExtraOttModel.setType_1(4);
            }
            if (!mPotentialObjModel.getPhone2().trim().equals("")) {
                mRegisterExtraOttModel.setPhone_2(mPotentialObjModel.getPhone2() != null ?
                        mPotentialObjModel.getPhone2() : "");
                mRegisterExtraOttModel.setType_2(4);
            }
            mRegisterExtraOttModel.setPosition(mPotentialObjModel.getPosition());
            mRegisterExtraOttModel.setRoom(mPotentialObjModel.getRoom() != null ?
                    mPotentialObjModel.getRoom() : "");
            mRegisterExtraOttModel.setSupporter(mPotentialObjModel.getSupporter() != null ?
                    mPotentialObjModel.getSupporter() : "");
            mRegisterExtraOttModel.setTaxId(mPotentialObjModel.getTaxID() != null ?
                    mPotentialObjModel.getTaxID() : "");
            mRegisterExtraOttModel.setPotentialID(mPotentialObjModel.getID());
            mRegisterExtraOttModel.setTypeHouse(mPotentialObjModel.getTypeHouse());
            mRegisterExtraOttModel.setSourceType(mPotentialObjModel.getSourceType());
        }
    }

    private void bindDataRegistrationModelToRegister() {
        if (mRegistrationModel == null) {
            return;
        }
        mRegisterExtraOttModel.setID(mRegistrationModel.getID());
        mRegisterExtraOttModel.setAddress(mRegistrationModel.getAddress() != null ?
                mRegistrationModel.getAddress() : "");
        mRegisterExtraOttModel.setAddressPassport(mRegistrationModel.getAddressPassport() != null ?
                mRegistrationModel.getAddressPassport() : "");
        mRegisterExtraOttModel.setBirthday(mRegistrationModel.getBirthday() != null ?
                mRegistrationModel.getBirthday() : "");
        mRegisterExtraOttModel.setBillTo_City(mRegistrationModel.getBillTo_City() != null ?
                mRegistrationModel.getBillTo_City() : "");
        mRegisterExtraOttModel.setBillTo_District(mRegistrationModel.getBillTo_District() != null ?
                mRegistrationModel.getBillTo_District() : "");
        mRegisterExtraOttModel.setBillTo_Number(mRegistrationModel.getBillTo_Number() != null ?
                mRegistrationModel.getBillTo_Number() : "");
        mRegisterExtraOttModel.setBillTo_Street(mRegistrationModel.getBillTo_Street() != null ?
                mRegistrationModel.getBillTo_Street() : "");
        mRegisterExtraOttModel.setBillTo_Ward(mRegistrationModel.getBillTo_Ward() != null ?
                mRegistrationModel.getBillTo_Ward() : "");
        mRegisterExtraOttModel.setContact_1(mRegistrationModel.getContact_1() != null ?
                mRegistrationModel.getContact_1() : "");
        mRegisterExtraOttModel.setContact_2(mRegistrationModel.getContact_2() != null ?
                mRegistrationModel.getContact_2() : "");
        mRegisterExtraOttModel.setCusTypeDetail(mRegistrationModel.getCusTypeDetail() != null ?
                mRegistrationModel.getCusTypeDetail() : "");
        mRegisterExtraOttModel.setNote(mRegistrationModel.getNote() != null ?
                mRegistrationModel.getNote() : "");
        mRegisterExtraOttModel.setEmail(mRegistrationModel.getEmail() != null ?
                mRegistrationModel.getEmail() : "");
        mRegisterExtraOttModel.setFloor(mRegistrationModel.getFloor() != null ?
                mRegistrationModel.getFloor() : "");
        mRegisterExtraOttModel.setFullName(mRegistrationModel.getFullName() != null ?
                mRegistrationModel.getFullName() : "");
        mRegisterExtraOttModel.setLocationID(String.valueOf(mRegistrationModel.getLocationID()));
        mRegisterExtraOttModel.setLot(mRegistrationModel.getLot() != null ?
                mRegistrationModel.getLot() : "");
        mRegisterExtraOttModel.setNameVilla(mRegistrationModel.getNameVilla());
        mRegisterExtraOttModel.setNameVillaDes(mRegistrationModel.getNameVillaVN() != null ?
                mRegistrationModel.getNameVillaVN() : "");
        mRegisterExtraOttModel.setPassport(mRegistrationModel.getPassport() != null ?
                mRegistrationModel.getPassport() : "");
        mRegisterExtraOttModel.setPhone_1(mRegistrationModel.getPhone_1() != null ?
                mRegistrationModel.getPhone_1() : "");
        mRegisterExtraOttModel.setType_1(mRegistrationModel.getType_1());
        mRegisterExtraOttModel.setPhone_2(mRegistrationModel.getPhone_2() != null ?
                mRegistrationModel.getPhone_2() : "");
        mRegisterExtraOttModel.setType_2(mRegistrationModel.getType_2());
        mRegisterExtraOttModel.setPosition(mRegistrationModel.getPosition());
        mRegisterExtraOttModel.setRoom(mRegistrationModel.getRoom() != null ?
                mRegistrationModel.getRoom() : "");
        mRegisterExtraOttModel.setSupporter(mRegistrationModel.getSupporter() != null ?
                mRegistrationModel.getSupporter() : "");
        mRegisterExtraOttModel.setTaxId(mRegistrationModel.getTaxId() != null ?
                mRegistrationModel.getTaxId() : "");
        mRegisterExtraOttModel.setPotentialID(mRegistrationModel.getPotentialID());
        mRegisterExtraOttModel.setTypeHouse(mRegistrationModel.getTypeHouse());
        mRegisterExtraOttModel.setCartExtraOtt(mRegistrationModel.getObjectExtraOTT());
        mRegisterExtraOttModel.setLocalType(mRegistrationModel.getLocalType());
        mRegisterExtraOttModel.setLocalTypeName(mRegistrationModel.getLocalTypeName());
        mRegisterExtraOttModel.setCategoryServiceList(mRegistrationModel.getCategoryServiceList());
        mRegisterExtraOttModel.setDescriptionIBB(mRegistrationModel.getDescriptionIBB());
        mRegisterExtraOttModel.setBillTo_CityVN(mRegistrationModel.getBillTo_CityVN());
        mRegisterExtraOttModel.setBillTo_DistrictVN(mRegistrationModel.getBillTo_DistrictVN());
        mRegisterExtraOttModel.setBillTo_WardVN(mRegistrationModel.getBillTo_WardVN());
        mRegisterExtraOttModel.setBillTo_StreetVN(mRegistrationModel.getBillTo_StreetVN());
        mRegisterExtraOttModel.setNameVillaVN(mRegistrationModel.getNameVillaVN());
        mRegisterExtraOttModel.setSourceType(mRegistrationModel.getSourceType());
    }

    public String getAddress() {
        String result = "";
        String sStreetSelected = "";
        String sApartmentSelected = "";
        int sHousePositionSelected = 0;
        String sCityNameValue = "";
        String sDistrictSelected = "";
        int sHouseType = -1;

        try {
            sDistrictSelected = fragCusInfo.tvDistrict != null ?
                    fragCusInfo.tvDistrict.getText().toString() : "";
            sStreetSelected = fragCusInfo.tvStreet != null ?
                    fragCusInfo.tvStreet.getText().toString() : "";
            sApartmentSelected = fragCusInfo.tvApartment != null ?
                    fragCusInfo.tvApartment.getText().toString() : "";
            sHouseType = mRegisterExtraOttModel.getTypeHouse();
            if (Constants.LST_REGION.size() > 0) {
                sCityNameValue = Constants.LST_REGION.get(0).getDescription();
            }
            if (fragCusInfo.housePositionPanel.getVisibility() == View.VISIBLE) {
                sHousePositionSelected = mRegisterExtraOttModel.getPosition();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sHouseType == 1) /*nhà Phố*/ {
            // diachi = số nhà + tổ ấp + , Phường + , Quận + , Tỉnh
            if (!Common.isEmpty(fragCusInfo.txtHouseNum))
                result += fragCusInfo.txtHouseNum.getText();
            if (!sStreetSelected.isEmpty())
                result += " " + sStreetSelected;

        } else if (sHouseType == 2) /*Chung Cư*/ {
            // dia chi = lo + tang + phòng + tên chung cư
            if (!Common.isEmpty(fragCusInfo.txtLot))
                result += "Lo " + fragCusInfo.txtLot.getText() + ", ";

            if (!Common.isEmpty(fragCusInfo.txtFloor))
                result += "T. " + fragCusInfo.txtFloor.getText() + ", ";

            if (!Common.isEmpty(fragCusInfo.txtRoom))
                result += "P. " + fragCusInfo.txtRoom.getText() + " ";

            if (!sApartmentSelected.isEmpty())
                result += sApartmentSelected + " ";

            if (!sStreetSelected.isEmpty())
                result += sStreetSelected;

        } else if (sHouseType == 3) /*Nhà không địa chỉ*/ {
            if (!Common.isEmpty(fragCusInfo.txtHouseNum))
                result += fragCusInfo.txtHouseNum.getText() + " ";

            if (!sStreetSelected.isEmpty())
                result += sStreetSelected;

            if (sHousePositionSelected > 0) {
                try {
                    String position = fragCusInfo.tvHousePosition != null ?
                            fragCusInfo.tvHousePosition.getText().toString() : "";
                    result += "(" + Common.convertVietNamToEnglishChar(position) + ")";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        String sWardSelected = fragCusInfo.tvWard != null ?
                fragCusInfo.tvWard.getText().toString() : "";
        if (!sWardSelected.isEmpty())
            result += ", " + sWardSelected;
        if (!sDistrictSelected.equals("-1"))
            result += ", " + sDistrictSelected + ", " + sCityNameValue;

        return result;
    }

    @Override
    public void onBackPressed() {
        try {
            Dialog dialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.txt_message_exits_create_registration))
                    .setCancelable(false)
                    .setPositiveButton(
                            getResources().getString(R.string.lbl_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                }
                            })
                    .setNegativeButton(
                            getResources().getString(R.string.lbl_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
