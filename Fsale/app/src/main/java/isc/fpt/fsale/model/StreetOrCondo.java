package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haulc3 on 19,July,2019
 */
public class StreetOrCondo implements Parcelable {
    @SerializedName("ErrorService")
    @Expose
    private Object errorService;
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("NameVN")
    @Expose
    private String nameVN;

    protected StreetOrCondo(Parcel in) {
        id = in.readString();
        name = in.readString();
        nameVN = in.readString();
    }

    public static final Creator<StreetOrCondo> CREATOR = new Creator<StreetOrCondo>() {
        @Override
        public StreetOrCondo createFromParcel(Parcel in) {
            return new StreetOrCondo(in);
        }

        @Override
        public StreetOrCondo[] newArray(int size) {
            return new StreetOrCondo[size];
        }
    };

    public Object getErrorService() {
        return errorService;
    }

    public void setErrorService(Object errorService) {
        this.errorService = errorService;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameVN() {
        return nameVN;
    }

    public void setNameVN(String nameVN) {
        this.nameVN = nameVN;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(nameVN);
    }
}
