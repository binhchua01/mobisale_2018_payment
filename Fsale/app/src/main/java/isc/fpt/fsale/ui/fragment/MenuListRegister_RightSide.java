package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.slidingmenu.lib.SlidingMenu;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.MobiSaleResetFPTPlay;
import isc.fpt.fsale.action.MobiSaleResetShortlink;
import isc.fpt.fsale.activity.BaseActivity;
import isc.fpt.fsale.activity.CreateContractActivity;
import isc.fpt.fsale.activity.DeployAppointmentActivity;
import isc.fpt.fsale.activity.InvestiageActivity;
import isc.fpt.fsale.activity.MapBookPortAuto;
import isc.fpt.fsale.activity.PTCDetailActivity;
import isc.fpt.fsale.activity.PayContractActivity;
import isc.fpt.fsale.activity.PaymentRetailAdditionalActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.activity.UpdateInternetReceiptActivity;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.activity.BookPortManualActivity;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.ui.extra_ott.ExtraOttActivity;
import isc.fpt.fsale.ui.fpt_camera.FptCameraActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// menu phải chi tiết phiếu đăng ký
public class MenuListRegister_RightSide extends ListFragment {
    private static final int MENU_REG_MANAGEMENT_GROUP = 0;
    private Context mContext;
    private FragmentManager fm;
    private RegistrationDetailModel mRegister;
    RightMenuListItem menuItemTitle, menuItemUpdateRegistration, menuItemBookPort,
            menuItemInvest, menuItemDeployAppointment, menuItemUpdateReceipt,
            menuItemChangePTCStatus, menuItemUpdateRegisterContract, menuItemDepositRegisterContract,
            menuItemCreateContract, menuItemPayContract, menuItemSeeDeployCapacityByDay,
            menuItemShortLink, menuItemResetFPTPlay;

    @SuppressLint("ValidFragment")
    public MenuListRegister_RightSide(RegistrationDetailModel model) {
        this.mRegister = model;
    }

    public MenuListRegister_RightSide() {
    }

    @SuppressLint("InflateParams")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_menu, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mContext = getActivity();
        fm = this.getFragmentManager();
        initAdapterFromRegister(mRegister);
    }

    public void initAdapterFromRegister(RegistrationDetailModel register) {
        this.mRegister = register;
        RightMenuListAdapter adapter = new RightMenuListAdapter(getActivity());
        menuItemTitle = new RightMenuListItem(getResources().getString(R.string.menu_reg_management_group), -1);
        adapter.add(menuItemTitle);
        if (mRegister != null) {
            if (mRegister.getCheckListRegistration() != null) {
                setupMenu(adapter);
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_emty_data_container_status_for_register), mContext);
            }
        }
        setListAdapter(adapter);
    }

    private void setupMenu(RightMenuListAdapter adapter) {
        showButtonUpdate(adapter);
        showButtonCreateContract(adapter);
        showButtonBookPort(adapter);
        showButtonPayment(adapter);
        showButtonInvest(adapter);
        showButtonDeployCapacityByDay(adapter);
        showButtonResetShortLink(adapter);
        showButtonResetTKFPTPlay(adapter);
        showButtonChangePtcStatus();
        showButtonAppointment(adapter);
        showButtonUpdateReceipt(adapter);
    }

    private void showButtonUpdate(RightMenuListAdapter adapter) {
        // Cập nhật
        if ((mRegister.getRegType() == 0 && mRegister.getContract().equals("")) ||
                (mRegister.getRegType() != 0 &&
                        mRegister.getCheckListRegistration().getTrangThaiThanhToan() != Constants.DEPOSIT_STATUS_PAYED)) {
            menuItemUpdateRegistration = new RightMenuListItem(getResources().getString(R.string.menu_update_register),
                    R.drawable.ic_edit_register);
            adapter.add(menuItemUpdateRegistration);
        }
    }

    private void showButtonCreateContract(RightMenuListAdapter adapter) {
        // Tạo hợp đồng
        if (mRegister.getRegType() == 0 && mRegister.getContract().equals("")) {
            menuItemCreateContract = new RightMenuListItem(getString(R.string.create_contract_header_activity),
                    R.drawable.ic_menu_item_collect_money);
            adapter.add(menuItemCreateContract);
        }
    }

    private void showButtonBookPort(RightMenuListAdapter adapter) {
        // Bookport
        if (mRegister.getRegType() == 0 && mRegister.getContract().equals("") &&
                (mRegister.getCheckListService().isInternet()
                        || mRegister.getCheckListService().isIPTV()
                        || mRegister.getCheckListService().isMaxy())) {
            menuItemBookPort = new RightMenuListItem(getResources().getString(R.string.menu_book_port),
                    R.drawable.ic_book_port);
            adapter.add(menuItemBookPort);
        }
    }

    private void showButtonPayment(RightMenuListAdapter adapter) {
        // Thanh toán
        if ((mRegister.getRegType() == 0 && !mRegister.getContract().equals("") &&
                mRegister.getCheckListRegistration().getTrangThaiThanhToan() != Constants.DEPOSIT_STATUS_PAYED) ||
                mRegister.getRegType() != 0 &&
                        mRegister.getCheckListRegistration().getTrangThaiThanhToan() != Constants.DEPOSIT_STATUS_PAYED) {
            menuItemPayContract = new RightMenuListItem(getString(R.string.pay_contract_header_activity),
                    R.drawable.ic_menu_item_collect_money);
            adapter.add(menuItemPayContract);
        }
    }

    private void showButtonInvest(RightMenuListAdapter adapter) {
        // Khảo sát
        if (mRegister.getRegType() == 0 && mRegister.getCheckListRegistration().isBookPort() &&
                !mRegister.getCheckListRegistration().isHopDong() &&
                (mRegister.getCheckListService().isInternet()
                        || mRegister.getCheckListService().isIPTV()
                        || mRegister.getCheckListService().isMaxy())) {
            menuItemInvest = new RightMenuListItem(getResources().getString(R.string.menu_invest),
                    R.drawable.ic_view_reason_slide);
            adapter.add(menuItemInvest);
        }
    }

    private void showButtonDeployCapacityByDay(RightMenuListAdapter adapter) {
        // Xem năng lực triển khai
        if (mRegister.getCheckListService().isInternet() ||
                mRegister.getCheckListService().isIPTV() ||
                mRegister.getCheckListService().isCamera() ||
                mRegister.getCheckListService().isDevice()||
                mRegister.getCheckListService().isMaxy()) {
            menuItemSeeDeployCapacityByDay = new RightMenuListItem(getString(R.string.title_activity_deploy_capacity_by_day),
                    R.drawable.ic_edit_register);
            adapter.add(menuItemSeeDeployCapacityByDay);
        }
    }

    private void showButtonResetShortLink(RightMenuListAdapter adapter) {
        // Reset shorlink
        if (mRegister.getRegType() == 0 && !mRegister.getContract().equals("")) {
            menuItemShortLink = new RightMenuListItem(getString(R.string.lbl_btn_reset_short_link),
                    R.drawable.ic_edit_register);
            adapter.add(menuItemShortLink);
        }
    }

    private void showButtonResetTKFPTPlay(RightMenuListAdapter adapter) {
        // Reset TK FPT Play
        if (mRegister.getStatusDeposit() == 1 && !mRegister.getContract().equals("") && mRegister.getObjectMaxy() != null) {
            if (mRegister.getObjectMaxy().getMaxyGeneral().getPromotionID() > 0) {
                menuItemResetFPTPlay = new RightMenuListItem(getString(R.string.lbl_btn_reset_fpt_play),
                        R.drawable.ic_edit_register);
                adapter.add(menuItemResetFPTPlay);
            }
        }
    }

    private void showButtonChangePtcStatus() {
        // Phiếu thi công
        if (mRegister.getCheckListRegistration().getTrangThaiThanhToan() == Constants.DEPOSIT_STATUS_PAYED) {
            if (mRegister.getCheckListService().isInternet() ||
                    mRegister.getCheckListService().isIPTV() ||
                    mRegister.getCheckListService().isCamera() ||
                    mRegister.getCheckListService().isDevice() ||
                    mRegister.getCheckListService().isMaxy()) {
                menuItemChangePTCStatus = new RightMenuListItem(getString(R.string.menu_change_ptc_status),
                        R.drawable.ic_report_prechecklist);
            }
        }
    }

    private void showButtonAppointment(RightMenuListAdapter adapter) {
        // Hẹn triển khai
        if (!mRegister.getContract().equals("")) {
            if (mRegister.getCheckListService().isInternet() ||
                    mRegister.getCheckListService().isIPTV() ||
                    mRegister.getCheckListService().isCamera() ||
                    mRegister.getCheckListService().isDevice() ||
                    mRegister.getCheckListService().isMaxy()) {
                menuItemDeployAppointment = new RightMenuListItem(getResources().getString(R.string.menu_depoy_appointment),
                        R.drawable.ic_menu_item_deploy_appointment);
                adapter.add(menuItemDeployAppointment);
            }
        }
    }

    private void showButtonUpdateReceipt(RightMenuListAdapter adapter) {
        // Cập nhật tổng tiền
        if (mRegister.getRegType() == 0 && !mRegister.getContract().equals("")
                && mRegister.getCheckListService().isInternet()) {
            menuItemUpdateReceipt = new RightMenuListItem(getString(R.string.menu_update_receipt_register), R.drawable.ic_edit_register);
            adapter.add(menuItemUpdateReceipt);
        }
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onListItemClick(ListView lv, View v, int position, long id) {
        RightMenuListItem item = (RightMenuListItem) getListAdapter().getItem(position);
        if (item == menuItemBookPort) {
            try {
                //close menu right
                ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
                int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getApplicationContext());
                if (resultCode == ConnectionResult.SUCCESS) {
                    GPSTracker gpsTracker = new GPSTracker(mContext);
                    if (gpsTracker.canGetLocation()) {
                        Intent intent;
                        if (Constants.AutoBookPort == 0) {
                            intent = new Intent(mContext, BookPortManualActivity.class);
                        } else {
                            intent = new Intent(mContext, MapBookPortAuto.class);
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                        mContext.startActivity(intent);

                    }
                } else if (resultCode == ConnectionResult.SERVICE_MISSING ||
                        resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED ||
                        resultCode == ConnectionResult.SERVICE_DISABLED) {
                    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), 1);
                    dialog.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (item == menuItemInvest) {
            try {
                ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
                // Kiểm tra xem có bookport chưa. Nếu chưa bookport thì không cho khảo sát
                if (mRegister.getCheckListRegistration() != null) {
                    if (mRegister.getCheckListRegistration().isBookPort()) {
                        Intent intent = new Intent(mContext, InvestiageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                        mContext.startActivity(intent);
                    } else {
                        Common.alertDialog("Vui lòng bookport trước khi khảo sát.", mContext);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (item == menuItemUpdateRegistration) {//CẬP NHẬT
            try {
                ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
                if (mRegister.getCheckListRegistration() != null) {
                    if (mRegister.getCheckListRegistration().getTrangThaiThanhToan() != 0) {
                        Intent intent = null;
                        if (mRegister.getRegType() > 0) {
                            // if (mRegister.getCategoryServiceGroupID() == 3) {
                            intent = new Intent(mContext, RegisterContractActivity.class);
//                            } else {
//
//                            }
                        } else {
                            if (mRegister.getCheckListService().isExtraOTT()) {
                                intent = new Intent(mContext, ExtraOttActivity.class);
                            } else if (mRegister.getCheckListService().isCamera()) {
                                intent = new Intent(mContext, FptCameraActivity.class);
                            } else {
                                intent = new Intent(mContext, RegisterActivityNew.class);
                            }
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                        mContext.startActivity(intent);
                    } else {
                        Common.alertDialog("TTKH đã thu tiền!", mContext);
                    }
                } else {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_emty_data_container_status_for_register), mContext);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            // HẸN TRIỂN KHAI
        } else if (item == menuItemDeployAppointment) {
            try {
                ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
                Intent intent = new Intent(mContext, DeployAppointmentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (item == menuItemUpdateReceipt) {//CẬP NHẬT TỔNG TIỀN
            try {
                ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
                Intent intent = new Intent(mContext, UpdateInternetReceiptActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (item == menuItemChangePTCStatus) {// PHIẾU THI CÔNG
            try {
                ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
                Intent intent = new Intent(mContext, PTCDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (item == menuItemUpdateRegisterContract) {// CẬP NHẬT PDK BÁN THÊM
            try {
                ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
                Intent intent = new Intent(mContext, RegisterContractActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (item == menuItemDepositRegisterContract) {//THANH TOÁN BÁN THÊM
            try {
                ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
                Intent intent = new Intent(mContext, PaymentRetailAdditionalActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (item == menuItemCreateContract) {
            ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
            Intent intent = new Intent(mContext, CreateContractActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.MODEL_REGISTER, mRegister);
            mContext.startActivity(intent);
        } else if (item == menuItemPayContract) {
            ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
            Intent intent;
            if (mRegister.getRegType() != 0) {//bán thêm
                intent = new Intent(mContext, PaymentRetailAdditionalActivity.class);
            } else {//bán mới
                intent = new Intent(mContext, PayContractActivity.class);
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.MODEL_REGISTER, mRegister);
            mContext.startActivity(intent);

        } else if (item == menuItemSeeDeployCapacityByDay) {
            ((BaseActivity) mContext).toggle(SlidingMenu.RIGHT);
            Intent intent = new Intent(mContext, DeployAppointmentActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.MODEL_REGISTER, mRegister);
            intent.putExtra(Constants.TAG_DEPLOY_CAPACITY, "DEPLOY_CAPACITY");
            startActivity(intent);
        } else if (item == menuItemShortLink) {
            new MobiSaleResetShortlink(mContext, mRegister);
        } else if (item == menuItemResetFPTPlay) {
            new MobiSaleResetFPTPlay(mContext, mRegister);
        }

    }

    /**
     * MODEL: RightMenuListItem
     * ============================================================
     */
    public class RightMenuListItem {
        public String tag;
        private int iconRes;
        private int VISIBLE;

        RightMenuListItem(String tag, int iconRes) {
            this.tag = tag;
            this.iconRes = iconRes;
        }

        public void setVisible(int visible) {
            this.VISIBLE = visible;
        }

        public int getVisible() {
            return this.VISIBLE;
        }
    }

    /**
     * ADAPTER: RightMenuListAdapter
     * ==============================================================
     */
    public class RightMenuListAdapter extends ArrayAdapter<RightMenuListItem> {
        RightMenuListAdapter(Context context) {
            super(context, 0);
        }

        @Override
        public int getPosition(RightMenuListItem item) {
            return super.getPosition(item);
        }

        @NonNull
        @SuppressLint("InflateParams")
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_menu, null);
            }

            if (getItem(position).getVisible() == View.GONE) {
                convertView.setVisibility(View.GONE);
            }
            TextView title = (TextView) convertView.findViewById(R.id.item_title);
            title.setText(getItem(position).tag);
            ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
            //TruongPV fix set image resource
            if (getItem(position).iconRes != -1) {
                icon.setImageResource(getItem(position).iconRes);
            }

            if (position == MENU_REG_MANAGEMENT_GROUP) {
                title.setTypeface(null, Typeface.BOLD);
                title.setTextSize(15);
                title.setTextColor(getResources().getColor(R.color.text_header_slide));
                title.setPadding(0, 10, 0, 10);
                // set background for header slide
                convertView.setBackgroundColor(getResources().getColor(R.color.bg_header_slide));
            } else
                title.setPadding(0, 20, 0, 20);

            return convertView;
        }
    }
}
