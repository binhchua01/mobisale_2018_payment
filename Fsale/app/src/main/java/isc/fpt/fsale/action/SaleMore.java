package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ConfirmPaymentRetailAdditionalActivity;
import isc.fpt.fsale.activity.PaymentRetailAdditionalOnlineActivity;
import isc.fpt.fsale.activity.conllection_bank.CollectionInfoBanking;
import isc.fpt.fsale.model.QRCodeResult;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.StartPaymentResult;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by HCM.TUANTT14 on 8/27/2018.
 */

public class SaleMore implements AsyncTaskCompleteListener<String> {
    private String[] arrParamName, arrParamValue;
    private Context mContext;
    private final String START_PAYMENT = "SaleMore";
    private RegistrationDetailModel registrationDetailModel;
    private String paidValue;

    //Constructor
    public SaleMore(Context context, String paidValue, int paidType, String linkImageSignature,
                    RegistrationDetailModel mRegister) {
        try {
            this.mContext = context;
            this.paidValue = paidValue;
            this.registrationDetailModel = mRegister;
            arrParamName = new String[]{"SaleName", "RegCode", "Contract", "PaidType", "ImageSignature", "Total"};
            this.arrParamValue = new String[]{mRegister.getUserName(), mRegister.getRegCode(),
                    mRegister.getContract(), String.valueOf(paidType), linkImageSignature, String.valueOf(mRegister.getTotal())};
            String message = mContext.getResources().getString(R.string.msg_pd_process_complete_payment);
            CallServiceTask service = new CallServiceTask(mContext, START_PAYMENT, arrParamName,
                    arrParamValue, Services.JSON_POST, message, SaleMore.this);
            service.execute();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<StartPaymentResult> objList;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject("ResponseResult");
                WSObjectsModel<StartPaymentResult> resultObject = new WSObjectsModel<>(jsObj,
                        StartPaymentResult.class, QRCodeResult.class.getName());
                if (resultObject.getErrorCode() == 0) {
                    objList = resultObject.getArrayListObject();
                    if (objList.size() > 0) {
                        StartPaymentResult startPaymentResult = objList.get(0);
                        if (startPaymentResult.getId() > 0) {
                            switch (startPaymentResult.getId()) {
                                //kết quả thanh toán thẻ quốc tế và nội địa
                                case 30:
                                case 40:
                                case 80:
                                    Intent intentQrCode = new Intent(mContext, PaymentRetailAdditionalOnlineActivity.class);
                                    intentQrCode.putExtra("PAID_TYPE_SELECTED", startPaymentResult.getId());
                                    intentQrCode.putExtra("PAID_TYPE_VALUE", this.paidValue);
                                    intentQrCode.putExtra("BASE64_QR_CODE", startPaymentResult.getQrCodeResult().getQrcode());
                                    intentQrCode.putExtra(Constants.MODEL_REGISTER, this.registrationDetailModel);
                                    mContext.startActivity(intentQrCode);
                                    break;
                                //kết quả thanh toán TPBANK
                                case 50:
                                    Intent intentTPBank = new Intent(mContext, PaymentRetailAdditionalOnlineActivity.class);
                                    intentTPBank.putExtra("PAID_TYPE_SELECTED", startPaymentResult.getId());
                                    intentTPBank.putExtra("PAID_TYPE_VALUE", this.paidValue);
                                    intentTPBank.putExtra(Constants.MODEL_REGISTER, this.registrationDetailModel);
                                    mContext.startActivity(intentTPBank);
                                    break;
                                //kết quả thanh toán tiền mặt
                                case 10:
                                case 100:
                                    if (Common.checkLifeActivity(mContext)) {
                                        new AlertDialog.Builder(mContext)
                                                .setTitle("Thông Báo")
                                                .setMessage(objList.get(0).getMessage())
                                                .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        Intent confirmIntent = new Intent(mContext, ConfirmPaymentRetailAdditionalActivity.class);
                                                        confirmIntent.putExtra("PAID_TYPE_VALUE", paidValue);
                                                        confirmIntent.putExtra(Constants.MODEL_REGISTER, registrationDetailModel);
                                                        mContext.startActivity(confirmIntent);
                                                        dialog.cancel();
                                                        ((Activity) mContext).finish();
                                                    }
                                                }).setCancelable(false).create().show();
                                    }
                                    break;
                                //kết quả thanh toán HiFpt hoặc các loại thanh toán chưa biết
                                case 60:
                                    Intent intent = new Intent(mContext, CollectionInfoBanking.class);
                                    intent.putExtra(Constants.TAG_RESULT_COLLECTION_BANKING, startPaymentResult.getMBBankGateway());
                                    mContext.startActivity(intent);
                                    break;
                                case 20:
                                case 70:
                                default:
                                    Common.alertDialog(objList.get(0).getMessage(), mContext);
                                    break;
                            }
                        } else {
                            Common.alertDialog(resultObject.getError(), mContext);
                        }
                    } else {
                        Common.alertDialog(resultObject.getError(), mContext);
                    }
                } else {
                    Common.alertDialog(resultObject.getError(), mContext);
                }
            }
        } catch (JSONException ex) {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
        }
    }
}
