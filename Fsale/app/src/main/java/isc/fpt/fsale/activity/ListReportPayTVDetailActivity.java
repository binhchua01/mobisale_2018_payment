package isc.fpt.fsale.activity;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetReportPayTVDetail;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ReportPayTVAdapter;
import isc.fpt.fsale.adapter.ReportPayTVDetailAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportPayTVDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

public class ListReportPayTVDetailActivity extends BaseActivity {
    // Phan trang
    private int mPage = 1, mLocalType = 0, mPackage = 0, mMonth = 0, mYear = 0;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
    private TextView lblSaleName;
    private String saleName = "", fromDate = "", toDate = "";

    private Spinner spPage;
    private ListView lvResult;


    public ListReportPayTVDetailActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.menu_report_pay_tv);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.menu_report_pay_tv));
        setContentView(R.layout.activity_report_potential_obj);
        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem != null) {
                    FLAG_FIRST_LOAD++;
                    if (FLAG_FIRST_LOAD > 1) {
                        if (mPage != selectedItem.getID()) {
                            mPage = selectedItem.getID();
                            getData(mPage);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        lvResult = (ListView) findViewById(R.id.lv_report_sbi_detail);
        lblSaleName = (TextView) findViewById(R.id.lbl_sale);
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        // TODO Auto-generated method stub
        Intent intent = getIntent();
        if (intent != null) {
            this.mLocalType = intent.getIntExtra(ReportPayTVAdapter.TAG_LOCAL_TYPE, 0);
            this.mPackage = intent.getIntExtra(ReportPayTVAdapter.TAG_PACKAGE, 0);
            this.fromDate = intent.getStringExtra(ReportPayTVAdapter.TAG_FROMDATE);
            this.toDate = intent.getStringExtra(ReportPayTVAdapter.TAG_TODATE);
            this.mMonth = intent.getIntExtra(ReportPayTVAdapter.TAG_MONTH, 0);
            this.mYear = intent.getIntExtra(ReportPayTVAdapter.TAG_YEAR, 0);
            this.saleName = intent.getStringExtra(ReportPayTVAdapter.TAG_SALE_NAME);
            lblSaleName.setText(this.saleName);
            getData(0);
        }
    }

    private void getData(int PageNumber) {

        new GetReportPayTVDetail(this, this.saleName, mMonth, mYear, mLocalType, mPackage, 0, "", PageNumber, fromDate, toDate);
        //GetReportPayTVDetail(Context context, String UserName, int Month, int Year, int LocalType, int Package, int Agent, String AgentName, int PageNumber)
    }

    public void loadData(ArrayList<ReportPayTVDetailModel> lst) {
        if (lst != null && lst.size() > 0) {
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            ReportPayTVDetailAdapter adapter = new ReportPayTVDetailAdapter(this, lst);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
            lvResult.setAdapter(new ReportPayTVDetailAdapter(this, new ArrayList<ReportPayTVDetailModel>()));
        }

    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
