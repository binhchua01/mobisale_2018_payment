package isc.fpt.fsale.model;

import java.lang.reflect.Field;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONObject;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table REPORT_SALARY_MODEL.
 */
public class ReportSalaryModel {

    private Integer RowNumber;
    private Integer TotalRow;
    private String FullName;
    private String PsID;
    private String Name;
    private String CN;
    private String Dept;
    private String DateSalary;
    private Integer ADSL_MegaYOU_Flat;
    private Integer ADSL_MegaYOU2_Flat;
    private Integer ADSL_MegaME_Flat;
    private Integer ADSL_MegaME2_Flat;
    private Integer ADSL_MegaSAVE_Flat;
    private Integer ADSL_MegaSAVE2_Flat;
    private Integer ADSL_MegaNET_Flat;
    private Integer ADSL_MegaNET2_Flat;
    private Integer FTTH_FiberBronze_Flat;
    private Integer FTTH_FiberSilver_Flat;
    private Integer FTTH_FiberGold_Flat;
    private Integer FTTH_FiberDiamond_Flat;
    private Integer FTTH_FiberPublicPlus_Flat;
    private Integer FTTH_FiberBusiness_Flat;
    private Integer FTTH_Fiber_Flat;
    private Integer FTTH_FiberPlay_Flat;
    private Integer FTTH_FiberHome_Flat;
    private Integer FTTH_FiberPlus_Flat;
    private Integer FTTH_FiberMe_Flat;
    private Integer FTTH_FiberFamily_Flat;
    private Integer FTTH_FiberHome_Flat_EOC;
    private Integer Total;
    private Integer OneTV;
    private Integer PlayHD;
    private Integer AddOn;
    private Integer SubQuantity;
    private Integer SubBoxQuantity;
    private Integer NETQuantity;
    private Integer BasicSalary;
    private Integer MonthCommission;
    private Integer Week1;
    private Integer Week2;
    private Integer Week3;
    private Integer Week4;
    private Integer PromIncome;
    private Integer PromPrice;
    private Integer PrepaidNew;
    private Integer IncomeSalary;
    private Integer TPlus2;
    private Integer TPlus4;
    private Integer TPlus6;
    private Integer TPlus12;
    private Integer QuantityTPlus2;
    private Integer QuantityTPlus4;
    private Integer QuantityTPlus6;
    private Integer QuantityTPlus12;
    private Integer TPlusTotal;
    private Integer TMin2;
    private Integer TMin4;
    private Integer TMin6;
    private Integer TMin8;
    private Integer TMin10;
    private Integer TMin12;
    private Integer QuantityTMin2;
    private Integer QuantityTMin4;
    private Integer QuantityTMin6;
    private Integer QuantityTMin8;
    private Integer QuantityTMin10;
    private Integer QuantityTMin12;
    private Integer TMinTotal;
    private Integer BoxTPlus2;
    private Integer BoxTPlus4;
    private Integer BoxTPlus6;
    private Integer BoxTPlus12;
    private Integer QuantityBoxTPlus2;
    private Integer QuantityBoxTPlus4;
    private Integer QuantityBoxTPlus6;
    private Integer QuantityBoxTPlus12;
    private Integer BoxTPlusTotal;
    private Integer BoxTMin2;
    private Integer BoxTMin4;
    private Integer BoxTMin6;
    private Integer BoxTMin8;
    private Integer BoxTMin10;
    private Integer BoxTMin12;
    private Integer QuantityBoxTMin2;
    private Integer QuantityBoxTMin4;
    private Integer QuantityBoxTMin6;
    private Integer QuantityBoxTMin8;
    private Integer QuantityBoxTMin10;
    private Integer QuantityBoxTMin12;
    private Integer BoxTMinTotal;
    private Integer TRecover;
    private Integer QuantityTRecover;
    private Integer SubstractIncome;
    private Integer HDTotal;
    private Integer VODTotal;
    private Integer NetSalary;
    private Integer TotalPage;
    private Integer CurrentPage;
    // Tuấn cập nhật code 28082017 phần hiển thị báo cáo lương html.
    private String Html;
    public ReportSalaryModel() {
    }

    public ReportSalaryModel(Integer RowNumber, Integer TotalRow, String FullName, String PsID, String Name, String CN, String Dept, String DateSalary, Integer ADSL_MegaYOU_Flat, Integer ADSL_MegaYOU2_Flat, Integer ADSL_MegaME_Flat, Integer ADSL_MegaME2_Flat, Integer ADSL_MegaSAVE_Flat, Integer ADSL_MegaSAVE2_Flat, Integer ADSL_MegaNET_Flat, Integer ADSL_MegaNET2_Flat, Integer FTTH_FiberBronze_Flat, Integer FTTH_FiberSilver_Flat, Integer FTTH_FiberGold_Flat, Integer FTTH_FiberDiamond_Flat, Integer FTTH_FiberPublicPlus_Flat, Integer FTTH_FiberBusiness_Flat, Integer FTTH_Fiber_Flat, Integer FTTH_FiberPlay_Flat, Integer FTTH_FiberHome_Flat, Integer FTTH_FiberPlus_Flat, Integer FTTH_FiberMe_Flat, Integer FTTH_FiberFamily_Flat, Integer FTTH_FiberHome_Flat_EOC, Integer Total, Integer OneTV, Integer PlayHD, Integer AddOn, Integer SubQuantity, Integer SubBoxQuantity, Integer NETQuantity, Integer BasicSalary, Integer MonthCommission, Integer Week1, Integer Week2, Integer Week3, Integer Week4, Integer PromIncome, Integer PromPrice, Integer PrepaidNew, Integer IncomeSalary, Integer TPlus2, Integer TPlus4, Integer TPlus6, Integer TPlus12, Integer QuantityTPlus2, Integer QuantityTPlus4, Integer QuantityTPlus6, Integer QuantityTPlus12, Integer TPlusTotal, Integer TMin2, Integer TMin4, Integer TMin6, Integer TMin8, Integer TMin10, Integer TMin12, Integer QuantityTMin2, Integer QuantityTMin4, Integer QuantityTMin6, Integer QuantityTMin8, Integer QuantityTMin10, Integer QuantityTMin12, Integer TMinTotal, Integer BoxTPlus2, Integer BoxTPlus4, Integer BoxTPlus6, Integer BoxTPlus12, Integer QuantityBoxTPlus2, Integer QuantityBoxTPlus4, Integer QuantityBoxTPlus6, Integer QuantityBoxTPlus12, Integer BoxTPlusTotal, Integer BoxTMin2, Integer BoxTMin4, Integer BoxTMin6, Integer BoxTMin8, Integer BoxTMin10, Integer BoxTMin12, Integer QuantityBoxTMin2, Integer QuantityBoxTMin4, Integer QuantityBoxTMin6, Integer QuantityBoxTMin8, Integer QuantityBoxTMin10, Integer QuantityBoxTMin12, Integer BoxTMinTotal, Integer TRecover, Integer QuantityTRecover, Integer SubstractIncome, Integer HDTotal, Integer VODTotal, Integer NetSalary, Integer TotalPage, Integer CurrentPage) {
        this.RowNumber = RowNumber;
        this.TotalRow = TotalRow;
        this.FullName = FullName;
        this.PsID = PsID;
        this.Name = Name;
        this.CN = CN;
        this.Dept = Dept;
        this.DateSalary = DateSalary;
        this.ADSL_MegaYOU_Flat = ADSL_MegaYOU_Flat;
        this.ADSL_MegaYOU2_Flat = ADSL_MegaYOU2_Flat;
        this.ADSL_MegaME_Flat = ADSL_MegaME_Flat;
        this.ADSL_MegaME2_Flat = ADSL_MegaME2_Flat;
        this.ADSL_MegaSAVE_Flat = ADSL_MegaSAVE_Flat;
        this.ADSL_MegaSAVE2_Flat = ADSL_MegaSAVE2_Flat;
        this.ADSL_MegaNET_Flat = ADSL_MegaNET_Flat;
        this.ADSL_MegaNET2_Flat = ADSL_MegaNET2_Flat;
        this.FTTH_FiberBronze_Flat = FTTH_FiberBronze_Flat;
        this.FTTH_FiberSilver_Flat = FTTH_FiberSilver_Flat;
        this.FTTH_FiberGold_Flat = FTTH_FiberGold_Flat;
        this.FTTH_FiberDiamond_Flat = FTTH_FiberDiamond_Flat;
        this.FTTH_FiberPublicPlus_Flat = FTTH_FiberPublicPlus_Flat;
        this.FTTH_FiberBusiness_Flat = FTTH_FiberBusiness_Flat;
        this.FTTH_Fiber_Flat = FTTH_Fiber_Flat;
        this.FTTH_FiberPlay_Flat = FTTH_FiberPlay_Flat;
        this.FTTH_FiberHome_Flat = FTTH_FiberHome_Flat;
        this.FTTH_FiberPlus_Flat = FTTH_FiberPlus_Flat;
        this.FTTH_FiberMe_Flat = FTTH_FiberMe_Flat;
        this.FTTH_FiberFamily_Flat = FTTH_FiberFamily_Flat;
        this.FTTH_FiberHome_Flat_EOC = FTTH_FiberHome_Flat_EOC;
        this.Total = Total;
        this.OneTV = OneTV;
        this.PlayHD = PlayHD;
        this.AddOn = AddOn;
        this.SubQuantity = SubQuantity;
        this.SubBoxQuantity = SubBoxQuantity;
        this.NETQuantity = NETQuantity;
        this.BasicSalary = BasicSalary;
        this.MonthCommission = MonthCommission;
        this.Week1 = Week1;
        this.Week2 = Week2;
        this.Week3 = Week3;
        this.Week4 = Week4;
        this.PromIncome = PromIncome;
        this.PromPrice = PromPrice;
        this.PrepaidNew = PrepaidNew;
        this.IncomeSalary = IncomeSalary;
        this.TPlus2 = TPlus2;
        this.TPlus4 = TPlus4;
        this.TPlus6 = TPlus6;
        this.TPlus12 = TPlus12;
        this.QuantityTPlus2 = QuantityTPlus2;
        this.QuantityTPlus4 = QuantityTPlus4;
        this.QuantityTPlus6 = QuantityTPlus6;
        this.QuantityTPlus12 = QuantityTPlus12;
        this.TPlusTotal = TPlusTotal;
        this.TMin2 = TMin2;
        this.TMin4 = TMin4;
        this.TMin6 = TMin6;
        this.TMin8 = TMin8;
        this.TMin10 = TMin10;
        this.TMin12 = TMin12;
        this.QuantityTMin2 = QuantityTMin2;
        this.QuantityTMin4 = QuantityTMin4;
        this.QuantityTMin6 = QuantityTMin6;
        this.QuantityTMin8 = QuantityTMin8;
        this.QuantityTMin10 = QuantityTMin10;
        this.QuantityTMin12 = QuantityTMin12;
        this.TMinTotal = TMinTotal;
        this.BoxTPlus2 = BoxTPlus2;
        this.BoxTPlus4 = BoxTPlus4;
        this.BoxTPlus6 = BoxTPlus6;
        this.BoxTPlus12 = BoxTPlus12;
        this.QuantityBoxTPlus2 = QuantityBoxTPlus2;
        this.QuantityBoxTPlus4 = QuantityBoxTPlus4;
        this.QuantityBoxTPlus6 = QuantityBoxTPlus6;
        this.QuantityBoxTPlus12 = QuantityBoxTPlus12;
        this.BoxTPlusTotal = BoxTPlusTotal;
        this.BoxTMin2 = BoxTMin2;
        this.BoxTMin4 = BoxTMin4;
        this.BoxTMin6 = BoxTMin6;
        this.BoxTMin8 = BoxTMin8;
        this.BoxTMin10 = BoxTMin10;
        this.BoxTMin12 = BoxTMin12;
        this.QuantityBoxTMin2 = QuantityBoxTMin2;
        this.QuantityBoxTMin4 = QuantityBoxTMin4;
        this.QuantityBoxTMin6 = QuantityBoxTMin6;
        this.QuantityBoxTMin8 = QuantityBoxTMin8;
        this.QuantityBoxTMin10 = QuantityBoxTMin10;
        this.QuantityBoxTMin12 = QuantityBoxTMin12;
        this.BoxTMinTotal = BoxTMinTotal;
        this.TRecover = TRecover;
        this.QuantityTRecover = QuantityTRecover;
        this.SubstractIncome = SubstractIncome;
        this.HDTotal = HDTotal;
        this.VODTotal = VODTotal;
        this.NetSalary = NetSalary;
        this.TotalPage = TotalPage;
        this.CurrentPage = CurrentPage;
    }

    public Integer getRowNumber() {
        return RowNumber;
    }

    public void setRowNumber(Integer RowNumber) {
        this.RowNumber = RowNumber;
    }

    public Integer getTotalRow() {
        return TotalRow;
    }

    public void setTotalRow(Integer TotalRow) {
        this.TotalRow = TotalRow;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public String getPsID() {
        return PsID;
    }

    public void setPsID(String PsID) {
        this.PsID = PsID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getCN() {
        return CN;
    }

    public void setCN(String CN) {
        this.CN = CN;
    }

    public String getDept() {
        return Dept;
    }

    public void setDept(String Dept) {
        this.Dept = Dept;
    }

    public String getDateSalary() {
        return DateSalary;
    }

    public void setDateSalary(String DateSalary) {
        this.DateSalary = DateSalary;
    }

    public Integer getADSL_MegaYOU_Flat() {
        return ADSL_MegaYOU_Flat;
    }

    public void setADSL_MegaYOU_Flat(Integer ADSL_MegaYOU_Flat) {
        this.ADSL_MegaYOU_Flat = ADSL_MegaYOU_Flat;
    }

    public Integer getADSL_MegaYOU2_Flat() {
        return ADSL_MegaYOU2_Flat;
    }

    public void setADSL_MegaYOU2_Flat(Integer ADSL_MegaYOU2_Flat) {
        this.ADSL_MegaYOU2_Flat = ADSL_MegaYOU2_Flat;
    }

    public Integer getADSL_MegaME_Flat() {
        return ADSL_MegaME_Flat;
    }

    public void setADSL_MegaME_Flat(Integer ADSL_MegaME_Flat) {
        this.ADSL_MegaME_Flat = ADSL_MegaME_Flat;
    }

    public Integer getADSL_MegaME2_Flat() {
        return ADSL_MegaME2_Flat;
    }

    public void setADSL_MegaME2_Flat(Integer ADSL_MegaME2_Flat) {
        this.ADSL_MegaME2_Flat = ADSL_MegaME2_Flat;
    }

    public Integer getADSL_MegaSAVE_Flat() {
        return ADSL_MegaSAVE_Flat;
    }

    public void setADSL_MegaSAVE_Flat(Integer ADSL_MegaSAVE_Flat) {
        this.ADSL_MegaSAVE_Flat = ADSL_MegaSAVE_Flat;
    }

    public Integer getADSL_MegaSAVE2_Flat() {
        return ADSL_MegaSAVE2_Flat;
    }

    public void setADSL_MegaSAVE2_Flat(Integer ADSL_MegaSAVE2_Flat) {
        this.ADSL_MegaSAVE2_Flat = ADSL_MegaSAVE2_Flat;
    }

    public Integer getADSL_MegaNET_Flat() {
        return ADSL_MegaNET_Flat;
    }

    public void setADSL_MegaNET_Flat(Integer ADSL_MegaNET_Flat) {
        this.ADSL_MegaNET_Flat = ADSL_MegaNET_Flat;
    }

    public Integer getADSL_MegaNET2_Flat() {
        return ADSL_MegaNET2_Flat;
    }

    public void setADSL_MegaNET2_Flat(Integer ADSL_MegaNET2_Flat) {
        this.ADSL_MegaNET2_Flat = ADSL_MegaNET2_Flat;
    }

    public Integer getFTTH_FiberBronze_Flat() {
        return FTTH_FiberBronze_Flat;
    }

    public void setFTTH_FiberBronze_Flat(Integer FTTH_FiberBronze_Flat) {
        this.FTTH_FiberBronze_Flat = FTTH_FiberBronze_Flat;
    }

    public Integer getFTTH_FiberSilver_Flat() {
        return FTTH_FiberSilver_Flat;
    }

    public void setFTTH_FiberSilver_Flat(Integer FTTH_FiberSilver_Flat) {
        this.FTTH_FiberSilver_Flat = FTTH_FiberSilver_Flat;
    }

    public Integer getFTTH_FiberGold_Flat() {
        return FTTH_FiberGold_Flat;
    }

    public void setFTTH_FiberGold_Flat(Integer FTTH_FiberGold_Flat) {
        this.FTTH_FiberGold_Flat = FTTH_FiberGold_Flat;
    }

    public Integer getFTTH_FiberDiamond_Flat() {
        return FTTH_FiberDiamond_Flat;
    }

    public void setFTTH_FiberDiamond_Flat(Integer FTTH_FiberDiamond_Flat) {
        this.FTTH_FiberDiamond_Flat = FTTH_FiberDiamond_Flat;
    }

    public Integer getFTTH_FiberPublicPlus_Flat() {
        return FTTH_FiberPublicPlus_Flat;
    }

    public void setFTTH_FiberPublicPlus_Flat(Integer FTTH_FiberPublicPlus_Flat) {
        this.FTTH_FiberPublicPlus_Flat = FTTH_FiberPublicPlus_Flat;
    }

    public Integer getFTTH_FiberBusiness_Flat() {
        return FTTH_FiberBusiness_Flat;
    }

    public void setFTTH_FiberBusiness_Flat(Integer FTTH_FiberBusiness_Flat) {
        this.FTTH_FiberBusiness_Flat = FTTH_FiberBusiness_Flat;
    }

    public Integer getFTTH_Fiber_Flat() {
        return FTTH_Fiber_Flat;
    }

    public void setFTTH_Fiber_Flat(Integer FTTH_Fiber_Flat) {
        this.FTTH_Fiber_Flat = FTTH_Fiber_Flat;
    }

    public Integer getFTTH_FiberPlay_Flat() {
        return FTTH_FiberPlay_Flat;
    }

    public void setFTTH_FiberPlay_Flat(Integer FTTH_FiberPlay_Flat) {
        this.FTTH_FiberPlay_Flat = FTTH_FiberPlay_Flat;
    }

    public Integer getFTTH_FiberHome_Flat() {
        return FTTH_FiberHome_Flat;
    }

    public void setFTTH_FiberHome_Flat(Integer FTTH_FiberHome_Flat) {
        this.FTTH_FiberHome_Flat = FTTH_FiberHome_Flat;
    }

    public Integer getFTTH_FiberPlus_Flat() {
        return FTTH_FiberPlus_Flat;
    }

    public void setFTTH_FiberPlus_Flat(Integer FTTH_FiberPlus_Flat) {
        this.FTTH_FiberPlus_Flat = FTTH_FiberPlus_Flat;
    }

    public Integer getFTTH_FiberMe_Flat() {
        return FTTH_FiberMe_Flat;
    }

    public void setFTTH_FiberMe_Flat(Integer FTTH_FiberMe_Flat) {
        this.FTTH_FiberMe_Flat = FTTH_FiberMe_Flat;
    }

    public Integer getFTTH_FiberFamily_Flat() {
        return FTTH_FiberFamily_Flat;
    }

    public void setFTTH_FiberFamily_Flat(Integer FTTH_FiberFamily_Flat) {
        this.FTTH_FiberFamily_Flat = FTTH_FiberFamily_Flat;
    }

    public Integer getFTTH_FiberHome_Flat_EOC() {
        return FTTH_FiberHome_Flat_EOC;
    }

    public void setFTTH_FiberHome_Flat_EOC(Integer FTTH_FiberHome_Flat_EOC) {
        this.FTTH_FiberHome_Flat_EOC = FTTH_FiberHome_Flat_EOC;
    }

    public Integer getTotal() {
        return Total;
    }

    public void setTotal(Integer Total) {
        this.Total = Total;
    }

    public Integer getOneTV() {
        return OneTV;
    }

    public void setOneTV(Integer OneTV) {
        this.OneTV = OneTV;
    }

    public Integer getPlayHD() {
        return PlayHD;
    }

    public void setPlayHD(Integer PlayHD) {
        this.PlayHD = PlayHD;
    }

    public Integer getAddOn() {
        return AddOn;
    }

    public void setAddOn(Integer AddOn) {
        this.AddOn = AddOn;
    }

    public Integer getSubQuantity() {
        return SubQuantity;
    }

    public void setSubQuantity(Integer SubQuantity) {
        this.SubQuantity = SubQuantity;
    }

    public Integer getSubBoxQuantity() {
        return SubBoxQuantity;
    }

    public void setSubBoxQuantity(Integer SubBoxQuantity) {
        this.SubBoxQuantity = SubBoxQuantity;
    }

    public Integer getNETQuantity() {
        return NETQuantity;
    }

    public void setNETQuantity(Integer NETQuantity) {
        this.NETQuantity = NETQuantity;
    }

    public Integer getBasicSalary() {
        return BasicSalary;
    }

    public void setBasicSalary(Integer BasicSalary) {
        this.BasicSalary = BasicSalary;
    }

    public Integer getMonthCommission() {
        return MonthCommission;
    }

    public void setMonthCommission(Integer MonthCommission) {
        this.MonthCommission = MonthCommission;
    }

    public Integer getWeek1() {
        return Week1;
    }

    public void setWeek1(Integer Week1) {
        this.Week1 = Week1;
    }

    public Integer getWeek2() {
        return Week2;
    }

    public void setWeek2(Integer Week2) {
        this.Week2 = Week2;
    }

    public Integer getWeek3() {
        return Week3;
    }

    public void setWeek3(Integer Week3) {
        this.Week3 = Week3;
    }

    public Integer getWeek4() {
        return Week4;
    }

    public void setWeek4(Integer Week4) {
        this.Week4 = Week4;
    }

    public Integer getPromIncome() {
        return PromIncome;
    }

    public void setPromIncome(Integer PromIncome) {
        this.PromIncome = PromIncome;
    }

    public Integer getPromPrice() {
        return PromPrice;
    }

    public void setPromPrice(Integer PromPrice) {
        this.PromPrice = PromPrice;
    }

    public Integer getPrepaidNew() {
        return PrepaidNew;
    }

    public void setPrepaidNew(Integer PrepaidNew) {
        this.PrepaidNew = PrepaidNew;
    }

    public Integer getIncomeSalary() {
        return IncomeSalary;
    }

    public void setIncomeSalary(Integer IncomeSalary) {
        this.IncomeSalary = IncomeSalary;
    }

    public Integer getTPlus2() {
        return TPlus2;
    }

    public void setTPlus2(Integer TPlus2) {
        this.TPlus2 = TPlus2;
    }

    public Integer getTPlus4() {
        return TPlus4;
    }

    public void setTPlus4(Integer TPlus4) {
        this.TPlus4 = TPlus4;
    }

    public Integer getTPlus6() {
        return TPlus6;
    }

    public void setTPlus6(Integer TPlus6) {
        this.TPlus6 = TPlus6;
    }

    public Integer getTPlus12() {
        return TPlus12;
    }

    public void setTPlus12(Integer TPlus12) {
        this.TPlus12 = TPlus12;
    }

    public Integer getQuantityTPlus2() {
        return QuantityTPlus2;
    }

    public void setQuantityTPlus2(Integer QuantityTPlus2) {
        this.QuantityTPlus2 = QuantityTPlus2;
    }

    public Integer getQuantityTPlus4() {
        return QuantityTPlus4;
    }

    public void setQuantityTPlus4(Integer QuantityTPlus4) {
        this.QuantityTPlus4 = QuantityTPlus4;
    }

    public Integer getQuantityTPlus6() {
        return QuantityTPlus6;
    }

    public void setQuantityTPlus6(Integer QuantityTPlus6) {
        this.QuantityTPlus6 = QuantityTPlus6;
    }

    public Integer getQuantityTPlus12() {
        return QuantityTPlus12;
    }

    public void setQuantityTPlus12(Integer QuantityTPlus12) {
        this.QuantityTPlus12 = QuantityTPlus12;
    }

    public Integer getTPlusTotal() {
        return TPlusTotal;
    }

    public void setTPlusTotal(Integer TPlusTotal) {
        this.TPlusTotal = TPlusTotal;
    }

    public Integer getTMin2() {
        return TMin2;
    }

    public void setTMin2(Integer TMin2) {
        this.TMin2 = TMin2;
    }

    public Integer getTMin4() {
        return TMin4;
    }

    public void setTMin4(Integer TMin4) {
        this.TMin4 = TMin4;
    }

    public Integer getTMin6() {
        return TMin6;
    }

    public void setTMin6(Integer TMin6) {
        this.TMin6 = TMin6;
    }

    public Integer getTMin8() {
        return TMin8;
    }

    public void setTMin8(Integer TMin8) {
        this.TMin8 = TMin8;
    }

    public Integer getTMin10() {
        return TMin10;
    }

    public void setTMin10(Integer TMin10) {
        this.TMin10 = TMin10;
    }

    public Integer getTMin12() {
        return TMin12;
    }

    public void setTMin12(Integer TMin12) {
        this.TMin12 = TMin12;
    }

    public Integer getQuantityTMin2() {
        return QuantityTMin2;
    }

    public void setQuantityTMin2(Integer QuantityTMin2) {
        this.QuantityTMin2 = QuantityTMin2;
    }

    public Integer getQuantityTMin4() {
        return QuantityTMin4;
    }

    public void setQuantityTMin4(Integer QuantityTMin4) {
        this.QuantityTMin4 = QuantityTMin4;
    }

    public Integer getQuantityTMin6() {
        return QuantityTMin6;
    }

    public void setQuantityTMin6(Integer QuantityTMin6) {
        this.QuantityTMin6 = QuantityTMin6;
    }

    public Integer getQuantityTMin8() {
        return QuantityTMin8;
    }

    public void setQuantityTMin8(Integer QuantityTMin8) {
        this.QuantityTMin8 = QuantityTMin8;
    }

    public Integer getQuantityTMin10() {
        return QuantityTMin10;
    }

    public void setQuantityTMin10(Integer QuantityTMin10) {
        this.QuantityTMin10 = QuantityTMin10;
    }

    public Integer getQuantityTMin12() {
        return QuantityTMin12;
    }

    public void setQuantityTMin12(Integer QuantityTMin12) {
        this.QuantityTMin12 = QuantityTMin12;
    }

    public Integer getTMinTotal() {
        return TMinTotal;
    }

    public void setTMinTotal(Integer TMinTotal) {
        this.TMinTotal = TMinTotal;
    }

    public Integer getBoxTPlus2() {
        return BoxTPlus2;
    }

    public void setBoxTPlus2(Integer BoxTPlus2) {
        this.BoxTPlus2 = BoxTPlus2;
    }

    public Integer getBoxTPlus4() {
        return BoxTPlus4;
    }

    public void setBoxTPlus4(Integer BoxTPlus4) {
        this.BoxTPlus4 = BoxTPlus4;
    }

    public Integer getBoxTPlus6() {
        return BoxTPlus6;
    }

    public void setBoxTPlus6(Integer BoxTPlus6) {
        this.BoxTPlus6 = BoxTPlus6;
    }

    public Integer getBoxTPlus12() {
        return BoxTPlus12;
    }

    public void setBoxTPlus12(Integer BoxTPlus12) {
        this.BoxTPlus12 = BoxTPlus12;
    }

    public Integer getQuantityBoxTPlus2() {
        return QuantityBoxTPlus2;
    }

    public void setQuantityBoxTPlus2(Integer QuantityBoxTPlus2) {
        this.QuantityBoxTPlus2 = QuantityBoxTPlus2;
    }

    public Integer getQuantityBoxTPlus4() {
        return QuantityBoxTPlus4;
    }

    public void setQuantityBoxTPlus4(Integer QuantityBoxTPlus4) {
        this.QuantityBoxTPlus4 = QuantityBoxTPlus4;
    }

    public Integer getQuantityBoxTPlus6() {
        return QuantityBoxTPlus6;
    }

    public void setQuantityBoxTPlus6(Integer QuantityBoxTPlus6) {
        this.QuantityBoxTPlus6 = QuantityBoxTPlus6;
    }

    public Integer getQuantityBoxTPlus12() {
        return QuantityBoxTPlus12;
    }

    public void setQuantityBoxTPlus12(Integer QuantityBoxTPlus12) {
        this.QuantityBoxTPlus12 = QuantityBoxTPlus12;
    }

    public Integer getBoxTPlusTotal() {
        return BoxTPlusTotal;
    }

    public void setBoxTPlusTotal(Integer BoxTPlusTotal) {
        this.BoxTPlusTotal = BoxTPlusTotal;
    }

    public Integer getBoxTMin2() {
        return BoxTMin2;
    }

    public void setBoxTMin2(Integer BoxTMin2) {
        this.BoxTMin2 = BoxTMin2;
    }

    public Integer getBoxTMin4() {
        return BoxTMin4;
    }

    public void setBoxTMin4(Integer BoxTMin4) {
        this.BoxTMin4 = BoxTMin4;
    }

    public Integer getBoxTMin6() {
        return BoxTMin6;
    }

    public void setBoxTMin6(Integer BoxTMin6) {
        this.BoxTMin6 = BoxTMin6;
    }

    public Integer getBoxTMin8() {
        return BoxTMin8;
    }

    public void setBoxTMin8(Integer BoxTMin8) {
        this.BoxTMin8 = BoxTMin8;
    }

    public Integer getBoxTMin10() {
        return BoxTMin10;
    }

    public void setBoxTMin10(Integer BoxTMin10) {
        this.BoxTMin10 = BoxTMin10;
    }

    public Integer getBoxTMin12() {
        return BoxTMin12;
    }

    public void setBoxTMin12(Integer BoxTMin12) {
        this.BoxTMin12 = BoxTMin12;
    }

    public Integer getQuantityBoxTMin2() {
        return QuantityBoxTMin2;
    }

    public void setQuantityBoxTMin2(Integer QuantityBoxTMin2) {
        this.QuantityBoxTMin2 = QuantityBoxTMin2;
    }

    public Integer getQuantityBoxTMin4() {
        return QuantityBoxTMin4;
    }

    public void setQuantityBoxTMin4(Integer QuantityBoxTMin4) {
        this.QuantityBoxTMin4 = QuantityBoxTMin4;
    }

    public Integer getQuantityBoxTMin6() {
        return QuantityBoxTMin6;
    }

    public void setQuantityBoxTMin6(Integer QuantityBoxTMin6) {
        this.QuantityBoxTMin6 = QuantityBoxTMin6;
    }

    public Integer getQuantityBoxTMin8() {
        return QuantityBoxTMin8;
    }

    public void setQuantityBoxTMin8(Integer QuantityBoxTMin8) {
        this.QuantityBoxTMin8 = QuantityBoxTMin8;
    }

    public Integer getQuantityBoxTMin10() {
        return QuantityBoxTMin10;
    }

    public void setQuantityBoxTMin10(Integer QuantityBoxTMin10) {
        this.QuantityBoxTMin10 = QuantityBoxTMin10;
    }

    public Integer getQuantityBoxTMin12() {
        return QuantityBoxTMin12;
    }

    public void setQuantityBoxTMin12(Integer QuantityBoxTMin12) {
        this.QuantityBoxTMin12 = QuantityBoxTMin12;
    }

    public Integer getBoxTMinTotal() {
        return BoxTMinTotal;
    }

    public void setBoxTMinTotal(Integer BoxTMinTotal) {
        this.BoxTMinTotal = BoxTMinTotal;
    }

    public Integer getTRecover() {
        return TRecover;
    }

    public void setTRecover(Integer TRecover) {
        this.TRecover = TRecover;
    }

    public Integer getQuantityTRecover() {
        return QuantityTRecover;
    }

    public void setQuantityTRecover(Integer QuantityTRecover) {
        this.QuantityTRecover = QuantityTRecover;
    }

    public Integer getSubstractIncome() {
        return SubstractIncome;
    }

    public void setSubstractIncome(Integer SubstractIncome) {
        this.SubstractIncome = SubstractIncome;
    }

    public Integer getHDTotal() {
        return HDTotal;
    }

    public void setHDTotal(Integer HDTotal) {
        this.HDTotal = HDTotal;
    }

    public Integer getVODTotal() {
        return VODTotal;
    }

    public void setVODTotal(Integer VODTotal) {
        this.VODTotal = VODTotal;
    }

    public Integer getNetSalary() {
        return NetSalary;
    }

    public void setNetSalary(Integer NetSalary) {
        this.NetSalary = NetSalary;
    }

    public Integer getTotalPage() {
        return TotalPage;
    }

    public void setTotalPage(Integer TotalPage) {
        this.TotalPage = TotalPage;
    }

    public Integer getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(Integer CurrentPage) {
        this.CurrentPage = CurrentPage;
    }
// Tuấn cập nhật code 28082017 hiển thị html cho báo cáo lương.
    public String getHtml() {
        return Html;
    }

    public void setHtml(String html) {
        Html = html;
    }

    ////////////////////////////////////////
    public static ReportSalaryModel Parse(JSONObject json){  
    	ReportSalaryModel object = new ReportSalaryModel();
    	try {
    		Field[] fields = object.getClass().getDeclaredFields();
    		 for (Field field : fields) {
    			 String propertiesName = field.getName();
    			 if(json.has(propertiesName)){
    				 field.setAccessible(true);
    				 if(field.getType().getSimpleName().equals("String"))    					
    					 field.set(object, json.getString(propertiesName));    					 
    				 else if(field.getType().getSimpleName().equals("Integer"))
    					 field.set(object, json.getInt(propertiesName));
    			 }
    		 }
    		 return object;
		} catch (Exception e) {
			//String message = e.getMessage();

			e.printStackTrace();
		}
    	return null;
    }
    //////////////////////////////////////////////
   
    
    @Override
    public String toString() {
    	 StringBuilder sb = new StringBuilder();
    	 Field[] fields = getClass().getDeclaredFields();    	
    	  for (Field f : fields) {
    	    sb.append(f.getName());
    	    sb.append(": ");
    	    try {
    	    	sb.append("[");
				sb.append(f.get(this));
				sb.append("]");
			} catch (Exception e) {
				// TODO: handle exception

			}
    	    
    	    sb.append(", ");
    	    sb.append('\n');
    	  }
    	  return sb.toString();
    }

    

}
