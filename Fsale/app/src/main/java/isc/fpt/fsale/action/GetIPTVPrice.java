package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.ui.fragment.FragmentRegisterContractIPTV;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.IPTVPriceModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3v2;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// API giá Extra IPTV
public class GetIPTVPrice implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    public final String TAG_METHOD_NAME = "GetIPTVPrice";
    private String[] paramValues;
    private final String[] paramNames = new String[]{"UserName", "Contract", "RegCode", "IPTVPromotionType", "IPTVPromotionID"};
    private FragmentRegisterContractIPTV fragmentRegister;
    private FragmentRegisterStep3 fragmentRegisterStep2;
    //ver3.19
    private FragmentRegisterStep3v2 fragmentRegisterStep2v2;

    //bán mới
    public GetIPTVPrice(Context context, FragmentRegisterStep3 fragment,
                        String contract, String regCode, int IPTVPromotionType, int IPTVPromotionID) {
        this.mContext = context;
        this.fragmentRegisterStep2 = fragment;
        this.paramValues = new String[]{Constants.USERNAME, contract, regCode,
                String.valueOf(IPTVPromotionType), String.valueOf(IPTVPromotionID)};
        execute();
    }

    //ver3.19
    public GetIPTVPrice(Context context, FragmentRegisterStep3v2 fragment,
                        String contract, String regCode, int IPTVPromotionType, int IPTVPromotionID) {
        this.mContext = context;
        this.fragmentRegisterStep2v2 = fragment;
        this.paramValues = new String[]{Constants.USERNAME, contract, regCode,
                String.valueOf(IPTVPromotionType), String.valueOf(IPTVPromotionID)};
        execute();
    }

    public GetIPTVPrice(Context context, FragmentRegisterContractIPTV fragment,
                        String contract, String regCode, int IPTVPromotionType, int IPTVPromotionID) {
        this.mContext = context;
        this.fragmentRegister = fragment;
        this.paramValues = new String[]{Constants.USERNAME, contract, regCode,
                String.valueOf(IPTVPromotionType), String.valueOf(IPTVPromotionID)};
        execute();
    }

    public void execute() {
        String message = "Đang lấy giá các gói Extra...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME,
                paramNames, paramValues, Services.JSON_POST, message, GetIPTVPrice.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            boolean isError = false;
            ArrayList<IPTVPriceModel> lst = null;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<IPTVPriceModel> resultObject = new WSObjectsModel<>(jsObj, IPTVPriceModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getArrayListObject();
                } else {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError) {
                    loadData(lst);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(ArrayList<IPTVPriceModel> lst) {
        try {
            if (fragmentRegister != null) {
                // bán thêm
                fragmentRegister.loadExtraAmount(lst);
            }
//            if (fragmentRegisterStep2 != null) {
//                // bán mới
//                fragmentRegisterStep2.loadIPTVPrice(lst);
//            }
            //ver3.19
            if (fragmentRegisterStep2v2 != null) {
                // bán mới
                fragmentRegisterStep2v2.loadIPTVPrice(lst);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
