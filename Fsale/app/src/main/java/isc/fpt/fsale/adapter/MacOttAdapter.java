package isc.fpt.fsale.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.MacOTT;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

/**
 * Created by haulc3 on 14,February,2020
 */
public class MacOttAdapter extends RecyclerView.Adapter<MacOttAdapter.SimpleViewHolder> {
    private Context mContext;
    private OnItemClickListener mListener;
    private List<MacOTT> mList;

    public MacOttAdapter(Context mContext, List<MacOTT> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mListener = mListener;
        this.mList = mList;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_key_value, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(mViewHolder.getAdapterPosition());
            }
        });
        return mViewHolder;
    }

    private void onItemClick(int position) {
        if (mList.get(position).isSelected()) {
            mList.get(position).setSelected(false);
        } else {
            mList.get(position).setSelected(true);
        }
        mListener.onItemClick(mList);
        notifyData(this.mList);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindViews(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.tv_name);
        }

        void bindViews(MacOTT mMacOtt) {
            mTextView.setText(mMacOtt.getMAC());
            if (mMacOtt.isSelected()) {
                mTextView.setBackgroundResource(R.drawable.background_radius_selected);
            } else {
                mTextView.setBackgroundResource(R.drawable.background_radius);
            }
        }
    }

    public void notifyData(List<MacOTT> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
