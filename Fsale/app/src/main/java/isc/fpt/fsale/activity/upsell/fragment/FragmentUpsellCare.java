package isc.fpt.fsale.activity.upsell.fragment;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.danh32.fontify.EditText;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.upsell.GetObjUpgradeTransList;
import isc.fpt.fsale.action.upsell.GetProgramUpgrade;
import isc.fpt.fsale.activity.SwipeRefreshLayout;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.upsell.KeyValuePairProgramUpgradeAdapter;
import isc.fpt.fsale.adapter.upsell.UpsellcareAdapter;
import isc.fpt.fsale.callback.upsell.OnItemListenerUpsell;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeListModel;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeTransListModel;
import isc.fpt.fsale.model.upsell.response.ProgramUpgradeModel;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class FragmentUpsellCare extends BaseFragment implements OnItemListenerUpsell {

    EditText mEdtSearch;
    Spinner mSpnAgent, mSpnprograme;
    RecyclerView mRecyclerDetail;
    ImageView mImgFind;
    SwipeRefreshLayout refreshLayout;
    private UpsellcareAdapter adapter;
    private List<ObjUpgradeTransListModel> nodelList = new ArrayList<>();
    private List<ProgramUpgradeModel> mProgram = new ArrayList<>();
    ProgramUpgradeModel keyvalue;
    boolean isLoading = false;
    int pageNum = 1;

    public static FragmentUpsellCare newInstance() {
        return new FragmentUpsellCare();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_upsell_main;
    }

    @Override
    protected void initView(View mView) {
        mEdtSearch = mView.findViewById(R.id.edt_search_value);
        mSpnprograme = mView.findViewById(R.id.spn_program);
        mSpnAgent = mView.findViewById(R.id.sp_agent);
        mRecyclerDetail = mView.findViewById(R.id.recycler_detail);
        mImgFind = mView.findViewById(R.id.img_find);
        refreshLayout = mView.findViewById(R.id.swiperefresh);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        String[] enable = {"1"};
        new GetProgramUpgrade(getContext(), enable, lst -> {
            if (lst != null) {
                this.mProgram = lst;
                loadProgramType();
            }
        });
    }

    @Override
    protected void initEvent() {
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) {
                mEdtSearch.setError(null);
            }
        });

        mImgFind.setOnClickListener(v -> {
            Common.hideSoftKeyboard(getContext());
            int agent = ((KeyValuePairModel) mSpnAgent.getSelectedItem()).getID();
            pageNum = 1;
            String[] arrValue = {keyvalue.getID().toString(), Constants.USERNAME, agent + " ",
                    mEdtSearch.getText().toString(), "10", String.valueOf(pageNum)};
            new GetObjUpgradeTransList(getContext(), arrValue, lst -> {
                if (lst != null) {
                    if (lst.size() < 10) {
                        isLoading = true;
                    } else {
                        pageNum = pageNum + 1;
                        isLoading = false;
                    }
                    nodelList = lst;
                    initRecyclerView();
                }
            });
        });
        refreshLayout.setOnRefreshListener(() -> {
            loadProgramType();
            mEdtSearch.setText("");
            refreshLayout.setRefreshing(false);
        });

        loadSearchType();
        initScrollListener();
    }

    @Override
    protected void bindData() {
    }

    private void initRecyclerView() {
        adapter = new UpsellcareAdapter(getContext(), nodelList, this);
        mRecyclerDetail.setAdapter(adapter);
        mRecyclerDetail.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void loadProgramType() {
        @SuppressLint("RtlHardcoded") KeyValuePairProgramUpgradeAdapter adapter = new KeyValuePairProgramUpgradeAdapter(getContext(),
                R.layout.my_spinner_style, mProgram, Gravity.LEFT);
        mSpnprograme.setAdapter(adapter);
        mSpnprograme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                keyvalue = mProgram.get(position);
                int agent = ((KeyValuePairModel) mSpnAgent.getSelectedItem()).getID();
                pageNum = 1;
                String[] arrValue = {keyvalue.getID().toString(), Constants.USERNAME.trim(),
                        String.valueOf(agent), "", "10", String.valueOf(pageNum)};
                new GetObjUpgradeTransList(getContext(), arrValue, lst -> {
                    if (lst != null) {
                        if (lst.size() < 10) {
                            isLoading = true;
                        } else {
                            pageNum = pageNum + 1;
                            isLoading = false;
                        }
                        nodelList = lst;
                        initRecyclerView();
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void loadSearchType() {
        ArrayList<KeyValuePairModel> lstSearchType = new ArrayList<>();
        lstSearchType.add(new KeyValuePairModel(1, "Số HD"));
        lstSearchType.add(new KeyValuePairModel(2, "Tên KH"));
        lstSearchType.add(new KeyValuePairModel(3, "Phone"));
        lstSearchType.add(new KeyValuePairModel(4, "Địa chỉ"));
        @SuppressLint("RtlHardcoded") KeyValuePairAdapter adapter = new KeyValuePairAdapter(getContext(),
                R.layout.my_spinner_style, lstSearchType, Gravity.LEFT);
        mSpnAgent.setAdapter(adapter);
    }

    @Override
    public void CareOnItemListener(ObjUpgradeTransListModel careModel) {
        Toast.makeText(getContext(), "resend", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void NotCareOnItemListener(ObjUpgradeListModel careModel) {

    }

    private void initScrollListener() {
        mRecyclerDetail.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null
                            && linearLayoutManager.findLastCompletelyVisibleItemPosition() == nodelList.size() - 1) {
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        isLoading = false;
    }

    private void loadMore() {
        new Handler().postDelayed(() -> {
            int agent = ((KeyValuePairModel) mSpnAgent.getSelectedItem()).getID();
            String[] arrValue = {keyvalue.getID().toString(), Constants.USERNAME.trim(),
                    String.valueOf(agent), "", "10", String.valueOf(pageNum)};
            new GetObjUpgradeTransList(getContext(), arrValue, lst -> {
                if (lst != null) {
                    if (lst.size() < 10) {
                        isLoading = true;
                    } else {
                        pageNum = pageNum + 1;
                        isLoading = false;
                    }
                    nodelList.addAll(lst);
                    adapter.notifyDataSetChanged();
                }
            });
        }, 200);
    }
}
