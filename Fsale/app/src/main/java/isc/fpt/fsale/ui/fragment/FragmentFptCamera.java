package isc.fpt.fsale.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.danh32.fontify.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetCostSetupCamera;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.adapter.DeviceCameraAdapter;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV3;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV4;
import isc.fpt.fsale.ui.fpt_camera.CameraDetailAdapter;
import isc.fpt.fsale.ui.fpt_camera.CloudDetailAdapter;
import isc.fpt.fsale.model.SellMoreCameraModel;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_package.CameraPackageActivity;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_type.CameraTypeActivity;
import isc.fpt.fsale.ui.fpt_camera.camera_price_list.DevicePriceListCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.cloud_detail.cloud_type.CloudTypeActivity;
import isc.fpt.fsale.ui.fpt_camera.device_list.DeviceListCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.fragment.FptCameraService;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera;
import isc.fpt.fsale.ui.fpt_camera.model.PromotionDetail;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.CAMERA_DETAIL;
import static isc.fpt.fsale.utils.Constants.CLASS_NAME;
import static isc.fpt.fsale.utils.Constants.CLOUD_DETAIL;
import static isc.fpt.fsale.utils.Constants.COMBO;
import static isc.fpt.fsale.utils.Constants.LIST_CAMERA_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_CLOUD_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.OBJECT_CAMERA_DETAIL;
import static isc.fpt.fsale.utils.Constants.POSITION;

/**
 * Created by Hau Le on 2018-12-26.
 */

public class FragmentFptCamera extends BaseFragment implements
        OnItemClickListenerV3<Object, Integer, Integer>, OnItemClickListener<Boolean>, OnItemClickListenerV4<Integer, Device, Integer> {
    private View loCameraDetail, loCloudDetail, loQuantityDeploy, loDeviceDetail;
    private CameraDetailAdapter mCameraAdapter;
    private CloudDetailAdapter mCloudAdapter;
    private TextView tvSetupDetailLess, tvSetupDetailPlus;
    private TextView tvSetupDetailQuantity, tvSetupDetailPrice;
    private SellMoreCameraModel mRegisterCamera;
    public List<CameraDetail> mListCamera;
    public List<CloudDetail> mListCloud;
    public RadioButton rdoDeploy, rdoNoDeploy;
    public SetupDetail mSetupDetail;

    //device
    private DeviceCameraAdapter mDeviceAdapter;
    public List<Device> mListDevice;

    private final int CODE_CAMERA_DETAIL = 1;
    private final int CODE_CLOUD_DETAIL = 2;
    private final int CODE_CAMERA_PACKAGE = 3;


    private final int CODE_DEVICE_LIST_SELECTED = 102;
    private final int CODE_DEVICE_PRICE_LIST_SELECTED = 103;

    public static FragmentFptCamera newInstance() {
        FragmentFptCamera fragment = new FragmentFptCamera();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        RegisterContractActivity activity = ((RegisterContractActivity) context);
        mRegisterCamera = activity.getRegisterCamera();
        if (mRegisterCamera.getObjectCamera() == null) {
            mRegisterCamera.setObjectCamera(
                    new ObjectCamera(
                            new ArrayList<CameraDetail>(), new ArrayList<CloudDetail>(),
                            new SetupDetail(), new PromotionDetail()
                    )
            );
        }
    }

    @Override
    protected void initView(View view) {
        loCameraDetail = view.findViewById(R.id.btn_add_camera);
        loCloudDetail = view.findViewById(R.id.btn_add_cloud_service);
        loDeviceDetail = view.findViewById(R.id.btn_add_device);
        RecyclerView mRecyclerViewCamera = view.findViewById(R.id.list_camera_device_final);
        RecyclerView mRecyclerViewCloud = view.findViewById(R.id.list_cloud_service_final);
        //ver 3.14
        RecyclerView mRecyclerViewDevice = view.findViewById(R.id.list_device_final);
        rdoDeploy = view.findViewById(R.id.rdo_deploy);
        rdoNoDeploy = view.findViewById(R.id.rdo_receive_on_bar);
        tvSetupDetailLess = view.findViewById(R.id.tv_camera_deploy_less);
        tvSetupDetailPlus = view.findViewById(R.id.tv_camera_deploy_plus);
        tvSetupDetailQuantity = view.findViewById(R.id.lbl_camera_deploy_quantity);
        tvSetupDetailPrice = view.findViewById(R.id.tv_camera_deploy_cost_total);
        loQuantityDeploy = view.findViewById(R.id.layout_deploy);

        //camera detail
        mListCamera = mRegisterCamera.getObjectCamera().getCameraDetail() != null ?
                mRegisterCamera.getObjectCamera().getCameraDetail() : new ArrayList<CameraDetail>();
        mCameraAdapter = new CameraDetailAdapter(getActivity(), mListCamera,
                this, this);
        mRecyclerViewCamera.setAdapter(mCameraAdapter);
        //cloud detail
        mListCloud = mRegisterCamera.getObjectCamera().getCloudDetail() != null ?
                mRegisterCamera.getObjectCamera().getCloudDetail() : new ArrayList<CloudDetail>();
        mCloudAdapter = new CloudDetailAdapter(getActivity(), mListCloud,
                this, this);
        mRecyclerViewCloud.setAdapter(mCloudAdapter);
        //device details
        mListDevice = mRegisterCamera.getDevice() != null ? mRegisterCamera.getDevice() :
                new ArrayList<Device>();
        mDeviceAdapter = new DeviceCameraAdapter(this.getActivity(), mListDevice, this);
        mRecyclerViewDevice.setAdapter(mDeviceAdapter);
        //setup detail
        mSetupDetail = mRegisterCamera.getObjectCamera().getSetupDetail();
        if (mSetupDetail.getType() == 1) {//có triển khai
            isDeploy();
        } else {
            isNoDeploy();
        }
    }

    public void isNoDeploy(){
        loQuantityDeploy.setVisibility(View.GONE);
        rdoNoDeploy.setChecked(true);
        rdoDeploy.setChecked(false);
        tvSetupDetailQuantity.setText(String.valueOf(0));
        tvSetupDetailPrice.setText(
                String.format(getString(R.string.txt_unit_price), Common.formatNumber(0)));
        mSetupDetail = null;
    }

    private void isDeploy(){
        loQuantityDeploy.setVisibility(View.VISIBLE);
        rdoDeploy.setChecked(true);
        rdoNoDeploy.setChecked(false);
        tvSetupDetailQuantity.setText(String.valueOf(mSetupDetail.getQty()));
        tvSetupDetailPrice.setText(
                String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(mSetupDetail.getQty() * mSetupDetail.getCost()))
        );
    }

    @Override
    protected void initEvent() {
        loCameraDetail.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(LIST_CAMERA_DETAIL_SELECTED, (ArrayList<? extends Parcelable>) mListCamera);
            bundle.putInt(COMBO, mRegisterCamera.getBaseObjID() == 0 &&
                    TextUtils.isEmpty(mRegisterCamera.getBaseContract()) ? 0 : 1);
            bundle.putString(Constants.CLASS_NAME, FragmentFptCamera.class.getSimpleName());
            Intent intent = new Intent(getContext(), CameraTypeActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_CAMERA_DETAIL);
        });

        loCloudDetail.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(LIST_CLOUD_DETAIL_SELECTED, (ArrayList<? extends Parcelable>) mListCloud);
            bundle.putInt(COMBO, mRegisterCamera.getBaseObjID() == 0 &&
                    TextUtils.isEmpty(mRegisterCamera.getBaseContract()) ? 0 : 1);
            bundle.putString(Constants.CLASS_NAME, FragmentFptCamera.class.getSimpleName());
            Intent intent = new Intent(getContext(), CloudTypeActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_CLOUD_DETAIL);
        });

        rdoDeploy.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if(isChecked){
                new GetCostSetupCamera(getActivity(), FragmentFptCamera.this, mRegisterCamera);
            }
        });

        rdoNoDeploy.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if(isChecked){
                isNoDeploy();
            }
        });

        tvSetupDetailLess.setOnClickListener(view -> {
            int quantity = mSetupDetail.getQty();
            if (quantity <= 1) {
                return;
            }
            quantity--;
            mSetupDetail.setQty(quantity);
            tvSetupDetailQuantity.setText(String.valueOf(quantity));
            tvSetupDetailPrice.setText(
                    String.format(getString(R.string.txt_unit_price),
                            Common.formatNumber(quantity * mSetupDetail.getCost()))
            );

        });

        tvSetupDetailPlus.setOnClickListener(view -> {
            int quantity = mSetupDetail.getQty();
            quantity++;
            mSetupDetail.setQty(quantity);
            tvSetupDetailQuantity.setText(String.valueOf(quantity));
            tvSetupDetailPrice.setText(
                    String.format(getString(R.string.txt_unit_price),
                            Common.formatNumber(quantity * mSetupDetail.getCost()))
            );
        });

        loDeviceDetail.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(Constants.DEVICE_LIST_SELECTED,
                    (ArrayList<? extends Parcelable>) mListDevice);
            bundle.putString(Constants.CLASS_NAME, FptCameraService.class.getSimpleName());
            Intent intent = new Intent(getActivity(), DeviceListCameraActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_DEVICE_LIST_SELECTED);
        });

    }

    public void loadInfoSetupDetail(List<SetupDetail> mList) {
        mSetupDetail = mList.get(0);

        mSetupDetail.setType(1);//1 - triển khai , 0 - giao tại quầy
        //set min set up detail
        mSetupDetail.setQty(1);

        isDeploy();
    }

    @Override
    protected void bindData() {
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_fpt_camera;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                switch (requestCode) {
                    case CODE_CAMERA_DETAIL:
                        List<CameraDetail> mList = data.getParcelableArrayListExtra(OBJECT_CAMERA_DETAIL);
                        mListCamera.clear();
                        if (mList != null && mList.size() != 0) {
                            mListCamera.addAll(mList);
                        }
                        mCameraAdapter.notifyDataChanged(mListCamera);
                        break;
                    case CODE_CLOUD_DETAIL:
                        CloudDetail mCloudDetail = data.getParcelableExtra(CLOUD_DETAIL);
                        if (this.mListCloud != null) {
                            this.mListCloud.add(mCloudDetail);
                        }
                        mCloudAdapter.notifyDataChanged(this.mListCloud);
                        break;
                    case CODE_CAMERA_PACKAGE:
                        CameraDetail mCameraDetail = data.getParcelableExtra(OBJECT_CAMERA_DETAIL);
                        int position = data.getIntExtra(POSITION, -1);
                        mListCamera.set(position, mCameraDetail);
                        mCameraAdapter.notifyDataChanged(mListCamera);
                        break;
                    case CODE_DEVICE_LIST_SELECTED:
                        Device mDevice = data.getParcelableExtra(Constants.DEVICE);
                        mListDevice.add(mDevice);
                        mDeviceAdapter.notifyData(mListDevice);
                        break;
                    case CODE_DEVICE_PRICE_LIST_SELECTED:
                        Device mDeviceSelected = data.getParcelableExtra(Constants.DEVICE);
                        if (mDeviceSelected == null) return;
                        int positions = data.getIntExtra(Constants.POSITION, 0);
                        if (mListDevice.size() > 1) {
                            for (Device item : mListDevice) {
                                if (item.getDeviceID() == mDeviceSelected.getDeviceID() &&
                                        item.getPriceID() == mDeviceSelected.getPriceID()) {
                                    Common.alertDialog(getString(R.string.message_double_price), this.getActivity());
                                    return; } } }
                        mListDevice.set(positions, mDeviceSelected);
                        mDeviceAdapter.notifyData(mListDevice);
                        break;
                }
            }
        }
    }

    @Override
    public void onItemClick(Object mObject, Integer position, Integer type) {
        //type == 1 is camera package, type == 3 remove cloud object
        Bundle bundle = new Bundle();
        Intent intent;
        switch (type) {
            case 1:
                CameraDetail mCameraDetail = (CameraDetail) mObject;
                bundle.putParcelable(CAMERA_DETAIL, mCameraDetail);
                bundle.putInt(POSITION, position);
                bundle.putInt(COMBO, mRegisterCamera.getBaseObjID() == 0 &&
                        TextUtils.isEmpty(mRegisterCamera.getBaseContract()) ? 0 : 1);
                bundle.putString(CLASS_NAME, FragmentFptCamera.class.getSimpleName());
                intent = new Intent(getContext(), CameraPackageActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_CAMERA_PACKAGE);
                break;
            case 3:
                CloudDetail mCloudDetailRemove = (CloudDetail) mObject;
                mListCloud.remove(mCloudDetailRemove);
                mCloudAdapter.notifyDataChanged(this.mListCloud);
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(Boolean object) {

    }

    //version 3.14
    @Override
    public void onItemClickV4(Integer type, Device mDevice, Integer position) {
        switch (type) {
            case 0:
                mListDevice.remove(mDevice);
                mRegisterCamera.setDevice(null);
                mDeviceAdapter.notifyData(mListDevice);
                break;
            case 1:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.DEVICE, mDevice);
                bundle.putInt(Constants.POSITION, position);
                bundle.putString(Constants.CLASS_NAME, FragmentFptCamera.class.getSimpleName());
                Intent intent = new Intent(getActivity(), DevicePriceListCameraActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_DEVICE_PRICE_LIST_SELECTED);
                break;
        }
    }
}
