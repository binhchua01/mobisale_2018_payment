package isc.fpt.fsale.ui.callback;

/**
 * Created by haulc3 on 09,September,2019
 */
public interface OnItemClickListenerV3<A, B, C> {
    void onItemClick(A object1, B object2, C object3);
}
