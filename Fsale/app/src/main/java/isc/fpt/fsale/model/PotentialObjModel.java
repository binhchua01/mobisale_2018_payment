package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.lang.reflect.Field;

public class PotentialObjModel implements Parcelable {
    private int ID = 0;
    private String Address;
    private String BillTo_City;
    private String BillTo_CityVN;
    private String BillTo_District;
    private String BillTo_DistrictVN;
    private String BillTo_Number;
    private String BillTo_Street;
    private String BillTo_StreetVN;
    private String BillTo_Ward;
    private String BillTo_WardVN;
    private int BranchCode = 0;
    private String Contact1;
    private String Contact2;
    private String CreateBy;
    private String CreateDate;
    private int CustomerType = 0;
    private String Note;
    private String Email;
    private String Facebook;
    private String Fax;
    private String Floor;
    private String FullName;
    private String Latlng;
    private int LocationID = 0;
    private String Lot;
    private String NameVilla;
    private String NameVillaVN;
    private String NameVillaDes;
    private String Passport;
    private String Phone1;
    private String Phone2;
    private int Position;
    private String RegCode;
    private String Room;
    private String Supporter;
    private String TaxID;
    private String Twitter;
    private int RowNumber = 0;
    private int TotalRow = 0;
    private int TotalPage = 0;
    private int CurrentPage = 0;
    private int TypeHouse = 0;
    private int RegID = 0;
    private int ServiceType = 0;
    private int ISPType = 0;
    private String ISPStartDate;
    private String ISPEndDate;
    private String ServiceTypeName;
    private String ISPIPTV;
    private String InternetISPDesc;
    private String IPTVISPDesc;
    private String Remainder;
    private int AcceptStatus;
    private int Source;
    private String CaseID;
    private String Contract;
    private int TotalSchedule;
    private int DisplayPhone;
    private int CodeStatus;
    private int SourceType;

    public PotentialObjModel() {
    }

    public PotentialObjModel(int ID, String address, String billTo_City, String billTo_CityVN, String billTo_District, String billTo_DistrictVN, String billTo_Number, String billTo_Street, String billTo_StreetVN, String billTo_Ward, String billTo_WardVN, int branchCode, String contact1, String contact2, String createBy, String createDate, int customerType, String note, String email, String facebook, String fax, String floor, String fullName, String latlng, int locationID, String lot, String nameVilla, String nameVillaVN, String nameVillaDes, String passport, String phone1, String phone2, int position, String regCode, String room, String supporter, String taxID, String twitter, int rowNumber, int totalRow, int totalPage, int currentPage, int typeHouse, int regID, int serviceType, int ISPType, String ISPStartDate, String ISPEndDate, String serviceTypeName, String ISPIPTV, String internetISPDesc, String IPTVISPDesc, String remainder, int acceptStatus, int source, String caseID, String contract, int totalSchedule, int displayPhone, int codeStatus, int sourceType) {
        this.ID = ID;
        Address = address;
        BillTo_City = billTo_City;
        BillTo_CityVN = billTo_CityVN;
        BillTo_District = billTo_District;
        BillTo_DistrictVN = billTo_DistrictVN;
        BillTo_Number = billTo_Number;
        BillTo_Street = billTo_Street;
        BillTo_StreetVN = billTo_StreetVN;
        BillTo_Ward = billTo_Ward;
        BillTo_WardVN = billTo_WardVN;
        BranchCode = branchCode;
        Contact1 = contact1;
        Contact2 = contact2;
        CreateBy = createBy;
        CreateDate = createDate;
        CustomerType = customerType;
        Note = note;
        Email = email;
        Facebook = facebook;
        Fax = fax;
        Floor = floor;
        FullName = fullName;
        Latlng = latlng;
        LocationID = locationID;
        Lot = lot;
        NameVilla = nameVilla;
        NameVillaVN = nameVillaVN;
        NameVillaDes = nameVillaDes;
        Passport = passport;
        Phone1 = phone1;
        Phone2 = phone2;
        Position = position;
        RegCode = regCode;
        Room = room;
        Supporter = supporter;
        TaxID = taxID;
        Twitter = twitter;
        RowNumber = rowNumber;
        TotalRow = totalRow;
        TotalPage = totalPage;
        CurrentPage = currentPage;
        TypeHouse = typeHouse;
        RegID = regID;
        ServiceType = serviceType;
        this.ISPType = ISPType;
        this.ISPStartDate = ISPStartDate;
        this.ISPEndDate = ISPEndDate;
        ServiceTypeName = serviceTypeName;
        this.ISPIPTV = ISPIPTV;
        InternetISPDesc = internetISPDesc;
        this.IPTVISPDesc = IPTVISPDesc;
        Remainder = remainder;
        AcceptStatus = acceptStatus;
        Source = source;
        CaseID = caseID;
        Contract = contract;
        TotalSchedule = totalSchedule;
        DisplayPhone = displayPhone;
        CodeStatus = codeStatus;
        SourceType = sourceType;
    }

    protected PotentialObjModel(Parcel in) {
        ID = in.readInt();
        Address = in.readString();
        BillTo_City = in.readString();
        BillTo_CityVN = in.readString();
        BillTo_District = in.readString();
        BillTo_DistrictVN = in.readString();
        BillTo_Number = in.readString();
        BillTo_Street = in.readString();
        BillTo_StreetVN = in.readString();
        BillTo_Ward = in.readString();
        BillTo_WardVN = in.readString();
        BranchCode = in.readInt();
        Contact1 = in.readString();
        Contact2 = in.readString();
        CreateBy = in.readString();
        CreateDate = in.readString();
        CustomerType = in.readInt();
        Note = in.readString();
        Email = in.readString();
        Facebook = in.readString();
        Fax = in.readString();
        Floor = in.readString();
        FullName = in.readString();
        Latlng = in.readString();
        LocationID = in.readInt();
        Lot = in.readString();
        NameVilla = in.readString();
        NameVillaVN = in.readString();
        NameVillaDes = in.readString();
        Passport = in.readString();
        Phone1 = in.readString();
        Phone2 = in.readString();
        Position = in.readInt();
        RegCode = in.readString();
        Room = in.readString();
        Supporter = in.readString();
        TaxID = in.readString();
        Twitter = in.readString();
        RowNumber = in.readInt();
        TotalRow = in.readInt();
        TotalPage = in.readInt();
        CurrentPage = in.readInt();
        TypeHouse = in.readInt();
        RegID = in.readInt();
        ServiceType = in.readInt();
        ISPType = in.readInt();
        ISPStartDate = in.readString();
        ISPEndDate = in.readString();
        ServiceTypeName = in.readString();
        ISPIPTV = in.readString();
        InternetISPDesc = in.readString();
        IPTVISPDesc = in.readString();
        Remainder = in.readString();
        AcceptStatus = in.readInt();
        Source = in.readInt();
        CaseID = in.readString();
        Contract = in.readString();
        TotalSchedule = in.readInt();
        DisplayPhone = in.readInt();
        CodeStatus = in.readInt();
        SourceType = in.readInt();
    }

    public static final Creator<PotentialObjModel> CREATOR = new Creator<PotentialObjModel>() {
        @Override
        public PotentialObjModel createFromParcel(Parcel in) {
            return new PotentialObjModel(in);
        }

        @Override
        public PotentialObjModel[] newArray(int size) {
            return new PotentialObjModel[size];
        }
    };

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getBillTo_City() {
        return BillTo_City;
    }

    public void setBillTo_City(String BillTo_City) {
        this.BillTo_City = BillTo_City;
    }

    public String getBillTo_District() {
        return BillTo_District;
    }

    public void setBillTo_District(String BillTo_District) {
        this.BillTo_District = BillTo_District;
    }

    public String getBillTo_Number() {
        return BillTo_Number;
    }

    public void setBillTo_Number(String BillTo_Number) {
        this.BillTo_Number = BillTo_Number;
    }

    public String getBillTo_Street() {
        return BillTo_Street;
    }

    public void setBillTo_Street(String BillTo_Street) {
        this.BillTo_Street = BillTo_Street;
    }

    public String getBillTo_Ward() {
        return BillTo_Ward;
    }

    public void setBillTo_Ward(String BillTo_Ward) {
        this.BillTo_Ward = BillTo_Ward;
    }

    public int getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(int BranchCode) {
        this.BranchCode = BranchCode;
    }

    public String getContact1() {
        return Contact1;
    }

    public void setContact1(String Contact1) {
        this.Contact1 = Contact1;
    }

    public String getContact2() {
        return Contact2;
    }

    public void setContact2(String Contact2) {
        this.Contact2 = Contact2;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String CreateBy) {
        this.CreateBy = CreateBy;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public int getCustomerType() {
        return CustomerType;
    }

    public void setCustomerType(int CustomerType) {
        this.CustomerType = CustomerType;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String Note) {
        this.Note = Note;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getFacebook() {
        return Facebook;
    }

    public void setFacebook(String Facebook) {
        this.Facebook = Facebook;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String Fax) {
        this.Fax = Fax;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String Floor) {
        this.Floor = Floor;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public String getLatlng() {
        return Latlng;
    }

    public void setLatlng(String Latlng) {
        this.Latlng = Latlng;
    }

    public int getLocationID() {
        return LocationID;
    }

    public void setLocationID(int LocationID) {
        this.LocationID = LocationID;
    }

    public String getLot() {
        return Lot;
    }

    public void setLot(String Lot) {
        this.Lot = Lot;
    }

    public String getNameVilla() {
        return NameVilla;
    }

    public void setNameVilla(String NameVilla) {
        this.NameVilla = NameVilla;
    }

    public String getPassport() {
        return Passport;
    }

    public void setPassport(String Passport) {
        this.Passport = Passport;
    }

    public String getPhone1() {
        return Phone1;
    }

    public void setPhone1(String Phone1) {
        this.Phone1 = Phone1;
    }

    public int getPosition() {
        return Position;
    }

    public void setPosition(int Position) {
        this.Position = Position;
    }

    public String getRegCode() {
        return RegCode;
    }

    public void setRegCode(String RegCode) {
        this.RegCode = RegCode;
    }

    public String getRoom() {
        return Room;
    }

    public void setRoom(String Room) {
        this.Room = Room;
    }

    public String getSupporter() {
        return Supporter;
    }

    public void setSupporter(String Supporter) {
        this.Supporter = Supporter;
    }

    public String getTaxID() {
        return TaxID;
    }

    public void setTaxID(String TaxID) {
        this.TaxID = TaxID;
    }

    public String getTwitter() {
        return Twitter;
    }

    public void setTwitter(String Twitter) {
        this.Twitter = Twitter;
    }

    public int getRowNumber() {
        return RowNumber;
    }

    public void setRowNumber(int RowNumber) {
        this.RowNumber = RowNumber;
    }

    public int getTotalRow() {
        return TotalRow;
    }

    public void setTotalRow(int TotalRow) {
        this.TotalRow = TotalRow;
    }

    public int getTotalPage() {
        return TotalPage;
    }

    public void setTotalPage(int TotalPage) {
        this.TotalPage = TotalPage;
    }

    public int getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(int CurrentPage) {
        this.CurrentPage = CurrentPage;
    }

    public int getTypeHouse() {
        return TypeHouse;
    }

    public void setTypeHouse(int TypeHouse) {
        this.TypeHouse = TypeHouse;
    }

    public String getPhone2() {
        return Phone2;
    }

    public void setPhone2(String Phone2) {
        this.Phone2 = Phone2;
    }

    public int getRegID() {
        return RegID;
    }

    public void setRegID(int RegID) {
        this.RegID = RegID;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int ServiceType) {
        this.ServiceType = ServiceType;
    }

    public int getISPType() {
        return ISPType;
    }

    public void setISPType(int ISPType) {
        this.ISPType = ISPType;
    }

    public String getISPStartDate() {
        return ISPStartDate;
    }

    public void setISPStartDate(String ISPStartDate) {
        this.ISPStartDate = ISPStartDate;
    }

    public String getISPEndDate() {
        return ISPEndDate;
    }

    public void setISPEndDate(String ISPEndDate) {
        this.ISPEndDate = ISPEndDate;
    }


    public String getServiceTypeName() {
        return this.ServiceTypeName;
    }

    public void setServiceTypeName(String ServiceTypeName) {
        this.ServiceTypeName = ServiceTypeName;
    }

    public String getISPIPTV() {
        return this.ISPIPTV;
    }

    public void setISPIPTV(String ISPIPTV) {
        this.ISPIPTV = ISPIPTV;
    }

    public String getInternetISPDesc() {
        return this.InternetISPDesc;
    }

    public void setInternetISPDesc(String InternetISPDesc) {
        this.InternetISPDesc = InternetISPDesc;
    }

    public String getIPTVISPDesc() {
        return this.IPTVISPDesc;
    }

    public void setIPTVISPDesc(String IPTVISPDesc) {
        this.IPTVISPDesc = IPTVISPDesc;
    }

    public String getRemainder() {
        return Remainder;
    }

    public void setRemainder(String remainder) {
        Remainder = remainder;
    }

    public int getAcceptStatus() {
        return AcceptStatus;
    }

    public void setAcceptStatus(int acceptStatus) {
        AcceptStatus = acceptStatus;
    }

    public int getSource() {
        return Source;
    }

    public void setSource(int source) {
        Source = source;
    }


    public String getCaseID() {
        return CaseID;
    }

    public void setCaseID(String caseID) {
        CaseID = caseID;
    }

    public String getContract() {
        return Contract;
    }

    public void setContract(String contract) {
        Contract = contract;
    }

    public int getTotalSchedule() {
        return TotalSchedule;
    }

    public void setTotalSchedule(int totalSchedule) {
        TotalSchedule = totalSchedule;
    }

    public int getDisplayPhone() {
        return DisplayPhone;
    }

    public void setDisplayPhone(int displayPhone) {
        DisplayPhone = displayPhone;
    }

    public int getCodeStatus() {
        return CodeStatus;
    }

    public void setCodeStatus(int codeStatus) {
        CodeStatus = codeStatus;
    }

    public String getNameVillaDes() {
        return NameVillaDes;
    }

    public void setNameVillaDes(String nameVillaDes) {
        NameVillaDes = nameVillaDes;
    }

    public String getBillTo_CityVN() {
        return BillTo_CityVN;
    }

    public void setBillTo_CityVN(String billTo_CityVN) {
        BillTo_CityVN = billTo_CityVN;
    }

    public String getBillTo_DistrictVN() {
        return BillTo_DistrictVN;
    }

    public void setBillTo_DistrictVN(String billTo_DistrictVN) {
        BillTo_DistrictVN = billTo_DistrictVN;
    }

    public String getBillTo_StreetVN() {
        return BillTo_StreetVN;
    }

    public void setBillTo_StreetVN(String billTo_StreetVN) {
        BillTo_StreetVN = billTo_StreetVN;
    }

    public String getBillTo_WardVN() {
        return BillTo_WardVN;
    }

    public void setBillTo_WardVN(String billTo_WardVN) {
        BillTo_WardVN = billTo_WardVN;
    }

    public String getNameVillaVN() {
        return NameVillaVN;
    }

    public void setNameVillaVN(String nameVillaVN) {
        NameVillaVN = nameVillaVN;
    }

    public int getSourceType() {
        return SourceType;
    }

    public void setSourceType(int sourceType) {
        SourceType = sourceType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Field[] fields = getClass().getDeclaredFields();
        for (Field f : fields) {
            sb.append(f.getName());
            sb.append(": ");
            try {
                sb.append("[");
                sb.append(f.get(this));
                sb.append("]");
            } catch (Exception e) {
                e.printStackTrace();
            }

            sb.append(", ");
            sb.append('\n');
        }
        return sb.toString();
    }


    public JSONObject toJSONObject() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("ID", this.getID());
            jsonObj.put("Address", this.getAddress());
            jsonObj.put("BillTo_City", this.getBillTo_City());
            jsonObj.put("BillTo_District", this.getBillTo_District());
            jsonObj.put("BillTo_Number", this.getBillTo_Number());
            jsonObj.put("BillTo_Street", this.getBillTo_Street());
            jsonObj.put("BillTo_Ward", this.getBillTo_Ward());
            jsonObj.put("Contact1", this.getContact1());
            jsonObj.put("Contact2", this.getContact2());
            jsonObj.put("CustomerType", this.getCustomerType());
            jsonObj.put("Note", this.getNote());
            jsonObj.put("Email", this.getEmail());
            jsonObj.put("Facebook", this.getFacebook());
            jsonObj.put("Fax", this.getFax());
            jsonObj.put("Floor", this.getFloor());
            jsonObj.put("FullName", this.getFullName());
            jsonObj.put("Latlng", this.getLatlng());
            jsonObj.put("Lot", this.getLot());
            jsonObj.put("NameVilla", this.getNameVilla());
            jsonObj.put("NameVillaDes", this.getNameVillaDes());
            jsonObj.put("Passport", this.getPassport());
            jsonObj.put("Phone1", this.getPhone1());
            jsonObj.put("Phone2", this.getPhone2());
            jsonObj.put("Position", this.getPosition());
            jsonObj.put("Room", this.getRoom());
            jsonObj.put("TaxID", this.getTaxID());
            jsonObj.put("Twitter", this.getTwitter());
            jsonObj.put("TypeHouse", this.getTypeHouse());
            jsonObj.put("ServiceType", this.getServiceType());
            jsonObj.put("ISPType", this.getISPType());
            jsonObj.put("ISPStartDate", this.getISPStartDate());
            jsonObj.put("ISPEndDate", this.getISPEndDate());
            jsonObj.put("TotalSchedule", this.getTotalSchedule());
            jsonObj.put("DisplayPhone", this.getDisplayPhone());
            jsonObj.put("CodeStatus", this.CodeStatus);
            jsonObj.put("SourceType", this.SourceType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeString(Address);
        parcel.writeString(BillTo_City);
        parcel.writeString(BillTo_CityVN);
        parcel.writeString(BillTo_District);
        parcel.writeString(BillTo_DistrictVN);
        parcel.writeString(BillTo_Number);
        parcel.writeString(BillTo_Street);
        parcel.writeString(BillTo_StreetVN);
        parcel.writeString(BillTo_Ward);
        parcel.writeString(BillTo_WardVN);
        parcel.writeInt(BranchCode);
        parcel.writeString(Contact1);
        parcel.writeString(Contact2);
        parcel.writeString(CreateBy);
        parcel.writeString(CreateDate);
        parcel.writeInt(CustomerType);
        parcel.writeString(Note);
        parcel.writeString(Email);
        parcel.writeString(Facebook);
        parcel.writeString(Fax);
        parcel.writeString(Floor);
        parcel.writeString(FullName);
        parcel.writeString(Latlng);
        parcel.writeInt(LocationID);
        parcel.writeString(Lot);
        parcel.writeString(NameVilla);
        parcel.writeString(NameVillaVN);
        parcel.writeString(NameVillaDes);
        parcel.writeString(Passport);
        parcel.writeString(Phone1);
        parcel.writeString(Phone2);
        parcel.writeInt(Position);
        parcel.writeString(RegCode);
        parcel.writeString(Room);
        parcel.writeString(Supporter);
        parcel.writeString(TaxID);
        parcel.writeString(Twitter);
        parcel.writeInt(RowNumber);
        parcel.writeInt(TotalRow);
        parcel.writeInt(TotalPage);
        parcel.writeInt(CurrentPage);
        parcel.writeInt(TypeHouse);
        parcel.writeInt(RegID);
        parcel.writeInt(ServiceType);
        parcel.writeInt(ISPType);
        parcel.writeString(ISPStartDate);
        parcel.writeString(ISPEndDate);
        parcel.writeString(ServiceTypeName);
        parcel.writeString(ISPIPTV);
        parcel.writeString(InternetISPDesc);
        parcel.writeString(IPTVISPDesc);
        parcel.writeString(Remainder);
        parcel.writeInt(AcceptStatus);
        parcel.writeInt(Source);
        parcel.writeString(CaseID);
        parcel.writeString(Contract);
        parcel.writeInt(TotalSchedule);
        parcel.writeInt(DisplayPhone);
        parcel.writeInt(CodeStatus);
        parcel.writeInt(SourceType);
    }
}
