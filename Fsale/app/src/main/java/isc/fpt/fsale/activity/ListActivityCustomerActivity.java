package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetCustomerCareActivityList;
import isc.fpt.fsale.adapter.ActivityListAdapter;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.ActivityModel;
import isc.fpt.fsale.model.CustomerCareListModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

// màn hình HOẠT ĐỘNG KHÁCH HÀNG
public class ListActivityCustomerActivity extends BaseActivity implements
        OnItemClickListener, OnScrollListener {

    private ListView mListView;
    private int mCurrentPage = 1, mTotalPage = 1;

    private ImageButton imgFind;
    private EditText txtAgentName;
    private Spinner spAgent;

    public ListActivityCustomerActivity() {
        super(R.string.lbl_screen_name_customer_activity_list);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(),
                getString(R.string.lbl_screen_name_customer_activity_list));
        setContentView(R.layout.activity_customer_care_activity_list);
        mListView = (ListView) findViewById(R.id.lv_object);
        mListView.setOnScrollListener(this);
        mListView.setOnItemClickListener(this);

        // **************************************************************************
        // ******************** Find *****************************
        txtAgentName = (EditText) findViewById(R.id.txt_agent_name);

        txtAgentName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtAgentName.setError(null);
            }
        });
        spAgent = (Spinner) findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                KeyValuePairModel item = (KeyValuePairModel) spAgent
                        .getItemAtPosition(position);
                if (item != null && item.getID() > 0) {
                    txtAgentName.setEnabled(true);
                    txtAgentName.requestFocus();
                    txtAgentName.setFocusable(true);
                    Common.showSoftKeyboard(ListActivityCustomerActivity.this);
                } else {
                    txtAgentName.setText("");
                    /* txtAgentName.setEnabled(false); */
                }
                txtAgentName.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        imgFind = (ImageButton) findViewById(R.id.img_find);
        imgFind.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Common.hideSoftKeyboard(ListActivityCustomerActivity.this);
                // TODO Auto-generated method stub

                if (spAgent.getSelectedItemPosition() > 0) {
                    if (txtAgentName.getText().toString().trim().equals("")) {
                        txtAgentName.setError("Nhập giá trị cần tìm");
                    } else {
                        getData(txtAgentName.getText().toString().trim(), 1);
                    }
                } else {
                    getData(txtAgentName.getText().toString().trim(), 1);
                }
            }
        });
        initAgent();

        getData("", 1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void initAgent() {
        if (spAgent.getAdapter() == null) {
            ArrayList<KeyValuePairModel> lstObjType = new ArrayList<KeyValuePairModel>();
            lstObjType.add(new KeyValuePairModel(0, "Tất cả"));
            lstObjType.add(new KeyValuePairModel(1, "Số HĐ"));
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(this,
                    R.layout.my_spinner_style, lstObjType, Gravity.LEFT);
            spAgent.setAdapter(adapter);
        }
    }

    private void getData(String Contract, int Page) {
        String UserName = "";
        UserName = ((MyApp) this.getApplication()).getUserName();
        // UserName = "I1.ThuanLV";
        new GetCustomerCareActivityList(this, UserName, Contract, Page);
    }

    @SuppressLint("LongLogTag")
    public void loadData(List<ActivityModel> lst) {
        if (lst != null && lst.size() > 0) {
            mTotalPage = lst.get(lst.size() - 1).getTotalPage();
            mCurrentPage = lst.get(lst.size() - 1).getCurrentPage();
            if (mListView.getAdapter() == null
                    || mListView.getAdapter().getCount() <= 0) {
                ActivityListAdapter mAdapter = new ActivityListAdapter(this,
                        lst);
                mListView.setAdapter(mAdapter);
            } else {
                try {
                    if (mCurrentPage > 1)
                        ((ActivityListAdapter) mListView.getAdapter())
                                .addAll(lst);
                    else {
                        ((ActivityListAdapter) mListView.getAdapter())
                                .setListData(lst);
                    }
                } catch (Exception e) {

                    Log.i("ListActivityCustomerActivity:", e.getMessage());
                }
            }
        } else {
            Common.alertDialog("Không có dữ liệu!", this);
            try {
                if (mListView.getAdapter() != null) {
                    ((ActivityListAdapter) mListView.getAdapter()).clearAll();
                }
            } catch (Exception e) {

                Log.i("ListActivityCustomerActivity:", e.getMessage());
            }
        }
    }

    // TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        // Get an Analytics tracker to report app starts and uncaught exceptions
        // etc.
        Common.reportActivityStart(this, this);
    }

    // TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        // Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    private int currentVisibleItemCount;
    private int currentScrollState;
    private int currentFirstVisibleItem;
    private int totalItem;

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // TODO Auto-generated method stub
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItem = totalItemCount;

    }

    private void isScrollCompleted() {
        if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                && this.currentScrollState == SCROLL_STATE_IDLE
                && currentVisibleItemCount > 0) {
            if (mCurrentPage < mTotalPage) {
                mCurrentPage++;
                getData("", mCurrentPage);
            }
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView,
                            int position, long id) {
        try {
            Common.hideSoftKeyboard(ListActivityCustomerActivity.this);
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        ActivityModel selectedItem = (ActivityModel) parentView
                .getItemAtPosition(position);
        if (selectedItem != null) {
            CustomerCareListModel cus = new CustomerCareListModel();
            cus.setAddress(selectedItem.getCustomer_address());
            cus.setPhoneNumber(selectedItem.getCustomer_phone());
            cus.setFullName(selectedItem.getCustomer_fullname());
            cus.setContract(selectedItem.getContract());

            Intent intent = new Intent(this, CustomerCareDetailActivity.class);
            intent.putExtra("CUSTOMER_CARE", cus);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            this.startActivity(intent);

            // String userName = ((MyApp)this.getApplication()).getUserName();
            // new GetCustomerCareList(this, userName, 2,
            // selectedItem.getContract(), 1);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
