package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by haulc3 on 05,November,2019
 */
public class ServiceCode implements Parcelable {
    private int ID;
    private String Name;
    private int TotalAfterTax;
    //use for IPTV
    private int BoxNumber;
    private int TotalPreTax;


    private ServiceCode(Parcel in) {
        ID = in.readInt();
        Name = in.readString();
        TotalAfterTax = in.readInt();
        BoxNumber = in.readInt();
        TotalPreTax = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServiceCode> CREATOR = new Creator<ServiceCode>() {
        @Override
        public ServiceCode createFromParcel(Parcel in) {
            return new ServiceCode(in);
        }

        @Override
        public ServiceCode[] newArray(int size) {
            return new ServiceCode[size];
        }
    };

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getTotalAfterTax() {
        return TotalAfterTax;
    }

    public void setTotalAfterTax(int totalAfterTax) {
        TotalAfterTax = totalAfterTax;
    }

    public int getBoxNumber() {
        return BoxNumber;
    }

    public void setBoxNumber(int boxNumber) {
        BoxNumber = boxNumber;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeString(Name);
        parcel.writeInt(TotalAfterTax);
        parcel.writeInt(BoxNumber);
        parcel.writeInt(TotalPreTax);
    }

    public JSONObject toJSONObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ID", getID());
            jsonObject.put("Name", getName());
            jsonObject.put("TotalAfterTax", getTotalAfterTax());
            jsonObject.put("BoxNumber", getBoxNumber());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
