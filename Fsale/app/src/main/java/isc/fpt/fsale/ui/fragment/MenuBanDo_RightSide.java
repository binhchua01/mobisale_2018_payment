package isc.fpt.fsale.ui.fragment;

import com.google.android.gms.maps.model.LatLng;

import isc.fpt.fsale.map.activity.ExploreMapPaintActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.utils.Constants;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MenuBanDo_RightSide extends ListFragment {
    private static final int MENU_GROUP = 0;
    private static final int MENU_PAINT = 1;

    private Context mContext;
    private LatLng lngTo, lngFrom;

    public MenuBanDo_RightSide() {
    }

    @SuppressLint("ValidFragment")
    public MenuBanDo_RightSide(LatLng lngTo, LatLng lngFrom) {
        this.lngTo = lngTo;
        this.lngFrom = lngFrom;
    }

    @SuppressLint("InflateParams")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_menu, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mContext = getActivity();
        RightMenuListAdapter adapter = new RightMenuListAdapter(this.mContext);
        //add menu items
        adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_bando), -1));
        adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_bando_paint), R.drawable.ic_paid_slide));
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView lv, View v, int position, long id) {
        Constants.SLIDING_MENU.toggle();
        if (position == MENU_PAINT) {
            Intent paint = new Intent(mContext, ExploreMapPaintActivity.class);
            paint.putExtra("lngTo", lngTo);
            paint.putExtra("lngFrom", lngFrom);
            mContext.startActivity(paint);
        }
    }

    /*
     * MODEL: RightMenuListItem
     */
    public class RightMenuListItem {
        public String tag;
        int iconRes;

        RightMenuListItem(String tag, int iconRes) {
            this.tag = tag;
            this.iconRes = iconRes;
        }
    }

    /*
     * ADAPTER: RightMenuListAdapter
     */
    public class RightMenuListAdapter extends ArrayAdapter<RightMenuListItem> {
        RightMenuListAdapter(Context context) {
            super(context, 0);
        }

        @SuppressLint("InflateParams")
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_menu, null);
            }

            TextView title = (TextView) convertView.findViewById(R.id.item_title);
            title.setText(getItem(position).tag);
            ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
            icon.setImageResource(getItem(position).iconRes);

            if (position == MENU_GROUP) {
                title.setTypeface(null, Typeface.BOLD);
                title.setTextSize(15);
                title.setTextColor(getResources().getColor(R.color.text_header_slide));
                title.setPadding(0, 10, 0, 10);
                // set background for header slide
                convertView.setBackgroundColor(getResources().getColor(R.color.bg_header_slide));
            } else {
                title.setPadding(0, 20, 0, 20);
            }
            return convertView;
        }
    }
}