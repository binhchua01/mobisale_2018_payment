package isc.fpt.fsale.ui.extra_ott.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CartExtraOtt;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.DetailPackage;
import isc.fpt.fsale.model.PromoCommand;
import isc.fpt.fsale.model.ServiceListExtraOtt;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV2;
import isc.fpt.fsale.ui.extra_ott.ExtraOttActivity;
import isc.fpt.fsale.ui.extra_ott.adapter.ExtraOttPackageItemAdapter;
import isc.fpt.fsale.ui.extra_ott.RegisterExtraOttModel;
import isc.fpt.fsale.ui.extra_ott.extra_ott_package.ExtraOttPackageActivity;
import isc.fpt.fsale.ui.extra_ott.promotion_extra_ott.PromotionExtraOttActivity;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 15,July,2019
 */
public class FragmentExtraOttService extends BaseFragment
        implements OnItemClickListenerV2<Integer, Integer> {

    private RegisterExtraOttModel mRegisterExtraOttModel;
    private List<CategoryServiceList> mListService;
    public ExtraOttPackageItemAdapter mAdapter;

    private TextView tvServiceType;
    public TextView tvLocalType;
    private Button btnExtraOttPackage;

    private final int CODE_EXTRA_OTT_PACKAGE = 101;
    private final int CODE_PROMOTION_EXTRA_OTT = 102;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ExtraOttActivity activity = (ExtraOttActivity) getActivity();
        mRegisterExtraOttModel = activity.getRegisterModel();
        mListService = activity.getListService();
    }

    @Override
    protected void initView(View mView) {
        tvServiceType = (TextView) mView.findViewById(R.id.tv_service_type_name);
        tvLocalType = (TextView) mView.findViewById(R.id.tv_local_type);
        btnExtraOttPackage = (Button) mView.findViewById(R.id.btn_choose_extra_ott_package);
        RadioButton rdoSaleRetail = (RadioButton) mView.findViewById(R.id.rdo_sale_retail);
        //set up object register
        if (mRegisterExtraOttModel.getCartExtraOtt() == null) {
            CartExtraOtt cartExtraOtt = new CartExtraOtt(0, rdoSaleRetail.isChecked() ? 1 : 2,
                    new ArrayList<ServiceListExtraOtt>());//1 - bán lẻ, 2 - bán sỉ
            mRegisterExtraOttModel.setCartExtraOtt(cartExtraOtt);
        }
        RecyclerView mRecyclerView = (RecyclerView) mView.findViewById(R.id.list_extra_ott);
        mAdapter = new ExtraOttPackageItemAdapter(getActivity(),
                mRegisterExtraOttModel.getCartExtraOtt().getServiceList(), this);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setFocusable(false);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_extra_ott_service;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindData();
    }

    @Override
    protected void bindData() {
        StringBuilder service = new StringBuilder();
        if (mRegisterExtraOttModel.getCategoryServiceList() != null &&
                mRegisterExtraOttModel.getCategoryServiceList().size() != 0) {
            mListService = mRegisterExtraOttModel.getCategoryServiceList();
        }
        for (int i = 0; i < mListService.size(); i++) {
            if ((mListService.size() - 1) == i) {
                service.append(mListService.get(i).getCategoryServiceName());
            } else {
                service.append(mListService.get(i).getCategoryServiceName()).append(", ");
            }
        }
        //set service type name to view
        tvServiceType.setText(service.toString());
        tvLocalType.setText("Giải trí");
        tvLocalType.setEnabled(false);
        mRegisterExtraOttModel.setLocalType(221);
    }

    @Override
    protected void initEvent() {
        btnExtraOttPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.REGISTRATION_EXTRA_OTT, mRegisterExtraOttModel);
                Intent intent = new Intent(getActivity(), ExtraOttPackageActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_EXTRA_OTT_PACKAGE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case CODE_EXTRA_OTT_PACKAGE:
                    List<DetailPackage> mListPackageOtt = data.getParcelableArrayListExtra(Constants.EXTRA_OTT_PACKAGE);
                    PromoCommand promoCommand = new PromoCommand();
                    promoCommand.setDetailPackage(mListPackageOtt);
                    mRegisterExtraOttModel.getCartExtraOtt().getServiceList()
                            .add(new ServiceListExtraOtt(promoCommand, 0));
                    mAdapter.notifyData(mRegisterExtraOttModel.getCartExtraOtt().getServiceList());
                    break;
                case CODE_PROMOTION_EXTRA_OTT:
                    RegisterExtraOttModel object = data.getParcelableExtra(Constants.REGISTRATION_EXTRA_OTT);
                    mRegisterExtraOttModel.getCartExtraOtt().setServiceList(object.getCartExtraOtt().getServiceList());
                    mAdapter.notifyData(mRegisterExtraOttModel.getCartExtraOtt().getServiceList());
                    break;
            }
        }
    }

    @Override
    public void onItemClick(Integer position, Integer type) {
        switch (type) {
            case 1://chọn CLKM
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.REGISTRATION_EXTRA_OTT, mRegisterExtraOttModel);
                bundle.putInt(Constants.POSITION, position);// position of item list
                Intent intent = new Intent(getActivity(), PromotionExtraOttActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_PROMOTION_EXTRA_OTT);
                break;
            case 2://xóa gói dịch vụ
                mRegisterExtraOttModel.getCartExtraOtt().getServiceList().remove(
                        mRegisterExtraOttModel.getCartExtraOtt().getServiceList().get(position)
                );
                mAdapter.notifyData(mRegisterExtraOttModel.getCartExtraOtt().getServiceList());
                break;
        }
    }
}