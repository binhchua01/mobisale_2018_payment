package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetReportSBIDetail;
import isc.fpt.fsale.adapter.ReportSBIDetailAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportSBIDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;
import android.widget.Spinner;

public class ListReportSBIDetailActivity extends BaseActivity {

    private ListView lvSBI;
    private Spinner spPage;
    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;

    private String UserName = "", AgentName = "";
    private int Day = 0, Month = 0, Year = 0, Agent = 0, ReportType = 0;//, PageNumber = 0;

    public ListReportSBIDetailActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.menu_report_sbi_detail);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_report_sbi_detail));
        setContentView(R.layout.activity_report_sbi_detaill);
        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getData(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        lvSBI = (ListView) findViewById(R.id.lv_report_sbi_detail);
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        try {
            Intent intent = getIntent();
			/*
			 * intent.putExtra("USER_NAME", item.getSaleName());
							intent.putExtra("DAY", mDay);
							intent.putExtra("MONTH", mMonth);
							intent.putExtra("YEAR", mYear);
							intent.putExtra("AGENT", mAgent);
							intent.putExtra("AGENTNAME", mAgentName);
							intent.putExtra("REPORT_TYPE", subItem.getID());
			 * */
            UserName = intent.getStringExtra("USER_NAME");
            AgentName = intent.getStringExtra("AGENT_NAME");
            Day = intent.getIntExtra("DAY", 0);
            Month = intent.getIntExtra("MONTH", 0);
            Year = intent.getIntExtra("YEAR", 0);
            Agent = intent.getIntExtra("AGENT", 0);
            ReportType = intent.getIntExtra("REPORT_TYPE", 0);
            getData(1);
        } catch (Exception e) {
            // TODO: handle exception

        }
    }

    private void getData(int pageNumber) {
        new GetReportSBIDetail(this, UserName, Day, Month, Year, Agent, AgentName, ReportType, pageNumber);
    }

    public void LoadData(List<ReportSBIDetailModel> list) {
        try {
            if (list == null || list.size() <= 0) {
				/*Common.alertDialog(getString(R.string.msg_no_data), this);	
				lvSBI.setAdapter(null);*/
                new AlertDialog.Builder(this).setTitle("Thông Báo").setMessage(getString(R.string.msg_no_data))
                        .setPositiveButton(R.string.lbl_ok, new OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                finish();
                            }
                        })
                        .setCancelable(false).create().show();
            } else {
                ReportSBIDetailAdapter adapter = new ReportSBIDetailAdapter(this, list);
                lvSBI.setAdapter(adapter);
            }
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }

    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
