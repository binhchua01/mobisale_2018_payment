package isc.fpt.fsale.ui.fpt_camera;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ListObjectModel;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

/**
 * Created by haulc3 on 05,September,2019
 */
public class ContractListAdapter extends RecyclerView.Adapter<ContractListAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<ListObjectModel> mList;
    private OnItemClickListener mListener;

    ContractListAdapter(Context mContext, List<ListObjectModel> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_contract_list_fpt_camera_combo, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindViews(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvContractNum,
                tvLocalType, tvAddress,
                tvServiceType, btnCreateContract;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvContractNum = (TextView) itemView.findViewById(R.id.tv_contract_num);
            tvLocalType = (TextView) itemView.findViewById(R.id.tv_service_package);
            tvAddress = (TextView) itemView.findViewById(R.id.tv_address);
            tvServiceType = (TextView) itemView.findViewById(R.id.tv_service_type);
            btnCreateContract = (TextView) itemView.findViewById(R.id.button_create_contract_combo);
        }

        void bindViews(final ListObjectModel objectModel) {
            tvName.setText(objectModel.getFullName());
            tvContractNum.setText(objectModel.getContract());
            tvLocalType.setText(objectModel.getLocalTypeName());
            tvAddress.setText(objectModel.getAddress());
            tvServiceType.setText(objectModel.getServiceTypeName());
            if(objectModel.getComboCameraStatus() > 0){
                btnCreateContract.setVisibility(View.GONE);
            }else{
                btnCreateContract.setVisibility(View.VISIBLE);
            }
            btnCreateContract.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(objectModel.getComboStatus() == 0){
                        mListener.onItemClick(objectModel);
                    }
                }
            });
        }
    }

    public void notifyData(List<ListObjectModel> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
