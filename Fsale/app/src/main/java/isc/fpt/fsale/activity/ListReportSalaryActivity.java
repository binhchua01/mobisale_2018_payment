package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetReportSalaryList;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportSalaryModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class ListReportSalaryActivity extends BaseActivity {


    private Spinner spDay, spMonth, spYear, spAgent;
    //	private ListView lvResult;
    private LinearLayout frmFind;
    private ImageView imgNavigation;
    private Spinner spPage;
    private EditText txtAgentName;

    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
    private int Day = 0, Month = 0, Year = 0, Agent = 0;
    private String AgentName;
    // Tuấn cập nhật code 28082017 hiển thị lương dạng html
    private WebView contentDetailSalary;

    public ListReportSalaryActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.menu_report_salary);
    }

    @Override
    public void startActivity(Intent intent) {
        // TODO Auto-generated method stub
        finish();
        super.startActivity(intent);
    }

    @SuppressLint("InflateParams")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_report_salary_activity));

        setContentView(R.layout.list_report_salary);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        spDay = (Spinner) findViewById(R.id.sp_day);
        spMonth = (Spinner) findViewById(R.id.sp_month);
        spYear = (Spinner) findViewById(R.id.sp_year);
//        lvResult = (ListView)findViewById(R.id.lv_report);
        txtAgentName = (EditText) findViewById(R.id.txt_agent_name);
        final Button btnFind = (Button) findViewById(R.id.btn_find);
        imgNavigation = (ImageView) findViewById(R.id.img_navigation_drop_down);
        frmFind = (LinearLayout) findViewById(R.id.frm_find);
        contentDetailSalary = (WebView) findViewById(R.id.wv_detail_salary);
        btnFind.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (FLAG_FIRST_LOAD > 0)
                    FLAG_FIRST_LOAD = 0;
                getReport(1);
            }
        });

        imgNavigation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dropDownNavigation();
            }
        });
        spYear.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                int month = 0, year = 0;
                try {
                    year = item.getID();
                    month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //common.
                Common.initDaySpinner(ListReportSalaryActivity.this, spDay, month, year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spMonth.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                int month = 0, year = 0;
                try {
                    month = item.getID();
                    year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Common.initDaySpinner(ListReportSalaryActivity.this, spDay, month, year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getReport(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spAgent = (Spinner) findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() > 0) {
                    txtAgentName.setEnabled(true);
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        Common.initYearSpinner(ListReportSalaryActivity.this, spYear);
        Common.initMonthSpinner(ListReportSalaryActivity.this, spMonth);
        initSpinnerAgent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
    //=========================================== Search Module =========================================

    /*public void initDaySpinner(int month, int year)
    {
        KeyValuePairAdapter adapterMonth;
        // Lấy tháng và năm hiện tại
        Calendar c = Calendar.getInstance();
        c.set(year, month-1, 1);
        int dayOfMonths =  c.getActualMaximum(Calendar.DAY_OF_MONTH);
        //Thang Search
        ArrayList<KeyValuePairModel> ListSpMonth = new ArrayList<KeyValuePairModel>();
        ListSpMonth.add( new KeyValuePairModel(0, "Tất cả"));
        for(int i=1; i<=dayOfMonths; i++)
        {
            ListSpMonth.add(new KeyValuePairModel(i, String.valueOf(i)));
        }
        adapterMonth = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpMonth);
        spDay.setAdapter(adapterMonth);
        // Set tháng là tháng hiện tại

    }

     public void initMonthSpinner()
     {
        KeyValuePairAdapter adapterMonth;
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH);
         ArrayList<KeyValuePairModel> ListSpMonth = new ArrayList<KeyValuePairModel>();
         for(int i=1;i<13;i++)
         {
             ListSpMonth.add(new KeyValuePairModel(i, String.valueOf(i)));
         }
         adapterMonth = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpMonth);
        spMonth.setAdapter(adapterMonth);
        spMonth.setSelection(Common.getIndex(spMonth, month+1));




     }
     private void initYearSpinner(){

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        ArrayList<KeyValuePairModel> ListSpYear = new ArrayList<KeyValuePairModel>();
        int iYear = getCurrentYear(getCurrentDate());
        int jYear=iYear-1;
        ListSpYear.add(new KeyValuePairModel(iYear,String.valueOf(iYear)));
        ListSpYear.add(new KeyValuePairModel(jYear,String.valueOf(jYear)));
        KeyValuePairAdapter adapterYear = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpYear);
        spYear.setAdapter(adapterYear);
        spYear.setSelection(Common.getIndex(spYear, year));
     }

     private void intAgentSpinner(){
        //Loai Tim kiem
        ArrayList<KeyValuePairModel> ListSpAgent= new ArrayList<KeyValuePairModel>();
        ListSpAgent.add(new KeyValuePairModel(0,"Tất cả"));
        ListSpAgent.add(new KeyValuePairModel(1,"Nhân viên"));
        KeyValuePairAdapter adapterAgent = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpAgent);
        spAgent.setAdapter(adapterAgent);

     }
     @SuppressLint("SimpleDateFormat")
    private String getCurrentDate(){
        String currentDate="";
        Calendar c= Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        currentDate =  df.format(c.getTime());
        return currentDate;
    }

    private int getCurrentYear(String sCurrentDate){
        return Integer.parseInt((String) sCurrentDate.subSequence(6, 10));
    }	*/
    private void initSpinnerAgent() {
        ArrayList<KeyValuePairModel> listAgent = new ArrayList<KeyValuePairModel>();
        listAgent.add(new KeyValuePairModel(0, "Tất cả"));
        listAgent.add(new KeyValuePairModel(1, "Nhân viên"));
        KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(this, R.layout.my_spinner_style, listAgent, Gravity.LEFT);
        spAgent.setAdapter(adapterStatus);
    }

    //========================================= Get/Load data ============================================
    private void getReport(int PageNumber) {
        mPage = PageNumber;
        Day = ((KeyValuePairModel) spDay.getSelectedItem()).getID();
        Month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
        Year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();
        AgentName = txtAgentName.getText().toString().trim();
        Agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
        if (Agent > 0 && AgentName.equals("")) {
            Common.alertDialog(getString(R.string.msg_search_value_empty), this);
        } else {
            new GetReportSalaryList(this, Constants.USERNAME, Day, Month, Year, String.valueOf(Agent), AgentName, mPage);
        }
    }


    /*private void getData(){
        //GetReportSalaryList(Context context, String SaleAccount, int Month, int Year, String Agent, String AgentName, int PageNumber)
        int day = 0, month =0, year = 0;
        String agent = "0", agentName = "";
        agent = String.valueOf(((KeyValuePairModel)spAgent.getSelectedItem()).getID());
        agentName = txtAgentName.getText().toString();
        if(agent.equals(""))
            agent = "0";
        if(agentName.equals(""))
            agentName = "0";
        day = ((KeyValuePairModel)spDay.getSelectedItem()).getID();
        month = ((KeyValuePairModel)spMonth.getSelectedItem()).getID();
        year = ((KeyValuePairModel)spYear.getSelectedItem()).getID();

        new GetReportSalaryList(mContext, Constants.USERNAME, day, month, year, agent, agentName, mCurrentPage);
    }*/
    // lấy thông tin phân trang và hiển thị chi tiết lương
    public void LoadData(ArrayList<ReportSalaryModel> lst) {
        if (lst != null && lst.size() > 0) {
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                dropDownNavigation();
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
                //adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            contentDetailSalary.loadDataWithBaseURL(null, lst.get(0).getHtml(), "text/html", "utf-8", null);
//			ReportSalaryAdapter abc = new ReportSalaryAdapter(this, lst);
//			lvResult.setAdapter(abc);
        }
// else{
//			Common.alertDialog(getString(R.string.msg_no_data), this);
//			lvResult.setAdapter(new ReportSalaryAdapter(this, new ArrayList<ReportSalaryModel>()));
//		}
//
    }
	
	/*public void LoadData(ArrayList<ReportSalaryModel> list){
		try {
			if(list != null && list.size() > 0){
				ReportSalaryAdapter abc = new ReportSalaryAdapter(mContext, list);
				mListViewObj.setAdapter(abc);
				dropDownNavigation();
				
				ReportSalaryModel item1 = list.get(0);
				mCurrentPage = item1.getCurrentPage();		
				mTotalPage = item1.getTotalPage();				
				txt_row_total.setText(String.valueOf(item1.getTotalRow()));
				if(SpPage.getAdapter() == null || FLAG_FIRST_LOAD == 0){
					ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
					for(int i=1; i <= mTotalPage; i++){
						lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));				
					}										
					KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPage, Color.WHITE);	
					//adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
					SpPage.setAdapter(pageAdapter);						
				}	
				
				
			}else{
				txt_row_total.setText("0");
				mListViewObj.setAdapter(new ReportSalaryAdapter(mContext, new ArrayList<ReportSalaryModel>()));
				Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}*/

    /*@SuppressLint("ClickableViewAccessibility")
    private void initOnTouchSpinner(){
        try {
            SpPage.setOnTouchListener(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    try {
                        Common.hideSoftKeyboard(ListReportSalaryActivity.this);
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    return false;
                }
            });
            spAgent.setOnTouchListener(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    try {
                        Common.hideSoftKeyboard(ListReportSalaryActivity.this);
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    return false;
                }
            });
            spDay.setOnTouchListener(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    try {
                        Common.hideSoftKeyboard(ListReportSalaryActivity.this);
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    return false;
                }
            });
            spMonth.setOnTouchListener(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    try {
                        Common.hideSoftKeyboard(ListReportSalaryActivity.this);
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    return false;
                }
            });

            spYear.setOnTouchListener(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    try {
                        Common.hideSoftKeyboard(ListReportSalaryActivity.this);
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }*/
    //TODO: dropDown Search frm
    private void dropDownNavigation() {
        if (frmFind.getVisibility() == View.VISIBLE) {
            frmFind.setVisibility(View.GONE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
        } else {
            frmFind.setVisibility(View.VISIBLE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
        }
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
