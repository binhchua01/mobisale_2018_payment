package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ListPrechecklistModel implements Parcelable{

	
	private String Contract;
	private String FullName;
	private String Address;
	private String CreateDate;
	private String Description;
	private String ErrorService;
	private String SupDescription;
	private String UpdateBy;
	private String UpdateDate;
	
	// add by GiauTQ 01-05-2014
	private String RowNumber;
	private String TotalPage;
	private String CurrentPage;
	private String TotalRow;
	
	public ListPrechecklistModel(String Contract, String CreateDate,String Description,			
	String SupDescription, String UpdateBy, String UpdateDate, String FullName, String Address,
	String rowNumber, String totalPage, String currentPage, String totalRow) 
	{
		this.setContract(Contract);
		this.setFullName(FullName);
		this.setAddress(Address);
		this.setCreateDate(CreateDate);
		this.setDescription(Description);
		this.setSupDescription(SupDescription);
		this.setUpdateBy(UpdateBy);
		this.setUpdateDate(UpdateDate);
		// add by GiauTQ 01-05-2014
		this.setRowNumber(rowNumber);
		this.setTotalPage(totalPage);
		this.setCurrentPage(currentPage);
		this.setTotalRow(totalRow);
		// TODO Auto-generated constructor stub
	}

	public String getContract() {
		return Contract;
	}

	public void setContract(String contract) {
		Contract = contract;
	}
	
	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullname) {
		FullName = fullname;
	}
	
	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getCreateDate() {
		return CreateDate;
	}

	public void setCreateDate(String createDate) {
		CreateDate = createDate;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getErrorService() {
		return ErrorService;
	}

	public void setErrorService(String errorService) {
		ErrorService = errorService;
	}

	public String getSupDescription() {
		return SupDescription;
	}

	public void setSupDescription(String supDescription) {
		SupDescription = supDescription;
	}

	public String getUpdateBy() {
		return UpdateBy;
	}

	public void setUpdateBy(String updateBy) {
		UpdateBy = updateBy;
	}

	public String getUpdateDate() {
		return UpdateDate;
	}

	public void setUpdateDate(String updateDate) {
		UpdateDate = updateDate;
	}
	
	// add by GiauTQ 01-05-2014
	public String getRowNumber() {
		return RowNumber;
	}

	public void setRowNumber(String rowNumber) {
		RowNumber = rowNumber;
	}
	
	public String getTotalPage() {
		return TotalPage;
	}

	public void setTotalPage(String totalPage) {
		TotalPage = totalPage;
	}
	
	public void setCurrentPage(String currentPage) {
		CurrentPage = currentPage;
	}
	
	public String getCurrentPage() {
		return CurrentPage;
	}
	
	public void setTotalRow(String totalRow) {
		TotalRow = totalRow;
	}
	
	public String getTotalRow() {
		return TotalRow;
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel pc, int flags) {
		// TODO Auto-generated method stub		
		pc.writeString(Contract);
		pc.writeString(FullName);
		pc.writeString(Address);
		pc.writeString(CreateDate);
		pc.writeString(Description);		
		pc.writeString(SupDescription);
		pc.writeString(UpdateBy);
		pc.writeString(UpdateDate);		
		// add by GiauTQ 01-05-2014
		pc.writeString(RowNumber);
		pc.writeString(TotalPage);
		pc.writeString(CurrentPage);
		pc.writeString(TotalRow);
	}
	public ListPrechecklistModel(Parcel source) {
		
		Contract = source.readString();
		FullName = source.readString();
		Address = source.readString();
		CreateDate = source.readString();
		Description = source.readString();
		SupDescription = source.readString();
		UpdateBy = source.readString();
		UpdateDate = source.readString();
		// add by GiauTQ 01-05-2014
		RowNumber = source.readString();
		TotalPage = source.readString();
		CurrentPage = source.readString();
		TotalRow = source.readString();
	}
	
	/** Static field used to regenerate object, individually or as arrays */
	public static final Creator<ListPrechecklistModel> CREATOR = new Creator<ListPrechecklistModel>() {
		@Override
		public ListPrechecklistModel createFromParcel(Parcel source) {
			return new ListPrechecklistModel(source);
		}

		@Override
		public ListPrechecklistModel[] newArray(int size) {
			return new ListPrechecklistModel[size];
		}
	};


}
