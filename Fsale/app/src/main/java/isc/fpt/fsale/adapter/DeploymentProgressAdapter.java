package isc.fpt.fsale.adapter;

import isc.fpt.fsale.action.PushDeployment;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ListDeploymentProgressModel;
import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
//Adapter TIẾN ĐỒ TRIỂN KHAI
public class DeploymentProgressAdapter extends BaseAdapter {

	private ArrayList<ListDeploymentProgressModel> mList;	
	private Context mContext;
	
	public DeploymentProgressAdapter(Context context, ArrayList<ListDeploymentProgressModel> lst) {
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.mList = lst;		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListDeploymentProgressModel deployment = mList.get(position);
		
		if (convertView == null) {			
			convertView = LayoutInflater.from(mContext).inflate(R.layout.list_row_deployment, null);		
		}
		if(deployment != null){
			TextView txtRowNumber = (TextView) convertView.findViewById(R.id.txt_row_number);
			txtRowNumber.setText(deployment.getRowNumber());
			
			TextView txtRegCode = (TextView) convertView.findViewById(R.id.txt_RegCode);
			txtRegCode.setText(deployment.getRegCode());		
			
			TextView txtContract = (TextView) convertView.findViewById(R.id.txt_Contract);
			txtContract.setText(deployment.getContract());				
			
			TextView txtObjDate = (TextView) convertView.findViewById(R.id.txt_Obj_Date);
			txtObjDate.setText(deployment.getObjDate());		
			
			TextView txtDeploymentDate= (TextView) convertView.findViewById(R.id.txt_Deployment_Date);
			txtDeploymentDate.setText(deployment.getDeploymentDate());	
			
			TextView txtWaitTime = (TextView) convertView.findViewById(R.id.txt_Wait_Time);
			txtWaitTime.setText(deployment.getWaitTime());		
			
			TextView txtFinishDate = (TextView) convertView.findViewById(R.id.txt_Finish_Date);
			txtFinishDate.setText(deployment.getFinishDate());
			
			TextView txtObjectPhone = (TextView) convertView.findViewById(R.id.txt_object_phone);
			txtObjectPhone.setText(deployment.getObjectPhone());
			
			TextView txtAllot = (TextView) convertView.findViewById(R.id.txt_allot);
			txtAllot.setText(deployment.getAllot());
			//
			TextView lblFullName = (TextView) convertView.findViewById(R.id.lbl_full_name);
			lblFullName.setText(deployment.getFullName());
			//
			TextView lblDeloyAppointmentDate = (TextView) convertView.findViewById(R.id.lbl_deploy_appointment_date);
			lblDeloyAppointmentDate.setText(deployment.getDeployAppointmentDate());
			
			Button sendMail = (Button)convertView.findViewById(R.id.img_btn_send_mail);
			int waitTime = 0;
			try {
				waitTime = Integer.parseInt(deployment.getWaitTime());
			} catch (Exception e) {
				// TODO: handle exception

			}
			if(deployment.getSendMail() == 0 && waitTime > 0)
				sendMail.setVisibility(View.VISIBLE);
			else
				sendMail.setVisibility(View.GONE);
			
			final String contract = deployment.getContract();
			
			sendMail.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					AlertDialog.Builder builder = null;
					Dialog dialog = null;
					builder = new AlertDialog.Builder(mContext);
					builder.setMessage("Bạn có muốn gửi mail không?").setCancelable(false).setPositiveButton("Có",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {		
		  					//HandleSaveAddress(sHouseTypeSelected);  						  					
		  					new PushDeployment(mContext, contract);
						}
					}).setNegativeButton("Không",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int id) {
							dialog.cancel();
						}
					});
					dialog = builder.create();
					dialog.show();				
				}
			});
		}		
		return convertView;
	}

}
