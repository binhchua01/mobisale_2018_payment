package isc.fpt.fsale.model.upsell.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObjUpgradeTransListModel implements Parcelable {

    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("AdvisoryUpgradeID")
    @Expose
    private Integer advisoryUpgradeID;
    @SerializedName("AdvisoryUpgradeName")
    @Expose
    private String advisoryUpgradeName;
    @SerializedName("ConfirmDate")
    @Expose
    private String confirmDate;
    @SerializedName("Contract")
    @Expose
    private String contract;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;
    @SerializedName("ExpiredDate")
    @Expose
    private String expiredDate;
    @SerializedName("FeeFromLocalType")
    @Expose
    private Double feeFromLocalType;
    @SerializedName("FeeToLocalType")
    @Expose
    private Double feeToLocalType;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("IsRecare")
    @Expose
    private Integer isRecare;
    @SerializedName("IsResendShortLink")
    @Expose
    private Integer isResendShortLink;
    @SerializedName("MoneyUpgrade")
    @Expose
    private Double moneyUpgrade;
    @SerializedName("Note")
    @Expose
    private String note;
    @SerializedName("ObjID")
    @Expose
    private Integer objID;
    @SerializedName("PageNum")
    @Expose
    private Integer pageNum;
    @SerializedName("PaymentStatusName")
    @Expose
    private String paymentStatusName;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("ProgramUpgradeID")
    @Expose
    private Integer programUpgradeID;
    @SerializedName("ProgramUpgradeName")
    @Expose
    private String programUpgradeName;
    @SerializedName("PromotionIPTVName")
    @Expose
    private String promotionIPTVName;
    @SerializedName("PromotionNetName")
    @Expose
    private String promotionNetName;
    @SerializedName("TotalMoney")
    @Expose
    private Integer totalMoney;
    @SerializedName("UpgradeStatus")
    @Expose
    private Integer upgradeStatus;
    @SerializedName("UpgradeStatusName")
    @Expose
    private String upgradeStatusName;

    protected ObjUpgradeTransListModel(Parcel in) {
        address = in.readString();
        if (in.readByte() == 0) {
            advisoryUpgradeID = null;
        } else {
            advisoryUpgradeID = in.readInt();
        }
        advisoryUpgradeName = in.readString();
        confirmDate = in.readString();
        contract = in.readString();
        createdDate = in.readString();
        expiredDate = in.readString();
        if (in.readByte() == 0) {
            feeFromLocalType = null;
        } else {
            feeFromLocalType = in.readDouble();
        }
        if (in.readByte() == 0) {
            feeToLocalType = null;
        } else {
            feeToLocalType = in.readDouble();
        }
        fullName = in.readString();
        if (in.readByte() == 0) {
            isRecare = null;
        } else {
            isRecare = in.readInt();
        }
        if (in.readByte() == 0) {
            isResendShortLink = null;
        } else {
            isResendShortLink = in.readInt();
        }
        if (in.readByte() == 0) {
            moneyUpgrade = null;
        } else {
            moneyUpgrade = in.readDouble();
        }
        note = in.readString();
        if (in.readByte() == 0) {
            objID = null;
        } else {
            objID = in.readInt();
        }
        if (in.readByte() == 0) {
            pageNum = null;
        } else {
            pageNum = in.readInt();
        }
        paymentStatusName = in.readString();
        phone = in.readString();
        if (in.readByte() == 0) {
            programUpgradeID = null;
        } else {
            programUpgradeID = in.readInt();
        }
        programUpgradeName = in.readString();
        promotionIPTVName = in.readString();
        promotionNetName = in.readString();
        if (in.readByte() == 0) {
            totalMoney = null;
        } else {
            totalMoney = in.readInt();
        }
        if (in.readByte() == 0) {
            upgradeStatus = null;
        } else {
            upgradeStatus = in.readInt();
        }
        upgradeStatusName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        if (advisoryUpgradeID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(advisoryUpgradeID);
        }
        dest.writeString(advisoryUpgradeName);
        dest.writeString(confirmDate);
        dest.writeString(contract);
        dest.writeString(createdDate);
        dest.writeString(expiredDate);
        if (feeFromLocalType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(feeFromLocalType);
        }
        if (feeToLocalType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(feeToLocalType);
        }
        dest.writeString(fullName);
        if (isRecare == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isRecare);
        }
        if (isResendShortLink == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isResendShortLink);
        }
        if (moneyUpgrade == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(moneyUpgrade);
        }
        dest.writeString(note);
        if (objID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(objID);
        }
        if (pageNum == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pageNum);
        }
        dest.writeString(paymentStatusName);
        dest.writeString(phone);
        if (programUpgradeID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(programUpgradeID);
        }
        dest.writeString(programUpgradeName);
        dest.writeString(promotionIPTVName);
        dest.writeString(promotionNetName);
        if (totalMoney == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalMoney);
        }
        if (upgradeStatus == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(upgradeStatus);
        }
        dest.writeString(upgradeStatusName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObjUpgradeTransListModel> CREATOR = new Creator<ObjUpgradeTransListModel>() {
        @Override
        public ObjUpgradeTransListModel createFromParcel(Parcel in) {
            return new ObjUpgradeTransListModel(in);
        }

        @Override
        public ObjUpgradeTransListModel[] newArray(int size) {
            return new ObjUpgradeTransListModel[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAdvisoryUpgradeID() {
        return advisoryUpgradeID;
    }

    public void setAdvisoryUpgradeID(Integer advisoryUpgradeID) {
        this.advisoryUpgradeID = advisoryUpgradeID;
    }

    public String getAdvisoryUpgradeName() {
        return advisoryUpgradeName;
    }

    public void setAdvisoryUpgradeName(String advisoryUpgradeName) {
        this.advisoryUpgradeName = advisoryUpgradeName;
    }

    public String getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(String confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Double getFeeFromLocalType() {
        return feeFromLocalType;
    }

    public void setFeeFromLocalType(Double feeFromLocalType) {
        this.feeFromLocalType = feeFromLocalType;
    }

    public Double getFeeToLocalType() {
        return feeToLocalType;
    }

    public void setFeeToLocalType(Double feeToLocalType) {
        this.feeToLocalType = feeToLocalType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getIsRecare() {
        return isRecare;
    }

    public void setIsRecare(Integer isRecare) {
        this.isRecare = isRecare;
    }

    public Integer getIsResendShortLink() {
        return isResendShortLink;
    }

    public void setIsResendShortLink(Integer isResendShortLink) {
        this.isResendShortLink = isResendShortLink;
    }

    public Double getMoneyUpgrade() {
        return moneyUpgrade;
    }

    public void setMoneyUpgrade(Double moneyUpgrade) {
        this.moneyUpgrade = moneyUpgrade;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getObjID() {
        return objID;
    }

    public void setObjID(Integer objID) {
        this.objID = objID;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public String getPaymentStatusName() {
        return paymentStatusName;
    }

    public void setPaymentStatusName(String paymentStatusName) {
        this.paymentStatusName = paymentStatusName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getProgramUpgradeID() {
        return programUpgradeID;
    }

    public void setProgramUpgradeID(Integer programUpgradeID) {
        this.programUpgradeID = programUpgradeID;
    }

    public String getProgramUpgradeName() {
        return programUpgradeName;
    }

    public void setProgramUpgradeName(String programUpgradeName) {
        this.programUpgradeName = programUpgradeName;
    }

    public String getPromotionIPTVName() {
        return promotionIPTVName;
    }

    public void setPromotionIPTVName(String promotionIPTVName) {
        this.promotionIPTVName = promotionIPTVName;
    }

    public String getPromotionNetName() {
        return promotionNetName;
    }

    public void setPromotionNetName(String promotionNetName) {
        this.promotionNetName = promotionNetName;
    }

    public Integer getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Integer totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Integer getUpgradeStatus() {
        return upgradeStatus;
    }

    public void setUpgradeStatus(Integer upgradeStatus) {
        this.upgradeStatus = upgradeStatus;
    }

    public String getUpgradeStatusName() {
        return upgradeStatusName;
    }

    public void setUpgradeStatusName(String upgradeStatusName) {
        this.upgradeStatusName = upgradeStatusName;
    }

}
