package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.cloud_detail.cloud_package.CloudPackageActivity;
import isc.fpt.fsale.ui.fpt_camera.cloud_detail.cloud_package_netorcam.CloudPackageV2Activity;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 05,April,2019
 */

public class GetAllCloudPackageV2 implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private CloudDetail mCloudDetail;

    public GetAllCloudPackageV2(Context mContext, CloudDetail mCloudDetail, int custype, int regtype) {
        this.mContext = mContext;
        this.mCloudDetail = mCloudDetail;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("CloudTypeID", mCloudDetail.getTypeID());
            jsonObject.put("CusType", custype);
            jsonObject.put("RegType", regtype);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.msg_progress_get_all_cloud_type);
        String GET_ALL_CLOUD_TYPE = "GetAllCloudOfNet_Package";
        CallServiceTask service = new CallServiceTask(mContext, GET_ALL_CLOUD_TYPE, jsonObject,
                Services.JSON_POST_OBJECT, message, GetAllCloudPackageV2.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<CloudDetail> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), CloudDetail.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    ((CloudPackageV2Activity) mContext).loadCloudPackage(lst, mCloudDetail);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
