package isc.fpt.fsale.adapter;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionModel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class PromotionAdapter extends ArrayAdapter<PromotionModel> {
    private ArrayList<PromotionModel> lstObj;
    private Context mContext;

    public PromotionAdapter(Context context, int textViewResourceId, ArrayList<PromotionModel> lstObj) {
        super(context, textViewResourceId);
        this.mContext = context;
        this.lstObj = lstObj;
    }

    public int getCount() {
        return lstObj != null ? lstObj.size() : 0;
    }

    public PromotionModel getItem(int position) {
        return lstObj != null ? lstObj.get(position) : null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        PromotionModel item = lstObj.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_list_view_promotion_internet, null);
        }
        TextView labelDropdown = (TextView) convertView.findViewById(R.id.lbl_internet_promotion_dropdown);
        TextView labelView = (TextView) convertView.findViewById(R.id.lbl_internet_promotion);
        labelView.setVisibility(View.GONE);
        labelDropdown.setVisibility(View.VISIBLE);
        if(position % 2 == 0){
            labelDropdown.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Bold.ttf"));
        }else{
            labelDropdown.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf"));
        }
        if (item != null) {
            labelDropdown.setText(item.getPromotionName());
        }
        return convertView;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        PromotionModel item = lstObj.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_list_view_promotion_internet, null);
        }
        TextView labelDropdown = (TextView) convertView.findViewById(R.id.lbl_internet_promotion_dropdown);
        TextView labelView = (TextView) convertView.findViewById(R.id.lbl_internet_promotion);
        labelDropdown.setVisibility(View.GONE);
        labelView.setVisibility(View.VISIBLE);
        if (item != null) {
            labelView.setText(item.getPromotionName());
        }
        return convertView;
    }
}