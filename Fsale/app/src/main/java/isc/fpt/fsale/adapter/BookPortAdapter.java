package isc.fpt.fsale.adapter;

import isc.fpt.fsale.model.BookportListModel;
import java.util.ArrayList;
import isc.fpt.fsale.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class BookPortAdapter extends BaseAdapter {
	 
	private ArrayList<BookportListModel> mlist;
	private Context mContext;
	
	
	public BookPortAdapter(Context mContext,ArrayList<BookportListModel> mlist)
	{
		this.mContext=mContext;
		this.mlist=mlist;
	}
	
	@Override
	public int getCount()
	{
		if(mlist != null)
			return this.mlist.size();
		return 0;		
	}
	
	@Override
	public Object getItem(int position)
	{
		if(mlist != null)
			return this.mlist.get(position);
		return null;
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	
		BookportListModel ItemBookPort = mlist.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.list_row_ports_detail, null);
		}		
		
		TextView txtStt = (TextView) convertView.findViewById(R.id.txt_stt_billing_detail);
		txtStt.setText(ItemBookPort.getID());
		
		TextView txtName = (TextView) convertView.findViewById(R.id.txt_trang);
		txtName.setText(ItemBookPort.getName());
		
		TextView txtisemptyport = (TextView) convertView.findViewById(R.id.txt_isempty_port);
		txtisemptyport.setText(ItemBookPort.getIsEmptyPort());
		
		TextView distance = (TextView) convertView.findViewById(R.id.txt_distance);
		distance.setText(ItemBookPort.getDistance());
		
		
		return convertView;
	}
	
}
