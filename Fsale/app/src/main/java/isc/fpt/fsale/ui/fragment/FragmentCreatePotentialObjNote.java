package isc.fpt.fsale.ui.fragment;

import java.util.List;

import com.google.android.gms.maps.model.LatLng;

import isc.fpt.fsale.action.UpdatePotentialObj;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.map.action.GetGeocoder;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.location.Address;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class FragmentCreatePotentialObjNote extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_TITLE = "section_title";
    private CreatePotentialObjActivity activity;
    private EditText txtFullName, txtPhone1, txtAddress, txtNote;
    private LatLng currentLocation;

    public static FragmentCreatePotentialObjNote newInstance(int sectionNumber, String sectionTitle) {
        FragmentCreatePotentialObjNote fragment = new FragmentCreatePotentialObjNote();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentCreatePotentialObjNote() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_potential_obj_note, container, false);
        Common.setupUI(getActivity(), rootView);
        activity = (CreatePotentialObjActivity) getActivity();
        try {
            activity.enableSlidingMenu(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtFullName = (EditText) rootView.findViewById(R.id.txt_full_name);
        txtPhone1 = (EditText) rootView.findViewById(R.id.txt_phone_1);
        txtAddress = (EditText) rootView.findViewById(R.id.txt_address);
        txtNote = (EditText) rootView.findViewById(R.id.txt_note);
        Button btnUpdate = (Button) rootView.findViewById(R.id.btn_update);

        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkForUpdate()) {
                    AlertDialog.Builder builder;
                    Dialog dialog;
                    builder = new AlertDialog.Builder(activity);
                    builder.setMessage(getResources().getString(R.string.msg_confirm_update))
                            .setCancelable(false)
                            .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    updateNote();
                                }
                            })
                            .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });
                    dialog = builder.create();
                    dialog.show();
                }
            }
        });

        ImageButton btnGetAddress = (ImageButton) rootView.findViewById(R.id.btn_get_address);
        btnGetAddress.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getAddress(currentLocation);
            }
        });
        loadData();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void loadData() {
        if (activity.getCurrentPotentialObj() == null)
            activity.setCurrentPotentialObj(new PotentialObjModel());
        txtAddress.setText(activity.getCurrentPotentialObj().getAddress());
        txtFullName.setText(activity.getCurrentPotentialObj().getFullName());
        txtNote.setText(activity.getCurrentPotentialObj().getNote());
        txtPhone1.setText(activity.getCurrentPotentialObj().getPhone1());
        try {
            if (activity.getCurrentPotentialObj().getLatlng() != null && !activity.getCurrentPotentialObj().getLatlng().equals("")) {
                String[] mLatLng = activity.getCurrentPotentialObj().getLatlng().replace("(", "").replace(")", "").split(",");
                currentLocation = new LatLng(Double.valueOf(mLatLng[0]), Double.valueOf(mLatLng[1]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkForUpdate() {
        if (txtFullName.getText().toString().trim().equals("")) {
            Common.alertDialog("Vui lòng nhập tên KH", getActivity());
            txtFullName.setFocusable(true);
            txtFullName.requestFocus();
            return false;
        }
        return true;
    }

    private void updateNote() {
        initPotential();
        String UserName = ((MyApp) activity.getApplication()).getUserName();
        new UpdatePotentialObj(activity, UserName, activity.getCurrentPotentialObj(), false);
    }

    public void initPotential() {
        if (activity.getCurrentPotentialObj() == null) {
            activity.setCurrentPotentialObj(new PotentialObjModel());
        }
        activity.getCurrentPotentialObj().setFullName(txtFullName.getText().toString());
        activity.getCurrentPotentialObj().setPhone1(txtPhone1.getText().toString());
        activity.getCurrentPotentialObj().setAddress(txtAddress.getText().toString());
        activity.getCurrentPotentialObj().setNote(txtNote.getText().toString().trim().replace("\n", ""));
        activity.getCurrentPotentialObj().setLocationID(Constants.LocationID);
    }

    @SuppressLint("StaticFieldLeak")
    private void getAddress(LatLng latlng) {
        if (latlng != null) {
            final ProgressDialog pb = Common.showProgressBar(activity, "Đang lấy địa chỉ");
            new AsyncTask<LatLng, String, List<Address>>() {
                @Override
                protected List<Address> doInBackground(LatLng... params) {
                    if (params != null && params.length > 0)
                        return GetGeocoder.getAddress(activity, params[0]);
                    return null;
                }

                @Override
                protected void onPostExecute(List<Address> result) {
                    if (pb != null) {
                        pb.dismiss();
                    }
                    if (result != null && result.size() > 0) {
                        Address item = result.get(0);
                        StringBuilder address = new StringBuilder();
                        for (int i = 0; i < item.getMaxAddressLineIndex(); i++) {
                            if (i < item.getMaxAddressLineIndex() - 1) {
                                address.append(item.getAddressLine(i)).append(", ");
                            } else {
                                address.append(item.getAddressLine(i));
                            }
                        }
                        try {
                            address = new StringBuilder(Common.convertVietNamToEnglishChar(address.toString()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        txtAddress.setText(address.toString());
                    } else {
                        Common.alertDialog("Không lấy được địa chỉ. Vui lòng thử lại", getActivity());
                    }
                }

                @Override
                protected void onPreExecute() {
                }
            }.execute(latlng);
        }
    }
}