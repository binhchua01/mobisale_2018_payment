package isc.fpt.fsale.action;

import android.content.Context;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.DeployAppointmentActivity;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import static isc.fpt.fsale.R.string.msg_no_search_result;

/**
 * Created by HCM.TUANTT14 on 11/20/2017.
 * API hiển thị năng lực triển khai
 */

public class GetAbility4Days implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private DeployAppointmentActivity deployAppointmentActivity;

    public GetAbility4Days(Context mContext, String iPartnerID, String iSubID, String appointmentDate, String regCode) {
        this.mContext = mContext;
        if (mContext.getClass().getSimpleName().equals(DeployAppointmentActivity.class.getSimpleName())) {
            deployAppointmentActivity = (DeployAppointmentActivity) this.mContext;
        }
        String message = mContext.getResources().getString(R.string.msg_show_deploy_appointment);
        String GET_ABILITY_4_DAYS = "GetAbility4Days";
        String[] arrParamName = new String[]{"iPartnerID", "iSubID", "AppointmentDate", "RegCode"};
        String[] arrParamValue = new String[]{iPartnerID, iSubID, Common.reverseString(appointmentDate, "-"), regCode};
        CallServiceTask service = new CallServiceTask(mContext, GET_ABILITY_4_DAYS, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetAbility4Days.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        JSONObject jsObj = JSONParsing.getJsonObj(result);
        if (jsObj != null) {
            String RESPONSE_RESULT = "ResponseResult";
            if (jsObj.has(RESPONSE_RESULT)) {
                JSONObject jsObjResult;
                try {
                    jsObjResult = jsObj.getString(RESPONSE_RESULT) != null ?
                            jsObj.getJSONObject(RESPONSE_RESULT) : null;
                    String TAG_ERROR_CODE = "ErrorCode";
                    assert jsObjResult != null;
                    if (jsObjResult.has(TAG_ERROR_CODE) && jsObjResult.getInt(TAG_ERROR_CODE) == 0) {
                        String TAG_OBJECT = "Object";
                        jsObjResult = jsObjResult.getString(TAG_OBJECT) != null ?
                                jsObjResult.getJSONObject(TAG_OBJECT) : null;
                        String TAG_HTML = "Html";
                        if (jsObjResult != null && jsObjResult.has(TAG_HTML)) {
                            String data = jsObjResult.getString(TAG_HTML);
                            if (data != null && data.length() > 0) {
                                deployAppointmentActivity.setDataDeployAppointment(jsObjResult.getString(TAG_HTML));
                            } else {
                                deployAppointmentActivity.getWvShowDeployAppointment().setVisibility(View.GONE);
                                Common.alertDialog(mContext.getResources()
                                        .getString(msg_no_search_result), mContext);
                            }
                        } else {
                            deployAppointmentActivity.getWvShowDeployAppointment().setVisibility(View.GONE);
                            Common.alertDialog(mContext.getResources().getString(msg_no_search_result), mContext);
                        }
                    } else {
                        String TAG_ERROR = "Error";
                        Common.alertDialog(jsObjResult.getString(TAG_ERROR), mContext);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}