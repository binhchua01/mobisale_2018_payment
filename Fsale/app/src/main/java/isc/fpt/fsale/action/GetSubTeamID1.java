package isc.fpt.fsale.action;
/*
 * @Description: Check application version
 * @author: DuHK
 * @create date: 	30/05/2015
 */

import isc.fpt.fsale.activity.DeployAppointmentActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.SubTeamID1Model;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

//API lấy danh sách Đối tác và Tổ con
public class GetSubTeamID1 implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    //Add by: DuHK
    public GetSubTeamID1(Context mContext, String UserName, String RegCode) {
        this.mContext = mContext;

        String[] paramNames = {"UserName", "RegCode"};
        String[] paramValues = {UserName, RegCode};

        String message = "Đang lấy dữ liệu...";
        String METHOD_NAME = "GetSubTeamID1";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, GetSubTeamID1.this);
        service.execute();
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        try {
            SubTeamID1Model subTeamID1 = new SubTeamID1Model();
            subTeamID1.setPartnerList(new ArrayList<KeyValuePairModel>());
            subTeamID1.setSubTeamList(new ArrayList<KeyValuePairModel>());
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                String TAG_ERROR_CODE = "ErrorCode";
                if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                    String TAG_LIST_OBJECT = "ListObject";
                    JSONArray jsArr = jsObj.getJSONArray(TAG_LIST_OBJECT);
                    assert jsArr != null;
                    if (jsArr.length() > 0) {
                        jsArr = jsObj.getJSONArray(TAG_LIST_OBJECT);
                        assert jsArr != null;
                        if (jsArr.length() > 0) {
                            for (int index = 0; index < jsArr.length(); index++) {
                                int partnerID = 0, subTeam = 0;
                                String partner = "";
                                JSONObject item = jsArr.getJSONObject(index);
                                String TAG_PARTNER = "Partner";
                                if (item.has(TAG_PARTNER))
                                    partner = item.getString(TAG_PARTNER);
                                String TAG_PARTNER_ID = "PartnerID";
                                if (item.has(TAG_PARTNER_ID))
                                    partnerID = item.getInt(TAG_PARTNER_ID);
                                String TAG_SUB_TEAM = "SubTeam";
                                if (item.has(TAG_SUB_TEAM))
                                    subTeam = item.getInt(TAG_SUB_TEAM);
                                if (partnerID > 0)
                                    subTeamID1.getPartnerList().add(new KeyValuePairModel(partnerID, partner));
                                if (subTeam > 0)
                                    subTeamID1.getSupTeamList().add(new KeyValuePairModel(subTeam, String.valueOf(subTeam)));
                            }
                        }
                    }
                } else {
                    String message = "Serrivce error";
                    String TAG_ERROR = "Error";
                    if (jsObj.has(TAG_ERROR))
                        message = jsObj.getString(TAG_ERROR);
                    Common.alertDialog(message, mContext);
                }
                loadData(subTeamID1);
            }
        } catch (JSONException e) {

            Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
        }
    }

    @SuppressLint("LongLogTag")
    private void loadData(SubTeamID1Model subTeamID1) {//ListReportSBITotalActivity
        try {
            if (mContext.getClass().getSimpleName().equals(DeployAppointmentActivity.class.getSimpleName())) {
                DeployAppointmentActivity activity = (DeployAppointmentActivity) mContext;
                activity.loadData(subTeamID1);
            }
        } catch (Exception e) {
            Log.i("ReportSBITotal.loadRpCodeInfo()", e.getMessage());
        }
    }

}
