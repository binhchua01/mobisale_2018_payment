package isc.fpt.fsale.ui.fpt_camera;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.danh32.fontify.Button;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetObjectDetail;
import isc.fpt.fsale.action.GetObjectList;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ListObjectModel;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class FptCameraSelectComboActivity extends BaseActivitySecond implements OnItemClickListener<ListObjectModel> {
    private Button btnCreateContract, btnFindContract;
    private List<CategoryServiceList> mListService;
    private PotentialObjModel mPotentialObjModel;
    private Spinner spAgent;
    private EditText edtAgentName;
    private ContractListAdapter mAdapter;
    private List<ListObjectModel> mList;
    private RelativeLayout loBack;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void initEvent() {
        btnCreateContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundleFptCamera = new Bundle();
                bundleFptCamera.putParcelableArrayList(Constants.LIST_CAT_SERVICE,
                        (ArrayList<? extends Parcelable>) mListService);
                bundleFptCamera.putParcelable(Constants.POTENTIAL_OBJECT, mPotentialObjModel);
                Common.getInstance().changeActivityFinish(
                        FptCameraSelectComboActivity.this,
                        FptCameraActivity.class,
                        bundleFptCamera
                );
            }
        });

        edtAgentName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(String.valueOf(editable))) {
                    btnFindContract.setEnabled(false);
                    btnFindContract.setAlpha(0.5F);
                } else {
                    btnFindContract.setEnabled(true);
                    btnFindContract.setAlpha(1F);
                }
            }
        });

        btnFindContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edtAgentName.getText().toString())) {
                    return;
                }
                int agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
                //Tìm kiếm theo loại dịch vụ:
                //default 0: tất cả, 1: Internet, 2: Thiết bị, 3: FPT Play Box, 4: Pay TV,  5: Camera, 6: Extra OTT
                new GetObjectList(FptCameraSelectComboActivity.this, agent,
                        edtAgentName.getText().toString(), 1);
            }
        });

        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_fpt_camera_select_combo;
    }

    @Override
    protected void initView() {
        btnCreateContract = (Button) findViewById(R.id.btn_create_contract);
        btnFindContract = (Button) findViewById(R.id.btn_find_contract);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_find_result);
        spAgent = (Spinner) findViewById(R.id.sp_agent);
        edtAgentName = (EditText) findViewById(R.id.txt_agent_name);
        loBack = (RelativeLayout) findViewById(R.id.btn_back);

        mList = new ArrayList<>();
        mAdapter = new ContractListAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);

        intAgentSpinner();
        getDataIntent();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelableArrayList(Constants.LIST_CAT_SERVICE) != null) {//lấy TT PĐK khi tạo mới
            mListService = bundle.getParcelableArrayList(Constants.LIST_CAT_SERVICE);
        }
        if (bundle != null && bundle.getParcelable(Constants.POTENTIAL_OBJECT) != null) {//lấy thông tin từ KHTN
            mPotentialObjModel = bundle.getParcelable(Constants.POTENTIAL_OBJECT);
        }
    }

    private void intAgentSpinner() {
        ArrayList<KeyValuePairModel> lstSearchType = new ArrayList<>();
        lstSearchType.add(new KeyValuePairModel(1, "Số HĐ"));
        lstSearchType.add(new KeyValuePairModel(2, "Họ Tên KH"));
        lstSearchType.add(new KeyValuePairModel(3, "Số Điện Thoại"));
        lstSearchType.add(new KeyValuePairModel(4, "Địa Chỉ"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstSearchType, Gravity.CENTER);
        spAgent.setAdapter(adapter);
    }

    public void loadData(List<ListObjectModel> mList) {
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }

    public void loadDataObjectDetail(ObjectDetailModel mModelDetail) {
        Bundle bundleFptCamera = new Bundle();
        bundleFptCamera.putParcelable(Constants.TAG_CONTRACT, mModelDetail);
        bundleFptCamera.putParcelableArrayList(Constants.LIST_CAT_SERVICE,
                (ArrayList<? extends Parcelable>) mListService);
        Common.getInstance().changeActivity(
                FptCameraSelectComboActivity.this,
                FptCameraActivity.class,
                bundleFptCamera
        );
    }

    @Override
    public void onItemClick(ListObjectModel object) {
        new GetObjectDetail(this, object.getContract());
    }
}
