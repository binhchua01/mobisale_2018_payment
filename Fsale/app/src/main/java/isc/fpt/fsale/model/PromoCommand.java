package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by haulc3 on 19,July,2019
 */
public class PromoCommand implements Parcelable {
    @SerializedName("IsRecare")
    @Expose
    private Integer isRecare;
    @SerializedName("CommandText")
    @Expose
    private String commandText;
    @SerializedName("DetailPackage")
    @Expose
    private List<DetailPackage> detailPackage;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("MaxCount")
    @Expose
    private Integer maxCount;
    @SerializedName("MinCount")
    @Expose
    private Integer minCount;
    @SerializedName("TotalAmount")
    @Expose
    private Integer totalAmount;
    @SerializedName("TotalAmountVAT")
    @Expose
    private Integer totalAmountVAT;

    public PromoCommand() {
    }

    public PromoCommand(String commandText, List<DetailPackage> detailPackage, Integer iD,
                        Integer maxCount, Integer minCount, Integer totalAmount, Integer totalAmountVAT) {
        this.commandText = commandText;
        this.detailPackage = detailPackage;
        this.iD = iD;
        this.maxCount = maxCount;
        this.minCount = minCount;
        this.totalAmount = totalAmount;
        this.totalAmountVAT = totalAmountVAT;
    }

    protected PromoCommand(Parcel in) {
        commandText = in.readString();
        detailPackage = in.createTypedArrayList(DetailPackage.CREATOR);
        if (in.readByte() == 0) {
            iD = null;
        } else {
            iD = in.readInt();
        }
        if (in.readByte() == 0) {
            maxCount = null;
        } else {
            maxCount = in.readInt();
        }
        if (in.readByte() == 0) {
            minCount = null;
        } else {
            minCount = in.readInt();
        }
        if (in.readByte() == 0) {
            totalAmount = null;
        } else {
            totalAmount = in.readInt();
        }
        if (in.readByte() == 0) {
            totalAmountVAT = null;
        } else {
            totalAmountVAT = in.readInt();
        }
    }

    public static final Creator<PromoCommand> CREATOR = new Creator<PromoCommand>() {
        @Override
        public PromoCommand createFromParcel(Parcel in) {
            return new PromoCommand(in);
        }

        @Override
        public PromoCommand[] newArray(int size) {
            return new PromoCommand[size];
        }
    };

    public Integer getIsRecare() {
        return isRecare;
    }

    public void setIsRecare(Integer isRecare) {
        this.isRecare = isRecare;
    }

    public String getCommandText() {
        return commandText;
    }

    public void setCommandText(String commandText) {
        this.commandText = commandText;
    }

    public List<DetailPackage> getDetailPackage() {
        return detailPackage;
    }

    public void setDetailPackage(List<DetailPackage> detailPackage) {
        this.detailPackage = detailPackage;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }

    public Integer getMinCount() {
        return minCount;
    }

    public void setMinCount(Integer minCount) {
        this.minCount = minCount;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalAmountVAT() {
        return totalAmountVAT;
    }

    public void setTotalAmountVAT(Integer totalAmountVAT) {
        this.totalAmountVAT = totalAmountVAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(commandText);
        dest.writeTypedList(detailPackage);
        if (iD == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(iD);
        }
        if (maxCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(maxCount);
        }
        if (minCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(minCount);
        }
        if (totalAmount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalAmount);
        }
        if (totalAmountVAT == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalAmountVAT);
        }
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("IsRecare", getIsRecare());
            jsonObject.put("ID", getID());
            jsonObject.put("TotalAmount", getTotalAmount());
            jsonObject.put("TotalAmountVAT", getTotalAmountVAT());
            jsonObject.put("CommandText", getCommandText());
            jsonObject.put("MinCount", getMinCount());
            jsonObject.put("MaxCount", getMaxCount());
            JSONArray jsonArray = new JSONArray();
            for(DetailPackage item : getDetailPackage()){
                jsonArray.put(item.toJsonObject());
            }
            jsonObject.put("DetailPackage", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
