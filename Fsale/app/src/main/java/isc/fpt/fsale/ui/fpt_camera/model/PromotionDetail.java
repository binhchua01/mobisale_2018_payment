package isc.fpt.fsale.ui.fpt_camera.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by haulc3 on 05,September,2019
 */
public class PromotionDetail implements Parcelable {
    private int PromoID;
    private String PromoName;
    private int Cost;
    private int Qty;
    private int Total;
    private int ServiceType;
    private boolean isSelected = false;

    public PromotionDetail() {
    }


    protected PromotionDetail(Parcel in) {
        PromoID = in.readInt();
        PromoName = in.readString();
        Cost = in.readInt();
        Qty = in.readInt();
        Total = in.readInt();
        ServiceType = in.readInt();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<PromotionDetail> CREATOR = new Creator<PromotionDetail>() {
        @Override
        public PromotionDetail createFromParcel(Parcel in) {
            return new PromotionDetail(in);
        }

        @Override
        public PromotionDetail[] newArray(int size) {
            return new PromotionDetail[size];
        }
    };

    public int getPromoID() {
        return PromoID;
    }

    public void setPromoID(int promoID) {
        PromoID = promoID;
    }

    public String getPromoName() {
        return PromoName;
    }

    public void setPromoName(String promoName) {
        PromoName = promoName;
    }

    public int getCost() {
        return Cost;
    }

    public void setCost(int cost) {
        Cost = cost;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeInt(PromoID);
        parcel.writeString(PromoName);
        parcel.writeInt(Cost);
        parcel.writeInt(Qty);
        parcel.writeInt(Total);
        parcel.writeInt(ServiceType);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
    }


    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("PromoID", getPromoID());
            jsonObject.put("PromoName", getPromoName());
            jsonObject.put("Cost", getCost());
            jsonObject.put("Qty", getQty());
            jsonObject.put("Total", getTotal());
            jsonObject.put("ServiceType", getServiceType());
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
