package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.DeployAppointmentModel;

public class DeployAppointmentAdapter extends BaseAdapter {
    private List<DeployAppointmentModel> mList;
    private Context mContext;

    public DeployAppointmentAdapter(Context context, List<DeployAppointmentModel> lst) {
        this.mContext = context;
        this.mList = lst;
    }

    @Override
    public int getCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public DeployAppointmentModel getItem(int position) {
        return mList != null ? mList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        DeployAppointmentModel item = mList.get(position);
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.row_deploy_appointment, null);
        }
        bindViews(item, view, position);

        return view;
    }

    @SuppressLint("SetTextI18n")
    private void bindViews(DeployAppointmentModel item, View view, int position) {
        if (item != null) {
            TextView tvNumRow = (TextView) view.findViewById(R.id.lbl_row_number);
            TextView tvDeployAppointment = (TextView) view.findViewById(R.id.lbl_deploy_appointment_contract);
            TextView tvCreateBy = (TextView) view.findViewById(R.id.lbl_deploy_appointment_create_by);
            TextView tvCreateDate = (TextView) view.findViewById(R.id.lbl_deploy_appointment_create_date);
            TextView tvDeployDate = (TextView) view.findViewById(R.id.lbl_deploy_appointment_date);
            TextView tvPartner = (TextView) view.findViewById(R.id.lbl_deploy_appointment_partner);
            TextView tvRegCode = (TextView) view.findViewById(R.id.lbl_deploy_appointment_reg_code);
            TextView tvSubTeam = (TextView) view.findViewById(R.id.lbl_deploy_appointment_sub_team);
            TextView tvTimeZone = (TextView) view.findViewById(R.id.lbl_deploy_appointment_time_zone);

            tvCreateBy.setText(item.getAccount());
            tvDeployAppointment.setText(item.getContract());
            tvNumRow.setText(String.valueOf(position + 1));
            tvCreateDate.setText(item.getDate());
            tvDeployDate.setText(item.getAppointmentDate());
            tvPartner.setText(item.getPartner() + "(" + item.getPartnerID() + ")");
            tvRegCode.setText(item.getRegCode());
            tvSubTeam.setText(String.valueOf(item.getSubID()));
            tvTimeZone.setText(item.getTimezone());
        }
    }

    public void notifyData(List<DeployAppointmentModel> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
