package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 10/22/2018.
 */
//model chứa thông tin CMND
public class IdentityCard implements Parcelable {
    private String id, name, dob, address, sex;

    public IdentityCard() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    protected IdentityCard(Parcel in) {
        id = in.readString();
        name = in.readString();
        dob = in.readString();
        address = in.readString();
        sex = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(dob);
        dest.writeString(address);
        dest.writeString(sex);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IdentityCard> CREATOR = new Creator<IdentityCard>() {
        @Override
        public IdentityCard createFromParcel(Parcel in) {
            return new IdentityCard(in);
        }

        @Override
        public IdentityCard[] newArray(int size) {
            return new IdentityCard[size];
        }
    };
}
