package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetReportSBITotal;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ReportSBITotalAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportSBITotalModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

public class ListReportSBITotalActivity extends BaseActivity {

    private Spinner spDay, spMonth, spYear, spStatus, spAgent;
    private ListView lvResult;
    private LinearLayout frmFind;
    private ImageView imgNavigation;
    private Spinner spPage;
    private EditText txtAgentName;

    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
    private int Status = 0, Day = 0, Month = 0, Year = 0, Agent = 0;
    private String AgentName;

    public ListReportSBITotalActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.menu_report_sbi_total);
    }

    @SuppressWarnings("static-access")
    @Override
    public void startActivity(Intent intent) {
        // TODO Auto-generated method stub
        //Finish this activity after start another activity without sub Report

        if (!intent.getComponent().getClassName().equals(ListReportSBIDetailActivity.class.getName()))
            finish();
        if (isShowLeftMenu() && Constants.SLIDING_MENU != null)
            toggle(Constants.SLIDING_MENU.LEFT);
        super.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_report_sbi));

        setContentView(R.layout.activity_report_sbi_total);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        spDay = (Spinner) findViewById(R.id.sp_day);
        spMonth = (Spinner) findViewById(R.id.sp_month);
        spYear = (Spinner) findViewById(R.id.sp_year);
        spStatus = (Spinner) findViewById(R.id.sp_status);
        lvResult = (ListView) findViewById(R.id.lv_report);
        txtAgentName = (EditText) findViewById(R.id.txt_agent_name);

        final Button btnFind = (Button) findViewById(R.id.btn_find);
        imgNavigation = (ImageView) findViewById(R.id.img_navigation_drop_down);
        frmFind = (LinearLayout) findViewById(R.id.frm_find);
        btnFind.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (FLAG_FIRST_LOAD > 0)
                    FLAG_FIRST_LOAD = 0;
                getReport(1);
            }
        });

        imgNavigation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dropDownNavigation();
            }
        });
        spYear.setOnItemSelectedListener(new OnItemSelectedListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                int month = 0, year = 0;
                try {
                    year = item.getID();
                    month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
                } catch (Exception e) {

                    Log.i("ContractFilterActivity_spYear.setOnItemSelectedListener:", e.getMessage());
                }
                //common.
                Common.initDaySpinner(ListReportSBITotalActivity.this, spDay, month, year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spMonth.setOnItemSelectedListener(new OnItemSelectedListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                int month = 0, year = 0;
                try {
                    month = item.getID();
                    year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();
                } catch (Exception e) {

                    Log.i("ContractFilterActivity_spMonth.setOnItemSelectedListener:", e.getMessage());
                }
                Common.initDaySpinner(ListReportSBITotalActivity.this, spDay, month, year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        lvResult.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            }
        });

        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getReport(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spAgent = (Spinner) findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() > 0) {
                    txtAgentName.setEnabled(true);
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        Common.initYearSpinner(ListReportSBITotalActivity.this, spYear);
        Common.initMonthSpinner(ListReportSBITotalActivity.this, spMonth);
        initSpinnerStatus();
        initSpinnerAgent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    //TODO: Show/Hide Search Form
    private void dropDownNavigation() {
        if (frmFind.getVisibility() == View.VISIBLE) {
            frmFind.setVisibility(View.GONE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
        } else {
            frmFind.setVisibility(View.VISIBLE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
        }
    }

    private void initSpinnerStatus() {
        ArrayList<KeyValuePairModel> listStatus = new ArrayList<>();
        listStatus.add(new KeyValuePairModel(-2, "Tất cả"));
        listStatus.add(new KeyValuePairModel(-1, "Còn hạn/Hết hạn"));
        listStatus.add(new KeyValuePairModel(0, "Hết hạn"));
        listStatus.add(new KeyValuePairModel(1, "Còn hạn"));
        listStatus.add(new KeyValuePairModel(2, "Đã sử dụng"));
        listStatus.add(new KeyValuePairModel(3, "Đã huỷ"));
		/*
		 *    -2: Tất cả các SBI   
			   -1: Hết hạn + còn hạn  
			    0: Hết hạn sử dụng  
			    1: Còn hạn sử dụng  
			    2: Đã sử dụng  
			    3: Hủy  
		 * */
        KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(this, R.layout.my_spinner_style, listStatus, Gravity.LEFT);
        spStatus.setAdapter(adapterStatus);
    }

    private void initSpinnerAgent() {
        ArrayList<KeyValuePairModel> listAgent = new ArrayList<>();
        listAgent.add(new KeyValuePairModel(0, "Tất cả"));
        listAgent.add(new KeyValuePairModel(1, "Số SBI"));
        listAgent.add(new KeyValuePairModel(2, "Số TTKH"));
        listAgent.add(new KeyValuePairModel(3, "Nhân viên"));
		/*
		 *    -2: Tất cả các SBI   
			   -1: Hết hạn + còn hạn  
			    0: Hết hạn sử dụng  
			    1: Còn hạn sử dụng  
			    2: Đã sử dụng  
			    3: Hủy  
		 * */
        KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(this, R.layout.my_spinner_style, listAgent, Gravity.LEFT);
        spAgent.setAdapter(adapterStatus);
    }

    private void getReport(int PageNumber) {
        String UserName = Constants.USERNAME;
        mPage = PageNumber;
        Status = ((KeyValuePairModel) spStatus.getSelectedItem()).getID();
        Day = ((KeyValuePairModel) spDay.getSelectedItem()).getID();
        Month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
        Year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();
        AgentName = txtAgentName.getText().toString().trim();
        Agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
        if (Agent > 0 && AgentName.equals("")) {
            Common.alertDialog(getString(R.string.msg_search_value_empty), this);
        } else {
            new GetReportSBITotal(this, UserName, Day, Month, Year, Agent, AgentName, Status, PageNumber);
        }
    }

    public void loadData(ArrayList<ReportSBITotalModel> lst) {
        if (lst != null && lst.size() > 0) {
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                dropDownNavigation();
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
                //adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            ReportSBITotalAdapter adapter = new ReportSBITotalAdapter(this, lst, Day, Month, Year, Agent, AgentName, mPage);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
            lvResult.setAdapter(new ReportSBITotalAdapter(this, new ArrayList<ReportSBITotalModel>(), 0, 0, 0, 0, "", 0));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
