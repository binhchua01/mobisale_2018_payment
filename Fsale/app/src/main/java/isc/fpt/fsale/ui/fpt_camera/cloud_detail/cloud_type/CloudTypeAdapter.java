package isc.fpt.fsale.ui.fpt_camera.cloud_detail.cloud_type;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;

/**
 * Created by Hau Le on 2018-12-26.
 */
public class CloudTypeAdapter extends RecyclerView.Adapter<CloudTypeAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<CloudDetail> mList;
    private OnItemClickListener mListener;

    CloudTypeAdapter(Context mContext, List<CloudDetail> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_dialog_cloud_type, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressServiceWhenClick(mList.get(mViewHolder.getAdapterPosition()), mViewHolder);
            }
        });
        return mViewHolder;
    }

    private void progressServiceWhenClick(CloudDetail mCloudDetail, SimpleViewHolder mViewHolder) {
        mListener.onItemClick(mCloudDetail);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.textView.setText(mList.get(position).getTypeName());
        if (mList.get(position).isSelected()) {
            holder.layout.setBackgroundResource(R.drawable.background_radius_selected);
        }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        private View layout;

        SimpleViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv_item_name);
            layout = itemView.findViewById(R.id.layout_background);
        }
    }

    public void notifyData(List<CloudDetail> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
