package isc.fpt.fsale.activity.choose_mac_fpt_box;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListMacOTT;
import isc.fpt.fsale.adapter.MacOttAdapter;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.MacOTT;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class ChooseMacActivity extends BaseActivitySecond implements OnItemClickListener<List<MacOTT>> {
    private View loBack;
    private Button btnUpdate, btnCancel;
    private MacOttAdapter mAdapter;
    private List<MacOTT> mList;
    private List<MacOTT> mListSelected;

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListSelected != null && mListSelected.size() != 0) {
                    Intent returnIntent = new Intent();
                    returnIntent.putParcelableArrayListExtra(Constants.LIST_MAC_SELECTED,
                            (ArrayList<? extends Parcelable>) mListSelected);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Common.alertDialog("Vui lòng chọn ít nhất 1 MAC", ChooseMacActivity.this);
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_choose_mac;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.lbl_screen_name_mac_list));
        btnUpdate = (Button) findViewById(R.id.btn_update);
        btnCancel = (Button) findViewById(R.id.btn_cancel);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_mac);
        mList = new ArrayList<>();
        mListSelected = new ArrayList<>();
        mAdapter = new MacOttAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);

        getDataIntent();
    }

    private void getDataIntent() {
        List<MaxyBox> mBox = new ArrayList<>();
        List<MaxyBox> mListBox = getIntent().getParcelableArrayListExtra(Constants.LIST_FPT_BOX_SELECTED);
        assert mListBox != null;
        for(MaxyBox item : mListBox){
            if(item.getBoxType() == 4){
                mBox.add(item);
            }
        }
        mListSelected = getIntent().getParcelableArrayListExtra(Constants.LIST_MAC_SELECTED);
        //if (mListFptBox == null || mListFptBox.size() == 0) return;
        //gọi API lấy DS MAC
        new GetListMacOTT(this, mBox);
    }

    public void loadMacList(List<MacOTT> mList) {
        this.mList = mList;
        for (MacOTT item : this.mList) {
            for (MacOTT itemSelected : mListSelected) {
                if (item.getMAC().equals(itemSelected.getMAC())) {
                    item.setSelected(true);
                }
            }
        }
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(List<MacOTT> mListMacSelected) {
        mListSelected.clear();
        for (MacOTT item : mList) {
            if (item.isSelected()) {
                mListSelected.add(item);
            }
        }
    }
}
