package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabWidget;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckHouseNumber;
import isc.fpt.fsale.action.CheckRegistration2;
import isc.fpt.fsale.adapter.CustomViewPagerAdapter;
import isc.fpt.fsale.callback.RegisterCallback;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.CheckListService;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.IdentityCard;
import isc.fpt.fsale.model.ImageDocument;
import isc.fpt.fsale.model.InternetTotal;
import isc.fpt.fsale.model.IpTvTotal;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ListGiftBoxOTT;
import isc.fpt.fsale.model.ListOTT;
import isc.fpt.fsale.model.PackageModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.PromotionIPTVModel;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.camera319.Camera2Total;
import isc.fpt.fsale.model.camera319.PromoCloud;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCameraOfNet;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep1;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3v2;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.ui.maxytv.model.MaxyGeneral;
import isc.fpt.fsale.ui.maxytv.model.MaxyTotal;
import isc.fpt.fsale.ui.maxytv.model.ObjectMaxy;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import static isc.fpt.fsale.activity.PromotionIPTVFilterActivity.TAG_PROMOTION_TYPE_BOX;
import static isc.fpt.fsale.utils.Constants.TAG_LIST_IMAGE_DOCUMENT;

/**
 * @author ISC-HUNGLQ9
 * update by haulc3
 */

@SuppressLint("ParcelCreator")
public class RegisterActivityNew extends BaseActivity implements
        ViewPager.OnPageChangeListener, OnTabChangeListener, RegisterCallback {
    public RegistrationDetailModel modelDetail;
    private Context mContext;
    private ViewPager pager;
    private TabHost mTabHost;
    List<Fragment> listBaseFragments;
    public FragmentRegisterStep1 step1;
    //public FragmentRegisterStep3 step2;
    public FragmentRegisterStep3v2 step2;
    public FragmentRegisterStep4 step3;
    public int serviceType;
    private List<CategoryServiceList> mListService;
    public boolean isUpdatePromotion = false;
    public int isCusType = 0;

    private int[] tabIcons = {
            R.drawable.tab_customer_info,
            R.drawable.tab_service,
            R.drawable.tab_money_total
    };

    public RegisterActivityNew() {
        super(R.string.lbl_create_registration);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_register_activity));
        setContentView(R.layout.activity_register_v2);
        this.mContext = this;
        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                modelDetail = myIntent.getParcelableExtra(Constants.MODEL_REGISTER);
                if (myIntent.getParcelableArrayListExtra(Constants.LIST_CAT_SERVICE) != null) {
                    mListService = myIntent.getParcelableArrayListExtra(Constants.LIST_CAT_SERVICE);
                } else {
                    mListService = modelDetail.getCategoryServiceList();
                }
                PotentialObjModel potentialObj = myIntent.getParcelableExtra(Constants.POTENTIAL_OBJECT);
                if (potentialObj != null && modelDetail == null) {
                    modelDetail = new RegistrationDetailModel();
                    modelDetail.setAddress(potentialObj.getAddress());
                    modelDetail.setBillTo_City(potentialObj.getBillTo_City());
                    modelDetail.setBillTo_CityVN(potentialObj.getBillTo_CityVN());
                    modelDetail.setBillTo_District(potentialObj.getBillTo_District());
                    modelDetail.setBillTo_DistrictVN(potentialObj.getBillTo_DistrictVN());
                    modelDetail.setBillTo_Number(potentialObj.getBillTo_Number());
                    modelDetail.setBillTo_Street(potentialObj.getBillTo_Street());
                    modelDetail.setBillTo_StreetVN(potentialObj.getBillTo_StreetVN());
                    modelDetail.setBillTo_Ward(potentialObj.getBillTo_Ward());
                    modelDetail.setBillTo_WardVN(potentialObj.getBillTo_WardVN());
                    modelDetail.setBranchCode(potentialObj.getBranchCode());
                    modelDetail.setContact_1(potentialObj.getContact1());
                    modelDetail.setContact_2(potentialObj.getContact2());
                    modelDetail.setCreateBy(potentialObj.getCreateBy());
                    modelDetail.setCreateDate(potentialObj.getCreateDate());
                    modelDetail.setCusType(potentialObj.getCustomerType());
                    modelDetail.setNote(potentialObj.getNote());
                    modelDetail.setEmail(potentialObj.getEmail());
                    modelDetail.setFloor(potentialObj.getFloor());
                    modelDetail.setFullName(potentialObj.getFullName());
                    modelDetail.setLatlng(potentialObj.getLatlng());
                    modelDetail.setLocationID(potentialObj.getLocationID());
                    modelDetail.setLot(potentialObj.getLot());
                    modelDetail.setNameVilla(potentialObj.getNameVilla());
                    modelDetail.setNameVillaVN(potentialObj.getNameVillaVN());
                    modelDetail.setPassport(potentialObj.getPassport());
                    if (!potentialObj.getPhone1().trim().equals("")) {
                        modelDetail.setPhone_1(potentialObj.getPhone1());
                        modelDetail.setType_1(1);
                    }
                    if (!potentialObj.getPhone2().trim().equals("")) {
                        modelDetail.setPhone_2(potentialObj.getPhone2());
                        modelDetail.setType_2(1);
                    }
                    if (potentialObj.getISPType() > 0) {
                        modelDetail.setObjectType(2);
                        modelDetail.setISPType(potentialObj.getISPType());
                    }
                    modelDetail.setObjectCameraOfNet(modelDetail.getObjectCameraOfNet() == null ?
                            new ObjectCameraOfNet(
                                    new ArrayList<>(), new ArrayList<>(), new SetupDetail(), new PromoCloud(), 0
                            ) :
                            modelDetail.getObjectCameraOfNet()
                    );
                    modelDetail.setObjectMaxy(modelDetail.getObjectMaxy() == null ?
                            new ObjectMaxy(new MaxyGeneral(), new ArrayList<>(), new ArrayList<>()
                            ) : modelDetail.getObjectMaxy()
                    );
                    modelDetail.setPosition(potentialObj.getPosition());
                    modelDetail.setRoom(potentialObj.getRoom());
                    modelDetail.setSupporter(potentialObj.getSupporter());
                    modelDetail.setTaxId(potentialObj.getTaxID());
                    modelDetail.setPotentialID(potentialObj.getID());
                    modelDetail.setTypeHouse(potentialObj.getTypeHouse());
                    modelDetail.setCheckListService(
                            new CheckListService(
                                    false,
                                    false,
                                    false,
                                    true,
                                    false,
                                    false,
                                    false
                            )
                    );
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            modelDetail = null;
        }
        initViewPager();
        initialiseTabHost();
    }

    public void setIsUpdatePromotion(boolean isUpdatePromotion) {
        this.isUpdatePromotion = isUpdatePromotion;
    }

    public void setIsCusType(int value) {
        this.isCusType = value;
    }

    public int getIsCusType() {
        return isCusType;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    public RegistrationDetailModel getRegistrationDetail() {
        return modelDetail != null ? modelDetail : new RegistrationDetailModel();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_right) {
            updateOnclick();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.update_icon, menu);
        menu.findItem(R.id.action_right).setVisible(false);
        return true;
    }

    @SuppressLint("ObsoleteSdkInt")
    private void initialiseTabHost() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();

        View tabView1 = createTabView(this, getResources().getString(R.string.lbl_tab_cus_info), tabIcons[0]);
        View tabView2 = createTabView(this, getResources().getString(R.string.lbl_tab_service), tabIcons[1]);
        View tabView3 = createTabView(this, getResources().getString(R.string.lbl_tab_total), tabIcons[2]);

        RegisterActivityNew.AddTab(this, this.mTabHost, this.mTabHost
                .newTabSpec("Tab1").setIndicator(tabView1));
        RegisterActivityNew.AddTab(this, this.mTabHost, this.mTabHost
                .newTabSpec("Tab2").setIndicator(tabView2));
        RegisterActivityNew.AddTab(this, this.mTabHost, this.mTabHost
                .newTabSpec("Tab3").setIndicator(tabView3));

        if (Build.VERSION.SDK_INT >= 11)
            mTabHost.getTabWidget().setShowDividers(TabWidget.SHOW_DIVIDER_MIDDLE);

        mTabHost.setOnTabChangedListener(this);
    }

    private static View createTabView(final Context context, final String text, int tabIcon) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
        TextView tv = view.findViewById(R.id.tabsText);
        ImageView img = view.findViewById(R.id.img_icon);
        img.setImageResource(tabIcon);
        tv.setText(text);
        return view;
    }

    private static void AddTab(RegisterActivityNew activity, TabHost tabHost, TabHost.TabSpec tabSpec) {
        tabSpec.setContent(new MyTabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    private void initViewPager() {
        pager = (ViewPager) findViewById(R.id.pager);
        listBaseFragments = new ArrayList<>();
        step1 = FragmentRegisterStep1.newInstance(modelDetail);
        step2 = FragmentRegisterStep3v2.newInstance(modelDetail);
        step3 = FragmentRegisterStep4.newInstance(modelDetail);
        listBaseFragments.add(step1);
        listBaseFragments.add(step2);
        listBaseFragments.add(step3);
        CustomViewPagerAdapter adapter = new CustomViewPagerAdapter(
                getSupportFragmentManager(), listBaseFragments);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(listBaseFragments.size());
        pager.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageSelected(int arg0) {
        try {
            int post = pager.getCurrentItem();
            mTabHost.setCurrentTab(post);
            if (post == 2) {
                if (step3 != null) {
                    step3.is_have_device = step2.is_have_device;
                    step3.is_have_ip_tv = step2.is_have_ip_tv;
                    step3.is_have_fpt_box = step2.is_have_fpt_box;
                    step3.is_have_camera = step2.is_have_camera;
                    step3.is_have_maxy = step2.is_have_maxy;
                    step3.is_clear_rp_and_voucher = step2.is_clear_rp_and_voucher;
                    if (step3.is_have_device && step2.getListDeviceSelect() != null) {
                        for (Device item : step2.getListDeviceSelect()) {
                            if (item.getPriceID() == 0) {
                                pager.setCurrentItem(1);
                                Common.alertDialog("Chưa chọn giá thiết bị!", this);
                                return;
                            }
                        }
                    }
                    if (step3.is_clear_rp_and_voucher) {
                        step3.clearDataRpVoucher(true);
                        step3.disableEnableControlsEVoucher(step3.layoutDisable, true);
                        step2.is_clear_rp_and_voucher = false;
                    }

                    step3.getAllPrice(step2.getListDeviceSelect(), step2.modelDetail.getObjectMaxy());

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTabChanged(String tabId) {
        int pos = this.mTabHost.getCurrentTab();
        this.pager.setCurrentItem(pos);
        Common.hideSoftKeyboard(this);
    }

    public void setPageViewpager(int index) {
        if (pager.getCurrentItem() != index) {
            pager.setCurrentItem(index);
        }
    }

    public int getServiceType() {
        return step2.serviceType;
    }

    public ListGiftBoxOTT getListGiftBoxOTT() {
        ArrayList<ListOTT> listOTTS = new ArrayList<ListOTT>();
        for (FPTBox item : step2.listFptBoxSelect) {
            listOTTS.add(new ListOTT(item.getOTTID(), item.getPromotionID(), item.getOTTCount()));
        }
        return new ListGiftBoxOTT(
                listOTTS,
                Integer.parseInt(Constants.LOCATION_ID),
                Integer.parseInt(Constants.BRANCH_CODE),
                Constants.USERNAME,
                String.valueOf(step1.spCusDetail.getSelectedItem() != null ? ((KeyValuePairModel) step1.spCusDetail.getSelectedItem()).getID() : 0));
    }

    public JSONObject getListGiftBox() {
        JSONObject jsonObject = new JSONObject();
        //get service type
        JSONArray jsonArray = new JSONArray();
        if (step2.frmInternet.getVisibility() == View.VISIBLE) {
            jsonArray.put(1);//internet
        }

        if (step2.is_have_device) {
            jsonArray.put(2);//thiết bị lẻ
        }

        if (step2.is_have_fpt_box) {
            jsonArray.put(3);//fpt play box
        }

        if (step2.frmIpTv.getVisibility() == View.VISIBLE) {
            jsonArray.put(4);//iptv
        }

        if(step2.is_have_maxy){
            jsonArray.put(8);//maxy
        }
        try {
            jsonObject.put("ServiceType", jsonArray);
            jsonObject.put("BranchCode", Constants.BRANCH_CODE);
            jsonObject.put("RegType", "0");//0 - bán mới, 1 - bán thêm
            if (step2.frmInternet.getVisibility() == View.VISIBLE) {
                jsonObject.put("NetPromotionID", step2.selectedInternetPromotion != null ?
                        step2.selectedInternetPromotion.getPromotionID() : 0);
            } else {
                jsonObject.put("NetPromotionID", 0);
            }

            if (step2.frmIpTv.getVisibility() == View.VISIBLE) {
                jsonObject.put("IPTVPromotionID", step2.promotionIPTVModelBox1 != null ?
                        step2.promotionIPTVModelBox1.getPromotionID() : 0);
                jsonObject.put("IPTVPromotionIDBoxOrder", step2.promotionIPTVModelBox2 != null ?
                        String.valueOf(step2.promotionIPTVModelBox2.getPromotionID()) : "0");
                jsonObject.put("IPTVPromotionType", step2.promotionIPTVModelBox1 != null ?
                        String.valueOf(step2.promotionIPTVModelBox1.getType()) : "0");
            } else {
                jsonObject.put("IPTVPromotionID", 0);
                jsonObject.put("IPTVPromotionIDBoxOrder", "0");
                jsonObject.put("IPTVPromotionType", "0");
            }

            jsonObject.put("LocalType", step2.localTypeId);
            jsonObject.put("LocationID", String.valueOf(Constants.LOCATION_ID));
            jsonObject.put("UserName", String.valueOf(Constants.USERNAME));
            jsonObject.put("CusTypeDetail", step1.spCusDetail.getSelectedItem() != null ?
                    String.valueOf(((KeyValuePairModel) step1.spCusDetail.getSelectedItem()).getID()) : "0");
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateOnclick() {
        try {
            final int sHouseTypeSelected = ((KeyValuePairModel) step1.spHouseTypes.getSelectedItem()).getID();
            if (checkForUpdateRegister(sHouseTypeSelected)) {
                if (pager.getCurrentItem() == 2) {
                    AlertDialog.Builder builder;
                    Dialog dialog;
                    builder = new AlertDialog.Builder(mContext);
                    builder.setMessage(mContext.getResources().getString(R.string.msg_confirm_update))
                            .setCancelable(false)
                            .setPositiveButton(mContext.getResources().getString(R.string.msg_button_yes),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Update(true);
                                        }
                                    })
                            .setNegativeButton(mContext.getResources().getString(R.string.msg_button_no),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    dialog = builder.create();
                    dialog.show();
                } else {
                    setPageViewpager(2);
                    Common.alertDialog(
                            mContext.getResources().getString(R.string.msg_alert_check_registration), this);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // gọi api cập nhật thông tin màn hình bán mới
    public void Update(boolean isUpdate) {
        try {
            if (modelDetail == null) {
                modelDetail = new RegistrationDetailModel();
            }
            NumberFormat nf = NumberFormat.getInstance(Locale.FRENCH);
            modelDetail.setLocationID(Integer.parseInt(Constants.LOCATION_ID));
            modelDetail.setID(modelDetail.getID());
            modelDetail.setObjID(modelDetail.getObjID());
            modelDetail.setRegCode(modelDetail.getRegCode());
            modelDetail.setUserName(Constants.USERNAME);
            modelDetail.setPotentialID(modelDetail.getPotentialID());
            modelDetail.setImage(modelDetail.getImage());
            modelDetail.setFullName(step1.txtCustomerName.getText().toString());
            modelDetail.setBirthDay(step1.txtBirthDay != null ? step1.txtBirthDay.getText().toString() : "");
            modelDetail.setPassport(step1.txtIdentityCard.getText().toString().trim());
            modelDetail.setAddressPassport(step1.txtAddressId.getText().toString());
            modelDetail.setPhone_1(step1.txtPhone1.getText().toString().trim());
            modelDetail.setPhone_2(step1.txtPhone2.getText().toString().trim());
            modelDetail.setTaxId(step1.txtTaxNum.getText().toString().trim().replace("\n", ""));
            modelDetail.setAddress(GetAddress());
            modelDetail.setBillTo_City((Constants.LST_REGION.size() > 0) ?
                    Constants.LST_REGION.get(0).getDescription() : "");
            modelDetail.setBillTo_District(step1.spDistricts.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spDistricts.getSelectedItem()).getsID() : "");
            modelDetail.setBillTo_Street(step1.spStreets.getAdapter() != null &&
                    step1.spStreets.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spStreets.getSelectedItem()).getsID() : "");
            modelDetail.setBillTo_Ward(step1.spWards.getAdapter() != null &&
                    step1.spWards.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spWards.getSelectedItem()).getsID() : "");
            KeyValuePairModel houseType = ((KeyValuePairModel) step1.spHouseTypes.getSelectedItem());
            if (houseType != null && houseType.getID() == 2) {// Loai nha la chung cu
                modelDetail.setNameVilla(step1.spApartment.getAdapter() != null &&
                        step1.spApartment.getSelectedItem() != null ?
                        ((KeyValuePairModel) step1.spApartment.getSelectedItem()).getsID() : null);
                modelDetail.setBillTo_Number("");
            } else {
                modelDetail.setNameVilla("");
                modelDetail.setBillTo_Number(step1.txtHouseNum.getText().toString().trim());
            }
            modelDetail.setTypeHouse((step1.spHouseTypes.getAdapter() != null &&
                    step1.spHouseTypes.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spHouseTypes.getSelectedItem()).getID() : 0));
            modelDetail.setPosition(step1.housePositionPanel.getVisibility() ==
                    View.VISIBLE && step1.spHousePosition.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spHousePosition.getSelectedItem()).getID() : 0);
            modelDetail.setFloor(step1.txtFloor.getText().toString().trim());
            modelDetail.setLot(step1.txtLot.getText().toString().trim());
            modelDetail.setRoom(step1.txtRoom.getText().toString().trim());
            modelDetail.setBookPortType(modelDetail.getBookPortType());
            modelDetail.setBranchCode(modelDetail.getBranchCode());
            modelDetail.setContact(step1.txtContactPhone1.getText().toString().trim());
            modelDetail.setContact_1(step1.txtContactPhone1.getText().toString().trim());
            modelDetail.setContact_2(step1.txtContactPhone2.getText().toString().trim());
            modelDetail.setType_1(step1.spPhone1.getAdapter() != null &&
                    step1.spPhone1.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spPhone1.getSelectedItem()).getID() : 0);
            modelDetail.setType_2(step1.spPhone2.getAdapter() != null &&
                    step1.spPhone2.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spPhone2.getSelectedItem()).getID() : 0);
            modelDetail.setContract(modelDetail.getContract());
            modelDetail.setCreateBy(modelDetail.getCreateBy());
            modelDetail.setCreateDate(modelDetail.getCreateDate());
            modelDetail.setCusType(step1.spCusType.getAdapter() != null &&
                    step1.spCusType.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spCusType.getSelectedItem()).getID() : 0);
            modelDetail.setCusTypeDetail(String.valueOf(step1.spCusDetail.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spCusDetail.getSelectedItem()).getID() : 0));
            modelDetail.setDeposit(step3.SpDepositNewObject.getSelectedItem() != null ?
                    ((KeyValuePairModel) step3.SpDepositNewObject.getSelectedItem()).getID() : 0);
            modelDetail.setDepositBlackPoint(0);
            modelDetail.setDescription(modelDetail.getDescription());
            modelDetail.setDescriptionIBB(step1.txtDescriptionIBB.getText().toString()
                    .trim().replace("\n", ""));
            modelDetail.setNote(step1.txtHouseDesc.getText().toString().trim().replace("\n", ""));
            modelDetail.setDivisionID(0);
            modelDetail.setEmail(step1.txtEmail.getText().toString().trim());
            modelDetail.setEoC(0);
            modelDetail.setInDoor(step3.txtIndoor.getText().toString().equals("") ? 0 :
                    Integer.valueOf(step3.txtIndoor.getText().toString()));
            modelDetail.setOutDoor(step3.txtOutdoor.getText().toString().equals("") ? 0 :
                    Integer.valueOf(step3.txtOutdoor.getText().toString()));
            modelDetail.setInDType(modelDetail.getInDType());
            modelDetail.setOutDType(modelDetail.getOutDType());
            modelDetail.setMapCode(modelDetail.getMapCode());
            modelDetail.setLatlng(modelDetail.getLatlng());
            modelDetail.setLocalType(step2.localTypeId);
            modelDetail.setLocalTypeName(step2.tvLocalType != null ? step2.tvLocalType.getText().toString() : "");
            modelDetail.setModem(modelDetail.getModem());
            modelDetail.setODCCableType(modelDetail.getODCCableType());
            modelDetail.setPromotionID(step2.selectedInternetPromotion != null ?
                    step2.selectedInternetPromotion.getPromotionID() : 0);
            modelDetail.setPromotionName(step2.selectedInternetPromotion != null ?
                    step2.selectedInternetPromotion.getPromotionName() : "");
            modelDetail.setStatusDeposit(modelDetail.getStatusDeposit());
            modelDetail.setSupporter(modelDetail.getID() == 0 ? Constants.USERNAME : modelDetail.getSupporter());
            modelDetail.setTDName(modelDetail.getTDName());
            modelDetail.setIPTVPackage(step2.spIPTVPackage.getAdapter() != null && step2.spIPTVPackage.getSelectedItem() != null ?
                    ((KeyValuePairModel) step2.spIPTVPackage.getSelectedItem()).getHint() : "");
            modelDetail.setIPTVPLCCount(Integer.parseInt(step2.lblIPTVPLCCount.getText().toString()));
            modelDetail.setIPTVBoxCount(Integer.parseInt(step2.txtIPTVBoxCount.getText().toString()));
            modelDetail.setIPTVRequestDrillWall(step2.radIPTVDrillWallYes.isChecked() ? 1 : 0);
            modelDetail.setIPTVReturnSTBCount(Integer.parseInt(step2.lblIPTVReturnSTBCount.getText().toString()));
            if (step2.serviceType == 0) {
                modelDetail.setIPTVStatus(0);
            } else {
                modelDetail.setIPTVStatus(step2.spIPTVFormDeployment.getSelectedItem() != null ?
                        ((KeyValuePairModel) step2.spIPTVFormDeployment.getSelectedItem()).getID() : 0);
            }
            modelDetail.setIPTVUseCombo(step2.spIPTVCombo.getSelectedItem() != null ?
                    ((KeyValuePairModel) step2.spIPTVCombo.getSelectedItem()).getID() : 0);
            modelDetail.setIPTVRequestSetUp(step2.radIpTvSetUpYes.isChecked() ? 1 : 0);
            modelDetail.setIPTVPromotionID(step2.promotionIPTVModelBox1 != null ?
                    step2.promotionIPTVModelBox1.getPromotionID() : 0);
            modelDetail.setIPTVChargeTimes(Integer.parseInt(step2.lblIPTVChargeTimesCount.getText().toString()));
            modelDetail.setIPTVHBOChargeTimes(Integer.parseInt(step2.lblIPTVHBOChargeTimesCount.getText().toString()));
            modelDetail.setIPTVHBOPrepaidMonth(Integer.parseInt(step2.lblIPTVHBOPrepaidMonthCount.getText().toString()));
            modelDetail.setIPTVKPlusChargeTimes(Integer.parseInt(step2.lblIPTVKPlusChargeTimesCount.getText().toString()));
            modelDetail.setIPTVKPlusPrepaidMonth(Integer.parseInt(step2.lblIPTVKPlusPrepaidMonthCount.getText().toString()));
            modelDetail.setIPTVVTCChargeTimes(Integer.parseInt(step2.lblIPTVVTCChargeTimesCount.getText().toString()));
            modelDetail.setIPTVVTCPrepaidMonth(Integer.parseInt(step2.lblIPTVVTCPrepaidMonthCount.getText().toString()));
            modelDetail.setIPTVVTVChargeTimes(Integer.parseInt(step2.lblIPTVVTVChargeTimesCount.getText().toString()));
            modelDetail.setIPTVVTVPrepaidMonth(Integer.parseInt(step2.lblIPTVVTVPrepaidMonthCount.getText().toString()));
            modelDetail.setFoxy2ChargeTimes(Integer.parseInt(step2.tvFoxy2ChargeTime.getText().toString()));
            modelDetail.setFoxy2PrepaidMonth(Integer.parseInt(step2.tvFoxy2PrepaidMonth.getText().toString()));
            modelDetail.setFoxy4ChargeTimes(Integer.parseInt(step2.tvFoxy4ChargeTime.getText().toString()));
            modelDetail.setFoxy4PrepaidMonth(Integer.parseInt(step2.tvFoxy4PrepaidMonth.getText().toString()));
            modelDetail.setIPTVPromotionType(step2.promotionIPTVModelBox1 != null ?
                    step2.promotionIPTVModelBox1.getType() : -1);
            modelDetail.setIPTVPromotionDesc(step2.promotionIPTVModelBox1 != null ?
                    step2.promotionIPTVModelBox1.getPromotionName() : "");
            modelDetail.setIPTVPromotionTypeBoxOrder(step2.promotionIPTVModelBox2 != null ?
                    step2.promotionIPTVModelBox2.getType() : -1);
            modelDetail.setIPTVPromotionBoxOrderDesc(step2.promotionIPTVModelBox2 != null ?
                    step2.promotionIPTVModelBox2.getPromotionName() : "");
            modelDetail.setPayment(step3.spPaymentType.getAdapter() != null &&
                    step3.spPaymentType.getSelectedItem() != null ?
                    ((KeyValuePairModel) step3.spPaymentType.getSelectedItem()).getID() : 0);
            modelDetail.setPromotionComboID(0);
            modelDetail.setIPTVFimPlusStdChargeTimes(Integer.parseInt(step2.lblIPTVFimStandard_ChargeTimes.getText().toString()));
            modelDetail.setIPTVFimPlusStdPrepaidMonth(Integer.parseInt(step2.lblIPTVFimStandard_MonthCount.getText().toString()));
            modelDetail.setIPTVFimPlusChargeTimes(Integer.parseInt(step2.lblIPTVFimPlus_ChargeTime.getText().toString()));
            modelDetail.setIPTVFimPlusPrepaidMonth(Integer.parseInt(step2.lblIPTVFimPlus_MonthCount.getText().toString()));
            modelDetail.setIPTVFimHotChargeTimes(Integer.parseInt(step2.lblIPTVFimHot_ChargeTime.getText().toString()));
            modelDetail.setIPTVFimHotPrepaidMonth(Integer.parseInt(step2.lblIPTVFimHot_MonthCount.getText().toString()));
            modelDetail.setIPTVPromotionIDBoxOther(step2.promotionIPTVModelBox2 != null ?
                    step2.promotionIPTVModelBox2.getPromotionID() : 0);
            modelDetail.setIPTVPrepaidTotal(step3.mIpTvTotal != null ? step3.mIpTvTotal.getPrepaidTotal() : 0);
            modelDetail.setIPTVDeviceTotal(step3.mIpTvTotal != null ? step3.mIpTvTotal.getDeviceTotal() : 0);
            modelDetail.setIPTVTotal(step3.mIpTvTotal != null ? step3.mIpTvTotal.getTotal() : 0);
            modelDetail.setEmailAdmin(modelDetail.getEmailAdmin());
            modelDetail.setDomainName(modelDetail.getDomainName());
            modelDetail.setTechName(modelDetail.getTechName());
            modelDetail.setTechEmail(modelDetail.getTechEmail());
            modelDetail.setTechPhoneNumber(modelDetail.getTechPhoneNumber());
            modelDetail.setListPackage(getListPackage());
            modelDetail.setListDevice(step2.getListDeviceSelect());
            String total = step3.lblDeviceTotal.getText().toString().replace(".", "");
            modelDetail.setDeviceTotal(Double.parseDouble(total));
            if (step3.is_have_fpt_box) {
                int boxCount = 0;
                for (FPTBox box : step2.getListFptBoxSelect()) {
                    boxCount += box.getOTTCount();
                }
                modelDetail.setOTTBoxCount(boxCount);
                String totalOtt = step3.lblOttTotal.getText().toString().replace(".", "");
                modelDetail.setOTTTotal(Integer.parseInt(totalOtt));
            }

            if (step3.is_have_camera) {
                modelDetail.setObjectCameraOfNet(step2.modelDetail.getObjectCameraOfNet() == null ?
                        new ObjectCameraOfNet(new ArrayList<>(), new ArrayList<>(), new SetupDetail(), new PromoCloud(), 0) : step2.modelDetail.getObjectCameraOfNet()
                );
            } else {
                modelDetail.setObjectCameraOfNet(new ObjectCameraOfNet(new ArrayList<>(), new ArrayList<>(), new SetupDetail(), new PromoCloud(), 0));
            }

            if (step3.is_have_maxy) {
                modelDetail.setObjectMaxy(step2.modelDetail.getObjectMaxy() == null ?
                        new ObjectMaxy(new MaxyGeneral(), new ArrayList<>(), new ArrayList<>()) : step2.modelDetail.getObjectMaxy());
            } else {
                modelDetail.setObjectMaxy(new ObjectMaxy(new MaxyGeneral(), new ArrayList<>(), new ArrayList<>()));
            }
            modelDetail.setListDeviceOTT(step2.getListFptBoxSelect());
            modelDetail.setListMACOTT(step2.getMacList());
            modelDetail.setVoucherCode(step3.mVoucher != null ? step3.mVoucher.getVoucherCode() : "");
            modelDetail.setVoucherCodeDesc(step3.mVoucher != null ? step3.mVoucher.getDescription() : "");
            modelDetail.setReferralCode(step3.edtReferralCode != null ? step3.edtReferralCode.getText().toString().trim() : "");
            modelDetail.setCategoryServiceList(step2.mListServiceSelected);
            String totalVoucher = step3.tvVoucherTotal.getText().toString()
                    .replace(".", "")
                    .replace("-", "");
            modelDetail.setDiscountRP_VoucherTotal(Integer.parseInt(totalVoucher));
            modelDetail.setServiceCodeInternetTotal(step3.mInternetTotal != null ? step3.mInternetTotal : new InternetTotal());
            modelDetail.setServiceCodeIpTvTotal(step3.mIpTvTotal != null ? step3.mIpTvTotal : new IpTvTotal());
            modelDetail.setInternetTotal(Integer.parseInt(step3.lblInternetTotal.getText().toString().replace(".", "")));
            modelDetail.setServiceCodeCameraTotal(step3.mCamera2Total != null ? step3.mCamera2Total : new Camera2Total());
            modelDetail.setTotal(nf.parse(step3.lblTotal.getText().toString()).intValue());
            modelDetail.setServiceCodeMaxyTotal(step3.mMaxyTotal != null ? step3.mMaxyTotal : new MaxyTotal());
            if (!isUpdate) {
                return;
            }

            modelDetail.setSourceType(step1.spSourceType.getAdapter() != null &&
                    step1.spSourceType.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spSourceType.getSelectedItem()).getID() : 0);

            modelDetail.setGiftID(step3.giftId);
            modelDetail.setGiftName(step3.giftName);

            if (step3.btnApplyVoucher.isEnabled()) {//xóa thông tin voucher khi nhấn cập nhật PDK mà ko chọn áp dụng
                modelDetail.setVoucherCode("");
                modelDetail.setVoucherCodeDesc("");
                modelDetail.setReferralCode("");
                modelDetail.setReferralCodeDesc("");
            }

            // update string danh sách hồ sơ
            String listID = step1.strListImageDocumentInfo;
            if (listID.contains("(null)") || listID.contains("/")) {
                if (step1.listImageDocument.size() > 1) {
                    confirmUpdateImageDocumentInfo(modelDetail, listID);
                } else {
                    modelDetail.setImageInfo(modelDetail.getImageInfo());
                    checkHouseNumber(modelDetail);
                }
            } else {
                modelDetail.setImageInfo(listID);
                checkHouseNumber(modelDetail);
            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }

    private List<PackageModel> getListPackage() {
        List<PackageModel> result = new ArrayList<>();
        try {
            for (int i = 0; i < step2.mDicPackage.size(); i++) {
                PackageModel obj = (PackageModel) step2.mDicPackage.values().toArray()[i];
                if (obj != null) {
                    if (obj.getSeat() > 0)
                        result.add(obj);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // cập nhật pdk mới vào hệ thống
    private void callCheckRegister(RegistrationDetailModel register) {
        if (register != null) {
            new CheckRegistration2(this, register);
        }
    }

    private void showHouseNumberError(String message, final RegistrationDetailModel register) {
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(mContext);
        builder.setTitle(
                mContext.getResources().getString(R.string.msg_confirm_update))
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callCheckRegister(register);
                    }
                })
                .setNegativeButton("Không",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        dialog = builder.create();
        dialog.show();
    }

    public void checkRegister(List<UpdResultModel> lstCheckHouseNumber, RegistrationDetailModel register) {
        if (lstCheckHouseNumber != null && lstCheckHouseNumber.size() > 0) {
            UpdResultModel item = lstCheckHouseNumber.get(0);
            if (item.getResultID() <= 0 && !item.getResult().trim().equals("")) {
                if (item.getIgnoreAction() <= 0) {
                    setPageViewpager(0);
                    step1.txtHouseNum.requestFocus();
                    step1.txtHouseNum.setError(item.getResult());
                } else {
                    showHouseNumberError(item.getResult(), register);
                }
            } else {
                register.setBillTo_Number(item.getResult());
                callCheckRegister(register);
            }
        } else {
            callCheckRegister(register);
        }
    }

    private void checkHouseNumber(RegistrationDetailModel register) {
        String LocationParent = ((MyApp) this.getApplication()).getUser().getLocationParent();
        if (register != null) {
            if (register.getTypeHouse() != 2) {// Loai nha khac chung cu
                if (!LocationParent.trim().equals(""))
                    new CheckHouseNumber(this, LocationParent, register.getBillTo_Number(), register);
                else
                    callCheckRegister(register);
            } else
                callCheckRegister(register);
        }
    }

    public String GetAddress() {
        String result = "";
        String sStreetSelected = "";
        String sApartmentSelected = "";
        int sHousePositionSelected = 0;
        String sCityNameValue = "";
        String sDistrictSelected = "";
        int sHouseType = -1;
        try {
            sDistrictSelected = step1.spDistricts.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spDistricts.getSelectedItem()).getDescription() : "";
            sStreetSelected = (step1.spStreets.getAdapter() != null &&
                    step1.spStreets.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spStreets.getSelectedItem()).getDescription() : "");
            sApartmentSelected = (step1.spApartment.getAdapter() != null &&
                    step1.spApartment.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spApartment.getSelectedItem()).getDescription() : "");
            sHouseType = (step1.spHouseTypes.getAdapter() != null &&
                    step1.spHouseTypes.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spHouseTypes.getSelectedItem()).getID() : -1);
            if (Constants.LST_REGION.size() > 0) {
                sCityNameValue = Constants.LST_REGION.get(0).getDescription();
            }
            if (step1.housePositionPanel.getVisibility() == View.VISIBLE)
                sHousePositionSelected = (step1.spHousePosition.getAdapter() != null &&
                        step1.spHousePosition.getSelectedItem() != null ?
                        ((KeyValuePairModel) step1.spHousePosition.getSelectedItem()).getID() : -1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sHouseType == 1) /*nhà Phố*/ {
            // diachi = số nhà + tổ ấp + , Phường + , Quận + , Tỉnh
            if (!Common.isEmpty(step1.txtHouseNum))
                result += step1.txtHouseNum.getText();
            if (sStreetSelected != null && !sStreetSelected.isEmpty())
                result += " " + sStreetSelected;

        } else if (sHouseType == 2) /*Chung Cư*/ {
            // dia chi = lo + tang + phòng + tên chung cư
            if (!Common.isEmpty(step1.txtLot))
                result += "Lo " + step1.txtLot.getText() + ", ";

            if (!Common.isEmpty(step1.txtFloor))
                result += "T. " + step1.txtFloor.getText() + ", ";

            if (!Common.isEmpty(step1.txtRoom))
                result += "P. " + step1.txtRoom.getText() + " ";

            if (sApartmentSelected != null && !sApartmentSelected.isEmpty())
                result += sApartmentSelected + " ";

            if (sStreetSelected != null && !sStreetSelected.isEmpty())
                result += sStreetSelected;

        } else if (sHouseType == 3) /*Nhà không địa chỉ*/ {
            if (!Common.isEmpty(step1.txtHouseNum))
                result += step1.txtHouseNum.getText() + " ";

            if (sStreetSelected != null && !sStreetSelected.isEmpty())
                result += sStreetSelected;

            if (sHousePositionSelected > 0) {
                try {
                    String position = step1.spHousePosition.getSelectedItem() != null ?
                            ((KeyValuePairModel) step1.spHousePosition.getSelectedItem()).getDescription() : "";
                    result += "(" + Common.convertVietNamToEnglishChar(position) + ")";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        String sWardSelected = step1.spWards.getSelectedItem() != null ?
                ((KeyValuePairModel) step1.spWards.getSelectedItem()).getDescription() : "";
        if (sWardSelected != null && !sWardSelected.isEmpty())
            result += ", " + sWardSelected;
        if (sDistrictSelected != null && !sDistrictSelected.equals("-1"))
            result += ", " + sDistrictSelected + ", " + sCityNameValue;

        return result;
    }

    public Boolean checkForUpdateRegister(int sHouseType) {
        int cusDetail = 0;
        if (step1.spCusDetail.getAdapter().getCount() > 0)
            cusDetail = step1.spCusDetail.getSelectedItem() != null ?
                    ((KeyValuePairModel) step1.spCusDetail.getSelectedItem()).getID() : 0;

        //Hình thức bán
        int sourceType = step1.spSourceType.getAdapter() != null &&
                step1.spSourceType.getSelectedItem() != null ?
                ((KeyValuePairModel) step1.spSourceType.getSelectedItem()).getID() : 0;
        if(sourceType == 0){
            setPageViewpager(0);
            Common.alertDialog("Vui lòng chọn nguồn bán", this);
            return false;
        }

        // FULL NAME
        if (Common.isEmpty(step1.txtCustomerName)) {
            setPageViewpager(0);
            Common.alertDialog("Vui lòng nhập Khách Hàng", this);
            step1.txtCustomerName.requestFocus();
            return false;
        }
        // ID & TAXID
        // kiểm tra nếu chọn  loại khách hàng là công ty thì cần nhập mã số thuế
        if (cusDetail == 10) {
            if (Common.isEmpty(step1.txtTaxNum)) {
                setPageViewpager(0);
                Common.alertDialog(getString(R.string.txt_message_enter_tax_id), this);
                step1.txtTaxNum.requestFocus();
                return false;
            }
        } else {
            String id = step1.txtIdentityCard.getText().toString().trim();
            if (id.equals("") || id.length() < 8) {
                setPageViewpager(0);
                Common.alertDialog("Chưa nhập CMND", this);
                step1.txtIdentityCard.requestFocus();
                return false;
            }

            if (step1.txtAddressId.getText().toString().trim().equals("")) {
                setPageViewpager(0);
                Common.alertDialog("Chưa nhập địa chỉ trên CMND", this);
                step1.txtAddressId.requestFocus();
                return false;
            }
        }
        // CHECK NGAY SINH
        String birthDay = step1.txtBirthDay.getText().toString();
        if (birthDay.trim().equals("")) {
            setPageViewpager(0);
            Common.alertDialog("Chưa nhập ngày sinh khách hàng", this);
            return false;
        }
        if (!Common.checkDateIsValid(mContext, birthDay)) {
            Common.alertDialog(getString(R.string.lbl_dob_id_card_not_valid_message), this);
            return false;
        }

        // CHECK EMAIL
        String email = step1.txtEmail.getText().toString().trim();
        // Cho phep de trong nhung se check neu co du lieu
        if (!email.equals("")) {
            if (!Common.checkMailValid(email)) {
                setPageViewpager(0);
                Common.alertDialog(getString(R.string.txt_message_invalid_email), this);
                Common.smoothScrollView(step1.scrollMain, step1.TaxNumLayout.getBottom());
                return false;
            }
        }
        // CHECK PHONE NUMBER 1
        if (!Common.checkPhoneValid(step1.txtPhone1.getText().toString())) {
            setPageViewpager(0);
            Common.alertDialog("Bạn chưa nhập số điện thoại hoặc số điện thoại không đúng", this);
            Common.smoothScrollView(step1.scrollMain, step1.SDT1SpinnerLayout.getBottom());
            return false;
        }
        if (Common.isEmpty(step1.txtContactPhone1)) {
            setPageViewpager(0);
            Common.alertDialog("Vui lòng nhập tên người liên hệ", this);
            Common.smoothScrollView(step1.scrollMain, step1.SDT1SpinnerLayout.getBottom());
            return false;
        }
        // kiểm tra thông tin ĐỊA CHỈ
        if (!checkAddress(sHouseType))
            return false;

        // CHECK NOT UPDATE IPTV TOTAL WHEN
        // REGISTER IPTV
        serviceType = step2.serviceType;
        if (serviceType != 3) { //serviceType != 3
            // neu là office only thì không cần kiểm tra
            // internet và IPTV
            if (serviceType > 0 && step2.frmIpTv.getVisibility() == View.VISIBLE) {
                // CHECK IPTV PACKAGE IS NOT SELECT
                if (step2.spIPTVPackage.getSelectedItemPosition() <= 0) {
                    setPageViewpager(1);
                    step2.spIPTVPackage.requestFocus();
                    Common.alertDialog("Chưa chọn gói dịch vụ IPTV.", this);
                    return false;
                }
                // Kiểm tra chưa chọn CLKM IPTV
                if (step2.promotionIPTVModelBox1 == null) {
                    Common.alertDialog("Chưa chọn CLKM IPTV Box 1.", this);
                    setPageViewpager(1);
                    return false;
                }
                if (Integer.parseInt(step2.txtIPTVBoxCount.getText().toString()) != 1) {
                    if (step2.promotionIPTVModelBox2 == null) {
                        Common.alertDialog("Chưa chọn CLKM IPTV Box 2.", this);
                        setPageViewpager(1);
                        return false;
                    }
                }
            }
            // Đăng ký Internet nhưng không có CLKM
            if (!step2.is_have_fpt_box) {// Đăng ký Internet
                int promotionID = 0;
                if (step2.selectedInternetPromotion != null) {
                    promotionID = step2.selectedInternetPromotion.getPromotionID();
                }
                if (promotionID <= 0) {
                    setPageViewpager(2);
                    step2.txtPromotion.requestFocus();
                    Common.alertDialog("Không tìm thấy CLKM phù hợp", this);
                    setPageViewpager(1);
                    return false;
                }

            }
            // Đăng ký Thiết bị nhưng không chọn thiết bị
            if (step2.is_have_device && step2.getListDeviceSelect().size() == 0) {
                setPageViewpager(1);
                Common.alertDialog("Chưa chọn thiết bị", mContext);
                setPageViewpager(1);
                return false;
            }

            // Đăng ký IPTV + Internet nhưng chưa
            // chọn Combo/ko combo
            if (!TextUtils.isEmpty(step2.lblIPTVPromotionBoxFirstAmount.getText().toString())) {
                int combo;
                combo = ((KeyValuePairModel) step2.spIPTVCombo.getSelectedItem()).getID();
                if (combo == 0) {
                    setPageViewpager(1);
                    Common.alertDialog("Chưa chọn Combo", this);
                    return false;
                }
            }

            if (step2.frmMaxyTV.getVisibility() == View.VISIBLE) {
                if (step2.mMaxyGeneral == null) {
                    Common.alertDialog("Vui lòng chọn gói dịch vụ truyền hình ", this);
                    return false;
                }
                else {
                    if(step2.mMaxyGeneral.getPackageID() != 0 && step2.mMaxyGeneral.getPromotionID() == 0){
                        Common.alertDialog("Vui lòng chọn CLKM cho gói dịch vụ", this);
                        setPageViewpager(1);
                        return false;
                    }
                }

                if(step2.mListMaxyDevice.size() > 0){
                    for(MaxyBox item : step2.mListMaxyDevice){
                        if(TextUtils.isEmpty(item.getPromotionDesc())){
                            Common.alertDialog("Vui lòng chọn CLKM thiết bị", this);
                            setPageViewpager(1);
                            return false;
                        }
                    }
                }
            }

            if (step2.frmCamera.getVisibility() == View.VISIBLE) {
                if (step2.mListCamera.size() == 0 && step2.mListCloud.size() == 0) {
                    Common.alertDialog("Vui lòng chọn gói Camera và Cloud", this);
                    return false;
                } else if (step2.mListCamera.size() != 0 && step2.mListCloud.size() == 0) {
                    Common.alertDialog("Không tìm thấy dịch vụ Cloud", this);
                    return false;
                } else {
                    if (step2.mProCloud == null) {
                        Common.alertDialog("Vui lòng chọn gói khuyến mãi Cloud", this);
                        return false;
                    }
                    if (step2.mListCamera.size() != 0) {
                        for (CameraDetail item : step2.mListCamera) {
                            if (TextUtils.isEmpty(item.getPackName())) {
                                Common.alertDialog(getString(R.string.msg_select_camera_package), this);
                                return false;
                            }
                        }
                    }

                    if (step2.mListCloud.size() != 0) {
                        for (CloudDetail item : step2.mListCloud) {
                            if (TextUtils.isEmpty(item.getPackName())) {
                                Common.alertDialog(getString(R.string.msg_select_cloud_package), this);
                                return false;
                            }
                        }
                    }
                }
            }
        }

        if (!checkUpdateFPTBox())
            return false;
        // ========================== Hinh thuc thanh toan
        int paymentType = step3.spPaymentType.getAdapter() != null &&
                step3.spPaymentType.getSelectedItem() != null ?
                ((KeyValuePairModel) step3.spPaymentType.getSelectedItem()).getID() : 0;
        if (paymentType <= 0) {
            setPageViewpager(2);
            Common.alertDialog("Chưa chọn hình thức thanh toán (Phía dưới tổng tiền)", this);
            Common.smoothScrollView(step3.scrollMain, step3.totalPriceLayout.getBottom());
            return false;
        }

        return true;
    }


    private boolean checkUpdateFPTBox() {
        // đăng ký FPT Box chưa chọn Box
        if (step2.is_have_fpt_box && step2.getListFptBoxSelect().size() == 0) {
            setPageViewpager(1);
            Common.alertDialog("Chưa chọn FPT Box", mContext);
            Common.smoothScrollView(step2.frmMain, step2.frmContentFptBox.getBottom());
            return false;
        }

        //chưa chọn CLKM box //added by: haulc3
        if (step2.is_have_fpt_box && step2.getListFptBoxSelect().size() != 0) {
            for (FPTBox item : step2.getListFptBoxSelect()) {
                if (item.getPromotionID() == -1) {
                    setPageViewpager(1);
                    Common.alertDialog("Chưa chọn CLKM Box", mContext);
                    Common.smoothScrollView(step2.frmMain, step2.frmContentFptBox.getBottom());
                    return false;
                }
            }
        }

        //chưa chọn MAC //added by: haulc3
        if (step2.is_have_fpt_box && step2.getMacList().size() == 0) {
            setPageViewpager(1);
            Common.alertDialog("Chưa chọn MAC", mContext);
            Common.smoothScrollView(step2.frmMain, step2.frmContentFptBox.getBottom());
            return false;
        }

        int boxCount = 0;
        for (FPTBox item : step2.getListFptBoxSelect()) {
            boxCount += item.getOTTCount();
        }
        if (step2.is_have_fpt_box && step2.getMacList().size() != boxCount) {
            setPageViewpager(1);
            Common.alertDialog("Số lượng Box và Mac không hợp lệ", mContext);
            Common.smoothScrollView(step2.frmMain, step2.frmContentFptBox.getBottom());
            return false;
        }
        return true;
    }

    public Boolean checkAddress(int sHouseType) {
        Log.e("sHouseType", sHouseType + "");
        String sWardSelected = null, sStreetSelected = null, sApartmentSelected = null, sDistrictSelected = null;
        try {
            if (step1.spWards.getAdapter() != null) {
                if (step1.spWards.getSelectedItem() != null) {
                    sWardSelected = step1.spWards.getSelectedItem() != null ?
                            ((KeyValuePairModel) step1.spWards.getSelectedItem()).getsID() : "";
                }
            }
            if (step1.spStreets.getAdapter() != null)
                if (step1.spStreets.getSelectedItem() != null)
                    sStreetSelected = ((KeyValuePairModel) step1.spStreets.getSelectedItem()).getsID();
            if (step1.spApartment.getAdapter() != null)
                if (step1.spApartment.getSelectedItem() != null)
                    sApartmentSelected = ((KeyValuePairModel) step1.spApartment.getSelectedItem()).getsID();
            if (step1.spDistricts.getAdapter() != null)
                if (step1.spDistricts.getSelectedItem() != null)
                    sDistrictSelected = ((KeyValuePairModel) step1.spDistricts.getSelectedItem()).getsID();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sDistrictSelected == null || sDistrictSelected.equals("-1")) {
            setPageViewpager(0);
            Common.alertDialog("Vui lòng chọn Quận/Huyện", this);
            Common.smoothScrollView(step1.scrollMain, step1.AddressLayoutLast.getBottom());
            step1.spDistricts.requestFocus();
            return false;
        }

        if (sWardSelected == null || sWardSelected.isEmpty()) {
            setPageViewpager(0);
            Common.alertDialog("Vui lòng chọn Phường/Xã", this);
            Common.smoothScrollView(step1.scrollMain, step1.AddressLayoutLast.getBottom());
            step1.spWards.requestFocus();
            return false;
        }

        if ((sHouseType == 1 || sHouseType == 3)
                && (sStreetSelected == null || sStreetSelected.isEmpty())) {
            setPageViewpager(0);
            Common.alertDialog("Vui lòng chọn tên Đường", this);
            Common.smoothScrollView(step1.scrollMain, step1.AddressLayoutLast.getBottom());
            step1.spStreets.requestFocus();
            return false;
        }

        if (sHouseType == 1) {
            if (Common.isEmpty(step1.txtHouseNum)) {
                setPageViewpager(0);
                Common.alertDialog("Vui lòng nhập vào số nhà", this);
                Common.smoothScrollView(step1.scrollMain, step1.AddressLayoutLast.getTop() + step1.txtHouseNum.getBottom());
                step1.txtHouseNum.requestFocus();
                return false;
            }
        } else if (sHouseType == 2) {//chung cư
            Log.e("txtRoom", step1.txtRoom.getText().toString());
            if (sApartmentSelected == null || sApartmentSelected.isEmpty()) {
                setPageViewpager(0);
                Common.alertDialog("Vui lòng chọn chung cư", this);
                Common.smoothScrollView(step1.scrollMain, step1.AddressLayoutLast.getTop() + step1.spApartment.getBottom());
                step1.spApartment.requestFocus();
                return false;
            } else if (TextUtils.isEmpty(step1.txtFloor.getText())) {
                setPageViewpager(0);
                Common.alertDialog("Vui lòng nhập Tầng", this);
                Common.smoothScrollView(step1.scrollMain, step1.AddressLayoutLast.getTop() + step1.txtFloor.getBottom());
                step1.txtFloor.requestFocus();
                return false;
            } else if (TextUtils.isEmpty(step1.txtRoom.getText())) {
                setPageViewpager(0);
                Common.alertDialog("Vui lòng nhập Phòng", this);
                Common.smoothScrollView(step1.scrollMain, step1.AddressLayoutLast.getTop() + step1.txtRoom.getBottom());
                step1.txtRoom.requestFocus();
                return false;
            }
        }
        return true;
    }

    @Override
    public void changePage(int pos) {
        setPageViewpager(pos);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int REQUEST_CAMERA = 0;
        if (data != null) {
            if (requestCode == 1) {
                if (data.hasExtra(TAG_PROMOTION_TYPE_BOX)) {
                    PromotionIPTVModel item = data.getParcelableExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_ITEM);
                    int typeIPTVBox = data.getIntExtra(TAG_PROMOTION_TYPE_BOX, 0);
                    if (typeIPTVBox == 1) {
                        if (item != null) {
                            step2.promotionIPTVModelBox1 = item;
                            step2.txtPromotionBox1.setText(item.getPromotionName() == null ? "" : item.getPromotionName());
                            step2.lblIPTVPromotionBoxFirstAmount.setText(
                                    String.format(getString(R.string.txt_unit_price), Common.formatNumber(item.getAmount()))
                            );
                            FragmentRegisterStep3v2.isBind = 1;
                            //clear thông tin RP & E-Voucher
                            step2.is_clear_rp_and_voucher = true;
                        }
                    }
                    if (typeIPTVBox == 2) {
                        if (item != null) {
                            step2.promotionIPTVModelBox2 = item;
                            step2.txtPromotionBox2.setText(item.getPromotionName() == null ? "" : item.getPromotionName());
                            step2.lblIPTVPromotionBoxOrderAmount.setText(
                                    String.format(getString(R.string.txt_unit_price), Common.formatNumber(item.getAmount()))
                            );
                            FragmentRegisterStep3v2.isBind = 1;
                            //clear thông tin RP & E-Voucher
                            step2.is_clear_rp_and_voucher = true;
                        }
                    }

                } else {
                    PromotionModel item = data.getParcelableExtra(PromotionFilterActivity.TAG_PROMOTION_ITEM);
                    if (item != null && step2 != null) {
                        step2.selectedInternetPromotion = item;
                        step2.txtPromotion.setText(item.getPromotionName() == null ? "" : item.getPromotionName());
                        step2.lblInternetTotal = Common.formatNumber(item.getRealPrepaid());
                        FragmentRegisterStep3v2.isBind = 1;
                        //clear thông tin RP & E-Voucher
                        step2.is_clear_rp_and_voucher = true;
                    }
                }
            } else if (requestCode == 2) {
                // nhận kết quả đường dẫn các ảnh upload thành công
                if (data.hasExtra(TAG_LIST_IMAGE_DOCUMENT)) {
                    ArrayList<ImageDocument> listImageDocument = data.getParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT);
                    step1.setListImageDocument(listImageDocument);
                }
                // nhận thông tin quét cmnd
                if (data.hasExtra(Constants.TAG_INFO_IDENTITY_CARD)) {
                    IdentityCard identityCard = data.getParcelableExtra(Constants.TAG_INFO_IDENTITY_CARD);
                    String pathFileCapture = data.getStringExtra(Constants.TAG_LOCAL_PATH_IDENTITY_CARD);
                    step1.setPathImageDocument(pathFileCapture);
                    step1.autoFieldDataIdentityCard(identityCard);
                }
                // kết quả trả về sau khi chọn ảnh cmnd tại màn hình bước 1
            } else if (requestCode == 4) {
                Uri selectedImageUri = data.getData();
                String selectedImagePath = Common.getRealPathUrlImage(mContext, selectedImageUri);
                if (selectedImagePath == null || selectedImagePath.equals("")) {
                    Common.alertDialog(getResources().getString(R.string.lbl_error_image_document_select_message), mContext);
                } else {
                    Intent intent = new Intent(mContext, IdentityCardScanActivity.class);
                    intent.putExtra(Constants.TAG_LOCAL_PATH_IDENTITY_CARD, selectedImagePath);
                    startActivityForResult(intent, 2);
                }
            } else {
                //requestCode == 65539
                // thêm 1 địa chỉ mac vào màn hình bước 3
                if (data.hasExtra("result") && step2 != null) {
                    String result = data.getStringExtra("result");
                    step2.addMacAddressToList(result);
                }
            }
        } else {
            // file ảnh trả về sau khi chụp ảnh cmnd ở bước 1
            if (requestCode == REQUEST_CAMERA) {
                File fileIdentityCardCapture = step1.fileIdentityCardImageDocument;
                if (fileIdentityCardCapture != null && resultCode == -1) {
                    String pathFileCapture = fileIdentityCardCapture.toString();
                    Intent intent = new Intent(mContext, IdentityCardScanActivity.class);
                    intent.putExtra(Constants.TAG_LOCAL_PATH_IDENTITY_CARD, pathFileCapture);
                    startActivityForResult(intent, 2);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        try {
            Dialog dialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.txt_message_exits_create_registration))
                    .setCancelable(false)
                    .setPositiveButton(
                            getResources().getString(R.string.lbl_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                }
                            })
                    .setNegativeButton(
                            getResources().getString(R.string.lbl_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // dialog hiển thị thông báo xác nhận người dùng cập nhật danh sách ảnh mới.
    private void confirmUpdateImageDocumentInfo(final RegistrationDetailModel modelDetail, final String listID) {
        try {
            AlertDialog.Builder builder;
            Dialog dialog;
            builder = new AlertDialog.Builder(this);
            builder.setMessage("Phiếu TTKH có ảnh cũ bị lỗi và sẽ bị xóa . Bạn có muốn tiếp tục không ?")
                    .setCancelable(false)
                    .setPositiveButton(
                            this.getResources().getString(R.string.lbl_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    String desListID = "";
                                    if (listID.contains("(null),")) {
                                        desListID = listID.replace("(null),", "");
                                    }
                                    if (listID.contains("/")) {
                                        desListID = listID.substring(listID.indexOf(",") + 1);
                                    }
                                    modelDetail.setImageInfo(desListID);
                                    checkHouseNumber(modelDetail);
                                }
                            })
                    .setNegativeButton(
                            this.getResources().getString(R.string.lbl_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });

            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // xóa toàn bộ cache ảnh hồ sơ
        Common.clearAllFilesCache(mContext);
        FragmentRegisterStep3v2.isBind = 0;
    }

    public List<CategoryServiceList> getListService() {
        return mListService != null ? mListService : new ArrayList<CategoryServiceList>();
    }

}

class MyTabFactory implements TabContentFactory {
    private final Context mContext;

    MyTabFactory(Context context) {
        mContext = context;
    }

    public View createTabContent(String tag) {
        View v = new View(mContext);
        v.setMinimumWidth(0);
        v.setMinimumHeight(0);
        return v;
    }
}