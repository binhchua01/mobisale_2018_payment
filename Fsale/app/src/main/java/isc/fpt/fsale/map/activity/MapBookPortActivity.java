package isc.fpt.fsale.map.activity;

import java.util.ArrayList;
import java.util.WeakHashMap;

import isc.fpt.fsale.action.GetListBookPortAction;
import isc.fpt.fsale.action.GetLocationBranchPOPAction;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.action.GetGeocoder;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.map.utils.MapConstants;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.SupportMapFragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;


public class MapBookPortActivity extends FragmentActivity implements OnMapReadyCallback {
    private ArrayList<RowBookPortModel> lstBookPort = new ArrayList<>();
    private String NUM_REG, ID;
    private Spinner spTapDiemType;
    private ImageView imgSearchTapDiem;
    private EditText txtTapDiem;

    private GoogleMap mMap;
    private Button btnMapMode, btnShowListTapDiem, btnGetLocation;
    private Context mContext;
    @SuppressWarnings("unused")
    private String[] snippet;
    public final int FTTH_NEW = 2;

    private String strContractLatLng;
    private FragmentManager fm;
    private int iTapDiemType;
    public WeakHashMap<String, RowBookPortModel> haspMap;
    private RowBookPortModel TdSelectedInfo;

    private RegistrationDetailModel modelRegister;
    //Add by: DuHK 21-01-2015
    private boolean isViewTDOnly = false;

    @SuppressWarnings("static-access")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_map_bookport_activity));
        setContentView(R.layout.activity_bookport_manual);
        mContext = this;
        //init control
        btnMapMode = (Button) findViewById(R.id.btn_map_mode);
        btnShowListTapDiem = (Button) findViewById(R.id.btn_show_list_port);
        btnGetLocation = (Button) findViewById(R.id.btn_get_location);
        imgSearchTapDiem = (ImageView) findViewById(R.id.btn_find_list_port);
        txtTapDiem = (EditText) findViewById(R.id.txt_tapdiem);
        spTapDiemType = (Spinner) findViewById(R.id.sp_port_type);
        fm = getSupportFragmentManager();

        //handle button clicked event
        btnMapMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if current map mode is satellite: change to normal
                if (MapConstants.IS_SATELLITE) {
                    btnMapMode.setText(getResources().getString(R.string.lbl_satellite));
                    MapConstants.IS_SATELLITE = false;
                    mMap.setMapType(MAP_TYPE_NORMAL);
                }
                //if current map mode is normal: change to satellite
                else {
                    btnMapMode.setText(getResources().getString(R.string.lbl_map));
                    MapConstants.IS_SATELLITE = true;
                    mMap.setMapType(MAP_TYPE_SATELLITE);
                }
            }
        });

        btnGetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //isChangeLocation = false;
                MapConstants.MAP.clear();
                setUpMapIfNeeded();
                //get extra data
                try {
                    GPSTracker gpsTracker = new GPSTracker(mContext);
                    String title;
                    title = mContext.getString(R.string.title_cuslocation);
                    Location myLocation = gpsTracker.getLocation();
                    LatLng LngLocation = null;
                    if (myLocation != null) {
                        LngLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                        strContractLatLng = LngLocation.latitude + "," + LngLocation.longitude;
                    } else {
                        SharedPreferences setting = MapBookPortActivity.this.getPreferences(Context.MODE_PRIVATE);
                        String cityLatLng = setting.getString(MapBookPortActivity.this.getString(R.string.lbl_Shared_Preferences_city_Lat_Lng), null);
                        if (cityLatLng == null || cityLatLng.equals("")) {
                            GetGeocoder geoCoder = new GetGeocoder(MapBookPortActivity.this, Constants.LOCATION_NAME);
                            geoCoder.execute();
                        } else {
                            LngLocation = MapCommon.ConvertStrToLatLng(strContractLatLng);
                        }
                    }

                    assert LngLocation != null;
                    String latitudeStr = String.valueOf(LngLocation.latitude);
                    if (latitudeStr.length() > 10)
                        latitudeStr = latitudeStr.substring(0, 10);
                    String longitudeStr = String.valueOf(LngLocation.longitude);
                    if (longitudeStr.length() > 10)
                        longitudeStr = longitudeStr.substring(0, 10);
                    title += "(" + latitudeStr + "," + longitudeStr + ")";
                    addCusLocationOnMap(LngLocation, title, R.drawable.cuslocation_red, true, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        // Sự kiện click tìm tap diem
        imgSearchTapDiem.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onClick(View v) {
                // Kiểm tra có tập điểm chưa
                String stdName = txtTapDiem.getText().toString().trim().toLowerCase();
                if (!stdName.equals("")) {
                    int TDKind = 0;
                    try {
                        TDKind = ((KeyValuePairModel) spTapDiemType.getSelectedItem()).getID();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    RowBookPortModel selectedItem;
                    if (TdSelectedInfo != null) {
                        if (TdSelectedInfo.getTDName().trim().toLowerCase().equals(stdName)) {
                            selectedItem = TdSelectedInfo;
                        } else {
                            selectedItem = new RowBookPortModel(0, 0, 0,
                                    0, "", 0, 0, 0,
                                    txtTapDiem.getText().toString().trim(), String.valueOf(TDKind),
                                    TDKind, "", "", "", 0);
                        }
                    } else {
                        selectedItem = new RowBookPortModel(0, 0, 0, 0,
                                "", 0, 0, 0,
                                txtTapDiem.getText().toString().trim(), String.valueOf(TDKind), TDKind,
                                "", "", "", 0);
                    }
                    new GetLocationBranchPOPAction(fm, mContext, selectedItem, NUM_REG, ID, modelRegister);
                } else {
                    txtTapDiem.setError(mContext.getResources().getString(R.string.msg_td_empty));
                }
            }
        });

        // Sự kiện click chọn loại tập điểm
        spTapDiemType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                iTapDiemType = selectedItem.getID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        // Sự kiện click xem các tập điểm
        btnShowListTapDiem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (strContractLatLng != null && !strContractLatLng.equals("") &&
                        !strContractLatLng.replace(",", "").equals("")) {
                    int tdType = ((KeyValuePairModel) spTapDiemType.getSelectedItem()).getID();
                    new GetListBookPortAction(mContext, ID, tdType, strContractLatLng);
                } else {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_unable_get_location), mContext);
                }
            }
        });

        setUpMapIfNeeded();

        //get extra data
        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                try {
                    isViewTDOnly = myIntent.getBooleanExtra("SEARCH_TAP_DIEM", false);
                    modelRegister = myIntent.getParcelableExtra("modelRegister");
                    lstBookPort = myIntent.getParcelableArrayListExtra("list_model_send");
                    this.NUM_REG = myIntent.getStringExtra("Number_Register");
                    this.ID = myIntent.getStringExtra("ID_Register");
                    if (modelRegister != null)
                        setListTapDiemType(modelRegister.getBookPortType());
                    if (myIntent.hasExtra("Latlng"))
                        strContractLatLng = myIntent.getStringExtra("Latlng");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //Chỉ xem tập điểm, ko book port
                if (isViewTDOnly) {
                    imgSearchTapDiem.setVisibility(View.GONE);
                    ArrayList<KeyValuePairModel> lstBookPortType = new ArrayList<>();
                    lstBookPortType.add(new KeyValuePairModel(0, "ADSL"));
                    lstBookPortType.add(new KeyValuePairModel(1, "FTTH"));
                    lstBookPortType.add(new KeyValuePairModel(2, "FTTHNew"));
                    KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                            R.layout.my_spinner_style, lstBookPortType, Gravity.LEFT);
                    spTapDiemType.setAdapter(adapter);
                    //Select lai loai tap diem
                    try {
                        spTapDiemType.setSelection(Common.getIndex(spTapDiemType, iTapDiemType), true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                String cityLatLng = null;
                if (strContractLatLng == null || strContractLatLng.equals("")) {
                    GPSTracker gpsTracker = new GPSTracker(mContext);
                    if (GPSTracker.location != null) {
                        strContractLatLng = gpsTracker.location.getLatitude() + "," + gpsTracker.location.getLongitude();
                    } else {
                        gpsTracker.getLocation();
                        SharedPreferences setting = this.getPreferences(Context.MODE_PRIVATE);
                        cityLatLng = setting.getString(this.getString(R.string.lbl_Shared_Preferences_city_Lat_Lng), null);
                        if (cityLatLng == null || cityLatLng.equals("")) {
                            GetGeocoder geoCoder = new GetGeocoder(MapBookPortActivity.this, Constants.LOCATION_NAME);
                            geoCoder.execute();
                        } else {
                            strContractLatLng = cityLatLng;
                        }
                    }
                }

                LatLng contractLatlng = MapCommon.ConvertStrToLatLng(strContractLatLng);

                //if contract location is null: draw current location
                //if not: draw contract and current location
                String title;
                if (contractLatlng != null) {
                    title = mContext.getString(R.string.title_cuslocation_old);
                    String latitudeStr = String.valueOf(contractLatlng.latitude);
                    if (latitudeStr.length() > 10) {
                        latitudeStr = latitudeStr.substring(0, 10);
                    }
                    String longitudeStr = String.valueOf(contractLatlng.longitude);
                    if (longitudeStr.length() > 10) {
                        longitudeStr = longitudeStr.substring(0, 10);
                    }
                    title += "(" + latitudeStr + "," + longitudeStr + ")";
                    //Neu load tao do của City thi zoom it
                    if (cityLatLng == null) {
                        addCusLocationOnMap(contractLatlng, title, R.drawable.cuslocation_red,
                                true, null);
                    } else {
                        addCusLocationOnMap2(contractLatlng, title, R.drawable.cuslocation_red,
                                true, null, 11);
                    }
                }

                LatLng TDLatlng;
                haspMap = new WeakHashMap<>();

                for (RowBookPortModel model : lstBookPort) {
                    title = model.getTDName();
                    TDLatlng = MapCommon.ConvertStrToLatLng(model.getLatlngPort());
                    if (TDLatlng != null) {
                        addCusLocationOnMap(TDLatlng, title,
                                R.drawable.cuslocation_black, false, model);
                    }
                }

                // Gan ten tap diem
                txtTapDiem.setText(modelRegister.getTDName());
                //Select Spinner loai tap diem theo Ten tap diem da book port truoc do
                try {
                    if (txtTapDiem.getText().toString().indexOf("/") > 0) {
                        spTapDiemType.setSelection(Common.getIndex(spTapDiemType, FTTH_NEW), true);
                    } else {
                        spTapDiemType.setSelection(Common.getIndex(spTapDiemType, iTapDiemType), true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    public void addCusLocationOnMap(LatLng latlng, String title, int dr,
                                    boolean isDraggable, RowBookPortModel model) {
        try {
            Marker marker = MapCommon.addMarkerOnMap(title, latlng, dr, isDraggable);
            try {
                if (model != null) {
                    marker.setSnippet(" Port trống: " + model.getPortFree());
                    haspMap.put(model.getTDName(), model);
                } else {
                    marker.showInfoWindow();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            MapCommon.animeToLocation(latlng);
            MapCommon.setMapZoom(18);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addCusLocationOnMap2(LatLng latlng, String title, int dr, boolean isDraggable,
                                     RowBookPortModel model, int zoomSize) {
        try {
            Marker marker = MapCommon.addMarkerOnMap(title, latlng, dr, isDraggable);
            try {
                if (model != null) {
                    marker.setSnippet(" Port trống: " + model.getPortFree());
                    haspMap.put(model.getTDName(), model);
                } else {
                    marker.showInfoWindow();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            MapCommon.animeToLocation(latlng);
            MapCommon.setMapZoom(zoomSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * @FUNCTION: setUpMapIfNeeded
     * @Description: init map
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            SupportMapFragment mapFrag = (SupportMapFragment)
                    this.getSupportFragmentManager().findFragmentById(R.id.map);
            //test code in onMapReady
            mapFrag.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {
            setUpMap();
        }
    }

    /*
     * @FUNCTION: set up and init map value
     * @Description: init map
     */
    private void setUpMap() {
        mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                String TDName = marker.getTitle();
                TdSelectedInfo = haspMap.get(TDName);
                if (TdSelectedInfo != null) {
                    txtTapDiem.setText(TDName);
                    if (TDName.indexOf("/") > 0) {
                        spTapDiemType.setSelection(Common.getIndex(spTapDiemType, FTTH_NEW), true);
                    } else
                        spTapDiemType.setSelection(Common.getIndex(spTapDiemType, iTapDiemType), true);
                }
                return false;
            }
        });
        mMap.setOnMapLongClickListener(new OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng newLat) {
                strContractLatLng = MapCommon.getGPSCoordinates(newLat);
                mMap.clear();
                String latitudeStr = String.valueOf(newLat.latitude);
                if (latitudeStr.length() > 10) {
                    latitudeStr = latitudeStr.substring(0, 10);
                }

                String longitudeStr = String.valueOf(newLat.longitude);
                if (longitudeStr.length() > 10) {
                    longitudeStr = longitudeStr.substring(0, 10);
                }
                String title = mContext.getString(R.string.title_cuslocation_old) + " (" + latitudeStr + "," + longitudeStr + ")";
                addCusLocationOnMap(newLat, title, R.drawable.cuslocation_red, true, null);
            }
        });

        mMap.setOnMarkerDragListener(new OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                LatLng coordinates = marker.getPosition();
                strContractLatLng = MapCommon.getGPSCoordinates(coordinates);
            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }
        });
        MapConstants.MAP = mMap;
    }

    // Gắn danh sách loại bookport
    public void setListTapDiemType(int type) {
        //Load loai tap diem theo Loai DV da dang ky tren PDK: ADSL-> {ADSL, FTTHNew}, FTTH ->{FTTH, FTTHNew}
        ArrayList<KeyValuePairModel> lstBookPortType = new ArrayList<KeyValuePairModel>();
        if (type == 0) {
            lstBookPortType.add(new KeyValuePairModel(0, "ADSL"));
        } else if (type == 1) {
            lstBookPortType.add(new KeyValuePairModel(1, "FTTH"));
        }
        lstBookPortType.add(new KeyValuePairModel(2, "FTTHNew"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstBookPortType, Gravity.LEFT);
        spTapDiemType.setAdapter(adapter);
        try {
            //Chon lai loai tap diem da chon truoc do
            if (iTapDiemType >= 0) {
                spTapDiemType.setSelection((Common.getIndex(spTapDiemType, iTapDiemType)), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    //Lay toa do cua LocationID khi khong lay duoc toa do
    public void setCityLocation(LatLng cityLocation) {
        if (cityLocation != null) {
            strContractLatLng = cityLocation.latitude + "," + cityLocation.longitude;
            String title = mContext.getString(R.string.title_cuslocation) + "(" + strContractLatLng + ")";
            addCusLocationOnMap2(cityLocation, title, R.drawable.cuslocation_red,
                    true, null, 11);
            SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(this.getString(R.string.lbl_Shared_Preferences_city_Lat_Lng), strContractLatLng);
            editor.apply();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }
}

