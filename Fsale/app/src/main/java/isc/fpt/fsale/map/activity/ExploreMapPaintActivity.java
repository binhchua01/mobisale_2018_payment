package isc.fpt.fsale.map.activity;


import isc.fpt.fsale.activity.BaseActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.MenuBanDo_RightSide;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

/*import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;*/
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.slidingmenu.lib.SlidingMenu;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;


public class ExploreMapPaintActivity extends BaseActivity implements OnTouchListener, OnMapReadyCallback {
	/* private LocationManager locationManager;
	 private String provider;
	 private Button bntImg;*/
	private Context mContext;
	private GoogleMap mMap;
	private LatLng lngTo, lngFrom;

	DrawPanel dp;
	private ArrayList<Path> pointsToDraw = new ArrayList<Path>();
	private Paint mPaint = new Paint();
	Path path;

	public ExploreMapPaintActivity() {
		// TODO Auto-generated constructor stub

		super(R.string.lbl_MapView);
		MyApp.setCurrentActivity(this);
	}


	//private WebView w ;
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_explore_map_paint_activity));
		setContentView(R.layout.explore_map_layout);

		this.mContext = this;

		//test code in onMapReady method
		((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
		 	/*
			//Hien thi bang do 
			mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			mMap.setMyLocationEnabled(true);
			*/


		try {
			Intent myIntent = getIntent();
			if (myIntent != null && myIntent.getExtras() != null) {
				Bundle bundle = myIntent.getExtras();
				if (bundle != null) {
					lngTo = (LatLng) MapCommon.ConvertStrToLatLng(bundle.getString("lngTo"));
					lngFrom = (LatLng) MapCommon.ConvertStrToLatLng(bundle.getString("lngFrom"));
				}
			}

		} catch (Exception e) {

			Log.d("LOG_GET_EXTRA_CONTRACT_INFO", "Error: " + e.getMessage());

		}

		if (lngFrom != null) {
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lngFrom, 15));

			mMap.addMarker(new MarkerOptions().position(lngFrom).title("Tập Điểm"));
		}

		// lay vi tri hien tai
		LatLng hientai = lngTo;

		if (hientai != null) {
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hientai, 18));
			mMap.addMarker(new MarkerOptions().position(hientai).title("Khách Hàng"));
		}


		super.addRight(new MenuBanDo_RightSide(lngTo, lngFrom));


		//Ve Hinh -----------------------------------------------------------
		//Thiết lập màn hình hình hiển thị không có thanh tiêu đề
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		//setContentView(R.layout.activity_main);
		dp = new DrawPanel(mContext);
		dp.setOnTouchListener(this);
		//Thiết lập full màn hình
		//   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		mPaint.setDither(true);
		//Thiết lập màu sắc của nét vẽ
		mPaint.setColor(Color.RED);
		//Thiết lập nét vẽ
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		//Độ rộng của nét vẽ
		mPaint.setStrokeWidth(3);
		//FrameLayout fl = new FrameLayout(this);
		// FrameLayout fl1 = (FrameLayout) findViewById(R.id.screenRoot);
		final FrameLayout fl2 = (FrameLayout) findViewById(R.id.screenRoot);
		fl2.setOnTouchListener(this);

		fl2.addView(dp);
		//-------------------------------------------------------------------------------
	       
	        /*
	    	bntImg=(Button)findViewById(R.id.btn_map_mode);	
	    	
			bntImg.setOnClickListener(new View.OnClickListener() {     
				@Override
				public void onClick(View v) {
					//View content= findViewById(R.id.screenRoot);
					//View content=((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getView();
					//content.setDrawingCacheEnabled(true);
					
					//loadBitmapFromView(mContext,content);
					//Common.alertDialog("chup hinh ban do", mContext);
					 
				  
					
				}
			});
			*/
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == android.R.id.home) {
			toggle(SlidingMenu.LEFT);
			return true;
		} else if (itemId == R.id.action_right) {
			toggle(SlidingMenu.RIGHT);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	public double KhoangCach(LatLng A, LatLng B) {
		double distance = 0;

		Location locationA = new Location("");

		locationA.setLatitude(A.latitude);

		locationA.setLongitude(A.longitude);

		Location locationB = new Location("");

		locationB.setLatitude(B.latitude);

		locationB.setLongitude(B.longitude);

		distance = locationA.distanceTo(locationB) / 1000;

		return distance;
	}
		
		/*
		private void chuphinh()
		{
			Bitmap bitmap ; 
			View v1 =  findViewById(R.id.map);
			v1 . setDrawingCacheEnabled ( true ); 
			v1.buildDrawingCache();
			bitmap =  Bitmap . createBitmap ( v1 . getDrawingCache ()); 
			v1.destroyDrawingCache();
			v1 . setDrawingCacheEnabled ( false );
			File file = new File( Environment.getExternalStorageDirectory() + "/DCIM/testhinhanh.png");
			try{
				file.createNewFile();
			    FileOutputStream ostream = new FileOutputStream(file);
			    bitmap.compress(CompressFormat.JPEG, 100, ostream);
			    ostream.close();
			} 
		    catch (Exception e) 
		    {
		        e.printStackTrace();
		    }
		}
		
		public void loadBitmapFromView(Context context, View v) {
			    DisplayMetrics dm = context.getResources().getDisplayMetrics(); 
			    v.measure(MeasureSpec.makeMeasureSpec(dm.widthPixels, MeasureSpec.EXACTLY),
			            MeasureSpec.makeMeasureSpec(dm.heightPixels, MeasureSpec.EXACTLY));
			    v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
			    Bitmap bitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
			            v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
			    Canvas c = new Canvas(bitmap);
			    v.draw(c);
			
			 File file = new File( Environment.getExternalStorageDirectory() + "/DCIM/testhinhanh.png");
			try{
					file.createNewFile();
				    FileOutputStream ostream = new FileOutputStream(file);
				    bitmap.compress(CompressFormat.JPEG, 100, ostream);
				    ostream.close();
				} 
			    catch (Exception e) 
			    {
			        e.printStackTrace();
			    }
		}
		*/

	//TODO: report activity start
	@Override
	protected void onStart() {
		super.onStart();
		//Get an Analytics tracker to report app starts and uncaught exceptions etc.
		Common.reportActivityStart(this, this);
	}

	//TODO: report activity stop
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}

	//ve hinh

	@Override
	protected void onPause() {
		super.onPause();
		dp.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
		dp.resume();
	}

	@SuppressLint("ClickableViewAccessibility")
	public boolean onTouch(View v, MotionEvent me) {
		synchronized (pointsToDraw) {
			if (me.getAction() == MotionEvent.ACTION_DOWN) {
				path = new Path();
				path.moveTo(me.getX(), me.getY());
				// path.lineTo(me.getX(), me.getY());
				pointsToDraw.add(path);
			} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
				path.lineTo(me.getX(), me.getY());
			} else if (me.getAction() == MotionEvent.ACTION_UP) {
				// path.lineTo(me.getX(), me.getY());
			}
		}
		return true;
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		mMap.setMyLocationEnabled(true);
	}

	//Tạo một lớp con DrawPanel
		    public class DrawPanel extends SurfaceView implements Runnable {
		        Thread t = null;
		        SurfaceHolder holder;
		        boolean isItOk = false;
		 
		        public DrawPanel(Context context) {
		            super(context);
		            holder = getHolder();
		            getHolder().setFormat(PixelFormat.TRANSPARENT); 
		          	
		        }
		 
		        @SuppressLint("WrongCall")
				public void run() {
		            while (isItOk == true) {
		 
		               if (!holder.getSurface().isValid()) {
		                   continue;
		               }
		               
		               	Canvas c = holder.lockCanvas();
		               	c.drawColor(Color.TRANSPARENT);
		               //c.drawARGB(255, 0, 0, 0);
		               //c.drawPicture(picture)
		                onDraw(c);
		                holder.unlockCanvasAndPost(c);
		            }
		        }
		 
		        @Override
		        protected void onDraw(Canvas canvas) {
		            super.onDraw(canvas);
		            synchronized (pointsToDraw) {
		                for (Path path : pointsToDraw) {
		                    canvas.drawPath(path, mPaint);
		                }
		            }
		        }
		 
		        public void pause() {
		            isItOk = false;
		            while (true) {
		                try {
		                    t.join();
		                } catch (InterruptedException e) {

		                    e.printStackTrace();
		                }
		                break;
		            }
		            t = null;
		        }
		 
		        public void resume() {
		            isItOk = true;
		            t = new Thread(this);
		            t.start();
		 
		        }
		    }
}



	
		
			
		
		
		
