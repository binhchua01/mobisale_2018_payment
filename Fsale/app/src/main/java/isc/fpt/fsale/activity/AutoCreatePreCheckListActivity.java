package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.AutoPreCheckListGetTimezone;
import isc.fpt.fsale.action.AutoPreCheckListInsertCL;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.TimeZoneAdapter;
import isc.fpt.fsale.model.AutoPreCheckListInfo;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.model.SubTeamID1Model;
import isc.fpt.fsale.model.TimeZone;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// màn hình TẠO AUTO-CHECKLIST
public class AutoCreatePreCheckListActivity extends BaseActivity {
    private AutoPreCheckListInfo autoPreCheckListInfo;
    private ObjectModel objectModel;
    private TextView lblAutoObjectId, lblAutoFullName, lblAutoAddress, txtAutoContract, txtAutoInitStatus;
    private EditText edtAutoContactName, edtAutoPhone, edtAutoNote;
    private Spinner spCreatePreCheckListHandworkAutoPartner, spCreatePreChecklistHandworkAutoSubTeam,
            spCreatePreCheckListAutoDate, spAutoCreatePreCheckListTimeZone, spAutoDivision;
    private String sAutoDivisionAutoValue;
    private ArrayList<KeyValuePairModel> lstAutoDateCreatePreCheckList;
    private KeyValuePairAdapter adapterAutoDateCreatePreCheckList;
    private AutoCreatePreCheckListActivity autoCreatePreChecklistActivity;
    private Context mContext;

    /**
     * Constructor
     */
    public AutoCreatePreCheckListActivity() {
        super(R.string.create_auto_pre_check_list_header_activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.create_auto_pre_check_list_header_activity));
        this.mContext = this;
        this.autoCreatePreChecklistActivity = this;
        setContentView(R.layout.activity_auto_create_pre_check_list);
        initWidget();
        setAutoDivision();
        getIntentData();
        initSpinnerDateCreatePreCheckList();

    }

    public void initWidget() {
        lblAutoObjectId = (TextView) findViewById(R.id.txt_auto_object_id);
        lblAutoFullName = (TextView) findViewById(R.id.txt_auto_full_name);
        lblAutoAddress = (TextView) findViewById(R.id.txt_auto_address);
        txtAutoContract = (TextView) findViewById(R.id.txt_auto_contract);
        edtAutoContactName = (EditText) findViewById(R.id.edt_auto_contact_name);
        txtAutoInitStatus = (TextView) findViewById(R.id.txt_auto_init_status);
        edtAutoPhone = (EditText) findViewById(R.id.edt_auto_phone);
        edtAutoNote = (EditText) findViewById(R.id.edt_auto_note);
        spCreatePreCheckListHandworkAutoPartner = (Spinner) findViewById(R.id.sp_create_pre_checklist_handwork_auto_partner);
        spCreatePreChecklistHandworkAutoSubTeam = (Spinner) findViewById(R.id.sp_create_pre_checklist_handwork_auto_sub_team);
        spCreatePreCheckListAutoDate = (Spinner) findViewById(R.id.sp_create_pre_checklist_auto_date);
        spCreatePreCheckListAutoDate.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Common.showTimePickerDialog(AutoCreatePreCheckListActivity.this, true);
                }
                return false;
            }
        });

        spAutoCreatePreCheckListTimeZone = (Spinner) findViewById(R.id.sp_auto_create_pre_checklist_auto_time_zone);
        spAutoDivision = (Spinner) findViewById(R.id.sp_auto_division);
        // sự kiện chọn phòng ban
        spAutoDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                sAutoDivisionAutoValue = selectedItem.getsID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        Button btnAutoCreatePreCheckList = (Button) findViewById(R.id.btn_auto_create_pre_check_list);
        btnAutoCreatePreCheckList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAutoPreCheckList();
            }
        });

        spAutoCreatePreCheckListTimeZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                TimeZone item = (TimeZone) spAutoCreatePreCheckListTimeZone.getSelectedItem();
                if (item.getAbilityDesc() == 0) {
                    Common.alertDialog("Múi giờ dự kiến full năng lực, vui lòng tư vấn KH sang múi giờ màu xanh", mContext);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void createAutoPreCheckList() {
        if (validateInputData()) {
            Dialog dialog;
            String supporter = "", subSupporter = "", appointmentDate = "", timeZone = "", abilityDesc = "";
            if (spCreatePreCheckListHandworkAutoPartner.getAdapter() != null && spCreatePreCheckListHandworkAutoPartner.getAdapter().getCount() > 0)
                supporter = ((KeyValuePairModel) spCreatePreCheckListHandworkAutoPartner.getSelectedItem()).getsID();
            if (spCreatePreChecklistHandworkAutoSubTeam.getAdapter() != null && spCreatePreChecklistHandworkAutoSubTeam.getAdapter().getCount() > 0)
                subSupporter = ((KeyValuePairModel) spCreatePreChecklistHandworkAutoSubTeam.getSelectedItem()).getsID();
            if (spCreatePreCheckListAutoDate.getAdapter() != null && spCreatePreCheckListAutoDate.getAdapter().getCount() > 0)
                appointmentDate = ((KeyValuePairModel) spCreatePreCheckListAutoDate.getSelectedItem()).getsID();
            if (spAutoCreatePreCheckListTimeZone.getAdapter() != null && spAutoCreatePreCheckListTimeZone.getAdapter().getCount() > 0)
                timeZone = ((TimeZone) spAutoCreatePreCheckListTimeZone.getSelectedItem()).getTimezoneValue();
            if (spAutoCreatePreCheckListTimeZone.getAdapter() != null && spAutoCreatePreCheckListTimeZone.getAdapter().getCount() > 0)
                abilityDesc = String.valueOf(((TimeZone) spAutoCreatePreCheckListTimeZone.getSelectedItem()).getAbilityDesc());

            final String[] paramValue = new String[]{lblAutoObjectId.getText().toString(), Constants.USERNAME,
                    String.valueOf(autoPreCheckListInfo.getInit_Status()), edtAutoContactName.getText().toString(),
                    edtAutoPhone.getText().toString().trim(), edtAutoNote.getText().toString().trim(),
                    sAutoDivisionAutoValue, supporter, subSupporter, String.valueOf(autoPreCheckListInfo.getDeptID()),
                    String.valueOf(autoPreCheckListInfo.getOwnerType()), timeZone, appointmentDate, abilityDesc
            };

            final String finalAbilityDesc = abilityDesc;
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                    .setMessage(mContext.getResources().getString(R.string.msg_confirm_auto_create_prechecklist))
                    .setCancelable(false).setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (Integer.parseInt(finalAbilityDesc) == 0) {
                                new AlertDialog.Builder(mContext).setTitle("Thông Báo")
                                        .setMessage("Sẽ hẹn lại nếu không đáp ứng múi giờ này")
                                        .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // tạo 1 checklist tự động
                                                new AutoPreCheckListInsertCL(mContext, paramValue);
                                            }
                                        }).setCancelable(false).create().show();
                            } else {
                                // tạo 1 checklist tự động
                                new AutoPreCheckListInsertCL(mContext, paramValue);
                            }
                        }
                    })
                    .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            dialog = builder.create();
            dialog.show();
        }
    }

    // khởi tạo dữ liệu ban đầu
    public void getIntentData() {
        Intent intentData = getIntent();
        if (intentData != null) {
            autoPreCheckListInfo = intentData.getParcelableExtra("autoPreCheckListInfo");
            objectModel = intentData.getParcelableExtra("objectModel");
            if (autoPreCheckListInfo != null || objectModel != null) {
                lblAutoObjectId.setText(String.valueOf(objectModel.getObjID()));
                lblAutoFullName.setText(objectModel.getFullName());
                lblAutoAddress.setText(objectModel.getAddress());
                txtAutoContract.setText(objectModel.getContract());
                txtAutoInitStatus.setText(autoPreCheckListInfo.getInit_Status_Text());
                loadDataSupport(autoPreCheckListInfo);
            } else {
                Common.alertDialog(getResources().getString(R.string.lbl_data_auto_checklist_transport_fail), mContext);
            }
        }
    }

    //Khởi tạo danh sách phòng ban
    private void setAutoDivision() {
        ArrayList<KeyValuePairModel> lstDivision = new ArrayList<>();
        lstDivision.add(new KeyValuePairModel("", "[ Vui lòng chọn phòng ban ]"));
        lstDivision.add(new KeyValuePairModel("4", "IBB"));
        KeyValuePairAdapter adapterAutoDivision = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstDivision, Gravity.LEFT);
        spAutoDivision.setAdapter(adapterAutoDivision);

    }

    // load đối tác và tổ con
    public void loadDataSupport(AutoPreCheckListInfo autoPreCheckListInfo) {
        SubTeamID1Model supporter = new SubTeamID1Model();
        if (autoPreCheckListInfo.getSupporter() != null) {
            supporter.setPartnerList(new ArrayList<KeyValuePairModel>());
            supporter.getPartnerList().add(new KeyValuePairModel(autoPreCheckListInfo.getSupporter(), autoPreCheckListInfo.getSupporter()));
        }
        if (autoPreCheckListInfo.getSubSupporter() != null) {
            supporter.setSubTeamList(new ArrayList<KeyValuePairModel>());
            supporter.getSupTeamList().add(new KeyValuePairModel(autoPreCheckListInfo.getSubSupporter(), autoPreCheckListInfo.getSubSupporter()));
        }
        if (supporter.getPartnerList().size() > 0 || supporter.getSupTeamList().size() > 0) {
            if (supporter.getPartnerList().size() > 0) {
                spCreatePreCheckListHandworkAutoPartner.setAdapter(new KeyValuePairAdapter(this, R.layout.my_spinner_style, supporter.getPartnerList(), Gravity.LEFT));
            }
            if (supporter.getSupTeamList().size() > 0) {
                spCreatePreChecklistHandworkAutoSubTeam.setAdapter(new KeyValuePairAdapter(this, R.layout.my_spinner_style, supporter.getSupTeamList(), Gravity.LEFT));
            }
        } else {
            new AlertDialog.Builder(this).setTitle("Thông Báo")
                    .setMessage(getString(R.string.msg_not_fount_partner_and_subteam))
                    .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    })
                    .setCancelable(false).create().show();
        }

    }

    //Load dữ liệu chọn ngày tạo PreCheckList
    private void initSpinnerDateCreatePreCheckList() {
        lstAutoDateCreatePreCheckList = new ArrayList<>();
        lstAutoDateCreatePreCheckList.add(new KeyValuePairModel("", "Chọn ngày"));
        adapterAutoDateCreatePreCheckList = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstAutoDateCreatePreCheckList, Gravity.LEFT);
        spCreatePreCheckListAutoDate.setAdapter(adapterAutoDateCreatePreCheckList);
    }

    // cập nhật dữ liệu vào spinner ngày tạo
    public void updateDateSpinner(int newYear, int newMonth, int newDay) {
        //update by tuantt14
        spAutoCreatePreCheckListTimeZone.setAdapter(null);
        if (spCreatePreCheckListAutoDate != null) {
            if (newYear > 0) {
                String customDateDesc = "Ngày " + newDay + " tháng " + (newMonth + 1) + " năm " + (newYear);
                Calendar date = Calendar.getInstance();
                date.set(newYear, newMonth, newDay);
                String customDate = Common.getSimpleDateFormat(date, Constants.DATE_FORMAT);
                lstAutoDateCreatePreCheckList.clear();
                lstAutoDateCreatePreCheckList.add(new KeyValuePairModel(customDate, customDateDesc));
                adapterAutoDateCreatePreCheckList.notifyDataSetChanged();
            }
            spCreatePreCheckListAutoDate.setSelection(0);
            Method method = null;
            try {
                method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            assert method != null;
            method.setAccessible(true);
            try {
                method.invoke(spCreatePreCheckListAutoDate);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            String supporter = "", subSupporter = "", appointmentDate = "";
            if (spCreatePreCheckListHandworkAutoPartner.getAdapter() != null && spCreatePreCheckListHandworkAutoPartner.getAdapter().getCount() > 0)
                supporter = ((KeyValuePairModel) spCreatePreCheckListHandworkAutoPartner.getSelectedItem()).getsID();
            if (spCreatePreChecklistHandworkAutoSubTeam.getAdapter() != null && spCreatePreChecklistHandworkAutoSubTeam.getAdapter().getCount() > 0)
                subSupporter = ((KeyValuePairModel) spCreatePreChecklistHandworkAutoSubTeam.getSelectedItem()).getsID();
            if (spCreatePreCheckListAutoDate.getAdapter() != null && spCreatePreCheckListAutoDate.getAdapter().getCount() > 0)
                appointmentDate = ((KeyValuePairModel) spCreatePreCheckListAutoDate.getSelectedItem()).getsID();
            if (autoPreCheckListInfo != null) {
                String[] paramsValue = new String[]{String.valueOf(autoPreCheckListInfo.getInit_Status()),
                        supporter, subSupporter, appointmentDate, Constants.USERNAME, String.valueOf(objectModel.getObjID())};
                // lấy múi giờ để tạo checklist tự động
                new AutoPreCheckListGetTimezone(mContext, autoCreatePreChecklistActivity, paramsValue);
            } else {
                Common.alertDialog(getResources().getString(R.string.msg_empty_input_get_timezone_info), mContext);
            }

        }
    }

    // Tải danh sách timezone
    public void loadTimeZone(ArrayList<TimeZone> timeZone) {
        ArrayList<TimeZone> lst = new ArrayList<>();
        lst.add(new TimeZone("Chọn múi giờ", "", -1, ""));
        if (timeZone != null) {
            //add item default
            lst.addAll(timeZone);
        }
        spAutoCreatePreCheckListTimeZone.setAdapter(new TimeZoneAdapter(this, R.layout.my_spinner_style, lst));
    }

    //kiểm tra dự liệu nhập
    private boolean validateInputData() {
        if (edtAutoContactName.getText() == null || edtAutoContactName.getText().toString().trim().equals("")) {
            edtAutoContactName.setError("Vui lòng nhập người liên hệ!");
            edtAutoContactName.requestFocus();
            return false;
        }
        if (lblAutoObjectId.getText() == null || lblAutoObjectId.getText().toString().trim().equals("")) {
            Common.alertDialog("Vui lòng nhập số hợp đồng", mContext);
            return false;
        }
        if (edtAutoPhone.getText() == null || edtAutoPhone.getText().toString().trim().equals("")) {
            edtAutoPhone.setError("Vui lòng nhập số điện thoại!");
            edtAutoPhone.requestFocus();
            return false;
        }
        if (spCreatePreCheckListHandworkAutoPartner.getAdapter() == null) {
            Common.alertDialog(getResources().getString(R.string.msg_empty_data_partner_checklist), mContext);
            return false;
        }
        if (spCreatePreChecklistHandworkAutoSubTeam.getAdapter() == null) {
            Common.alertDialog(getResources().getString(R.string.msg_empty_data_partner_sub_team_checklist), mContext);
            return false;
        }
        if (spCreatePreCheckListHandworkAutoPartner.getAdapter() != null || spCreatePreChecklistHandworkAutoSubTeam.getAdapter() != null) {
            if (spCreatePreCheckListAutoDate.getAdapter() != null && spCreatePreCheckListAutoDate.getAdapter().getCount() > 0) {
                String dateSelectedValue = ((KeyValuePairModel) spCreatePreCheckListAutoDate.getSelectedItem()).getsID();
                if (dateSelectedValue.length() == 0) {
                    Common.alertDialog(getResources().getString(R.string.msg_not_selected_date_create_checklist), mContext);
                    return false;
                } else {
                    if (spAutoCreatePreCheckListTimeZone.getAdapter() == null) {
                        Common.alertDialog(getResources().getString(R.string.msg_not_found_time_zone_create_checklist), mContext);
                        return false;
                    }
                }

                if (spAutoCreatePreCheckListTimeZone.getAdapter() != null && spAutoCreatePreCheckListTimeZone.getAdapter().getCount() > 0 &&
                        TextUtils.isEmpty(((TimeZone) spAutoCreatePreCheckListTimeZone.getSelectedItem()).getTimezoneValue())) {
                    Common.alertDialog(getResources().getString(R.string.label_message_choose_time_zone), mContext);
                    return false;
                }
            }
        }

        if (edtAutoNote.getText() == null || edtAutoNote.getText().toString().trim().equals("")) {
            edtAutoNote.setError("Vui lòng nhập ghi chú!");
            edtAutoNote.requestFocus();
            return false;
        }
        if (sAutoDivisionAutoValue == null || sAutoDivisionAutoValue.trim().equals("")) {
            Common.alertDialog("Vui lòng chọn phòng ban", mContext);
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
