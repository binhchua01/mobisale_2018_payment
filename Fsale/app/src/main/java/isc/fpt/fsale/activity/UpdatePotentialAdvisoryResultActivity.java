package isc.fpt.fsale.activity;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPotentialAdvisoryResultDetail;
import isc.fpt.fsale.action.GetPotentialAdvisoryResultValue;
import isc.fpt.fsale.action.UpdatePotentialAdvisoryResult;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.PotentialAdvisoryResultDetailListAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialAdvisoryResultDetailModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.SurveyValueModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

//màn hình KẾT QUẢ TƯ VẤN KHÁCH HÀNG TIỀM NĂNG
public class UpdatePotentialAdvisoryResultActivity extends BaseActivity {
    private Spinner spSurveyValue;
    private EditText txtValue;
    private Button btnUpdate;
    private PotentialObjModel mPotential;
    private ListView lvResult;
    private ImageView imgNavigation;
    private LinearLayout frmUpdate;

    public UpdatePotentialAdvisoryResultActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.lbl_screen_name_potential_update_advisory_result);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_potential_update_advisory_result));
        setContentView(R.layout.activity_update_potential_advisory_result);
        spSurveyValue = (Spinner) findViewById(R.id.sp_potential_obj_advisory_result);
        spSurveyValue.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                try {
                    KeyValuePairModel item = (KeyValuePairModel) parent.getItemAtPosition(position);
                    if (item.getID() == -1) {
                        txtValue.setEnabled(true);
                        Common.showSoftKeyboard(UpdatePotentialAdvisoryResultActivity.this);
                    } else {
                        txtValue.setEnabled(false);
                        txtValue.setText("");
                        Common.hideSoftKeyboard(UpdatePotentialAdvisoryResultActivity.this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txtValue = (EditText) findViewById(R.id.txt_potential_obj_adisory_result);
        btnUpdate = (Button) findViewById(R.id.btn_update);
        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkForUpdate()) {
                    comfirmToUpdate();
                }
            }
        });

        lvResult = (ListView) findViewById(R.id.lv_result);
        frmUpdate = (LinearLayout) findViewById(R.id.frm_update);
        imgNavigation = (ImageView) findViewById(R.id.img_navigation_drop_down);
        imgNavigation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dropDownNavigation();
            }
        });
        getDataFromIntent();
        getAdvisoryResultValue();
        getResultList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void dropDownNavigation() {
        if (frmUpdate.getVisibility() == View.VISIBLE) {
            frmUpdate.setVisibility(View.GONE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
        } else {
            frmUpdate.setVisibility(View.VISIBLE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
        }
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(PotentialObjDetailActivity.TAG_POTENTIAL_OBJECT)) {
                mPotential = intent.getParcelableExtra(PotentialObjDetailActivity.TAG_POTENTIAL_OBJECT);
            }
        }
    }

    private void getAdvisoryResultValue() {
        new GetPotentialAdvisoryResultValue(this);
    }

    public void loadAdvisoryResultValue(ArrayList<SurveyValueModel> lst) {
        if (lst != null && lst.size() > 0) {
            ArrayList<KeyValuePairModel> items = new ArrayList<>();
            for (int i = 0; i < lst.size(); i++) {
                SurveyValueModel item = lst.get(i);
                items.add(new KeyValuePairModel(item.getID(), item.getDesc()));
            }
            spSurveyValue.setAdapter(new KeyValuePairAdapter(this, R.layout.my_spinner_style, items, Gravity.LEFT));
        } else {
            Common.alertDialog("Không có dữ liệu trả về!", this);
        }
    }

    private boolean checkForUpdate() {
        if (spSurveyValue.getAdapter() == null || spSurveyValue.getAdapter().getCount() <= 0) {
            Common.alertDialog("Chưa chọn kết quả tư vấn!", this);
            return false;
        } else {
            KeyValuePairModel item = (KeyValuePairModel) spSurveyValue.getSelectedItem();
            if (item.getID() == -1 && Common.isEmpty(txtValue)) {
                Common.alertDialog("Chưa nhập nội dung!", this);
                txtValue.setFocusable(true);
                return false;
            }
        }
        return true;
    }

    private void comfirmToUpdate() {
        String Message = "Bạn có muốn cập nhật?";
        AlertDialog.Builder builder = null;
        Dialog dialog = null;
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Thông báo").setMessage(Message).setCancelable(false).setPositiveButton("Có",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        update();
                    }
                }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,
                                int id) {
                dialog.cancel();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void update() {
        KeyValuePairModel surveyValue = (KeyValuePairModel) spSurveyValue.getSelectedItem();
        int surveyValueID = surveyValue.getID();

        int potentialID = mPotential != null ? mPotential.getID() : 0;
        int sourceID = mPotential != null ? mPotential.getSource() : 0;
        String caseID = mPotential != null ? mPotential.getCaseID() : "";
        String value = "";
        value = txtValue.getText().toString() == null ? "" : txtValue.getText().toString();
        new UpdatePotentialAdvisoryResult(this, potentialID, surveyValueID, value, sourceID, caseID);

    }


    public void getResultList() {
        new GetPotentialAdvisoryResultDetail(this, mPotential.getID());
    }

    public void loadResultList(ArrayList<PotentialAdvisoryResultDetailModel> lst) {
        if (lst != null && lst.size() > 0) {
            PotentialAdvisoryResultDetailListAdapter adapter = new PotentialAdvisoryResultDetailListAdapter(this, lst);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog("Không có dữ liệu trả về!", this);
        }
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
