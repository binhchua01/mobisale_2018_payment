package isc.fpt.fsale.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.danh32.fontify.Button;
import com.danh32.fontify.TextView;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CreateObject_2018;
import isc.fpt.fsale.model.CreateObject_2018POST;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/*
 * Contract Activity
 * Author: TruongPV5
 * Date: 19/04/2018
 * upadte by: haulc3
 */

public class CreateContractActivity extends BaseActivity {
    private String linkImage = "";
    private RegistrationDetailModel mRegister;
    private TextView tvRegCode, tvCusName, tvPhone;
    private ImageView imgSignature;
    private Button btnContract;
    private LinearLayout viewSignature;

    public CreateContractActivity() {
        super(R.string.create_contract_header_activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.create_contract_header_activity));
        setContentView(R.layout.activity_create_contract);
        //Create widget
        createWidgets();
        //Create events
        createEvents();
        //Show data on text
        showDataOnText();
    }

    private void createWidgets() {
        tvRegCode = (TextView) findViewById(R.id.tvRegCodeCreateContract);
        tvCusName = (TextView) findViewById(R.id.tvCusNameCreateContract);
        tvPhone = (TextView) findViewById(R.id.tvPhoneCreateContract);
        viewSignature = (LinearLayout) findViewById(R.id.imgSignatureCreateContract_view);
        viewSignature.setVisibility(View.GONE);
        imgSignature = (ImageView) findViewById(R.id.imgSignatureCreateContract);
        btnContract = (Button) findViewById(R.id.btn_create_contract);
        viewSignature = (LinearLayout) findViewById(R.id.imgSignatureCreateContract_view);
        viewSignature.setVisibility(View.GONE);
    }

    private void createEvents() {
        //ImageView Signature
        imgSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateContractActivity.this, SignatureActivity.class);
                intent.putExtra("RegID", mRegister.getID());
                startActivityForResult(intent, 1);
            }
        });

        //Button contract
        btnContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { createNewContract(); }
        });
    }

    private void showDataOnText() {
        mRegister = getIntent().getParcelableExtra(Constants.MODEL_REGISTER);
        if (mRegister != null) {
            tvRegCode.setText(mRegister.getRegCode());
            tvCusName.setText(mRegister.getFullName());
            tvPhone.setText(mRegister.getPhone_1());
        } else {
            Common.alertDialog("Thông tin chi tiết phiếu đăng ký truyền đi thất bại", this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("image");
                linkImage = data.getStringExtra("Id");
                Bitmap decodedByte = Common.StringToBitMap(result);
                BitmapDrawable ob = new BitmapDrawable(getResources(), decodedByte);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imgSignature.setBackground(ob);
                }
            }
        }
    }

    /*
     * Create a new contract
     */
    private void createNewContract() {
        //POST object
        CreateObject_2018POST obj = new CreateObject_2018POST();
        obj.setUserName(mRegister.getUserName());
        obj.setRegCode(mRegister.getRegCode());
        obj.setTotal(String.valueOf(mRegister.getTotal()));
        obj.setImageSignature(linkImage);
        //kết nối API tạo hợp đồng
        new CreateObject_2018(CreateContractActivity.this, obj, mRegister);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
}