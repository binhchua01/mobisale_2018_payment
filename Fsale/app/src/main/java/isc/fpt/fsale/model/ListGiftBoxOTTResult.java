package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class ListGiftBoxOTTResult {
    @SerializedName("GiftID")
    @Expose
    private Integer giftID;
    @SerializedName("GiftName")
    @Expose
    private String giftName;

    public ListGiftBoxOTTResult(Integer giftID, String giftName) {
        this.giftID = giftID;
        this.giftName = giftName;
    }

    public Integer getGiftID() {
        return giftID;
    }

    public void setGiftID(Integer giftID) {
        this.giftID = giftID;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("GiftID", getGiftID());
            jsonObject.put("GiftName", getGiftName());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
