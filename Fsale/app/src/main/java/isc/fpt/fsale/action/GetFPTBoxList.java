package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3v2;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 1/18/2018.
 */

public class GetFPTBoxList implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterStep3 fragmentStep3;

    //ver3.19
    private FragmentRegisterStep3v2 fragmentStep3v2;

    public GetFPTBoxList(Context mContext, FragmentRegisterStep3 fragment) {
        this.mContext = mContext;
        this.fragmentStep3 = fragment;
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        String[] arrParamName = new String[]{"UserName"};
        String[] arrParamValue = new String[]{UserName};
        String message = mContext.getResources().getString(R.string.msg_pd_get_fpt_box_list);
        String GET_FPT_BOX_LIST = "GetListDeviceOTT";
        CallServiceTask service = new CallServiceTask(mContext, GET_FPT_BOX_LIST, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetFPTBoxList.this);
        service.execute();
    }

    //ver3.19
    public GetFPTBoxList(Context mContext, FragmentRegisterStep3v2 fragment) {
        this.mContext = mContext;
        this.fragmentStep3v2 = fragment;
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        String[] arrParamName = new String[]{"UserName"};
        String[] arrParamValue = new String[]{UserName};
        String message = mContext.getResources().getString(R.string.msg_pd_get_fpt_box_list);
        String GET_FPT_BOX_LIST = "GetListDeviceOTT";
        CallServiceTask service = new CallServiceTask(mContext, GET_FPT_BOX_LIST, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetFPTBoxList.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<FPTBox> lst = new ArrayList<>();
            boolean isError = false;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<FPTBox> resultObject = new WSObjectsModel<>(jsObj, FPTBox.class);
                if (resultObject.getErrorCode() == 0) {
                    lst = resultObject.getArrayListObject();
                } else {
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }

//                if (!isError) {
//                    if (lst.size() > 0) {
//                        if (fragmentStep3 != null) {
//                            fragmentStep3.loadFptBox(lst);
//                        }
//                    } else {
//                        Common.alertDialog(mContext.getResources().getString(R.string.title_empty_data), mContext);
//                    }
//                }
                //ver3.19
                if (!isError) {
                    if (lst.size() > 0) {
                        if (fragmentStep3v2 != null) {
                            fragmentStep3v2.loadFptBox(lst);
                        }
                    } else {
                        Common.alertDialog(mContext.getResources().getString(R.string.title_empty_data), mContext);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
