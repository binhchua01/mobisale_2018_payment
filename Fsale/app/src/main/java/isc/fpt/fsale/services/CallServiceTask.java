package isc.fpt.fsale.services;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.MapBookPortAuto;
import isc.fpt.fsale.activity.UploadRegistrationDocumentActivity;
import isc.fpt.fsale.activity.LoginActivity;
import isc.fpt.fsale.ui.MyProgressDialog;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/*
 * ASYNC TASK CLASS:CallServiceTask
 *
 * @description: call service api task, handle response result
 * @author: vandn
 * @created on:		12/08/2013
 */
public class CallServiceTask extends AsyncTask<String, String, String> implements AsyncTaskCompleteListener<Boolean> {
    private String methodName;
    private String params;//define params variable for 'url/params' format
    private String[] arrParams, paramsValue; // define params variable for json post format
    private JSONObject jsonObjectParam;
    private String result = null;
    private String type;
    private String msg;
    private ProgressDialog pd;
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private AsyncTaskCompleteListener<String> callback;
    // count variable to retry call WS (add by ltthao - 13.11.2013)
    private int iExecuteCount = 1;
    private boolean isNetWorkAvailable = true;
    private Bitmap bitmap;

    public CallServiceTask(Context mContext, String methodName, String params,
                           String type, String msg, AsyncTaskCompleteListener<String> cb) {
        this.methodName = methodName;
        this.params = params;
        this.type = type;
        this.msg = msg;
        this.mContext = mContext;
        this.callback = cb;

        try {
            ((MyApp) mContext.getApplicationContext()).addSyncThreads(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CallServiceTask(Context mContext, String methodName, String[] params,
                           String[] paramsValue, String type, String msg,
                           AsyncTaskCompleteListener<String> cb) {
        this.methodName = methodName;
        this.arrParams = params;
        this.paramsValue = paramsValue;
        this.type = type;
        this.msg = msg;
        this.mContext = mContext;
        this.callback = cb;

        try {
            ((MyApp) mContext.getApplicationContext()).addSyncThreads(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CallServiceTask(Context mContext, String methodName, JSONObject jsonObjParam,
                           String type, String msg, AsyncTaskCompleteListener<String> cb) {
        this.methodName = methodName;
        this.jsonObjectParam = jsonObjParam;
        this.type = type;
        this.msg = msg;
        this.mContext = mContext;
        this.callback = cb;

        try {
            ((MyApp) mContext.getApplicationContext()).addSyncThreads(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //upload image protocol multipart
    public CallServiceTask(Context mContext, String methodName, Bitmap bitmap, String[] params,
                           String[] paramsValue, String type, String msg, AsyncTaskCompleteListener<String> cb) {
        this.methodName = methodName;
        this.bitmap = bitmap;
        this.arrParams = params;
        this.paramsValue = paramsValue;
        this.type = type;
        this.msg = msg;
        this.mContext = mContext;
        this.callback = cb;

        try {
            ((MyApp) mContext.getApplicationContext()).addSyncThreads(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        if (Common.isNetworkAvailable(this.mContext)) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                public void run() {
//                    if(mMyProgressDialog != null){
//                        mMyProgressDialog.show();
//                    }else{
//                        pd = Common.showProgressBar(mContext, msg + " (" + iExecuteCount + ")");
//                    }
                    pd = Common.showProgressBar(mContext, msg + " (" + iExecuteCount + ")");

                }
            });
        } else isNetWorkAvailable = false;
    }

    @Override
    protected String doInBackground(String... params) {
        if (isNetWorkAvailable) {
            try {
                result = RetryToCallService();
                if (this.type.equals(Services.GET)) {
                    // define max time to call service
                    int MAX_TIMES = 10;
                    while (result == null && Common.checkSession(mContext) && iExecuteCount < MAX_TIMES) {
                        Thread.sleep(6000);
                        iExecuteCount++;
                        publishProgress("" + iExecuteCount);
                        result = RetryToCallService();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        //Kiem tra neu activity bi huỷ thi ko chay
        if (!Common.checkLifeActivity(mContext)) {
            return;
        }

        //Huy task
        ArrayList<CallServiceTask> Syncs = ((MyApp) mContext.getApplicationContext()).getSyncThreads();
        if (Syncs != null) {
            for (int i = 0; i < Syncs.size(); i++) {
                CallServiceTask task = Syncs.get(i);
                if (task == this) {
                    Syncs.remove(i);
                }
            }
        }

        if (isCancelled()){
            result = null;
        }

        if (isNetWorkAvailable) {
            // Tuấn cập nhật code 25092017 kiểm tra trạng thái của processbar
            if (this.pd != null) {
                if (this.pd.isShowing()) {
                    this.pd.dismiss();
                }
            }
            if (Common.checkSession(mContext)) {
                if (result == null) {
                    //check the reason of fail connection by poor network connection or by server error:
                    //if ping host request is fail > 3 times: poor network
                    //else: server error
                    PingHostTask pingHost = new PingHostTask(mContext, CallServiceTask.this);
                    pingHost.execute();
                } else {
                    try {
                        callback.onTaskComplete(result);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                mContext.startActivity(new Intent(mContext, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        } else {
            // kiểm tra tình trạng upload số hình thành công và thất bại khi mạng mất kết nối
            if (mContext != null && mContext.getClass().getSimpleName().equals(UploadRegistrationDocumentActivity.class.getSimpleName())) {
                Intent intent = new Intent("UPLOAD_FILE_DISCONNECT_TO_INTERNET");
                mContext.sendBroadcast(intent);
            }
            if(mContext != null){
                Common.alertDialog(mContext.getResources().getString(R.string.msg_check_internet), mContext);
            }
        }
    }

    private String RetryToCallService() {
        String result = null;
        if (Common.checkSession(mContext)) {
            switch (this.type) {
                case Services.GET:
                    result = Services.get(this.methodName, this.params, mContext);
                    break;
                case Services.POST:
                    result = Services.post(this.methodName, this.params);
                    break;
                case Services.JSON_POST:
                    try {
                        result = Services.postJson(this.methodName, this.arrParams, this.paramsValue, mContext);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Services.JSON_POST_OBJECT:
                    try {
                        result = Services.postJsonObject(this.methodName, jsonObjectParam, mContext);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Services.JSON_POST_UPLOAD_MULTIPART:
                    try {
                        result = Services.postFileMultipart(this.methodName, this.bitmap,
                                this.arrParams, this.paramsValue, mContext);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    try {
                        result = Services.postJsonUpload(this.methodName, this.arrParams, this.paramsValue, mContext);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
        return result;
    }

    @Override
    public void onProgressUpdate(String... progress) {
        this.pd.setMessage(this.msg + " (" + Integer.parseInt(progress[0]) + ")");
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    /*
     * handle ping host request result
     * @author: vandn, added on 25/11/2013
     */
    @Override
    public void onTaskComplete(Boolean isPoorSignal) {
        if (mContext.getClass().getSimpleName().equals(MapBookPortAuto.class.getSimpleName())) {
            MapBookPortAuto mMapBookPortAuto = (MapBookPortAuto) mContext;
            int mNumberRetryConnect = mMapBookPortAuto.getNumberRetryConnect() + 1;
            mMapBookPortAuto.setNumberRetryConnect(mNumberRetryConnect);
            if (isPoorSignal) {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_network_warning), mContext);
            } else {
                if (mMapBookPortAuto.getNumberRetryConnect() > Constants.AutoBookPort_RetryConnect) {
                    ((MapBookPortAuto) this.mContext).showMessageAutoBookPortManual(mContext, mContext.getResources().getString(R.string.title_message_box_book_port_time_out_last));
                } else {
                    Common.alertDialog(mContext.getResources().getString(R.string.title_message_box_book_port_time_out), mContext);
                }
            }
        } else {
            if (isPoorSignal) {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_network_warning), mContext);
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
            }
        }
    }
}