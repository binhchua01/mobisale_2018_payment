package isc.fpt.fsale.activity.house_position;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.extra_ott.adapter.KeyPairValueAdapter;
import isc.fpt.fsale.utils.Constants;

public class HousePositionActivity extends BaseActivitySecond
        implements OnItemClickListener<KeyValuePairModel> {
    private RelativeLayout rltBack;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_house_position;
    }

    @Override
    protected void initView() {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_house_position);
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        ArrayList<KeyValuePairModel> lstHousePositions = new ArrayList<>();
        lstHousePositions.add(new KeyValuePairModel(1, "Kế bên phải"));
        lstHousePositions.add(new KeyValuePairModel(2, "Kế bên trái"));
        lstHousePositions.add(new KeyValuePairModel(3, "Đối diện"));
        lstHousePositions.add(new KeyValuePairModel(4, "Phía sau"));
        lstHousePositions.add(new KeyValuePairModel(5, "Cách"));
        KeyPairValueAdapter mAdapter = new KeyPairValueAdapter(this, lstHousePositions, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(KeyValuePairModel object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.KEY_PAIR_OBJ, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
