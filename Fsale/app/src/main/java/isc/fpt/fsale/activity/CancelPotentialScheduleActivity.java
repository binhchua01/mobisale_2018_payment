package isc.fpt.fsale.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Map;

import isc.fpt.fsale.utils.MyApp;

public class CancelPotentialScheduleActivity extends AppCompatActivity {
    private Map<Integer, Ringtone> listRingtone;
    private NotificationManager alarmNotificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listRingtone = ((MyApp) getApplicationContext()).getListRingtone();
        alarmNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent myIntent = getIntent();
        int potentialObjScheduleID = myIntent.getIntExtra("potentialObjScheduleID", 0);
        if (myIntent != null) {
            if (listRingtone.containsKey(potentialObjScheduleID)) {
                alarmNotificationManager.cancel(potentialObjScheduleID);
                Ringtone ring = listRingtone.get(potentialObjScheduleID);
                if (ring.isPlaying())
                    ring.stop();
            }
        }
        finish();
    }
}