package isc.fpt.fsale.ui.fragment;

import java.util.ArrayList;

import org.json.JSONObject;

import isc.fpt.fsale.action.GetPromotionBrochure;
import isc.fpt.fsale.activity.PromotionAdTextFullScreenActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.PromotionAdTextAdapter;
import isc.fpt.fsale.model.PromotionBrochureModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class FragmentPromotionAdTextList extends Fragment implements OnItemClickListener{

	public static final String ARG_MEDIA_TYPE = "ARG_MEDIA_TYPE";
    private Context mContext;
    private ArrayList<PromotionBrochureModel> mList;
    
    private ListView mListView;
    private ProgressBar procBar; 
    
    public static FragmentPromotionAdTextList newInstance(Bundle args) {
		FragmentPromotionAdTextList fragment = new FragmentPromotionAdTextList();
		/*Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		args.putString(ARG_SECTION_TITLE, sectionTitle);*/
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentPromotionAdTextList() {
		
	}
	
	 // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);  
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContext = getActivity();
		View rootView = inflater.inflate(R.layout.fragment_promotion_text_list, container, false);
		mListView = (ListView)rootView.findViewById(R.id.lv_object);
		procBar = (ProgressBar)rootView.findViewById(R.id.progress_bar);
		
		mListView.setOnItemClickListener(this);
		return rootView;
	}
	
	
	private void getData(int page){
    	//new GetPromotionBrochure(mContext, this, GetPromotionBrochure.TYPE_TEXT);
		
		procBar.setVisibility(View.VISIBLE);
		mListView.setVisibility(View.GONE);
		new GetBrochure().execute(String.valueOf(GetPromotionBrochure.TYPE_TEXT));
    }
    
    public void loadData(ArrayList<PromotionBrochureModel> lst){
    	procBar.setVisibility(View.GONE);
		mListView.setVisibility(View.VISIBLE);
    	if(lst != null && lst.size() >0){    		    
    		mList = lst;
    		mListView.setAdapter(new PromotionAdTextAdapter(getActivity(), mList));
    	}else{
			Toast.makeText(mContext, "Không có dữ liệu!", Toast.LENGTH_LONG).show();
    	}
    }
    
    private class GetBrochure extends AsyncTask<String, Integer, ArrayList<PromotionBrochureModel>> {
	     protected ArrayList<PromotionBrochureModel> doInBackground(String... type) {
	         String result = null;
	         String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
	         String[] paramsValue = new String[]{userName, type[0]};
	         ArrayList<PromotionBrochureModel> lst = null;
	         try {
				result = Services.postJson(GetPromotionBrochure.METHOD_NAME, GetPromotionBrochure.paramNames, paramsValue, mContext);
				 JSONObject jsObj = new JSONObject(result);
				 if(jsObj != null){
					 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
					 WSObjectsModel<PromotionBrochureModel> resultObject = new WSObjectsModel<PromotionBrochureModel>(jsObj, PromotionBrochureModel.class);
					 if(resultObject != null){
						 if(resultObject.getErrorCode() == 0){//OK not Error
							lst = resultObject.getArrayListObject();								
						 }else{//ServiceType Error
							 Toast.makeText(mContext, resultObject.getError(), Toast.LENGTH_LONG).show();
							 //Common.alertDialog( resultObject.getError(),mContext);
						 }
					 }
				 }
				 return lst;
			} catch (Exception e) {
				// TODO Auto-generated catch block

				e.printStackTrace();
			}
	    	 
	    	 return null;
	     }

	     protected void onProgressUpdate(Integer... progress) {
	         
	     }

	     protected void onPostExecute(ArrayList<PromotionBrochureModel> lst) {
	    	 loadData(lst);
	     }
	 }
    
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(mListView != null && mListView.getAdapter() == null)
			getData(0);
	}
	
    @Override
    public void onDestroyView() {
        super.onDestroyView();        
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub
		try {
			PromotionBrochureModel item = (PromotionBrochureModel)parent.getItemAtPosition(position);
			if(item != null){
				Intent intent = new Intent(getActivity(), PromotionAdTextFullScreenActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				intent.putExtra(PromotionAdTextFullScreenActivity.TAG_PROMOTION_AD_ITEM, item);
				startActivity(intent);
			}
			
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
	}
}
