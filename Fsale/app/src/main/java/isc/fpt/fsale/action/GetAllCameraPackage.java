package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_package.CameraPackageActivity;
import isc.fpt.fsale.model.CameraPackage;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by Hau Le on 2019-01-02.
 */

public class GetAllCameraPackage implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private CameraDetail mCameraDetail;

    public GetAllCameraPackage(Context mContext, CameraDetail mCameraDetail, int combo, boolean isSellMore) {
        this.mContext = mContext;
        this.mCameraDetail = mCameraDetail;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("CameraTypeID", mCameraDetail.getTypeID());
            jsonObject.put("Combo", combo);
            jsonObject.put("RegType", isSellMore ? 1 : 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.msg_progress_get_all_camera_package);
        String GET_ALL_CAMERA_PACKAGE = "GetAllCameraPackage";
        CallServiceTask service = new CallServiceTask(mContext, GET_ALL_CAMERA_PACKAGE, jsonObject,
                Services.JSON_POST_OBJECT, message, GetAllCameraPackage.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            List<CameraPackage> lst;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<CameraPackage> resultObject = new WSObjectsModel<>(jsObj, CameraPackage.class);
                lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (mContext != null) {
                    if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                        Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                    } else {
                        ((CameraPackageActivity) mContext).loadCameraPackage(lst, mCameraDetail);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
