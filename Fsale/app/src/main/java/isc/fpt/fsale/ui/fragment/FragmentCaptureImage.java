package isc.fpt.fsale.ui.fragment;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.TouchImageView;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FragmentCaptureImage extends Fragment {

	public static final String TAG_IMAGE_PATH = "TAG_IMAGE_PATH";
    private String imagePath ;
    private TouchImageView imgView;
    private TextView lblPromDesc;
    
	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static FragmentCaptureImage newInstance(String imagePath) {
		FragmentCaptureImage fragment = new FragmentCaptureImage();
		Bundle bundle = new Bundle();
		bundle.putString(TAG_IMAGE_PATH, imagePath);
		fragment.setArguments(bundle);
		return fragment;
	}

	public FragmentCaptureImage() {
		
	}
	
	 // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);      
        Bundle bundle = getArguments();//.getBundle(ARG_BUNDLE);
        if(bundle != null){
        	imagePath = bundle.getString(TAG_IMAGE_PATH);	       
        }
    }

    //TouchImageView img ;
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_promotion_image_detail, container, false);
		lblPromDesc = (TextView) rootView.findViewById(R.id.title);
		lblPromDesc.setVisibility(View.GONE);
		
		imgView = (TouchImageView) rootView.findViewById(R.id.grid_item_image);
		
		FrameLayout frameLayout = (FrameLayout) rootView.findViewById(R.id.main_background);
		ColorDrawable colorDrawable = new ColorDrawable(Color.BLACK);
		ProgressBar procBar = (ProgressBar)rootView.findViewById(R.id.progress_bar);
		procBar.setVisibility(View.GONE);
        final int sdk = android.os.Build.VERSION.SDK_INT;
		if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			frameLayout.setBackgroundDrawable(colorDrawable);
		} else {
			frameLayout.setBackground(colorDrawable);
		}
		updateImage();
		return rootView;
	}
	
	private void updateImage(){
		if(imgView != null && lblPromDesc != null){
			 Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
		     imgView.setImageBitmap(bitmap);		
		     lblPromDesc.setText(imagePath);
		}
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}
	
    @Override
    public void onDestroyView() {
        super.onDestroyView();        
    }
}
