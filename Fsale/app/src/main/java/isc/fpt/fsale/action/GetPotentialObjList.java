package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListReportPotentialObjActivity;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentPotentialObjListItem;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.google.gson.Gson;

// API lấy danh sách khách hàng tiềm năng
public class GetPotentialObjList implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentPotentialObjListItem mFragment;
    private final String TAG_METHOD_NAME = "GetPotentialObjList";

    public GetPotentialObjList(FragmentPotentialObjListItem mFragment, Context mContext, int Agent,
                               String AgentName, int PageNumber) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("Agent", Agent);
            jsonObject.put("AgentName", AgentName);
            jsonObject.put("PageNumber", PageNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.message_get_potential_obj_list);
        CallServiceTask service = new CallServiceTask(this.mContext, TAG_METHOD_NAME, jsonObject,
                Services.JSON_POST_OBJECT, message, GetPotentialObjList.this);
        service.execute();

    }

    public GetPotentialObjList(Context mContext, int Agent, String AgentName, int PageNumber,
                               String FromDate, String ToDate, int Status, String saleName) {
        this.mContext = mContext;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", saleName);
            jsonObject.put("Agent", Agent);
            jsonObject.put("AgentName", AgentName);
            jsonObject.put("PageNumber", PageNumber);
            jsonObject.put("FromDate", FromDate);
            jsonObject.put("ToDate", ToDate);
            jsonObject.put("Status", Status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.message_get_potential_obj_list);
        CallServiceTask service = new CallServiceTask(this.mContext, TAG_METHOD_NAME, jsonObject,
                Services.JSON_POST_OBJECT, message, GetPotentialObjList.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<PotentialObjModel> mList = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                if (jsObj.getInt(Constants.ERROR_CODE) == 0) {//OK not Error
                    JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        mList.add(new Gson().fromJson(jsonArray.get(i).toString(), PotentialObjModel.class));
                    }
                } else {
                    Common.alertDialog(jsObj.getString(Constants.ERROR), mContext);
                }
                loadData(mList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(ArrayList<PotentialObjModel> mList) {
        try {
            if (mContext.getClass().getSimpleName().equals(ListReportPotentialObjActivity.class.getSimpleName())) {
                ListReportPotentialObjActivity activity = (ListReportPotentialObjActivity) mContext;
                activity.loadData(mList);
            }
            if(mFragment != null){
                mFragment.loadData(mList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
