package isc.fpt.fsale.activity.cus_type_detail;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.extra_ott.adapter.KeyPairValueAdapter;
import isc.fpt.fsale.utils.Constants;

public class CusTypeDetailActivity extends BaseActivitySecond
        implements OnItemClickListener<KeyValuePairModel> {
    private RelativeLayout rltBack;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_cus_type_detail;
    }

    @Override
    protected void initView() {
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_cus_type_detail);
        ArrayList<KeyValuePairModel> lstObjType = new ArrayList<>();
        lstObjType.add(new KeyValuePairModel(12, "Hộ gia đình"));
        lstObjType.add(new KeyValuePairModel(10, "Công ty"));
        lstObjType.add(new KeyValuePairModel(11, "Học sinh, sinh viên"));
        lstObjType.add(new KeyValuePairModel(13, "Hộ kinh doanh"));
        lstObjType.add(new KeyValuePairModel(14, "Khách hàng nước ngoài"));
        lstObjType.add(new KeyValuePairModel(15, "KXD"));
        lstObjType.add(new KeyValuePairModel(16, "Ký túc xá"));
        lstObjType.add(new KeyValuePairModel(17, "Đại lý Internet"));
        KeyPairValueAdapter mAdapter = new KeyPairValueAdapter(this, lstObjType, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(KeyValuePairModel object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.KEY_PAIR_OBJ, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
