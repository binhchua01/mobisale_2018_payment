package isc.fpt.fsale.ui.maxytv.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.maxy.GetPromotionByMaxyDevice;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyPromotionDeviceAdapter;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.BOX_FIRST;
import static isc.fpt.fsale.utils.Constants.LOCAL_TYPE;
import static isc.fpt.fsale.utils.Constants.MAXY_PROMOTION_ID;
import static isc.fpt.fsale.utils.Constants.MAXY_PROMOTION_TYPE;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_PROMOTION_BOX_1;
import static isc.fpt.fsale.utils.Constants.POSITION;
import static isc.fpt.fsale.utils.Constants.REGTYPE;
import static isc.fpt.fsale.utils.Constants.TAG_CONTRACT;

public class MaxyPromotionBox1Activity extends BaseActivitySecond implements OnItemClickListener<MaxyBox> {
    private View loBack;
    private MaxyBox mMaxy;
    private List<MaxyBox> mListBoxSelected;
    private MaxyPromotionDeviceAdapter mAdapter;
    private int position;
    private String mClassName;
    private int RegType, box, maxyPromotionID,maxyPromotionType, localtype;
    private String contract;

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_device_list;
    }

    @Override
    protected void initView() {
        getDataIntent();

        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.lbl_screen_name_maxy_promotion_device_list));
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_device_list);
        mAdapter = new MaxyPromotionDeviceAdapter(this, mListBoxSelected != null ? mListBoxSelected : new ArrayList<MaxyBox>(), this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(Constants.LIST_MAXY_PROMOTION_BOX_1) != null) {
            mMaxy = bundle.getParcelable(Constants.LIST_MAXY_PROMOTION_BOX_1);
            position = bundle.getInt(POSITION);
            RegType = bundle.getInt(REGTYPE);
            localtype = bundle.getInt(LOCAL_TYPE);
            box = bundle.getInt(BOX_FIRST);
            maxyPromotionID = bundle.getInt(MAXY_PROMOTION_ID);
            maxyPromotionType = bundle.getInt(MAXY_PROMOTION_TYPE);
            contract = bundle.getString(TAG_CONTRACT);
        }
        if (bundle != null && bundle.getString(Constants.CLASS_NAME) != null) {
            mClassName = bundle.getString(Constants.CLASS_NAME);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mMaxy != null) {
            new GetPromotionByMaxyDevice(this, mMaxy.getBoxID(), RegType, box, localtype, maxyPromotionID, maxyPromotionType, contract);
        }
    }

    public void mLoadPromotionDevice(List<MaxyBox> mList) {
        this.mListBoxSelected = mList;
        mAdapter.notifyData(this.mListBoxSelected);
    }

    @Override
    public void onItemClick(MaxyBox mBox) {
        mBox.setBoxID(this.mMaxy.getBoxID());
        mBox.setBoxName(this.mMaxy.getBoxName());
        mBox.setBoxType(this.mMaxy.getBoxType());
        mBox.setBoxCount(1);
        mBox.setBoxFirst(box);
        Intent returnIntent = new Intent();
        returnIntent.putExtra(OBJECT_MAXY_PROMOTION_BOX_1, mBox);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
