package isc.fpt.fsale.ui.maxytv.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV4;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.ui.maxytv.model.MaxyExtra;
import isc.fpt.fsale.utils.Common;

public class MaxyTVExtraAdapter extends RecyclerView.Adapter<MaxyTVExtraAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<MaxyExtra> mList;
    private OnItemClickListenerV4 mListener;
    private OnItemClickListener mUpdatePromotion;

    public MaxyTVExtraAdapter(Context mContext, List<MaxyExtra> mList,
                              OnItemClickListenerV4 mListener, OnItemClickListener mUpdatePromotion) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
        this.mUpdatePromotion = mUpdatePromotion;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_list_maxytv_extra, parent, false);
        return new SimpleViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.bindView(mList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvMaxyExtraName, tvMaxyExtraPrice, tvMaxyChargeTimesLess, tvMaxyChargeTimesPlus, tvMaxyChargeTimesQuantity,
                tvPricePerUnit, tvMaxyPrepaidMonthLess, tvMaxyPrepaidMonthPlus, tvMaxyPrepaidMonthQuantity;
        ImageView imgDelete;

        SimpleViewHolder(View itemView) {
            super(itemView);
            tvMaxyExtraName = (TextView) itemView.findViewById(R.id.tv_extra_maxytv);
            tvMaxyExtraPrice = itemView.findViewById(R.id.tv_extra_maxy_price);
            tvMaxyChargeTimesLess = (TextView) itemView.findViewById(R.id.tv_maxytv_charge_times_less);
            tvMaxyChargeTimesPlus = (TextView) itemView.findViewById(R.id.tv_maxytv_charge_times_plus);
            tvMaxyChargeTimesQuantity = (TextView) itemView.findViewById(R.id.tv_maxytv_charge_times_quantity);
            tvMaxyPrepaidMonthLess = (TextView) itemView.findViewById(R.id.tv_maxytv_prepaid_month_less);
            tvMaxyPrepaidMonthPlus = (TextView) itemView.findViewById(R.id.tv_maxytv_prepaid_month_plus);
            tvMaxyPrepaidMonthQuantity = (TextView) itemView.findViewById(R.id.tv_maxytv_prepaid_month_quantity);
            tvPricePerUnit = (TextView) itemView.findViewById(R.id.tv_maxytv_extra_total);
            imgDelete = itemView.findViewById(R.id.img_delete);
        }

        void bindView(final MaxyExtra mDetail, final int position) {
            //set value default to view
            tvMaxyExtraName.setText(mDetail.getName());
            tvMaxyExtraPrice.setText(convertStringPrice(mDetail.getPrice()));
            tvMaxyChargeTimesQuantity.setText(String.valueOf(mDetail.getChargeTimes()));
            tvMaxyPrepaidMonthQuantity.setText(String.valueOf(mDetail.getPrepaidMonth()));
            tvPricePerUnit.setText(
                    calculatorTotalExtraPackage(mList.get(position).getPrice(), mList.get(position).getChargeTimes(), mList.get(position).getPrepaidMonth()));
            //set event press less or plus quantity
            tvMaxyChargeTimesLess.setOnClickListener(view -> {
                int minQuantity = 1;
                int quantity = mList.get(position).getChargeTimes();
                if (quantity == minQuantity) {
                    return;
                }
                quantity--;
                mList.get(position).setChargeTimes(quantity);
                tvPricePerUnit.setText(
                        calculatorTotalExtraPackage(mList.get(position).getPrice(), mList.get(position).getChargeTimes(), mList.get(position).getPrepaidMonth()));
                notifyDataChanged(mList);
            });

            tvMaxyChargeTimesPlus.setOnClickListener(view -> {
                int maxQuantity;
                if (mDetail.getExtraType() == 6) {
                    maxQuantity = 1;
                } else {
                    if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
                        maxQuantity = ((RegisterActivityNew) mContext).step2.countBoxOnNet();
                    }
                    else {
                        maxQuantity = ((RegisterContractActivity)mContext).getMaxyFragment().countBoxOnNet();
                    }
                }
                int quantity = mList.get(position).getChargeTimes();
                if (quantity == maxQuantity) {
                    return;
                }
                quantity++;
                mList.get(position).setChargeTimes(quantity);
                tvPricePerUnit.setText(
                        calculatorTotalExtraPackage(mList.get(position).getPrice(), mList.get(position).getChargeTimes(), mList.get(position).getPrepaidMonth()));
                notifyDataChanged(mList);
            });

            tvMaxyPrepaidMonthLess.setOnClickListener(view -> {
                int minQuantity = 0;
                int quantity = mList.get(position).getPrepaidMonth();
                if (quantity == minQuantity) {
                    return;
                }
                quantity--;
                mList.get(position).setPrepaidMonth(quantity);
                tvPricePerUnit.setText(
                        calculatorTotalExtraPackage(mList.get(position).getPrice(), mList.get(position).getChargeTimes(), mList.get(position).getPrepaidMonth()));
                notifyDataChanged(mList);
//                if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName()))
//                    ((RegisterActivityNew) mContext).step2.modelDetail.getObjectCameraOfNet().setCameraDetail(mList);
//                mUpdatePromotion.onItemClick(true);
            });

            tvMaxyPrepaidMonthPlus.setOnClickListener(view -> {
                int maxQuantity = 999;
                int quantity = mList.get(position).getPrepaidMonth();
                if (quantity == maxQuantity) {
                    return;
                }
                quantity++;
                mList.get(position).setPrepaidMonth(quantity);
                tvPricePerUnit.setText(
                        calculatorTotalExtraPackage(mList.get(position).getPrice(), mList.get(position).getChargeTimes(), mList.get(position).getPrepaidMonth()));
                notifyDataChanged(mList);
//                if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName()))
//                    ((RegisterActivityNew) mContext).step2.modelDetail.getObjectCameraOfNet().setCameraDetail(mList);
//                mUpdatePromotion.onItemClick(true);
            });

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClickV4(mDetail, position, 8);//delete
                }
            });


        }
    }

    public void notifyDataChanged(List<MaxyExtra> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    private String convertStringPrice(int cost) {
        return String.format(mContext.getString(R.string.txt_unit_price), Common.formatNumber(cost));
    }

    private String calculatorTotalExtraPackage(int cost, int quantity, int quantity2) {
        return String.format(mContext.getString(R.string.txt_unit_price), Common.formatNumber((cost * quantity * quantity2)));
    }
}
