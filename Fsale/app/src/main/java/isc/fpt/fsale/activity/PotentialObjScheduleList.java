package isc.fpt.fsale.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetAllPotentialObjSchedule;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.PotentialObjScheduleAdapter;
import isc.fpt.fsale.ui.fragment.DatePickerReportDialog;
import isc.fpt.fsale.ui.fragment.PotentialScheduleCreateDialog;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.PotentialSchedule;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import android.support.v4.widget.SwipeRefreshLayout;

public class PotentialObjScheduleList extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener, DatePickerDialog.OnDateSetListener {
    // model KHTN
    private PotentialObjModel mPotential;
    // list view danh sách lịch hẹn
    private ListView lvPotentialScheduleList;
    // refesh lại dữ liệu từ server
    private SwipeRefreshLayout swipeContainer;
    // nút tạo lịch hẹn
    private ImageView imgCreateSchedule;
    private FragmentManager fm;
    private Context mContext;
    private PotentialObjScheduleAdapter adapterPotentialSchedule;
    private ArrayList<PotentialSchedule> listPotentialSchedule;
    private TextView lblFullName, lblTotalSchedule, lblEmptyPotentialObjScheduleList;
    public static final String TAG_POTENTIAL_OBJ_SCHEDULE_OBJECT = "TAG_POTENTIAL_OBJ_SCHEDULE_OBJECT";
    public static final String TAG_SELECTED_POSITION = "TAG_SELECTED_POSITION";
    private EditText txtFromDate, txtToDate;
    private DatePickerReportDialog mDateDialog;
    private Button findPotential;
    private LinearLayout frmFind;
    private ImageView imgNavigation;
    private Spinner spPage;
    private static int FLAG_FIRST_LOAD = 0;
    private int pageNumber = 1;
    private String strFromDate = "", strToDate = "";
    private String supporter;
    private boolean isStartSearch = true;

    public PotentialObjScheduleList() {
        super(R.string.lbl_screen_name_potential_schedule_list);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(),
                getString(R.string.lbl_screen_name_potential_schedule_list));
        setContentView(R.layout.activity_potential_obj_schedule_list);
        this.mContext = this;
        getDataFromIntent();
        fm = this.getSupportFragmentManager();
        frmFind = (LinearLayout) findViewById(R.id.frm_find);
        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem != null) {
                    FLAG_FIRST_LOAD++;
                    if (FLAG_FIRST_LOAD > 1) {
                        if (pageNumber != selectedItem.getID()) {
                            pageNumber = selectedItem.getID();
                            getData(txtFromDate.getText().toString(), txtToDate.getText().toString(), pageNumber);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        imgNavigation = (ImageView) findViewById(R.id.img_navigation_drop_down);
        imgNavigation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (isStartSearch) {
                    isStartSearch = false;
                    setDateTextDefault();
                }
                dropDownNavigation();
            }
        });

        txtFromDate = (EditText) findViewById(R.id.txt_from_date);
        txtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String title = getString(R.string.title_dialog_from_date);
                initDatePickerDialog(txtFromDate, title);
                if (mDateDialog != null) {
                    mDateDialog.setCancelable(false);
                    mDateDialog.show(getSupportFragmentManager(), "datePicker");
                }
            }
        });
        txtFromDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (hasFocus) {
                    if (txtFromDate.getText().toString().equals("")) {
                        Calendar date = Calendar.getInstance();
                        date.set(Calendar.DAY_OF_MONTH, 1);
                        String s = Common.getSimpleDateFormat(date, Constants.DATE_FORMAT_VN);
                        txtFromDate.setText(s);
                    } else {
                        String title = getString(R.string.title_dialog_from_date);
                        initDatePickerDialog(txtFromDate, title);
                        if (mDateDialog != null) {
                            mDateDialog.setCancelable(false);
                            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                            ft.add(mDateDialog, "datePicker");
                            ft.commitAllowingStateLoss();
                        }
                    }
                    txtFromDate.setError(null);
                }
            }
        });
        txtToDate = (EditText) findViewById(R.id.txt_to_date);
        txtToDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String title = getString(R.string.title_dialog_to_date);
                initDatePickerDialog(txtToDate, title);
                if (mDateDialog != null) {
                    mDateDialog.setCancelable(false);
                    mDateDialog.show(getSupportFragmentManager(), "datePicker");
                }
            }
        });
        txtToDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (hasFocus) {
                    String title = getString(R.string.title_dialog_to_date);
                    initDatePickerDialog(txtToDate, title);
                    if (mDateDialog != null) {
                        mDateDialog.setCancelable(false);
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.add(mDateDialog, "datePicker");
                        ft.commitAllowingStateLoss();
                    }
                    txtToDate.setError(null);
                }
            }
        });
        findPotential = (Button) findViewById(R.id.btn_find);
        findPotential.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String newFromDate = txtFromDate.getText().toString();
                String newToDate = txtToDate.getText().toString();
                if (!strFromDate.equals(newFromDate) || !strToDate.equals(newToDate)) {
                    pageNumber = 1;
                }
                getData(newFromDate, newToDate, pageNumber);
            }
        });
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.container);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main));
        swipeContainer.setOnRefreshListener(this);
        lvPotentialScheduleList = (ListView) findViewById(R.id.lv_potential_obj_schedule_list);
        lvPotentialScheduleList.setOnItemClickListener(this);
        imgCreateSchedule = (ImageView) findViewById(R.id.img_create_schedule);
        lblFullName = (TextView) findViewById(R.id.lbl_full_name);
        lblEmptyPotentialObjScheduleList = (TextView) findViewById(R.id.lbl_empty_potential_obj_schedule_list);
        lblTotalSchedule = (TextView) findViewById(R.id.lbl_total_schedule);
        imgCreateSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PotentialScheduleCreateDialog dialog = new PotentialScheduleCreateDialog(PotentialObjScheduleList.this);
                Bundle args = new Bundle();
                args.putParcelable(PotentialObjDetailActivity.TAG_POTENTIAL_OBJECT, mPotential);
                dialog.setArguments(args);
                Common.showFragmentDialog(fm, dialog, "fragment_potential_schedule_create_dialog");
            }
        });
        if (mPotential != null) {
            lblFullName.setText(mPotential.getFullName());
        }
        getData(txtFromDate.getText().toString(), txtToDate.getText().toString(), pageNumber);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    public void getData(String fromDate, String toDate, int pageNumber) {
        new GetAllPotentialObjSchedule(this.mContext, this.supporter, fromDate, toDate, String.valueOf(mPotential.getID()), String.valueOf(pageNumber), false);
    }

    @Override
    public void onRefresh() {
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                getData(txtFromDate.getText().toString(), txtToDate.getText().toString(), pageNumber);
                swipeContainer.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(PotentialObjDetailActivity.TAG_POTENTIAL_OBJECT)) {
                mPotential = intent.getParcelableExtra(PotentialObjDetailActivity.TAG_POTENTIAL_OBJECT);
                supporter = intent.getStringExtra(PotentialObjDetailActivity.TAG_SUPPORTER);
            }
        }
    }

    public void loadPotentialScheduleList(ArrayList<PotentialSchedule> lst) {
        boolean newList = false;
        if (this.listPotentialSchedule != null) {
            if (lst.size() > 0) {
                if (this.listPotentialSchedule.size() > 0 && this.listPotentialSchedule.get(0).getTotalPage() != lst.get(0).getTotalPage() || this.listPotentialSchedule.size() == 0) {
                    newList = true;
                }
            } else {
                setNumberPage(lst);
            }
        }
        this.listPotentialSchedule = lst;
        this.lblTotalSchedule.setText(String.valueOf(lst.size()));
        adapterPotentialSchedule = new PotentialObjScheduleAdapter(mContext, this.listPotentialSchedule);
        lvPotentialScheduleList.setAdapter(adapterPotentialSchedule);
        if (this.listPotentialSchedule.size() > 0) {
            if (spPage.getAdapter() == null) {
                setNumberPage(this.listPotentialSchedule);
            } else {
                if (newList) {
                    FLAG_FIRST_LOAD = 1;
                    setNumberPage(this.getListPotentialSchedule());
                }
            }
            setVisibilityPotentialScheduleList(true);
        } else {
            if (newList) {
                FLAG_FIRST_LOAD = 1;
                setNumberPage(this.getListPotentialSchedule());
                setVisibilityPotentialScheduleList(true);
            } else {
                setVisibilityPotentialScheduleList(false);
            }
        }
    }

    public void setNumberPage(List<PotentialSchedule> lst) {
        ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
        if (lst.size() >= 1) {
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            for (int i = 1; i <= mTotalPage; i++) {
                lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
            }
        }
        KeyValuePairAdapter adapterSpage = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
        spPage.setAdapter(adapterSpage);
    }

    public PotentialObjModel getmPotential() {
        return mPotential;
    }

    public void setmPotential(PotentialObjModel mPotential) {
        this.mPotential = mPotential;
    }

    public PotentialObjScheduleAdapter getAdapterPotentialSchedule() {
        return adapterPotentialSchedule;
    }

    public void setAdapterPotentialSchedule(PotentialObjScheduleAdapter adapterPotentialSchedule) {
        this.adapterPotentialSchedule = adapterPotentialSchedule;
    }

    public ArrayList<PotentialSchedule> getListPotentialSchedule() {
        return listPotentialSchedule;
    }

    public void setListPotentialSchedule(ArrayList<PotentialSchedule> listPotentialSchedule) {
        this.listPotentialSchedule = listPotentialSchedule;
    }

    public void setVisibilityPotentialScheduleList(boolean visibility) {
        if (visibility) {
            setHideFrameSearch();
        } else {
            lblEmptyPotentialObjScheduleList.setVisibility(View.VISIBLE);
            swipeContainer.setVisibility(View.GONE);
        }
    }

    public void setHideFrameSearch() {
        swipeContainer.setVisibility(View.VISIBLE);
        lblEmptyPotentialObjScheduleList.setVisibility(View.GONE);
    }

    public TextView getLblTotalSchedule() {
        return lblTotalSchedule;
    }

    public void setLblTotalSchedule(TextView lblTotalSchedule) {
        this.lblTotalSchedule = lblTotalSchedule;
    }

    public Spinner getSpPage() {
        return spPage;
    }

    public void setSpPage(Spinner spPage) {
        this.spPage = spPage;
    }

    private void dropDownNavigation() {
        if (frmFind.getVisibility() == View.VISIBLE) {
            frmFind.setVisibility(View.GONE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
        } else {
            frmFind.setVisibility(View.VISIBLE);
            txtFromDate.setFocusable(false);
            txtToDate.setFocusable(false);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {
        try {
            Common.hideSoftKeyboard(this);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        PotentialSchedule selectedPotentialSchedule = (PotentialSchedule) parentView.getItemAtPosition(position);
        PotentialScheduleCreateDialog dialog = new PotentialScheduleCreateDialog(PotentialObjScheduleList.this);
        Bundle args = new Bundle();
        args.putParcelable(PotentialObjDetailActivity.TAG_POTENTIAL_OBJECT, mPotential);
        args.putInt(PotentialObjScheduleList.TAG_SELECTED_POSITION, position);
        args.putParcelable(PotentialObjScheduleList.TAG_POTENTIAL_OBJ_SCHEDULE_OBJECT, selectedPotentialSchedule);
        dialog.setArguments(args);
        Common.showFragmentDialog(fm, dialog, "fragment_potential_schedule_create_dialog");
    }

    private void setDateTextDefault() {
        if (txtFromDate != null || txtToDate != null) {
            Calendar date = Calendar.getInstance();
            String s = Common.getSimpleDateFormat(date, Constants.DATE_FORMAT_VN);
            txtFromDate.setText(s);
            txtToDate.setText(s);
        }
    }

    private void initDatePickerDialog(EditText txtValue, String dialogTitle) {
        Calendar minDate = Calendar.getInstance(), starDate = Calendar.getInstance(), maxDate = Calendar.getInstance();
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        minDate.set(Calendar.MONTH, 0);
        minDate.add(Calendar.YEAR, -1);
        maxDate.add(Calendar.YEAR, 2100);
        mDateDialog = new DatePickerReportDialog(this, starDate, minDate, maxDate, dialogTitle, txtValue);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        // TODO Auto-generated method stub
        Calendar date = Calendar.getInstance();
        date.set(year, monthOfYear, dayOfMonth);
        if (mDateDialog.getEditText() == txtFromDate && txtFromDate != null) {
            txtFromDate.setText(Common.getSimpleDateFormat(date,
                    Constants.DATE_FORMAT_VN));
        }
        if (mDateDialog.getEditText() == txtToDate && txtToDate != null) {
            txtToDate.setText(Common.getSimpleDateFormat(date,
                    Constants.DATE_FORMAT_VN));
        }
    }
}
