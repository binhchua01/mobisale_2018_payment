package isc.fpt.fsale.action;

import android.content.Context;
import android.view.Gravity;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/*
 * @Description: Lấy DS Hình thức thanh toán MobiSale
 * @author: DuHK
 * @create date: 	01/04/2016
 * API lấy danh sách hình thức thanh toán
 *
 * Update by haulc3 on 05/11/2019
 */

public class GetPaymentType implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private Spinner mPinner;
    private int id;
    private FragmentRegisterStep4 fragmentRegisterStep4;

    public GetPaymentType(Context mContext, FragmentRegisterStep4 fragment, RegistrationDetailModel mRegister) {
        this.mContext = mContext;
        this.fragmentRegisterStep4 = fragment;
        String message = mContext.getResources().getString(R.string.msg_pd_get_payment_method);
        String METHOD_NAME = "GetPaymentType";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME,
                mRegister.toJSONObjectGetPaymentMethodList(), Services.JSON_POST_OBJECT,
                message, GetPaymentType.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            boolean isError = false;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<KeyValuePairModel> resultObject = new WSObjectsModel<>(jsObj, KeyValuePairModel.class);
                if (resultObject.getErrorCode() != 0) {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError)
                    loadData(result);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(String result) {
        try {
            if (mContext != null) {
                Common.savePreference(mContext, Constants.SHARE_PRE_OBJECT, Constants.SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST, result);
                if (fragmentRegisterStep4 != null) {
                    fragmentRegisterStep4.loadSpinnerPaymentType();
                }
            }
            if (mPinner != null) {
                loadDataFromCachePaymentType(mContext, mPinner, id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean hasCacheDistrict(Context context) {
        return Common.hasPreference(context, Constants.SHARE_PRE_OBJECT, Constants.SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST);
    }

    private String getCachePaidType(Context context) {
        return Common.loadPreference(context, Constants.SHARE_PRE_OBJECT, Constants.SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST);
    }

    private void loadDataFromCachePaymentType(Context context, Spinner sp, int id) {
        try {
            if (hasCacheDistrict(context)) {
                String deptStr = getCachePaidType(context);
                JSONObject jObj;
                WSObjectsModel<KeyValuePairModel> wsObject = null;
                ArrayList<KeyValuePairModel> listAgent;
                try {
                    jObj = new JSONObject(deptStr);
                    jObj = jObj.getJSONObject(Constants.RESPONSE_RESULT);
                    wsObject = new WSObjectsModel<>(jObj, KeyValuePairModel.class);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (wsObject != null) {
                    listAgent = wsObject.getArrayListObject();
                } else {
                    listAgent = new ArrayList<>();
                    listAgent.add(new KeyValuePairModel(0, "[Không có dữ liệu]"));
                }
                KeyValuePairAdapter adapterStatus =
                        new KeyValuePairAdapter(context, R.layout.my_spinner_style, listAgent, Gravity.CENTER);
                sp.setAdapter(adapterStatus);
                sp.setSelection(Common.getIndex(sp, id), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}