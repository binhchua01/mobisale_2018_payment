package isc.fpt.fsale.ui.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import isc.fpt.fsale.R;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by haulc3 on 12,July,2019
 */
public abstract class BaseActivitySecond extends AppCompatActivity {

    @Nullable
    View layoutContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        Fabric.with(this, new Crashlytics());
        logUser();
        initView();
        initEvent();
        layoutContainer = findViewById(R.id.container);
        if(layoutContainer != null){
            hiddenKeySoft(layoutContainer);
        }
    }

    protected abstract void initEvent();

    protected abstract int getLayoutResourceId();

    public void showError(String errorMessage) {
        Common.alertDialog(errorMessage, this);
    }

    protected abstract void initView();

    private void logUser() {
        // You can call any combination of these three methods
        MyApp myApp = ((MyApp) getApplicationContext());
        DeviceInfo info = new DeviceInfo(getApplicationContext());
        Crashlytics.setUserIdentifier(info.GetDeviceIMEI(this));
        if (myApp != null) {
            // You can call any combination of these three methods
            Crashlytics.setUserName(myApp.getUserName());
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    protected void hiddenKeySoft(View view) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    hideKeyboardOutside(view.getContext(), view);
                    view.clearFocus();
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hiddenKeySoft(innerView);
            }
        }
    }

    /**
     * hidden soft keybroad when un-focus edit text
     */
    protected void hideKeyboardOutside(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
