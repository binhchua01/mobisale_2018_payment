package isc.fpt.fsale.activity;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.ViewPagerPromotionAdImageDetailAdapter;
import isc.fpt.fsale.ui.fragment.FragmentPromotionAdImageDetail;
import isc.fpt.fsale.model.PromotionBrochureModel;
import isc.fpt.fsale.model.ZoomOutPageTransformer;
import isc.fpt.fsale.utils.Constants;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

public class PromotionAdImageViewActivity extends FragmentActivity {
    public static final String ARG_PROMOTION_BROCHURE_LIST = "ARG_PROMOTION_BROCHURE_LIST";
    public static final String ARG_BUNDLE = "ARG_BUNDLE";
    public static final String ARG_SELECTED_POSITION = "ARG_SELECTED_POSITION";
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private ArrayList<PromotionBrochureModel> mList;
    private int mSelectedPosition = -1;
    //private Bundle mBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_promotion_image_view);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            mList = intent.getParcelableArrayListExtra(ARG_PROMOTION_BROCHURE_LIST);
            mSelectedPosition = intent.getIntExtra(ARG_SELECTED_POSITION, -1);
            //mBundle = getIntent().getBundleExtra(ARG_BUNDLE);
            if (mList != null) {
                ArrayList<Bundle> bundles = new ArrayList<Bundle>();
                for (PromotionBrochureModel promotionModel : mList) {
                    Bundle item = new Bundle();
                    item.putParcelable(FragmentPromotionAdImageDetail.ARG_PROMOTION_OBJECT, promotionModel);
                    if (mSelectedPosition >= 0)
                        item.putInt(ARG_SELECTED_POSITION, mSelectedPosition);
                    bundles.add(item);
                }
                initViewPagerAdapter(bundles);
            }
        }
    }

    private void initViewPagerAdapter(ArrayList<Bundle> bundles) {
        mPagerAdapter = new ViewPagerPromotionAdImageDetailAdapter(getSupportFragmentManager(), bundles);
        mPager.setAdapter(mPagerAdapter);
        if (mSelectedPosition >= 0)
            mPager.setCurrentItem(mSelectedPosition);
    	
    	/* Bundle imageBundle = new Bundle(), videoBundle = new Bundle();    
    	 imageBundle.putInt(FragmentPromotionImageList.ARG_MEDIA_TYPE, FragmentPromotionImageList.TAG_MEDIA_IMAGE_TYPE);
    	 videoBundle.putInt(FragmentPromotionImageList.ARG_MEDIA_TYPE, FragmentPromotionImageList.TAG_MEDIA_VIDEO_TYPE);
    	 mPagerAdapter = new ViewPagerPromotionAdAdapter(getSupportFragmentManager(), imageBundle, videoBundle);
    	 mPager.setAdapter(mPagerAdapter);*/
    }


}
