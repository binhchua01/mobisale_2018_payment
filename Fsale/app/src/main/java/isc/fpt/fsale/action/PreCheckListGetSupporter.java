package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.model.AutoPreCheckListInfo;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by HCM.TUANTT14 on 7/4/2018.
 */

public class PreCheckListGetSupporter implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private CreatePreChecklistActivity createPreChecklistActivity;

    public PreCheckListGetSupporter(Context context, CreatePreChecklistActivity createPreChecklistActivity, String[] paramsValue) {
        this.mContext = context;
        this.createPreChecklistActivity = createPreChecklistActivity;
        String[] arrParamName = new String[]{"ObjID"};
        String message = mContext.getResources().getString(R.string.msg_process_pre_checklist_get_supporter);
        String PRE_CHECK_LIST_GET_SUPPORTER = "PreCheckList_GetSupporter";
        CallServiceTask service = new CallServiceTask(mContext, PRE_CHECK_LIST_GET_SUPPORTER,
                arrParamName, paramsValue, Services.JSON_POST, message, PreCheckListGetSupporter.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<AutoPreCheckListInfo> lst;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                String TAG_RESULT_ID = "ResultID";
                String TAG_RESULT = "Result";
                if (jsObj.has(TAG_RESULT_ID) && jsObj.getInt(TAG_RESULT_ID) < 0) {
                    Common.alertDialog(jsObj.getString(TAG_RESULT), mContext);
                } else {
                    if (jsObj.has(TAG_RESULT_ID) && jsObj.getInt(TAG_RESULT_ID) > 0) {
                        WSObjectsModel<AutoPreCheckListInfo> resultObject = new WSObjectsModel<>(jsObj, AutoPreCheckListInfo.class);
                        lst = resultObject.getArrayListObject();
                        if (lst!= null && lst.size() > 0) {
                            AutoPreCheckListInfo autoPreCheckListInfo = lst.get(0);
                            createPreChecklistActivity.loadSupportInfo(autoPreCheckListInfo);
                        } else {
                            Common.alertDialog(jsObj.getString(TAG_RESULT), mContext);
                        }
                    } else {
                        Common.alertDialog(jsObj.getString(TAG_RESULT), mContext);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
