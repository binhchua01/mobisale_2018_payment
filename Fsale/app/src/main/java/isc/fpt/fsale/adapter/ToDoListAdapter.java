package isc.fpt.fsale.adapter;

import isc.fpt.fsale.action.UpdateConfirmNotification;
import isc.fpt.fsale.activity.ListTodoListDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentCustomerCareToDoList;
import isc.fpt.fsale.model.CustomerCareSubDetailModel;
import isc.fpt.fsale.model.ParseJsonModel;
import isc.fpt.fsale.model.ToDoListModel;
import isc.fpt.fsale.utils.Common;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ToDoListAdapter  extends BaseAdapter{
	private List<ToDoListModel> mList;	
	private Context mContext;
	private FragmentCustomerCareToDoList mFragment;
	
	public ToDoListAdapter(Context context, List<ToDoListModel> lst, FragmentCustomerCareToDoList fragment){
		this.mContext = context;
		this.mList = lst;	
		this.mFragment = fragment;
		//
		lstHolders = new ArrayList<ViewHolder>();
        startUpdateTimer();
	}
	
	public void setListData(List<ToDoListModel> lst){
		this.mList.clear();
		this.mList.addAll(lst);
		this.notifyDataSetChanged();		
	}
	
	public void addAll(List<ToDoListModel> lst){
		this.mList.addAll(lst);
		this.notifyDataSetChanged();		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	
	@SuppressWarnings("deprecation")
	@SuppressLint({ "InflateParams", "DefaultLocale", "NewApi" })
	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ToDoListModel item = mList.get(position);
		ViewHolder holder = null;
		
		if (view == null) {
			view = LayoutInflater.from(mContext).inflate(R.layout.row_customer_care_todolist_item, null);
			
			final int sdk = android.os.Build.VERSION.SDK_INT;
			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				view.setBackgroundDrawable( mContext.getResources().getDrawable(R.drawable.selector_list_row_layout) );
			} else {
				view.setBackground( mContext.getResources().getDrawable(R.drawable.selector_list_row_layout));
			}
			view.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(mContext, ListTodoListDetailActivity.class);
					intent.putExtra("TODOLIST_ITEM", item);
					mContext.startActivity(intent);
				}
			});
			
			
			holder = new ViewHolder();
			holder.lblDivisionName = (TextView) view.findViewById(R.id.lbl_division_name);
			
			
			holder.lblObjStatusName = (TextView) view.findViewById(R.id.lbl_obj_status_name);
				
			
			//holder.lblObjStatusDesc = (TextView) convertView.findViewById(R.id.lbl_obj_status_desc);
			

			holder.lblFullName = (TextView) view.findViewById(R.id.lbl_full_name);
			
			
			holder.lblLevelName = (TextView) view.findViewById(R.id.lbl_level_name);
			
			/*final int sdk = android.os.Build.VERSION.SDK_INT;
					
					if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
						frmTitle.setBackgroundDrawable(drwTitle);
					} else {
						frmTitle.setBackground(drwTitle);
					}*/
			holder.lblEmail = (TextView) view.findViewById(R.id.lbl_email);
			

			holder.lblPhoneNumber = (TextView) view.findViewById(R.id.lbl_phone);
			
			
			holder.lblStartDate = (TextView) view.findViewById(R.id.lbl_start_date);
			holder.lblProcessTime = (TextView) view.findViewById(R.id.lbl_process_time);
			
			holder.lblTimer = (TextView) view.findViewById(R.id.lbl_timer);		
			//Add by: DuHK 15-12-2015
			holder.lblEndDate = (TextView) view.findViewById(R.id.lbl_end_date);
			holder.lblStatusName = (TextView) view.findViewById(R.id.lbl_status_name);
			holder.lblProcessTimeEnd = (TextView) view.findViewById(R.id.lbl_process_time_end);
			holder.lblRowNumber = (TextView) view.findViewById(R.id.lbl_row_number);
			
			//
			holder.frmSaleInfo = (LinearLayout)view.findViewById(R.id.frm_sale_info);
			holder.lblSaleEmail = (TextView)view.findViewById(R.id.lbl_sale_email);
			holder.lblSaleFullName = (TextView)view.findViewById(R.id.lbl_sale_full_name);
			holder.lblSaleName = (TextView)view.findViewById(R.id.lbl_sale_name);
			holder.lblSalePhone= (TextView)view.findViewById(R.id.lbl_sale_phone);
			holder.wv = (WebView)view.findViewById(R.id.web_view);
			holder.btnUpdate = (Button)view.findViewById(R.id.btn_comfirm_info);
			holder.btnUpdate.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String Note = null;
					String code = item.getCode();
					try {
						JSONObject json = new JSONObject(item.getObjStatusDesc());
						CustomerCareSubDetailModel desc = ParseJsonModel.parse(json, CustomerCareSubDetailModel.class);
						code = desc.getConfirm_code();
					} catch (Exception e) {
						// TODO: handle exception

						code = item.getCode();
						e.printStackTrace();
					}
					new UpdateConfirmNotification(mContext, code,  Note, mFragment);
				}
			});
			//
			holder.frmDesc = (LinearLayout)view.findViewById(R.id.frm_desc);
			holder.lblDescCusFeedback = (TextView)view.findViewById(R.id.lbl_desc_customer_feedback);
			holder.lblDescSurveyManNote = (TextView)view.findViewById(R.id.lbl_desc_survey_man_note);
			holder.lblDescSurveyTask = (TextView)view.findViewById(R.id.lbl_desc_survey_task);
			
			view.setTag(holder);
            synchronized (lstHolders) {
                lstHolders.add(holder);
            }
		}else{
			 holder = (ViewHolder) view.getTag();			 
		}
		holder.updateTimer(item);	
		loadDataHolder(holder, item);		
		//holder.updateTimer(item);				
		return view;
	}

	@SuppressLint("DefaultLocale")
	private void loadDataHolder(ViewHolder holder, ToDoListModel item){
		if(item.getDivisionName() != null){
			holder.lblDivisionName.setText(item.getDivisionName());			
		}
		if(item.getStartDate() != null)
			holder.lblStartDate.setText(item.getStartDate());	
		if(item.getObjStatusName() != null)
			holder.lblObjStatusName.setText(item.getObjStatusName());
		/*if(item.getObjStatusDesc() != null)
			holder.lblObjStatusDesc.setText(item.getObjStatusDesc());	*/
		if(item.getFullName() != null)
			holder.lblFullName.setText(item.getFullName());	
		int color;
		if(item.getLevelName() != null){
			holder.lblLevelName.setText(item.getLevelName());	
			switch (item.getLevelID()) {
				case 1:
					color = mContext.getResources().getColor(R.color.theme_color);
					break;
				case 2:
					color = mContext.getResources().getColor(R.color.orange);
					break;
				case 3:
					color = mContext.getResources().getColor(R.color.red);
					break;
				default:
					color = mContext.getResources().getColor(R.color.theme_color);
					break;
			}
			holder.lblLevelName.setTextColor(color);				
		}			
		if(item.getEmail() != null){
			holder.lblEmail.setText(item.getEmail());
			if(Common.checkMailValid(item.getEmail())){
				String a = "<a href=\"mailto:" + item.getEmail().trim() + "\">"+ item.getEmail() +"</a>";
				holder.lblEmail.setText(Html.fromHtml(a));
				holder.lblEmail.setMovementMethod(LinkMovementMethod.getInstance());			
			}
		}
		if(item.getPhoneNumber() != null){
			holder.lblPhoneNumber.setText(item.getPhoneNumber());
			if(!item.getPhoneNumber().trim().equals("")){
				String a = "<a href=\"tel:" + item.getPhoneNumber().trim() + "\">"+ item.getPhoneNumber() +"</a>";
				holder.lblPhoneNumber.setText(Html.fromHtml(a));
				holder.lblPhoneNumber.setMovementMethod(LinkMovementMethod.getInstance());	
			}
		}
		if(item.getProcessTime() > 0){
			holder.lblProcessTime.setText(String.valueOf(item.getProcessTime()));
			try {
				if(item.getProcessTime() > 0){
					int seconds = item.getProcessTime() * 60;
					 int hr = seconds/3600;
					  int rem = seconds%3600;
					  int mn = rem/60;
					  int sec = rem%60;					 
					  holder.lblProcessTime.setText(String.format("%02d:%02d:%02d",  hr, mn, sec));
				}else{
					holder.lblProcessTime.setText("");
				}
			} catch (Exception e) {
				// TODO: handle exception

				e.printStackTrace();
			}			
		}
		
		//Add by: DuHK, 17-12-2015
		if(item.getEndDate() != null){
			holder.lblEndDate.setText(item.getEndDate());
		}
		
		if(item.getStatusName() != null){
			holder.lblStatusName.setText(item.getStatusName());
		}
		
		if(item.getEndDateTime() != null && item.getStartDateTime() != null){
			try {
				Calendar calEndDate = Calendar.getInstance(), calStartDate = Calendar.getInstance();
				calEndDate.setTime(item.getEndDateTime());  
				calStartDate.setTime(item.getStartDateTime());
				long ProcessTime = calEndDate.getTimeInMillis() - calStartDate.getTimeInMillis();
				String am = "";
				if(ProcessTime < 0){
					ProcessTime = -ProcessTime;
	    			am = "-";
	    		}
				String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(ProcessTime),
		    		    TimeUnit.MILLISECONDS.toMinutes(ProcessTime) % TimeUnit.HOURS.toMinutes(1),
		    		    TimeUnit.MILLISECONDS.toSeconds(ProcessTime) % TimeUnit.MINUTES.toSeconds(1));		    	
		    	holder.lblProcessTimeEnd.setText(am + hms);	    	
			} catch (Exception e) {

				e.printStackTrace();
			}					
		}else{
			holder.lblProcessTimeEnd.setText("");
		}
		if(item.getRowNumber() > 0){
			holder.lblRowNumber.setText("" +item.getRowNumber() + ". ");
		}
		
		//
		if(item.getDivisionID() == 82){
			holder.frmSaleInfo.setVisibility(View.VISIBLE);
			if(item.getSaleName() != null)
				holder.lblSaleName.setText(item.getSaleName());
			if(item.getSaleFullName() != null)
				holder.lblSaleFullName.setText(item.getSaleFullName());
			if(item.getSaleEmail() != null)
				holder.lblSaleEmail.setText(item.getSaleEmail());
			if(item.getSalePhone() != null)
				holder.lblSalePhone.setText(item.getSalePhone());
			if(item.getObjStatusDesc() != null)
				holder.wv.loadData(item.getObjStatusDesc(), "text/html; charset=utf-8","UTF-8");	
		}else{
			holder.frmSaleInfo.setVisibility(View.GONE);
		}
		try {
			holder.wv.loadData(item.getObjStatusDesc(), "text/html; charset=utf-8","UTF-8");				
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
//
		
		try {
			holder.wv.setVisibility(View.GONE);
			holder.frmDesc.setVisibility(View.VISIBLE);
			JSONObject json = new JSONObject(item.getObjStatusDesc());
			CustomerCareSubDetailModel desc = ParseJsonModel.parse(json, CustomerCareSubDetailModel.class);
			holder.lblDescCusFeedback.setText(desc.getCsat() + " (CSAT = " + desc.getPoint() + ")");
			holder.lblDescSurveyManNote.setText(desc.getNote());
			holder.lblDescSurveyTask.setText(desc.getTime() +" - Mã KS: " + desc.getCode());
			int drawID = 0, point = 0;
			try {
				point = Integer.valueOf(desc.getPoint());
			} catch (Exception e) {
				// TODO: handle exception

				e.printStackTrace();
				point = -1;
			}
			switch (point) {
			case 1:
				drawID = R.drawable.ic_cast_01;
				break;
			case 2:
				drawID = R.drawable.ic_cast_02;
				break;
			case 3:
				drawID = R.drawable.ic_cast_03;
				break;
			case 4:
				drawID = R.drawable.ic_cast_04;
				break;
			case 5:
				drawID = R.drawable.ic_cast_05;
				break;

			default:
				break;
			}
			
			if(drawID > 0){
				try {
					//holder.imgCastPoint.setImageResource(drawID);
					holder.lblDescCusFeedback.setCompoundDrawablesWithIntrinsicBounds(
							0, 0, drawID, 0);
				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}
				
			}
		} catch (Exception e) {
			// TODO: handle exception

			holder.frmDesc.setVisibility(View.GONE);
			holder.wv.setVisibility(View.VISIBLE);
			holder.wv.loadData(item.getObjStatusDesc(), "text/html; charset=utf-8","UTF-8");				
		}
	}
	//
	private List<ViewHolder> lstHolders;
	private Handler mHandler = new Handler();
    private Runnable updateRemainingTimeRunnable = new Runnable() {
        @Override
        public void run() {
            synchronized (lstHolders) {
                long currentTime = System.currentTimeMillis();
                for (ViewHolder holder : lstHolders) {
                	holder.updateTimeRemaining(currentTime);
                }
            }
        }
    };
    
    private void startUpdateTimer() {
        Timer tmr = new Timer();
        tmr.schedule(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(updateRemainingTimeRunnable);
            }
        }, 1000, 1000);
    }
	
	private class ViewHolder {
		TextView lblDivisionName, lblObjStatusName, /*lblObjStatusDesc,*/ 
			lblFullName, lblLevelName, lblEmail, lblPhoneNumber, lblStartDate, lblProcessTime, lblTimer,
			lblEndDate, lblStatusName, lblProcessTimeEnd, lblRowNumber;
	    ToDoListModel todoItem;
	    //
	    LinearLayout frmSaleInfo, frmDesc;
	    TextView lblSaleName, lblSaleFullName, lblSaleEmail, lblSalePhone,
	    		lblDescSurveyTask, lblDescCusFeedback, lblDescSurveyManNote;
	    WebView wv;
	    Button btnUpdate;
	    
	    public void updateTimer(ToDoListModel item) {	  
	    	todoItem = item;
        	updateTimeRemaining(System.currentTimeMillis());	       
	    }
	    	    	    
	    @SuppressLint("DefaultLocale")
		public void updateTimeRemaining(long currentTime) {
	    	if(todoItem != null){	    		
		    	try {
		    		String hms = "00:00:00";
		    		if(todoItem.getStatus() == 0){
			    		long millisUntilFinished = 0;
			    		Calendar cal = Calendar.getInstance();
			    		cal.setTime(todoItem.getStartDateTime());  	  
			    		cal.add(Calendar.MINUTE, todoItem.getProcessTime());
			    		String am = "";
			    		millisUntilFinished = cal.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
			    		if(millisUntilFinished < 0){
			    			millisUntilFinished = -millisUntilFinished;
			    			am = "-";
			    		}
			    		
				    	hms = am +  String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
				    		    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
				    		    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));				    	
		    		}
			    	if(lblTimer != null)
			    		lblTimer.setText(hms);	    	
				} catch (Exception e) {

					e.printStackTrace();
				}	    			
	    	}
	    	
	    }
	}
}
