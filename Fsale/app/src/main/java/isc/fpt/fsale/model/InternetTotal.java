package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by haulc3 on 05,November,2019
 */
public class InternetTotal implements Parcelable {
    private String Message;
    private int ResultID;
    private List<ServiceCode> ServiceCodes;
    private int Total;

    public InternetTotal() {
    }

    private InternetTotal(Parcel in) {
        Message = in.readString();
        ResultID = in.readInt();
        ServiceCodes = in.createTypedArrayList(ServiceCode.CREATOR);
        Total = in.readInt();
    }

    public static final Creator<InternetTotal> CREATOR = new Creator<InternetTotal>() {
        @Override
        public InternetTotal createFromParcel(Parcel in) {
            return new InternetTotal(in);
        }

        @Override
        public InternetTotal[] newArray(int size) {
            return new InternetTotal[size];
        }
    };

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getResultID() {
        return ResultID;
    }

    public void setResultID(int resultID) {
        ResultID = resultID;
    }

    public List<ServiceCode> getServiceCodes() {
        return ServiceCodes;
    }

    public void setServiceCodes(List<ServiceCode> serviceCodes) {
        ServiceCodes = serviceCodes;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Message);
        parcel.writeInt(ResultID);
        parcel.writeTypedList(ServiceCodes);
        parcel.writeInt(Total);
    }
}
