package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 7/10/2018.
 */

public class TimeZone implements Parcelable {
    private int AbilityDesc;
    private String ColorCode;
    private String TimezoneText;
    private String TimezoneValue;

    public TimeZone(String timezoneText, String timezoneValue, int abilityDesc, String colorCode) {
        TimezoneText = timezoneText;
        TimezoneValue = timezoneValue;
        AbilityDesc = abilityDesc;
        ColorCode = colorCode;
    }

    public String getTimezoneText() {
        return TimezoneText;
    }

    public void setTimezoneText(String timezoneText) {
        TimezoneText = timezoneText;
    }

    public String getTimezoneValue() {
        return TimezoneValue;
    }

    public void setTimezoneValue(String timezoneValue) {
        TimezoneValue = timezoneValue;
    }

    public int getAbilityDesc() {
        return AbilityDesc;
    }

    public void setAbilityDesc(int abilityDesc) {
        AbilityDesc = abilityDesc;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    protected TimeZone(Parcel in) {
        TimezoneText = in.readString();
        TimezoneValue = in.readString();
        AbilityDesc = in.readInt();
        ColorCode = in.readString();
    }

    public static final Creator<TimeZone> CREATOR = new Creator<TimeZone>() {
        @Override
        public TimeZone createFromParcel(Parcel in) {
            return new TimeZone(in);
        }

        @Override
        public TimeZone[] newArray(int size) {
            return new TimeZone[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(TimezoneText);
        parcel.writeString(TimezoneValue);
        parcel.writeInt(AbilityDesc);
        parcel.writeString(ColorCode);
    }
}
