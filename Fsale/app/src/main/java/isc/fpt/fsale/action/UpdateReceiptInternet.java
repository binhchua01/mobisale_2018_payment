package isc.fpt.fsale.action;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

// API cập nhật tổng tiền phiếu thu điện tử
public class UpdateReceiptInternet implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    public final String TAG_METHOD_NAME = "UpdateReceiptInternet";
    public static String[] paramNames = new String[]{
            "UserName",
            "RegCode",
            "NewLocalType",
            "NewPromotionID",
            "DepositBlackPoint",
            "DepositContract",
            "Total",
            "AcceptExcessMoney",
            "GiftID",
            "GiftName"
    };
    private RegistrationDetailModel mRegister;

    public UpdateReceiptInternet(Context context, RegistrationDetailModel register, int LocalTypeID,
                                 int PromotionID, int DepositBlackPoint, int DepositContract, double Total,
                                 int AcceptExcessMoney) {
        mContext = context;
        this.mRegister = register;
        String message = "Đang cập nhật...";
        String[] paramValues = new String[]{
                ((MyApp) mContext.getApplicationContext()).getUserName(),
                mRegister.getRegCode(),
                String.valueOf(LocalTypeID),
                String.valueOf(PromotionID),
                String.valueOf(DepositBlackPoint),
                String.valueOf(DepositContract),
                String.valueOf(Total),
                String.valueOf(AcceptExcessMoney),
                String.valueOf(mRegister.getGiftID()),
                mRegister.getGiftName()
        };
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, UpdateReceiptInternet.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<UpdResultModel> lst = null;
            boolean isError = false;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(jsObj, UpdResultModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                } else {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError)
                    if (lst != null && lst.size() > 0) {
                        if (lst.get(0).getResultID() > 0) {
                            AlertDialog.Builder builder;
                            Dialog dialog;
                            builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("Thông báo")
                                    .setMessage(lst.get(0).getResult()).setCancelable(false)
                                    .setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            try {
                                                new GetRegistrationDetail(
                                                        mContext,
                                                        ((MyApp) mContext.getApplicationContext()).getUserName(),
                                                        mRegister.getID()
                                                );
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                            dialog = builder.create();
                            dialog.show();
                        } else {
                            Common.alertDialog(lst.get(0).getResult(), mContext);
                        }
                    } else{
                        Common.alertDialog("Không có dữ liệu trả về!", mContext);
                    }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
