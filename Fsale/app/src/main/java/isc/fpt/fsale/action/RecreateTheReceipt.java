package isc.fpt.fsale.action;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PayContractActivity;
import isc.fpt.fsale.model.PaymentInformationPOST;
import isc.fpt.fsale.model.RecreateTheReceiptResult;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

/**
 * Created by HCM.TUANTT14 on 4/27/2018.
 */

// API tạo lại khoản thu thanh toán online bán mới
public class RecreateTheReceipt implements AsyncTaskCompleteListener<String> {
    private final String RE_CREATE_THE_RECEIPT = "RecreateTheReceipt";
    private Context mContext;
    private String[] arrParamName, arrParamValue;
    private PaymentInformationPOST paymentInformationPOST;

    public RecreateTheReceipt(Context mContext, PaymentInformationPOST mObject) {
        this.mContext = mContext;
        this.paymentInformationPOST = mObject;
        arrParamName = new String[]{"SaleName", "RegCode"};
        arrParamValue = new String[]{mObject.getSaleName(), mObject.getRegCode()};
        String message = mContext.getResources().getString(
                R.string.msg_pd_recreate_the_receipt);
        CallServiceTask service = new CallServiceTask(mContext,
                RE_CREATE_THE_RECEIPT, arrParamName, arrParamValue,
                Services.JSON_POST, message, RecreateTheReceipt.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        {
            try {
                ArrayList<RecreateTheReceiptResult> objList = null;
                boolean isError = false;
                if (result != null && Common.jsonObjectValidate(result)) {
                    JSONObject jsObj = new JSONObject(result);
                    if (jsObj != null) {
                        jsObj = jsObj.getJSONObject("RecreateTheReceiptResult");
                        WSObjectsModel<RecreateTheReceiptResult> resultObject = new WSObjectsModel<RecreateTheReceiptResult>(jsObj, RecreateTheReceiptResult.class);
                        if (resultObject != null) {
                            if (resultObject.getErrorCode() == 0) {
                                objList = resultObject.getArrayListObject();
                            } else {
                                isError = true;
                                Common.alertDialog(resultObject.getError(), mContext);
                            }
                        }
                    }
                    if (!isError) {
                        if (objList.size() > 0) {
                            RecreateTheReceiptResult recreateTheReceiptResult = objList.get(0);
                            if (recreateTheReceiptResult.getResultID() == 1) {
                                new PaymentInformation(this.mContext, paymentInformationPOST);
                            }
                            Common.alertDialog(objList.get(0).getResult(), mContext);
                        } else {
                            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
                        }

                    }
                }
            } catch (JSONException ex) {
                Log.i("RecreateTheReceipt_onTaskComplete:", ex.getMessage());
                Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
            }
        }
    }
}
