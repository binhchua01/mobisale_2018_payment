package isc.fpt.fsale.activity;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListDeploymentProgress;
import isc.fpt.fsale.adapter.DeploymentProgressAdapter;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ListDeploymentProgressModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

// màn hình TIẾN ĐỘ TRIỂN KHAI
public class DeploymentProgressActivity extends BaseActivity {

    private ArrayList<ListDeploymentProgressModel> lstDeloy;
    public ListView lvDeploy;
    private DeploymentProgressAdapter adapter;
    private Context mContext;
    private ImageView btnFind;
    private EditText txtContract;

    // Phan trang
    private String PageSize, ParamName, SearchType;
    private String[] arrayParam;
    private int PAGE_COUNT = 1;
    public int iCurrentPage = 1;
    public Spinner SpPage, SpSearchType;
    private Button btnPre, btnNext;


    public DeploymentProgressActivity() {
        //super(R.id.both);
        // TODO Auto-generated constructor stub
        super(R.string.lbl_deployment_progress);
        MyApp.setCurrentActivity(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_deployment_progress_activity));
        setContentView(R.layout.list_deployment_progress);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        mContext = DeploymentProgressActivity.this;
        txtContract = (EditText) findViewById(R.id.txt_contract_num_deploy);
        btnFind = (ImageView) findViewById(R.id.btn_find_contract_deploy);
        SpSearchType = (Spinner) findViewById(R.id.sp_search_type);
        lvDeploy = (ListView) findViewById(R.id.lv_deployment_list);
        // Phân trang
        SpPage = (Spinner) this.findViewById(R.id.sp_page_num);
        btnPre = (Button) this.findViewById(R.id.btn_previous);
        btnNext = (Button) this.findViewById(R.id.btn_next);
        loadSearchType();
        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                lstDeloy = myIntent.getParcelableArrayListExtra("list_deployment");

                if (lstDeloy != null) {
                    adapter = new DeploymentProgressAdapter(mContext, lstDeloy);
                    lvDeploy.setAdapter(adapter);
                }
                PageSize = myIntent.getStringExtra("PageSize");
                arrayParam = myIntent.getStringArrayExtra("ArrayParam");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtContract.getText() == null || txtContract.getText().toString().trim().equals("")) {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_search_value_empty), mContext);
                    return;
                }
                if (!Common.isEmpty(txtContract))
                    new GetListDeploymentProgress(mContext, Constants.USERNAME, SearchType, txtContract.getText().toString(), String.valueOf(iCurrentPage), true);
                else
                    new GetListDeploymentProgress(mContext, Constants.USERNAME, "0", "0", String.valueOf(iCurrentPage), true);
            }
        });


        String[] PageSizeModel = PageSize.split(";");
        int CurrentPage = Integer.parseInt(PageSizeModel[0]);
        int TotalPage = Integer.parseInt(PageSizeModel[1]);
        PAGE_COUNT = TotalPage;
        iCurrentPage = CurrentPage;
        ArrayList<KeyValuePairModel> ListSpPage = new ArrayList<KeyValuePairModel>();
        for (int i = 1; i <= TotalPage; i++) {
            String GT = String.valueOf(i);
            ListSpPage.add(new KeyValuePairModel(i, GT));
        }
        KeyValuePairAdapter adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
        SpPage.setAdapter(adapterList);
        SpPage.setSelection(Common.getIndex(SpPage, CurrentPage), true);
        SpPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() != iCurrentPage) {
                    iCurrentPage = selectedItem.getID();
                    new GetListDeploymentProgress(mContext, arrayParam[0], arrayParam[1], arrayParam[2], String.valueOf(iCurrentPage), true);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        SpSearchType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                SearchType = selectedItem.getsID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        btnPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iCurrentPage--;
                CheckEnable();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iCurrentPage++;
                CheckEnable();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void CheckEnable() {
        String iCurrentPageIndex = String.valueOf(iCurrentPage);
        if (PAGE_COUNT == 1) {
            btnPre.setEnabled(false);
            btnNext.setEnabled(false);
        } else {
            if (iCurrentPage >= PAGE_COUNT) {
                btnNext.setEnabled(false);
                btnPre.setEnabled(true);
                if (iCurrentPage == PAGE_COUNT) {
                    new GetListDeploymentProgress(mContext, arrayParam[0], arrayParam[1], arrayParam[2], iCurrentPageIndex, true);

                }
            } else if (iCurrentPage <= 1) {
                btnPre.setEnabled(false);
                btnNext.setEnabled(true);
                if (iCurrentPage == 1) {
                    new GetListDeploymentProgress(mContext, arrayParam[0], arrayParam[1], arrayParam[2], iCurrentPageIndex, true);

                }
            } else {
                btnPre.setEnabled(true);
                btnNext.setEnabled(true);
                new GetListDeploymentProgress(mContext, arrayParam[0], arrayParam[1], arrayParam[2], iCurrentPageIndex, true);

            }
        }
    }

    // Load dữ liệu loại tìm kiếm
    private void loadSearchType() {
        ArrayList<KeyValuePairModel> lstSearchType = new ArrayList<KeyValuePairModel>();
        lstSearchType.add(new KeyValuePairModel("1", "Số HD"));
        lstSearchType.add(new KeyValuePairModel("2", "Tên KH"));
        lstSearchType.add(new KeyValuePairModel("3", "Phone"));
        lstSearchType.add(new KeyValuePairModel("4", "Số TTKH"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstSearchType, Gravity.LEFT);
        SpSearchType.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
