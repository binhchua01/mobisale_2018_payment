package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.database.MessageTable;
import isc.fpt.fsale.model.MessageModel;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ChatAdapter extends BaseAdapter {
	public List<MessageModel> mList;
	private Context mContext;
	//private int[] colors = new int[] { 0xFFCC99, 0x30f0efda };
	
	public ChatAdapter(Context context, List<MessageModel> lst){
		this.mContext = context;
		this.mList = lst;
		}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		final MessageModel mess = mList.get(position);
		if(mess != null){
			mess.setIsRead(true);
			//Set lay out cho nguoi mess do nguoi gui va nguoi nhận
			try {
				if(mess.getSendfrom().equals(Constants.USERNAME))
					view = LayoutInflater.from(mContext).inflate(R.layout.row_chat_right, null);
				else
					view = LayoutInflater.from(mContext).inflate(R.layout.row_chat_left, null);
			} catch (Exception e) {

				e.printStackTrace();
				Log.i("ChatAdapter.getView()", e.getMessage());
			}
			TextView lblMessage = (TextView)view.findViewById(R.id.lbl_chat_message);
			TextView lblDateTime = (TextView)view.findViewById(R.id.lbl_chat_date_time);
			TextView lblTitle = (TextView)view.findViewById(R.id.lbl_chat_title_message);
			if(mess.getMessage()!= null)
				lblMessage.setText(mess.getMessage());
			if(mess.getDatetime() != null)
				lblDateTime.setText("\n"+mess.getDatetime());
			if(mess.getTitle() != null && !mess.getTitle().trim().equals("")){
				lblTitle.setText(mess.getTitle());
			}
			try {
				MessageTable mTable = new MessageTable(mContext);
				mTable.update(mess);
			} catch (Exception e) {
				//TODO: xoa Toast khi chay that

				e.printStackTrace();
				Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
			}
		}else{
			view = LayoutInflater.from(mContext).inflate(R.layout.row_chat_right, null);
		}
		
		return view;
	}
	
	

}
