package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionBrochureModel;
import isc.fpt.fsale.utils.Constants;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.PlayerStyle;
import com.google.android.youtube.player.YouTubePlayerView;

public class VideoYoutubeActivity extends YouTubeBaseActivity implements
        YouTubePlayer.OnInitializedListener {

    private static final int RECOVERY_DIALOG_REQUEST = 1;
    public static final String ARG_BUNDLE = "ARG_BUNDLE";
    public static final String DEVELOPER_KEY = "AIzaSyDzr6WAprYxMKkROnfTnu2h1yxtaScHhZM";
    /*public static final String YOUTUBE_VIDEO_CODE = "rxKjkAPqKXs";*/
    public static final String ARG_PROMOTION_BROCHURE_OBJECT = "TAG_PROMOTION_OBJECT";

    // YouTube player view
    private YouTubePlayerView youTubeView;

    private PromotionBrochureModel mPromotionObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube);
        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            mPromotionObj = intent.getBundleExtra(ARG_BUNDLE).getParcelable(ARG_PROMOTION_BROCHURE_OBJECT);
            if (mPromotionObj != null) {
                youTubeView.initialize(DEVELOPER_KEY, this);
            }
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult errorReason) {
        try {
            if (errorReason.isUserRecoverableError()) {
                errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
            } else {
                String errorMessage = String.format(getString(R.string.error_player), errorReason.toString());
                Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

            e.printStackTrace();
            String errorMessage = String.format(getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {

            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically
            player.loadVideo(mPromotionObj.getMediaUrl());
            // Hiding player controls
            player.setPlayerStyle(PlayerStyle.DEFAULT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            //getYouTubePlayerProvider().initialize(DEVELOPER_KEY, this);
        }
    }
}
