package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haulc3 on 19,July,2019
 */
public class LocalType implements Parcelable {
    @SerializedName("LocalTypeID")
    @Expose
    private Integer localTypeID;
    @SerializedName("LocalTypeName")
    @Expose
    private String localTypeName;

    public LocalType(Integer localTypeID, String localTypeName) {
        this.localTypeID = localTypeID;
        this.localTypeName = localTypeName;
    }

    protected LocalType(Parcel in) {
        if (in.readByte() == 0) {
            localTypeID = null;
        } else {
            localTypeID = in.readInt();
        }
        localTypeName = in.readString();
    }

    public static final Creator<LocalType> CREATOR = new Creator<LocalType>() {
        @Override
        public LocalType createFromParcel(Parcel in) {
            return new LocalType(in);
        }

        @Override
        public LocalType[] newArray(int size) {
            return new LocalType[size];
        }
    };

    public Integer getLocalTypeID() {
        return localTypeID;
    }

    public void setLocalTypeID(Integer localTypeID) {
        this.localTypeID = localTypeID;
    }

    public String getLocalTypeName() {
        return localTypeName;
    }

    public void setLocalTypeName(String localTypeName) {
        this.localTypeName = localTypeName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (localTypeID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(localTypeID);
        }
        dest.writeString(localTypeName);
    }
}
