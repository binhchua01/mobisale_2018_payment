package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportPayTVActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportItemModel;
import isc.fpt.fsale.model.ReportPayTVModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetReportPayTV implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetReportPayTV";
	private String[] paramValues;
	//public static final String[] paramNames = new String[]{"UserName","FromDate","ToDate","Agent", "AgentName","PageNumber"};
	public static final String[] paramNames = new String[]{"UserName","Month", "Year", "Agent", "AgentName", "PageNumber","FromDate","ToDate"};
	//
	private int month, year;
	private String fromDate, toDate;
	public GetReportPayTV(Context context, int Month, int Year, int Agent, String AgentName, int PageNumber, String FromDate,String ToDate) {	
		mContext = context;
		String UserName = "";
		this.month = Month;
		this.year = Year;
		this.fromDate = FromDate;
		this.toDate = ToDate;
		if(mContext != null)
			UserName = ((MyApp)mContext.getApplicationContext()).getUserName();
		this.paramValues = new String[]{UserName,String.valueOf(Month), String.valueOf(Year), String.valueOf(Agent), AgentName, String.valueOf(PageNumber),fromDate, toDate};			
		execute();
	}
	
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportPayTV.this);
		service.execute();		
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			ArrayList<ReportPayTVModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ReportPayTVModel> resultObject = 
						 new WSObjectsModel<ReportPayTVModel>(jsObj, ReportPayTVModel.class, ReportItemModel.class.getName());
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();					
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetCustomerCareList:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	private void loadData(ArrayList<ReportPayTVModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListReportPayTVActivity.class.getSimpleName())){
				ListReportPayTVActivity activity = (ListReportPayTVActivity)mContext;
				activity.loadData(lst,fromDate,toDate, month, year);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetCustomerCareList.loadRpCodeInfo()", e.getMessage());
		}
	}
}
