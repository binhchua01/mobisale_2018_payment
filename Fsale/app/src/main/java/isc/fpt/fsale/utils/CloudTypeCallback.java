package isc.fpt.fsale.utils;

import isc.fpt.fsale.model.AllCloudType;

/**
 * Created by Hau Le on 2018-12-26.
 */
public interface CloudTypeCallback {
    void CallbackCloudTypeSelected(AllCloudType obj);
}
