package isc.fpt.fsale.model;

import isc.fpt.fsale.adapter.PotentialObjSurveyListAdapter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PotentialObjSurveyModel implements Parcelable {
    @SerializedName("SurveyID")
    @Expose
    private int SurveyID;
    @SerializedName("SurveyName")
    @Expose
    private String SurveyName;
    @SerializedName("SurveyDesc")
    @Expose
    private String SurveyDesc;
    @SerializedName("Type")
    @Expose
    private int Type;
    @SerializedName("Values")
    @Expose
    private List<PotentialObjSurveyValueModel> Values;
    @SerializedName("Category")
    @Expose
    private int Category;
    @SerializedName("CategoryName")
    @Expose
    private String CategoryName;
    @SerializedName("RowNumber")
    @Expose
    private int RowNumber;

    public int getSurveyID() {
        return SurveyID;
    }

    public void setSurveyID(int SurveyID) {
        this.SurveyID = SurveyID;
    }

    public String getSurveyName() {
        return SurveyName;
    }

    public void setSurveyName(String SurveyName) {
        this.SurveyName = SurveyName;
    }

    public String getSurveyDesc() {
        return SurveyDesc;
    }

    public void setSurveyDesc(String SurveyDesc) {
        this.SurveyDesc = SurveyDesc;
    }

    public int getType() {
        return Type;
    }

    public void setType(int Type) {
        this.Type = Type;
    }

    public List<PotentialObjSurveyValueModel> getValues() {
        return Values;
    }

    public void setValues(List<PotentialObjSurveyValueModel> Values) {
        this.Values = Values;
    }

    public int getCategory() {
        return Category;
    }

    public void setCategory(int Category) {
        this.Category = Category;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String CategoryName) {
        this.CategoryName = CategoryName;
    }

    public int getRowNumber() {
        return RowNumber;
    }

    public void setRowNumber(int RowNumber) {
        this.RowNumber = RowNumber;
    }

    public String toString() {
        JSONObject jsonObj = new JSONObject();
        try {
            Field[] fields = getClass().getDeclaredFields();
            for (Field f : fields) {
                f.setAccessible(true);
                jsonObj.put(f.getName(), f.get(this));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj.toString();
    }

    private PotentialObjSurveyModel(Parcel in) {
        this.SurveyDesc = in.readString();
        this.SurveyID = in.readInt();
        this.SurveyName = in.readString();
        this.Type = in.readInt();
        this.Values = new ArrayList<>();
        in.readList(Values, null);
        this.Category = in.readInt();
        this.CategoryName = in.readString();
        this.RowNumber = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.SurveyDesc);
        out.writeInt(this.SurveyID);
        out.writeString(this.SurveyName);
        out.writeInt(this.Type);
        out.writeList(this.Values);
        out.writeInt(this.Category);
        out.writeString(this.CategoryName);
        out.writeInt(this.RowNumber);
    }

    public static final Creator<PotentialObjSurveyModel> CREATOR = new Creator<PotentialObjSurveyModel>() {
        @Override
        public PotentialObjSurveyModel createFromParcel(Parcel source) {
            return new PotentialObjSurveyModel(source);
        }

        @Override
        public PotentialObjSurveyModel[] newArray(int size) {
            return new PotentialObjSurveyModel[size];
        }
    };

    public JSONObject toJSONObject() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("SurveyID", this.SurveyID);
            JSONArray arr = new JSONArray();
            if (this.Type != PotentialObjSurveyListAdapter.TAG_SURVEY_TYPE_EDIT_TEXT) {
                for (PotentialObjSurveyValueModel item : this.Values) {
                    if (item.getSelected() > 0) {
                        arr.put(item.toJSONObject());
                    }
                }
            } else {
                if (this.Values.size() > 0) {
                    arr.put(Values.get(0).toJSONObject());
                }
            }
            jsonObj.put("Values", arr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }
}