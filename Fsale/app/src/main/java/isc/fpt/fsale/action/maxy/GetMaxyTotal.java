package isc.fpt.fsale.action.maxy;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.activity.ListReportPotentialObjTotalActivity;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.camera319.Camera2Total;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractMaxy;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.ui.maxytv.model.MaxyTotal;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by nhannh26 on 23/03/2021
 */
public class GetMaxyTotal implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterStep4 fragment;
    private FragmentRegisterContractMaxy fragment_contract;

    public GetMaxyTotal(Context mContext, JSONObject jsonObjParam, FragmentRegisterStep4 fragment) {
        this.fragment = fragment;
        this.mContext = mContext;
        String message = "Đang tính tổng tiền MaxyTV, Vui lòng chờ...";
        String GET_MAXY_TOTAL = "GetMaxyTotal";
        CallServiceTask service = new CallServiceTask(mContext, GET_MAXY_TOTAL, jsonObjParam,
                Services.JSON_POST_OBJECT, message, GetMaxyTotal.this);
        service.execute();
    }

    public GetMaxyTotal(Context mContext, JSONObject jsonObjParam, FragmentRegisterContractMaxy fragment) {
        this.fragment_contract = fragment;
        this.mContext = mContext;
        String message = "Đang tính tổng tiền MaxyTV, Vui lòng chờ...";
        String GET_MAXY_TOTAL = "GetMaxyTotal";
        CallServiceTask service = new CallServiceTask(mContext, GET_MAXY_TOTAL, jsonObjParam,
                Services.JSON_POST_OBJECT, message, GetMaxyTotal.this);
        service.execute();
    }


    @Override
    public void onTaskComplete(String result) {
        try {
            List<MaxyTotal> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), MaxyTotal.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (fragment!=null) {
                        fragment.loadMaxyPrice(lst.get(0));
                    }
                    else {
                        ((RegisterContractActivity) mContext)
                                .getTotalFragment().loadMaxyPrice(lst.get(0));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
