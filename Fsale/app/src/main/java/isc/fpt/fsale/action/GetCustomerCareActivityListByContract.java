package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.CustomerCareDetailActivity;

import isc.fpt.fsale.ui.fragment.FragmentCustomerCareActivityList;
import isc.fpt.fsale.model.ActivityByContractModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;

public class GetCustomerCareActivityListByContract implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    public final String TAG_METHOD_NAME = "GetActivityListByContract";
    private String[] paramNames, paramValues;
    private FragmentCustomerCareActivityList fragment;

    public GetCustomerCareActivityListByContract(Context context, FragmentCustomerCareActivityList fragment,
                                                 String Contract, int PageNumber) {
        this.mContext = context;
        this.paramNames = new String[]{"Contract", "PageNumber"};
        this.paramValues = new String[]{Contract, String.valueOf(PageNumber)};
        this.fragment = fragment;
        execute();
    }


    public void execute() {
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues,
                Services.JSON_POST, message, GetCustomerCareActivityListByContract.this);
        service.execute();
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onTaskComplete(String result) {
        try {
            boolean isError = false;
            List<ActivityByContractModel> lst = null;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<ActivityByContractModel> resultObject = new WSObjectsModel<>(jsObj, ActivityByContractModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                } else {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }

                if (!isError) {
                    loadData(lst);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(List<ActivityByContractModel> lst) {
        if (mContext.getClass().getSimpleName().equals(CustomerCareDetailActivity.class.getSimpleName())) {
            if (fragment != null) {
                fragment.loadData(lst);
            }
        }
    }
}
