package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.voucher_list.VoucherListActivity;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.Voucher;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 29,October,2019
 */
public class GetListEVoucher implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public GetListEVoucher(Context mContext, RegistrationDetailModel mRegistrationDetailModel) {
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.msg_pd_get_list_e_voucher);
        String GET_VOUCHER_LIST = "GetVouchers";
        CallServiceTask service = new CallServiceTask(mContext, GET_VOUCHER_LIST,
                mRegistrationDetailModel.toJSONObjectGetListEVoucher(), Services.JSON_POST_OBJECT,
                message, GetListEVoucher.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<Voucher> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), Voucher.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        ((VoucherListActivity) mContext).loadData(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
