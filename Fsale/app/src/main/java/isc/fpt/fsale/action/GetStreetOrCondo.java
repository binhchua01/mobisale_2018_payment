package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.activity.street_or_condo.StreetOrCondoActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.SharedPref;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.Gravity;
import android.widget.Spinner;

/*
 * ACTION: 	 	 CusInfo_GetStreetOrCondominiumList
 *
 * @description: - call api 'GetStreetOrCondominiumList' to get street/condo name
 * @author: vandn, on 02/12/2013
 */
// API lấy danh sách đường, chung cư.
public class GetStreetOrCondo implements AsyncTaskCompleteListener<String> {
    public static int condo = 1;
    public static int street = 0;
    private final String GET_STREET_CONDO = "GetStreetOrCondominiumList";
    private Context mContext;
    private Spinner spStreet;
    private String selectedValue;
    //create by haulc3
    private String district;
    private String ward;
    private int type;
    private StreetOrCondoActivity activity;

    public GetStreetOrCondo(Context mContext, String sDistrict, String sWard, int type, Spinner sp, String selectedValue) {
        this.mContext = mContext;
        this.spStreet = sp;
        this.selectedValue = selectedValue;
        String[] params = new String[]{"LocationID", "District", "Ward", "Type"};
        String[] arrParams = new String[]{Constants.LST_REGION.get(0).getsID(), sDistrict, sWard, String.valueOf(type)};
        //call service
        String message;
        if (type == 0) {
            message = mContext.getResources().getString(R.string.msg_pd_get_info_street);
        } else {
            message = mContext.getResources().getString(R.string.msg_pd_get_info_apparment);
        }
        CallServiceTask service = new CallServiceTask(mContext, GET_STREET_CONDO,
                params, arrParams, Services.JSON_POST, message, GetStreetOrCondo.this);
        service.execute();
    }

    public GetStreetOrCondo(StreetOrCondoActivity activity, String sDistrict, String sWard, int type) {
        this.activity = activity;
        this.district = sDistrict;
        this.ward = sWard;
        this.type = type;
        String[] params = new String[]{"LocationID", "District", "Ward", "Type"};
        String[] arrParams = new String[]{Constants.LOCATION_ID, sDistrict, sWard, String.valueOf(type)};
        //call service
        String message;
        if (type == 0) {//get street
            message = activity.getResources().getString(R.string.msg_pd_get_info_street);
        } else {//get condo
            message = activity.getResources().getString(R.string.msg_pd_get_info_apparment);
        }
        CallServiceTask service = new CallServiceTask(activity, GET_STREET_CONDO,
                params, arrParams, Services.JSON_POST, message, GetStreetOrCondo.this);
        service.execute();
    }

    private void handleGetWardsResult(String json) {
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if (!Common.isEmptyJSONObject(jsObj))
                bindData(jsObj);
        }
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        String TAG_GET_STREET_CONDO_RESULT = "GetStreetOrCondominiumListMethodPostResult";
        try {
            jsArr = jsObj.getJSONArray(TAG_GET_STREET_CONDO_RESULT);
            int l = jsArr.length();
            ArrayList<KeyValuePairModel> lstStreet;
            if (l > 0) {
                String TAG_ERROR = "ErrorService";
                String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                if (error.equals("null")) {
                    lstStreet = new ArrayList<>();
                    for (int i = 0; i < l; i++) {
                        JSONObject iObj = jsArr.getJSONObject(i);
                        String TAG_NAME = "NameVN";
                        String TAG_ID = "Id";
                        if (iObj.has(TAG_ID)) {
                            lstStreet.add(new KeyValuePairModel(iObj.getString(TAG_ID), iObj.getString(TAG_NAME)));
                        } else {
                            lstStreet.add(new KeyValuePairModel(iObj.getString(TAG_NAME), iObj.getString(TAG_NAME)));
                        }
                    }
                    try {
                        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                                android.R.layout.simple_spinner_item, lstStreet, Gravity.RIGHT);
                        spStreet.setAdapter(adapter);
                        try {
                            int index = Common.getIndex(spStreet, selectedValue);
                            if (index < 0) {
                                index = Common.getIndexDesc(spStreet, selectedValue);
                            }
                            if (index >= 0) {
                                spStreet.setSelection(index, true);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            } else {
                lstStreet = new ArrayList<>();
                lstStreet.add(new KeyValuePairModel("", "[ Danh sách trống ]"));
                KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                        android.R.layout.simple_spinner_item, lstStreet, Gravity.RIGHT);
                spStreet.setAdapter(adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        if (activity != null) {
            //added by haulc3
            handleGetStreetOrCondo(result);
        } else {
            //old
            handleGetWardsResult(result);
        }
    }

    private void handleGetStreetOrCondo(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                String TAG_GET_STREET_OR_CONDO_RESULT = "GetStreetOrCondominiumListMethodPostResult";
                JSONObject jsObj = new JSONObject(result);
                JSONArray jsonArray = jsObj.getJSONArray(TAG_GET_STREET_OR_CONDO_RESULT);
                if (jsonArray.length() > 0) {
                    SharedPref.put(activity, district + ward + type, result);
                    if (activity != null) {
                        activity.loadStreetOrCondo();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
