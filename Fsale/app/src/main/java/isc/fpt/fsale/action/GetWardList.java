package isc.fpt.fsale.action;

import android.content.Context;
import android.view.Gravity;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.activity.ward.WardActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.SharedPref;

/*
 * ACTION: 	 	 GetWardList
 *
 * @description: - call api "GetWardList" to get all wards of selected district
 * - handle response result: transfer response result to cusInfo screen
 * @author: vandn, on 03/12/2013
 */
// API lấy danh sách Phường(Xã)
public class GetWardList implements AsyncTaskCompleteListener<String> {
    private final String GET_WARDS = "GetWardList";
    private Spinner spWards;
    private String wardId;
    private Context mContext;
    private String districtID;
    private WardActivity activity;

    public GetWardList(Context mContext, String sDistrict, Spinner sp) {
        this.mContext = mContext;
        this.spWards = sp;
        districtID = sDistrict;
        if (sDistrict == null || sDistrict.equals(""))
            sDistrict = " ";
        String[] params = new String[]{"LocationID", "District"};
        String[] arrParams = new String[]{Constants.LOCATION_ID, sDistrict};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_ward);
        CallServiceTask service = new CallServiceTask(mContext, GET_WARDS, params,
                arrParams, Services.JSON_POST, message, GetWardList.this);
        service.execute();
    }

    public GetWardList(Context mContext, String sDistrict, Spinner sp, String wardId) {
        this.mContext = mContext;
        this.spWards = sp;
        this.wardId = wardId;
        districtID = sDistrict;
        if (sDistrict == null || sDistrict.equals(""))
            sDistrict = " ";
        String[] params = new String[]{"LocationID", "District"};
        String[] arrParams = new String[]{Constants.LOCATION_ID, sDistrict};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_ward);
        CallServiceTask service = new CallServiceTask(mContext, GET_WARDS, params,
                arrParams, Services.JSON_POST, message, GetWardList.this);
        service.execute();
    }

    public GetWardList(WardActivity activity, String district) {
        this.activity = activity;
        this.districtID = district;
        String[] params = new String[]{"LocationID", "District"};
        String[] arrParams = new String[]{Constants.LOCATION_ID, district};
        String message = activity.getResources().getString(R.string.msg_pd_get_info_ward);
        CallServiceTask service = new CallServiceTask(activity, GET_WARDS,
                params, arrParams, Services.JSON_POST, message, GetWardList.this);
        service.execute();
    }

    private void handleGetWardsResult(String json) {
        try {
            if (Common.jsonObjectValidate(json)) {
                Common.savePreference(mContext, Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID, json);
                if (spWards != null)
                    loadDataFromCacheDistrict(mContext, spWards, districtID, wardId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        if (activity != null) {
            //update by haulc3
            handleWardResult(result);
        } else {
            //old
            handleGetWardsResult(result);
        }
    }

    private void handleWardResult(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                String TAG_GET_WARD_RESULT = "GetWardListMethodPostResult";
                JSONObject jsObj = new JSONObject(result);
                JSONArray jsonArray = jsObj.getJSONArray(TAG_GET_WARD_RESULT);
                if (jsonArray.length() > 0) {
                    SharedPref.put(activity, districtID, result);
                    if (activity != null) {
                        activity.loadWardList();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static boolean hasCacheWard(Context context, String districtID) {
        return Common.hasPreference(context, Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID);
    }

    private static void loadDataFromCacheDistrict(Context context, Spinner sp, String districtID, String id) {
        try {
            ArrayList<KeyValuePairModel> lst = new ArrayList<>();
            if (hasCacheWard(context, districtID)) {
                final String TAG_GET_WARDS_RESULT = "GetWardListMethodPostResult";
                final String TAG_NAME = "Name";
                final String TAG_NAME_VN = "NameVN";
                final String TAG_ERROR = "ErrorService";
                String jsonStr = Common.loadPreference(context, Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID);

                try {
                    JSONObject json = new JSONObject(jsonStr);
                    JSONArray jsArr;
                    jsArr = json.getJSONArray(TAG_GET_WARDS_RESULT);
                    if(jsArr != null){
                        for (int i = 0; i < jsArr.length(); i++) {
                            String code = "", error = "", desc = "";
                            JSONObject item = jsArr.getJSONObject(i);

                            if (item.has(TAG_ERROR)){
                                error = item.getString(TAG_ERROR);
                            }
                            if (item.isNull(TAG_ERROR) || error.equals("null")) {
                                if (item.has(TAG_NAME)){
                                    code = item.getString(TAG_NAME);
                                }
                                if(item.has(TAG_NAME_VN)){
                                    desc = item.getString(TAG_NAME_VN);
                                }
                                lst.add(new KeyValuePairModel(code, desc));
                            }else{
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(context, R.layout.my_spinner_style, lst, Gravity.RIGHT);
                sp.setAdapter(adapterStatus);
                int index = Common.getIndex(sp, id);
                if (index > 0)
                    sp.setSelection(index, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
