package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.ResetFPTPlayModel;
import isc.fpt.fsale.model.upsell.response.ResendShortlinkModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// API kết nối Reset FPT Play
public class MobiSaleResetFPTPlay implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    //Constructor
    public MobiSaleResetFPTPlay(Context context, RegistrationDetailModel mRegister) {
        try {
            this.mContext = context;
            String[] arrParamName = new String[]{"UserName", "Contract", "ObjID", "PackageID"};
            String[] arrParamValue = new String[]{mRegister.getUserName(), mRegister.getContract(),
                    String.valueOf(mRegister.getObjID()), String.valueOf(mRegister.getObjectMaxy().getMaxyGeneral().getPackageID())};
            String message = mContext.getResources().getString(R.string.msg_pd_reset_fpt_play);
            String MOBISALE_RESET_FPT_PLAY = "ResendSMSContractAccount";
            CallServiceTask service = new CallServiceTask(mContext, MOBISALE_RESET_FPT_PLAY, arrParamName,
                    arrParamValue, Services.JSON_POST, message, MobiSaleResetFPTPlay.this);
            service.execute();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<ResetFPTPlayModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ResetFPTPlayModel.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    Common.alertDialog(String.valueOf(lst.get(0).getMessage()), mContext);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
