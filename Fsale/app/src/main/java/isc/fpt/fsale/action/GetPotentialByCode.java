package isc.fpt.fsale.action;

import android.content.Context;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.activity.CreatePotentialCEMObjActivity;
import isc.fpt.fsale.activity.ListNotificationActivity;
import isc.fpt.fsale.model.CEMCodeResultObjectModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetPotentialByCode implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private String mUserName;

    public GetPotentialByCode(Context context, String UserName, String Code, String potentialObjID) {
        this.mContext = context;
        this.mUserName = UserName;
        String[] paramNames = new String[]{"UserName", "Code", "PotentialObjID"};
        String[] paramValues = new String[]{UserName, Code, potentialObjID};
        String message = "Đang lấy thông tin code...";
        String TAG_METHOD_NAME = "GetPotentialByCode";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues,
                Services.JSON_POST, message, GetPotentialByCode.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<CEMCodeResultObjectModel> lst;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<CEMCodeResultObjectModel> resultObject = new WSObjectsModel<>(jsObj, CEMCodeResultObjectModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                    checkResultCode(lst);
                } else {//ServiceType Error
                    Common.alertDialog(resultObject.getError(), mContext);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkResultCode(List<CEMCodeResultObjectModel> lst) {
        if (lst != null && lst.size() > 0) {
            CEMCodeResultObjectModel obj = lst.get(0);
            if (obj != null) {
                if (obj.getResultID() > 0) {
                    new GetPotentialObjDetail(mContext, mUserName, obj.getID());
                } else {
                    if (mContext != null && (mContext.getClass().getSimpleName()
                            .equals(CreatePotentialCEMObjActivity.class.getSimpleName()))) {
                        Common.alertDialog(obj.getResult(), mContext);
                    } else {
                        Intent intent = new Intent(mContext, ListNotificationActivity.class);
                        intent.putExtra("resultAcceptCode", obj.getResult());
                        mContext.startActivity(intent);
                    }
                }
            }
        }
    }
}