package isc.fpt.fsale.model;

import android.annotation.SuppressLint;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class WSObjectsModel<T> {

    private List<T> listObject;
    private ArrayList<T> arrayListObject;
    private int errorCode = -1;
    private String error = null;
    private static final String ERROR_CODE_TAG = "ErrorCode";
    private static final String ERROR_TAG = "Error";
    private static final String LIST_OBJECT = "ListObject";
    private static final String LIST_PACKET = "ListPackage";
    private JSONObject json = new JSONObject();

    public WSObjectsModel() {

    }

    @SuppressLint("LongLogTag")
    public WSObjectsModel(JSONObject json, Class<T> out) {
        this.json = json;
        try {
            if (json != null) {
                if (json.has(ERROR_CODE_TAG))
                    this.errorCode = json.getInt(ERROR_CODE_TAG);
                if (json.has(ERROR_TAG))
                    this.error = json.getString(ERROR_TAG);
                if (json.has(LIST_OBJECT) && !json.isNull(LIST_OBJECT)) {
                    this.listObject = new ArrayList<T>();
                    for (int i = 0; i < json.getJSONArray(LIST_OBJECT).length(); i++) {
                        JSONObject item = json.getJSONArray(LIST_OBJECT).getJSONObject(i);
                        this.listObject.add(ParseJsonModel.parse(item, out));
                    }
                }
            }
        } catch (JSONException e) {
            Log.i("ObjectsModel_ObjectsModel()", e.getMessage());
            e.printStackTrace();
        }
    }

    @SuppressLint("LongLogTag")
    public WSObjectsModel(JSONObject json, Class<T> out, String subClass) {
        this.json = json;
        try {
            if (json != null) {
                if (json.has(ERROR_CODE_TAG))
                    this.errorCode = json.getInt(ERROR_CODE_TAG);
                if (json.has(ERROR_TAG))
                    this.error = json.getString(ERROR_TAG);
                if (json.has(LIST_OBJECT) && !json.isNull(LIST_OBJECT)) {
                    this.listObject = new ArrayList<T>();
                    for (int i = 0; i < json.getJSONArray(LIST_OBJECT).length(); i++) {
                        JSONObject item = json.getJSONArray(LIST_OBJECT).getJSONObject(i);
                        this.listObject.add(ParseJsonModel.parse(item, out, subClass));
                    }
                }
            }
        } catch (JSONException e) {
            Log.i("ObjectsModel_ObjectsModel()", e.getMessage());
            e.printStackTrace();
        }
    }

    @SuppressLint("LongLogTag")
    public WSObjectsModel(JSONObject json, Class<T> out, String subClassPackage,
                          String subClassDevice, String subClassListDeviceOTT,
                          String subClassListMacOTT, String subClassCheckListRegistration) {
        this.json = json;
        try {
            if (json != null) {
                if (json.has(ERROR_CODE_TAG))
                    this.errorCode = json.getInt(ERROR_CODE_TAG);
                if (json.has(ERROR_TAG))
                    this.error = json.getString(ERROR_TAG);
                if (json.has(LIST_OBJECT) && !json.isNull(LIST_OBJECT)) {
                    this.listObject = new ArrayList<T>();
                    for (int i = 0; i < json.getJSONArray(LIST_OBJECT).length(); i++) {
                        JSONObject item = json.getJSONArray(LIST_OBJECT).getJSONObject(i);
                        this.listObject.add(ParseJsonModel.parse(item, out, subClassPackage,
                                subClassDevice, subClassListDeviceOTT, subClassListMacOTT,
                                subClassCheckListRegistration));
                    }
                }
            }
        } catch (JSONException e) {
            Log.i("ObjectsModel_ObjectsModel()", e.getMessage());
            e.printStackTrace();
        }
    }

    @SuppressLint("LongLogTag")
    public WSObjectsModel(JSONObject json, Class<T> out, String subClass, boolean flagReg) {
        this.json = json;
        try {
            if (json != null) {
                if (json.has(ERROR_CODE_TAG))
                    this.errorCode = json.getInt(ERROR_CODE_TAG);
                if (json.has(ERROR_TAG))
                    this.error = json.getString(ERROR_TAG);
                if (json.has(LIST_OBJECT) && !json.isNull(LIST_OBJECT)) {
                    this.listObject = new ArrayList<T>();
                    for (int i = 0; i < json.getJSONArray(LIST_PACKET).length(); i++) {
                        JSONObject item = json.getJSONArray(LIST_PACKET).getJSONObject(i);
                        this.listObject.add(ParseJsonModel.parse(item, out, subClass));
                    }
                }
            }
        } catch (JSONException e) {
            Log.i("ObjectsModel_ObjectsModel()", e.getMessage());
            e.printStackTrace();
        }
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getError() {
        return this.error;
    }

    public List<T> getListObject() {
        return this.listObject;
    }

    public JSONObject getJsonObject() {
        return json;
    }

    public ArrayList<T> getArrayListObject() {
        if (listObject != null) {
            arrayListObject = new ArrayList<T>();
            for (T t : listObject) {
                arrayListObject.add(t);
            }
        } else
            arrayListObject = null;
        return this.arrayListObject;
    }

    public String toString() {
        JSONObject jsonObj = new JSONObject();
        try {
            Field[] fields = getClass().getDeclaredFields();
            for (Field f : fields) {
                f.setAccessible(true);
                jsonObj.put(f.getName(), f.get(this));
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return jsonObj.toString();
    }

}
