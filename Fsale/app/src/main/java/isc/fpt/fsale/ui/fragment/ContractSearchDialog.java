package isc.fpt.fsale.ui.fragment;

//import isc.fpt.fsale.action.GetContractInfo;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * DIALOG FRAGMENT: SearchDialog
 *
 * @Description: contract search dialog
 * @author: vandn, on 26/07/2013
 */
public class ContractSearchDialog extends DialogFragment {

    private Spinner spRegion;
    private EditText txtContractNum;
    private Button btnSearch;
    private ImageButton btnClose;
    private Context mContext;

    private KeyValuePairAdapter adapterLocationList;
    //private String sSelectedLocation;
    //private String sContractNum;
    //private final String TAG_DEPOSIT = "DepositActivity";

    public ContractSearchDialog() {
    }

    @SuppressLint("ValidFragment")
    public ContractSearchDialog(Context context, Class<?> mclass) {
        this.mContext = context;
        //this.mClass = mclass;
    }

    /**
     * The system calls this to get the DialogFragment's layout, regardless
     * of whether it's being displayed as a dialog or an embedded fragment.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View view = inflater.inflate(R.layout.dialog_search_contract, container);
        spRegion = (Spinner) view.findViewById(R.id.sp_region);
        txtContractNum = (EditText) view.findViewById(R.id.txt_contract_num);
        btnSearch = (Button) view.findViewById(R.id.btn_search);
        btnClose = (ImageButton) view.findViewById(R.id.btn_close);

        // bind hint to edit text contract num (add by thaoltt - 15.10.2013)
        txtContractNum.setText(Constants.LST_REGION.get(0).getHint());
        int textLength = txtContractNum.getText().length();
        txtContractNum.setSelection(textLength, textLength);

        adapterLocationList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, Constants.LST_REGION, Gravity.LEFT);
        spRegion.setAdapter(adapterLocationList);
        spRegion.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }

        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Common.isEmpty(txtContractNum) == false) {
                    //sContractNum = txtContractNum.getText().toString();
                    //String[] sParams = {Constants.USERNAME, sSelectedLocation,sContractNum};
                    //new GetContractInfo(mContext, sParams, ContractSearchDialog.this);
                } else
                    Common.alertDialog(getResources().getString(R.string.msg_key_search_blank), mContext);
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return view;
    }

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }
}