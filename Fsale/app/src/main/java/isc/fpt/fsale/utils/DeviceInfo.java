package isc.fpt.fsale.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.provider.Settings.Secure;

import org.apache.http.conn.util.InetAddressUtils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

/*
 * Class: 			DEVICE INFO
 *
 * @description: get device infomation: sim imei, device imei, android sdk version, model number
 * @author: vandn
 * @created on: 	13/08/2013
 */
//Model chứa thông tin thiết bị
public class DeviceInfo {
    public static String DEVICE_IMEI;
    public static String SIM_IMEI;
    public static int ANDROID_SDK_VERSION;
    public static String ANDROID_OS_VERSION;
    public static String MODEL_NUMBER;
    private TelephonyManager mTelephonyMgr;

    public DeviceInfo(Context mContext) {
        if (mContext == null) mContext = MyApp.getAppContext();
        this.mTelephonyMgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        SIM_IMEI = GetSimImei(mContext);
        if (Build.VERSION.SDK_INT >= 29) {//android 10 tro len
            DEVICE_IMEI = getAndroidID(mContext);
        } else {
            DEVICE_IMEI = GetDeviceIMEI(mContext);
        }
        //imei a Binh
//        DEVICE_IMEI = "863886032436238";
//        SIM_IMEI = "452048817472034";
//        DEVICE_IMEI = "356581040387069";
//        SIM_IMEI = "452048889043778";
//        DEVICE_IMEI = "354021084345089";
//        SIM_IMEI = "452040003970309";
//        DEVICE_IMEI = "354021084345089";
//        SIM_IMEI = "-1";
        ANDROID_SDK_VERSION = getDeviceAndroidSDKVersion();
        MODEL_NUMBER = getDeviceModelNumber();
        ANDROID_OS_VERSION = getDeviceAndroidOSVersion();
    }

    @SuppressLint("HardwareIds")
    private String getAndroidID(Context mContext) {
        return Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID);
    }

    private String GetSimImei(Context mContext) {
        try {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                return "";
            }
            @SuppressLint("HardwareIds")
            String simIMEI = mTelephonyMgr.getSubscriberId();
            if (TextUtils.isEmpty(simIMEI)) {
                return "-1";
            } else {
                return simIMEI;
            }
        } catch (Exception e) {
            return "-1";
        }
    }

    @SuppressLint("HardwareIds")
    public String GetDeviceIMEI(Context mContext) {
        try {
            if (ActivityCompat.checkSelfPermission(mContext,
                    Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return "";
            }
            String deviceImei = mTelephonyMgr.getDeviceId(); //*** use for mobiles
            if (deviceImei == null)
                deviceImei = Build.SERIAL; //*** use for tablets

            if (TextUtils.isEmpty(deviceImei)) {
                return "-1";
            } else {
                return deviceImei;
            }
        } catch (Exception e) {
            return "-1";
        }
    }

    private int getDeviceAndroidSDKVersion() {
        int androidVer;
        try {
            androidVer = Build.VERSION.SDK_INT;
        } catch (Exception e) {

            androidVer = -1;
        }
        return androidVer;
    }

    private String getDeviceModelNumber() {
        String modelNum;
        try {
            modelNum = Build.MODEL;
        } catch (Exception e) {
            modelNum = "-1";
        }
        return modelNum;
    }

    private String getDeviceAndroidOSVersion() {
        String androidVer;
        try {
            androidVer = Build.VERSION.RELEASE;
        } catch (Exception e) {

            androidVer = "-1";
        }
        return androidVer;
    }

    @SuppressLint("DefaultLocale")
    private static String getIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (isIPv4 && intf.getDisplayName().startsWith("wlan")) {
                            return sAddr;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            return null;
        }
        return null;
    }
}