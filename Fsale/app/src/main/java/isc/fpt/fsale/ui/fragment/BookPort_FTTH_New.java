package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import com.danh32.fontify.Button;
import com.danh32.fontify.EditText;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetIPV4Action;
import isc.fpt.fsale.action.GetPortFreeFTTHNewAction;
import isc.fpt.fsale.action.InsertBookPortFTTHNew;
import isc.fpt.fsale.action.RecoverRegistrationAction;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import isc.fpt.fsale.utils.MyApp.TrackerName;


public class BookPort_FTTH_New extends DialogFragment {
    private Context mContext;
    private ImageButton btnClose;
    private RowBookPortModel selectedItemParams;
    private String SOPHIEUDK, ID;
    private Spinner SpRegions, SpBranch, SpPop, SpTapDiem, SpPortFree;
    public EditText txt_NumberRegister, txt_NumberCard;
    private KeyValuePairAdapter adapter;
    private Button btnBookPort, btnRecoverRegistration;
    private ImageView imgReload;
    private String IDPort = "-1";
    public String _BranchID, _LocationID, _TDID;
    private RegistrationDetailModel modelRegister;
    private Location locationCurrentDevice;
    private Location locationMarker;
    private String latlngDevice = "";
    private String latlngMarker = "";


    public BookPort_FTTH_New() {
    }

    @SuppressLint("ValidFragment")
    public BookPort_FTTH_New(Context _mContext, RowBookPortModel _selectedItem, String _SOPHIEUDK,
                             String _ID, RegistrationDetailModel modelRegister,
                             Location locationCurrentDevice, Location locationMarker) {
        this.mContext = _mContext;
        this.selectedItemParams = _selectedItem;
        this.SOPHIEUDK = _SOPHIEUDK;
        this.ID = _ID;
        this.modelRegister = modelRegister;
        this.locationCurrentDevice = locationCurrentDevice;
        this.locationMarker = locationMarker;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_ftth_new, container);
        btnClose = (ImageButton) view.findViewById(R.id.btn_close);
        btnBookPort = (Button) view.findViewById(R.id.btn_book_port);

        // Thu hồi phiếu đăng ký
        btnRecoverRegistration = (Button) view.findViewById(R.id.btn_recover_registration);
        btnRecoverRegistration.setVisibility(View.GONE);
        // Neu tap diem da duoc book va ODCCable khac ten tap diem ==> hien nut thu hoi
        try {
            // if((this.modelRegister.getODCCableType()== null ||modelRegister.getODCCableType().equals("")) && (this.modelRegister.getTDName() != null ||!this.modelRegister.getTDName().equals("") ) )
            if ((this.modelRegister.getTDName() != null && !this.modelRegister.getTDName().equals("")))
                btnRecoverRegistration.setVisibility(View.VISIBLE);
        } catch (Exception e) {

            //Common.alertDialog("BookPort_FTTH_New_onCreateView: " + e.getMessage(), mContext);
        }
        btnRecoverRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                Dialog dialog;
                builder = new AlertDialog.Builder(mContext);
                builder.setMessage(mContext.getResources().getString(R.string.msg_confirm_recover_registration))
                        .setCancelable(false)
                        .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                recoveryPort();
                            }
                        })
                        .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                dialog = builder.create();
                dialog.show();
            }
        });
        txt_NumberRegister = (EditText) view.findViewById(R.id.txt_bookport_number_registration);
        txt_NumberRegister.setText(this.SOPHIEUDK);
        txt_NumberRegister.setEnabled(isVisible());
        SpRegions = (Spinner) view.findViewById(R.id.sp_region);
        SpBranch = (Spinner) view.findViewById(R.id.sp_branch);
        SpPop = (Spinner) view.findViewById(R.id.sp_pop);
        SpTapDiem = (Spinner) view.findViewById(R.id.sp_point);
        SpPortFree = (Spinner) view.findViewById(R.id.sp_portFree);
        imgReload = (ImageView) view.findViewById(R.id.img_reload);
        ShowData();
        SpPortFree.setEnabled(false);
        SpPortFree.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                IDPort = String.valueOf(selectedItem.getID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


        imgReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadPort();
            }
        });
        btnBookPort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Kiểm tra port
                if (IDPort == null || IDPort.equals("-1")) {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_port_empty), mContext);
                    return;
                }
                AlertDialog.Builder builder;
                Dialog dialog;
                builder = new AlertDialog.Builder(mContext);
                builder.setMessage(mContext.getResources().getString(R.string.msg_confirm_update))
                        .setCancelable(false)
                        .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String UserName = Constants.USERNAME;
                                String TDName = selectedItemParams.getTDName();
                                GetIPV4Action NewIP = new GetIPV4Action();
                                String IP = NewIP.GetIP();
                                String Latlng = selectedItemParams.getLatlngPort();
                                if (locationCurrentDevice != null)
                                    latlngDevice = locationCurrentDevice.getLatitude() + "," + locationCurrentDevice.getLongitude();

                                if (locationMarker != null)
                                    latlngMarker = locationMarker.getLatitude() + "," + locationMarker.getLongitude();

                                String[] paramsValue = new String[]{IDPort, SOPHIEUDK,
                                        TDName, UserName, IP, Latlng, latlngDevice, latlngMarker};
                                // kết nối API Bookport bằng tay FTTH_NEW
                                new InsertBookPortFTTHNew(mContext, paramsValue, ID);
                                onClickTracker("Book-Port FTTH New");
                            }
                        })
                        .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
                dialog = builder.create();
                dialog.show();

            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void ShowData() {
        //Vung mien SpRegions
        ArrayList<KeyValuePairModel> ListSpRegions = new ArrayList<>();
        ListSpRegions.add(new KeyValuePairModel(1, selectedItemParams.GetLocationName()));
        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpRegions, Gravity.LEFT);
        SpRegions.setAdapter(adapter);
        //Vung mien SpBranch
        ArrayList<KeyValuePairModel> ListSpBranch = new ArrayList<>();
        ListSpBranch.add(new KeyValuePairModel(1, selectedItemParams.GetNameBranch()));
        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpBranch, Gravity.LEFT);
        SpBranch.setAdapter(adapter);
        //Ten POP SpPop
        ArrayList<KeyValuePairModel> ListSpPop = new ArrayList<>();
        ListSpPop.add(new KeyValuePairModel(1, selectedItemParams.GetPopName()));
        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPop, Gravity.LEFT);
        SpPop.setAdapter(adapter);
        //Tap diem SpTapDiem
        ArrayList<KeyValuePairModel> ListSpTapDiem = new ArrayList<>();
        ListSpTapDiem.add(new KeyValuePairModel(1, selectedItemParams.getTDName()));
        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpTapDiem, Gravity.LEFT);
        SpTapDiem.setAdapter(adapter);
        //PortFree SpPortFree
        LoadPort();
    }

    private void LoadPort() {
        //PortFree SpPortFree
        String TDName = selectedItemParams.getTDName();
        new GetPortFreeFTTHNewAction(mContext, TDName, SpPortFree);
    }

    private void recoveryPort() {
        String UserName = Constants.USERNAME;
        GetIPV4Action NewIP = new GetIPV4Action();
        String IP = NewIP.GetIP();
        String[] paramsValue = new String[]{"3", SOPHIEUDK, UserName, IP};
        new RecoverRegistrationAction(mContext, paramsValue);
        onClickTracker("Thu hồi port FTTH New");
    }

    private void onClickTracker(String lable) {
        try {
            // Get tracker.
            Tracker t = ((MyApp) getActivity().getApplication()).getTracker(
                    TrackerName.APP_TRACKER);
            // Set screen name.
            t.setScreenName(getString(R.string.lbl_screen_name_dialog_book_port_ftth_new) + ".onClick()");
            // Send a screen view.
            t.send(new HitBuilders.ScreenViewBuilder().build());
            // This event will also be sent with the most recently set screen name.
            // Build and send an Event.
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("BOOK_PORT_ONCLICK")
                    .setAction("onClick")
                    .setLabel(lable)
                    .build());

            // Clear the screen name field when we're done.
            t.setScreenName(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}