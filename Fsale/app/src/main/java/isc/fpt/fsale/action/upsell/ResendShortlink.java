package isc.fpt.fsale.action.upsell;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.model.upsell.response.ResendShortlinkModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class ResendShortlink implements AsyncTaskCompleteListener<String> {

    private Context mContext;

    public ResendShortlink(Context mContext, String[] value) {
        this.mContext = mContext;
        String[] params = new String[]{"ObjID", "ProgramUpgradeID", "UserName", "PhoneNumber"};
        String message = "Vui lòn chờ...";
        String RESEND_SHORTLINK = "ResendShortlink";
        CallServiceTask service = new CallServiceTask(mContext, RESEND_SHORTLINK, params, value,
                Services.JSON_POST, message, ResendShortlink.this);
        service.execute();
    }


    @Override
    public void onTaskComplete(String result) {
        try {
            List<ResendShortlinkModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ResendShortlinkModel.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    Common.alertDialog(String.valueOf(lst.get(0).getResult()), mContext);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
