package isc.fpt.fsale.ui.fpt_camera.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckDuplicateRegistrationCamera;
import isc.fpt.fsale.action.GetPriceCamTotal;
import isc.fpt.fsale.action.TotalPriceEquipmentCamera;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.PriceCamTotal;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.ui.fpt_camera.FptCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.model.GetAllPromotionPackage;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera;
import isc.fpt.fsale.ui.fpt_camera.model.PromotionDetail;
import isc.fpt.fsale.ui.fpt_camera.model.RegisterFptCameraModel;
import isc.fpt.fsale.ui.fpt_camera.promotion_detail.PromotionDetailActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.CAMERA_PROMOTION;
import static isc.fpt.fsale.utils.Constants.CAMERA_PROMOTION_SELECTED;

/**
 * Created by haulc3 on 28,August,2019
 */
public class FptCameraTotal extends BaseFragment {
    private TextView tvCameraTotal, tvDeviceTotal, tvPriceTotal;
    private TextView tvPromotion;
    private RegisterFptCameraModel mRegisterFptCameraModel;
    private Button btnUpdate;
    private FptCameraActivity activity;
    public PromotionDetail mPromotionDetail;
    public boolean is_have_device;
    private final int CODE_CAMERA_PROMOTION = 1;
    private double deviceTotal, cameraTotal;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (FptCameraActivity) getActivity();
        mRegisterFptCameraModel = activity.getRegisterModel();
        if (mRegisterFptCameraModel.getObjectCamera().getPromotionDetail() != null) {
            mPromotionDetail = mRegisterFptCameraModel.getObjectCamera().getPromotionDetail();
        }
    }

    @Override
    protected void initView(View mView) {
        tvCameraTotal = mView.findViewById(R.id.tv_total_camera);
        tvPromotion = mView.findViewById(R.id.tv_promotion);
        btnUpdate = mView.findViewById(R.id.button_update);
        tvDeviceTotal = mView.findViewById(R.id.tv_total_device);
        tvPriceTotal = mView.findViewById(R.id.tv_total_price);
    }

    @Override
    protected void initEvent() {
        tvPromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetAllPromotionPackage mObject = new GetAllPromotionPackage(mRegisterFptCameraModel.getObjectCamera(),
                        Constants.USERNAME, 0, 0);
                Bundle bundle = new Bundle();
                bundle.putParcelable(CAMERA_PROMOTION, mObject);
                bundle.putParcelable(CAMERA_PROMOTION_SELECTED, mPromotionDetail);
                Intent intent = new Intent(getContext(), PromotionDetailActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_CAMERA_PROMOTION);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateRegistration();
            }
        });
    }

    @Override
    public void bindData() {
        if (mPromotionDetail == null) {
            tvPromotion.setText(getString(R.string.lbl_message_choose_promotion_camera));
        } else {
            tvPromotion.setText(mPromotionDetail.getPromoName() == null ?
                    getString(R.string.lbl_message_choose_promotion_camera) : mPromotionDetail.getPromoName());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                switch (requestCode) {
                    case CODE_CAMERA_PROMOTION:
                        mPromotionDetail = data.getParcelableExtra(CAMERA_PROMOTION);
                        if (mPromotionDetail == null) return;
                        tvPromotion.setText(mPromotionDetail.getPromoName());
                        mRegisterFptCameraModel.getObjectCamera().setPromotionDetail(mPromotionDetail);
                        activity.setIsUpdatePromotion(false);
                        getTotal();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    // tính tổng tiền thiết bị
    public void getDeviceTotalPrice(List<Device> mListDevice) {
        if (mListDevice == null || mListDevice.size() == 0) {
            tvDeviceTotal.setText(String.format(getString(R.string.txt_unit_price), Common.formatNumber(0)));
            updateTotal();
            deviceTotal = 0;
        } else {
            new TotalPriceEquipmentCamera(this.activity, mListDevice, 0, false);
            //kết nối api lấy tổng tiền thiết bị
            //------->               new TotalPriceEquipment(this.getActivity(), mListDevice, 0);
        }
    }

    public void loadDevicePrice(double priceDevice) {
        tvDeviceTotal.setText(String.format(getString(R.string.txt_unit_price), Common.formatNumber(priceDevice)));
        mRegisterFptCameraModel.setDeviceTotal(priceDevice);
        deviceTotal = priceDevice;
        updateTotal();
    }


    private void updateTotal() {
        int total = (int) (deviceTotal + cameraTotal);
        tvPriceTotal.setText(String.format(getString(R.string.txt_unit_price), Common.formatNumber(total)));
        mRegisterFptCameraModel.setTotal(total);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_fpt_camera_amount_total;
    }

    public void loadFptCameraTotal(PriceCamTotal mPriceCamTotal) {
        tvCameraTotal.setText(
                String.format(getString(R.string.txt_unit_price), Common.formatNumber(mPriceCamTotal.getTotal()))
        );
        mRegisterFptCameraModel.setCameraTotal(mPriceCamTotal.getTotal());
        cameraTotal = mPriceCamTotal.getTotal();
        updateTotal();

    }

    public void getTotal() {
        //tính tổng tiền
        new GetPriceCamTotal(getActivity(), this, false,
                new ObjectCamera(Constants.USERNAME, activity.getListCameraService()).toJSONObjectTotalPrice());
        getDeviceTotalPrice(activity.mRegisterFptCameraModel.getListDevice());
    }

    public void updateRegistration() {
        if (tvPromotion.getText().toString().equals(getString(R.string.lbl_message_choose_promotion_camera))) {
            showError(getString(R.string.txt_message_choose_promotion_fpt_camera));
            return;
        }
        //kiểm tra thông tin KH trc khi tạo PDK
        new CheckDuplicateRegistrationCamera(getContext(), mRegisterFptCameraModel);
    }
}