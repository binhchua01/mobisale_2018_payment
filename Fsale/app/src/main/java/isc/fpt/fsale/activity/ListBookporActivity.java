package isc.fpt.fsale.activity;


import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.BookPortAdapter;
import isc.fpt.fsale.model.BookportListModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.danh32.fontify.Button;

public class ListBookporActivity extends BaseActivity implements OnItemClickListener {

    private ArrayList<BookportListModel> lstbookport = new ArrayList<BookportListModel>();
    private ListView lvBilling;
    private ArrayList<BookportListModel> lstPop = new ArrayList<BookportListModel>();
    private BookPortAdapter adapter;
    private Button bttclose;

    public ListBookporActivity() {
        super(R.string.menu_book_port);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_bookport_activity));
        setContentView(R.layout.list_port);

        lvBilling = (ListView) findViewById(R.id.lv_billing_detail);
        lvBilling.setItemsCanFocus(true);
        lvBilling.setOnItemClickListener(this);
        lvBilling.setTextFilterEnabled(true);
        lvBilling.setSelection(0);


        bttclose = (Button) this.findViewById(R.id.btn_close);

        bttclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                lstPop = myIntent.getParcelableArrayListExtra("list_model_send");

                if (lstPop != null) {
                    lstbookport.add(lstPop.get(0));
                }
            }
        } catch (Exception e) {

        }
		
		
		/*
		BookportListModel mode1=new BookportListModel("1","Cum 1","10","20 m",1);
		BookportListModel mode2=new BookportListModel("2","Cum 2","5","15 m",2);
		BookportListModel mode3=new BookportListModel("3","Cum 3","8","18 m",3);
		BookportListModel mode4=new BookportListModel("4","Cum 4","11","21 m",1);
		BookportListModel mode5=new BookportListModel("5","Cum 5","15","16 m",2);
		BookportListModel mode6=new BookportListModel("6","Cum 6","18","19 m",3);
		*/
        //lstbookport.add(mode1);
        //lstbookport.add(mode2);
        //lstbookport.add(mode3);
        //lstbookport.add(mode4);
        //lstbookport.add(mode5);
        //lstbookport.add(mode6);

        adapter = new BookPortAdapter(ListBookporActivity.this, lstbookport);
        lvBilling.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        // TODO Auto-generated method stub

        // BookPortFTTH dialogFTTH = new BookPortFTTH();
        // BookPortADSL dialogADSL = new BookPortADSL();
        //if(selectedItem.getType()==1)
        // Common.showFragmentDialog(fm, dialogADSL, "fragment_bookPort_dialog");
        // else
        // Common.showFragmentDialog(fm, dialogFTTH, "fragment_bookPort_dialog");
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }
}
