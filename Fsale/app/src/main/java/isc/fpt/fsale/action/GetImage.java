package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ShowImageActivity;
import isc.fpt.fsale.model.ImageObjectModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class GetImage implements AsyncTaskCompleteListener<String> {

    public final static String GET_IMAGE_METHOD = "GetImage";
    public static String[] paramNames = new String[]{"Path"};
    private Context mContext;

    public GetImage(Context mContext, String Path) {
        this.mContext = mContext;
        String[] paramValues = new String[]{Path};
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, GET_IMAGE_METHOD, paramNames,
                paramValues, Services.JSON_POST, message, GetImage.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<ImageObjectModel> lst = null;
            boolean isError = false;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<ImageObjectModel> resultObject = new WSObjectsModel<>(jsObj, ImageObjectModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getArrayListObject();
                } else {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError) {
                    loadData(lst);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(ArrayList<ImageObjectModel> lst) {
        if (lst != null && lst.size() > 0) {
            if (mContext.getClass().getSimpleName().equals(ShowImageActivity.class.getSimpleName())) {
                ShowImageActivity activity = (ShowImageActivity) mContext;
                activity.loadData(lst.get(0).getImage());
            }
        } else {
            Common.alertDialog("Không có dữ liệu!", mContext);
        }
    }
}
