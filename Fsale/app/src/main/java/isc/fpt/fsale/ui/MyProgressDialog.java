package isc.fpt.fsale.ui;

import android.app.Dialog;
import android.content.Context;

import isc.fpt.fsale.R;

/**
 * Created by haulc3 on 27,November,2019
 */
public class MyProgressDialog extends Dialog {

    public MyProgressDialog(Context context) {
        super(context);
    }

    @Override
    public void show() {
        super.show();
        setContentView(R.layout.layout_progress_dialog);
    }
}
