package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentCustomerCareActivityList;
import isc.fpt.fsale.ui.fragment.FragmentCustomerCareInfo;
import isc.fpt.fsale.ui.fragment.FragmentCustomerCareToDoList;
import isc.fpt.fsale.model.CustomerCareListModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.TextView;

// màn hình CHĂM SÓC KHÁCH HÀNG
public class CustomerCareDetailActivity extends BaseActivity {

    public final String TAB_CUS_DETAIL = "CUS_DETAIL";
    public final String TAB_TODO_LIST = "TODO_LIST";
    public final String TAB_ACTIVITY = "ACTIVITY";

    private CustomerCareListModel mCustomer;

    public static FragmentManager fragmentManager;
    private FragmentTabHost mTabHost;
    private TextView lblFullName, lblContract;
    private LinearLayout frmTitle;

    public CustomerCareDetailActivity() {
        super(R.string.lbl_screen_name_customer_care_detail);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_customer_care_detail));
        try {
            if (((MyApp) getApplication()).getIsLogIn() == false) {
                Common.LogoutWithoutConfirm(this);
                finish();
            }

        } catch (Exception e) {

            //Log.i("CustomerCareDetailActivity:", e.getMessage());
        }

        setContentView(R.layout.activity_customer_care);
        lblFullName = (TextView) findViewById(R.id.lbl_full_name);
        lblContract = (TextView) findViewById(R.id.lbl_contract);
        frmTitle = (LinearLayout) findViewById(R.id.frm_title);
        getDataFromIntent();
        fragmentManager = getSupportFragmentManager();
        mTabHost = (FragmentTabHost) findViewById(R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.tabFrameLayout);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_CUS_DETAIL).setIndicator(getString(R.string.lbl_cus_info).toUpperCase()), FragmentCustomerCareInfo.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_TODO_LIST).setIndicator(getString(R.string.lbl_customer_care_todolist).toUpperCase()), FragmentCustomerCareToDoList.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_ACTIVITY).setIndicator(getString(R.string.lbl_customer_care_activity).toUpperCase()), FragmentCustomerCareActivityList.class, null);


    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        mCustomer = intent.getExtras().getParcelable("CUSTOMER_CARE");//.getParcelableExtra("CUSTOMER_CARE");
        if (mCustomer != null) {
            lblFullName.setText(mCustomer.getFullName());
            lblContract.setText(mCustomer.getContract());
            changBackgroud();
        }
    }

    public CustomerCareListModel getCustomer() {
        return this.mCustomer;
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    private void changBackgroud() {
        if (mCustomer != null) {
            Drawable drwTitle;
            switch (mCustomer.getLevelID()) {

                case 3:
                    drwTitle = getResources().getDrawable(R.drawable.border_title_level_1);
                    break;
                case 2:
                    drwTitle = getResources().getDrawable(R.drawable.border_title_level_2);
                    break;
                case 1:
                    drwTitle = getResources().getDrawable(R.drawable.border_title_level_3);
                    break;

                default:
                    drwTitle = getResources().getDrawable(R.drawable.border_title_level_3);
                    break;
            }
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                frmTitle.setBackgroundDrawable(drwTitle);
            } else {
                frmTitle.setBackground(drwTitle);
            }

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
