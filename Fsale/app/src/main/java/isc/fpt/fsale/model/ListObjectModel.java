package isc.fpt.fsale.model;


public class ListObjectModel {
    private String Contract;
    private String FullName;
    private String Address;
    private String LocalTypeName;
    private String Phone;
    private int ServiceType;
    private String ServiceTypeName;
    private int ComboStatus;
    private String ComboStatusName;
    private int ComboCameraStatus;
    private String ComboCameraStatusName;

    public String getContract() {
        return Contract;
    }

    public void setContract(String contract) {
        Contract = contract;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getLocalTypeName() {
        return LocalTypeName;
    }

    public void setLocalTypeName(String localTypeName) {
        LocalTypeName = localTypeName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }

    public String getServiceTypeName() {
        return ServiceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        ServiceTypeName = serviceTypeName;
    }

    public int getComboStatus() {
        return ComboStatus;
    }

    public void setComboStatus(int comboStatus) {
        ComboStatus = comboStatus;
    }

    public String getComboStatusName() {
        return ComboStatusName;
    }

    public void setComboStatusName(String comboStatusName) {
        ComboStatusName = comboStatusName;
    }

    public int getComboCameraStatus() {
        return ComboCameraStatus;
    }

    public void setComboCameraStatus(int comboCameraStatus) {
        ComboCameraStatus = comboCameraStatus;
    }

    public String getComboCameraStatusName() {
        return ComboCameraStatusName;
    }

    public void setComboCameraStatusName(String comboCameraStatusName) {
        ComboCameraStatusName = comboCameraStatusName;
    }
}