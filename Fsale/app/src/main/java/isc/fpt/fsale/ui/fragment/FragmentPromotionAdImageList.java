package isc.fpt.fsale.ui.fragment;

import java.util.ArrayList;

import isc.fpt.fsale.action.GetPromotionBrochure;
import isc.fpt.fsale.activity.PromotionAdImageViewActivity;
import isc.fpt.fsale.activity.PromotionAdVideoViewActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.VideoYoutubeActivity;
import isc.fpt.fsale.adapter.GridViewAdapter;
import isc.fpt.fsale.model.PromotionBrochureModel;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

public class FragmentPromotionAdImageList extends Fragment {

	public static final String ARG_MEDIA_TYPE = "ARG_MEDIA_TYPE";
	public static final int TAG_MEDIA_TYPE_VIDEO = 2, TAG_MEDIA_TYPE_IMAGE = 1, TAG_MEDIA_TYPE_TEXT = 3;
	private GridView mGridView;
    private ProgressBar mProgressBar;
    private Context mContext;
    private int mMediaType = 0;
    private ArrayList<PromotionBrochureModel> mList;
    
	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static FragmentPromotionAdImageList newInstance(Bundle args) {
		FragmentPromotionAdImageList fragment = new FragmentPromotionAdImageList();
		/*Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		args.putString(ARG_SECTION_TITLE, sectionTitle);*/
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentPromotionAdImageList() {
		
	}
	
	 // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		if(getArguments() != null) {
			mMediaType = getArguments().getInt(ARG_MEDIA_TYPE);
		}
    }

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContext = getActivity();
		View rootView = inflater.inflate(R.layout.fragment_promotion_image_list, container, false);
		mGridView = (GridView) rootView.findViewById(R.id.gridView);
        mProgressBar = (ProgressBar)  rootView.findViewById(R.id.progressBar);
    
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                //Get item at position
            	if(mMediaType == TAG_MEDIA_TYPE_IMAGE){
	            	Intent intent = new Intent(mContext, PromotionAdImageViewActivity.class);
	            	intent.putExtra(PromotionAdImageViewActivity.ARG_PROMOTION_BROCHURE_LIST, mList); 
	            	intent.putExtra(PromotionAdImageViewActivity.ARG_SELECTED_POSITION, position); 
		            startActivity(intent);
            	}else if(mMediaType == TAG_MEDIA_TYPE_VIDEO){  
            		PromotionBrochureModel item = (PromotionBrochureModel) parent.getItemAtPosition(position); 
            		Intent intent = new Intent(mContext, VideoYoutubeActivity.class);
            		Bundle imageBundle = new Bundle();//	    
            		imageBundle.putInt(PromotionAdVideoViewActivity.ARG_SELECTED_POSITION, position);
            		imageBundle.putParcelable(PromotionAdVideoViewActivity.ARG_PROMOTION_BROCHURE_OBJECT, item);
 	                intent.putExtra(PromotionAdVideoViewActivity.ARG_BUNDLE, imageBundle); 	                
 	                startActivity(intent);
            	}
	            
            	//Neu Tab la loai media Image => Start activity xem anh;
            	/*
            	 PromotionModel item = (PromotionModel) parent.getItemAtPosition(position); 
            	 if(mMediaType == TAG_MEDIA_IMAGE_TYPE){
	                Intent intent = new Intent(mContext, PromotionAdImageViewActivity.class);
	                ImageView imageView = (ImageView) v.findViewById(R.id.grid_item_image);	
	                int[] screenLocation = new int[2];
	                imageView.getLocationOnScreen(screenLocation);	                
	                Bundle imageBundle = new Bundle();//
	                imageBundle.putInt(PromotionAdImageViewActivity.ARG_WIDTH, imageView.getWidth());
	                imageBundle.putInt(PromotionAdImageViewActivity.ARG_HEIGHT, imageView.getHeight());
	                imageBundle.putInt(PromotionAdImageViewActivity.ARG_LEFT,  screenLocation[0]);
	                imageBundle.putInt(PromotionAdImageViewActivity.ARG_TOP, screenLocation[1]);
	                imageBundle.putInt(PromotionAdImageViewActivity.ARG_TOP, screenLocation[1]);	
	                imageBundle.putParcelable(PromotionAdImageViewActivity.ARG_PROMOTION_OBJECT, item);	
	                intent.putExtra(PromotionAdImageViewActivity.ARG_BUNDLE, imageBundle);
	                startActivity(intent);
            	}else if(mMediaType == TAG_MEDIA_VIDEO_TYPE){            		
            		Intent intent = new Intent(mContext, PromotionAdVideoViewActivity.class);
            		Bundle imageBundle = new Bundle();//	    
            		imageBundle.putParcelable(PromotionAdVideoViewActivity.ARG_PROMOTION_OBJECT, item);
 	                intent.putExtra(PromotionAdVideoViewActivity.ARG_BUNDLE, imageBundle); 	                
 	                startActivity(intent);
            	}*/
            }
        });

        //Start download
        //new AsyncHttpTask().execute(FEED_URL);
        mProgressBar.setVisibility(View.VISIBLE);
        getData(0);
		return rootView;
	}
	
	
	private void getData(int page){
    	new GetPromotionBrochure(getActivity(), this, mMediaType);
    }
    
    public void loadData(ArrayList<PromotionBrochureModel> lst){
    	if(lst != null && lst.size() >0){    		    
    		mList = lst;
    		GridViewAdapter adapter = new GridViewAdapter(mContext, R.layout.grid_item_image_layout, mList);
    		mGridView.setAdapter(adapter);
    	}
    	mProgressBar.setVisibility(View.GONE);
    }
    
    
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}
	
    @Override
    public void onDestroyView() {
        super.onDestroyView();        
    }
}
