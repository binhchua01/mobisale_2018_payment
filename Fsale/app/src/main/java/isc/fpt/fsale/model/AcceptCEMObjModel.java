package isc.fpt.fsale.model;

public class AcceptCEMObjModel {
	
	private String Result ;  
	private int ResultID ;
	public String getResult() {
		return Result;
	}
	public void setResult(String result) {
		Result = result;
	}
	public int getResultID() {
		return ResultID;
	}
	public void setResultID(int resultID) {
		ResultID = resultID;
	} 
	
	public AcceptCEMObjModel()
	{}

}
