package isc.fpt.fsale.adapter;

import java.util.ArrayList;
import java.util.Locale;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import isc.fpt.fsale.ui.fragment.FragmentPromotionAdImageDetail;

public class ViewPagerPromotionAdImageDetailAdapter extends FragmentPagerAdapter {

	//private static int NUM_ITEMS = 2;
	private ArrayList<FragmentPromotionAdImageDetail> mFragments;
	
	public ViewPagerPromotionAdImageDetailAdapter(FragmentManager fm, ArrayList<Bundle> imageBundle) {
		super(fm);
		mFragments = new ArrayList<FragmentPromotionAdImageDetail>();
		for (Bundle bundle : imageBundle) {			
			mFragments.add(FragmentPromotionAdImageDetail.newInstance(bundle));			
		}
	}

	@Override
	public Fragment getItem(int position) {
		// getItem is called to instantiate the fragment for the given page.
		// Return a PlaceholderFragment (defined as a static inner class
		// below).
		
		//return CreatePotentialObjMapFragment.newInstance(position + 1);
		/*switch (position) {
        case 0: // Fragment # 0 - This will show FirstFragment        	
        	//return FragmentPromotionImageView.newInstance(mImageBundle);
        	return FragmentPromotionImageDetail.newInstance(mImageBundle);
        case 1: // Fragment # 0 - This will show FirstFragment different title
            //return CreatePotentialObjMapFragment.newInstance(1, "Page # 2");
        	//return FragmentPromotionVideo.newInstance(mVideoBundle);
        	return FragmentPromotionImageList.newInstance(mVideoBundle);
        default:
            return null;
        }*/
		
		/*if(mFragments != null && position < mFragments.size() - 1)
			return mFragments.get(position);
		return null;*/
		return mFragments.get(position);
	}
		
	@Override
	public int getCount() {
		// Show 3 total pages.
		if(mFragments != null)
			return mFragments.size();
		return 0;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		Locale l = Locale.getDefault();
		String title = "Page " + String.valueOf(position);		
		return title.toUpperCase(l);
	}
}
