package isc.fpt.fsale.action.maxy;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.maxytv.model.MaxySetupType;
import isc.fpt.fsale.ui.maxytv.model.MaxyTypeDeploy;
import isc.fpt.fsale.ui.maxytv.ui.MaxySetupTypeActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyTypeDeployActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//Lấy danh sách triển khai
public class GetSetupMaxy implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private int RegType;

    public GetSetupMaxy(Context mContext, int regType, int localType, String contract, int typeDeployID) {
        this.mContext = mContext;
        this.RegType = regType;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("RegType",RegType);
            jsonObject.put("LocationID", Constants.LST_REGION.get(0).getsID());
            jsonObject.put("TypeDeployID", typeDeployID);
            jsonObject.put("BranchCode", Constants.BRANCH_CODE);
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("LocalType", localType);
            jsonObject.put("Contract", contract);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.msg_progress_get_type_deploy);
        String GET_MAXY_SETUP_TYPE = "GetSetupMaxy";
        CallServiceTask service = new CallServiceTask(mContext, GET_MAXY_SETUP_TYPE, jsonObject,
                Services.JSON_POST_OBJECT, message, GetSetupMaxy.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<MaxySetupType> resultObject = new WSObjectsModel<>(jsObj, MaxySetupType.class);
                List<MaxySetupType> lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (mContext != null) {
                    if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                        Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                    } else {
                        ((MaxySetupTypeActivity) mContext).loadData(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
