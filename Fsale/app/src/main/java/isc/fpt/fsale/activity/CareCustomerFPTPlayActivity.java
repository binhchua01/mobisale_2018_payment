package isc.fpt.fsale.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import isc.fpt.fsale.R;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// màn hình chăm sóc khách hàng paytv
public class CareCustomerFPTPlayActivity extends BaseActivity {
    private WebView wvShowCareCustomerFPTPlay;
    private TextView tvEmptyData;
    private Context mContext;

    public CareCustomerFPTPlayActivity() {
        super(R.string.lbl_screen_name_care_customer_fpt_play);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_care_customer_fpt_play);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_care_customer_fpt_play));
        this.mContext = this;
        tvEmptyData = (TextView) findViewById(R.id.tv_empty_data);
        wvShowCareCustomerFPTPlay = (WebView) findViewById(R.id.w_care_customer_fpt_play);
        getIntentData();
    }

    public void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            String linkWebCareCustomerFPTPlay = intent.getStringExtra("LINK_CS_KH_PAY_TV");
            if (linkWebCareCustomerFPTPlay == null || linkWebCareCustomerFPTPlay.equals("")) {
                tvEmptyData.setVisibility(View.VISIBLE);
                wvShowCareCustomerFPTPlay.setVisibility(View.GONE);
            } else {
                wvShowCareCustomerFPTPlay.setVisibility(View.VISIBLE);
                wvShowCareCustomerFPTPlay.getSettings().setJavaScriptEnabled(true);
                wvShowCareCustomerFPTPlay.setWebViewClient(new WebViewClient() {
                    @SuppressWarnings("deprecation")
                    @Override
                    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                        Toast.makeText(mContext, description, Toast.LENGTH_SHORT).show();
                    }

                    @TargetApi(android.os.Build.VERSION_CODES.M)
                    @Override
                    public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                        // Redirect to deprecated method, so you can use it in all SDK versions
                        onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                    }
                });
                wvShowCareCustomerFPTPlay.loadUrl(linkWebCareCustomerFPTPlay);
            }
        } else {
            Toast.makeText(mContext, "Dữ liệu đường dẫn truyền đi thất bại", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
}
