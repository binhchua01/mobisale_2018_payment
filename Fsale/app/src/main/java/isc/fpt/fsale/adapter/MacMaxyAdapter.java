package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.MacOTT;

public class MacMaxyAdapter extends RecyclerView.Adapter<MacMaxyAdapter.SimpleViewHolder> {
    private List<MacOTT> mList;
    private Context mContext;

    public MacMaxyAdapter(Context context, List<MacOTT> lst) {
        this.mContext = context;
        this.mList = lst;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_row_mac, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position), position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    private void confirmRemoveMac(final int position) {
        try {
            Dialog dialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage("Bạn muốn xóa địa chỉ MAC này?")
                    .setCancelable(false)
                    .setPositiveButton(mContext.getResources().getString(R.string.lbl_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (mList.size() > 0) {
                                        mList.remove(position);
                                        notifyDataSetChanged();
                                    }
                                }
                            })
                    .setNegativeButton(mContext.getResources().getString(R.string.lbl_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView titleNumberBox, tvTextMacBox;
        private ImageView imgDeleteTextMacBox;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            titleNumberBox = itemView.findViewById(R.id.title_number_box);
            tvTextMacBox = itemView.findViewById(R.id.tv_text_mac_box);
            imgDeleteTextMacBox = itemView.findViewById(R.id.tv_delete_text_mac_box);
        }

        @SuppressLint("SetTextI18n")
        public void bindView(MacOTT mMac, final int position) {
            titleNumberBox.setText("Box " + (position + 1));
            tvTextMacBox.setText(mMac.getMAC());
            imgDeleteTextMacBox.setOnClickListener(v -> confirmRemoveMac(position));
        }
    }

    public void notifyData(List<MacOTT> mList){
        this.mList = mList;
        notifyDataSetChanged();
    }
}
