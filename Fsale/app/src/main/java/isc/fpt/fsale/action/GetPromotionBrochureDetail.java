package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.PromotionAdTextFullScreenActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionBrochureModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetPromotionBrochureDetail implements AsyncTaskCompleteListener<String>{ 

	public final static String METHOD_NAME = "GetBrochureDetail";
	public static String[] paramNames = new String[]{"UserName", "Type", "ID"};
	public static int TYPE_VIDEO = 2, TYPE_IMAGE = 1, TYPE_TEXT = 3;
	private Context mContext; 
	
	public GetPromotionBrochureDetail(Context context, int Type, int ID ) {
		this.mContext = context;
		String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
		String[] paramValues = new String[]{userName, String.valueOf(Type), String.valueOf(ID)};		
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST_UPLOAD, message, GetPromotionBrochureDetail.this);
		service.execute();	
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			ArrayList<PromotionBrochureModel> lst = null;
			boolean isError = false;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<PromotionBrochureModel> resultObject = new WSObjectsModel<PromotionBrochureModel>(jsObj, PromotionBrochureModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();								
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(ArrayList<PromotionBrochureModel> lst){
		if(lst != null && lst.size() >0){
			if(mContext.getClass().getSimpleName().equals(PromotionAdTextFullScreenActivity.class.getSimpleName())){
				PromotionAdTextFullScreenActivity activity = (PromotionAdTextFullScreenActivity)mContext;
				activity.loadData(lst.get(0).getMediaUrl());
			}
		}else
			Common.alertDialog("Không có dữ liệu!", mContext);
	}
	
	

}
