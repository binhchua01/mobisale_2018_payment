package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PayContractActivity;
import isc.fpt.fsale.activity.PaymentRetailAdditionalActivity;
import isc.fpt.fsale.model.PaidType;
import isc.fpt.fsale.model.PaymentInformationPOST;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by HCM.TUANTT14 on 4/27/2018.
 * API lấy danh sách thanh toán online bán mới
 */

public class GetListPaidType implements AsyncTaskCompleteListener<String> {
    private final String GET_LIST_PAID_TYPE = "GetListPaidType";
    private Context mContext;
    private String[] arrParamName, arrParamValue;
    private PayContractActivity payContractActivity;
    private PaymentRetailAdditionalActivity paymentRetailAdditionalActivity;

    public GetListPaidType(Context mContext, PayContractActivity payContractActivity, PaymentInformationPOST mObject) {
        this.mContext = mContext;
        this.payContractActivity = payContractActivity;
        arrParamName = new String[]{"UserName", "RegCode"};
        arrParamValue = new String[]{mObject.getSaleName(), mObject.getRegCode()};
        String message = mContext.getResources().getString(
                R.string.msg_pd_get_paid_type_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_PAID_TYPE, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetListPaidType.this);
        service.execute();
    }

    public GetListPaidType(Context mContext, PaymentRetailAdditionalActivity paymentRetailAdditionalActivity, String userName, String regCode) {
        this.mContext = mContext;
        this.paymentRetailAdditionalActivity = paymentRetailAdditionalActivity;
        arrParamName = new String[]{"UserName", "RegCode"};
        arrParamValue = new String[]{userName, regCode};
        String message = mContext.getResources().getString(
                R.string.msg_pd_get_paid_type_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_PAID_TYPE, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetListPaidType.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        WSObjectsModel<PaidType> resultObject;
        ArrayList<PaidType> listPaidType;
        try {
            boolean isError = false;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                resultObject = new WSObjectsModel<>(jsObj, PaidType.class);
                if (resultObject.getErrorCode() != 0) {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError) {
                    listPaidType = resultObject.getArrayListObject();
                    if (payContractActivity != null) {
                        this.payContractActivity.loadPaidTypeList(listPaidType);
                    }
                    if (this.paymentRetailAdditionalActivity != null) {
                        this.paymentRetailAdditionalActivity.loadPaidTypeList(listPaidType);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
