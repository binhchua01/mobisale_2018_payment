package isc.fpt.fsale.ui.fpt_camera.camera_price_list;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListPriceEquipment;
import isc.fpt.fsale.action.GetListPriceEquipmentCamera;
import isc.fpt.fsale.activity.device_price_list.DevicePrice;
import isc.fpt.fsale.activity.device_price_list.DevicePriceListAdapter;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.utils.Constants;

public class DevicePriceListCameraActivity extends BaseActivitySecond implements OnItemClickListener<Device> {
    private View loBack;
    private List<DevicePrice> mList;
    private Device mDeviceSelected;
    private int position;
    private DevicePriceListAdapter mAdapter;
    private String mClassName;

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_device_price_list;
    }

    @Override
    protected void initView() {
        getDataIntent();

        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.lbl_screen_name_device_price_list));
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_device_price_list);
        mList = new ArrayList<>();
        mAdapter = new DevicePriceListAdapter(this, mList, mDeviceSelected, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(Constants.DEVICE) != null) {
            mDeviceSelected = bundle.getParcelable(Constants.DEVICE);
            position = bundle.getInt(Constants.POSITION);
            mClassName = bundle.getString(Constants.CLASS_NAME);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mDeviceSelected != null){
            new GetListPriceEquipmentCamera(this, mDeviceSelected, mClassName);
        }
    }

    public void loadDeviceList(List<DevicePrice> mList){
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(Device mDeviceSelected) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.DEVICE, mDeviceSelected);
        returnIntent.putExtra(Constants.POSITION, position);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
