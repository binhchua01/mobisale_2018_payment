package isc.fpt.fsale.ui.fragment;

import isc.fpt.fsale.R;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class AboutVersionDialog extends DialogFragment{

	private ImageButton imgClose;
	private Button btnOK;
	private Context mContext;

	public AboutVersionDialog(){}

	@SuppressLint("ValidFragment")
	public AboutVersionDialog(Context mContext) {
		// TODO Auto-generated constructor stub
		this.mContext = mContext;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	    // Inflate the layout to use as dialog or embedded fragment
		try {
			Common.savePreference(mContext, Constants.SHARE_PRE_ABOUT_VERSION,  Common.GetAppVersion(mContext), true);
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			Log.i("AboutVersionDialog.onCreateView(0", e.getMessage());
		}
		
		View view = inflater.inflate(R.layout.dialog_about_version, container);
		imgClose = (ImageButton)view.findViewById(R.id.btn_close);
		imgClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getDialog().dismiss();
			}
		});
		btnOK  = (Button)view.findViewById(R.id.btn_ok);
		btnOK.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getDialog().dismiss();
			}
		});
		TextView lblTitle = (TextView)view.findViewById(R.id.lbl_title);
		lblTitle.setText("Phiên bản " + Common.GetAppVersion(mContext));
		return view;
	}
	
	
	/** The system calls this only when creating the layout in a dialog. */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {	   
	    Dialog dialog = super.onCreateDialog(savedInstanceState);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    return dialog;
	}
	
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		 
	}
}
