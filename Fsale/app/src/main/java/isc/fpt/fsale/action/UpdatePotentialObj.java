package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

//API tạo khách hàng tiềm năng
public class UpdatePotentialObj implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    public final String TAG_METHOD_NAME = "UpdatePotentialObj";
    private int PotentialID = 0;
    private String userName;
    private boolean doSurvey;
    private String support;
    public UpdatePotentialObj(Context context, String UserName, PotentialObjModel obj, boolean doSurvey) {
        mContext = context;
        this.doSurvey = doSurvey;
        String message = "Đang cập nhật...";
        this.userName = UserName;
        CallServiceTask service;
        try {
            PotentialID = obj.getID();
            JSONObject json = obj.toJSONObject();
            json.put("UserName", UserName);
            service = new CallServiceTask(mContext, TAG_METHOD_NAME, json, Services.JSON_POST_OBJECT, message, UpdatePotentialObj.this);
            service.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public UpdatePotentialObj(Context context, String UserName, PotentialObjModel obj, boolean doSurvey, String support) {
        mContext = context;
        this.doSurvey = doSurvey;
        String message = "Đang cập nhật...";
        this.userName = UserName;
        this.support = support;
        CallServiceTask service;
        try {
            PotentialID = obj.getID();
            JSONObject json = obj.toJSONObject();
            json.put("UserName", UserName);
            service = new CallServiceTask(mContext, TAG_METHOD_NAME, json, Services.JSON_POST_OBJECT, message, UpdatePotentialObj.this);
            service.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onTaskComplete(String result) {
        try {
            List<UpdResultModel> lst = null;
            if(Common.jsonObjectValidate(result)){
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(jsObj, UpdResultModel.class);
                if(resultObject.getErrorCode() == 0){//OK not Error
                    lst = resultObject.getListObject();
                }else{//ServiceType Error
                    Common.alertDialog( resultObject.getError(),mContext);
                }
                loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(List<UpdResultModel> lst){
        try {
            if(mContext.getClass().getSimpleName().equals(CreatePotentialObjActivity.class.getSimpleName())){
                if(lst != null && lst.size() > 0){
                    final UpdResultModel item = lst.get(0);
                    if(item.getResultID() >0){
                        //PotentialID = item.getResultID();
                        new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(item.getResult())
                                .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            if(PotentialID == 0 && doSurvey)
                                                new GetPotentialObjDetail(mContext, userName, item.getResultID(), true, support);
                                            else
                                                new GetPotentialObjDetail(mContext, userName, item.getResultID(),support);
                                            dialog.cancel();
                                        } catch (Exception e) {

                                            e.printStackTrace();
                                        }

                                    }
                                })
                                .setCancelable(false).create().show();
                    }else
                        Common.alertDialog(lst.get(0).getResult(), mContext);
                }
                else
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}