package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// màn hình xác nhận thanh toán online bán thêm
public class ConfirmPaymentRetailAdditionalActivity extends BaseActivity {
    private TextView tvRegCodeConfirmPayment, tvContractCodeConfirmPayment,
            tvCusNameConfirmPayment, tvPhoneConfirmPayment, tvTotalConfirmPayment,
            tvPayTypeConfirmPayment, tvStatusConfirmPayment;
    private Button btnBackToMainScreen, btnGoToAppointmentScreen;
    private RegistrationDetailModel registrationDetail;
    private String paidTypeValue = "";
    private Context mContext;

    public ConfirmPaymentRetailAdditionalActivity() {
        super(R.string.confirm_payment_header_activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.confirm_payment_header_activity));
        setContentView(R.layout.activity_confirm_payment);
        this.mContext = this;
        initWidget();
        Intent intent = getIntent();
        if (intent == null) {
            Common.showToast(mContext, "Thông tin khách hàng truyền đi thất bại!");
            return;
        }
        registrationDetail = intent.getParcelableExtra(Constants.MODEL_REGISTER);
        paidTypeValue = intent.getStringExtra("PAID_TYPE_VALUE");
        createEvents();
        showDataOnText();
    }

    private void initWidget() {
        tvRegCodeConfirmPayment = (TextView) findViewById(R.id.tvRegCodeConfirmPayment);
        tvContractCodeConfirmPayment = (TextView) findViewById(R.id.tvContractCodeConfirmPayment);
        tvCusNameConfirmPayment = (TextView) findViewById(R.id.tvCusNameConfirmPayment);
        tvPhoneConfirmPayment = (TextView) findViewById(R.id.tvPhoneConfirmPayment);
        tvTotalConfirmPayment = (TextView) findViewById(R.id.tvTotalConfirmPayment);
        tvPayTypeConfirmPayment = (TextView) findViewById(R.id.tvPayTypeConfirmPayment);
        tvStatusConfirmPayment = (TextView) findViewById(R.id.tvStatusConfirmPayment);
        btnBackToMainScreen = (Button) findViewById(R.id.btn_back_to_main_screen);
        btnGoToAppointmentScreen = (Button) findViewById(R.id.btn_go_to_appointment);
    }

    /**
     * Create Events
     */
    private void createEvents() {
        //Back button
        btnBackToMainScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                ConfirmPaymentRetailAdditionalActivity.this.finish();
            }
        });

        btnGoToAppointmentScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DeployAppointmentActivity.class);
                intent.putExtra(Constants.MODEL_REGISTER, registrationDetail);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                //Finish confirm payment activity
                ConfirmPaymentRetailAdditionalActivity.this.finish();
            }
        });
    }

    /**
     * Show data on text
     */
    @SuppressLint("SetTextI18n")
    private void showDataOnText() {
        tvRegCodeConfirmPayment.setText(registrationDetail.getRegCode());
        tvContractCodeConfirmPayment.setText(registrationDetail.getContract());
        tvCusNameConfirmPayment.setText(registrationDetail.getFullName());
        tvPhoneConfirmPayment.setText(registrationDetail.getPhone_1());
        tvPayTypeConfirmPayment.setText(paidTypeValue);
        tvTotalConfirmPayment.setText(Common.formatNumber(registrationDetail.getTotal()));
        tvStatusConfirmPayment.setText("Thanh toán thành công");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    public void onBackPressed() {

    }
}
