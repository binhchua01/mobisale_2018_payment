package isc.fpt.fsale.ui.fpt_camera.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by haulc3 on 10,September,2019
 */
public class GetAllPromotionPackage implements Parcelable {
    private ObjectCamera mObjectCamera;
    private String mUsername;
    private int mCombo;
    private int mRegType;

    public GetAllPromotionPackage(ObjectCamera mObjectCamera, String mUsername, int mCombo, int mRegType) {
        this.mObjectCamera = mObjectCamera;
        this.mUsername = mUsername;
        this.mCombo = mCombo;
        this.mRegType = mRegType;
    }

    private GetAllPromotionPackage(Parcel in) {
        mObjectCamera = in.readParcelable(ObjectCamera.class.getClassLoader());
        mUsername = in.readString();
        mCombo = in.readInt();
        mRegType = in.readInt();
    }

    public static final Creator<GetAllPromotionPackage> CREATOR = new Creator<GetAllPromotionPackage>() {
        @Override
        public GetAllPromotionPackage createFromParcel(Parcel in) {
            return new GetAllPromotionPackage(in);
        }

        @Override
        public GetAllPromotionPackage[] newArray(int size) {
            return new GetAllPromotionPackage[size];
        }
    };

    public ObjectCamera getmObjectCamera() {
        return mObjectCamera;
    }

    public void setmObjectCamera(ObjectCamera mObjectCamera) {
        this.mObjectCamera = mObjectCamera;
    }

    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public int getmCombo() {
        return mCombo;
    }

    public void setmCombo(int mCombo) {
        this.mCombo = mCombo;
    }

    public int getmRegType() {
        return mRegType;
    }

    public void setmRegType(int mRegType) {
        this.mRegType = mRegType;
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cameraServices", mObjectCamera.toJsonObject());
            jsonObject.put("UserName", getmUsername());
            jsonObject.put("Combo", getmCombo());
            jsonObject.put("RegType", getmRegType());
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(mObjectCamera, i);
        parcel.writeString(mUsername);
        parcel.writeInt(mCombo);
        parcel.writeInt(mRegType);
    }
}
