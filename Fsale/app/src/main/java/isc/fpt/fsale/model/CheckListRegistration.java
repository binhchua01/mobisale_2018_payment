package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 5/4/2018.
 */
// model trạng thái phiếu đăng ký
public class CheckListRegistration implements Parcelable {
    private int TrangThaiThanhToan;
    private boolean isBookPort, isHopDong, isKhaoSat, isKhoanThu;
    private String ThongDiepThanhToan;

    private CheckListRegistration(Parcel in) {
        TrangThaiThanhToan = in.readInt();
        isBookPort = in.readByte() != 0;
        isHopDong = in.readByte() != 0;
        isKhaoSat = in.readByte() != 0;
        isKhoanThu = in.readByte() != 0;
        ThongDiepThanhToan = in.readString();
    }

    public int getTrangThaiThanhToan() {
        return TrangThaiThanhToan;
    }

    public void setTrangThaiThanhToan(int trangThaiThanhToan) {
        TrangThaiThanhToan = trangThaiThanhToan;
    }

    public boolean isBookPort() {
        return isBookPort;
    }

    public void setBookPort(boolean bookPort) {
        isBookPort = bookPort;
    }

    public boolean isHopDong() {
        return isHopDong;
    }

    public void setHopDong(boolean hopDong) {
        isHopDong = hopDong;
    }

    public boolean isKhaoSat() {
        return isKhaoSat;
    }

    public void setKhaoSat(boolean khaoSat) {
        isKhaoSat = khaoSat;
    }

    public boolean isKhoanThu() {
        return isKhoanThu;
    }

    public void setKhoanThu(boolean khoanThu) {
        isKhoanThu = khoanThu;
    }

    public String getThongDiepThanhToan() {
        return ThongDiepThanhToan;
    }

    public void setThongDiepThanhToan(String thongDiepThanhToan) {
        ThongDiepThanhToan = thongDiepThanhToan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(TrangThaiThanhToan);
        parcel.writeByte((byte) (isBookPort ? 1 : 0));
        parcel.writeByte((byte) (isHopDong ? 1 : 0));
        parcel.writeByte((byte) (isKhaoSat ? 1 : 0));
        parcel.writeByte((byte) (isKhoanThu ? 1 : 0));
        parcel.writeString(ThongDiepThanhToan);
    }

    public static final Creator<CheckListRegistration> CREATOR = new Creator<CheckListRegistration>() {
        @Override
        public CheckListRegistration createFromParcel(Parcel in) {
            return new CheckListRegistration(in);
        }

        @Override
        public CheckListRegistration[] newArray(int size) {
            return new CheckListRegistration[size];
        }
    };
}
