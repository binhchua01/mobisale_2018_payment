package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.DepositActivity;
import isc.fpt.fsale.activity.DepositRegisterContractActivity;
import isc.fpt.fsale.activity.MainActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterDetailActivity;
import isc.fpt.fsale.ui.fragment.RegisterCollectMoneyDialog;
import isc.fpt.fsale.ui.fragment.SBIManageDialog;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.SBIModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.support.v4.app.FragmentManager;
// API lấy danh sách SBI
public class GetSBIList implements AsyncTaskCompleteListener<String> {

	private String[] arrParamName, arrParamValue;	
	private Context mContext;
	private final String TAG_METHOD_NAME = "GetSBIList";
	private final String TAG_SBI_LIST = "ListObject", /*TAG_ID = "ID", TAG_SBI = "SBI", */TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";
	private RegistrationDetailModel mRegister = null;
	private FragmentManager mFM;
	private SBIManageDialog mSBIManageDialog;
	
	public GetSBIList(Context context, FragmentManager fm, RegistrationDetailModel regDetail,int Status){	
		try {
			String message = "Đang lấy SBI...";
			mContext = context;
			mFM = fm;			
			mRegister = regDetail;
			mSBIManageDialog = null;
			String regCode = "";
			if(regDetail!= null)
				regCode = regDetail.getRegCode();
			arrParamName = new String[]{"UserName", "RegCode", "Status"};			
			this.arrParamValue = new String[]{Constants.USERNAME, regCode, String.valueOf(Status)};
			CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, GetSBIList.this);
			service.execute();
		} catch (Exception e) {

			e.getMessage();
		}		
	}
	
	public GetSBIList(Context context, SBIManageDialog sbiManageDialog, RegistrationDetailModel regDetail,int Status){	
		try {
			String message = "Đang lấy SBI...";
			mContext = context;
			mFM = null;			
			mRegister = regDetail;
			mSBIManageDialog = sbiManageDialog;			
			arrParamName = new String[]{"UserName", "RegCode", "Status"};			
			this.arrParamValue = new String[]{Constants.USERNAME, "", String.valueOf(Status)};
			CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, GetSBIList.this);
			service.execute();
		} catch (Exception e) {

			e.getMessage();
		}		
	}
	
	public GetSBIList(Context context, RegistrationDetailModel regDetail,int Status){	
		try {
			String message = "Đang lấy SBI...";
			mContext = context;
			mRegister = regDetail;
			mSBIManageDialog = null;
			String regCode = "";
			if(regDetail!= null)
				regCode = regDetail.getRegCode();
			arrParamName = new String[]{"UserName", "RegCode", "Status"};			
			this.arrParamValue = new String[]{Constants.USERNAME, regCode, String.valueOf(Status)};
			CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, GetSBIList.this);
			service.execute();
		} catch (Exception e) {

			e.getMessage();
		}		
	}
	public void handleUpdate(String json){
		if(json != null && Common.jsonObjectValidate(json)){
		JSONObject jsObj = getJsonObject(json);		
		ArrayList<SBIModel> lstSBI = new ArrayList<SBIModel>();			
		if(jsObj != null){
			try {
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);								
				if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
					JSONArray sbiArray = jsObj.getJSONArray(TAG_SBI_LIST);
					if(sbiArray != null){
						for(int index=0; index < sbiArray.length(); index++)
							lstSBI.add(SBIModel.Parse(sbiArray.getJSONObject(index)));
					}					
				}else{
					Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
				}
			} catch (Exception e) {

			}			
		}else{
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + 
					"-" + Constants.RESPONSE_RESULT, mContext);
		}
		//Update SBI status for MainActivity
		if(mContext.getClass().getSimpleName().equals(MainActivity.class.getSimpleName())){
			//Update SBI Status
			if(mSBIManageDialog == null){
				MainActivity activity = (MainActivity)mContext;
				activity.loadSBIStatus(lstSBI);
			}else{
				//Update SBI Manage Dialog
				try {
					mSBIManageDialog.loadData(lstSBI);
				} catch (Exception e) {

					Common.alertDialog(e.getMessage(), mContext);
				}
			}
			//activity.onCreateOptionsMenu(menu)
		}else if(mContext.getClass().getSimpleName().equals(RegisterDetailActivity.class.getSimpleName())){			
			//Show dialog Collect Money
			if(mFM != null){
				RegisterCollectMoneyDialog dialog = new RegisterCollectMoneyDialog(mContext, mRegister, lstSBI);
				Common.showFragmentDialog(mFM, dialog, "");
			}
		}
		
		//
		if(mContext.getClass().getSimpleName().equals(DepositActivity.class.getSimpleName())){
			DepositActivity activity = (DepositActivity)mContext;
			activity.loadData(lstSBI);
		}
		if(mContext.getClass().getSimpleName().equals(DepositRegisterContractActivity.class.getSimpleName())){
			DepositRegisterContractActivity activity = (DepositRegisterContractActivity)mContext;
			activity.loadData(lstSBI);
		}
		}
	}
	
	private JSONObject getJsonObject(String result){
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);		
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception

		}
		return null;
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdate(result);			
	}
}
