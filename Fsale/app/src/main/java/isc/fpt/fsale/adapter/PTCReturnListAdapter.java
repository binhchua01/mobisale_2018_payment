package isc.fpt.fsale.adapter;

import isc.fpt.fsale.model.PTCReturnListModel;
import java.util.ArrayList;

import isc.fpt.fsale.action.UpdatePTCStatusForPTCReturn;
import isc.fpt.fsale.R;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class PTCReturnListAdapter extends BaseAdapter {

	private ArrayList<PTCReturnListModel> mList;	
	private Context mContext;
	
	public PTCReturnListAdapter(Context context, ArrayList<PTCReturnListModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final PTCReturnListModel item = mList.get(position);
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_ptc_return_list, null);
			holder.lblRowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			holder.lblContract = (TextView)convertView.findViewById(R.id.lbl_contract);
			holder.lblFullName = (TextView)convertView.findViewById(R.id.lbl_full_name);
			holder.lblAddress = (TextView)convertView.findViewById(R.id.lbl_address);
			holder.lblPTCStatus = (TextView)convertView.findViewById(R.id.lbl_ptc_status);
			holder.lblReturnReason = (TextView)convertView.findViewById(R.id.lbl_return_reason);
			holder.lblDescription = (TextView)convertView.findViewById(R.id.lbl_ptc_return_note);
			holder.lblReturnDate = (TextView)convertView.findViewById(R.id.lbl_ptc_return_date);
			holder.lblReturnTimes = (TextView)convertView.findViewById(R.id.lbl_ptc_return_times);
			holder.btnChangePTCStatus = (Button)convertView.findViewById(R.id.btn_change_ptc_status);	
			holder.lblEmaiList = (TextView)convertView.findViewById(R.id.lbl_ptc_return_email_list);
			holder.btnChangePTCStatus.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String Message = "Bạn có muốn cập nhật?";			
					AlertDialog.Builder builder = null;
					Dialog dialog = null;
					builder = new AlertDialog.Builder(mContext);
					builder.setTitle("Thông báo").setMessage(Message).setCancelable(false).setPositiveButton("Có",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											new UpdatePTCStatusForPTCReturn(mContext, item.getContract(), item.getPTCId(), 0);
										}
									}).setNegativeButton("Không",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,
												int id) {
											dialog.cancel();												
										}
									});
					dialog = builder.create();
					dialog.show();
				}
			});
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		holder.item = item;
		holder.lblRowNumber.setText(String.valueOf(position + 1));
		holder.lblContract.setText(holder.item.getContract());
		holder.lblFullName.setText(holder.item.getFullName());
		holder.lblAddress.setText(holder.item.getAddress());
		holder.lblPTCStatus.setText(holder.item.getPTCStatus());
		holder.lblReturnReason.setText(holder.item.getReason());
		holder.lblDescription.setText(holder.item.getDescription());
		holder.lblReturnDate.setText(holder.item.getReturnDate());
		holder.lblEmaiList.setText(holder.item.getEmailList());
		holder.lblReturnTimes.setText(String.valueOf(holder.item.getReturnTimes()));
		
		return convertView;
	}


	private class ViewHolder{
		public TextView lblRowNumber, lblContract, lblFullName, lblAddress, lblPTCStatus, lblReturnReason, 
					lblDescription, lblReturnDate,lblReturnTimes, lblEmaiList;
		public Button btnChangePTCStatus;
		
		public PTCReturnListModel item;
	}
}
