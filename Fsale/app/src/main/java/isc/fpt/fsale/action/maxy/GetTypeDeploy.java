package isc.fpt.fsale.action.maxy;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.maxytv.model.MaxyTypeDeploy;
import isc.fpt.fsale.ui.maxytv.ui.MaxyTypeDeployActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//Lấy danh sách deploy
public class GetTypeDeploy implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private int RegType;

    public GetTypeDeploy(Context mContext, int regType, int localType, String contract) {
        this.mContext = mContext;
        this.RegType = regType;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("RegType",RegType);
            jsonObject.put("LocationID", Constants.LST_REGION.get(0).getsID());
            jsonObject.put("BranchCode", Constants.BRANCH_CODE);
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("LocalType", localType);
            jsonObject.put("Contract", contract);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.msg_progress_get_type_deploy);
        String GET_MAXY_TYPE_DEPLOY = "GetTypeDeploy";
        CallServiceTask service = new CallServiceTask(mContext, GET_MAXY_TYPE_DEPLOY, jsonObject,
                Services.JSON_POST_OBJECT, message, GetTypeDeploy.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<MaxyTypeDeploy> resultObject = new WSObjectsModel<>(jsObj, MaxyTypeDeploy.class);
                List<MaxyTypeDeploy> lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (mContext != null) {
                    if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                        Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                    } else {
                        ((MaxyTypeDeployActivity) mContext).loadData(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
