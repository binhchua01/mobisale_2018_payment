package isc.fpt.fsale.model;

public class UpdResultModel {
    private String Result;
    private String Message;
    private int ResultID;
    private String ErrorService;
    private int Failure;
    private int DupID;
    private String ClientID;
    private int IgnoreAction = 0;

    public UpdResultModel() {
    }

    public UpdResultModel(String Result, int ResultID, String Message, String ErrorService, int Failure, int IgnoreAction) {
        this.Result = Result;
        this.ResultID = ResultID;
        this.Message = Message;
        this.ErrorService = ErrorService;
        this.Failure = Failure;
        this.IgnoreAction = IgnoreAction;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        this.Result = result;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    public String getErrorService() {
        return ErrorService;
    }

    public void setErrorService(String ErrorService) {
        this.ErrorService = ErrorService;
    }

    public int getResultID() {
        return ResultID;
    }

    public void setResultID(int resultID) {
        this.ResultID = resultID;
    }

    public int getFailure() {
        return Failure;
    }

    public void setFailure(int Failure) {
        this.Failure = Failure;
    }

    public int getIgnoreAction() {
        return IgnoreAction;
    }

    public void setIgnoreAction(int IgnoreAction) {
        this.IgnoreAction = IgnoreAction;
    }

    public int getDupID() {
        return DupID;
    }

    public void setDupID(int DupID) {
        this.DupID = DupID;
    }

    public String getClientID() {
        return ClientID;
    }

    public void setClientID(String ClientID) {
        this.ClientID = ClientID;
    }

}
