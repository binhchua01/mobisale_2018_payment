package isc.fpt.fsale.action.upsell;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.activity.upsell.create_upgrade.CreateInfoUpgradeActicity;
import isc.fpt.fsale.model.upsell.response.UpdateObjModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class UpdateObjUpgrade implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    CreateInfoUpgradeActicity acticity;

    public UpdateObjUpgrade(Context mContext, CreateInfoUpgradeActicity activity, JSONObject jsonObject) {
        this.mContext = mContext;
        this.acticity = activity;
        String message = "Đang cập nhật ...";
        String UPGRADE_OBJ_UPGRADE = "UpdateObjUpgrade";
        CallServiceTask service = new CallServiceTask(mContext, UPGRADE_OBJ_UPGRADE, jsonObject,
                Services.JSON_POST_OBJECT, message, UpdateObjUpgrade.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<UpdateObjModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), UpdateObjModel.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    Common.alertDialogUpgrade(String.valueOf(lst.get(0).getMessage()), mContext);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
