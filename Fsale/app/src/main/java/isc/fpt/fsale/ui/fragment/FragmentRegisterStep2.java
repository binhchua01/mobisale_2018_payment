package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.callback.RegisterCallback;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;

public class FragmentRegisterStep2 extends Fragment {
    public Context mContext;
    public Spinner spCusObjectType, spCusISP, spCusCurrentHabitat, spCusPaymentAbility, spCusPartner, spCusLegalEntity;
    public RadioButton radTVCabStatusYes, radTVCabStatusNo;
    public RadioButton radTVCabDemandYes, radTVCabDemandNo;
    public RegistrationDetailModel modelDetail = null;
    private RegisterCallback callback;
    private ImageButton imgNext, imgBack;

    public static FragmentRegisterStep2 newInstance(RegistrationDetailModel modelDetail, RegisterCallback callback) {
        FragmentRegisterStep2 fragment = new FragmentRegisterStep2(modelDetail, callback);
        return fragment;
    }

    @SuppressLint("ValidFragment")
    public FragmentRegisterStep2(RegistrationDetailModel modelDetail, RegisterCallback callback) {
        // TODO Auto-generated constructor stub
        super();
        this.modelDetail = modelDetail;
        this.callback = callback;
    }

    public FragmentRegisterStep2() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_register_step2, container, false);
        this.mContext = getActivity();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        if (getArguments() != null) {
//            this.modelDetail = getArguments().getParcelable("data");
//            this.callback = getArguments().getParcelable("callback");
//        }
        initView(getView());
        initSpinners();
        addEventControl();
        initControlElses();
    }

    private void initView(View view) {
        //img next button
        imgNext = (ImageButton) view.findViewById(R.id.img_next);
        imgNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                callback.changePage(2);
            }
        });

        imgBack = (ImageButton) view.findViewById(R.id.img_back);
        imgBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                callback.changePage(0);
            }
        });
        // Loại khách hàng
        spCusObjectType = (Spinner) view.findViewById(R.id.sp_object_type);
        //ISP
        spCusISP = (Spinner) view.findViewById(R.id.sp_cus_isp_type);
        spCusISP.setEnabled(false);
        spCusCurrentHabitat = (Spinner) view.findViewById(R.id.sp_cus_current_habitat);
        spCusPaymentAbility = (Spinner) view.findViewById(R.id.sp_cus_solvency);
        spCusPartner = (Spinner) view.findViewById(R.id.sp_cus_partner);
        spCusLegalEntity = (Spinner) view.findViewById(R.id.sp_cus_legal_entity);
        //Tình trạng truyền hình cáp Đã có
        radTVCabStatusYes = (RadioButton) view.findViewById(R.id.rad_cus_tv_cab_status_yes);
        //Tình trạng truyền hình cáp Chưa có
        radTVCabStatusNo = (RadioButton) view.findViewById(R.id.rad_cus_tv_cab_status_no);
        //Có nhu cầu lắp đặt truyền hình Có nhu cầu
        radTVCabDemandYes = (RadioButton) view.findViewById(R.id.rad_cus_tv_cab_demand_yes);
        //Có nhu cầu lắp đặt truyền hình Không có nhu cầu
        radTVCabDemandNo = (RadioButton) view.findViewById(R.id.rad_cus_tv_cab_demand_no);
    }

    private void initSpinners() {
        initCusObjectType();
        initCusISPSpinner();
        initCusCurrentHabitat();
        initCusPaymentAbility();
        initCusPartner();
        initCusLegalEntity();
    }

    private void addEventControl() {
        spCusObjectType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parent.getItemAtPosition(position);
                if (selectedItem.getID() == 2) {
                    spCusISP.setEnabled(true);
                } else {
                    spCusISP.setEnabled(false);
                    spCusISP.setSelection(0, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
    }

    //khởi tạo Pháp nhân
    private void initCusLegalEntity() {
        ArrayList<KeyValuePairModel> lstLegalEntity = new ArrayList<KeyValuePairModel>();
        lstLegalEntity.add(new KeyValuePairModel(1, "FPT"));
        lstLegalEntity.add(new KeyValuePairModel(2, "Đối tác"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstLegalEntity, Gravity.LEFT);
        spCusLegalEntity.setAdapter(adapter);
        if (modelDetail != null) {
            try {
                spCusLegalEntity.setSelection(Common.getIndex(spCusLegalEntity, modelDetail.getLegalEntity()), true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //khởi tạo Khách hàng của đối tác
    private void initCusPartner() {
        ArrayList<KeyValuePairModel> lstCusPartner = new ArrayList<KeyValuePairModel>();
        lstCusPartner.add(new KeyValuePairModel(0, "[Đối tác]"));
        lstCusPartner.add(new KeyValuePairModel(1, "SST"));
        lstCusPartner.add(new KeyValuePairModel(2, "BTS"));
        lstCusPartner.add(new KeyValuePairModel(3, "VITA"));
        lstCusPartner.add(new KeyValuePairModel(5, "KNT"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstCusPartner, Gravity.LEFT);
        spCusPartner.setAdapter(adapter);
        if (modelDetail != null) {
            try {
                spCusPartner.setSelection(Common.getIndex(spCusPartner, modelDetail.getPartnerID()), true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // khởi tạo Khả năng thanh toán
    private void initCusPaymentAbility() {
        ArrayList<KeyValuePairModel> lstCusSolvency = new ArrayList<>();
        lstCusSolvency.add(new KeyValuePairModel(1, "Cao"));
        lstCusSolvency.add(new KeyValuePairModel(2, "Trung bình"));
        lstCusSolvency.add(new KeyValuePairModel(3, "Thấp"));
        lstCusSolvency.add(new KeyValuePairModel(4, "Rất thấp"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, lstCusSolvency, Gravity.LEFT);
        spCusPaymentAbility.setAdapter(adapter);
        if (modelDetail != null) {
            if (modelDetail.getPaymentAbility() != null && !modelDetail.getPaymentAbility().equals("")) {
                try {
                    spCusPaymentAbility.setSelection(Common.getIndex(spCusPaymentAbility, Integer.parseInt(modelDetail.getPaymentAbility())), true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // khởi tạo Nơi ở hiện tại
    private void initCusCurrentHabitat() {
        ArrayList<KeyValuePairModel> lstCusHabitat = new ArrayList<>();
        lstCusHabitat.add(new KeyValuePairModel(1, "Ở trọ, ở thuê"));
        lstCusHabitat.add(new KeyValuePairModel(2, "Định cư lâu dài"));
        lstCusHabitat.add(new KeyValuePairModel(3, "Ký túc xá"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, lstCusHabitat, Gravity.LEFT);
        spCusCurrentHabitat.setAdapter(adapter);
        if (modelDetail != null) {
            if (modelDetail.getCurrentHouse() != null && !modelDetail.getCurrentHouse().equals("")) {
                try {
                    spCusCurrentHabitat.setSelection(Common.getIndex(spCusCurrentHabitat, Integer.parseInt(modelDetail.getCurrentHouse())), true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //khởi tạo ISP
    private void initCusISPSpinner() {
        ArrayList<KeyValuePairModel> lstISP = new ArrayList<>();
        lstISP.add(new KeyValuePairModel(0, mContext.getResources().getString(R.string.lbl_hint_spinner_none)));
        lstISP.add(new KeyValuePairModel(1, "Viettel"));
        lstISP.add(new KeyValuePairModel(2, "VNPT"));
        lstISP.add(new KeyValuePairModel(3, "Netnam"));
        lstISP.add(new KeyValuePairModel(4, "CMC"));
        lstISP.add(new KeyValuePairModel(5, "SCTV"));
        lstISP.add(new KeyValuePairModel(6, "HCTV"));
        lstISP.add(new KeyValuePairModel(7, "Khác"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstISP, Gravity.LEFT);
        spCusISP.setAdapter(adapter);
        if (modelDetail != null) {
            try {
                spCusISP.setSelection(Common.getIndex(spCusISP, modelDetail.getISPType()), true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //khởi tạo Đối tượng KH
    private void initCusObjectType() {
        ArrayList<KeyValuePairModel> lstCusDetail = new ArrayList<>();
        lstCusDetail.add(new KeyValuePairModel(1, "Khách hàng mới hoàn toàn"));
        lstCusDetail.add(new KeyValuePairModel(2, "Khách hàng từ ISP khác"));
        lstCusDetail.add(new KeyValuePairModel(3, "Khách hàng FPT hủy cũ, ký lại"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstCusDetail, Gravity.LEFT);
        spCusObjectType.setAdapter(adapter);
        if (modelDetail != null) {
            try {
                spCusObjectType.setSelection(Common.getIndex(spCusObjectType, modelDetail.getObjectType()), true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //khởi tạo Tình trạng truyền hình cáp, Có nhu cầu lặp đặt truyền hình
    @SuppressLint("DefaultLocale")
    private void initControlElses() {
        if (modelDetail != null) {
            if (modelDetail.getCableStatus().equals("1"))
                radTVCabStatusYes.setChecked(true);
            else
                radTVCabStatusNo.setChecked(true);

            if (modelDetail.getINSCable() == 1)
                radTVCabDemandYes.setChecked(true);
            else
                radTVCabDemandNo.setChecked(true);

        }
    }

}
