package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 5/2/2018.
 */

public class CheckStatusPaymentPost implements Parcelable {
    //Declares variables
    private String saleMan;
    private String regCode;
    private String contract;

    /**
     * Constructor
     */
    public CheckStatusPaymentPost() {
    }

    protected CheckStatusPaymentPost(Parcel in) {
        saleMan = in.readString();
        regCode = in.readString();
        contract = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(saleMan);
        dest.writeString(regCode);
        dest.writeString(contract);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CheckStatusPaymentPost> CREATOR = new Creator<CheckStatusPaymentPost>() {
        @Override
        public CheckStatusPaymentPost createFromParcel(Parcel in) {
            return new CheckStatusPaymentPost(in);
        }

        @Override
        public CheckStatusPaymentPost[] newArray(int size) {
            return new CheckStatusPaymentPost[size];
        }
    };

    public String getSaleMan() {
        return saleMan;
    }

    public void setSaleMan(String saleMan) {
        this.saleMan = saleMan;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }
}
