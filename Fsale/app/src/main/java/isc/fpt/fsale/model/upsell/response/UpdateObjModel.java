package isc.fpt.fsale.model.upsell.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateObjModel implements Parcelable {
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("TransID")
    @Expose
    private Integer transID;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTransID() {
        return transID;
    }

    public void setTransID(Integer transID) {
        this.transID = transID;
    }

    protected UpdateObjModel(Parcel in) {
        message = in.readString();
        if (in.readByte() == 0) {
            transID = null;
        } else {
            transID = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        if (transID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(transID);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UpdateObjModel> CREATOR = new Creator<UpdateObjModel>() {
        @Override
        public UpdateObjModel createFromParcel(Parcel in) {
            return new UpdateObjModel(in);
        }

        @Override
        public UpdateObjModel[] newArray(int size) {
            return new UpdateObjModel[size];
        }
    };
}
