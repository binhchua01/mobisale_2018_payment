package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.lang.reflect.Field;

/**
 * Created by HCM.TUANTT14 on 1/18/2018.
 */

public class FPTBox implements Parcelable {
    private int OTTCount;
    private int OTTID;
    private int OTTPrice;
    private int PromotionID;
    private String PromotionText;
    private String OTTName;

    public FPTBox() {

    }

    public FPTBox(int OTTCount, int OTTID, int OTTPrice, int PromotionID, String PromotionText, String OTTName) {
        this.OTTCount = OTTCount;
        this.OTTID = OTTID;
        this.OTTPrice = OTTPrice;
        this.PromotionID = PromotionID;
        this.PromotionText = PromotionText;
        this.OTTName = OTTName;
    }

    public FPTBox(Parcel in) {
        this.OTTCount = in.readInt();
        this.OTTID = in.readInt();
        this.OTTPrice = in.readInt();
        this.PromotionID = in.readInt();
        this.PromotionText = in.readString();
        this.OTTName = in.readString();
    }

    public int getOTTCount() {
        return OTTCount;
    }

    public void setOTTCount(int OTTCount) {
        this.OTTCount = OTTCount;
    }

    public int getOTTID() {
        return OTTID;
    }

    public void setOTTID(int OTTID) {
        this.OTTID = OTTID;
    }

    public int getOTTPrice() {
        return OTTPrice;
    }

    public void setOTTPrice(int OTTPrice) {
        this.OTTPrice = OTTPrice;
    }

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int promotionID) {
        PromotionID = promotionID;
    }

    public String getPromotionText() {
        return PromotionText;
    }

    public void setPromotionText(String promotionText) {
        PromotionText = promotionText;
    }

    public String getOTTName() {
        return OTTName;
    }

    public void setOTTName(String OTTName) {
        this.OTTName = OTTName;
    }

    public static final Creator<FPTBox> CREATOR = new Creator<FPTBox>() {
        @Override
        public FPTBox createFromParcel(Parcel in) {
            return new FPTBox(in);
        }

        @Override
        public FPTBox[] newArray(int size) {
            return new FPTBox[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public JSONObject toJSONObject() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("OTTCount", this.OTTCount);
            jsonObj.put("OTTID", this.OTTID);
            jsonObj.put("PromotionID", this.PromotionID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObj;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(OTTCount);
        parcel.writeInt(OTTID);
        parcel.writeInt(OTTPrice);
        parcel.writeInt(PromotionID);
        parcel.writeString(PromotionText);
        parcel.writeString(OTTName);
    }

    public String toString() {
        JSONObject jsonObj = new JSONObject();
        try {
            Field[] fields = getClass().getDeclaredFields();
            for (Field f : fields) {
                f.setAccessible(true);
                jsonObj.put(f.getName(), f.get(this));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj.toString();
    }
}
