package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.util.Log;

public class InsertBookPortFTTH implements AsyncTaskCompleteListener<String>{ 

	private final String InsertBookPortFTTH = "BookPortFTTH";
	private final String InsertBookPortFTTH_RESULT = "BookPortFTTHResult";
	private final String ERRORSERVICE = "ErrorService";
	private final String RESULT = "Result";
	private Context mContext;
	private int ID;
	private Location locationCurrentDevice;
	public InsertBookPortFTTH(Context mContext,String[] paramsValue,String _ID) 
	{
		this.mContext = mContext;	
		try {
			this.ID= Integer.valueOf(_ID);
		} catch (Exception e) {
			// TODO: handle exception

			ID = 0;
		}
		
		InsertFTTH(paramsValue);
	}
	
	public void InsertFTTH(String[] paramsValue)
	{		
		String[] params = new String[]{"RegCode","CardNumber","UserName","TDID","IDCoreTD","TDName","PortTextTD",
				"LocationID","BranchID","IP","TypeCore","TypeContract","Latlng","IDCoreSC","LatlngDevice", "LatlngMarker"};
		String message = "Vui lòng đợi giây lát";
		CallServiceTask service = new CallServiceTask(mContext,InsertBookPortFTTH,params,paramsValue, Services.JSON_POST, message, InsertBookPortFTTH.this);
		service.execute();
	}
	
	public void handleUpdateRegistration(String json){		
		if(json != null && Common.jsonObjectValidate(json)){			
			 try {	
				   JSONArray jsArr;
				    JSONObject jsObj = JSONParsing.getJsonObj(json);
				    jsArr = jsObj.getJSONArray(InsertBookPortFTTH_RESULT);
				    int l = jsArr.length();
				    if (l > 0) 
				    {
				    	 String error = jsArr.getJSONObject(0).getString(ERRORSERVICE);
					     if (error.equals("null")) 
					     {
						      String result = jsArr.getJSONObject(0).getString(RESULT);
						      int ResultID=jsArr.getJSONObject(0).getInt("ResultID");
						      if(ResultID==1)
						      {
						    	  new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage("BookPort thành công !")
					 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
					 				    @Override
					 				    public void onClick(DialogInterface dialog, int which) { 
						 				    
						 				    //Close map
					 				    	try {
					 				    		new GetRegistrationDetail(mContext,Constants.USERNAME,ID);
					 				    		dialog.cancel();
												//((Activity)mContext).finish();
											} catch (Exception e) {

												Log.i("InsertBookPortFTTH", "_handleUpdateRegistration=" + e.getMessage());
											}
					 				       
					 				    }
					 				})
				 					.setCancelable(false).create().show();
						    	 
						      }
						      else
						      {
						    	 // new GetRegistrationDetail(mContext,"oanh254",this.ID);
						    	 Common.alertDialog(result, mContext);
						      }
					     }
					     else
					    	 Common.alertDialog(error, mContext);
				    }
			 }
			 catch (Exception e)
			 {

				e.printStackTrace();
				Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
			 }
		}		
		else Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);		
	}
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);
	}

}
