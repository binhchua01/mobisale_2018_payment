package isc.fpt.fsale.map.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import isc.fpt.fsale.R;
import isc.fpt.fsale.map.activity.BookPortManualActivity;
import isc.fpt.fsale.map.activity.MapBookPortActivity;
import isc.fpt.fsale.utils.Common;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * CLASS: 			GetRouteTask
 * @description: 	handle route direction response data: draw route direction line on map
 * @author: 		vandn
 * @created on:		09/08/2013
 * */
public class GetGeocoder extends AsyncTask<String, Void, String>{
	private Context mContext;
	private String locationName;
	private ProgressDialog pd;
	
	private final String TAG_RESULT = "results", TAG_GEOMETRY = "geometry", TAG_LOCATION = "location", TAG_LAT = "lat", TAG_LNG = "lng";
      
	public GetGeocoder(Context mContext,String locationName){
    	this.mContext = mContext;    	
    	this.locationName = locationName;
    }
    @Override
    protected void onPreExecute() {
        pd = Common.showProgressBar(this.mContext, mContext.getResources().getString(R.string.msg_pd_search));
    }

    @Override
    protected String doInBackground(String... urls) {
          //Get All Route values
          return getFromLocationName();
    }

    @Override
    protected void onPostExecute(String result) {
    	try{
    		pd.dismiss();
    		if(result != null){
    		  JSONObject jsonObject = new JSONObject();
    			try {
    				jsonObject = new JSONObject(result);
    				if(jsonObject.has(TAG_RESULT)){
    					JSONArray jsonArray = jsonObject.getJSONArray(TAG_RESULT);    					
    					if(jsonArray.length() > 0){
    						jsonObject = jsonArray.getJSONObject(0);
    						jsonObject = jsonObject.getJSONObject(TAG_GEOMETRY);
    						jsonObject = jsonObject.getJSONObject(TAG_LOCATION);
    						LatLng cityLocation = new LatLng(jsonObject.getDouble(TAG_LAT), jsonObject.getDouble(TAG_LNG));
    						udpateLocation(cityLocation);
    					}    					
    				}    				
    			} catch (JSONException e) {
    				// TODO Auto-generated catch block

    				e.printStackTrace();
    			}
          }
          else{
        	  Common.alertDialog("Kết nối đến trợ giúp chỉ đường của google thất bại. Vui lòng thử lại sau", mContext);
          }
    	}
    	catch(Exception e){

    		e.printStackTrace();
    	}
    }
    
    private String getFromLocationName(){
		StringBuilder stringBuilder = new StringBuilder();
		String url = "http://maps.google.com/maps/api/geocode/json?"
	                + "address=" + locationName.replace(" ", "%20") + "ka&sensor=false";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(url);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            InputStream in = response.getEntity().getContent();
            int b;
			while ((b = in.read()) != -1) {
				stringBuilder.append((char) b);
			}				
            return stringBuilder.toString();
        } catch (Exception e) {

            e.printStackTrace();
        }
        return null;
    }
    
    private void udpateLocation(LatLng location){
    	try {
			if(mContext.getClass().getSimpleName().equals(MapBookPortActivity.class.getSimpleName())){
				MapBookPortActivity activity = (MapBookPortActivity)mContext;
				activity.setCityLocation(location);
			}else if(mContext.getClass().getSimpleName().equals(BookPortManualActivity.class.getSimpleName())){
				BookPortManualActivity activity = (BookPortManualActivity)mContext;
				activity.setCityLocation(location);
			}
			
		} catch (Exception e) {
			// TODO: handle exception

		}
    }
    
    /*
     * 
     http://maps.google.com/maps/api/geocode/json?address=Ho%20Chi%20Minhka&sensor=false
     
     	{
		   "results" : [
		      {
		         "address_components" : [
		            {
		               "long_name" : "Hồ Chí Minh",
		               "short_name" : "Hồ Chí Minh",
		               "types" : [ "locality", "political" ]
		            },
		            {
		               "long_name" : "Hồ Chí Minh",
		               "short_name" : "Hồ Chí Minh",
		               "types" : [ "administrative_area_level_1", "political" ]
		            },
		            {
		               "long_name" : "Việt Nam",
		               "short_name" : "VN",
		               "types" : [ "country", "political" ]
		            }
		         ],
		         "formatted_address" : "Hồ Chí Minh, Hồ Chí Minh, Việt Nam",
		         "geometry" : {
		            "bounds" : {
		               "northeast" : {
		                  "lat" : 11.1602136,
		                  "lng" : 107.0248468
		               },
		               "southwest" : {
		                  "lat" : 10.3766885,
		                  "lng" : 106.3638784
		               }
		            },
		            "location" : {
		               "lat" : 10.8230989,
		               "lng" : 106.6296638
		            },
		            "location_type" : "APPROXIMATE",
		            "viewport" : {
		               "northeast" : {
		                  "lat" : 11.1602136,
		                  "lng" : 107.0248468
		               },
		               "southwest" : {
		                  "lat" : 10.3766885,
		                  "lng" : 106.3638784
		               }
		            }
		         },
		         "partial_match" : true,
		         "place_id" : "ChIJ0T2NLikpdTERKxE8d61aX_E",
		         "types" : [ "locality", "political" ]
		      }
		   ],
		   "status" : "OK"
		}
     * 
     * 
     */
    
    public static List<Address> getAddress(Context context, LatLng location) {
    	//"http://maps.googleapis.com/maps/api/geocode/json?latlng=44.4647452,7.3553838&sensor=true"
    	Geocoder geocoder = new Geocoder(context, Locale.getDefault());
    	//List<Address> addressList = new ArrayList<Address>();
    	try {
    		return geocoder.getFromLocation(location.latitude, location.longitude, 1);
		} catch (IOException e1) {
			// TODO Auto-generated catch block

			e1.printStackTrace();
		}
    	/*StringBuilder stringBuilder = new StringBuilder();
    	String latlng = location.latitude + "," + location.longitude;
        String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlng + "&sensor=true";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(url);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            InputStream in = response.getEntity().getContent();
            int b;
			while ((b = in.read()) != -1) {
				stringBuilder.append((char) b);
			}				
            //stringBuilder.toString();
			JSONObject json = new JSONObject(stringBuilder.toString());
			if(json.has("results"))
				addressList = ParseJsonModel.parse(json.getJSONArray("json"), Address.class);
			return addressList;
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        return null;
    }
    
}


