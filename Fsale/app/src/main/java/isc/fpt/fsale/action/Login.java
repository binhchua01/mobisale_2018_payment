package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UserModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/*
 * ACTION: 	 	 Login
 * description: - pass value and call service to do login
 * - handle response result: show main menu if login successful, show error des if login failed
 * author: vandn, on 13/08/2013
 */
public class Login implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public Login(Context mContext, String[] arrParams) {
        this.mContext = mContext;
        String[] params = new String[]{"Username", "Password", "SimKey", "DeviceKey"};
        String message = mContext.getResources().getString(R.string.msg_pd_login);
        String LOGIN = "Login";
        CallServiceTask service = new CallServiceTask(mContext, LOGIN, params,
                arrParams, Services.JSON_POST, message, Login.this);
        service.execute();
    }

    @SuppressLint("DefaultLocale")
    private void handleLoginResult(String json) {
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            UserModel user = new UserModel();
            try {
                String TAG_LOGIN_RESULT = "LoginResult";
                JSONObject obj = jsObj.getJSONObject(TAG_LOGIN_RESULT);
                String TAG_LOGIN_ERROR = "ErrorService";
                String error = obj.getString(TAG_LOGIN_ERROR);
                if (error.equals("null")) {
                    String TAG_LOGIN_USERNAME = "UserName";
                    user.setUsername(obj.getString(TAG_LOGIN_USERNAME));
                    String TAG_LOGIN_IS_ACTIVE = "IsActive";
                    user.setIsActive(obj.getInt(TAG_LOGIN_IS_ACTIVE));
                    String TAG_LOGIN_ERROR_CODE = "ErrorCode";
                    user.setErrorCode(obj.getInt(TAG_LOGIN_ERROR_CODE));
                    String TAG_LOGIN_DES = "Description";
                    user.setDescription(obj.getString(TAG_LOGIN_DES));
                    String TAG_LOGIN_IS_CREATE_CONTRACT = "IsCreateContract";
                    if (obj.has(TAG_LOGIN_IS_CREATE_CONTRACT))
                        user.setIsCreateContact(obj.getInt(TAG_LOGIN_IS_CREATE_CONTRACT));
                    if ((user.getErrorCode() == 0) && (user.getIsActive() == 1)) {
                        Constants.USERNAME = user.getUsername();
                        try {
                            Common.savePreference(mContext, Constants.SHARE_PRE_GROUP_USER,
                                    Constants.SHARE_PRE_GROUP_USER_NAME, user.getUsername());
                            ((MyApp) mContext.getApplicationContext()).setUser(user);
                            ((MyApp) mContext.getApplicationContext()).setUserName(user.getUsername());
                            ((MyApp) mContext.getApplicationContext()).setIsLogIn(true);
                            ((MyApp) mContext.getApplicationContext()).setIsLogOut(false);
                            if (user.getDescription().toLowerCase().contains("admin".toLowerCase())) {
                                ((MyApp) mContext.getApplicationContext()).setIsAdmin(true);
                            } else {
                                ((MyApp) mContext.getApplicationContext()).setIsAdmin(false);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        new GetLocationList(mContext);
                    } else {
                        Common.alertDialog(user.getDescription(), mContext);
                    }
                } else {
                    Common.alertDialog("Lỗi Webservice: " + error, mContext);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleLoginResult(result);
    }
}
