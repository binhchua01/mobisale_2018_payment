package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.ui.fpt_camera.fragment.FptCameraTotal;
import isc.fpt.fsale.model.PriceCamTotal;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 07,January,2019
 */
public class GetPriceCamTotal implements AsyncTaskCompleteListener<String> {
    private FptCameraTotal fragment;
    private RegisterContractActivity activity;
    private boolean isSellMore;
    private final String GET_PRICE_CAM_TOTAL = "GetPriceCamTotal";

    public GetPriceCamTotal(Context mContext, FptCameraTotal fragment, boolean isSellMore, JSONObject jsonObject) {
        this.fragment = fragment;
        this.isSellMore = isSellMore;
        String message = mContext.getResources().getString(R.string.msg_pd_update);
        CallServiceTask service = new CallServiceTask(mContext, GET_PRICE_CAM_TOTAL, jsonObject, Services.JSON_POST_OBJECT,
                message, GetPriceCamTotal.this);
        service.execute();
    }

    public GetPriceCamTotal(RegisterContractActivity activity, boolean isSellMore, JSONObject jsonObject) {
        this.activity = activity;
        this.isSellMore = isSellMore;
        String message = activity.getResources().getString(R.string.msg_pd_update);
        CallServiceTask service = new CallServiceTask(activity, GET_PRICE_CAM_TOTAL,
                jsonObject, Services.JSON_POST_OBJECT, message, GetPriceCamTotal.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if(Common.jsonObjectValidate(result)){
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<PriceCamTotal> resultObject = new WSObjectsModel<>(jsObj, PriceCamTotal.class);
                List<PriceCamTotal> lst = resultObject.getListObject();
                //callback to activity
                if(lst.size() == 0){
                    return;
                }
                if(isSellMore){
                    activity.loadFptCameraTotal(lst.get(0));
                }else{
                    fragment.loadFptCameraTotal(lst.get(0));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
