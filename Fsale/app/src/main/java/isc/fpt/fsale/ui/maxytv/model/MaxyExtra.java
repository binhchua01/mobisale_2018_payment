package isc.fpt.fsale.ui.maxytv.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class MaxyExtra implements Parcelable {
    private int ID;
    private String Name;
    private int PrepaidMonth;
    private int ChargeTimes;
    //6:Theo hợp đồng 1:Theo box
    private int ExtraType;
    private int ExtraCount;
    private int ServiceCode;
    private int Price;
    private int Visible;

    private boolean isSelected = false;

    public MaxyExtra(int ID, String name, int prepaidMonth, int chargeTimes, int extraType, int extraCount, int serviceCode, int price, int visible, boolean isSelected) {
        this.ID = ID;
        Name = name;
        PrepaidMonth = prepaidMonth;
        ChargeTimes = chargeTimes;
        ExtraType = extraType;
        ExtraCount = extraCount;
        ServiceCode = serviceCode;
        Price = price;
        Visible = visible;
        this.isSelected = isSelected;
    }

    public MaxyExtra() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getPrepaidMonth() {
        return PrepaidMonth;
    }

    public void setPrepaidMonth(int prepaidMonth) {
        PrepaidMonth = prepaidMonth;
    }

    public int getChargeTimes() {
        return ChargeTimes;
    }

    public void setChargeTimes(int chargeTimes) {
        ChargeTimes = chargeTimes;
    }

    public int getExtraType() {
        return ExtraType;
    }

    public void setExtraType(int extraType) {
        ExtraType = extraType;
    }

    public int getExtraCount() {
        return ExtraCount;
    }

    public void setExtraCount(int extraCount) {
        ExtraCount = extraCount;
    }

    public int getServiceCode() {
        return ServiceCode;
    }

    public void setServiceCode(int serviceCode) {
        ServiceCode = serviceCode;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public int getVisible() {
        return Visible;
    }

    public void setVisible(int visible) {
        Visible = visible;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public static Creator<MaxyExtra> getCREATOR() {
        return CREATOR;
    }

    protected MaxyExtra(Parcel in) {
        ID = in.readInt();
        Name = in.readString();
        PrepaidMonth = in.readInt();
        ChargeTimes = in.readInt();
        ExtraType = in.readInt();
        ExtraCount = in.readInt();
        ServiceCode = in.readInt();
        Price = in.readInt();
        Visible = in.readInt();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeString(Name);
        dest.writeInt(PrepaidMonth);
        dest.writeInt(ChargeTimes);
        dest.writeInt(ExtraType);
        dest.writeInt(ExtraCount);
        dest.writeInt(ServiceCode);
        dest.writeInt(Price);
        dest.writeInt(Visible);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MaxyExtra> CREATOR = new Creator<MaxyExtra>() {
        @Override
        public MaxyExtra createFromParcel(Parcel in) {
            return new MaxyExtra(in);
        }

        @Override
        public MaxyExtra[] newArray(int size) {
            return new MaxyExtra[size];
        }
    };

    public JSONObject toJsonObjectMaxyExtra() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ID", getID());
            jsonObject.put("Name", getName());
            jsonObject.put("PrepaidMonth", getPrepaidMonth());
            jsonObject.put("ChargeTimes", getChargeTimes());
            jsonObject.put("ExtraType", getExtraType());
            //jsonObject.put("ExtraCount", getExtraCount());
            jsonObject.put("ServiceCode", getServiceCode());
            jsonObject.put("Price", getPrice());
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}