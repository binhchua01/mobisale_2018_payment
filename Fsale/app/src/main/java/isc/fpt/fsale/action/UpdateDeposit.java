package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.RegisterCollectMoneyDialog;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class UpdateDeposit implements AsyncTaskCompleteListener<String> {

	private String[] arrParamName, arrParamValue;	
	private Context mContext;
	private final String TAG_METHOD_NAME = "UpdateDeposit";
	private final String TAG_RESULT_LIST = "ListObject", TAG_RESULT = "Result", TAG_RESULT_ID = "ResultID", TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";
	private int RegID = 0;
	private RegisterCollectMoneyDialog mDialog = null;
	
	public UpdateDeposit(Context context, RegisterCollectMoneyDialog dialog, String RegCode, String SBI_Internet, String SBI_IPTV, int Total, int RegID){	
		try {
			mContext = context;
			this.RegID = RegID;
			mDialog = dialog;
			arrParamName = new String[]{"Username", "RegCode","SBI_Internet","SBI_IPTV","Total"};
			this.arrParamValue = new String[]{Constants.USERNAME, RegCode, SBI_Internet, SBI_IPTV, String.valueOf(Total)};
			
			String message = "Đang cập nhật...";
			CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, UpdateDeposit.this);
			service.execute();
		} catch (Exception e) {

			e.getMessage();
		}		
	}
	
	public UpdateDeposit(Context context, String RegCode, String SBI_Internet, String SBI_IPTV, int Total, int RegID){	
		try {
			mContext = context;
			this.RegID = RegID;
			arrParamName = new String[]{"Username", "RegCode","SBI_Internet","SBI_IPTV","Total"};
			this.arrParamValue = new String[]{Constants.USERNAME, RegCode, SBI_Internet, SBI_IPTV, String.valueOf(Total)};
			String message = "Đang cập nhật...";
			CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, UpdateDeposit.this);
			service.execute();
		} catch (Exception e) {

			e.getMessage();
		}		
	}
	public  UpdateDeposit(Context context, String RegCode, String SBI_Internet, String SBI_IPTV, int Total, int RegID, String ImageSignature,int payType){
		try {
			mContext = context;
			this.RegID = RegID;
			arrParamName = new String[]{"Username", "RegCode","SBI_Internet","SBI_IPTV","Total","ImageSignature","PaidType"};
			this.arrParamValue = new String[]{Constants.USERNAME, RegCode, SBI_Internet, SBI_IPTV, String.valueOf(Total), ImageSignature, String.valueOf(payType)};
			String message = "Đang cập nhật...";
			CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, UpdateDeposit.this);
			service.execute();
		} catch (Exception e) {

			e.getMessage();
		}		
	}
	
	public void handleUpdateRegistration(String json){
		if(json != null && Common.jsonObjectValidate(json)){
		JSONObject jsObj = getJsonObject(json);				
		if(jsObj != null){
			try {
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);	
				/*ArrayList<KeyValuePairModel> lstSBI = new ArrayList<KeyValuePairModel>();*/				
				if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
					JSONArray resultArray = jsObj.getJSONArray(TAG_RESULT_LIST);
					if(resultArray != null && resultArray.length()>0){			
						int resultID = resultArray.getJSONObject(0).getInt(TAG_RESULT_ID);
						String result = resultArray.getJSONObject(0).getString(TAG_RESULT);
						//Cap nhat thanh cong
						if(resultID > 0){
							 new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(result)
				 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
				 				    @Override
				 				    public void onClick(DialogInterface dialog, int which) {			 				      
				 				    	try {				 				    		
				 				    		if(mDialog != null)
				 				    			mDialog.dismiss();
				 				    		new GetRegistrationDetail(mContext, Constants.USERNAME, RegID);				 				    		
										} catch (Exception e) {
											// TODO: handle exception
											e.printStackTrace();
										}
				 				      
				 				       dialog.cancel();
				 				    }
				 				})
			 					.setCancelable(false).create().show();	
						}else{
							Common.alertDialog(resultArray.getJSONObject(0).getString(TAG_RESULT), mContext);
						}
						try {
						} catch (Exception e) {
							// TODO: handle exception
						}
					}						
									
				}else{
					Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
				}				
			} catch (Exception e) {

			}
			
		}else{
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + 
					"-" + Constants.RESPONSE_RESULT, mContext);
		}
		}
	}
	
	private JSONObject getJsonObject(String result){
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);		
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception

		}
		return null;
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);			
	}
}
