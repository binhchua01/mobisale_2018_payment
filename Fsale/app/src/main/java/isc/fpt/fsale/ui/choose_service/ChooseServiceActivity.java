package isc.fpt.fsale.ui.choose_service;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListCategoryService;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.extra_ott.ExtraOttActivity;
import isc.fpt.fsale.ui.fpt_camera.FptCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.FptCameraSelectComboActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class ChooseServiceActivity extends BaseActivitySecond
        implements OnItemClickListener<ServiceType> {
    private RelativeLayout rltBack;
    private List<ServiceType> mList;
    private ChooseServiceAdapter mAdapter;
    private PotentialObjModel mPotentialObjModel;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_choose_service;
    }

    @Override
    protected void initView() {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_service);
        rltBack = (RelativeLayout) findViewById(R.id.layout_back);
        mList = new ArrayList<>();
        mAdapter = new ChooseServiceAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);
        getDataIntent();
    }

    private void getDataIntent() {
        if(getIntent().getParcelableExtra(Constants.POTENTIAL_OBJECT) != null){
            mPotentialObjModel = getIntent().getParcelableExtra(Constants.POTENTIAL_OBJECT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetListCategoryService(this, Constants.USERNAME);
    }

    public void loadListCatService(List<ServiceType> mList) {
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }


    @Override
    public void onItemClick(ServiceType object) {
        int id = object.getCategoryServiceGroupID();
        Intent intent;
        switch (id){
            case 1:
                intent = new Intent(this, RegisterActivityNew.class);
                intent.putExtra(Constants.POTENTIAL_OBJECT, mPotentialObjModel);
                intent.putParcelableArrayListExtra(Constants.LIST_CAT_SERVICE,
                        (ArrayList<? extends Parcelable>) object.getCategoryServiceList());
                startActivity(intent);
                finish();
                break;
            case 2:
                intent = new Intent(this, RegisterActivityNew.class);
                intent.putExtra(Constants.POTENTIAL_OBJECT, mPotentialObjModel);
                intent.putParcelableArrayListExtra(Constants.LIST_CAT_SERVICE,
                        (ArrayList<? extends Parcelable>) object.getCategoryServiceList());
                startActivity(intent);
                finish();
                break;
            case 3:
                Bundle bundleFptCamera = new Bundle();
                bundleFptCamera.putParcelableArrayList(Constants.LIST_CAT_SERVICE,
                        (ArrayList<? extends Parcelable>) object.getCategoryServiceList());
                bundleFptCamera.putParcelable(Constants.POTENTIAL_OBJECT, mPotentialObjModel);
                Common.getInstance().changeActivityFinish(this, FptCameraSelectComboActivity.class, bundleFptCamera);
                finish();
                break;
            case 4:
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Constants.LIST_CAT_SERVICE,
                        (ArrayList<? extends Parcelable>) object.getCategoryServiceList());
                bundle.putParcelable(Constants.POTENTIAL_OBJECT, mPotentialObjModel);
                Common.getInstance().changeActivityFinish(this, ExtraOttActivity.class, bundle);
                break;
        }
    }
}
