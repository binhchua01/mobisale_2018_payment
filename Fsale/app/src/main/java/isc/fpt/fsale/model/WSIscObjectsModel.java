package isc.fpt.fsale.model;

import java.util.ArrayList;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class WSIscObjectsModel<T> {

    private List<T> details;
    private static final String TAG_PAGE = "page";
    private static final String TAG_PAGE_TOTAL = "total";
    private static final String TAG_RESPONE_STATUS = "responestatus";
    private static final String TAG_RESPONE_MESSAGE = "responemessage";
    private static final String TAG_DETAIL_LIST = "detail";
    
    private int page = 0;
    private int total = 0;
    private String responestatus = null;
    private String responemessage = null;
    
    public WSIscObjectsModel(){    	
    	
    }
    
    public WSIscObjectsModel(JSONObject json, Class<T> out) {
	   try {
	    	if(json!= null){
	    		if(json.has(TAG_RESPONE_STATUS))					
						this.responestatus = json.getString(TAG_RESPONE_STATUS);
	    		if(json.has(TAG_RESPONE_MESSAGE))					
					this.responemessage = json.getString(TAG_RESPONE_MESSAGE);
	    		if(json.has(TAG_PAGE))					
					this.page = json.getInt(TAG_PAGE);
	    		if(json.has(TAG_PAGE_TOTAL))					
					this.total = json.getInt(TAG_PAGE_TOTAL);
	    		
	    		if(json.has(TAG_DETAIL_LIST) && !json.isNull(TAG_DETAIL_LIST)){
	    			this.details = new ArrayList<T>();
	    			for(int i = 0;i < json.getJSONArray(TAG_DETAIL_LIST).length(); i++){
	    				JSONObject item  = json.getJSONArray(TAG_DETAIL_LIST).getJSONObject(i);
	    				this.details.add(ParseJsonModel.parse(item, out));
	    			}
	    		}    			
	    	}
    	} catch (JSONException e) {
			Log.i("ObjectsModel_ObjectsModel()", e.getMessage());
			e.printStackTrace();
		}
    }
    
    public int getPage() {
        return this.page;
    }

    public int getPageTotal() {
        return this.total;
    }
    
    public String getResponeMessage() {
        return this.responemessage;
    }

    public String getResponeStatus() {
        return this.responestatus;
    }
    
    public List<T> getListObject() {
        return this.details;
    }
   
	
}
