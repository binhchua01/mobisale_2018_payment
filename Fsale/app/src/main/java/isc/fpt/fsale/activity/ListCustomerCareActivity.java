package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentListCustomerCare;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.view.Menu;

// màn hình KS ĐỘ HÀI LÒNG KHÁCH HÀNG
public class ListCustomerCareActivity extends BaseActivity {

    public static final String TAB_LEVEL_1 = "TAB_LEVEL_1";
    public static final String TAB_LEVEL_2 = "TAB_LEVEL_2";
    public static final String TAB_LEVEL_3 = "TAB_LEVEL_3";

    public static FragmentManager fragmentManager;
    private FragmentTabHost mTabHost;

    public ListCustomerCareActivity() {
        super(R.string.lbl_screen_name_customer_care_list);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_customer_care_list));
        setContentView(R.layout.activity_list_customer_care_by_tab);
        fragmentManager = getSupportFragmentManager();
        mTabHost = (FragmentTabHost) findViewById(R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.tabFrameLayout);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_LEVEL_1).setIndicator(getString(R.string.lbl_level_name_1).toUpperCase()), FragmentListCustomerCare.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_LEVEL_2).setIndicator(getString(R.string.lbl_level_name_2).toUpperCase()), FragmentListCustomerCare.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_LEVEL_3).setIndicator(getString(R.string.lbl_level_name_3).toUpperCase()), FragmentListCustomerCare.class, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    public static int getLevelByName(String fragmentTabName) {
        if (fragmentTabName.equals(TAB_LEVEL_1))
            return 1;
        if (fragmentTabName.equals(TAB_LEVEL_2))
            return 2;
        if (fragmentTabName.equals(TAB_LEVEL_3))
            return 3;
        return -1;
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
