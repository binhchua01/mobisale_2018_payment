package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Report Potential Obj Total
 * @author: 		DuHK
 * @create date: 	16/02/2016
 * */
import isc.fpt.fsale.activity.ListReportCreateObjectDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportCreateObjectDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetReportCreateObjectDetail implements AsyncTaskCompleteListener<String> {

	public static final String METHOD_NAME = "ReportCreateObjectDetail";
	public static final String[] paramNames = {"UserName", "FromDate", "ToDate", "Status", "PageNumber"};
	private Context mContext;	
	/*private String fromDate, toDate;
	private int status;*/
	
	//Add by: DuHK
	public GetReportCreateObjectDetail(Context mContext, String UserName, String FromDate, String ToDate, int Status, int PageNumber){
		this.mContext = mContext;	
		
		String[] paramValues = {UserName, FromDate, ToDate, String.valueOf(Status), String.valueOf(PageNumber)};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportCreateObjectDetail.this);
		service.execute();	
	}	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			ArrayList<ReportCreateObjectDetailModel> lst = null;
			boolean isError = false;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ReportCreateObjectDetailModel> resultObject = new WSObjectsModel<ReportCreateObjectDetailModel>(jsObj, ReportCreateObjectDetailModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();							
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(ArrayList<ReportCreateObjectDetailModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListReportCreateObjectDetailActivity.class.getSimpleName())){
				ListReportCreateObjectDetailActivity activity = (ListReportCreateObjectDetailActivity)mContext;
				activity.loadData(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("ReportSBIDetail.loadRpCodeInfo()", e.getMessage());
		}
	}
	

}
