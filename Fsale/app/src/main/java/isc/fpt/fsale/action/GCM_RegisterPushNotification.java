package isc.fpt.fsale.action;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;

/*
 * ACTION: Register Push Notification
 *
 * @description: - call service and check if have new version
 * - handle response result: if having new version, show new version download link
 * @author: vandn, on 13/08/2013
 * API đăng ký notification lên server
 *
 * update by: haulc3 on 2019-12-10
 */

public class GCM_RegisterPushNotification implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private String RegID;

    public GCM_RegisterPushNotification(final Context mContext, String RegID, int FunctionType) {
        this.mContext = mContext;
        this.RegID = RegID;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("RegID", RegID);
            jsonObject.put("DeviceIMEI", DeviceInfo.DEVICE_IMEI);
            jsonObject.put("DeviceType", 0);
            jsonObject.put("FunctionType", FunctionType);
            //FunctionType:
            //0: default, set 0 sẽ đăng ký noty bình thường theo rule cũ
            //1: check máy admin không cho auto đăng ký noty"
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String message = mContext.getResources().getString(R.string.message_register_notification);
        String REGISTER_PUSH_NOTIFICATION = "GCM_RegisterPushNotification";
        CallServiceTask service = new CallServiceTask(mContext, REGISTER_PUSH_NOTIFICATION, jsonObject,
                Services.JSON_POST_OBJECT, message, GCM_RegisterPushNotification.this);
        service.execute();
    }

    public void onTaskComplete(String result) {
        boolean showToast = Common.loadPreferenceBoolean(mContext, Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_SHOW_TOAST);
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(jsObj, UpdResultModel.class);
                if (resultObject.getErrorCode() == 0) {
                    if (resultObject.getListObject().size() > 0) {
                        UpdResultModel item = resultObject.getListObject().get(0);
                        if (item.getResultID() > 0) {
                            Common.savePreference(mContext, Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_SHOW_TOAST, false);
                            Common.savePreference(mContext, Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_REG_ID, RegID);
                            if (showToast) {
                                Toast.makeText(mContext, item.getResult(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } else {
                    if (showToast) {
                        Toast.makeText(mContext, resultObject.getError(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
