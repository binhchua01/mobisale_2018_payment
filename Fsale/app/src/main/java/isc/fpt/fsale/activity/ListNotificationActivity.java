package isc.fpt.fsale.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListNotification;
import isc.fpt.fsale.adapter.NotificationAdapter;
import isc.fpt.fsale.model.Notification;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.utils.Common;

// màn hình DANH SÁCH THÔNG BÁO
public class ListNotificationActivity extends BaseActivitySecond implements SwipeRefreshLayout.OnRefreshListener {
    private ListView lvNotification;
    private TextView tvEmptyNotification;
    private SwipeRefreshLayout swipeRefreshListNotification;
    private int pageCurrent = 1;
    private Button btnPre, btnNext;
    private TextView tvNumberPageCurrent;
    private boolean pageEnd;
    private LinearLayout frmPage;
    private View viewBack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_notification));
        //nhận Code gửi về từ
        Intent intent = getIntent();
        if (intent != null) {
            String resultAcceptCode = intent.getStringExtra("resultAcceptCode");
            if (resultAcceptCode != null) {
                Common.alertDialog(resultAcceptCode, this);
            }
        }
    }

    @Override
    protected void initEvent() {
        viewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        swipeRefreshListNotification.setOnRefreshListener(this);

        //khởi tạo sự kiện nhấn nút PREVIOUS
        btnPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageCurrent > 1) {
                    pageCurrent--;
                    tvNumberPageCurrent.setText(String.valueOf(pageCurrent));
                    new GetListNotification(ListNotificationActivity.this, pageCurrent);
                }
            }
        });

        //khởi tạo sự kiện nhấn nút NEXT
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pageEnd) {
                    pageCurrent++;
                    tvNumberPageCurrent.setText(String.valueOf(pageCurrent));
                    new GetListNotification(ListNotificationActivity.this, pageCurrent);
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_list_notification;
    }

    @Override
    protected void initView() {
        viewBack = findViewById(R.id.btn_back);
        TextView tvTitle = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitle.setText(R.string.lbl_screen_name_list_notification);
        tvNumberPageCurrent = (TextView) findViewById(R.id.tv_number_page);
        lvNotification = (ListView) findViewById(R.id.lv_notification);
        tvEmptyNotification = (TextView) findViewById(R.id.tv_empty_notification);
        swipeRefreshListNotification = (SwipeRefreshLayout) findViewById(R.id.container_list_notification);
        frmPage = (LinearLayout) findViewById(R.id.frm_pager_notification);
        swipeRefreshListNotification.setColorSchemeColors(getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main));
        btnPre = (Button) this.findViewById(R.id.btn_previous);
        btnNext = (Button) this.findViewById(R.id.btn_next);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //kết nối API lấy DANH SÁCH THÔNG BÁO
        new GetListNotification(this, pageCurrent);
    }

    //call back đẩy danh sách thông báo vào listView
    public void loadListNotification(List<Notification> notifications) {
        if (notifications.size() == 0) {
            tvEmptyNotification.setVisibility(View.VISIBLE);
            lvNotification.setVisibility(View.GONE);
            frmPage.setVisibility(View.GONE);
        } else {
            pageEnd = notifications.size() < 50;
            frmPage.setVisibility(View.VISIBLE);
            tvEmptyNotification.setVisibility(View.GONE);
            lvNotification.setVisibility(View.VISIBLE);
            NotificationAdapter notificationAdapter = new NotificationAdapter(this, notifications);
            lvNotification.setAdapter(notificationAdapter);
            lvNotification.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parentView, View view, int position, long in) {
                    Notification itemNotification = (Notification) parentView.getItemAtPosition(position);
                    Intent intent = null;
                    if (itemNotification.getTypeName().equals("KHTN")) {
                        String tagKey = "Key";
                        String tagValue = "Value";
                        String code = null;
                        String id = null;
                        JSONArray jsonArr;
                        try {
                            jsonArr = new JSONArray(itemNotification.getKeysJSON());
                            for (int i = 0; i < jsonArr.length(); i++) {
                                JSONObject item = jsonArr.getJSONObject(i);
                                if (item.has(tagKey)) {
                                    if (item.get(tagKey).equals("Code")) {
                                        code = item.getString(tagValue);
                                    }
                                }
                                if (item.has(tagKey)) {
                                    if (item.get(tagKey).equals("ID")) {
                                        id = item.getString(tagValue);
                                    }
                                }
                            }
                            intent = new Intent(ListNotificationActivity.this, CreatePotentialCEMObjActivity.class);
                            intent.putExtra("PotentialObjID", id);
                            intent.putExtra("Code", code);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else if (itemNotification.getTypeName().equals("PTC_Return")) {
                        intent = new Intent(ListNotificationActivity.this, ListPTCReturnActivity.class);
                    }
                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    //sự kiện refresh DANH SÁCH THÔNG BÁO
    @Override
    public void onRefresh() {
        swipeRefreshListNotification.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshListNotification.setRefreshing(true);
                new GetListNotification(ListNotificationActivity.this, pageCurrent);
                swipeRefreshListNotification.setRefreshing(false);
            }
        });
    }
}
