package isc.fpt.fsale.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.TimeZone;

/**
 * Created by HCM.TUANTT14 on 7/10/2018.
 */

public class TimeZoneAdapter extends ArrayAdapter<TimeZone> {
    private ArrayList<TimeZone> lstObj;
    private Context mContext;
    private boolean hasInitText = false;
    private int iColor = 2;
    private Typeface tf;

    public TimeZoneAdapter(Context context, int textViewResourceId, ArrayList<TimeZone> lstObj) {
        super(context, textViewResourceId);
        // TODO Auto-generated constructor stub
        this.mContext = context;
        this.lstObj = lstObj;
        this.tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    public TimeZoneAdapter(Context context, int textViewResourceId, ArrayList<TimeZone> lstObj, int color) {
        super(context, textViewResourceId);
        // TODO Auto-generated constructor stub
        this.mContext = context;
        this.lstObj = lstObj;
        this.tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");
        this.iColor = color;
    }

    public TimeZoneAdapter(Context context, int textViewResourceId, ArrayList<TimeZone> lstObj, boolean hasInitText) {
        super(context, textViewResourceId);
        // TODO Auto-generated constructor stub
        this.mContext = context;
        this.lstObj = lstObj;
        this.hasInitText = true;
    }


    public ArrayList<TimeZone> getList() {
        return lstObj;
    }

    public int getCount() {
        if (lstObj != null)
            return lstObj.size();
        return 0;
    }

    public TimeZone getItem(int position) {
        if (lstObj != null)
            return lstObj.get(position);
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        if (hasInitText && position == 0) {
            label.setVisibility(View.GONE);
        } else {
            label.setText(lstObj.get(position).getTimezoneText());
            label.setTypeface(tf);
            int padding = (int) mContext.getResources().getDimension(R.dimen.padding_medium);
            label.setPadding(padding, padding, padding, padding);

            if (!TextUtils.isEmpty(lstObj.get(position).getColorCode())) {
                label.setBackgroundColor(Color.parseColor(lstObj.get(position).getColorCode()));
                label.setTextColor(Color.parseColor("#ffffff"));
            }

            //không cho chọn item khi ability < 0
            if (lstObj.get(position).getAbilityDesc() < 0) {
                label.setEnabled(false);
                label.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
            }
        }
        return label;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setTypeface(tf);
        if (!TextUtils.isEmpty(lstObj.get(position).getColorCode())) {
            label.setTextColor(Color.parseColor(lstObj.get(position).getColorCode()));//set color for text
        }
        label.setText(lstObj.get(position).getTimezoneText());
        if (this.iColor != 2)
            label.setTextColor(ColorStateList.valueOf(this.iColor));
        return label;
    }
}
