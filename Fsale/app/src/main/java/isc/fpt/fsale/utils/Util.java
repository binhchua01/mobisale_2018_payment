package isc.fpt.fsale.utils;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class Util {
    public static String Base64(String data) {

        String result = "";
        String MD5 = "MD5";

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance(MD5);
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        digest.update(data.getBytes());
        byte messageDigest[] = digest.digest();

        result = android.util.Base64.encodeToString(messageDigest,
                android.util.Base64.DEFAULT);
        return result;
    }

    public static String Base64New(String data) {
        return android.util.Base64.encodeToString(data.getBytes(), android.util.Base64.NO_WRAP);
    }

    public static final String MD5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getSignature(String data) {
        String base64 = Base64New(data) + Constants.SECRET_KEY;
        String md5 = MD5(base64);
        return md5.toLowerCase(Locale.getDefault());
    }

    public static String replaceCharacter(String str) {
        int startIndex = str.indexOf("(");
        int endIndex = str.indexOf(")");
        if (startIndex != -1 && endIndex != -1) {
            return str.substring(startIndex + 1, endIndex);
        } else {
            return str.trim();
        }
    }

    public static String writeFileApkToDisk(InputStream body) {
        //make folder
        String folderMain = "Mobisale";
        String subFolder = "Apk";
        File f = new File(Environment.getExternalStorageDirectory(), folderMain);
        if (!f.exists()) {
            f.mkdirs();
        }
        //create sub folder
        File f1 = new File(Environment.getExternalStorageDirectory() + "/" + folderMain, subFolder);
        if (!f1.exists()) {
            f1.mkdirs();
        }
        String path = Environment.getExternalStorageDirectory() + "/" + folderMain + "/" + subFolder;
        File futureStudioIconFile = new File(f1, "app.apk");
        //create path and file name
        OutputStream outputStream;
        //write file
        try {
            outputStream = new FileOutputStream(futureStudioIconFile);

            byte[] buffer = new byte[1024];
            int len1;
            while ((len1 = body.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len1);
            }
            outputStream.close();
            body.close();
            return path;
        } catch (Exception e) {
            return null;
        }
    }
}
