package isc.fpt.fsale.ui.callback;

public interface OnItemClickListenerV4<A, B, C> {
    void onItemClickV4(A object1, B object2, C object3);
}
