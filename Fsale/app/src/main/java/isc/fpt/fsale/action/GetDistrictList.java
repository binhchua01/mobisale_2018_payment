package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.activity.district.DistrictActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.SharedPref;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.view.Gravity;
import android.widget.Spinner;
import android.widget.Toast;

/*
 * ACTION: 	 	 GetDistrictList
 *
 * @description: - call api "GetDistrictList" to get all districts of city
 * - handle response result: transfer response result to cusInfo screen
 * @author: vandn, on 02/12/2013
 * API kết nối lấy danh sách Quận(Huyện)
 */

public class GetDistrictList implements AsyncTaskCompleteListener<String> {
    private final String GET_DISTRICTS = "GetDistrictList";
    private Spinner spDistrict;
    private Context mContext;
    private String districtId;
    private DistrictActivity activity;
    private String locationId;

    public GetDistrictList(Context mContext, String sRegion, Spinner sp, String districtId) {
        this.mContext = mContext;
        this.spDistrict = sp;
        this.districtId = districtId;
        String[] params = new String[]{"LocationID"};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_district);
        CallServiceTask service = new CallServiceTask(mContext, GET_DISTRICTS,
                params, new String[]{sRegion}, Services.JSON_POST, message, GetDistrictList.this);
        service.execute();
    }

    public GetDistrictList(DistrictActivity activity, String locationId) {
        this.activity = activity;
        this.locationId = locationId;
        String[] params = new String[]{"LocationID"};
        //call service
        String message = activity.getResources().getString(R.string.msg_pd_get_info_district);
        CallServiceTask service = new CallServiceTask(activity, GET_DISTRICTS,
                params, new String[]{locationId}, Services.JSON_POST, message, GetDistrictList.this);
        service.execute();
    }

    public void handleGetDistrictsResult(String json) {
        try {
            if (Common.jsonObjectValidate(json)) {
                Common.savePreference(mContext, Constants.SHARE_PRE_CACHE_REGISTER,
                        Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT, json);
                if (spDistrict != null) {
                    loadDataFromCacheDistrict(mContext, spDistrict, districtId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onTaskComplete(String result) {
        if (activity != null) {
            //update by haulc3
            handleDataDistrict(result);
        } else {
            //old
            handleGetDistrictsResult(result);
        }
    }

    private void handleDataDistrict(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                String TAG_GET_DISTRICTS_RESULT = "GetDistrictListMethodPostResult";
                JSONObject jsObj = new JSONObject(result);
                JSONArray jsonArray = jsObj.getJSONArray(TAG_GET_DISTRICTS_RESULT);
                if (jsonArray.length() > 0) {
                    SharedPref.put(activity, locationId, result);
                    if (activity != null) {
                        activity.loadDistrictList();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static boolean hasCacheDistrict(Context context) {
        return Common.hasPreference(context, Constants.SHARE_PRE_CACHE_REGISTER,
                Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT);
    }

    private static void loadDataFromCacheDistrict(Context context, Spinner sp, String id) {
        if (context != null && sp != null) {
            try {
                ArrayList<KeyValuePairModel> lst = new ArrayList<>();
                lst.add(new KeyValuePairModel("-1", "[ Chọn Quận/Huyện ]"));
                if (hasCacheDistrict(context)) {
                    final String TAG_GET_DISTRICTS_RESULT = "GetDistrictListMethodPostResult";
                    final String TAG_FULL_NAME = "FullNameVN";
                    final String TAG_NAME = "Name";
                    final String TAG_ERROR = "ErrorService";
                    String jsonStr = Common.loadPreference(context, Constants.SHARE_PRE_CACHE_REGISTER,
                            Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT);
                    try {
                        JSONObject json = new JSONObject(jsonStr);
                        JSONArray jsArr;
                        jsArr = json.getJSONArray(TAG_GET_DISTRICTS_RESULT);
                        for (int i = 0; jsArr != null && i < jsArr.length(); i++) {
                            String code = "", desc = "", error = "";
                            JSONObject item = jsArr.getJSONObject(i);

                            if (item.has(TAG_ERROR))
                                error = item.getString(TAG_ERROR);
                            if (item.isNull(TAG_ERROR) || error.equals("null")) {
                                if (item.has(TAG_NAME)) {
                                    code = item.getString(TAG_NAME);
                                }
                                if (item.has(TAG_FULL_NAME)) {
                                    desc = item.getString(TAG_FULL_NAME);
                                }
                                lst.add(new KeyValuePairModel(code, desc));
                            } else {
                                break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    KeyValuePairAdapter adapter = new KeyValuePairAdapter(context, R.layout.my_spinner_style, lst, Gravity.RIGHT);
                    sp.setAdapter(adapter);
                    int index = Common.getIndex(sp, id);
                    if (index > 0) {
                        sp.setSelection(index, true);
                    }
                }
            } catch (Exception e) {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
