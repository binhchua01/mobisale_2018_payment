package isc.fpt.fsale.action.maxy;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.maxytv.model.MaxyGeneral;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPackageActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//API lấy danh sách CLKM cloud
public class GetMaxyPackage implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private int regType; // 0-bán mới: 1-bán thêm

    public GetMaxyPackage(Context mContext, int RegType, int localtype, String contract) {
        this.mContext = mContext;
        this.regType = RegType;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("RegType",regType);
            jsonObject.put("LocationID", Constants.LST_REGION.get(0).getsID());
            jsonObject.put("BranchCode", Constants.BRANCH_CODE);
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("LocalType", localtype);
            jsonObject.put("Contract", contract);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = "Đang lấy dánh sách gói truyền hình maxy...";
        String GET_MAXY_PACKAGE = "GetMaxyPackage";
        CallServiceTask service = new CallServiceTask(mContext, GET_MAXY_PACKAGE, jsonObject,
                Services.JSON_POST_OBJECT, message, GetMaxyPackage.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<MaxyGeneral> resultObject = new WSObjectsModel<>(jsObj, MaxyGeneral.class);
                List<MaxyGeneral> lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (mContext != null) {
                    if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                        Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                    } else {
                        ((MaxyPackageActivity) mContext).mLoadPackage(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
