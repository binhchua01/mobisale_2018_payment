package isc.fpt.fsale.ui.extra_ott.promotion_extra_ott;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPromotionExtraOtt;
import isc.fpt.fsale.model.DetailPackage;
import isc.fpt.fsale.model.PromoCommand;
import isc.fpt.fsale.model.PromotionExtraOtt;
import isc.fpt.fsale.model.ServiceListExtraOtt;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.extra_ott.RegisterExtraOttModel;
import isc.fpt.fsale.utils.Constants;

public class PromotionExtraOttActivity extends BaseActivitySecond
        implements OnItemClickListener<PromotionExtraOtt> {
    private RelativeLayout rltBack;

    private PromotionExtraOttAdapter mAdapter;
    private List<PromotionExtraOtt> mList;
    private RegisterExtraOttModel mRegisterExtraOttModel;
    private ServiceListExtraOtt mServiceListExtraOtt;
    private int position;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_promotion_extra_ott;
    }

    @Override
    protected void initView() {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_extra_ott_promotion);
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        mList = new ArrayList<>();
        mAdapter = new PromotionExtraOttAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);
        getDataIntent();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(Constants.REGISTRATION_EXTRA_OTT) != null &&
                bundle.getInt(Constants.POSITION) != -1) {
            mRegisterExtraOttModel = bundle.getParcelable(Constants.REGISTRATION_EXTRA_OTT);
            position = bundle.getInt(Constants.POSITION);
            mServiceListExtraOtt = mRegisterExtraOttModel.getCartExtraOtt().getServiceList().get(position);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRegisterExtraOttModel != null && mServiceListExtraOtt != null) {
            new GetPromotionExtraOtt(this, mRegisterExtraOttModel, mServiceListExtraOtt);
        }
    }

    @Override
    public void onItemClick(PromotionExtraOtt object) {
        if (object == null) {
            return;
        }
        PromoCommand pc = mRegisterExtraOttModel.getCartExtraOtt().getServiceList().get(position).getPromoCommand();
        pc.setCommandText(object.getCommandText());
        pc.setID(object.getID());
        pc.setMaxCount(object.getMaxCount());
        pc.setMinCount(object.getMinCount());
        pc.setTotalAmount(object.getTotalAmount());
        pc.setTotalAmountVAT(object.getTotalAmountVAT());
        mRegisterExtraOttModel.getCartExtraOtt().getServiceList().get(position).setQuantity(pc.getMinCount());

        if (compareDataLoop()) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.REGISTRATION_EXTRA_OTT, mRegisterExtraOttModel);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else {
            showError(getString(R.string.txt_message_two_package_loop));
        }

    }

    private boolean compareDataLoop() {
        boolean isValidate = true;
        if (mRegisterExtraOttModel.getCartExtraOtt().getServiceList() != null &&
                mRegisterExtraOttModel.getCartExtraOtt().getServiceList().size() > 1) {
            for (int i = 0; i < mRegisterExtraOttModel.getCartExtraOtt().getServiceList().size(); i++) {
                for (int j = i + 1; j < mRegisterExtraOttModel.getCartExtraOtt().getServiceList().size(); j++) {
                    if (mRegisterExtraOttModel.getCartExtraOtt().getServiceList().get(i).getPromoCommand().getID() != null &&
                            mRegisterExtraOttModel.getCartExtraOtt().getServiceList().get(i).getPromoCommand().getID()
                            .equals(mRegisterExtraOttModel.getCartExtraOtt().getServiceList().get(j).getPromoCommand().getID())) {
                        List<DetailPackage> lst1 =  mRegisterExtraOttModel.getCartExtraOtt()
                                .getServiceList().get(i).getPromoCommand().getDetailPackage();
                        List<DetailPackage> lst2 =  mRegisterExtraOttModel.getCartExtraOtt()
                                .getServiceList().get(j).getPromoCommand().getDetailPackage();
                        for (DetailPackage item1 : lst1){
                            for (DetailPackage item2: lst2){
                                if(item1.getiD().equals(item2.getiD())){
                                    isValidate = false;
                                    break;
                                }
//                                if(item1.getiD().equals(item2.getiD()) && lst1.size() == lst2.size()){
//                                    isValidate = false;
//                                    break;
//                                }
                            }
                        }
                    }
                }
            }
        }
        return isValidate;
    }

    public void loadPromotionList(List<PromotionExtraOtt> mList) {
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }
}
