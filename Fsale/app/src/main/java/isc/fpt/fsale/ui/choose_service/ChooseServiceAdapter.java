package isc.fpt.fsale.ui.choose_service;

import android.content.Context;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

/**
 * Created by haulc3 on 15,July,2019
 */
public class ChooseServiceAdapter extends RecyclerView.Adapter<ChooseServiceAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<ServiceType> mList;
    private OnItemClickListener mListener;
    private long mLastClickTime = 0;

    ChooseServiceAdapter(Context mContext, List<ServiceType> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_choose_service, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                mListener.onItemClick(mList.get(mViewHolder.getAdapterPosition()));
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.tvServiceName.setText(mList.get(position).getCategoryServiceGroupName());
        holder.tvServiceDes.setText(mList.get(position).getCategoryServiceGroupDesc());
        Picasso.get()
                .load(mList.get(position).getCategoryServiceGroupLogo())
                .into(holder.imgService);
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgService;
        private TextView tvServiceName, tvServiceDes;

        SimpleViewHolder(View itemView) {
            super(itemView);
            imgService = (ImageView) itemView.findViewById(R.id.img_service);
            tvServiceName = (TextView) itemView.findViewById(R.id.tv_service_name);
            tvServiceDes = (TextView) itemView.findViewById(R.id.tv_service_des);
        }
    }

    void notifyData(List<ServiceType> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
