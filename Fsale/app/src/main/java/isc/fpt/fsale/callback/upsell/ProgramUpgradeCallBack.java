package isc.fpt.fsale.callback.upsell;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.model.upsell.response.ProgramUpgradeModel;

public interface ProgramUpgradeCallBack {
    void onGetProgramSuccess(List<ProgramUpgradeModel> lst);
}
