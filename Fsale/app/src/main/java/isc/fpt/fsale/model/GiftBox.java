package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haulc3 on 20,June,2019
 */
public class GiftBox {
    @SerializedName("GiftID")
    @Expose
    private int giftID;
    @SerializedName("GiftName")
    @Expose
    private String giftName;

    public GiftBox(Integer giftID, String giftName) {
        this.giftID = giftID;
        this.giftName = giftName;
    }

    public int getGiftID() {
        return giftID;
    }

    public void setGiftID(int giftID) {
        this.giftID = giftID;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }
}
