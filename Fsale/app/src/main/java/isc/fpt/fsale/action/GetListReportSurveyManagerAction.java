package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportSurveyManagerActivity;

import isc.fpt.fsale.model.ReportSurveyManagerModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

public class GetListReportSurveyManagerAction implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public GetListReportSurveyManagerAction(Context _mContext, String userName, int Day, int Month, int Year,
                                            String Agent, String AgentName, int PageNumber) {
        this.mContext = _mContext;
        String[] paramName = new String[]{"UserName", "Day", "Month", "Year", "Agent", "AgentName", "PageNumber"};
        String[] paramValue = new String[]{userName, String.valueOf(Day), String.valueOf(Month),
                String.valueOf(Year), String.valueOf(Agent), String.valueOf(AgentName), String.valueOf(PageNumber)};
        String message = "Xin vui lòng chờ giây lát";
        String GET_REPORT_SURVEY_MANAGER = "Reportsurvey";
        CallServiceTask service = new CallServiceTask(mContext, GET_REPORT_SURVEY_MANAGER, paramName,
                paramValue, Services.JSON_POST, message, GetListReportSurveyManagerAction.this);
        service.execute();
    }

    private void handleUpdate(String json) {
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = getJsonObject(json);
            ArrayList<ReportSurveyManagerModel> reportList = new ArrayList<>();
            if (jsObj != null) {
                try {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    String TAG_ERROR_CODE = "ErrorCode";
                    if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        /*, TAG_ID = "ID", TAG_SBI = "SBI"*/
                        String TAG_SBI_LIST = "ListObject";
                        JSONArray sbiArray = jsObj.getJSONArray(TAG_SBI_LIST);
                        if (sbiArray != null) {
                            for (int index = 0; index < sbiArray.length(); index++)
                                reportList.add(ReportSurveyManagerModel.Parse(sbiArray.getJSONObject(index)));
                            try {
                                ListReportSurveyManagerActivity activity = (ListReportSurveyManagerActivity) mContext;
                                activity.LoadData(reportList);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        String TAG_ERROR = "Error";
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            return JSONParsing.getJsonObj(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onTaskComplete(String result) {
        handleUpdate(result);
    }
}
