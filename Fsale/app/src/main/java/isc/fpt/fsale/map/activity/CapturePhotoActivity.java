package isc.fpt.fsale.map.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.BaseActivity;
import isc.fpt.fsale.activity.InvestiageActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class CapturePhotoActivity extends BaseActivity {
    public static final String TAG_IMAGE_PATH = "TAG_URL";
    private final int REQUEST_CAMERA = 0;
    private File sdcardPath = null;
    private Button btnChosePhoto;
    private ImageButton imgCapturePhoto;
    private TextView txtPageNum;
    private Context mContext;
    private String sCurrentImg = "";

    public CapturePhotoActivity() {
        super(R.string.title_update_img);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_photo_gallary);
        getSlidingMenu().setSlidingEnabled(false);
        // init controls
        this.mContext = CapturePhotoActivity.this;
        btnChosePhoto = (Button) this.findViewById(R.id.btn_send_photo);
        imgCapturePhoto = (ImageButton) this
                .findViewById(R.id.btn_capture_photo);
        txtPageNum = (TextView) this.findViewById(R.id.txt_page_num);
        btnChosePhoto.setVisibility(View.GONE);
        // handle button clicked event
        imgCapturePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capture();
            }
        });
        btnChosePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = null;
                Dialog dialog = null;
                builder = new AlertDialog.Builder(CapturePhotoActivity.this);
                builder.setTitle("Thông báo");
                builder.setMessage(
                        (mContext.getResources()
                                .getString(R.string.msg_delete_capture_photo)))
                        .setCancelable(false)
                        .setPositiveButton("Có",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        displayImgOnViewPager();
                                        updateInvestiageActivity();
                                    }
                                })
                        .setNegativeButton("Không",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });
                dialog = builder.create();
                dialog.show();
            }
        });
        createDirIfNotExists();
        displayImgOnViewPager();
        capture();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void updateInvestiageActivity() {
        Intent intent = new Intent(CapturePhotoActivity.this,
                InvestiageActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(TAG_IMAGE_PATH, sCurrentImg);
        startActivity(intent);
        finish();
    }

    public void displayImgOnViewPager() {
        try {
            if (!(sCurrentImg.equals(""))) {
                btnChosePhoto.setVisibility(View.VISIBLE);
                txtPageNum.setText(sCurrentImg);
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap myBitmap = BitmapFactory.decodeFile(sCurrentImg, options);
                ImageView iv = (ImageView) findViewById(R.id.view_image);
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
                iv.setImageBitmap(myBitmap);
            } else {
                btnChosePhoto.setVisibility(View.GONE);
                txtPageNum.setText(mContext.getResources().getString(
                        R.string.msg_no_img_select));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String fileName = null;
    private File tempFile;

    @SuppressLint("SimpleDateFormat")
    public void capture() {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());
            fileName = "JPEG_" + timeStamp + ".jpg";
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/Mobisale Images/");
            myDir.mkdirs();
            tempFile = new File(myDir, fileName);
            if (!tempFile.exists()) {
                tempFile = new File(myDir, fileName);
            }
            Uri outputFileUri = Uri.fromFile(tempFile);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(cameraIntent, REQUEST_CAMERA);
        } catch (Exception e) {
            // TODO: handle exception
            Common.alertDialog(e.getMessage(), this);
        }

    }

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                sCurrentImg = tempFile.toString();
                displayImgOnViewPager();
            }
        }
    }

    private List<String> getListImgsFromSDCard() {
        List<String> list = new ArrayList<String>();
        File[] lstFile = null;
        try {
            if (sdcardPath != null) {
                lstFile = sdcardPath.listFiles();
                Arrays.sort(lstFile, new Comparator<Object>() {
                    public int compare(Object o1, Object o2) {
                        if (((File) o1).lastModified() > ((File) o2)
                                .lastModified()) {
                            return -1;
                        } else if (((File) o1).lastModified() < ((File) o2)
                                .lastModified()) {
                            return +1;
                        } else {
                            return 0;
                        }
                    }

                });
            }
            if (lstFile != null) {
                int l = lstFile.length;
                for (int i = 0; i < l; i++) {
                    if (acceptFile(lstFile[i]))
                        list.add(lstFile[i].getAbsolutePath());
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return list;
    }

    @SuppressLint("DefaultLocale")
    public boolean acceptFile(File file) {
        final String[] okFileExtensions = new String[]{"jpg", "png", "gif",
                "jpeg"};
        for (String extension : okFileExtensions) {
            if (file.getName().toLowerCase().endsWith(extension)) {
                return true;
            }
        }
        return false;
    }

    private void createDirIfNotExists() {
        try {
            sdcardPath = new File(Environment.getExternalStorageDirectory(),
                    Constants.DIRECTORY_ROOT);
            if (!sdcardPath.exists()) {
                sdcardPath.mkdirs();
            }
        } catch (Exception e) {
        }
    }

    private void DeleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles()) {
                child.delete();
                DeleteRecursive(child);
            }
        fileOrDirectory.delete();
        displayImgOnViewPager();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
    }
}
