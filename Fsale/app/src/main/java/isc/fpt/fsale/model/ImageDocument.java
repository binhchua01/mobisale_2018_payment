package isc.fpt.fsale.model;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 10/16/2018.
 */
// Model ảnh hồ sơ khách hàng
public class ImageDocument implements Parcelable, Cloneable {
    private String pathCache;
    private boolean updated;
    // 1 là thành công, 2 là fail, 0 là mới
    private int statusUpload;
    private String ID;
    private String path;

    public ImageDocument() {
    }

    public ImageDocument(String ID) {
        this.ID = ID;
    }

    public ImageDocument(String path, boolean updated, int statusUpload) {
        this.path = path;
        this.updated = updated;
        this.statusUpload = statusUpload;
    }

    public String getPathCache() {
        return pathCache;
    }

    public void setPathCache(String pathCache) {
        this.pathCache = pathCache;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public int getStatusUpload() {
        return statusUpload;
    }

    public void setStatusUpload(int statusUpload) {
        this.statusUpload = statusUpload;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    protected ImageDocument(Parcel in) {
        pathCache = in.readString();
        updated = in.readByte() != 0;
        statusUpload = in.readInt();
        ID = in.readString();
        path = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pathCache);
        dest.writeByte((byte) (updated ? 1 : 0));
        dest.writeInt(statusUpload);
        dest.writeString(ID);
        dest.writeString(path);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ImageDocument> CREATOR = new Creator<ImageDocument>() {
        @Override
        public ImageDocument createFromParcel(Parcel in) {
            return new ImageDocument(in);
        }

        @Override
        public ImageDocument[] newArray(int size) {
            return new ImageDocument[size];
        }
    };

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
