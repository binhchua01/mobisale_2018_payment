package isc.fpt.fsale.activity;


import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.MessageAdapter;
import isc.fpt.fsale.database.MessageTable;
import isc.fpt.fsale.model.MessageModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.readystatesoftware.viewbadger.BadgeView;
/*import java.util.Collections;
import java.util.Comparator;*/


public class NotificationActivity extends /*BaseActivity*/ Activity {

	/*public NotificationActivity() {
		super(R.string.lbl_screen_name_notification);
		// TODO Auto-generated constructor stub
	}*/

    private Context mContext;
    public ListView lvMessageList;
    private ImageView imgDelete, imgCheckAll, imgCancelDelete;
    private TextView txtTilteNotification;
    public LinearLayout frmDelete;
    public int countDetele = 0;
    public BadgeView badgeDetelte;
    public Boolean isDelete = false;
    public Boolean isCheckAll = false;
    private List<MessageModel> lstMessage;
    private MessageTable tableMess;

    @SuppressLint("LongLogTag")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        try {
            if (((MyApp) getApplication()).getIsLogIn() == false) {
                Common.LogoutWithoutConfirm(this);
                finish();
            }
        } catch (Exception e) {

            Log.i("CustomerCareDetailActivity:", e.getMessage());
        }
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_notification));
        mContext = NotificationActivity.this;
        lvMessageList = (ListView) findViewById(R.id.lv_notification_detail);

        imgCheckAll = (ImageView) findViewById(R.id.img_checkall);
        imgCancelDelete = (ImageView) findViewById(R.id.img_cancel_delete);
        imgDelete = (ImageView) findViewById(R.id.img_delete);
        txtTilteNotification = (TextView) findViewById(R.id.txt_titel_notification);
        frmDelete = (LinearLayout) findViewById(R.id.frm_delete);
        try {
            loadData();
            // xoa message da chon
            imgDelete.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        if (countDetele > 0) {
                            deleteMessage();
                            // load lai du lieu
                            //loadRpCodeInfo();
                            hideControlDelete();
                            isDelete = false;
                        }
                    } catch (Exception ex) {

                        Toast.makeText(mContext, ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });
            // check all
            imgCheckAll.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (isCheckAll)
                        isCheckAll = false;
                    else
                        isCheckAll = true;
                    checkItemDelete(isCheckAll);

                }
            });

            // su kien huy xoa
            imgCancelDelete.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    isDelete = false;
                    hideControlDelete();
                    clearCheckDelete();
                }
            });

            lvMessageList.setOnItemLongClickListener(new OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view,
                                               int position, long id) {
                    // TODO Auto-generated method stub
                    MessageModel itemSeclected = (MessageModel) (lvMessageList.getItemAtPosition(position));
                    if (itemSeclected != null)
                        itemSeclected.setIsDelete(true);
                    isDelete = true;
                    MessageAdapter adapter = (MessageAdapter) lvMessageList.getAdapter();
                    List<MessageModel> messageList = adapter.mList;
                    countDetele = 0;
                    for (MessageModel mess : messageList)
                        if (mess.getIsDelete()) {
                            countDetele++;
                        }
                    txtTilteNotification.setVisibility(View.GONE);
                    imgDelete.setVisibility(View.VISIBLE);
                    imgCheckAll.setVisibility(View.VISIBLE);
                    imgCancelDelete.setVisibility(View.VISIBLE);
                    for (int item = 0; item < parent.getChildCount(); item++) {
                        ((LinearLayout) ((LinearLayout) parent.getChildAt(item)).findViewById(R.id.frm_bookmark)).setVisibility(View.GONE);
                        ((LinearLayout) ((LinearLayout) parent.getChildAt(item)).findViewById(R.id.frm_delete)).setVisibility(View.VISIBLE);
                    }
                    showbadge();
                    return true;
                }
            });

            lvMessageList.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // TODO Auto-generated method stub
                    TextView txtMessage = (TextView) view.findViewById(R.id.txt_notification);
                    TextView txtDateTime = (TextView) view.findViewById(R.id.txt_notification_date_time);
                    txtMessage.setTypeface(null, Typeface.NORMAL);
                    txtDateTime.setTypeface(null, Typeface.NORMAL);

                    MessageModel selectedItem = (MessageModel) lvMessageList.getItemAtPosition(position);
                    try {
                        selectedItem.setIsRead(true);
                        /*MessageTable messTable = new MessageTable(NotificationActivity.this);*/
                        tableMess.update(selectedItem);
                        Intent chatIntent = new Intent(NotificationActivity.this, ChatActivity.class);
                        chatIntent.putExtra("SEND_FROM", selectedItem.getSendfrom());
                        chatIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        NotificationActivity.this.startActivity(chatIntent);
                    } catch (Exception ex) {

                        Toast.makeText(mContext, ex.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            });

        } catch (Exception e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Common.reportActivityStop(this, this);
        clearCheckDelete();
    }

    // hiển thị số lượng cần xóa
    public void showbadge() {
        countDetele = 0;
        MessageAdapter adapter = (MessageAdapter) lvMessageList.getAdapter();
        List<MessageModel> messageList = adapter.mList;
        for (MessageModel mess : messageList)
            if (mess.getIsDelete()) {
                countDetele++;
            }
        if (badgeDetelte == null) {
            badgeDetelte = new BadgeView(this, imgDelete);
            badgeDetelte.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        }
        badgeDetelte.setText(String.valueOf(countDetele));
        badgeDetelte.hide();
        TranslateAnimation anim = new TranslateAnimation(0, 0, -100, 0);
        anim.setInterpolator(new BounceInterpolator());
        anim.setDuration(1000);
        badgeDetelte.toggle(anim, null);
        badgeDetelte.show();
    }

    // xu ly chon all va bo chon all delete
    void checkItemDelete(Boolean isCheck) {
        for (int item = 0; item < lvMessageList.getChildCount(); item++) {
            CheckBox chkdelete = ((CheckBox) ((LinearLayout) lvMessageList.getChildAt(item)).findViewById(R.id.chk_delete));
            if (chkdelete.isChecked() != isCheck)
                chkdelete.setChecked(isCheck);
        }
        updateCheckDetele(isCheck);
        showbadge();

    }

    // cap nhat du lieu check delete
    void updateCheckDetele(Boolean isCheck) {
        for (MessageModel mess : lstMessage)
            mess.setIsDelete(isCheck);
    }

    // bo all check delete
    void clearCheckDelete() {
        updateCheckDetele(false);
    }

    void deleteMessage() {
        if (isCheckAll)
            lstMessage.clear();
        else {
            for (MessageModel item : lstMessage) {
                if (item.getIsDelete()) {
                    //lstMessage.remove(item);
                    tableMess.remove(tableMess.getConversation(item.getSendfrom()));
                    loadData();
                    //tableMess.remove(item);
                }
            }
        }
        //((MessageAdapter) lvMessageList.getAdapter()).notifyDataSetChanged(); 
    }

    // Load du lieu len listview
    void loadData() {
        try {
            tableMess = new MessageTable(this);
            lstMessage = tableMess.getSendFromList();
			/*if(lstMessage.size()>1)
			{
				Collections.sort(lstMessage, new Comparator<MessageModel>(){
					  public int compare(MessageModel emp1, MessageModel emp2) {
						  int i=0;
					      i = String.valueOf(emp1.getIsRead()).compareTo(String.valueOf(emp2.getIsRead())) ;
					      if(i==0)
					    	  i = String.valueOf(emp2.getIsBookmark()).compareTo(String.valueOf(emp1.getIsBookmark()))  ;
					      if(i==0)
					    	  i = String.valueOf(emp2.getDatetime()).compareTo(String.valueOf(emp1.getDatetime()))  ;
					      return i;
					  }
					});
			}*/
            MessageAdapter adapter = new MessageAdapter(mContext, lstMessage);
            lvMessageList.setAdapter(adapter);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    // ẩn các control xóa tin nhắn
    void hideControlDelete() {
        txtTilteNotification.setVisibility(View.VISIBLE);
        imgDelete.setVisibility(View.GONE);
        imgCheckAll.setVisibility(View.GONE);
        imgCancelDelete.setVisibility(View.GONE);

        for (int item = 0; item < lvMessageList.getChildCount(); item++) {
            ((LinearLayout) ((LinearLayout) lvMessageList.getChildAt(item)).findViewById(R.id.frm_bookmark)).setVisibility(View.VISIBLE);
            ((LinearLayout) ((LinearLayout) lvMessageList.getChildAt(item)).findViewById(R.id.frm_delete)).setVisibility(View.GONE);
        }
        badgeDetelte.hide();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        //super.onBackPressed();
        if (imgDelete.getVisibility() == View.VISIBLE) {
            isDelete = false;
            hideControlDelete();
            clearCheckDelete();
        } else {
            finish();
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }
}
