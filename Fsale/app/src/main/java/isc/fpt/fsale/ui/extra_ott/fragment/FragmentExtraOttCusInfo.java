package isc.fpt.fsale.ui.extra_ott.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckPhoneRecare;
import isc.fpt.fsale.action.GetOTTInventory;
import isc.fpt.fsale.activity.source_type.SourceTypeActivity;
import isc.fpt.fsale.model.District;
import isc.fpt.fsale.model.IdentityCard;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PromoCommand;
import isc.fpt.fsale.model.ServiceListExtraOtt;
import isc.fpt.fsale.model.StreetOrCondo;
import isc.fpt.fsale.model.Ward;
import isc.fpt.fsale.activity.IdentityCardScanActivity;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.activity.district.DistrictActivity;
import isc.fpt.fsale.activity.house_position.HousePositionActivity;
import isc.fpt.fsale.activity.house_type.HouseTypeActivity;
import isc.fpt.fsale.activity.phone_type.PhoneTypeActivity;
import isc.fpt.fsale.activity.street_or_condo.StreetOrCondoActivity;
import isc.fpt.fsale.activity.ward.WardActivity;
import isc.fpt.fsale.ui.extra_ott.ExtraOttActivity;
import isc.fpt.fsale.ui.extra_ott.RegisterExtraOttModel;
import isc.fpt.fsale.ui.fragment.DatePickerReportDialog;
import isc.fpt.fsale.ui.fragment.HouseNumFormatRuleDialog;
import isc.fpt.fsale.ui.fragment.InventoryDeviceFragment;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import isc.fpt.fsale.utils.SharedPref;

import static isc.fpt.fsale.utils.DateTimeHelper.formatDateOfBirthRegister;

/**
 * Created by haulc3 on 15,July,2019
 */
public class FragmentExtraOttCusInfo extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    public Context mContext;
    public ScrollView scrollMain;
    public LinearLayout taxNumLayout, phoneNumberLayout1, addressLayoutLast, recareSuccess, recareFail;
    public EditText txtCustomerName, txtIdentityCard, txtAddressId, txtTaxNum,
            txtEmail, txtLot, txtFloor, txtRoom, txtHouseNum, txtPhone1,
            txtContactPhone1, txtPhone2, txtContactPhone2, txtHouseDesc, txtDescriptionIBB, txtBirthDay, txtSourceType;
    public TextView tvPhone1, tvPhone2, tvDistrict,
            tvWard, tvHouseType, tvStreet, tvApartment, tvHousePosition, tvSourceType;
    public ViewGroup apartmentPanel, housePositionPanel, houseNumberPanel;
    public Button btnShowHouseNumFormat, btnTakePhoto, btnOpenGallery, btnRecare;
    private Calendar myCalendar;
    private RegisterExtraOttModel mRegisterExtraOttModel;
    // file ảnh sau khi chụp ảnh cmnd
    public File fileIdentityCardImageDocument;
    public DatePickerReportDialog mDateDialog;

    private final int CODE_PHONE_TYPE_1 = 102;
    private final int CODE_PHONE_TYPE_2 = 103;
    private final int CODE_DISTRICT = 104;
    private final int CODE_WARD = 105;
    private final int CODE_HOUSE_TYPE = 106;
    private final int CODE_STREET = 107;
    private final int CODE_CONDO = 108;
    private final int CODE_HOUSE_POSITION = 109;
    private final int CODE_OPEN_GALLERY = 110;
    private final int CODE_REQUEST_OPEN_GALLERY = 111;
    private final int CODE_REQUEST_TAKE_PHOTO = 112;
    private final int CODE_SCAN_IDENTITY = 113;
    private final int REQUEST_CAMERA_TAKE_PHOTO = 0;
    private final int CODE_SOURCE_TYPE= 116;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
        ExtraOttActivity activity = (ExtraOttActivity) getActivity();
        mRegisterExtraOttModel = activity.getRegisterModel();
    }

    @Override
    protected void initView(View view) {
        scrollMain = (ScrollView) view.findViewById(R.id.frm_main);
        taxNumLayout = (LinearLayout) view.findViewById(R.id.frm_tax_num);
        phoneNumberLayout1 = (LinearLayout) view.findViewById(R.id.frm_phone_number_1);
        addressLayoutLast = (LinearLayout) view.findViewById(R.id.frm_address_input);
        recareSuccess = (LinearLayout) view.findViewById(R.id.frm_recare_success);
        recareFail = (LinearLayout) view.findViewById(R.id.frm_recare_fail);
        apartmentPanel = (ViewGroup) view.findViewById(R.id.frm_apartment);// Apartment Panel
        housePositionPanel = (ViewGroup) view.findViewById(R.id.frm_house_position); // Position
        houseNumberPanel = (ViewGroup) view.findViewById(R.id.frm_house_num);// House Number
        txtCustomerName = (EditText) view.findViewById(R.id.txt_customer_name); //Họ tên khách hàng
        txtCustomerName.requestFocus();
        txtIdentityCard = (EditText) view.findViewById(R.id.txt_identity_card);//CMND
        txtBirthDay = (EditText) view.findViewById(R.id.txt_birthday);// Ngay sinh
        txtAddressId = (EditText) view.findViewById(R.id.txt_address_in_id);//Địa chỉ trên CMND
        txtTaxNum = (EditText) view.findViewById(R.id.txt_tax_num);//Mã số thuế
        txtEmail = (EditText) view.findViewById(R.id.txt_email);
        tvApartment = (TextView) view.findViewById(R.id.tv_apartment_name);
        txtRoom = (EditText) view.findViewById(R.id.txt_room);
        txtFloor = (EditText) view.findViewById(R.id.txt_floor);
        txtLot = (EditText) view.findViewById(R.id.txt_apartment_group);
        txtHouseNum = (EditText) view.findViewById(R.id.txt_house_num);
        txtPhone1 = (EditText) view.findViewById(R.id.txt_phone_1);
        txtContactPhone1 = (EditText) view.findViewById(R.id.txt_contact_phone_1);
        txtHouseDesc = (EditText) view.findViewById(R.id.txt_note_address);
        txtDescriptionIBB = (EditText) view.findViewById(R.id.txt_description_ibb);
        txtPhone2 = (EditText) view.findViewById(R.id.txt_phone_2);
        txtContactPhone2 = (EditText) view.findViewById(R.id.txt_contact_phone_2);
        tvPhone1 = (TextView) view.findViewById(R.id.tv_phone_1);
        tvPhone2 = (TextView) view.findViewById(R.id.tv_phone_2);
        tvDistrict = (TextView) view.findViewById(R.id.tv_districts);
        tvWard = (TextView) view.findViewById(R.id.tv_wards);
        tvStreet = (TextView) view.findViewById(R.id.tv_streets);
        tvHouseType = (TextView) view.findViewById(R.id.tv_house_types);
        tvHousePosition = (TextView) view.findViewById(R.id.tv_house_position);
        btnShowHouseNumFormat = (Button) view.findViewById(R.id.btn_show_house_num_format);
        btnTakePhoto = (Button) view.findViewById(R.id.btn_capture_identity_card);
        btnOpenGallery = (Button) view.findViewById(R.id.btn_open_local_identity_card);
        btnRecare = (Button) view.findViewById(R.id.btn_recare);
        tvSourceType = view.findViewById(R.id.txt_source_type);//hình thức bán
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_extra_ott_cus_info;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initEvent() {
        mDateDialog = initDatePickerDialog(txtBirthDay, getString(R.string.title_dialog_birth_day));
        txtPhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setRecare(-1);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
        tvPhone1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), PhoneTypeActivity.class), CODE_PHONE_TYPE_1);
            }
        });
        tvPhone2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), PhoneTypeActivity.class), CODE_PHONE_TYPE_2);
            }
        });
        tvDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), DistrictActivity.class), CODE_DISTRICT);
            }
        });
        tvWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mRegisterExtraOttModel.getBillTo_District())) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DISTRICT_ID, mRegisterExtraOttModel.getBillTo_District());
                    Intent intent = new Intent(getActivity(), WardActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_WARD);
                } else {
                    showError(getString(R.string.txt_message_choose_district));
                }
            }
        });
        tvHouseType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), HouseTypeActivity.class), CODE_HOUSE_TYPE);
            }
        });
        tvStreet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mRegisterExtraOttModel.getBillTo_District()) &&
                        !TextUtils.isEmpty(mRegisterExtraOttModel.getBillTo_Ward())) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DISTRICT_ID, mRegisterExtraOttModel.getBillTo_District());
                    bundle.putString(Constants.WARD_ID, mRegisterExtraOttModel.getBillTo_Ward());
                    bundle.putInt(Constants.TYPE, 0); //get street
                    Intent intent = new Intent(getActivity(), StreetOrCondoActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_STREET);
                } else {
                    if (TextUtils.isEmpty(mRegisterExtraOttModel.getBillTo_District())) {
                        showError(getString(R.string.txt_message_choose_district));
                    } else {
                        showError(getString(R.string.txt_message_choose_ward));
                    }
                }
            }
        });
        tvApartment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mRegisterExtraOttModel.getBillTo_District()) &&
                        !TextUtils.isEmpty(mRegisterExtraOttModel.getBillTo_Ward())) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DISTRICT_ID, mRegisterExtraOttModel.getBillTo_District());
                    bundle.putString(Constants.WARD_ID, mRegisterExtraOttModel.getBillTo_Ward());
                    bundle.putInt(Constants.TYPE, 1); //get condo
                    Intent intent = new Intent(getActivity(), StreetOrCondoActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_CONDO);
                } else {
                    if (TextUtils.isEmpty(mRegisterExtraOttModel.getBillTo_District())) {
                        showError(getString(R.string.txt_message_choose_district));
                    } else {
                        showError(getString(R.string.txt_message_choose_ward));
                    }
                }
            }
        });
        tvHousePosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), HousePositionActivity.class), CODE_HOUSE_POSITION);
            }
        });
        myCalendar = Calendar.getInstance();
        txtBirthDay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (getActivity() != null) {
                        mDateDialog.setCancelable(false);
                        mDateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
                    }
                    txtBirthDay.setError(null);
                    return true;
                }
                return false;
            }
        });
        btnShowHouseNumFormat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HouseNumFormatRuleDialog dialog = new HouseNumFormatRuleDialog();
                Common.showFragmentDialog(getFragmentManager(), dialog, "fragment_house_num_format_dialog");
            }
        });
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mContext.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                            || mContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, CODE_REQUEST_TAKE_PHOTO);
                    } else {
                        fileIdentityCardImageDocument = Common.capture();
                        takePhoto(fileIdentityCardImageDocument);
                    }
                } else {
                    fileIdentityCardImageDocument = Common.capture();
                    takePhoto(fileIdentityCardImageDocument);
                }
            }
        });
        btnOpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mở file ảnh từ thiết bị
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                            PackageManager.PERMISSION_GRANTED ||
                            mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) !=
                                    PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, CODE_REQUEST_OPEN_GALLERY);
                    } else {
                        openGallery();
                    }
                } else {
                    openGallery();
                }
            }
        });
        btnRecare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!txtPhone1.getText().toString().equals("")){
                    new CheckPhoneRecare(getActivity(), FragmentExtraOttCusInfo.this,
                            txtPhone1.getText().toString(), ((MyApp) getActivity().getApplication()).getUserName());
                }
            }
        });

        tvSourceType.setOnClickListener(v ->
                startActivityForResult(new Intent(getActivity(),
                                SourceTypeActivity.class),
                        CODE_SOURCE_TYPE));
    }

    private DatePickerReportDialog initDatePickerDialog(EditText txtValue, String dialogTitle) {
        Calendar minDate = Calendar.getInstance(), starDate = Calendar.getInstance(), maxDate = Calendar.getInstance();
        starDate.set(Calendar.DAY_OF_MONTH, 1);
        starDate.set(Calendar.MONTH, 0);
        starDate.set(Calendar.YEAR, 1990);
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        minDate.set(Calendar.MONTH, 0);
        minDate.set(Calendar.YEAR, 1900);
        return new DatePickerReportDialog(this, starDate, minDate, maxDate,
                dialogTitle, txtValue, true);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                switch (requestCode) {
                    case CODE_PHONE_TYPE_1:
                        KeyValuePairModel modelPhoneType1 = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        tvPhone1.setText(modelPhoneType1.getDescription());
                        mRegisterExtraOttModel.setType_1(modelPhoneType1.getID());
                        break;
                    case CODE_PHONE_TYPE_2:
                        KeyValuePairModel modelPhoneType2 = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        tvPhone2.setText(modelPhoneType2.getDescription());
                        mRegisterExtraOttModel.setType_2(modelPhoneType2.getID());
                        break;
                    case CODE_DISTRICT:
                        District district = (District) data.getSerializableExtra(Constants.DISTRICT);
                        tvDistrict.setText(district.getFullNameVN());
                        mRegisterExtraOttModel.setBillTo_District(district.getName());
                        //xóa data ward, street khi chọn lại district
                        tvWard.setText("");
                        tvStreet.setText("");
                        txtFloor.setText("");
                        txtLot.setText("");
                        txtRoom.setText("");
                        tvApartment.setText("");
                        txtHouseNum.setText("");
                        mRegisterExtraOttModel.setBillTo_Number("");
                        mRegisterExtraOttModel.setBillTo_Ward("");
                        mRegisterExtraOttModel.setBillTo_Street("");
                        mRegisterExtraOttModel.setNameVilla("");

                        //clear CLKM khi chọn lại quận/huyện
                        if (mRegisterExtraOttModel.getCartExtraOtt().getServiceList() != null) {
                            for (ServiceListExtraOtt item : mRegisterExtraOttModel.getCartExtraOtt().getServiceList()) {
                                item.setQuantity(0);
                                item.setPromoCommand(new PromoCommand("", item.getPromoCommand().getDetailPackage(),
                                        0, 0, 0, 0, 0));
                            }
                            ((ExtraOttActivity) mContext).fragService.mAdapter
                                    .notifyData(mRegisterExtraOttModel.getCartExtraOtt().getServiceList());
                        }
                        break;
                    case CODE_WARD:
                        tvDistrict.setError(null);
                        Ward ward = (Ward) data.getSerializableExtra(Constants.WARD);
                        tvWard.setText(ward.getNameVN());
                        mRegisterExtraOttModel.setBillTo_Ward(ward.getName());
                        //xóa data  street khi chọn lại district
                        tvStreet.setText("");
                        txtFloor.setText("");
                        txtLot.setText("");
                        txtRoom.setText("");
                        tvApartment.setText("");
                        txtHouseNum.setText("");
                        mRegisterExtraOttModel.setBillTo_Number("");
                        mRegisterExtraOttModel.setBillTo_Street("");
                        mRegisterExtraOttModel.setNameVilla("");
                        break;
                    case CODE_HOUSE_TYPE:
                        KeyValuePairModel modelHouseType = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        tvHouseType.setText(modelHouseType.getDescription());
                        mRegisterExtraOttModel.setTypeHouse(modelHouseType.getID());
                        int id = modelHouseType.getID();
                        switch (id) {
                            case 1:// chọn nhà phố
                                //vùng chứa control Số chung cư
                                apartmentPanel.setVisibility(View.GONE);
                                //vùng chứa control Vị trí nhà
                                housePositionPanel.setVisibility(View.GONE);
                                //vùng chứa control Số nhà
                                houseNumberPanel.setVisibility(View.VISIBLE);
                                // Tầng, Lô, Phòng
                                txtFloor.setText("");
                                txtLot.setText("");
                                txtRoom.setText("");
                                tvApartment.setText("");
                                mRegisterExtraOttModel.setNameVilla("");
                                break;
                            case 2:// cu xa, chung cu, cho, villa, thuong xa
                                apartmentPanel.setVisibility(View.VISIBLE);
                                housePositionPanel.setVisibility(View.GONE);
                                houseNumberPanel.setVisibility(View.GONE);
                                txtHouseNum.setText("");
                                txtHouseNum.setText("");
                                mRegisterExtraOttModel.setBillTo_Number("");
                                break;
                            case 3://Nhà không địa chỉ
                                apartmentPanel.setVisibility(View.GONE);
                                housePositionPanel.setVisibility(View.VISIBLE);
                                houseNumberPanel.setVisibility(View.VISIBLE);
                                txtFloor.setText("");
                                txtLot.setText("");
                                txtRoom.setText("");
                                tvApartment.setText("");
                                mRegisterExtraOttModel.setNameVilla("");
                                break;
                        }
                        break;
                    case CODE_STREET:
                        StreetOrCondo modelStreet = data.getParcelableExtra(Constants.STREET_OR_CONDO);
                        tvStreet.setText(modelStreet.getNameVN());
                        txtFloor.setText("");
                        txtLot.setText("");
                        txtRoom.setText("");
                        tvApartment.setText("");
                        mRegisterExtraOttModel.setBillTo_Street(modelStreet.getId());
                        mRegisterExtraOttModel.setNameVilla("");
                        break;
                    case CODE_CONDO:
                        StreetOrCondo modelCondo = data.getParcelableExtra(Constants.STREET_OR_CONDO);
                        tvApartment.setText(modelCondo.getNameVN());
                        mRegisterExtraOttModel.setNameVilla(modelCondo.getName());
                        break;
                    case CODE_HOUSE_POSITION:
                        KeyValuePairModel modelHousePosition = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        tvHousePosition.setText(modelHousePosition.getDescription());
                        mRegisterExtraOttModel.setPosition(modelHousePosition.getID());
                        break;
                    case CODE_OPEN_GALLERY:
                        Uri selectedImageUri = data.getData();
                        String selectedImagePath = Common.getRealPathUrlImage(mContext, selectedImageUri);
                        if (selectedImagePath == null || selectedImagePath.equals("")) {
                            Common.alertDialog(
                                    getResources().getString(R.string.lbl_error_image_document_select_message),
                                    mContext
                            );
                        } else {
                            Intent intent = new Intent(mContext, IdentityCardScanActivity.class);
                            intent.putExtra(Constants.TAG_LOCAL_PATH_IDENTITY_CARD, selectedImagePath);
                            startActivityForResult(intent, CODE_SCAN_IDENTITY);
                        }
                        break;
                    case CODE_SCAN_IDENTITY:
                        // nhận thông tin quét cmnd
                        if (data.hasExtra(Constants.TAG_INFO_IDENTITY_CARD)) {
                            IdentityCard identityCard = data.getParcelableExtra(Constants.TAG_INFO_IDENTITY_CARD);
                            autoFieldDataIdentityCard(identityCard);
                        }
                        break;
                    case CODE_SOURCE_TYPE:
                        KeyValuePairModel modelSourceType = data.getParcelableExtra(Constants.KEY_PAIR_OBJ);
                        if (modelSourceType == null) return;
                        tvSourceType.setText(modelSourceType.getDescription());
                        mRegisterExtraOttModel.setSourceType(modelSourceType.getID());
                }
            } else {
                // file ảnh trả về sau khi chụp ảnh cmnd ở bước 1
                if (requestCode == REQUEST_CAMERA_TAKE_PHOTO) {
                    File fileIdentityCardCapture = fileIdentityCardImageDocument;
                    if (fileIdentityCardCapture != null) {
                        String pathFileCapture = fileIdentityCardCapture.toString();
                        Intent intent = new Intent(mContext, IdentityCardScanActivity.class);
                        intent.putExtra(Constants.TAG_LOCAL_PATH_IDENTITY_CARD, pathFileCapture);
                        startActivityForResult(intent, CODE_SCAN_IDENTITY);
                    }
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = mContext.getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(mContext, message);
                return;
            }
        }
        switch (requestCode) {
            case CODE_REQUEST_TAKE_PHOTO:
                fileIdentityCardImageDocument = Common.capture();
                takePhoto(fileIdentityCardImageDocument);
                break;
            case CODE_REQUEST_OPEN_GALLERY:
                openGallery();
                break;
        }
    }

    // mở file ảnh từ thiết bị
    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), CODE_OPEN_GALLERY);
    }

    private void takePhoto(File tempFile) {
        Uri selectedImageUri = Uri.fromFile(tempFile);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
        startActivityForResult(cameraIntent, REQUEST_CAMERA_TAKE_PHOTO);
    }

    // điền thông tin chứng minh nhân dân tự động
    public void autoFieldDataIdentityCard(IdentityCard identityCard) {
        if (identityCard != null) {
            txtCustomerName.setText(identityCard.getName() == null ? "" : identityCard.getName());
            txtIdentityCard.setText(identityCard.getId() == null ? "" : identityCard.getId());
            txtBirthDay.setText(identityCard.getDob() == null ? "" : formatDateOfBirthRegister(identityCard.getDob()));
            txtAddressId.setText(identityCard.getAddress() == null ? "" : identityCard.getAddress());
        } else {
            Common.alertDialogNotTitle(getResources().getString(R.string.title_empty_data), mContext);
        }
    }

    public void setRecare(int type){
        // -1: unknown; 0: not recare; 1: recare
        switch (type){
            case 0:
                recareSuccess.setVisibility(View.GONE);
                recareFail.setVisibility(View.VISIBLE);
                break;
            case 1:
                recareSuccess.setVisibility(View.VISIBLE);
                recareFail.setVisibility(View.GONE);
                break;
            default:
                recareSuccess.setVisibility(View.GONE);
                recareFail.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void bindData() {
        //nguồn bán
        tvSourceType.setText(getSourceType(mRegisterExtraOttModel.getSourceType()));

        txtCustomerName.setText(mRegisterExtraOttModel.getFullName() != null ?
                mRegisterExtraOttModel.getFullName() : "");
        txtIdentityCard.setText(mRegisterExtraOttModel.getPassport() != null ?
                mRegisterExtraOttModel.getPassport() : "");
        txtBirthDay.setText(mRegisterExtraOttModel.getBirthday() == null ? "" :
                Common.getSimpleDateFormat(mRegisterExtraOttModel.getBirthday(), Constants.DATE_FORMAT_VN));
        txtAddressId.setText(mRegisterExtraOttModel.getAddressPassport() != null ?
                mRegisterExtraOttModel.getAddressPassport() : "");
        txtTaxNum.setText(mRegisterExtraOttModel.getTaxId() != null ?
                mRegisterExtraOttModel.getTaxId() : "");
        txtEmail.setText(mRegisterExtraOttModel.getEmail() != null ?
                mRegisterExtraOttModel.getEmail() : "");
        if (mRegisterExtraOttModel.getType_1() == 0) {//set default when create registration
            tvPhone1.setText(getPhone(4));
            mRegisterExtraOttModel.setType_1(4);
        } else {
            tvPhone1.setText(getPhone(mRegisterExtraOttModel.getType_1()));
        }

        tvPhone2.setText(getPhone(mRegisterExtraOttModel.getType_2()));
        txtPhone1.setText(mRegisterExtraOttModel.getPhone_1() != null ?
                mRegisterExtraOttModel.getPhone_1() : "");
        txtPhone2.setText(mRegisterExtraOttModel.getPhone_2() != null ?
                mRegisterExtraOttModel.getPhone_2() : "");
        txtContactPhone1.setText(mRegisterExtraOttModel.getContact_1() != null ?
                mRegisterExtraOttModel.getContact_1() : "");
        txtContactPhone2.setText(mRegisterExtraOttModel.getContact_2() != null ?
                mRegisterExtraOttModel.getContact_2() : "");
        tvDistrict.setText(mRegisterExtraOttModel.getBillTo_DistrictVN() != null ?
                mRegisterExtraOttModel.getBillTo_DistrictVN() : "");
        tvWard.setText(mRegisterExtraOttModel.getBillTo_WardVN() != null ?
                mRegisterExtraOttModel.getBillTo_WardVN() : "");
        if (mRegisterExtraOttModel.getTypeHouse() == 0) {//set default when create registration
            tvHouseType.setText(getTypeHouse(1));
            mRegisterExtraOttModel.setTypeHouse(1);
        } else {
            tvHouseType.setText(getTypeHouse(mRegisterExtraOttModel.getTypeHouse()));
            if (mRegisterExtraOttModel.getTypeHouse() == 2) {
                apartmentPanel.setVisibility(View.VISIBLE);
                houseNumberPanel.setVisibility(View.GONE);
                mRegisterExtraOttModel.setBillTo_Number("");
                txtLot.setText(mRegisterExtraOttModel.getLot() != null ?
                        mRegisterExtraOttModel.getLot() : "");
                txtFloor.setText(mRegisterExtraOttModel.getFloor() != null ?
                        mRegisterExtraOttModel.getFloor() : "");
                txtRoom.setText(mRegisterExtraOttModel.getRoom() != null ?
                        mRegisterExtraOttModel.getRoom() : "");
                tvApartment.setText(mRegisterExtraOttModel.getNameVillaDes());
            } else if (mRegisterExtraOttModel.getTypeHouse() == 3) {
                housePositionPanel.setVisibility(View.VISIBLE);
                tvHousePosition.setText(getHousePosition(mRegisterExtraOttModel.getPosition()));
            }
        }
        tvStreet.setText(mRegisterExtraOttModel.getBillTo_StreetVN() != null ?
                mRegisterExtraOttModel.getBillTo_StreetVN() : "");
        txtHouseNum.setText(mRegisterExtraOttModel.getBillTo_Number() != null ?
                mRegisterExtraOttModel.getBillTo_Number() : "");
        txtHouseDesc.setText(mRegisterExtraOttModel.getNote() != null ?
                mRegisterExtraOttModel.getNote() : "");
        txtDescriptionIBB.setText(mRegisterExtraOttModel.getDescriptionIBB() != null ?
                mRegisterExtraOttModel.getDescriptionIBB() : "");
    }
    //ver 3.23
    private String getSourceType(int type){
        if (type == 0) {
            return "";
        }
        ArrayList<KeyValuePairModel> lstSourceType = new ArrayList<>();
        lstSourceType.add(new KeyValuePairModel(1, "Nguồn D2D"));
        lstSourceType.add(new KeyValuePairModel(2, "Nguồn Online"));
        for (KeyValuePairModel item : lstSourceType) {
            if (item.getID() == type) {
                return item.getDescription();
            }
        }
        return "";
    }

    private String getPhone(int type) {
        if (type == 0) {
            return "";
        }
        ArrayList<KeyValuePairModel> lstPhone = new ArrayList<>();
        lstPhone.add(new KeyValuePairModel(1, "Cơ quan"));
        lstPhone.add(new KeyValuePairModel(2, "Fax"));
        lstPhone.add(new KeyValuePairModel(3, "Nhà riêng"));
        lstPhone.add(new KeyValuePairModel(4, "Di động"));
        lstPhone.add(new KeyValuePairModel(5, "Nhắn tin"));
        for (KeyValuePairModel item : lstPhone) {
            if (item.getID() == type) {
                return item.getDescription();
            }
        }
        return "";
    }

    private String getDistrictName(String districtId) {
        if (TextUtils.isEmpty(districtId)) {
            return "";
        }
        List<District> mList = SharedPref.getDistrictList(mContext, Constants.LOCATION_ID);
        if (mList == null || mList.size() == 0) {
            return districtId;
        }
        for (District item : mList) {
            if (item.getName().equals(districtId)) {
                return item.getFullNameVN();
            }else if(item.getFullName().equals(districtId)){
                return item.getFullNameVN();
            }
        }
        return districtId;
    }

    private String getTypeHouse(int typeHouse) {
        if (typeHouse == 0) {
            return "";
        }
        ArrayList<KeyValuePairModel> lstHouseTypes = new ArrayList<>();
        lstHouseTypes.add(new KeyValuePairModel(1, "Nhà phố"));
        lstHouseTypes.add(new KeyValuePairModel(2, "C.Cư, C.xá, Villa, Chợ, Thương Xá"));
        lstHouseTypes.add(new KeyValuePairModel(3, "Nhà không địa chỉ"));
        for (KeyValuePairModel item : lstHouseTypes) {
            if (item.getID() == typeHouse) {
                return item.getDescription();
            }
        }
        return "";
    }

    private String getHousePosition(int id) {
        if (id == 0) {
            return "";
        }
        ArrayList<KeyValuePairModel> lstHousePositions = new ArrayList<>();
        lstHousePositions.add(new KeyValuePairModel(1, "Kế bên phải"));
        lstHousePositions.add(new KeyValuePairModel(2, "Kế bên trái"));
        lstHousePositions.add(new KeyValuePairModel(3, "Đối diện"));
        lstHousePositions.add(new KeyValuePairModel(4, "Phía sau"));
        lstHousePositions.add(new KeyValuePairModel(5, "Cách"));
        for (KeyValuePairModel item : lstHousePositions) {
            if (item.getID() == id) {
                return item.getDescription();
            }
        }
        return "";
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar date = Calendar.getInstance();
        date.set(year, month, dayOfMonth);
        if (mDateDialog.getEditText() == txtBirthDay && txtBirthDay != null) {
            myCalendar = date;
            txtBirthDay.setText(Common.getSimpleDateFormat(myCalendar, Constants.DATE_FORMAT_VN));
        }
    }
}