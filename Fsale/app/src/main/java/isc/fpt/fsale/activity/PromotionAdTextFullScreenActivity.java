package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPromotionBrochureDetail;
import isc.fpt.fsale.model.PromotionBrochureModel;
import isc.fpt.fsale.utils.Constants;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.webkit.WebView;
import android.widget.TextView;

public class PromotionAdTextFullScreenActivity extends FragmentActivity {

    public static final String TAG_PROMOTION_AD_ITEM = "TAG_PROMOTION_AD_ITEM";

    private WebView wvHTML;
    private TextView lblTitle;
    private PromotionBrochureModel promotion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_promotion_ad_text_full_screen);
        wvHTML = (WebView) findViewById(R.id.web_view);
        wvHTML.getSettings().setBuiltInZoomControls(true);
        wvHTML.getSettings().setSupportZoom(true);
        lblTitle = (TextView) findViewById(R.id.lbl_promotion_ad_title);
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(TAG_PROMOTION_AD_ITEM)) {
            promotion = intent.getParcelableExtra(TAG_PROMOTION_AD_ITEM);
            if (promotion != null) {
                lblTitle.setText(promotion.getTitle());
                //wvHTML.loadRpCodeInfo(promotion.getMediaUrl(), "text/html; charset=utf-8","UTF-8");
                getData();
            }
        }
    }

    private void getData() {
        if (promotion != null) {
            new GetPromotionBrochureDetail(this, promotion.getType(), promotion.getID());
        }
    }

    public void loadData(String htlm) {
        wvHTML.loadData(htlm, "text/html; charset=utf-8", "UTF-8");
    }


}
