package isc.fpt.fsale.ui.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.danh32.fontify.Button;
import com.danh32.fontify.EditText;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;


public class Search_ReportSubscriberGrowthForManager extends DialogFragment{
	
	private Context mContext;
	private Spinner SpMonth,SpYear,SpAgent;
	public EditText txt_Agent;
	private KeyValuePairAdapter adapter;
	private Button btnSearch;
	private ImageButton btnClose;
	
	/*private String TextMonth="1";
	private String TextYear=String.valueOf(getCurrentYear(getCurrentDate()));
	private String TextAgent="0";
	private String TextAgentName="0";
	private String Page="1";
	private DialogFragment dialog;*/
	
	private LinearLayout frm_agent;


	public Search_ReportSubscriberGrowthForManager(){ }

	@SuppressLint("ValidFragment")
	public Search_ReportSubscriberGrowthForManager(Context _mContext) {
		// TODO Auto-generated constructor stub
		this.mContext=_mContext;
		//this.dialog = this;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	    // Inflate the layout to use as dialog or embedded fragment
		  View view = inflater.inflate(R.layout.dialog_search_report_subscriber_growth_for_manager, container);
		  btnClose = (ImageButton)view.findViewById(R.id.btn_close);
		  btnSearch=(Button)view.findViewById(R.id.btn_search_list); 
		  //TXT
		  txt_Agent=(EditText)view.findViewById(R.id.txt_search_agent); 
		    //Phieu dang ki
		  
		  	
		  	
		  SpMonth=(Spinner)view.findViewById(R.id.sp_search_month);
		  SpYear=(Spinner)view.findViewById(R.id.sp_search_year);
		  SpAgent=(Spinner)view.findViewById(R.id.sp_search_agent);
		  
		  frm_agent = (LinearLayout) view.findViewById(R.id.frm_agent);
		  
		  // Nếu là nhân viên thì ẩn đi tìm kiếm
		  	if(!Constants.IS_MANAGER)
		  	{
		  		frm_agent.setVisibility(View.GONE);
		  	}
		  	ShowData();
		  	
		  	SpMonth.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
					//KeyValuePairModel selectedItem = (KeyValuePairModel)parentView.getItemAtPosition(position);
					//TextMonth=selectedItem.getsID();
				}
				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub				
				}	
		 	});
		  	SpYear.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
					//KeyValuePairModel selectedItem = (KeyValuePairModel)parentView.getItemAtPosition(position);
					//TextYear=selectedItem.getsID();
				}
				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub				
				}	
		 	});
		  	SpAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
					KeyValuePairModel selectedItem = (KeyValuePairModel)parentView.getItemAtPosition(position);
					if(selectedItem.getID()==0)
					{
						//TextAgent="0";
						txt_Agent.setEnabled(isHidden());
						//TextAgentName="0";
					}
					else
					{
						//TextAgent="1";
						txt_Agent.setEnabled(isAdded());
						txt_Agent.findFocus();
					}
					
				}
				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub				
				}	
		 	});
		  	
		  	btnSearch.setOnClickListener(new View.OnClickListener() {  			
				@Override
				public void onClick(View v) {
					
					/*TextAgentName=txt_Agent.getText().toString().trim();
					if(Common.isEmpty(txt_Agent))
						TextAgentName="0";
					
					if(Constants.IS_MANAGER)
					{
						String params = Services.getParams(new String[]{Constants.USERNAME,TextMonth,TextYear,TextAgent,TextAgentName,Page});
						//
						int month = 0, year = 0;
						try {
							month = Integer.parseInt(TextMonth);
							year = Integer.parseInt(TextYear);
						} catch (Exception e) {
							month = 0;
							year = 0;
						}
						new ReportSubscriberGrowthForManagerAction(mContext,params,false,dialog, month, year);
						
						//new ReportSubscriberGrowthForManagerAction(mContext,params,false,dialog);
					}
					else
					{
						String managername = Constants.MANAGER_NAME;
						if(managername.isEmpty())
							managername = Constants.USERNAME;
			 			String params = Services.getParams(new String[]{managername,TextMonth,TextYear,"1",Constants.USERNAME,"1"});
				 		
			 			//Convert Month/Year from text to int
			 			int month = 0, year = 0;
			 			try {
							month = Integer.parseInt(TextMonth);
							year = Integer.parseInt(TextYear);
						} catch (Exception e) {
							month = 0;
							year = 0;
						}
			 			//Get Report
						
						new ReportSubscriberGrowthForManagerAction(mContext,params,false,dialog, month, year);
						
			 			//new ReportSubscriberGrowthForManagerAction(mContext,params,false,dialog);
					}
					
				*/
				}
    	  });
		  
		  btnClose.setOnClickListener(new View.OnClickListener() {  			
				@Override
				public void onClick(View v) {
					getDialog().dismiss();
				}
    	  });	
		  
		  return view;
	}
	
	/** The system calls this only when creating the layout in a dialog. */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {	   
	    Dialog dialog = super.onCreateDialog(savedInstanceState);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    return dialog;
	}
	
	 public void ShowData()
	 {
		 
		 	// Lấy tháng và năm hiện tại
			Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			
			//Thang Search
		 	ArrayList<KeyValuePairModel> ListSpMonth = new ArrayList<KeyValuePairModel>();
		 	for(int i=1;i<13;i++)
		 	{
		 		String GT= String.valueOf(i);
		 		ListSpMonth.add(new KeyValuePairModel(GT,GT));
		 	}
			adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpMonth, Gravity.LEFT);
			SpMonth.setAdapter(adapter);
			
			// Set tháng là tháng hiện tại
			SpMonth.setSelection(Common.getIndex(SpMonth, String.valueOf(month+1)), true);
			
			//Nam Search
			ArrayList<KeyValuePairModel> ListSpYear = new ArrayList<>();
			int iYear = getCurrentYear(getCurrentDate());
			int jYear=iYear-1;
			ListSpYear.add(new KeyValuePairModel(String.valueOf(iYear),String.valueOf(iYear)));
			ListSpYear.add(new KeyValuePairModel(String.valueOf(jYear),String.valueOf(jYear)));
			adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpYear, Gravity.LEFT);
			SpYear.setAdapter(adapter);	
			
			// Set năm là năm hiện tại
			SpYear.setSelection(Common.getIndex(SpYear, String.valueOf(year)), true);
			
			//Loai Tim kiem
			ArrayList<KeyValuePairModel> ListSpAgent= new ArrayList<>();
			ListSpAgent.add(new KeyValuePairModel(0,"Tất cả"));
			ListSpAgent.add(new KeyValuePairModel(1,"Nhân viên"));
			adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpAgent, Gravity.LEFT);
			SpAgent.setAdapter(adapter);	
			
			
	 }
	 
	@SuppressLint("SimpleDateFormat")
	public static String getCurrentDate(){
			String currentDate="";
			Calendar c= Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			currentDate =  df.format(c.getTime());
			return currentDate;
		}
	 public static int getCurrentYear(String sCurrentDate){
			return Integer.parseInt((String) sCurrentDate.subSequence(6, 10));
		}	

}
