package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ActivityByContractModel;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ActivityListByContractAdapter  extends BaseAdapter{
	private List<ActivityByContractModel> mList;	
	private Context mContext;
	
	public ActivityListByContractAdapter(Context context, List<ActivityByContractModel> lst){
		this.mContext = context;
		this.mList = lst;	
	}
	
	public void setListData(List<ActivityByContractModel> lst){
		this.mList.clear();
		this.mList.addAll(lst);
		this.notifyDataSetChanged();		
	}
	
	public void addAll(List<ActivityByContractModel> lst){
		this.mList.addAll(lst);
		this.notifyDataSetChanged();		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ActivityByContractModel item = mList.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_customer_care_activity_list_by_contract_item, null);
		}
		if(item != null){
			TextView lblActivityTypeName = (TextView) convertView.findViewById(R.id.lbl_activity_type_name);
			lblActivityTypeName.setText(item.getActivity_Type_Name());
			
			TextView lblActivityName = (TextView) convertView.findViewById(R.id.lbl_activity_name);
			lblActivityName.setText(item.getActivity_Name());
			
			TextView lblContract = (TextView) convertView.findViewById(R.id.lbl_contract);
			lblContract.setText(item.getContract());
			
			TextView lblID = (TextView) convertView.findViewById(R.id.lbl_id);
			lblID.setText(item.getActivity_code());
			
			TextView lblCreateDate = (TextView) convertView.findViewById(R.id.lbl_create_date);
			lblCreateDate.setText(item.getDate());
			
			TextView lblCreateBy = (TextView) convertView.findViewById(R.id.lbl_activity_by);
			lblCreateBy.setText(item.getActivity_by());
			
			TextView lblContent = (TextView) convertView.findViewById(R.id.lbl_content);
			lblContent.setText(item.getContent());
			
			TextView lblRemark= (TextView) convertView.findViewById(R.id.lbl_remark);
			if(item.getRemark() != null && !item.getRemark().equals("")){
				lblRemark.setText(item.getRemark());
			}else{
				lblRemark.setVisibility(View.GONE);
			}
		}
		
		return convertView;
	}

}
