package isc.fpt.fsale.activity.source_type;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.extra_ott.adapter.KeyPairValueAdapter;
import isc.fpt.fsale.utils.Constants;

public class SourceTypeActivity extends BaseActivitySecond implements OnItemClickListener<KeyValuePairModel> {
    private RelativeLayout rltBack;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_cus_type;
    }

    @Override
    protected void initView() {
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_cus_type);
        ArrayList<KeyValuePairModel> lst = new ArrayList<>();
        lst.add(new KeyValuePairModel(1, "Nguồn D2D"));
        lst.add(new KeyValuePairModel(2, "Nguồn Online"));
        KeyPairValueAdapter mAdapter = new KeyPairValueAdapter(this, lst, this);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onItemClick(KeyValuePairModel object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.KEY_PAIR_OBJ, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
