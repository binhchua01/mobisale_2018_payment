package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.PrechecklistAdapter;
import isc.fpt.fsale.model.ListPrechecklistModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ListView;

public class ListPrechecklistActivity extends BaseActivity {

    private ArrayList<ListPrechecklistModel> lstPrechecklist;
    private ListView lvPrechecklist;
    private PrechecklistAdapter adapter;

    public ListPrechecklistActivity() {

        // TODO Auto-generated constructor stub
        super(R.string.lbl_precheck_list);
        MyApp.setCurrentActivity(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_precheck_list_activity));

        setContentView(R.layout.list_prechecklist);
		
		  /*if (DeviceInfo.ANDROID_SDK_VERSION >=11) {
				try{
					android.app.ActionBar actionBar = getActionBar();
					actionBar.setDisplayHomeAsUpEnabled(true);
					actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.main_color_blue));	
					actionBar.setTitle(R.string.lbl_precheck_list);
				}
				catch(Exception e){
					e.printStackTrace();
				}			
			}*/

        lvPrechecklist = (ListView) findViewById(R.id.lv_precheck_list);
        lvPrechecklist.setItemsCanFocus(true);

        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                lstPrechecklist = myIntent.getParcelableArrayListExtra("list_prechecklist");

                if (lstPrechecklist != null) {
                    adapter = new PrechecklistAdapter(ListPrechecklistActivity.this, lstPrechecklist);
                    lvPrechecklist.setAdapter(adapter);

                }
            }
        } catch (Exception e) {

            e.printStackTrace();
            Log.d("LOG_GET_EXTRA_PRECHECK_LIST", "Error: " + e.getMessage());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
