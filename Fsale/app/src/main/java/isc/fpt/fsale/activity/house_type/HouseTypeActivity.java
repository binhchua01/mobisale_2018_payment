package isc.fpt.fsale.activity.house_type;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.extra_ott.adapter.KeyPairValueAdapter;
import isc.fpt.fsale.utils.Constants;

public class HouseTypeActivity extends BaseActivitySecond
        implements OnItemClickListener<KeyValuePairModel> {
    private RelativeLayout rltBack;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_house_type;
    }

    @Override
    protected void initView() {
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_house_type);
        ArrayList<KeyValuePairModel> lstHouseTypes = new ArrayList<>();
        lstHouseTypes.add(new KeyValuePairModel(1, "Nhà phố"));
        lstHouseTypes.add(new KeyValuePairModel(2, "C.Cư, C.xá, Villa, Chợ, Thương Xá"));
        lstHouseTypes.add(new KeyValuePairModel(3, "Nhà không địa chỉ"));
        KeyPairValueAdapter mAdapter = new KeyPairValueAdapter(this, lstHouseTypes, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(KeyValuePairModel object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.KEY_PAIR_OBJ, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
