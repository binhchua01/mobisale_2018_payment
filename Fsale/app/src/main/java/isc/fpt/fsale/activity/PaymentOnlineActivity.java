package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckStatusPayment;
import isc.fpt.fsale.model.CheckStatusPaymentPost;
import isc.fpt.fsale.model.PaymentInformationResult;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

// màn hình thanh toán online bán mới QR, TPBANK, QUỐC TẾ
public class PaymentOnlineActivity extends BaseActivity {
    //Declares variables
    private boolean statusProcessEnd;
    private int timeDelay = 60;
    private Context mContext;
    private CheckStatusPaymentPost checkStatusPaymentPost;
    private String paidTypeValue = "";
    private int typePaidSelected;
    private PaymentInformationResult payInfoPublic = null;
    //Widgets
    //nút kiểm tra trạng thái thanh toán
    private Button txtWaitingForProcessPayment;
    // ảnh QR code
    private ImageView imageQrImage;
    private TextView tvCodePayContract, tvCusNamePayContract, tvPaidType, tvTotalPayContract, tvChangePaidType;

    /**
     * Constructor
     */
    public PaymentOnlineActivity() {
        super(R.string.qr_pay_mobile_header_activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        this.mContext = this;
        createWidgets();
        initDataIntent();
        createEvents();
        createWaitingProcess();

    }

    /**
     * Create Widgets
     */
    private void createWidgets() {
        txtWaitingForProcessPayment = (Button) findViewById(R.id.txt_waiting_for_process_payment);
        imageQrImage = (ImageView) findViewById(R.id.iv_Qr_Image);
        tvCodePayContract = (TextView) findViewById(R.id.tv_code_pay_contract);
        tvCusNamePayContract = (TextView) findViewById(R.id.tv_cus_name_pay_contract);
        tvPaidType = (TextView) findViewById(R.id.tvPaidType);
        tvTotalPayContract = (TextView) findViewById(R.id.tv_total_pay_contract);
        tvChangePaidType = (TextView) findViewById(R.id.tvChangePaidType);
    }

    /**
     * Create Events
     */
    private void createEvents() {
        tvChangePaidType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
        txtWaitingForProcessPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CheckStatusPayment(mContext, checkStatusPaymentPost, payInfoPublic, paidTypeValue);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
        ((MyApp) getApplication()).registerReceiver(mMessageReceiver,
                new IntentFilter("NAVIGATE_CONFIRM_PAYMENT"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        ((MyApp) getApplication()).unregisterReceiver(mMessageReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        statusProcessEnd = true;
    }

    //init wait for processing payment
    public void createWaitingProcess() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!statusProcessEnd) {
                    handler.sendEmptyMessage(0);
                    try {
                        Thread.sleep(1000);
                        timeDelay--;
                        if (timeDelay == 0) {
                            statusProcessEnd = true;
                            handler.sendEmptyMessage(0);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    //lấy thông tin khách hàng
    private void initDataIntent() {
        Intent intent = getIntent();
        checkStatusPaymentPost = new CheckStatusPaymentPost();
        if (intent != null) {
            typePaidSelected = intent.getIntExtra("PAID_TYPE_SELECTED", 0);
            paidTypeValue = intent.getStringExtra("PAID_TYPE_VALUE");
            payInfoPublic = intent.getParcelableExtra("CONFIRM_PAYMENT_INFO");
            if (typePaidSelected != 50) {
                tvChangePaidType.setVisibility(View.VISIBLE);
                imageQrImage.setVisibility(View.VISIBLE);
                String base64QRCode = intent.getStringExtra("BASE64_QR_CODE");
                if (base64QRCode != null) {
                    if (base64QRCode.length() > 0) {
                        Bitmap qrImage = Common.StringToBitMap(base64QRCode);
                        imageQrImage.setImageBitmap(qrImage);
                        //Set dimension for image
                        Display display = getWindowManager().getDefaultDisplay();
                        Point pointSize = new Point();
                        display.getSize(pointSize);
                        int width = Integer.valueOf(pointSize.x * 90 / 100); //get 90% width of screen
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, width);
                        layoutParams.gravity = Gravity.CENTER;
                        imageQrImage.setLayoutParams(layoutParams);
                    }
                }
            }
            tvPaidType.setText(paidTypeValue);
            if (payInfoPublic != null) {
                tvCusNamePayContract.setText(payInfoPublic.getFullName());
                tvCodePayContract.setText(payInfoPublic.getContract());
                tvTotalPayContract.setText(Common.formatNumber(payInfoPublic.getTotalUnpaidReceipts()));
                if (typePaidSelected == 40) {
                    Common.reportActivityCreate(getApplication(), getString(R.string.qr_pay_the_header_activity));
                    setTitle(R.string.qr_pay_the_header_activity);
                } else if (typePaidSelected == 30) {
                    Common.reportActivityCreate(getApplication(), getString(R.string.qr_pay_mobile_header_activity));
                } else {
                    setTitle(R.string.tp_bank_pay_the_header_activity);
                    Common.reportActivityCreate(getApplication(), getString(R.string.tp_bank_pay_the_header_activity));
                }
                checkStatusPaymentPost.setContract(payInfoPublic.getContract());
                checkStatusPaymentPost.setSaleMan(payInfoPublic.getCreateBy());
                checkStatusPaymentPost.setRegCode(payInfoPublic.getRegCode());
            }
        } else {
            Common.showToast(mContext, "Thông tin khách hàng truyền đi thất bại!");
        }
    }

    private final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (timeDelay == 0) {
                txtWaitingForProcessPayment.setText("Kiểm tra Trạng thái Thanh toán");
                txtWaitingForProcessPayment.setBackgroundColor(getResources().getColor(R.color.button_dialog));
                txtWaitingForProcessPayment.setEnabled(true);
            } else {
                txtWaitingForProcessPayment.setText("Kiểm tra Trạng thái Thanh toán (Đợi " + timeDelay + "s)");
            }

        }
    };
    // chuyển sang trang thanh toán thành công.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @SuppressLint("DefaultLocale")
        @Override
        public void onReceive(Context context, Intent intent) {
            String regCode = intent.getStringExtra("RegCode");
            Intent confirmIntent = new Intent(mContext, ConfirmPaymentActivity.class);
            confirmIntent.putExtra("PAID_TYPE_VALUE", paidTypeValue);
            if (regCode.equals(payInfoPublic.getRegCode())) {
                confirmIntent.putExtra("CONFIRM_PAYMENT_INFO", payInfoPublic);
                mContext.startActivity(confirmIntent);
                //hủy màn hình thanh toán hiện tại
                if (Common.checkLifeActivity(mContext)) {
                    ((Activity) mContext).finish();
                }
            }
        }
    };
}
