package isc.fpt.fsale.activity.street_or_condo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetStreetOrCondo;
import isc.fpt.fsale.model.StreetOrCondo;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.SharedPref;

public class StreetOrCondoActivity extends BaseActivitySecond
        implements OnItemClickListener<StreetOrCondo> {
    private RelativeLayout rltBack;
    private ImageView imgRefresh;
    private String district, ward;
    private int type;
    private List<StreetOrCondo> mList;
    private StreetOrCondoAdapter mAdapter;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (district != null && ward != null && type != -1) {
                    //get list ward
                    new GetStreetOrCondo(StreetOrCondoActivity.this, district, ward, type);
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_street_or_condo;
    }

    @Override
    protected void initView() {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_street_or_condo);
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        imgRefresh = (ImageView) findViewById(R.id.img_refresh);
        mList = new ArrayList<>();
        mAdapter = new StreetOrCondoAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);
        getDataIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<StreetOrCondo> mList = SharedPref.getStreetOrCondoList(this, district, ward, type);
        if (mList != null && mList.size() > 0) {
            this.mList = mList;
            mAdapter.notifyData(this.mList);
        } else {
            if (district != null && ward != null && type != -1) {
                //get list ward
                new GetStreetOrCondo(this, district, ward, type);
            }
        }
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getString(Constants.DISTRICT_ID) != null
                && bundle.getString(Constants.WARD_ID) != null && bundle.getInt(Constants.TYPE) != -1) {
            district = bundle.getString(Constants.DISTRICT_ID);
            ward = bundle.getString(Constants.WARD_ID);
            type = bundle.getInt(Constants.TYPE);
        }
    }

    public void loadStreetOrCondo() {
        List<StreetOrCondo> mList = SharedPref.getStreetOrCondoList(this, district, ward, type);
        if (mList != null && mList.size() > 0) {
            this.mList = mList;
            mAdapter.notifyData(this.mList);
        }
    }

    @Override
    public void onItemClick(StreetOrCondo object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.STREET_OR_CONDO, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
