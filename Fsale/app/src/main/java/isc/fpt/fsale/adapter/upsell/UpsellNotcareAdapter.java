package isc.fpt.fsale.adapter.upsell;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.callback.upsell.OnItemListenerUpsell;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeForCare;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeListModel;

public class UpsellNotcareAdapter extends RecyclerView.Adapter<UpsellNotcareAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<ObjUpgradeListModel> list;
    private OnItemListenerUpsell mListener;


    public UpsellNotcareAdapter(Context mContext, List<ObjUpgradeListModel> mList, OnItemListenerUpsell mListener) {
        this.mContext = mContext;
        this.list = mList;
        this.mListener = mListener;
    }


    @Override
    public UpsellNotcareAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_recycler_upsell_notcare, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(UpsellNotcareAdapter.SimpleViewHolder holder, int position) {
        holder.bindView(list.get(position), position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView mNumberItem, mContract, mName, mPhone, mAddress, mService, mCLKM;
        LinearLayout mPeriod, layoutContainer;

        public SimpleViewHolder(View itemView) {
            super(itemView);

            mNumberItem = itemView.findViewById(R.id.txt_number_item);
            mContract = itemView.findViewById(R.id.txt_number_contract);
            mName = itemView.findViewById(R.id.txt_name);
            mPhone = itemView.findViewById(R.id.txt_phone);
            mAddress = itemView.findViewById(R.id.txt_address);
            mService = itemView.findViewById(R.id.txt_service);
            mCLKM = itemView.findViewById(R.id.txt_clkm);
            mPeriod = itemView.findViewById(R.id.layout_period);
            mPeriod.setVisibility(View.GONE);
            layoutContainer = itemView.findViewById(R.id.layoutContainer);
        }

        public void bindView(final ObjUpgradeListModel model, final int position) {
            mNumberItem.setText(position + 1 + "");
            mContract.setText(model.getContract());
            mName.setText(model.getFullName());
            mPhone.setText(model.getPhone());
            mAddress.setText(model.getAddress());
            mService.setText(model.getLocalTypeName());
            mCLKM.setText(model.getPromotionName());

            layoutContainer.setOnClickListener(v -> mListener.NotCareOnItemListener(model));
        }
    }
}
