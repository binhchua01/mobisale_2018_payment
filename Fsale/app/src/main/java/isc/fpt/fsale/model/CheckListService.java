package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by haulc3 on 16,January,2019
 */

public class CheckListService implements Parcelable {
    private boolean Camera;
    private boolean Device;
    private boolean IPTV;
    private boolean Internet;
    private boolean OTT;
    private boolean Maxy;
    private boolean ExtraOTT;

    public CheckListService() {
    }

    public CheckListService(boolean camera, boolean device, boolean IPTV,
                            boolean internet, boolean OTT, boolean maxy, boolean extraOTT) {
        Camera = camera;
        Device = device;
        this.IPTV = IPTV;
        Internet = internet;
        this.OTT = OTT;
        Maxy = maxy;
        ExtraOTT = extraOTT;
    }

    public boolean isCamera() {
        return Camera;
    }

    public void setCamera(boolean camera) {
        this.Camera = camera;
    }

    public boolean isDevice() {
        return Device;
    }

    public void setDevice(boolean device) {
        this.Device = device;
    }

    public boolean isIPTV() {
        return IPTV;
    }

    public void setIPTV(boolean IPTV) {
        this.IPTV = IPTV;
    }

    public boolean isInternet() {
        return Internet;
    }

    public void setInternet(boolean internet) {
        this.Internet = internet;
    }

    public boolean isOTT() {
        return OTT;
    }

    public void setOTT(boolean OTT) {
        this.OTT = OTT;
    }

    public boolean isExtraOTT() {
        return ExtraOTT;
    }

    public void setExtraOTT(boolean extraOTT) {
        ExtraOTT = extraOTT;
    }

    public boolean isMaxy() {
        return Maxy;
    }

    public void setMaxy(boolean maxy) {
        Maxy = maxy;
    }

    protected CheckListService(Parcel in) {
        Camera = in.readByte() != 0;
        Device = in.readByte() != 0;
        IPTV = in.readByte() != 0;
        Internet = in.readByte() != 0;
        OTT = in.readByte() != 0;
        ExtraOTT = in.readByte() != 0;
        Maxy = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (Camera ? 1 : 0));
        parcel.writeByte((byte) (Device ? 1 : 0));
        parcel.writeByte((byte) (IPTV ? 1 : 0));
        parcel.writeByte((byte) (Internet ? 1 : 0));
        parcel.writeByte((byte) (OTT ? 1 : 0));
        parcel.writeByte((byte) (ExtraOTT ? 1 : 0));
        parcel.writeByte((byte) (Maxy ? 1 : 0));
    }

    public static final Creator<CheckListService> CREATOR = new Creator<CheckListService>() {
        @Override
        public CheckListService createFromParcel(Parcel in) {
            return new CheckListService(in);
        }

        @Override
        public CheckListService[] newArray(int size) {
            return new CheckListService[size];
        }
    };
}
