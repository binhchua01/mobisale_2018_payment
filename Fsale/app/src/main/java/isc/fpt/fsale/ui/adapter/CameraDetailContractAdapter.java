package isc.fpt.fsale.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV4;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.utils.Common;

public class CameraDetailContractAdapter extends RecyclerView.Adapter<CameraDetailContractAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<CameraDetail> mList;
    private OnItemClickListenerV4 mListener;
    private OnItemClickListener mUpdatePromotion;

    public CameraDetailContractAdapter(Context mContext, List<CameraDetail> mList,
                                       OnItemClickListenerV4 mListener, OnItemClickListener mUpdatePromotion) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
        this.mUpdatePromotion = mUpdatePromotion;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_list_camera_device_selected, parent, false);
        return new SimpleViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.bindView(mList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvCameraDeviceName, tvCameraPackageQuantity, tvPricePerUnit,
                tvCameraPackageMore, tvCameraPackageLess, tvCameraPackage;

        SimpleViewHolder(View itemView) {
            super(itemView);
            tvCameraDeviceName = (TextView) itemView.findViewById(R.id.tv_camera_device_name);
            tvCameraPackage = (TextView) itemView.findViewById(R.id.spn_camera_device_package);
            tvCameraPackageMore = (TextView) itemView.findViewById(R.id.tv_camera_package_plus);
            tvCameraPackageLess = (TextView) itemView.findViewById(R.id.tv_camera_package_less);
            tvCameraPackageQuantity = (TextView) itemView.findViewById(R.id.lbl_camera_quantity);
            tvPricePerUnit = (TextView) itemView.findViewById(R.id.tv_price_per_unit);
        }

        void bindView(final CameraDetail mCameraDetail, final int position) {
            //set value default to view
            tvCameraDeviceName.setText(mCameraDetail.getTypeName());
            if (mCameraDetail.getPackName().equals("")) {
                tvCameraPackage.setText(mContext.getResources().getString(R.string.msg_spinner_item_default));
            } else {
                tvCameraPackage.setText(mCameraDetail.getPackName());
            }
            tvCameraPackageQuantity.setText(String.valueOf(mCameraDetail.getQty()));
            tvPricePerUnit.setText(
                    calculatorTotalCameraPackage(mCameraDetail.getCost(), mCameraDetail.getQty())
            );

            //set event when click to position
            tvCameraPackage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClickV4(mCameraDetail, position, 1);
                }
            });

            //set event press less or plus quantity
            tvCameraPackageLess.setOnClickListener(view -> {
                int minQuantity = mList.get(position).getMinCam();
                int quantity = mList.get(position).getQty();
                if (quantity == minQuantity) {
                    return;
                }
                quantity--;
                mList.get(position).setQty(quantity);
                tvPricePerUnit.setText(
                        calculatorTotalCameraPackage(mList.get(position).getCost(), mList.get(position).getQty())
                );
                notifyDataChanged(mList);
                if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
                    ((RegisterActivityNew) mContext).step2.modelDetail.getObjectCameraOfNet().setCameraDetail(mList);
                } else {
                    ((RegisterContractActivity) mContext).getCameraFragment().mRegister.getObjectCameraOfNet().setCameraDetail(mList);
                }
                mUpdatePromotion.onItemClick(true);
            });

            tvCameraPackageMore.setOnClickListener(view -> {
                int maxQuantity = mList.get(position).getMaxCam();
                int quantity = mList.get(position).getQty();
                if (quantity == maxQuantity) {
                    return;
                }
                quantity++;
                mList.get(position).setQty(quantity);
                tvPricePerUnit.setText(
                        calculatorTotalCameraPackage(mList.get(position).getCost(), mList.get(position).getQty())
                );
                notifyDataChanged(mList);
                if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
                    ((RegisterActivityNew) mContext).step2.modelDetail.getObjectCameraOfNet().setCameraDetail(mList);
                } else {
                    ((RegisterContractActivity) mContext).getCameraFragment().mRegister.getObjectCameraOfNet().setCameraDetail(mList);
                }
                mUpdatePromotion.onItemClick(true);
            });
        }
    }

    public void notifyDataChanged(List<CameraDetail> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    private String calculatorTotalCameraPackage(int cost, int quantity) {
        return String.format(mContext.getString(R.string.txt_unit_price), Common.formatNumber((cost * quantity)));
    }
}
