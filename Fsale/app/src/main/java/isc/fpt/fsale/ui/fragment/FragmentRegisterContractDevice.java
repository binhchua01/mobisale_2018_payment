package isc.fpt.fsale.ui.fragment;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.activity.device_list.DeviceListActivity;
import isc.fpt.fsale.activity.device_price_list.DevicePriceListActivity;
import isc.fpt.fsale.adapter.DeviceAdapter;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV3;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentRegisterContractDevice extends Fragment implements OnItemClickListenerV3<Integer, Device, Integer> {
    private Context mContext;
    private RegistrationDetailModel mRegister;
    private DeviceAdapter mDeviceAdapter;
    private List<Device> mListDeviceSelected;
    private View mLayoutListDevice;
    private Button btnAddDevice;

    private final int CODE_DEVICE_LIST_SELECTED = 102;
    private final int CODE_DEVICE_PRICE_LIST_SELECTED = 103;

    public static FragmentRegisterContractDevice newInstance() {
        FragmentRegisterContractDevice fragment = new FragmentRegisterContractDevice();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentRegisterContractDevice() {

    }

    public List<Device> getListDeviceSelect() {
        return mListDeviceSelected;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_contract_device, container, false);
        this.mContext = getActivity();
        getRegisterFromActivity();
        initView(view);
        initDataDevice();
        initEventDevice();
        return view;
    }

    // init controls device
    public void initView(View view) {
        btnAddDevice = (Button) view.findViewById(R.id.btn_add_device);
        mLayoutListDevice = view.findViewById(R.id.frm_list_contract_device);
        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.list_device);

        mListDeviceSelected = new ArrayList<>();
        mDeviceAdapter = new DeviceAdapter(this.getActivity(), mListDeviceSelected, this);
        mRecyclerView.setAdapter(mDeviceAdapter);
    }

    public void initDataDevice() {
        if (mRegister != null) {
            mListDeviceSelected = mRegister.getListDevice();
            mDeviceAdapter.notifyData(mListDeviceSelected);
            if (mListDeviceSelected.size() > 0) {
                mLayoutListDevice.setVisibility(View.VISIBLE);
            }
        }
    }

    public void initEventDevice() {
        btnAddDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Constants.DEVICE_LIST_SELECTED,
                        (ArrayList<? extends Parcelable>) mListDeviceSelected);
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractDevice.class.getSimpleName());
                Intent intent = new Intent(getActivity(), DeviceListActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_DEVICE_LIST_SELECTED);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case CODE_DEVICE_LIST_SELECTED:
                    Device mDevice = data.getParcelableExtra(Constants.DEVICE);
                    mListDeviceSelected.add(mDevice);
                    if (mListDeviceSelected.size() > 0) {
                        mLayoutListDevice.setVisibility(View.VISIBLE);
                    }else{
                        mLayoutListDevice.setVisibility(View.GONE);
                    }
                    mDeviceAdapter.notifyData(mListDeviceSelected);
                    break;
                case CODE_DEVICE_PRICE_LIST_SELECTED:
                    Device mDeviceSelected = data.getParcelableExtra(Constants.DEVICE);
                    int position = data.getIntExtra(Constants.POSITION, 0);

                    if (mListDeviceSelected.size() > 1) {
                        for (Device item : mListDeviceSelected) {
                            if (item.getDeviceID() == mDeviceSelected.getDeviceID() &&
                                    item.getPriceID() == mDeviceSelected.getPriceID()) {
                                Common.alertDialog(getString(R.string.message_double_price), this.getActivity());
                                return;
                            }
                        }
                    }

                    mListDeviceSelected.set(position, mDeviceSelected);
                    mDeviceAdapter.notifyData(mListDeviceSelected);
                    break;
            }
        }
    }

    public boolean checkForUpdate() {
        return true;
    }

    private void getRegisterFromActivity() {
        if (mContext != null
                && mContext.getClass().getSimpleName()
                .equals(RegisterContractActivity.class.getSimpleName())) {
            mRegister = ((RegisterContractActivity) mContext).getRegister();
        }
    }

    public void updateRegister() {
        try {
            if (mContext.getClass().getSimpleName()
                    .equals(RegisterContractActivity.class.getSimpleName())) {
                RegisterContractActivity activity = (RegisterContractActivity) mContext;
                RegistrationDetailModel register = activity.getRegister();
                register.setListDevice(mListDeviceSelected);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(Integer type, Device mDevice, Integer position) {
        switch (type) {
            case 0:
                mListDeviceSelected.remove(mDevice);
                mDeviceAdapter.notifyData(mListDeviceSelected);
                break;
            case 1:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.DEVICE, mDevice);
                bundle.putInt(Constants.POSITION, position);
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractDevice.class.getSimpleName());
                Intent intent = new Intent(getActivity(), DevicePriceListActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_DEVICE_PRICE_LIST_SELECTED);
                break;
        }
    }
}
