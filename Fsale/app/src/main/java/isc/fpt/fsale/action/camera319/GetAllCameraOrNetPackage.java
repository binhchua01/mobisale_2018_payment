package isc.fpt.fsale.action.camera319;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CameraPackage;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_package.CameraOrNetPackageActivity;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_package.CameraPackageActivity;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by Hau Le on 2019-01-02.
 */

public class GetAllCameraOrNetPackage implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private CameraDetail mCameraDetail;

    public GetAllCameraOrNetPackage(Context mContext, CameraDetail mCameraDetail, int custype, int regtype) {
        this.mContext = mContext;
        this.mCameraDetail = mCameraDetail;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("CameraTypeID", mCameraDetail.getTypeID());
            jsonObject.put("CusType", custype);
            jsonObject.put("RegType", regtype);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.msg_progress_get_all_camera_package);
        String GET_ALL_CAMERA_PACKAGE = "GetAllCameraOfNet_Package";
        CallServiceTask service = new CallServiceTask(mContext, GET_ALL_CAMERA_PACKAGE, jsonObject,
                Services.JSON_POST_OBJECT, message, GetAllCameraOrNetPackage.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            List<CameraPackage> lst;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<CameraPackage> resultObject = new WSObjectsModel<>(jsObj, CameraPackage.class);
                lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (mContext != null) {
                    if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                        Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                    } else {
                        ((CameraOrNetPackageActivity) mContext).loadCameraPackage(lst, mCameraDetail);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
