package isc.fpt.fsale.activity.district;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetDistrictList;
import isc.fpt.fsale.model.District;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.SharedPref;

public class DistrictActivity extends BaseActivitySecond
        implements OnItemClickListener<District> {
    private List<District> mList;
    private DistrictAdapter mAdapter;
    private RelativeLayout rltBack;
    private ImageView imgRefresh;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get list District
                new GetDistrictList(DistrictActivity.this, Constants.LOCATION_ID);
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_district;
    }

    @Override
    protected void initView() {
        bindView();
    }

    private void bindView() {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_district);
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        imgRefresh = (ImageView) findViewById(R.id.img_refresh);
        mList = new ArrayList<>();
        mAdapter = new DistrictAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<District> mList = SharedPref.getDistrictList(this, Constants.LOCATION_ID);
        if (mList != null && mList.size() > 0) {
            this.mList = mList;
            mAdapter.notifyData(this.mList);
        } else {
            //get list District
            new GetDistrictList(this, Constants.LOCATION_ID);
        }
    }

    public void loadDistrictList() {
        List<District> mList = SharedPref.getDistrictList(this, Constants.LOCATION_ID);
        if (mList != null && mList.size() > 0) {
            this.mList = mList;
            mAdapter.notifyData(this.mList);
        }
    }

    @Override
    public void onItemClick(District object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.DISTRICT, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
