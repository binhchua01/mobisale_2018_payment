package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ListRegistrationModel;

public class RegistrationListAdapter extends BaseAdapter {
    private List<ListRegistrationModel> mList;
    private Context mContext;

    public RegistrationListAdapter(Context context, List<ListRegistrationModel> lst) {
        this.mContext = context;
        this.mList = lst;
    }

    @Override
    public int getCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mList != null ? mList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListRegistrationModel registrationList = mList.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_row_registration, null);
        }
        TextView txtRowNumber = (TextView) convertView.findViewById(R.id.txt_row_number);
        txtRowNumber.setText(registrationList.getRowNumber());
        TextView txtRegCode = (TextView) convertView.findViewById(R.id.txt_registration_id);
        txtRegCode.setText(registrationList.getRegistrationId());
        TextView txtCusName = (TextView) convertView.findViewById(R.id.txt_cus_name_registration);
        txtCusName.setText(registrationList.getFullName());
        TextView txtAddress = (TextView) convertView.findViewById(R.id.txt_cus_address_registration);
        txtAddress.setText(registrationList.getAddress());
        TextView txtLocalType = (TextView) convertView.findViewById(R.id.txt_local_type_registration);
        txtLocalType.setText(registrationList.getLocalType());
        TextView txtStatusBookPort = (TextView) convertView.findViewById(R.id.txt_status_book_port);
        if (registrationList.getStatusBookPort() != null)
            txtStatusBookPort.setText(registrationList.getStatusBookPort().equals("1") ? "Đã book port" : "Chưa book port");
        else if (registrationList.getStrStatusBookPort() != null)
            txtStatusBookPort.setText(registrationList.getStrStatusBookPort());
        TextView txtStatusInvest = (TextView) convertView.findViewById(R.id.txt_status_invest);
        txtStatusInvest.setText(registrationList.getStatusInVest().equals("1") ? "Đã khảo sát" : "Chưa khảo sát");
        TextView txtStatusBilling = (TextView) convertView.findViewById(R.id.txt_status_billing);
        txtStatusBilling.setText(registrationList.getStatusBilling().equals("1") ? "Đã xuất phiếu thu" : "Chưa xuất phiếu thu");
        TextView txtStatusCollectMoney = (TextView) convertView.findViewById(R.id.txt_status_collection_money);
        txtStatusCollectMoney.setText(registrationList.getStatusDeposit() > 0 ? "Đã thu tiền" : "Chưa thu tiền");
        if(registrationList.getStatusDeposit() > 0){
            txtStatusCollectMoney.setTextColor(mContext.getResources().getColor(R.color.green));
        }else{
            txtStatusCollectMoney.setTextColor(mContext.getResources().getColor(R.color.red));
        }
        TextView txtStatusPaidMoney = (TextView) convertView.findViewById(R.id.txt_status_pay_money);
        View layoutPaidStatus = convertView.findViewById(R.id.layout_paid_status);
        View layoutContractStatus = convertView.findViewById(R.id.layout_contract_status);
        View layoutBookPort = convertView.findViewById(R.id.layout_book_port);
        View layoutInvest = convertView.findViewById(R.id.layout_invest);
        View layoutCollectStatus = convertView.findViewById(R.id.layout_collection_status);
        txtStatusPaidMoney.setText(registrationList.getPaidStatusName());
        if (registrationList.getPaidStatusName() == null || registrationList.getPaidStatusName().equals("")) {
            layoutPaidStatus.setVisibility(View.GONE);
        } else {
            layoutPaidStatus.setVisibility(View.VISIBLE);
        }
        TextView lblAutoCreateStatusDesc = (TextView) convertView.findViewById(R.id.lbl_auto_create_contract_status_desc);
        if (registrationList.getAutoContractStatus() > 0) {
            lblAutoCreateStatusDesc.setText(registrationList.getAutoContractStatusDesc());
            layoutInvest.setVisibility(View.GONE);
            layoutBookPort.setVisibility(View.GONE);
            layoutCollectStatus.setVisibility(View.GONE);
            layoutContractStatus.setVisibility(View.VISIBLE);
        } else {
            layoutInvest.setVisibility(View.VISIBLE);
            layoutBookPort.setVisibility(View.VISIBLE);
            layoutCollectStatus.setVisibility(View.VISIBLE);
            layoutContractStatus.setVisibility(View.GONE);
        }
        LinearLayout frmContract = (LinearLayout) convertView.findViewById(R.id.frm_contract);
        TextView lblContact = (TextView) convertView.findViewById(R.id.lbl_contract);
        if (registrationList.getContract() != null && !registrationList.getContract().trim().equals("")) {
            lblContact.setText(registrationList.getContract());
            frmContract.setVisibility(View.VISIBLE);
        } else {
            frmContract.setVisibility(View.GONE);
        }
        return convertView;
    }

    public void notifyData(List<ListRegistrationModel> mList){
        this.mList = mList;
        notifyDataSetChanged();
    }
}
