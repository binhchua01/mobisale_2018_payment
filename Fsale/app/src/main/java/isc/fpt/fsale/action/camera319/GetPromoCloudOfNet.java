package isc.fpt.fsale.action.camera319;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CameraType;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.model.camera319.PromoCloud;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_type.CameraOrNetTypeActivity;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.ui.promo_cloud.PromoCloudActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//API lấy danh sách CLKM cloud
public class GetPromoCloudOfNet implements AsyncTaskCompleteListener<String> {

    private Context mContext;

    public GetPromoCloudOfNet(Context mContext,int regtype, int custype, int qty) {
        this.mContext = mContext;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("RegType",regtype);
            jsonObject.put("CusType", custype);
            jsonObject.put("Qty", qty);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.msg_progress_get_promotion_camera_type);
        String GET_PROMOTION_CLOUD = "GetPromoCloudOfNet";
        CallServiceTask service = new CallServiceTask(mContext, GET_PROMOTION_CLOUD, jsonObject,
                Services.JSON_POST_OBJECT, message, GetPromoCloudOfNet.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<PromoCloud> resultObject = new WSObjectsModel<>(jsObj, PromoCloud.class);
                List<PromoCloud> lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (mContext != null) {
                    if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                        Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                    } else {
                        ((PromoCloudActivity) mContext).loadCloudPromotion(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
