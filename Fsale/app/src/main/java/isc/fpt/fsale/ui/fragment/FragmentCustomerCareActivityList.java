package isc.fpt.fsale.ui.fragment;

import java.util.List;

import isc.fpt.fsale.action.GetCustomerCareActivityListByContract;
import isc.fpt.fsale.activity.CustomerCareDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.ActivityListByContractAdapter;
import isc.fpt.fsale.model.ActivityByContractModel;
import isc.fpt.fsale.utils.Common;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.AbsListView.OnScrollListener;

// màn hình Hoạt động CHĂM SÓC KHÁCH HÀNG
public class FragmentCustomerCareActivityList extends Fragment implements OnScrollListener {

    private CustomerCareDetailActivity activity;
    // Phan trang
    private int mCurrentPage = 1, mTotalPage = 1;
    private ListView mListView;
    //private ToDoListAdapter mAdapter;

    /**
     * Returns a new instance of this fragment for the given section number.
     */

    public static FragmentCustomerCareActivityList newInstance(int sectionNumber, String sectionTitle) {
        FragmentCustomerCareActivityList fragment = new FragmentCustomerCareActivityList();
        Bundle args = new Bundle();
        /*args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		args.putString(ARG_SECTION_TITLE, sectionTitle);*/
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentCustomerCareActivityList() {

    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		/*View rootView = inflater.inflate(R.layout.map_create_potential_obj, container,
				false);*/
        View view = inflater.inflate(R.layout.fragment_customer_care_todo_list, container, false);
        //Common.setupUI(getActivity(), view);
        activity = (CustomerCareDetailActivity) getActivity();
        mListView = (ListView) view.findViewById(R.id.lv_object);
        mListView.setOnScrollListener(this);
        return view;
    }


    private void getData(int Page) {
        String Contract = "";
        if (activity.getCustomer() != null) {
            Contract = activity.getCustomer().getContract();
        }
        new GetCustomerCareActivityListByContract(activity, this, Contract, Page);
    }

    public void loadData(List<ActivityByContractModel> lst) {
        if (lst != null && lst.size() > 0) {
            mTotalPage = lst.get(lst.size() - 1).getTotalPage();
            mCurrentPage = lst.get(lst.size() - 1).getCurrentPage();
            if (mListView.getAdapter() == null || mListView.getAdapter().getCount() <= 0) {
                ActivityListByContractAdapter mAdapter = new ActivityListByContractAdapter(activity, lst);
                mListView.setAdapter(mAdapter);
            } else {
                try {
                    ((ActivityListByContractAdapter) mListView.getAdapter()).addAll(lst);
                } catch (Exception e) {

                    Log.i("FragmentCustomerCareActivityList:", e.getMessage());
                }
            }
        } else {
            Common.alertDialog("Không có dữ liệu!", activity);
        }
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        if (mListView.getAdapter() == null)
            getData(1);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private int currentVisibleItemCount;
    private int currentScrollState;
    private int currentFirstVisibleItem;
    private int totalItem;

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // TODO Auto-generated method stub
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItem = totalItemCount;

    }

    private void isScrollCompleted() {
        if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                && this.currentScrollState == SCROLL_STATE_IDLE && currentVisibleItemCount > 0) {
            if (mCurrentPage < mTotalPage) {
                mCurrentPage++;
                getData(mCurrentPage);
            }
        }
    }
}
