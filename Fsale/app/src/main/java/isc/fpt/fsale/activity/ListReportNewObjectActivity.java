package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetReportNewObjectList;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ReportNewObjectListAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportNewObjectModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

public class ListReportNewObjectActivity extends BaseActivity implements OnItemClickListener {

    private Context mContext;
    // Phan trang
    private int mCurrentPage = 1, mTotalPage = 1;
    private int mDay = 0, mYear = 0, mMonth = 0, mAgent = 0, mLocalType = 0;
    private String mAgentName = "";

    private String mSaleAcount = "";
    private ArrayList<ReportNewObjectModel> lstObject = null;
    private Spinner SpPage;
    private Button btnPre, btnNext;
    private ListView mListViewObj;
    //
    private String FromDate, ToDate;

    public ListReportNewObjectActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.menu_report_develop_detail);
        MyApp.setCurrentActivity(this);
        mContext = this;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_report_new_object_activity));
        setContentView(R.layout.list_report_new_object);  
       /* if (DeviceInfo.ANDROID_SDK_VERSION >=11) {
			try{
				android.app.ActionBar actionBar = getActionBar();
				actionBar.setDisplayHomeAsUpEnabled(true);
				actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.main_color_blue));	
				actionBar.setTitle(R.string.menu_report_develop_detail);
			}
			catch(Exception e){
				e.printStackTrace();
			}			
		}*/
        SpPage = (Spinner) this.findViewById(R.id.sp_page_num);
        btnPre = (Button) this.findViewById(R.id.btn_previous);
        btnNext = (Button) this.findViewById(R.id.btn_next);
        mListViewObj = (ListView) findViewById(R.id.lv_object);
        mListViewObj.setOnItemClickListener(this);
        SpPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() != mCurrentPage) {
                    mCurrentPage = selectedItem.getID();
                    getData();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        btnPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentPage > 1) {
                    mCurrentPage--;
                    SpPage.setSelection(mCurrentPage - 1);
                    getData();
                }
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentPage < mTotalPage) {
                    mCurrentPage++;
                    SpPage.setSelection(mCurrentPage - 1);
                    getData();
                }
            }
        });

        Common.hideSoftKeyboard(mContext);

        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                mDay = myIntent.getIntExtra("Day", 0);
                mMonth = myIntent.getIntExtra("Month", 0);
                mYear = myIntent.getIntExtra("Year", 0);
                mSaleAcount = myIntent.getStringExtra("SaleAccount");
                mLocalType = myIntent.getIntExtra("LocalType", 0);
                mAgent = myIntent.getIntExtra("Agent", 0);
                mAgentName = myIntent.getStringExtra("AgentName");
                FromDate = myIntent.getStringExtra("FromDate");
                ToDate = myIntent.getStringExtra("ToDate");
                getData();
                //new GetReportNewObjectList(mContext, mSaleAcount, mYear, mMonth, mLocalTypeName, mCurrentPage);
            }


        } catch (Exception e) {

            Log.d("LOG_GET_EXTRA_CONTRACT_INFO", "Error: " + e.getMessage());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        try {
            ReportNewObjectModel selectedItem = (ReportNewObjectModel) parentView.getItemAtPosition(position);
            if (selectedItem != null) {
                //new GetObjectDetail(this, selectedItem.getContract());
            }
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }

    }

    private void getData() {
        if (!FromDate.equals("") && !ToDate.equals(""))
            new GetReportNewObjectList(mContext, mSaleAcount, FromDate, ToDate, mLocalType, mCurrentPage);
        else if (mMonth > 0 && mYear > 0)
            new GetReportNewObjectList(mContext, mSaleAcount, mDay, mYear, mMonth, mAgent, mAgentName, mLocalType, mCurrentPage);
    }

    public void LoadData(ArrayList<ReportNewObjectModel> list) {
        try {
            lstObject = list;
            if (lstObject != null && lstObject.size() > 0) {
                ReportNewObjectListAdapter adapter = new ReportNewObjectListAdapter(mContext, lstObject);
                ReportNewObjectModel item1 = lstObject.get(0);
                mTotalPage = item1.getTotalPage();
                mListViewObj.setAdapter(adapter);
                if (SpPage.getAdapter() == null) {
                    ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                    for (int i = 1; i <= item1.getTotalPage(); i++) {
                        lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                    }
                    KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPage, Color.WHITE);
                    SpPage.setAdapter(pageAdapter);
                }
            } else {
                new AlertDialog.Builder(mContext).setTitle("Thông Báo").setMessage(getString(R.string.msg_no_data))
                        .setPositiveButton(R.string.lbl_ok, new OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                finish();
                            }
                        })
                        .setCancelable(false).create().show();
            }
        } catch (Exception e) {
            // TODO: handle exception

        }

    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
