package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Check application version
 * @author: 		DuHK
 * @create date: 	30/05/2015
 * */
import isc.fpt.fsale.activity.ListReportSBIDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportSBIDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetReportSBIDetail implements AsyncTaskCompleteListener<String> {

	private final String METHOD_NAME = "ReportSBIDetail";
	private Context mContext;	
	//Add by: DuHK
	public GetReportSBIDetail(Context mContext, String UserName, int Day, int Month, int Year, int Agent, String AgentName, int ReportType, int PageNumber){
		this.mContext = mContext;	
		
		String[] paramNames = {"UserName", "Day", "Month", "Year", "Agent", "AgentName", "ReportType", "PageNumber"};
		String[] paramValues = {UserName, String.valueOf(Day), String.valueOf(Month), String.valueOf(Year), 
				String.valueOf(Agent), String.valueOf(AgentName), String.valueOf(ReportType), String.valueOf(PageNumber)};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportSBIDetail.this);
		service.execute();	
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<ReportSBIDetailModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ReportSBIDetailModel> resultObject = new WSObjectsModel<ReportSBIDetailModel>(jsObj, ReportSBIDetailModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//ServiceType Error
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(List<ReportSBIDetailModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListReportSBIDetailActivity.class.getSimpleName())){
				ListReportSBIDetailActivity activity = (ListReportSBIDetailActivity)mContext;
				activity.LoadData(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("ReportSBIDetail.loadRpCodeInfo()", e.getMessage());
		}
	}
	

}
