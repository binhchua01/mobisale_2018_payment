package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.InvestiageActivity;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

// API upload ảnh lên server
public class UploadImage implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public UploadImage(Context mContext, String[] paramsValue) {
        this.mContext = mContext;
        String[] arrParams;
        if (paramsValue.length == 2)
            arrParams = new String[]{"image", "UserName"};
        else
            arrParams = new String[]{"image", "UserName", "Regcode", "Type"};
        String message = "Đang upload ảnh...";
        String UPLOAD_IMAGE = "UploadImageInvest";
        CallServiceTask service = new CallServiceTask(mContext, UPLOAD_IMAGE,
                arrParams, paramsValue, Services.JSON_POST_UPLOAD, message,
                UploadImage.this);
        service.execute();
    }

    private void HandleUploadImage(String json) {
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if (Common.isEmptyJSONObject(jsObj)) {
                return;
            }
            bindData(jsObj);
        } else {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
        }
    }

    public void bindData(JSONObject jsObj) {
        try {
            String TAG_UPLOAD_IMAGE_RESULT = "UploadImageInvestResult";
            JSONObject obj = jsObj.getJSONObject(TAG_UPLOAD_IMAGE_RESULT);
            String TAG_ERROR = "ErrorService";
            String error = obj.getString(TAG_ERROR);
            if (error.equals("null")) {
                if (obj.length() > 0) {
                    String TAG_RESULT = "Result";
                    String image = obj.getString(TAG_RESULT);
                    if (image != null && !image.trim().equals("")) {
                        if (mContext.getClass().getSimpleName().equals(InvestiageActivity.class.getSimpleName())) {
                            InvestiageActivity activity = (InvestiageActivity) mContext;
                            activity.updateInvestiage(image);
                        }
                    } else {
                        Common.alertDialog("Upload hình không thành công.", mContext);
                    }
                }
            } else {
                Common.alertDialog("Lỗi WS:" + error, mContext);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        HandleUploadImage(result);
    }
}