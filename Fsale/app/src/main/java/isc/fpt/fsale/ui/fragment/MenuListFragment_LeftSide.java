package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListDeploymentProgress;
import isc.fpt.fsale.action.GetListPrechecklist;
import isc.fpt.fsale.action.GetListRegistration;
import isc.fpt.fsale.action.GetSaleCOD;
import isc.fpt.fsale.activity.CareCustomerFPTPlayActivity;
import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.activity.ListActivityCustomerActivity;
import isc.fpt.fsale.activity.ListCustomerCareActivity;
import isc.fpt.fsale.activity.ListPotentialObjActivity;
import isc.fpt.fsale.activity.ListReportCreateObjectTotalActivity;
import isc.fpt.fsale.activity.ListReportPayTVActivity;
import isc.fpt.fsale.activity.ListReportPotentialObjTotalActivity;
import isc.fpt.fsale.activity.ListReportPotentialSurveyActivity;
import isc.fpt.fsale.activity.ListReportPrecheckListActivity;
import isc.fpt.fsale.activity.ListReportRegistrationActivity;
import isc.fpt.fsale.activity.ListReportSBITotalActivity;
import isc.fpt.fsale.activity.ListReportSalaryActivity;
import isc.fpt.fsale.activity.ListReportSurveyManagerTotalActivity;
import isc.fpt.fsale.activity.ListReportSuspendCustomerActivity;
import isc.fpt.fsale.activity.MainActivity;
import isc.fpt.fsale.activity.PromotionAdList;
import isc.fpt.fsale.activity.ReceiveDeviceActivity;
import isc.fpt.fsale.activity.RegistrationListNewActivity;
import isc.fpt.fsale.activity.ReportSubscriberGrowthForManagerActivity;
import isc.fpt.fsale.activity.upsell.UpSellActivity;
import isc.fpt.fsale.ui.choose_service.ChooseServiceActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import static isc.fpt.fsale.utils.Constants.Link_CSKH_PayTv;

/*
 * LIST FRAGMENT:	MenuListFragment
 *
 * @Descripiton: sliding menu fragment
 * @author: vandn, on 24/06/2013
 */
// menu trái chức năng chính
public class MenuListFragment_LeftSide extends ListFragment {
    private Context mContext;
    private FragmentManager fm;
    MenuListItem itemSupportDeployment, itemLogOut, itemHome;

    @SuppressLint("InflateParams")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_menu, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.mContext = getActivity();
        this.fm = getActivity().getSupportFragmentManager();

        ArrayList<MenuListSubItem> subMenuReportAdapter = new ArrayList<>();
        subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_survey));
        subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_subscriber_growth));
        subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_prechecklist));
        subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_salary));
        subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_register));
        subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_sbi));
        subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_potential_obj));
        subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_pay_tv));
        subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_survey_potential_obj));
        //B/C KH roi mang
        subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_suspend_customer));
        if (((MyApp) getActivity().getApplication()).HCM_VERSION)
            subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_create_object));

        ArrayList<MenuListSubItem> subMenuSale = new ArrayList<>();
        subMenuSale.add(new MenuListSubItem(R.string.menu_registration_create));
        subMenuSale.add(new MenuListSubItem(R.string.menu_registration_list));
        subMenuSale.add(new MenuListSubItem(R.string.menu_create_contract));
        if (Constants.IS_SALE_COD) {
            subMenuSale.add(new MenuListSubItem(R.string.menu_sale_cod));
        }
        if (((MyApp) getActivity().getApplication()).HCM_VERSION)
            subMenuSale.add(new MenuListSubItem(R.string.menu_potentail_obj_list));
        subMenuSale.add(new MenuListSubItem(R.string.menu_promotion_ad));
        subMenuSale.add(new MenuListSubItem(R.string.menu_receive_device));
        ArrayList<MenuListSubItem> subMenuCustomerCare = new ArrayList<>();
        subMenuCustomerCare.add(new MenuListSubItem(R.string.menu_prechecklist_list));
        subMenuCustomerCare.add(new MenuListSubItem(R.string.menu_prechecklist_create));
        if (((MyApp) getActivity().getApplication()).HCM_VERSION)
            subMenuCustomerCare.add(new MenuListSubItem(R.string.menu_customer_care));
        if (((MyApp) getActivity().getApplication()).HCM_VERSION)
            subMenuCustomerCare.add(new MenuListSubItem(R.string.menu_customer_activity));
        if (!Link_CSKH_PayTv.equals("")) {
            subMenuCustomerCare.add(new MenuListSubItem(R.string.menu_care_customer_fpt_play_activity));
        }
        subMenuCustomerCare.add(new MenuListSubItem(R.string.menu_create_upsell));
        ArrayList<MenuListSubItem> subMenuSetting = new ArrayList<>();
        subMenuSetting.add(new MenuListSubItem(R.string.menu_reset_password));
        subMenuSetting.add(new MenuListSubItem(R.string.menu_about));

        List<MenuListItem> lst = new ArrayList<>();
        itemHome = new MenuListItem(getString(R.string.menu_main), R.drawable.ic_menu_home);
        lst.add(itemHome);
        lst.add(new MenuListItem(getString(R.string.tile_sale), R.drawable.ic_menu_sale, subMenuSale));
        lst.add(new MenuListItem(getString(R.string.menu_customer_care_group), R.drawable.ic_menu_customer_care, subMenuCustomerCare));
        itemSupportDeployment = new MenuListItem(getString(R.string.menu_support_develop), R.drawable.ic_menu_support_deployment);
        lst.add(itemSupportDeployment);
        lst.add(new MenuListItem(getString(R.string.menu_report_main), R.drawable.ic_menu_report_develop, subMenuReportAdapter));
        lst.add(new MenuListItem(getString(R.string.menu_setting_group), R.drawable.ic_menu_setting, subMenuSetting));

        itemLogOut = new MenuListItem(getString(R.string.menu_logout), R.drawable.ic_menu_exit);
        lst.add(itemLogOut);

        MenuListAdapter adapter = new MenuListAdapter(this.mContext, lst);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView lv, View v, int position, long id) {
        Constants.SLIDING_MENU.toggle();
        MenuListItem item = (MenuListItem) lv.getAdapter().getItem(position);
        if (item == itemLogOut) {
            Common.Logout(mContext);
        } else if (item == itemSupportDeployment) {
            new GetListDeploymentProgress(mContext, Constants.USERNAME, "0", "0", "1", false);
        } else if (item == itemHome) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            getActivity().startActivity(intent);
        }
    }

    /**
     * MODEL: MenuListItem
     * ============================================================
     */
    public class MenuListItem {
        public String title;
        private int iconRes;
        private ArrayList<MenuListSubItem> subMenuList;

        private MenuListItem(String tag, int iconRes) {
            this.title = tag;
            this.iconRes = iconRes;
        }

        private MenuListItem(String tag, int iconRes, ArrayList<MenuListSubItem> subs) {
            this.title = tag;
            this.iconRes = iconRes;
            this.subMenuList = subs;
        }
    }

    /*
     * SubMenu
     * */
    private class MenuListSubItem {
        private int stringID;

        private MenuListSubItem(int stringID) {
            this.stringID = stringID;
        }
    }

    /**
     * ADAPTER: MenuListAdapter
     * ==============================================================
     */
    @SuppressLint("InflateParams")
    public class MenuListAdapter extends BaseAdapter {
        List<MenuListItem> mList;
        Context mContext;

        private MenuListAdapter(Context context, List<MenuListItem> lst) {
            this.mList = lst;
            this.mContext = context;
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public MenuListItem getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("InflateParams")
        public View getView(int position, View convertView, ViewGroup parent) {
            MenuListItem item = mList.get(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(this.mContext).inflate(R.layout.list_row_menu_multi_level, null);
            }
            TextView title = (TextView) convertView.findViewById(R.id.item_title);

            final LinearLayout frmSubMenu = (LinearLayout) convertView.findViewById(R.id.frm_sub_menu);
            final ImageView imgNavigater = (ImageView) convertView.findViewById(R.id.img_navigation_sub_menu);
            title.setText(getItem(position).title);
            try {
                imgNavigater.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        dropDownNavigation(imgNavigater, frmSubMenu);

                    }
                });

                if (item.subMenuList == null) {
                    imgNavigater.setVisibility(View.GONE);
                    frmSubMenu.setVisibility(View.GONE);
                } else {
                    convertView.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            dropDownNavigation(imgNavigater, frmSubMenu);
                        }
                    });

                    if (frmSubMenu.getChildCount() > 0) {
                        frmSubMenu.removeAllViews();
                    }
                    if (item.title.equals(getString(R.string.menu_report_main))) {
                        addViewSupMenuReport(frmSubMenu, item.subMenuList);
                    } else if (item.title.equals(getString(R.string.tile_sale))) {
                        addViewSupMenuSale(frmSubMenu, item.subMenuList);
                    } else if (item.title.equals(getString(R.string.menu_customer_care_group))) {
                        addViewSupMenuCustomerCare(frmSubMenu, item.subMenuList);
                    } else if (item.title.equals(getString(R.string.menu_setting_group))) {
                        addViewSupMenuSetting(frmSubMenu, item.subMenuList);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            ////////////////////////////////////////////////////////////////////////////
            ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
            icon.setImageResource(getItem(position).iconRes);

            //check if is current menu v,iew
            if (position == Constants.CURRENT_MENU) {
                title.setTextColor(getResources().getColor(R.color.main_color_light));
//                highlightMenuIcon(position, icon);
            } else {
                title.setTextColor(getResources().getColor(R.color.text_color_light));
            }

            title.setTypeface(null, Typeface.BOLD);
            title.setTextSize(15);
            title.setTextColor(getResources().getColor(R.color.text_header_slide));
            title.setPadding(0, 10, 0, 10);
            return convertView;
        }

        private void dropDownNavigation(ImageView btnNavigater, LinearLayout frmSubMenu) {
            if (frmSubMenu.getVisibility() == View.VISIBLE) {
                frmSubMenu.setVisibility(View.GONE);
                btnNavigater.setImageResource(R.drawable.ic_navigation_drop_up_menu);
            } else {
                frmSubMenu.setVisibility(View.VISIBLE);
                btnNavigater.setImageResource(R.drawable.ic_navigation_drop_down_menu);
            }
        }

        private void addViewSupMenuReport(LinearLayout frmSubMenu, ArrayList<MenuListSubItem> subMenuList) {
            for (int i = 0; i < subMenuList.size(); i++) {
                final MenuListSubItem subItem = subMenuList.get(i);
                View viewChild = LayoutInflater.from(mContext).inflate(R.layout.list_row_menu_sub, null);
                viewChild.setClickable(true);
                TextView lblTitle = (TextView) viewChild.findViewById(R.id.item_title);
                ImageView imgIcon = (ImageView) viewChild.findViewById(R.id.item_icon);
                imgIcon.setVisibility(View.GONE);
                if (subItem.stringID > 0)
                    lblTitle.setText(getString(subItem.stringID));
                viewChild.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        Constants.SLIDING_MENU.toggle();
                        if (subItem.stringID == R.string.menu_report_survey) {
                            Intent intent = new Intent(getActivity(), ListReportSurveyManagerTotalActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            getActivity().startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_report_subscriber_growth) {
                            Intent intent = new Intent(getActivity(), ReportSubscriberGrowthForManagerActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            getActivity().startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_report_prechecklist) {
                            Intent intent = new Intent(getActivity(), ListReportPrecheckListActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_report_register) {
                            Intent intent = new Intent(getActivity(), ListReportRegistrationActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            getActivity().startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_report_sbi) {
                            Intent intent = new Intent(getActivity(), ListReportSBITotalActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            getActivity().startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_report_salary) {
                            Intent intent = new Intent(getActivity(), ListReportSalaryActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            getActivity().startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_report_potential_obj) {
                            Intent intent = new Intent(mContext, ListReportPotentialObjTotalActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_report_create_object) {
                            Intent intent = new Intent(mContext, ListReportCreateObjectTotalActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_report_pay_tv) {
                            Intent intent = new Intent(mContext, ListReportPayTVActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_report_survey_potential_obj) {
                            Intent intent = new Intent(mContext, ListReportPotentialSurveyActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_report_suspend_customer) {
                            Intent intent = new Intent(mContext, ListReportSuspendCustomerActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                        }
                    }
                });
                frmSubMenu.addView(viewChild);
            }
        }

        //Add SubMenu Bán hàng
        private void addViewSupMenuSale(LinearLayout frmSubMenu, ArrayList<MenuListSubItem> subMenuList) {
            for (int i = 0; i < subMenuList.size(); i++) {
                final MenuListSubItem subItem = subMenuList.get(i);
                View viewChild = LayoutInflater.from(mContext).inflate(R.layout.list_row_menu_sub, null);
                viewChild.setClickable(true);
                TextView lblTitle = (TextView) viewChild.findViewById(R.id.item_title);
                ImageView imgIcon = (ImageView) viewChild.findViewById(R.id.item_icon);
                imgIcon.setVisibility(View.GONE);
                if (subItem.stringID > 0)
                    lblTitle.setText(getString(subItem.stringID));
                viewChild.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        Constants.SLIDING_MENU.toggle();
                        if (subItem.stringID == R.string.menu_registration_list) {
                            gotoRegistrationList();
                        } else if (subItem.stringID == R.string.menu_registration_create) {
                            gotoRegistration();
                        } else if (subItem.stringID == R.string.menu_potentail_obj_list) {
                            Intent intent = new Intent(mContext, ListPotentialObjActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mContext.startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_create_contract) {
                            new GetListRegistration(mContext, Constants.USERNAME, 1, 1, "", false);
                        } else if (subItem.stringID == R.string.menu_sale_cod) {
                            new GetSaleCOD(mContext, Constants.USERNAME);
                        } else if (subItem.stringID == R.string.menu_promotion_ad) {
                            Intent intent = new Intent(mContext, PromotionAdList.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mContext.startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_receive_device) {
                            Intent intent = new Intent(mContext, ReceiveDeviceActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mContext.startActivity(intent);
                        }
                    }
                });
                frmSubMenu.addView(viewChild);
            }
        }

        //Add SubMenu CSKH
        private void addViewSupMenuCustomerCare(LinearLayout frmSubMenu, ArrayList<MenuListSubItem> subMenuList) {
            for (int i = 0; i < subMenuList.size(); i++) {
                final MenuListSubItem subItem = subMenuList.get(i);
                View viewChild = LayoutInflater.from(mContext).inflate(R.layout.list_row_menu_sub, null);
                viewChild.setClickable(true);
                TextView lblTitle = (TextView) viewChild.findViewById(R.id.item_title);
                ImageView imgIcon = (ImageView) viewChild.findViewById(R.id.item_icon);
                imgIcon.setVisibility(View.GONE);
                if (subItem.stringID > 0)
                    lblTitle.setText(getString(subItem.stringID));
                viewChild.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Constants.SLIDING_MENU.toggle();
                        if (subItem.stringID == R.string.menu_prechecklist_list) {
                            gotoListPrechecklist();
                        } else if (subItem.stringID == R.string.menu_prechecklist_create) {
                            gotoPrechecklist();
                        } else if (subItem.stringID == R.string.menu_customer_care) {
                            Intent intent = new Intent(mContext, ListCustomerCareActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mContext.startActivity(intent);
                        } else if (subItem.stringID == R.string.menu_customer_activity) {
                            Intent intent = new Intent(mContext, ListActivityCustomerActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mContext.startActivity(intent);
                        }
                        // sự kiên menu chăm sóc khách hàng paytv
                        else if (subItem.stringID == R.string.menu_care_customer_fpt_play_activity) {
                            Intent intent = new Intent(mContext, CareCustomerFPTPlayActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("LINK_CS_KH_PAY_TV", Constants.Link_CSKH_PayTv);
                            mContext.startActivity(intent);
                        }
                        //Upsell 27/05/2020
                        else if (subItem.stringID == R.string.menu_create_upsell) {
                            Intent intent = new Intent(mContext, UpSellActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            //intent.putExtra("LINK_CS_KH_PAY_TV", Constants.Link_CSKH_PayTv);
                            mContext.startActivity(intent);
                        }
                    }
                });
                frmSubMenu.addView(viewChild);
            }
        }

        //Add SubMenu Cài đặt
        private void addViewSupMenuSetting(LinearLayout frmSubMenu, ArrayList<MenuListSubItem> subMenuList) {
            for (int i = 0; i < subMenuList.size(); i++) {
                final MenuListSubItem subItem = subMenuList.get(i);
                View viewChild = LayoutInflater.from(mContext).inflate(R.layout.list_row_menu_sub, null);
                viewChild.setClickable(true);

                TextView lblTitle = (TextView) viewChild.findViewById(R.id.item_title);
                ImageView imgIcon = (ImageView) viewChild.findViewById(R.id.item_icon);
                imgIcon.setVisibility(View.GONE);
                if (subItem.stringID > 0)
                    lblTitle.setText(getString(subItem.stringID));
                viewChild.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        Constants.SLIDING_MENU.toggle();
                        if (subItem.stringID == R.string.menu_reset_password) {
                            ResetPasswordDialog resetpassDialog = new ResetPasswordDialog(mContext);
                            Common.showFragmentDialog(fm, resetpassDialog, "fragment_reset_password_dialog");
                        } else if (subItem.stringID == R.string.menu_about) {
                            AboutUsDialog aboutDialog = new AboutUsDialog(mContext);
                            Common.showFragmentDialog(fm, aboutDialog, "fragment_about_us_dialog");
                        }
                    }
                });
                frmSubMenu.addView(viewChild);
            }
        }
    }

    public void gotoRegistration() {
        Intent intent = new Intent(mContext, ChooseServiceActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(intent);
    }

    public void gotoRegistrationList() {
        Intent intent = new Intent(mContext, RegistrationListNewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        mContext.startActivity(intent);
    }

    public void gotoPrechecklist() {
        Intent intent = new Intent(mContext, CreatePreChecklistActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        mContext.startActivity(intent);

    }

    public void gotoListPrechecklist() {
        // add by GiauTQ 30-04-2014
        String[] arrayParrams = new String[]{Constants.USERNAME, "1"};
        new GetListPrechecklist(mContext, arrayParrams);
    }
}
