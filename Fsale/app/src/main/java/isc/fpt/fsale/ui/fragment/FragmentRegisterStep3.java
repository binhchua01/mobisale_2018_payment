package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetFPTBoxList;
import isc.fpt.fsale.action.GetIPTVPrice;
import isc.fpt.fsale.action.GetPromotionIPTVList;
import isc.fpt.fsale.action.GetPromotionList;
import isc.fpt.fsale.activity.choose_mac_fpt_box.ChooseMacActivity;
import isc.fpt.fsale.activity.device_list.DeviceListActivity;
import isc.fpt.fsale.activity.device_price_list.DevicePriceListActivity;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.LocalType;
import isc.fpt.fsale.activity.BarcodeScanActivity;
import isc.fpt.fsale.activity.PromotionFilterActivity;
import isc.fpt.fsale.activity.PromotionIPTVFilterActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.ShowImageActivity;
import isc.fpt.fsale.adapter.DeviceAdapter;
import isc.fpt.fsale.adapter.FPTBoxAdapter;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.MacAdapter;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.IPTVPriceModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.MacOTT;
import isc.fpt.fsale.model.PackageModel;
import isc.fpt.fsale.model.PromotionFPTBoxModel;
import isc.fpt.fsale.model.PromotionIPTVModel;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.activity.local_type.LocalTypeActivity;
import isc.fpt.fsale.activity.service_type.ServiceTypeActivity;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV3;
import isc.fpt.fsale.ui.fpt_camera.FptCameraActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class FragmentRegisterStep3 extends BaseFragment implements
        OnCheckedChangeListener, OnItemClickListenerV3<Integer, Device, Integer> {

    public RegistrationDetailModel modelDetail = null;
    public PromotionModel selectedInternetPromotion;
    public RegistrationDetailModel mRegister;
    public PromotionIPTVModel promotionIPTVModelBox1, promotionIPTVModelBox2;

    private MacAdapter macAdapter;
    private FPTBoxAdapter fptBoxAdapter;
    public List<FPTBox> listFptBox, listFptBoxSelect;

    public List<Device> mListDeviceSelected;
    private DeviceAdapter mDeviceAdapter;

    private List<CharSequence> listNameFptBox;
    private List<MacOTT> macList;
    public ArrayList<PromotionModel> mListPromotion;
    public ArrayList<PromotionIPTVModel> mListPromotionIPTVBox1;
    public ArrayList<PromotionIPTVModel> mListPromotionIPTVBox2;
    public List<CategoryServiceList> mListServiceSelected;

    private HashMap<Integer, ArrayList<PromotionFPTBoxModel>> listPromotionFptBox;
    @SuppressLint("UseSparseArrays")
    public Map<Integer, PackageModel> mDicPackage = new HashMap<>();
    public boolean imgIPTVTotal = false, spIPTVEquipmentDelivery = false, frmIPTVMonthlyTotal,
            is_have_fpt_box, is_have_device, is_have_ip_tv, is_clear_rp_and_voucher;
    public int serviceType;
    public int localTypeId;
    public int customerType = 1;
    public static int isBind = 0;
    public String lblInternetTotal = "0";
    public String lblIPTVTotal = "0";
    public String lblTotal = "0";
    public String errorStr;
    public String contractParam;
    public String regCodeParam;

    private final int CODE_LOCAL_TYPE = 100;
    private final int CODE_SERVICE_TYPE = 101;
    private final int CODE_DEVICE_LIST_SELECTED = 102;
    private final int CODE_DEVICE_PRICE_LIST_SELECTED = 103;
    private final int CODE_CHOOSE_MAC = 104;

    public RadioButton radIpTvSetUpYes, radIpTvSetUpNo, radIPTVDrillWallYes, radIPTVDrillWallNo;
    public EditText txtPromotion;
    public EditText txtPromotionBox1;
    public EditText txtPromotionBox2;
    public EditText txtIPTVPrepaid;
    public ImageView imgIPTVBoxPlus, imgIPTVPLCLess, imgIPTVPLCPlus, imgIPTVReturnSTBLess,
            imgIPTVReturnSTBPlus, imgIPTVChargeTimesPlus, imgIPTVExtraNavigator, imgIPTVHBOChargeTimeLess,
            imgIPTVHBOChargeTimePlus, imgIPTVHBOPrepaidMonthLess, imgIPTVHBOPrepaidMonthPlus, imgIPTVBoxLess,
            imgPromHasImg, imgPromHasVideo, imgIPTVFimPlus_ChargeTimeLess, imgIPTVFimPlus_ChargeTimePlus,
            imgIPTVFimPlus_PrepaidMonthLess, imgIPTVFimPlus_PrepaidMonthPlus, imgIPTVFimHot_ChargeTimeLess,
            imgIPTVFimHot_ChargeTimePlus, imgIPTVFimHot_PrepaidMonthLess, imgIPTVKPlusChargeTimeLess,
            imgIPTVKPlusChargeTimePlus, imgIPTVKPlusPrepaidMonthLess, imgIPTVKPlusPrepaidMonthPlus,
            imgIPTVFimHot_PrepaidMonthPlus, imgIPTVVTVChargeTimeLess, imgIPTVVTVChargeTimePlus,
            imgIPTVVTVPrepaidMonthLess, imgIPTVVTVPrepaidMonthPlus,
            imgIPTVVTCChargeTimeLess, imgIPTVVTCChargeTimePlus, imgIPTVVTCPrepaidMonthLess,
            imgIPTVVTCPrepaidMonthPlus, imgIPTVFimStandard_ChargeTimeLess, imgIPTVFimStandard_ChargeTimePlus,
            imgIPTVFimStandard_MonthCountLess, imgIPTVFimStandard_MonthCountPlus,
            imgFoxy2ChargeTimeLess, imgFoxy2ChargeTimePlus, imgFoxy2PrepaidMonthLess, imgFoxy2PrepaidMonthPlus,
            imgFoxy4ChargeTimeLess, imgFoxy4ChargeTimePlus, imgFoxy4PrepaidMonthLess, imgFoxy4PrepaidMonthPlus;
    public Spinner spIPTVPackage, spIPTVCombo, spIPTVFormDeployment;
    public TextView lblIPTVPromotionBoxFirstAmount, lblIPTVPromotionBoxOrderAmount, lblIPTVReturnSTBCount,
            lblIPTVChargeTimesCount, txtIPTVBoxCount, lblIPTVPLCCount, lblPromotionInternetDetail,
            lblIPTVHBOChargeTimesCount, lblIPTVHBOPrepaidMonthCount, lblIPTVFimPlusAmount,
            lblIPTVFimPlus_ChargeTime, lblIPTVFimPlus_MonthCount, lblIPTVFimHotAmount, lblIPTVFimHot_ChargeTime,
            lblIPTVKPlusAmount, lblIPTVKPlusChargeTimesCount, lblIPTVKPlusPrepaidMonthCount, lblIPTVVTVAmount,
            lblIPTVVTVChargeTimesCount, lblIPTVVTVPrepaidMonthCount, lblIPTVFimStandard_ChargeTimes,
            lblIPTVFimStandard_Amount, lblIPTVFimStandard_MonthCount, lblIPTVVTCChargeTimesCount,
            lblIPTVVTCPrepaidMonthCount, lblIPTVVTCAmount, lblIPTVFimHot_MonthCount, tvLocalType, tvServiceType,
            tvFoxy2ChargeTime, tvFoxy2PrepaidMonth, tvFoxy4ChargeTime, tvFoxy4PrepaidMonth, tvFoxy2Amount, tvFoxy4Amount;
    public CheckBox chbIpTvHbo, chbIPTVFimPlus, chbIpTvFimHot, chbIPTVKPlus,
            chbIpTvVtc, chbIpTvVtv, chbIPTVFimStandard, cbFoxy2, cbFoxy4;
    public LinearLayout frmIPTVFimPlus, frmIpTvFimHot, frmInternet, frmIpTv, frmIPTVExtraNavigation,
            frmIPTVExtra, frmIPTVKPlus, frmIpTvVtc, frmIPTVFimStandard, frmIpTvVtv, frmListFptBox, frmDacSacHD;
    public ScrollView frmMain;
    public Button btnShowKeyWord, btnChooseMac, btnSelectDevice, btnSelectFptBox;
    public View frmDevicesBanner, frmContentFptBox;

    public static FragmentRegisterStep3 newInstance(RegistrationDetailModel modelDetail) {
        return new FragmentRegisterStep3(modelDetail);
    }

    @SuppressLint("ValidFragment")
    public FragmentRegisterStep3(RegistrationDetailModel modelDetail) {
        super();
        this.modelDetail = modelDetail;
    }

    public FragmentRegisterStep3() {
    }

    @Override
    protected void bindData() {
        //bind old data
        List<CategoryServiceList> mListService = new ArrayList<>();
        if (modelDetail != null && modelDetail.getCategoryServiceList() != null &&
                modelDetail.getCategoryServiceList().size() != 0) {
            mListService = modelDetail.getCategoryServiceList();
        }
        if (((RegisterActivityNew) getActivity()).getListService() != null &&
                ((RegisterActivityNew) getActivity()).getListService().size() != 0) {
            mListService = ((RegisterActivityNew) getActivity()).getListService();
        }
        if (modelDetail != null) {
            if (mListService.size() != 0) {//chỉ gọi khi nào bán Internet
                if (modelDetail.getLocalType() != 0) {//cập nhật PDK
                    StringBuilder service = new StringBuilder();
                    for (int i = 0; i < mListService.size(); i++) {
                        if ((mListService.size() - 1) == i) {
                            service.append(mListService.get(i).getCategoryServiceName());
                        } else {
                            service.append(mListService.get(i).getCategoryServiceName()).append(", ");
                        }
                    }
                    //set service type name to view
                    tvServiceType.setText(service.toString());
                    txtPromotion.setText(modelDetail.getPromotionName());
                    if (selectedInternetPromotion == null) {
                        selectedInternetPromotion = new PromotionModel();
                    }
                    selectedInternetPromotion.setPromotionID(modelDetail.getPromotionID());
                    selectedInternetPromotion.setPromotionName(modelDetail.getPromotionName());
                    mListServiceSelected = mListService;
                    serviceType = 1;
                    tvLocalType.setText(modelDetail.getLocalTypeName());
                    localTypeId = modelDetail.getLocalType();
                    new GetPromotionList(getActivity(), this, Constants.LOCATION_ID, localTypeId, serviceType);
                } else {
                    if (mListService.size() == 1) {
                        tvServiceType.setText(mListService.get(0).getCategoryServiceName());
                        serviceType = mListService.get(0).getCategoryServiceID();
                    }
                }
            }
        } else {
            if (mListService.size() == 1) {
                tvServiceType.setText(mListService.get(0).getCategoryServiceName());
                serviceType = mListService.get(0).getCategoryServiceID();
                mListServiceSelected = mListService;
            }
        }

        initViewByServiceType(mListService);
        initDataDevice();
        initDataFptBox();
        initIPTVCombo();
        initIPTVFormDeployment();
        initIPTVAllPackages();
        initContractRegCode();
        initControlElse();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_register_step3;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case CODE_SERVICE_TYPE:
                    List<CategoryServiceList> mListService = data.getParcelableArrayListExtra(Constants.SERVICE_TYPE_LIST);
                    mListServiceSelected.clear();
                    mListServiceSelected = mListService;
                    StringBuilder service = new StringBuilder();
                    for (int i = 0; i < mListServiceSelected.size(); i++) {
                        if ((mListServiceSelected.size() - 1) == i) {
                            service.append(mListServiceSelected.get(i).getCategoryServiceName());
                        } else {
                            service.append(mListServiceSelected.get(i).getCategoryServiceName()).append(", ");
                        }
                    }
                    //set service type name to view
                    tvServiceType.setText(service.toString());
                    initViewByServiceType(mListServiceSelected);

                    //clear thông tin RP & E-Voucher
                    is_clear_rp_and_voucher = true;
                    break;
                case CODE_LOCAL_TYPE:
                    LocalType modelLocalType = data.getParcelableExtra(Constants.LOCAL_TYPE);
                    if (modelLocalType == null) return;
                    tvLocalType.setText(modelLocalType.getLocalTypeName());
                    localTypeId = modelLocalType.getLocalTypeID();
                    if (this.mListServiceSelected.size() != 0) {//chỉ gọi khi nào bán Internet
                        new GetPromotionList(getActivity(), this,
                                Constants.LOCATION_ID, localTypeId, 1/*Id của laoji dịch vụ internet*/);
                        serviceType = 1;
                    }

                    //clear thông tin RP & E-Voucher
                    is_clear_rp_and_voucher = true;
                    break;
                case CODE_DEVICE_LIST_SELECTED:
                    Device mDevice = data.getParcelableExtra(Constants.DEVICE);
                    mListDeviceSelected.add(mDevice);
                    mDeviceAdapter.notifyData(mListDeviceSelected);
                    break;
                case CODE_DEVICE_PRICE_LIST_SELECTED:
                    Device mDeviceSelected = data.getParcelableExtra(Constants.DEVICE);
                    if (mDeviceSelected == null) return;
                    int position = data.getIntExtra(Constants.POSITION, 0);

                    if (mListDeviceSelected.size() > 1) {
                        for (Device item : mListDeviceSelected) {
                            if (item.getDeviceID() == mDeviceSelected.getDeviceID() &&
                                    item.getPriceID() == mDeviceSelected.getPriceID()) {
                                Common.alertDialog(getString(R.string.message_double_price), this.getActivity());
                                return;
                            }
                        }
                    }

                    mListDeviceSelected.set(position, mDeviceSelected);
                    mDeviceAdapter.notifyData(mListDeviceSelected);
                    break;
                case CODE_CHOOSE_MAC:
                    macList = data.getParcelableArrayListExtra(Constants.LIST_MAC_SELECTED);
                    macAdapter.notifyData(macList);
                    break;
            }
        }
    }

    @SuppressLint("UseSparseArrays")
    @Override
    protected void initView(View view) {
        frmMain = view.findViewById(R.id.frm_main);
        tvLocalType = view.findViewById(R.id.tv_local_type);
        tvServiceType = view.findViewById(R.id.tv_service_type);
        lblPromotionInternetDetail = view.findViewById(R.id.lbl_promotion_internet_detail);
        txtPromotion = view.findViewById(R.id.txt_promotion);
        txtPromotion.setFocusable(false);
        txtPromotion.setInputType(InputType.TYPE_NULL);
        txtPromotionBox1 = view.findViewById(R.id.txt_promotion_box1);
        txtPromotionBox2 = view.findViewById(R.id.txt_promotion_box2);
        frmInternet = view.findViewById(R.id.frm_internet);
        frmIpTv = view.findViewById(R.id.frm_ip_tv);
        frmIpTv.setVisibility(View.GONE);
        radIpTvSetUpYes = view.findViewById(R.id.rad_iptv_request_setup_yes);
        radIpTvSetUpNo = view.findViewById(R.id.rad_ip_tv_request_setup_no);
        radIPTVDrillWallYes = view.findViewById(R.id.rad_ip_tv_request_drill_the_wall_yes);
        radIPTVDrillWallNo = view.findViewById(R.id.rad_ip_tv_request_drill_the_wall_no);
        spIPTVCombo = view.findViewById(R.id.sp_iptv_combo);
        spIPTVFormDeployment = view.findViewById(R.id.sp_iptv_form_deployment);
        imgIPTVBoxLess = view.findViewById(R.id.img_btn_iptv_box_less);
        txtIPTVBoxCount = view.findViewById(R.id.txt_iptv_box_count);
        imgIPTVBoxPlus = view.findViewById(R.id.img_btn_iptv_box_plus);
        imgIPTVPLCLess = view.findViewById(R.id.img_btn_iptv_plc_less);
        lblIPTVPLCCount = view.findViewById(R.id.txt_iptv_plc_count);
        imgIPTVPLCPlus = view.findViewById(R.id.img_btn_iptv_plc_plus);
        imgIPTVReturnSTBLess = view.findViewById(R.id.img_btn_iptv_return_stb_less);
        lblIPTVReturnSTBCount = view.findViewById(R.id.txt_iptv_return_stb_count);
        imgIPTVReturnSTBPlus = view.findViewById(R.id.img_btn_iptv_return_stb_plus);
        spIPTVPackage = view.findViewById(R.id.sp_ip_tv_package);
        lblIPTVPromotionBoxFirstAmount = view.findViewById(R.id.lbl_iptv_promotion_box_first_amount);
        lblIPTVPromotionBoxOrderAmount = view.findViewById(R.id.lbl_iptv_promotion_box_order_amount);
        lblIPTVChargeTimesCount = view.findViewById(R.id.txt_iptv_charge_times);
        imgIPTVChargeTimesPlus = view.findViewById(R.id.img_btn_iptv_charge_times_plus);
        txtIPTVPrepaid = view.findViewById(R.id.txt_iptv_prepaid);
        frmIPTVExtra = view.findViewById(R.id.frm_iptv_extra);
        frmIPTVExtraNavigation = view.findViewById(R.id.frm_iptv_extra_navigation);
        imgIPTVExtraNavigator = view.findViewById(R.id.img_iptv_extra_navigation_drop_down);
        chbIpTvHbo = view.findViewById(R.id.chb_iptv_hbo);
        chbIPTVFimPlus = view.findViewById(R.id.chb_iptv_fim_plus);
        chbIpTvFimHot = view.findViewById(R.id.chb_iptv_fim_hot);
        lblIPTVFimPlus_ChargeTime = view.findViewById(R.id.lbl_iptv_fim_plus_charge_times);
        lblIPTVFimHot_ChargeTime = view.findViewById(R.id.lbl_iptv_fim_hot_charge_times);
        chbIPTVKPlus = view.findViewById(R.id.chb_iptv_k_plus);
        lblIPTVKPlusChargeTimesCount = view.findViewById(R.id.txt_iptv_k_plus_charge_times);
        chbIpTvVtc = view.findViewById(R.id.chb_iptv_vtc);
        lblIPTVVTCChargeTimesCount = view.findViewById(R.id.txt_iptv_vtc_charge_times);
        lblIPTVVTCPrepaidMonthCount = view.findViewById(R.id.txt_iptv_vtc_month_count);
        chbIpTvVtv = view.findViewById(R.id.chb_iptv_vtv_cab);
        lblIPTVVTVChargeTimesCount = view.findViewById(R.id.txt_iptv_vtv_cab_charge_times);
        btnShowKeyWord = view.findViewById(R.id.btn_show_key_word);
        imgPromHasImg = view.findViewById(R.id.img_prom_has_image);
        imgPromHasVideo = view.findViewById(R.id.img_prom_has_video);
        imgIPTVHBOChargeTimeLess = view.findViewById(R.id.img_btn_iptv_hbo_charge_times_less);
        lblIPTVHBOChargeTimesCount = view.findViewById(R.id.txt_ip_tv_hbo_charge_times);
        imgIPTVHBOChargeTimePlus = view.findViewById(R.id.img_btn_iptv_hbo_charge_times_plus);
        imgIPTVHBOPrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_hbo_month_count_less);
        imgIPTVHBOPrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_hbo_month_plus);
        lblIPTVHBOPrepaidMonthCount = view.findViewById(R.id.txt_iptv_hbo_month_count);
        frmIPTVFimPlus = view.findViewById(R.id.frm_iptv_fim_plus);
        frmIpTvFimHot = view.findViewById(R.id.frm_iptv_fim_hot);
        lblIPTVFimPlusAmount = view.findViewById(R.id.lbl_iptv_fim_plus_amount);
        imgIPTVFimPlus_ChargeTimeLess = view.findViewById(R.id.img_iptv_fim_plus_charge_time_less);
        imgIPTVFimPlus_ChargeTimePlus = view.findViewById(R.id.img_iptv_fim_plus_charge_time_plus);
        imgIPTVFimPlus_PrepaidMonthLess = view.findViewById(R.id.img_iptv_fim_plus_month_count_less);
        imgIPTVFimPlus_PrepaidMonthPlus = view.findViewById(R.id.img_iptv_fim_plus_month_count_plus);
        lblIPTVFimPlus_MonthCount = view.findViewById(R.id.lbl_iptv_fim_plus_month_count);
        lblIPTVFimHotAmount = view.findViewById(R.id.lbl_iptv_fim_hot_amount);
        imgIPTVFimHot_ChargeTimeLess = view.findViewById(R.id.img_iptv_fim_hot_charge_time_less);
        imgIPTVFimHot_ChargeTimePlus = view.findViewById(R.id.img_iptv_fim_hot_charge_time_plus);
        imgIPTVFimHot_PrepaidMonthLess = view.findViewById(R.id.img_iptv_fim_hot_month_count_less);
        lblIPTVFimHot_MonthCount = view.findViewById(R.id.lbl_iptv_fim_hot_month_count);
        imgIPTVFimHot_PrepaidMonthPlus = view.findViewById(R.id.img_iptv_fim_hot_month_count_plus);
        frmIPTVKPlus = view.findViewById(R.id.frm_iptv_k_plus);
        lblIPTVKPlusAmount = view.findViewById(R.id.lbl_iptv_k_plus_amount);
        lblIPTVKPlusPrepaidMonthCount = view.findViewById(R.id.txt_iptv_k_plus_month_count);
        imgIPTVKPlusChargeTimeLess = view.findViewById(R.id.img_btn_iptv_k_plus_charge_times_less);
        imgIPTVKPlusChargeTimePlus = view.findViewById(R.id.img_btn_iptv_k_plus_charge_times_plus);
        imgIPTVKPlusPrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_k_plus_month_count_pless);
        imgIPTVKPlusPrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_k_plus_month_plus);
        frmIpTvVtc = view.findViewById(R.id.frm_iptv_vtc_hd);
        lblIPTVVTCAmount = view.findViewById(R.id.lbl_iptv_vtc_amount);
        imgIPTVVTCChargeTimeLess = view.findViewById(R.id.img_btn_iptv_vtc_charge_times_less);
        imgIPTVVTCChargeTimePlus = view.findViewById(R.id.img_btn_iptv_vtc_charge_times_plus);
        imgIPTVVTCPrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_vtc_month_count_pless);
        imgIPTVVTCPrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_vtc_month_plus);
        frmIpTvVtv = view.findViewById(R.id.frm_iptv_vtv_cap);
        lblIPTVVTVAmount = view.findViewById(R.id.lbl_iptv_vtv_cab_amount);
        lblIPTVVTVPrepaidMonthCount = view.findViewById(R.id.txt_iptv_vtv_cab_month_count);
        imgIPTVVTVChargeTimeLess = view.findViewById(R.id.img_btn_iptv_vtv_cab_charge_times_less);
        imgIPTVVTVChargeTimePlus = view.findViewById(R.id.img_btn_iptv_vtv_cab_charge_times_plus);
        imgIPTVVTVPrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_vtv_cab_month_count_pless);
        imgIPTVVTVPrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_vtv_cab_month_plus);
        frmIPTVFimStandard = view.findViewById(R.id.frm_ip_tv_fim_standard);
        chbIPTVFimStandard = view.findViewById(R.id.chb_ip_tv_fim_standard);
        lblIPTVFimStandard_ChargeTimes = view.findViewById(R.id.lbl_ip_tv_fim_standard_charge_times);
        lblIPTVFimStandard_Amount = view.findViewById(R.id.lbl_ip_tv_fim_standard_amount);
        lblIPTVFimStandard_MonthCount = view.findViewById(R.id.lbl_ip_tv_fim_standard_month_count);
        imgIPTVFimStandard_ChargeTimeLess = view.findViewById(R.id.img_ip_tv_fim_standard_charge_time_less);
        imgIPTVFimStandard_ChargeTimePlus = view.findViewById(R.id.img_ip_tv_fim_standard_charge_time_plus);
        imgIPTVFimStandard_MonthCountLess = view.findViewById(R.id.img_ip_tv_fim_standard_month_count_less);
        imgIPTVFimStandard_MonthCountPlus = view.findViewById(R.id.img_ip_tv_fim_standard_month_count_plus);
        frmDevicesBanner = view.findViewById(R.id.frm_devices);
        btnSelectDevice = view.findViewById(R.id.btn_select_device);
        RecyclerView mRecyclerViewDevice = view.findViewById(R.id.lv_list_device);
        frmContentFptBox = view.findViewById(R.id.frm_content_fpt_box);
        frmContentFptBox.setVisibility(View.GONE);
        frmListFptBox = view.findViewById(R.id.frm_list_fpt_box);
        RecyclerView listViewFptBox = view.findViewById(R.id.lv_list_fpt_box);
        RecyclerView listViewMac = view.findViewById(R.id.lv_list_mac);
        btnChooseMac = view.findViewById(R.id.btn_scan_mac);
        btnSelectFptBox = view.findViewById(R.id.btn_select_fpt_box);

        mListServiceSelected = new ArrayList<>();

        mListDeviceSelected = new ArrayList<>();
        mDeviceAdapter = new DeviceAdapter(this.getActivity(), mListDeviceSelected, this);
        mRecyclerViewDevice.setAdapter(mDeviceAdapter);

        listPromotionFptBox = new HashMap<>();
        listFptBoxSelect = new ArrayList<>();
        macList = new ArrayList<>();
        listNameFptBox = new ArrayList<>();
        fptBoxAdapter = new FPTBoxAdapter(this.getActivity(), listFptBoxSelect, listPromotionFptBox);
        listViewFptBox.setAdapter(fptBoxAdapter);
        macAdapter = new MacAdapter(getActivity(), macList);
        listViewMac.setAdapter(macAdapter);

        cbFoxy2 = view.findViewById(R.id.chb_iptv_foxy_2);
        imgFoxy2ChargeTimeLess = view.findViewById(R.id.img_btn_iptv_foxy_2_charge_times_less);
        imgFoxy2ChargeTimePlus = view.findViewById(R.id.img_btn_iptv_foxy_2_charge_times_plus);
        imgFoxy2PrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_foxy_2_month_count_pless);
        imgFoxy2PrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_foxy_2_month_count_plus);
        tvFoxy2ChargeTime = view.findViewById(R.id.txt_iptv_foxy_2_charge_times);
        tvFoxy2PrepaidMonth = view.findViewById(R.id.txt_iptv_foxy_2_month_count);
        tvFoxy2Amount = view.findViewById(R.id.lbl_iptv_foxy_2_amount);

        cbFoxy4 = view.findViewById(R.id.chb_iptv_foxy_4);
        imgFoxy4ChargeTimeLess = view.findViewById(R.id.img_btn_iptv_foxy_4_charge_times_less);
        imgFoxy4ChargeTimePlus = view.findViewById(R.id.img_btn_iptv_foxy_4_charge_times_plus);
        imgFoxy4PrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_foxy_4_month_count_pless);
        imgFoxy4PrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_foxy_4_month_count_plus);
        tvFoxy4ChargeTime = view.findViewById(R.id.txt_iptv_foxy_4_charge_times);
        tvFoxy4PrepaidMonth = view.findViewById(R.id.txt_iptv_foxy_4_month_count);
        tvFoxy4Amount = view.findViewById(R.id.lbl_iptv_foxy_4_amount);

        frmDacSacHD = view.findViewById(R.id.dac_sac_hd);
        frmDacSacHD.setVisibility(View.GONE);
    }

    @Override
    protected void initEvent() {
        txtPromotion.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterActivity();
            }
        });
        txtPromotion.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    lblPromotionInternetDetail.setText(txtPromotion.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        txtPromotionBox1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideSoftKeyboard(txtPromotionBox1);
                showFilterActivity(true, 1);
            }
        });
        txtPromotionBox1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    // lấy giá gói IPTV Extra box 1
                    getIPTVPrice();
                    lblIPTVPromotionBoxFirstAmount.setVisibility(View.VISIBLE);
                }
            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        txtPromotionBox2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideSoftKeyboard(txtPromotionBox2);
                showFilterActivity(true, 2);
            }
        });
        txtPromotionBox2.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showFilterActivity(true, 2);
                    Common.hideSoftKeyboard(txtPromotionBox2);
                }
            }
        });
        txtPromotionBox2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    lblIPTVPromotionBoxOrderAmount.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        txtIPTVBoxCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    int boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    if (boxCount >= 2) {
                        txtPromotionBox2.setEnabled(true);
                        getIPTVPromotionBoxOrder();
                    } else if (boxCount == 1) {
                        try {
                            txtPromotionBox2.setText(null);
                            txtPromotionBox2.setEnabled(false);
                            promotionIPTVModelBox2 = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        lblIPTVPromotionBoxOrderAmount.setText(null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnShowKeyWord.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PromotionKeyWordSearchDialog dialog = new PromotionKeyWordSearchDialog(getActivity());
                Common.showFragmentDialog(getFragmentManager(), dialog,
                        "fragment_promotion_keyWord_search_dialog");
            }
        });
        imgPromHasImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedInternetPromotion != null) {
                    String imagePath = selectedInternetPromotion.getImageUrl();
                    if (imagePath != null && !imagePath.equals("")) {
                        Intent intent = new Intent(getActivity(), ShowImageActivity.class);
                        intent.putExtra("PATH", imagePath);
                        startActivity(intent);
                    }
                }
            }
        });
        tvServiceType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelDetail != null) {//cập nhật
                    Bundle bundle = new Bundle();
                    List<CategoryServiceList> lstSv = new ArrayList<>();
                    lstSv.add(new CategoryServiceList(1, "Internet"));
                    lstSv.add(new CategoryServiceList(2, "Thiết bị"));
                    lstSv.add(new CategoryServiceList(4, "Pay TV"));
                    lstSv.add(new CategoryServiceList(7, "FPT Camera"));
                    bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                            (ArrayList<? extends Parcelable>) lstSv);
                    bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST_SELECTED,
                            (ArrayList<? extends Parcelable>) mListServiceSelected);
                    Intent intent = new Intent(getActivity(), ServiceTypeActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_SERVICE_TYPE);
                } else {//tạo PDK mới
                    if (((RegisterActivityNew) getActivity()).getListService().size() > 1) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                                (ArrayList<? extends Parcelable>) ((RegisterActivityNew) getActivity()).getListService());
                        bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST_SELECTED,
                                (ArrayList<? extends Parcelable>) mListServiceSelected);
                        Intent intent = new Intent(getActivity(), ServiceTypeActivity.class);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, CODE_SERVICE_TYPE);
                    }
                }
            }
        });
        tvLocalType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((RegisterActivityNew) getActivity()).getListService().size() > 1) {
                    if (TextUtils.isEmpty(tvServiceType.getText().toString())) {
                        Toast.makeText(getActivity(), "Chưa chọn loại dịch vụ", Toast.LENGTH_SHORT).show();
                    } else {
                        getLocalType();
                    }
                } else {
                    getLocalType();
                }
            }
        });
        tvLocalType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtPromotion.setText(null);
                localTypeId = -1;
            }
        });

        btnSelectDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Constants.DEVICE_LIST_SELECTED,
                        (ArrayList<? extends Parcelable>) mListDeviceSelected);
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3.class.getSimpleName());
                Intent intent = new Intent(getActivity(), DeviceListActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_DEVICE_LIST_SELECTED);

            }
        });

        btnSelectFptBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isBind = 1;
                new GetFPTBoxList(getActivity(), FragmentRegisterStep3.this);
            }
        });

        btnChooseMac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ChooseMacActivity.class);
                intent.putParcelableArrayListExtra(Constants.LIST_FPT_BOX_SELECTED,
                        (ArrayList<? extends Parcelable>) listFptBoxSelect);
                intent.putParcelableArrayListExtra(Constants.LIST_MAC_SELECTED,
                        (ArrayList<? extends Parcelable>) macList);
                startActivityForResult(intent, CODE_CHOOSE_MAC);
            }
        });

        radIpTvSetUpNo.setChecked(true);
        radIPTVDrillWallNo.setChecked(true);
        chbIpTvHbo.setOnCheckedChangeListener(this);
        chbIpTvFimHot.setOnCheckedChangeListener(this);
        chbIPTVFimPlus.setOnCheckedChangeListener(this);
        chbIPTVKPlus.setOnCheckedChangeListener(this);
        chbIpTvVtc.setOnCheckedChangeListener(this);
        chbIpTvVtv.setOnCheckedChangeListener(this);
        chbIPTVFimStandard.setOnCheckedChangeListener(this);
        spIPTVFormDeployment.setSelection(0, true);
        imgIPTVVTVPrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtv.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVVTVPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVVTVPrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtv.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVVTVPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVVTVChargeTimePlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtv.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVVTVChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVVTVChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtv.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVVTVChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        frmIpTvVtv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIpTvVtv.isChecked();
                chbIpTvVtv.setChecked(!checked);
            }
        });

        imgIPTVVTCPrepaidMonthPlus.setOnClickListener(new OnClickListener() {

            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtc.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVVTCPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVVTCPrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtc.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVVTCPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVVTCChargeTimePlus.setOnClickListener(new OnClickListener() {

            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtc.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVVTCChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVVTCChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtc.isChecked()) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVVTCChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        frmIpTvVtc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIpTvVtc.isChecked();
                chbIpTvVtc.setChecked(!checked);
            }
        });
        imgIPTVKPlusPrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVKPlusPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVKPlusPrepaidMonthLess.setOnClickListener(new OnClickListener() {

            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVKPlusPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVKPlusChargeTimePlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVKPlusChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVKPlusChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVKPlusChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        frmIPTVKPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIPTVKPlus.isChecked();
                chbIPTVKPlus.setChecked(!checked);
            }
        });
        // Số tháng trả trước FimHot
        imgIPTVFimHot_PrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvFimHot.isChecked()) {
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVFimHot_MonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgIPTVFimHot_PrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvFimHot.isChecked()) {
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVFimHot_MonthCount.setText(String.valueOf(count));
                }
            }
        });
        // Số lần tính cước FimHot
        imgIPTVFimHot_ChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvFimHot.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_ChargeTime.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVFimHot_ChargeTime.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgIPTVFimHot_ChargeTimePlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvFimHot.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_ChargeTime.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVFimHot_ChargeTime.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVFimPlus_PrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVFimPlus_MonthCount.setText(String.valueOf(count));
                }
            }
        });
        // Số tháng trả trước Fim+
        imgIPTVFimPlus_PrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVFimPlus_MonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgIPTVFimPlus_ChargeTimePlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_ChargeTime.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVFimPlus_ChargeTime.setText(String.valueOf(count));
                }
            }
        });
        // Số lần tính cước Fim+
        imgIPTVFimPlus_ChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_ChargeTime.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVFimPlus_ChargeTime.setText(String.valueOf(count));
                    }
                }
            }
        });

        frmIpTvFimHot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIpTvFimHot.isChecked();
                chbIpTvFimHot.setChecked(!checked);
            }
        });

        frmIPTVFimPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIPTVFimPlus.isChecked();
                chbIPTVFimPlus.setChecked(!checked);
            }
        });

        //Cac nút tăng giảm trả trước các gói
        imgIPTVHBOPrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvHbo.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVHBOPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVHBOPrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvHbo.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        // lại tiền IPTV)
                        count--;
                        lblIPTVHBOPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVHBOChargeTimePlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvHbo.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVHBOChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVHBOChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvHbo.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVHBOChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVChargeTimesPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVChargeTimesCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int boxCount;
                try {
                    boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    if (count < boxCount)
                        count++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                lblIPTVChargeTimesCount.setText(String.valueOf(count));
            }
        });

        spIPTVPackage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                int packageID = selectedItem.getID();
                if (packageID != 0) {
                    new GetPromotionIPTVList(getActivity(), FragmentRegisterStep3.this, selectedItem.getHint(),
                            1, customerType, contractParam, regCodeParam);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        imgIPTVReturnSTBPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVReturnSTBCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                count++;
                lblIPTVReturnSTBCount.setText(String.valueOf(count));
            }
        });

        imgIPTVReturnSTBLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVReturnSTBCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (count > 0) {
                    count--;
                    lblIPTVReturnSTBCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVPLCPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVPLCCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                count++;
                lblIPTVPLCCount.setText(String.valueOf(count));
            }
        });

        imgIPTVPLCLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVPLCCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (count > 0) {
                    count--;
                    lblIPTVPLCCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVBoxPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // IPTV)
                if (spIPTVPackage.getSelectedItemPosition() == 0)
                    spIPTVPackage.setSelection(1, true);
                int boxCount = 0;
                try {
                    boxCount = Integer.parseInt(txtIPTVBoxCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                boxCount++;
                txtIPTVBoxCount.setText(String.valueOf(boxCount));
            }
        });
        imgIPTVBoxLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // IPTV)
                int boxCount = 0;
                try {
                    boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (boxCount > 1) {
                    boxCount--;
                    txtIPTVBoxCount.setText(String.valueOf(boxCount));
                }
            }
        });
        imgIPTVFimStandard_MonthCountPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chbIPTVFimStandard.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimStandard_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVFimStandard_MonthCount.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVFimStandard_MonthCountLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chbIPTVFimStandard.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimStandard_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVFimStandard_MonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVFimStandard_ChargeTimePlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chbIPTVFimStandard.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    int boxCount = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimStandard_ChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVFimStandard_ChargeTimes.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVFimStandard_ChargeTimeLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chbIPTVFimStandard.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimStandard_ChargeTimes.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVFimStandard_ChargeTimes.setText(String.valueOf(count));
                    }
                }
            }
        });
        frmIPTVFimStandard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIPTVFimStandard.isChecked();
                chbIPTVFimStandard.setChecked(!checked);
            }
        });

        //foxy 2
        imgFoxy2ChargeTimeLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy2.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy2ChargeTime.getText().toString());
                if (quantity == 1) {
                    return;
                }
                quantity--;
                tvFoxy2ChargeTime.setText(String.valueOf(quantity));
            }
        });

        imgFoxy2ChargeTimePlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy2.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy2ChargeTime.getText().toString());
                int maxQuantity = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                if (quantity < maxQuantity) {
                    quantity++;
                }
                tvFoxy2ChargeTime.setText(String.valueOf(quantity));
            }
        });

        imgFoxy2PrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy2.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy2PrepaidMonth.getText().toString());
                if (quantity == 0) {
                    return;
                }
                quantity--;
                tvFoxy2PrepaidMonth.setText(String.valueOf(quantity));
            }
        });

        imgFoxy2PrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy2.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy2PrepaidMonth.getText().toString());
                quantity++;
                tvFoxy2PrepaidMonth.setText(String.valueOf(quantity));
            }
        });

        //foxy4
        imgFoxy4ChargeTimeLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy4.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy4ChargeTime.getText().toString());
                if (quantity == 1) {
                    return;
                }
                quantity--;
                tvFoxy4ChargeTime.setText(String.valueOf(quantity));
            }
        });

        imgFoxy4ChargeTimePlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy4.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy4ChargeTime.getText().toString());
                int maxQuantity = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                if (quantity < maxQuantity) {
                    quantity++;
                }
                tvFoxy4ChargeTime.setText(String.valueOf(quantity));
            }
        });

        imgFoxy4PrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy4.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy4PrepaidMonth.getText().toString());
                if (quantity == 0) {
                    return;
                }
                quantity--;
                tvFoxy4PrepaidMonth.setText(String.valueOf(quantity));
            }
        });

        imgFoxy4PrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy4.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy4PrepaidMonth.getText().toString());
                quantity++;
                tvFoxy4PrepaidMonth.setText(String.valueOf(quantity));
            }
        });

        cbFoxy2.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                if (modelDetail == null) {
                    tvFoxy2ChargeTime.setText(String.valueOf(1));
                    tvFoxy2PrepaidMonth.setText(String.valueOf(0));
                    return;
                }
                tvFoxy2ChargeTime.setText(modelDetail.getFoxy2ChargeTimes() != 0 ?
                        String.valueOf(modelDetail.getFoxy2ChargeTimes()) : String.valueOf(1));
                tvFoxy2PrepaidMonth.setText(modelDetail.getFoxy2PrepaidMonth() != 0 ?
                        String.valueOf(modelDetail.getFoxy2PrepaidMonth()) : String.valueOf(0));
            } else {
                tvFoxy2ChargeTime.setText(String.valueOf(0));
                tvFoxy2PrepaidMonth.setText(String.valueOf(0));
            }
        });

        cbFoxy4.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    if (modelDetail == null) {
                        tvFoxy4ChargeTime.setText(String.valueOf(1));
                        tvFoxy4PrepaidMonth.setText(String.valueOf(0));
                        return;
                    }
                    tvFoxy4ChargeTime.setText(modelDetail.getFoxy4ChargeTimes() != 0 ?
                            String.valueOf(modelDetail.getFoxy4ChargeTimes()) : String.valueOf(1));
                    tvFoxy4PrepaidMonth.setText(modelDetail.getFoxy4PrepaidMonth() != 0 ?
                            String.valueOf(modelDetail.getFoxy4PrepaidMonth()) : String.valueOf(0));
                } else {
                    tvFoxy4ChargeTime.setText(String.valueOf(0));
                    tvFoxy4PrepaidMonth.setText(String.valueOf(0));
                }
            }
        });
    }

    public List<Device> getListDeviceSelect() {
        return mListDeviceSelected;
    }

    public List<FPTBox> getListFptBoxSelect() {
        return listFptBoxSelect;
    }

    public List<MacOTT> getMacList() {
        return macList;
    }

    private void getLocalType() {
        if (mListServiceSelected.size() == 0) {
            List<CategoryServiceList> mListService = new ArrayList<>();
            if (modelDetail != null && modelDetail.getCategoryServiceList() != null &&
                    modelDetail.getCategoryServiceList().size() != 0) {
                mListService = modelDetail.getCategoryServiceList();
            }
            if (((RegisterActivityNew) getActivity()).getListService() != null &&
                    ((RegisterActivityNew) getActivity()).getListService().size() != 0) {
                mListService = ((RegisterActivityNew) getActivity()).getListService();
            }
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                    (ArrayList<? extends Parcelable>) mListService);
            Intent intent = new Intent(getActivity(), LocalTypeActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_LOCAL_TYPE);
        } else {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                    (ArrayList<? extends Parcelable>) mListServiceSelected);
            Intent intent = new Intent(getActivity(), LocalTypeActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_LOCAL_TYPE);
        }
    }

    @SuppressLint("SetTextI18n")
    private void initViewByServiceType(List<CategoryServiceList> lstServiceType) {
        if (lstServiceType.size() == 1) {
            if (lstServiceType.get(0).getCategoryServiceID() == 1) {//internet
                visibleFPTBoxControl(false);
                visibleDevicesControl(false);
                visibleIPTVControl(false);
            } else if (lstServiceType.get(0).getCategoryServiceID() == 3) {//fpt box
                visibleInternetControl(false);
                visibleDevicesControl(false);
                visibleIPTVControl(false);
            } else {//camera
                visibleInternetControl(false);
                visibleFPTBoxControl(false);
                visibleDevicesControl(false);
                visibleIPTVControl(false);
            }
        } else if (lstServiceType.size() == 2) {
            if (lstServiceType.get(0).getCategoryServiceID() == 1 &&
                    lstServiceType.get(1).getCategoryServiceID() == 2) {//internet + thiet bi
                visibleFPTBoxControl(false);
                visibleIPTVControl(false);
            } else {//internet + iptv
                visibleFPTBoxControl(false);
                visibleDevicesControl(false);
            }
        } else if (lstServiceType.size() == 3) {
            if (!TextUtils.isEmpty(tvServiceType.getText().toString())) {
                visibleFPTBoxControl(false);
            } else {
                visibleInternetControl(false);
                visibleFPTBoxControl(false);
                visibleDevicesControl(false);
                visibleIPTVControl(false);
                return;
            }
        }

        for (CategoryServiceList item : lstServiceType) {
            //internet
            if (item.getCategoryServiceID() == 1) {
                visibleInternetControl(true);
            }
            //thiết bị
            if (item.getCategoryServiceID() == 2) {
                visibleDevicesControl(true);
            }
            //ip tv
            if (item.getCategoryServiceID() == 4) {
                visibleIPTVControl(true);
            }
            //fpt play box
            if (item.getCategoryServiceID() == 3) {
                visibleFPTBoxControl(true);
                tvLocalType.setText("FPT Play");
                localTypeId = 210;
                tvLocalType.setEnabled(false);
                tvServiceType.setEnabled(false);
            }
        }
    }

    private void showFilterActivity(boolean promotionIPTV, int box) {
        try {
            if (promotionIPTV) {
                if (mListPromotionIPTVBox1 != null && box == 1) {
                    Intent intent = new Intent(getActivity(), PromotionIPTVFilterActivity.class);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_TYPE_BOX, box);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_LIST, mListPromotionIPTVBox1);
                    getActivity().startActivityForResult(intent, 1);
                }
                if (mListPromotionIPTVBox2 != null && box == 2) {
                    Intent intent = new Intent(getActivity(), PromotionIPTVFilterActivity.class);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_TYPE_BOX, box);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_LIST, mListPromotionIPTVBox2);
                    getActivity().startActivityForResult(intent, 1);
                }
            } else {
                if (mListPromotion != null) {
                    Intent intent = new Intent(getActivity(), PromotionFilterActivity.class);
                    intent.putExtra(PromotionFilterActivity.TAG_PROMOTION_LIST, mListPromotion);
                    startActivityForResult(intent, 1);
                } else {
                    Toast.makeText(getActivity(), "Chưa chọn gói dịch vụ", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // init data device
    public void initDataDevice() {
        if (modelDetail != null && modelDetail.getListDevice() != null) {
            mListDeviceSelected = modelDetail.getListDevice();
            mDeviceAdapter.notifyData(mListDeviceSelected);
        }
        if (mListDeviceSelected.size() > 0) {
            visibleDevicesControl(true);
        }
    }

    // Tuấn cập nhật code 16012018
    // init data fpt box
    public void initDataFptBox() {
        if (modelDetail != null) {
            if (modelDetail.getListDeviceMACOTT() != null && modelDetail.getListDeviceMACOTT().getListDeviceOTT() != null) {
                listFptBoxSelect.addAll(modelDetail.getListDeviceMACOTT().getListDeviceOTT());
                for (FPTBox fptBox : listFptBoxSelect) {
                    if (listPromotionFptBox.get(fptBox.getOTTID()) == null) {
                        ArrayList<PromotionFPTBoxModel> arrayList = new ArrayList<>();
                        arrayList.add(new PromotionFPTBoxModel(-1, "Chọn CLKM FPT Box"));
                        arrayList.add(new PromotionFPTBoxModel(fptBox.getPromotionID(), fptBox.getPromotionText()));
                        listPromotionFptBox.put(fptBox.getOTTID(), arrayList);
                    }
                }
            }
            if (modelDetail.getListDeviceMACOTT() != null && modelDetail.getListDeviceMACOTT().getListMACOTT() != null) {
                macList.addAll(modelDetail.getListDeviceMACOTT().getListMACOTT());
            }
        }

        if (listFptBoxSelect.size() > 0) {
            frmListFptBox.setVisibility(View.VISIBLE);
            btnChooseMac.setVisibility(View.VISIBLE);
        }
    }

    // thêm 1 địa chỉ mac vào danh sách.
    public void addMacAddressToList(String id) {
        if (checkMacAddress(id)) {
            Common.alertDialog("Địa chỉ Mac này đã được quét trước đó.", getActivity());
        } else {
            macList.add(new MacOTT(id));
            macAdapter.notifyDataSetChanged();
            Common.hideSoftKeyboard(getActivity());
        }
    }

    // kiểm tra sự tồn tại của địa chỉ Mac trong danh sách Mac
    public boolean checkMacAddress(String value) {
        boolean checkExist = false;
        for (MacOTT mac : macList) {
            if (mac.getMAC().equals(value)) {
                checkExist = true;
                break;
            }
        }
        return checkExist;
    }

    private void startScan() {
        Intent intent = new Intent(getActivity(), BarcodeScanActivity.class);
        startActivityForResult(intent, 3);
    }

    // tai danh sach fpt box tu server
    public void loadFptBox(ArrayList<FPTBox> fptBos) {
        macAdapter.notifyDataSetChanged();
        listNameFptBox.clear();
        this.listFptBox = fptBos;
        boolean[] is_checked_fpt_box = new boolean[listFptBox.size()];
        for (int i = 0; i < listFptBox.size(); i++) {
            listNameFptBox.add(listFptBox.get(i).getOTTName());
        }
        if (listFptBoxSelect.size() > 0) {
            Common.getFPTBoxSelected(listFptBoxSelect, listFptBox, is_checked_fpt_box);
            listFptBoxSelect.clear();
        }
        listFptBoxSelect.addAll(listFptBox);
        CharSequence[] dialogListFptBox = listNameFptBox.toArray(new CharSequence[listNameFptBox.size()]);
        AlertDialog alert = Common.getAlertDialogShowFPTBox(
                getActivity(),
                "FPT BOX",
                is_checked_fpt_box,
                dialogListFptBox,
                listFptBoxSelect,
                listFptBox,
                frmListFptBox,
                fptBoxAdapter,
                true,
                btnChooseMac
        );
        alert.setCancelable(false);
        alert.show();
    }

    // tai cau lenh khuyen mai fpt box tu server
    public void loadPromotionFptBox(ArrayList<PromotionFPTBoxModel> lst, Spinner spinnerPromotion,
                                    int fptBoxID, int idPromotionSelected) {
        listPromotionFptBox.get(fptBoxID).clear();
        listPromotionFptBox.get(fptBoxID).add(new PromotionFPTBoxModel(-1, "Chọn CLKM FPT Box"));
        listPromotionFptBox.get(fptBoxID).addAll(lst);
        spinnerPromotion.setSelection(Common.getIndex(spinnerPromotion, idPromotionSelected), true);
    }

    private void getIPTVPromotionBoxOrder() {
        String packageName = ((KeyValuePairModel) spIPTVPackage.getSelectedItem()).getHint();
        new GetPromotionIPTVList(getActivity(), this, packageName, 2,
                customerType, contractParam, regCodeParam);
    }

    // khởi tạo màn hình LỌC CÂU LỆNH KHUYẾN MÃI INTERNET
    private void showFilterActivity() {
        try {
            if (mListPromotion != null) {
                Intent intent = new Intent(getActivity(), PromotionFilterActivity.class);
                intent.putExtra(PromotionFilterActivity.TAG_PROMOTION_LIST, mListPromotion);
                getActivity().startActivityForResult(intent, 1);
            } else {
                Toast.makeText(getActivity(), "Chưa chọn gói dịch vụ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIPTVPrice() {
        int IPTVPromotionType = (promotionIPTVModelBox1 != null ? promotionIPTVModelBox1.getType() : -1);
        int IPTVPromotionID = (promotionIPTVModelBox1 != null ? promotionIPTVModelBox1.getPromotionID() : 0);
        new GetIPTVPrice(getActivity(), this, contractParam, regCodeParam, IPTVPromotionType, IPTVPromotionID);
    }

    public void loadIPTVPrice(List<IPTVPriceModel> lst) {
        if (lst != null && lst.size() > 0) {
            try {
                IPTVPriceModel item = lst.get(0);
                lblIPTVFimHotAmount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFimHot())));
                lblIPTVFimPlusAmount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFimPlus())));
                lblIPTVKPlusAmount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getKPlus())));
                lblIPTVVTVAmount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getVTVCap())));
                lblIPTVVTCAmount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getDacSacHD())));
                lblIPTVFimStandard_Amount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFimPlusStd())));
                tvFoxy2Amount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFoxy2())));
                tvFoxy4Amount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFoxy4())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initControlElse() {
        if (modelDetail != null) {
            if (modelDetail.getIPTVRequestSetUp() > 0) {
                radIpTvSetUpYes.setChecked(true);
            } else {
                radIpTvSetUpNo.setChecked(true);
            }

            if (modelDetail.getIPTVRequestDrillWall() > 0) {
                radIPTVDrillWallYes.setChecked(true);
            } else {
                radIPTVDrillWallNo.setChecked(true);
            }
            txtIPTVBoxCount.setText(String.valueOf(modelDetail.getIPTVBoxCount()));
            lblIPTVPLCCount.setText(String.valueOf(modelDetail.getIPTVPLCCount()));
            lblIPTVReturnSTBCount.setText(String.valueOf(modelDetail.getIPTVReturnSTBCount()));
            lblIPTVChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVChargeTimes()));
            txtIPTVPrepaid.setText(String.valueOf(modelDetail.getIPTVPrepaid()));

            // load HBO Package Extra
            if (modelDetail.getIPTVHBOChargeTimes() > 0 || modelDetail.getIPTVHBOPrepaidMonth() > 0) {
                chbIpTvHbo.setChecked(true);
            }
            //hiển thị thông tin K+
            if (modelDetail.getIPTVKPlusChargeTimes() > 0 || modelDetail.getIPTVKPlusPrepaidMonth() > 0) {
                chbIPTVKPlus.setChecked(true);
            }
            //hiển thị thông tin Đặc sắc HD
            if (modelDetail.getIPTVVTCChargeTimes() > 0 || modelDetail.getIPTVVTCPrepaidMonth() > 0) {
                chbIpTvVtc.setChecked(true);
            }
            //hiển thị thông tin VTV Cab
            if (modelDetail.getIPTVVTVChargeTimes() > 0 || modelDetail.getIPTVVTVPrepaidMonth() > 0) {
                chbIpTvVtv.setChecked(true);
            }
            //hiển thị thông tin Fim+ -> Fim Cao Cap
            if (modelDetail.getIPTVFimPlusChargeTimes() > 0 || modelDetail.getIPTVFimPlusPrepaidMonth() > 0) {
                chbIPTVFimPlus.setChecked(true);
            }
            //hiển thị thông tin Fim Hot
            if (modelDetail.getIPTVFimHotChargeTimes() > 0 || modelDetail.getIPTVFimHotPrepaidMonth() > 0) {
                chbIpTvFimHot.setChecked(true);
            }
            //hiển thị thông tin Fim cơ bản
            if (modelDetail.getIPTVFimPlusStdChargeTimes() > 0 || modelDetail.getIPTVFimPlusStdPrepaidMonth() > 0) {
                chbIPTVFimStandard.setChecked(true);
            }

            if (modelDetail.getFoxy2ChargeTimes() > 0 || modelDetail.getFoxy2PrepaidMonth() > 0) {
                cbFoxy2.setChecked(true);
            }

            if (modelDetail.getFoxy4ChargeTimes() > 0 || modelDetail.getFoxy4PrepaidMonth() > 0) {
                cbFoxy4.setChecked(true);
            }
        }
    }

    // khởi tạo giá trị contract và regcode từ pdk
    private void initContractRegCode() {
        contractParam = modelDetail != null ? modelDetail.getContract() : "";
        regCodeParam = modelDetail != null ? modelDetail.getRegCode() : "";
    }

    // khởi tạo danh sách clkm iptv box 1
    @SuppressLint("SetTextI18n")
    public void initIPTVPromotionIPTVBox1(ArrayList<PromotionIPTVModel> lst) {
        this.mListPromotionIPTVBox1 = lst;
        if (this.mListPromotionIPTVBox1.size() > 0) {
            try {
                if (modelDetail != null) {
                    PromotionIPTVModel promotionIPTVModel = Common.getItemPromotionIPTVBox(lst, 1, modelDetail);
                    promotionIPTVModelBox1 = promotionIPTVModel;
                    if (promotionIPTVModel != null) {
                        txtPromotionBox1.setText(promotionIPTVModel.getPromotionName());
                        lblIPTVPromotionBoxFirstAmount.setText(
                                String.format(getString(R.string.txt_unit_price), Common.formatNumber(promotionIPTVModel.getAmount())));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // khởi tạo danh sách clkm iptv box 2
    public void initIPTVPromotionIPTVBox2(ArrayList<PromotionIPTVModel> lst) {
        this.mListPromotionIPTVBox2 = lst;
        if (this.mListPromotionIPTVBox2.size() > 0) {
            try {
                if (modelDetail != null) {
                    PromotionIPTVModel promotionIPTVModel = Common.getItemPromotionIPTVBox(lst, 2, modelDetail);
                    if (promotionIPTVModel != null) {
                        promotionIPTVModelBox2 = promotionIPTVModel;
                        txtPromotionBox2.setText(promotionIPTVModel.getPromotionName());
                        lblIPTVPromotionBoxOrderAmount.setText(
                                String.format(getString(R.string.txt_unit_price),
                                        Common.formatNumber(promotionIPTVModel.getAmount()))
                        );
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Combo IPTV
    private void initIPTVCombo() {
        ArrayList<KeyValuePairModel> lstCombo = new ArrayList<>();
        lstCombo.add(new KeyValuePairModel(0, "[Chọn combo]"));
        lstCombo.add(new KeyValuePairModel(2, "Không combo"));
        lstCombo.add(new KeyValuePairModel(3, "Combo"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(), R.layout.my_spinner_style, lstCombo, Gravity.LEFT);
        spIPTVCombo.setAdapter(adapter);
        if (modelDetail != null) {
            try {
                spIPTVCombo.setSelection(Common.getIndex(spIPTVCombo, modelDetail.getIPTVUseCombo()), true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initIPTVFormDeployment() {
        ArrayList<KeyValuePairModel> lstCusSolvency = new ArrayList<>();
        lstCusSolvency.add(new KeyValuePairModel(5, "Triển khai mới một line"));
        lstCusSolvency.add(new KeyValuePairModel(1, "Triển khai mới nhiều line"));
        spIPTVFormDeployment.setAdapter(new KeyValuePairAdapter(getActivity(), R.layout.my_spinner_style, lstCusSolvency, Gravity.LEFT));
        if (modelDetail != null && modelDetail.getIPTVPackage() != null) {
            if (!modelDetail.getIPTVPackage().trim().equals("")) {
                spIPTVFormDeployment.setSelection(Common.getIndex(spIPTVFormDeployment, modelDetail.getIPTVStatus()), true);
            }
        }
    }

    /* ======================= Init Package ============================= */
    private void initIPTVAllPackages() {
        ArrayList<KeyValuePairModel> lst = new ArrayList<>();
        lst.add(new KeyValuePairModel(0, "--Chọn gói dịch vụ--", ""));
        lst.add(new KeyValuePairModel(76, "Truyền Hình FPT", "VOD HD"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(), R.layout.my_spinner_style, lst, Gravity.RIGHT);
        spIPTVPackage.setAdapter(adapter);
        if (modelDetail != null) {
            try {
                for (int i = 0; i < lst.size(); i++) {
                    KeyValuePairModel selectedItem = lst.get(i);
                    if (selectedItem.getHint().equalsIgnoreCase(modelDetail.getIPTVPackage())) {
                        spIPTVPackage.setSelection(i, true);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void visibleInternetControl(boolean flag) {
        if (flag) {
            frmInternet.setVisibility(View.VISIBLE);
        } else {
            frmInternet.setVisibility(View.GONE);
        }
    }

    private void visibleIPTVControl(boolean flag) {
        if (flag) {
            is_have_ip_tv = true;
            frmIpTv.setVisibility(View.VISIBLE);
            disableIPTVControls(false);
        } else {
            is_have_ip_tv = false;
            frmIpTv.setVisibility(View.GONE);
            disableIPTVControls(true);
        }
    }

    private void visibleFPTBoxControl(boolean flag) {
        if (flag) {
            is_have_fpt_box = true;
            frmContentFptBox.setVisibility(View.VISIBLE);
            if (listFptBoxSelect.size() > 0) {
                frmListFptBox.setVisibility(View.VISIBLE);
                btnChooseMac.setVisibility(View.VISIBLE);
            }
        } else {
            is_have_fpt_box = false;
            frmContentFptBox.setVisibility(View.GONE);
            frmListFptBox.setVisibility(View.GONE);
            btnChooseMac.setVisibility(View.GONE);
            if (listFptBoxSelect.size() > 0) {
                listFptBoxSelect.clear();
            }
            if (macList.size() > 0) {
                macList.clear();
            }
        }
        fptBoxAdapter.notifyDataSetChanged();
        macAdapter.notifyDataSetChanged();
    }

    // ẩn hiện tab thiết bị
    private void visibleDevicesControl(boolean flag) {
        if (flag) {
            is_have_device = true;
            frmDevicesBanner.setVisibility(View.VISIBLE);
        } else {
            is_have_device = false;
            frmDevicesBanner.setVisibility(View.GONE);
            if (mListDeviceSelected.size() > 0) {
                mListDeviceSelected.clear();
            }
        }
        mDeviceAdapter.notifyData(mListDeviceSelected);
    }

    private void disableIPTVControls(boolean status) {
        boolean enabled = !status;
        radIpTvSetUpNo.setEnabled(enabled);
        radIpTvSetUpYes.setEnabled(enabled);
        radIPTVDrillWallYes.setEnabled(enabled);
        radIPTVDrillWallNo.setEnabled(enabled);
        spIPTVFormDeployment.setEnabled(enabled);
        txtIPTVBoxCount.setEnabled(enabled);
        spIPTVPackage.setEnabled(enabled);
        txtPromotionBox1.setEnabled(enabled);
        txtIPTVPrepaid.setEnabled(false);
        imgIPTVBoxLess.setEnabled(enabled);
        imgIPTVBoxPlus.setEnabled(enabled);
        imgIPTVPLCLess.setEnabled(enabled);
        imgIPTVPLCPlus.setEnabled(enabled);
        imgIPTVReturnSTBLess.setEnabled(enabled);
        imgIPTVReturnSTBPlus.setEnabled(enabled);
        imgIPTVChargeTimesPlus.setEnabled(enabled);
        imgIPTVTotal = enabled;
        spIPTVEquipmentDelivery = enabled;
        spIPTVCombo.setEnabled(enabled);
        frmIPTVMonthlyTotal = false;
        if (!enabled) {
            radIPTVDrillWallNo.setChecked(true);
            radIpTvSetUpNo.setChecked(true);
            spIPTVPackage.setSelection(0, true);
            spIPTVFormDeployment.setSelection(0, true);
            lblIPTVTotal = "0";
            lblTotal = "0";
            txtIPTVBoxCount.setText("0");
            lblIPTVPLCCount.setText("0");
            txtIPTVPrepaid.setText("0");
            lblIPTVReturnSTBCount.setText("0");
            lblIPTVChargeTimesCount.setText("0");
            spIPTVCombo.setSelection(0, true);
            lblIPTVPromotionBoxFirstAmount.setText(null);
            lblIPTVPromotionBoxOrderAmount.setText(null);

            txtPromotionBox1.setText("");
            txtPromotionBox2.setText("");
            cbFoxy2.setChecked(false);
            cbFoxy4.setChecked(false);
            chbIpTvFimHot.setChecked(false);
            chbIPTVFimPlus.setChecked(false);
            chbIPTVFimStandard.setChecked(false);
            chbIpTvHbo.setChecked(false);
            chbIPTVKPlus.setChecked(false);
            chbIpTvVtc.setChecked(false);
            chbIpTvVtv.setChecked(false);

            lblIPTVFimStandard_Amount.setText(String.format(getString(R.string.txt_unit_price), String.valueOf(0)));
            lblIPTVFimPlusAmount.setText(String.format(getString(R.string.txt_unit_price), String.valueOf(0)));
            lblIPTVFimHotAmount.setText(String.format(getString(R.string.txt_unit_price), String.valueOf(0)));
            lblIPTVKPlusAmount.setText(String.format(getString(R.string.txt_unit_price), String.valueOf(0)));
            lblIPTVVTCAmount.setText(String.format(getString(R.string.txt_unit_price), String.valueOf(0)));
            lblIPTVVTVAmount.setText(String.format(getString(R.string.txt_unit_price), String.valueOf(0)));
            tvFoxy2Amount.setText(String.format(getString(R.string.txt_unit_price), String.valueOf(0)));
            tvFoxy4Amount.setText(String.format(getString(R.string.txt_unit_price), String.valueOf(0)));
        } else {
            if (lblIPTVChargeTimesCount.getText().toString().equals("0")
                    || lblIPTVChargeTimesCount.getText().toString().equals(""))
                lblIPTVChargeTimesCount.setText("1");
            if (txtIPTVBoxCount.getText().toString().equals("0") || txtIPTVBoxCount.getText().toString().equals(""))
                txtIPTVBoxCount.setText("1");
        }
    }

    public static int indexOf(ArrayList<PromotionModel> lst, int value) {
        for (int i = 0; i < lst.size(); i++) {
            PromotionModel item = lst.get(i);
            if (item.getPromotionID() == value)
                return i;
        }
        return -1;
    }

    // tải danh sách câu lệnh khuyến mãi về
    public void setAutoCompletePromotionAdapter(ArrayList<PromotionModel> lst) {
        this.mListPromotion = lst;
        if (txtPromotion != null) {
            txtPromotion.setText(null);
            try {
                if (lst != null && lst.size() > 0) {
                    selectedInternetPromotion = lst.get(0);
                    if (modelDetail != null) {
                        int position = indexOf(lst, modelDetail.getPromotionID());
                        if (position >= 0) {
                            selectedInternetPromotion = lst.get(position);
                            txtPromotion.setText(selectedInternetPromotion.getPromotionName());
                            int total = selectedInternetPromotion.getRealPrepaid();
                            lblInternetTotal = Common.formatNumber(total);
                        } else {
                            selectedInternetPromotion = null;
                            txtPromotion.setText("");
                            lblInternetTotal = "0";
                        }
                    } else {
                        selectedInternetPromotion = null;
                        txtPromotion.setText("");
                        lblInternetTotal = "0";
                    }
                } else {
                    selectedInternetPromotion = null;
                    txtPromotion.setText("");
                    lblInternetTotal = "0";
                }
            } catch (Exception e) {
                e.printStackTrace();
                e.printStackTrace();
            }
        }
    }

    public void getIPTVTotalNew() {
        if (spIPTVPackage.getSelectedItemPosition() <= 0) {
            errorStr = "Chưa chọn gói dịch vụ IPTV!";
        } else {
            int IPTVPLCCount = Integer.parseInt(lblIPTVPLCCount.getText().toString()),
                    IPTVReturnSTBCount = Integer.parseInt(lblIPTVReturnSTBCount.getText().toString()),
                    IPTVPromotionID = promotionIPTVModelBox1 != null ? promotionIPTVModelBox1.getPromotionID() : 0,
                    IPTVChargeTimes = 1,
                    IPTVPrepaid = (!txtIPTVPrepaid.getText().toString().equals("")
                            ? Integer.parseInt(txtIPTVPrepaid.getText().toString()) : 0),
                    IPTVBoxCount = Integer.parseInt(txtIPTVBoxCount.getText().toString()), IPTVHBOPrepaidMonth = Integer.parseInt(lblIPTVHBOPrepaidMonthCount.getText().toString()),
                    IPTVHBOChargeTimes = Integer.parseInt(lblIPTVHBOChargeTimesCount.getText().toString()), IPTVVTVPrepaidMonth = Integer.parseInt(lblIPTVVTVPrepaidMonthCount.getText().toString()),
                    IPTVVTVChargeTimes = Integer.parseInt(lblIPTVVTVChargeTimesCount.getText().toString()), IPTVKPlusPrepaidMonth = Integer.parseInt(lblIPTVKPlusPrepaidMonthCount.getText().toString()),
                    IPTVKPlusChargeTimes = Integer.parseInt(lblIPTVKPlusChargeTimesCount.getText().toString()), IPTVVTCPrepaidMonth = Integer.parseInt(lblIPTVVTCPrepaidMonthCount.getText().toString()),
                    IPTVVTCChargeTimes = Integer.parseInt(lblIPTVVTCChargeTimesCount.getText().toString()),
                    // Fim Chuan
                    IPTVFimStandardChargeTimes = Integer.parseInt(lblIPTVFimStandard_ChargeTimes.getText().toString()),
                    IPTVFimStandardPrepaidMonth = Integer.parseInt(lblIPTVFimStandard_MonthCount.getText().toString()),

                    IPTVFimPlusPrepaidMonth = Integer.parseInt(lblIPTVFimPlus_MonthCount.getText().toString()),
                    IPTVFimPlusChargeTimes = Integer.parseInt(lblIPTVFimPlus_ChargeTime.getText().toString()),
                    IPTVFimHotPrepaidMonth = Integer.parseInt(lblIPTVFimHot_MonthCount.getText().toString()),
                    IPTVFimHotChargeTimes = Integer.parseInt(lblIPTVFimHot_ChargeTime.getText().toString()),
                    IPTVPromotionIDBoxOrder = promotionIPTVModelBox2 != null ? promotionIPTVModelBox2.getPromotionID() : 0;
            RegistrationDetailModel mRegister = new RegistrationDetailModel();
            mRegister.setIPTVPackage(((KeyValuePairModel) spIPTVPackage.getSelectedItem()).getHint());
            mRegister.setIPTVPromotionID(IPTVPromotionID);
            mRegister.setIPTVChargeTimes(IPTVChargeTimes);
            mRegister.setIPTVPrepaid(IPTVPrepaid);
            mRegister.setIPTVBoxCount(IPTVBoxCount);
            mRegister.setIPTVPLCCount(IPTVPLCCount);
            mRegister.setIPTVReturnSTBCount(IPTVReturnSTBCount);
            mRegister.setIPTVVTVPrepaidMonth(IPTVVTVPrepaidMonth);
            mRegister.setIPTVVTVChargeTimes(IPTVVTVChargeTimes);
            mRegister.setIPTVKPlusPrepaidMonth(IPTVKPlusPrepaidMonth);
            mRegister.setIPTVKPlusChargeTimes(IPTVKPlusChargeTimes);
            mRegister.setIPTVVTCPrepaidMonth(IPTVVTCPrepaidMonth);
            mRegister.setIPTVVTCChargeTimes(IPTVVTCChargeTimes);
            mRegister.setIPTVHBOPrepaidMonth(IPTVHBOPrepaidMonth);
            mRegister.setIPTVHBOChargeTimes(IPTVHBOChargeTimes);
            mRegister.setIPTVFimPlusPrepaidMonth(IPTVFimPlusPrepaidMonth);
            mRegister.setIPTVFimPlusChargeTimes(IPTVFimPlusChargeTimes);
            mRegister.setIPTVFimHotPrepaidMonth(IPTVFimHotPrepaidMonth);
            mRegister.setIPTVFimHotChargeTimes(IPTVFimHotChargeTimes);
            mRegister.setIPTVPromotionIDBoxOther(IPTVPromotionIDBoxOrder);
            mRegister.setIPTVFimPlusStdChargeTimes(IPTVFimStandardChargeTimes);
            mRegister.setIPTVFimPlusStdPrepaidMonth(IPTVFimStandardPrepaidMonth);
            mRegister.setFoxy2ChargeTimes(Integer.parseInt(tvFoxy2ChargeTime.getText().toString()));
            mRegister.setFoxy2PrepaidMonth(Integer.parseInt(tvFoxy2PrepaidMonth.getText().toString()));
            mRegister.setFoxy4ChargeTimes(Integer.parseInt(tvFoxy4ChargeTime.getText().toString()));
            mRegister.setFoxy4PrepaidMonth(Integer.parseInt(tvFoxy4PrepaidMonth.getText().toString()));

            int serviceType = this.serviceType;
            if (IPTVPromotionID == -100) {
                errorStr = "Chưa chọn CLKM IPTV!";
            } else {
                this.mRegister = mRegister;
                this.serviceType = serviceType;
                errorStr = "ok";
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.chb_iptv_hbo) {
            imgIPTVHBOChargeTimeLess.setEnabled(isChecked);
            imgIPTVHBOChargeTimePlus.setEnabled(isChecked);
            imgIPTVHBOPrepaidMonthLess.setEnabled(isChecked);
            imgIPTVHBOPrepaidMonthPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVHBOChargeTimesCount.setText("0");
                lblIPTVHBOPrepaidMonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVHBOChargeTimes() > 0 || modelDetail.getIPTVHBOPrepaidMonth() > 0) {
                        lblIPTVHBOChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVHBOChargeTimes()));
                        lblIPTVHBOPrepaidMonthCount.setText(String.valueOf(modelDetail.getIPTVHBOPrepaidMonth()));
                    } else {
                        lblIPTVHBOChargeTimesCount.setText("1");
                        lblIPTVHBOPrepaidMonthCount.setText("0");
                    }
                } else {
                    lblIPTVHBOChargeTimesCount.setText("1");
                    lblIPTVHBOPrepaidMonthCount.setText("0");
                }
            }

        } else if (buttonView.getId() == R.id.chb_iptv_k_plus) {
            imgIPTVKPlusChargeTimeLess.setEnabled(isChecked);
            imgIPTVKPlusChargeTimePlus.setEnabled(isChecked);
            imgIPTVKPlusPrepaidMonthLess.setEnabled(isChecked);
            imgIPTVKPlusPrepaidMonthPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVKPlusChargeTimesCount.setText("0");
                lblIPTVKPlusPrepaidMonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVKPlusChargeTimes() > 0 || modelDetail.getIPTVKPlusPrepaidMonth() > 0) {
                        lblIPTVKPlusChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVKPlusChargeTimes()));
                        lblIPTVKPlusPrepaidMonthCount.setText(String.valueOf(modelDetail.getIPTVKPlusPrepaidMonth()));
                    } else {
                        lblIPTVKPlusChargeTimesCount.setText("1");
                        lblIPTVKPlusPrepaidMonthCount.setText("0");
                    }
                } else {
                    lblIPTVKPlusChargeTimesCount.setText("1");
                    lblIPTVKPlusPrepaidMonthCount.setText("0");
                }
            }
        }

        else if (buttonView.getId() == R.id.chb_iptv_vtv_cab) {
            imgIPTVVTVChargeTimeLess.setEnabled(isChecked);
            imgIPTVVTVChargeTimePlus.setEnabled(isChecked);
            imgIPTVVTVPrepaidMonthLess.setEnabled(isChecked);
            imgIPTVVTVPrepaidMonthPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVVTVChargeTimesCount.setText("0");
                lblIPTVVTVPrepaidMonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVVTVChargeTimes() > 0 || modelDetail.getIPTVVTVPrepaidMonth() > 0) {
                        lblIPTVVTVChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVVTVChargeTimes()));
                        lblIPTVVTVPrepaidMonthCount.setText(String.valueOf(modelDetail.getIPTVVTVPrepaidMonth()));
                    } else {
                        lblIPTVVTVChargeTimesCount.setText("1");
                        lblIPTVVTVPrepaidMonthCount.setText("0");
                    }
                } else {
                    lblIPTVVTVChargeTimesCount.setText("1");
                    lblIPTVVTVPrepaidMonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_iptv_fim_plus) {
            imgIPTVFimPlus_ChargeTimeLess.setEnabled(isChecked);
            imgIPTVFimPlus_ChargeTimePlus.setEnabled(isChecked);
            imgIPTVFimPlus_PrepaidMonthLess.setEnabled(isChecked);
            imgIPTVFimPlus_PrepaidMonthPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVFimPlus_ChargeTime.setText("0");
                lblIPTVFimPlus_MonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVFimPlusChargeTimes() > 0 || modelDetail.getIPTVFimPlusPrepaidMonth() > 0) {
                        lblIPTVFimPlus_ChargeTime.setText(String.valueOf(modelDetail.getIPTVFimPlusChargeTimes()));
                        lblIPTVFimPlus_MonthCount.setText(String.valueOf(modelDetail.getIPTVFimPlusPrepaidMonth()));
                    } else {
                        lblIPTVFimPlus_ChargeTime.setText("1");
                        lblIPTVFimPlus_MonthCount.setText("0");
                    }
                } else {
                    lblIPTVFimPlus_ChargeTime.setText("1");
                    lblIPTVFimPlus_MonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_iptv_fim_hot) {
            imgIPTVFimHot_ChargeTimeLess.setEnabled(isChecked);
            imgIPTVFimHot_ChargeTimePlus.setEnabled(isChecked);
            imgIPTVFimHot_PrepaidMonthLess.setEnabled(isChecked);
            imgIPTVFimHot_PrepaidMonthPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVFimHot_ChargeTime.setText("0");
                lblIPTVFimHot_MonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVFimHotChargeTimes() > 0 || modelDetail.getIPTVFimHotPrepaidMonth() > 0) {
                        lblIPTVFimHot_ChargeTime.setText(String.valueOf(modelDetail.getIPTVFimHotChargeTimes()));
                        lblIPTVFimHot_MonthCount.setText(String.valueOf(modelDetail.getIPTVFimHotPrepaidMonth()));
                    } else {
                        lblIPTVFimHot_ChargeTime.setText("1");
                        lblIPTVFimHot_MonthCount.setText("0");
                    }
                } else {
                    lblIPTVFimHot_ChargeTime.setText("1");
                    lblIPTVFimHot_MonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_ip_tv_fim_standard) {
            imgIPTVFimStandard_ChargeTimeLess.setEnabled(isChecked);
            imgIPTVFimStandard_ChargeTimePlus.setEnabled(isChecked);
            imgIPTVFimStandard_MonthCountLess.setEnabled(isChecked);
            imgIPTVFimStandard_MonthCountPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVFimStandard_ChargeTimes.setText("0");
                lblIPTVFimStandard_MonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVFimPlusStdChargeTimes() > 0 || modelDetail.getIPTVFimPlusStdPrepaidMonth() > 0) {
                        lblIPTVFimStandard_ChargeTimes.setText(String.valueOf(modelDetail.getIPTVFimPlusStdChargeTimes()));
                        lblIPTVFimStandard_MonthCount.setText(String.valueOf(modelDetail.getIPTVFimPlusStdPrepaidMonth()));
                    } else {
                        lblIPTVFimStandard_ChargeTimes.setText("1");
                        lblIPTVFimStandard_MonthCount.setText("0");
                    }
                } else {
                    lblIPTVFimStandard_ChargeTimes.setText("1");
                    lblIPTVFimStandard_MonthCount.setText("0");
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = getActivity().getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(getActivity(), message);
                return;
            }
        }
        if (requestCode == 2) {
            startScan();
        }
    }

    @Override
    public void onItemClick(Integer type, Device mDevice, Integer position) {
        switch (type) {
            case 0:
                mListDeviceSelected.remove(mDevice);
                mDeviceAdapter.notifyData(mListDeviceSelected);
                break;
            case 1:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.DEVICE, mDevice);
                bundle.putInt(Constants.POSITION, position);
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3.class.getSimpleName());
                Intent intent = new Intent(getActivity(), DevicePriceListActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_DEVICE_PRICE_LIST_SELECTED);
                break;
        }
    }
}
