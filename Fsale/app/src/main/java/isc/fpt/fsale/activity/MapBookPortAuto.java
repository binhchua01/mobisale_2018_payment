package isc.fpt.fsale.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

import fpt.isc.distance2pointslib.DistanceTwoPointsProvider;
import fpt.isc.distance2pointslib.services.DistanceLibsCallbackResult;
import isc.fpt.fsale.R;
import isc.fpt.fsale.action.AutoBookPort;
import isc.fpt.fsale.action.GetIPV4Action;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.action.RecoverRegistrationAction;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.action.GetGeocoder;
import isc.fpt.fsale.map.activity.BookPortManualActivity;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;

// màn hình BookPort Auto
public class MapBookPortAuto extends FragmentActivity implements OnMapReadyCallback {
    private Marker customerMarker;
    private GoogleMap mMap;
    private int zoomTD = 18, tryAgainGoogleApi;
    private int numberRetryConnect;
    private Location mapLocation, locationDevice;
    private RegistrationDetailModel mRegister;
    private Spinner spTDType;
    private Circle mapCircleLocationCustomize, mapCircleLocationDevice;
    private DistanceTwoPointsProvider distanceTwoPointsProvider;
    private Button btnLocationCustomize, btnLocationDevice, btnMapMode, btnBookPort, btnRecoverPort;
    private LatLng locationCustomize, locationCustomizeMoved;
    private ProgressDialog progressDialog;

    public MapBookPortAuto() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_book_port_auto);
        initView();
        initViewEvent();
        setUpMapIfNeeded();
        getDataFromIntent();
    }

    private void initViewEvent() {
        btnLocationCustomize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRegister != null) {
                    locationCustomize = null;
                    progressDialog = Common.showProgressBar(
                            MapBookPortAuto.this,
                            getResources().getString(R.string.title_process_dialog_find_address_customize)
                    );
                    tryAgainGoogleApi = 0;
                    setMarkerCurrentLocationCustomize(mRegister.getAddressOrigin());
                }
            }
        });
        btnLocationDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED
                            || checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION}, 2);
                    } else {
                        getCurrentLocation();
                    }
                } else {
                    getCurrentLocation();
                }
            }
        });

        btnBookPort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Location currentLocationDevice = getCurrentLocationDevice();
                int type = ((KeyValuePairModel) spTDType.getSelectedItem()).getID();
                String currentLocationDeviceValue = "";
                if (currentLocationDevice != null) {
                    currentLocationDeviceValue = currentLocationDevice.getLatitude() + ","
                            + currentLocationDevice.getLongitude();
                }
                if (locationCustomize != null) {
                    if (locationCustomizeMoved != null) {
                        if (Constants.AutoBookPort_iR == 0) {
                            if (mRegister != null && numberRetryConnect <= Constants.AutoBookPort_RetryConnect) {
                                numberRetryConnect++;
                                String locationCustomizedMovedValue = locationCustomizeMoved.latitude +
                                        "," + locationCustomizeMoved.longitude;
                                new AutoBookPort(MapBookPortAuto.this,
                                        Constants.USERNAME, mRegister.getRegCode(), type,
                                        locationCustomizedMovedValue, currentLocationDeviceValue);
                            }
                        } else {
                            float result = distanceTwoPointsProvider.distanceTwoLatLongPoints(
                                    locationCustomize.latitude,
                                    locationCustomize.longitude,
                                    locationCustomizeMoved.latitude,
                                    locationCustomizeMoved.longitude
                            );
                            if (result <= Constants.AutoBookPort_iR) {
                                if (mRegister != null && numberRetryConnect <= Constants.AutoBookPort_RetryConnect) {
                                    numberRetryConnect++;
                                    String locationCustomizedMovedValue =
                                            locationCustomizeMoved.latitude + "," + locationCustomizeMoved.longitude;
                                    new AutoBookPort(
                                            MapBookPortAuto.this,
                                            Constants.USERNAME,
                                            mRegister.getRegCode(),
                                            type,
                                            locationCustomizedMovedValue,
                                            currentLocationDeviceValue
                                    );
                                }
                            } else {
                                Common.alertDialog(
                                        getResources()
                                                .getString(R.string.title_message_box_position_move_marker_from_location_customize)
                                                + " " + Constants.AutoBookPort_iR + "m",
                                        MapBookPortAuto.this
                                );
                            }
                        }
                    } else {
                        if (numberRetryConnect <= Constants.AutoBookPort_RetryConnect) {
                            numberRetryConnect++;
                            String locationCustomizeSelected = locationCustomize.latitude + "," + locationCustomize.longitude;
                            new AutoBookPort(
                                    MapBookPortAuto.this,
                                    Constants.USERNAME,
                                    mRegister.getRegCode(),
                                    type,
                                    locationCustomizeSelected,
                                    currentLocationDeviceValue
                            );
                        }
                    }
                } else {
                    Common.alertDialog(
                            "Vị trí bookport không đúng, vui lòng chọn lại vị trí",
                            MapBookPortAuto.this
                    );
                }
            }
        });
        btnRecoverPort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmToReconvertPort();
            }
        });
        btnMapMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if current map mode is satellite: change to normal
                if (mMap.getMapType() == MAP_TYPE_SATELLITE) {
                    btnMapMode.setText(getResources().getString(R.string.lbl_satellite));
                    mMap.setMapType(MAP_TYPE_NORMAL);
                } else {
                    btnMapMode.setText(getResources().getString(R.string.lbl_map));
                    mMap.setMapType(MAP_TYPE_SATELLITE);
                }
            }
        });
    }

    private void initView() {
        numberRetryConnect = 0;
        distanceTwoPointsProvider = new DistanceTwoPointsProvider();
        spTDType = (Spinner) findViewById(R.id.sp_port_type);
        btnLocationCustomize = (Button) findViewById(R.id.btn_location_customize);
        btnLocationDevice = (Button) findViewById(R.id.btn_location_device);
        btnBookPort = (Button) findViewById(R.id.btn_book_port);
        btnRecoverPort = (Button) findViewById(R.id.btn_recover_port);
        btnMapMode = (Button) findViewById(R.id.btn_map_mode);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    // lấy thông tin chi tiết pdk
    public void getDetailRegister() {
        if (mRegister != null) {
            new GetRegistrationDetail(
                    MapBookPortAuto.this,
                    Constants.USERNAME,
                    mRegister.getID()
            );
        }
    }

    public void setNumberRetryConnect(int mNumberRetryConnect){
        numberRetryConnect = mNumberRetryConnect;
    }

    public int getNumberRetryConnect(){
        return numberRetryConnect;
    }

    // lấy thông tin phiếu đăng ký qua intent
    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            mRegister = intent.getParcelableExtra(Constants.MODEL_REGISTER);
        }
        initSpTDType();
    }

    // khởi tạo loại hạ tầng
    private void initSpTDType() {
        String groupPoint = "";
        if (mRegister != null) {
            groupPoint = mRegister.getTDName();
        }
        ArrayList<KeyValuePairModel> lstBookPortType = Constants.TypePortOfSale != null ?
                Constants.TypePortOfSale : new ArrayList<KeyValuePairModel>();
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this,
                R.layout.my_spinner_style, lstBookPortType, Gravity.LEFT);
        spTDType.setAdapter(adapter);
        if (groupPoint.equals("")) {
            // chọn mặc định ftth new
            for (int index = 0; index < lstBookPortType.size(); index++) {
                if (lstBookPortType.get(index).getDescription().equals("FTTHNew")) {
                    spTDType.setSelection(index);
                    break;
                }
            }
        } else {
            if (groupPoint.contains("/")) {
                spTDType.setSelection(lstBookPortType.size() - 1);
            }
        }
    }

    // chuyển về bookport bằng tay
    public void fowardToBookPortManual() {
        if (mRegister != null) {
            Intent intent = new Intent(MapBookPortAuto.this, BookPortManualActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.MODEL_REGISTER, mRegister);
            startActivity(intent);
            finish();
        }
    }

    // hiển thị port thành công lên màn hình
    public void drawMakerBookPortSuccess(String port, String message, String ODCCable) {
        if (port != null && port.contains(",")) {
            String[] arrPort = port.split(",");
            LatLng positionPort = new LatLng(Double.valueOf(arrPort[0].trim()), Double.valueOf(arrPort[1].trim()));
            drawMarkerOnMap(positionPort, ODCCable,
                    R.drawable.ic_marker_blue, true, null, true, zoomTD);
            Common.alertDialog(message, this);
        }
    }

    //   khởi tạo map
    private void setUpMapIfNeeded() {
        if (mMap != null)
            setUpMap();
        if (mMap == null) {
            SupportMapFragment mapFrag = (SupportMapFragment) this
                    .getSupportFragmentManager().findFragmentById(R.id.map);
            mapFrag.getMapAsync(this);
        }
    }

    // lấy vị trí hiện tại của thiết bị
    public Location getCurrentLocationDevice() {
        GPSTracker gps = new GPSTracker(this);
        Location location = gps.getLocation(true);
        if (location == null)
            location = gps.getLocation(false);
        if ((location == null && mapLocation != null))
            location = mapLocation;
        if (Common.distanceTwoLocation(location, mapLocation) > 10)
            location = mapLocation;
        return location;
    }

    // lấy vị trí hiện tại của thiết bị và vẽ lên bản đồ
    private void getCurrentLocation() {
        locationCustomize = null;
        if (mMap != null) {
            try {
                Location location = getCurrentLocationDevice();
                if (location != null) {
                    LatLng curLocation = new LatLng(
                            location.getLatitude(),
                            location.getLongitude()
                    );
                    if (customerMarker != null)
                        customerMarker.remove();
                    if (mapCircleLocationDevice != null) {
                        mapCircleLocationDevice.remove();
                    }
                    customerMarker = drawMarkerOnMap(
                            curLocation,
                            getString(R.string.title_cuslocation),
                            R.drawable.icon_home,
                            true,
                            null,
                            true,
                            zoomTD
                    );
                    locationCustomize = curLocation;
                    locationCustomizeMoved = null;
                    // nếu bán kính di chuyển marker quy định > 0
                    if (Constants.AutoBookPort_iR > 0) {
                        mapCircleLocationDevice = mMap.addCircle(new CircleOptions()
                                .center(customerMarker.getPosition())
                                .radius(Constants.AutoBookPort_iR)
                                .strokeColor(getResources().getColor(R.color.bg_chat_right_message)));
                    }
                    // di chuyển camera google map vào vị trí vẽ marker
                    moveZoomCamera(customerMarker, zoomTD);
                    btnLocationDevice.setBackgroundColor(getResources().getColor(R.color.red));
                    btnLocationCustomize.setBackgroundColor(getResources().getColor(R.color.blue_press));
                } else {
                    new GetGeocoder(this, Constants.LOCATION_NAME).execute();
                }
            } catch (Exception e) {
                MyApp myApp = ((MyApp) getApplicationContext());
                if (myApp != null) {
                    Crashlytics.logException(new Exception(Constants.USERNAME + "," + "không lấy được vị trí" + locationDevice.toString() + " thiết bị"));
                }
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    // vẽ vị trí từ thông tin địa chỉ phiếu đăng ký của khách hàng
    private void getCurrentLocationCustomize(LatLng locationCustomize) {
        if (customerMarker != null)
            customerMarker.remove();
        if (mapCircleLocationCustomize != null) {
            mapCircleLocationCustomize.remove();
        }
        customerMarker = drawMarkerOnMap(
                locationCustomize,
                getString(R.string.title_cuslocation),
                R.drawable.icon_home,
                true,
                null,
                true,
                zoomTD
        );
        locationCustomizeMoved = null;
        if (Constants.AutoBookPort_iR > 0) {
            mapCircleLocationCustomize = mMap.addCircle(new CircleOptions()
                    .center(customerMarker.getPosition())
                    .radius(Constants.AutoBookPort_iR)
                    .strokeColor(getResources().getColor(R.color.bg_chat_right_message)));
        }
        // di chuyển camera google map vào vị trí vẽ marker
        moveZoomCamera(customerMarker, zoomTD);
        btnLocationCustomize.setBackgroundColor(getResources().getColor(R.color.red));
        btnLocationDevice.setBackgroundColor(getResources().getColor(R.color.blue_press));
    }

    //cài đặt map
    private void setUpMap() {
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latlng) {

            }
        });
        // Di chuyển marker
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                locationCustomizeMoved = marker.getPosition();
            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }
        });
        // Khi nhấn vào marker
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return false;
            }
        });
        if (mMap != null) {
            if (mRegister != null) {
                progressDialog = Common.showProgressBar(
                        this,
                        getResources().getString(R.string.title_process_dialog_find_address_customize)
                );
                setMarkerCurrentLocationCustomize(mRegister.getAddressOrigin());
            }
        }
    }

    // hiện dialog thu hồi port đã bookport
    private void confirmToReconvertPort() {
        if (mRegister != null) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.title_notification))
                    .setMessage(getString(R.string.msg_confirm_recover_registration))
                    .setPositiveButton(R.string.lbl_ok,
                            new Dialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    recoveryPort();
                                    dialog.cancel();
                                }
                            })
                    .setNeutralButton(R.string.lbl_cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    // phương thức thu hồi port đã bookport
    private void recoveryPort() {
        if (mRegister != null) {
            GetIPV4Action NewIP = new GetIPV4Action();
            String IP = NewIP.GetIP();
            int type = 0;
            if (mRegister.getTDName() != null) {
                if (mRegister.getTDName().contains("/"))
                    type = 3;
                else {
                    KeyValuePairModel item = (KeyValuePairModel) spTDType
                            .getItemAtPosition(0);
                    type = item.getID() + 1;
                }
            }
            String[] paramsValue = new String[]{String.valueOf(type),
                    mRegister.getRegCode(), Constants.USERNAME, IP};
            new RecoverRegistrationAction(this, paramsValue);
            switch (type) {
                case 1:
                    onClickTracker(
                            "Thu hồi port ADSL",
                            getString(R.string.lbl_screen_name_dialog_book_port_adsl));
                    break;
                case 2:
                    onClickTracker(
                            "Thu hồi port FTTH",
                            getString(R.string.lbl_screen_name_dialog_book_port_ftth));
                    break;
                case 3:
                    onClickTracker(
                            "Thu hồi port FTTH New",
                            getString(R.string.lbl_screen_name_dialog_book_port_ftth_new));
                    break;
                default:
                    break;
            }
        }
    }

    private void onClickTracker(String label, String screenName) {
        try {
            // Get tracker.
            Tracker t = ((MyApp) this.getApplication())
                    .getTracker(MyApp.TrackerName.APP_TRACKER);
            // Set screen name.
            t.setScreenName(screenName + ".onClick()");
            // Send a screen view.
            t.send(new HitBuilders.ScreenViewBuilder().build());
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("BOOK_PORT_ONCLICK").setAction("onClick")
                    .setLabel(label).build());
            t.setScreenName(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Marker drawMarkerOnMap(LatLng latlng, String title, int dr,
                                   boolean isDraggable, String snippet, boolean isShowInfoWindow,
                                   int zoomSize) {
        Marker marker = null;
        try {
            marker = MapCommon.addMarkerOnMap(mMap, title, latlng, dr,
                    isDraggable);
            marker.setSnippet(snippet);
            if (isShowInfoWindow)
                marker.showInfoWindow();
            MapCommon.animeToLocation(mMap, latlng);
            MapCommon.setMapZoom(mMap, zoomSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return marker;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null)
            setUpMap();
        else {
            Toast.makeText(this, "Sorry! unable to create maps",
                    Toast.LENGTH_SHORT).show();
        }
    }

    // di chuyển và hướng google map vào marker
    public void moveZoomCamera(Marker location, int zoomTD) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(location.getPosition(), zoomTD);
        mMap.animateCamera(cameraUpdate);
    }

    public void setMarkerCurrentLocationCustomize(String address) {
        distanceTwoPointsProvider.searchAddressOnGoogleMapAPI(address, Constants.keyGoogleMap, new DistanceLibsCallbackResult() {
            @Override
            public void SuccessSearchGAPI(double latitude, double longitude) {
                locationCustomize = new LatLng(latitude, longitude);
                getCurrentLocationCustomize(locationCustomize);
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void SuccessDistanceAPI(double v, double v1, int i) {
            }

            @Override
            public void FailureSearchGAPI(String s) {
                if (tryAgainGoogleApi == 1) {
                    btnLocationCustomize.setBackgroundColor(getResources().getColor(R.color.red));
                    btnLocationDevice.setBackgroundColor(getResources().getColor(R.color.blue_press));
                    progressDialog.dismiss();
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(2.0f));
                    mMap.clear();
                    Common.alertDialog(getResources().getString(R.string.title_message_box_fail_connect_to_google_map), MapBookPortAuto.this);
                }
                if (tryAgainGoogleApi < 1) {
                    try {
                        Thread.sleep(1000);
                        setMarkerCurrentLocationCustomize(mRegister.getAddressOrigin());
                        tryAgainGoogleApi++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    // hiện thông báo cho sale biết bookport tự động không thành công chuyển sang bookport bằng tay
    public void showMessageAutoBookPortManual(Context mContext, String message) {
        try {
            AlertDialog.Builder builder;
            Dialog dialog;
            builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Thông Báo");
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    fowardToBookPortManual();
                                }
                            });
            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        getDetailRegister();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Check request permission array , if have once denied , return
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(this, message);
                return;
            }
        }
        if (requestCode == 2) {
            getCurrentLocation();
        }
    }
}