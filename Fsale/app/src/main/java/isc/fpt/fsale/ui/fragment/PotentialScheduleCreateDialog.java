package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.UpdatePotentialObjSchedule;
import isc.fpt.fsale.activity.PotentialObjDetailActivity;
import isc.fpt.fsale.activity.PotentialObjScheduleList;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.PotentialSchedule;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class PotentialScheduleCreateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private Context mContext;
    private PotentialScheduleCreateDialog potentialScheduleCreateDialog;
    // nút thoát
    private ImageButton imgClose;
    // edit nội dung hẹn và ngày hẹn
    private EditText edtContentSchedule, edtDateSchedule;
    // nút ngày chọn ngày hẹn và nút cập nhật 1 lịch hẹn
    private Button btnDateOfTimeSchedule, btnPotentialScheduleCreate;
    // chọn giờ hẹn
    private TimePicker timePicker;
    private DatePickerReportDialog mDateDialog;
    // model KHTN
    private PotentialObjModel mPotential;
    private int selectedPostion;
    private PotentialSchedule potentialSchedule;
    private SimpleDateFormat formatDate;
    private Date date;

    public PotentialScheduleCreateDialog() {
    }

    @SuppressLint("ValidFragment")
    public PotentialScheduleCreateDialog(Context mContext) {
        // TODO Auto-generated constructor stub
        this.mContext = mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_potential_schedule_create_dialog, container);
        try {
            Common.setupUI(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.potentialScheduleCreateDialog  = this;
        formatDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        if (getArguments() != null) {
            mPotential = getArguments().getParcelable(PotentialObjDetailActivity.TAG_POTENTIAL_OBJECT);
            selectedPostion = getArguments().getInt(PotentialObjScheduleList.TAG_SELECTED_POSITION, -1);
            potentialSchedule = getArguments().getParcelable(PotentialObjScheduleList.TAG_POTENTIAL_OBJ_SCHEDULE_OBJECT);
        }
        imgClose = (ImageButton) view.findViewById(R.id.btn_close);
        imgClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getDialog().dismiss();
            }
        });
        edtContentSchedule = (EditText) view.findViewById(R.id.edt_content_schedule);
        edtContentSchedule.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                edtContentSchedule.setError(null);
            }
        });
        timePicker = (TimePicker) view.findViewById(R.id.tpk_hour_of_time_schedule);
        edtDateSchedule = (EditText) view.findViewById(R.id.edt_date_schedule);
        btnDateOfTimeSchedule = (Button) view.findViewById(R.id.btn_date_of_time_schedule);
        if (selectedPostion != -1 && potentialSchedule != null) {
            edtContentSchedule.setText(potentialSchedule.getScheduleDescription());
            String scheduleDate = potentialSchedule.getScheduleDate();
            try {
                date = formatDate.parse(scheduleDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            StringTokenizer strToken = new StringTokenizer(potentialSchedule.getScheduleDate(), " ");
            while (strToken.hasMoreTokens()) {
                String temp = strToken.nextToken();
                edtDateSchedule.setText(temp);
                break;
            }
            timePicker.setCurrentHour(date.getHours());
            timePicker.setCurrentMinute(date.getMinutes());
        }
        btnDateOfTimeSchedule.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String title = getString(R.string.lbl_title_date_create_potential_schedule);
                initDatePickerDialog(edtDateSchedule, title);
                if (mDateDialog != null) {
                    mDateDialog.setCancelable(false);
                    mDateDialog.show(getFragmentManager(), "datePicker");
                }
            }
        });
        btnPotentialScheduleCreate = (Button) view.findViewById(R.id.btn_potential_schedule_create);
        btnPotentialScheduleCreate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkUpdatePotentialSchedule()) {
                    // TODO Auto-generated method stub
                    int potentialID = mPotential != null ? mPotential.getID() : 0;
                    String potentialObjScheduleID = potentialSchedule != null ? String.valueOf(potentialSchedule.getPotentialObjScheduleID()) : null;
                    String scheduleDescription = edtContentSchedule.getText().toString().trim().replace("\n","");
                    String supporter = ((MyApp) mContext.getApplicationContext()).getUserName();
                    int hour = timePicker.getCurrentHour();
                    String strHour = hour > 9 ? String.valueOf(hour) : "0" + hour;
                    int minute = timePicker.getCurrentMinute();
                    String strMinute = minute < 10 ? "0" + minute : String.valueOf(minute);
                    String scheduleDate = edtDateSchedule.getText().toString() + " " + strHour + ":" + strMinute + ":00";
                    Calendar calendar = Calendar.getInstance();
                    try {
                        Date sDate = formatDate.parse(scheduleDate);
                        calendar.setTime(sDate);
                        if (Calendar.getInstance().getTimeInMillis() < calendar.getTimeInMillis()) {
                            new UpdatePotentialObjSchedule(mContext, potentialID, supporter, potentialObjScheduleID, scheduleDescription, scheduleDate, selectedPostion, potentialSchedule,potentialScheduleCreateDialog);
                        } else {
                        Common.alertDialog(getResources().getString(R.string.msg_select_time_create_potential_schedule), mContext);
                    }
                    } catch (ParseException e) {
                        Common.alertDialog(getResources().getString(R.string.msg_select_time_format_incorrect), mContext);
                        e.printStackTrace();
                    }
                }
            }
        });
        setDateScheduleDefault();
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void initDatePickerDialog(EditText txtValue, String dialogTitle) {
        Calendar minDate = Calendar.getInstance(), starDate = Calendar.getInstance(), maxDate = Calendar.getInstance();
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        minDate.set(Calendar.MONTH, 0);
        minDate.add(Calendar.YEAR, -1);
        maxDate.add(Calendar.YEAR,2100);
        mDateDialog = new DatePickerReportDialog(this, starDate, minDate, maxDate, dialogTitle, txtValue);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear,
                          int dayOfMonth) {
        // TODO Auto-generated method stub
        Calendar date = Calendar.getInstance();
        date.set(year, monthOfYear, dayOfMonth);
        if (mDateDialog.getEditText() == edtDateSchedule && edtDateSchedule != null) {
            edtDateSchedule.setText(Common.getSimpleDateFormat(date,
                    Constants.DATE_FORMAT_VN));
        }
    }

    private void setDateScheduleDefault() {
        if (edtDateSchedule.getText().toString().equals("")) {
            Calendar date = Calendar.getInstance();
            String s = Common.getSimpleDateFormat(date, Constants.DATE_FORMAT_VN);
            edtDateSchedule.setText(s);
        }
    }

    public boolean checkUpdatePotentialSchedule() {
        if (edtContentSchedule.getText().toString() == null
                || edtContentSchedule.getText().toString().equals("")) {
            edtContentSchedule.requestFocus();
            edtContentSchedule.setError(getResources().getString(R.string.msg_emty_description_potential_schedule));
            return false;
        }
        String dateSelected = edtDateSchedule.getText().toString();
        if (dateSelected != null && dateSelected.length() > 0) {
            boolean checkDateSelected = Common.checkDateIsValid(mContext,dateSelected);
            if (!checkDateSelected) {
                Common.alertDialog(getResources().getString(R.string.msg_select_time_format_incorrect), mContext);
            }
            return checkDateSelected;
        }
        return true;
    }

}