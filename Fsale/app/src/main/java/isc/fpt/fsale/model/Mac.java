package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.lang.reflect.Field;

/**
 * Created by HCM.TUANTT14 on 1/23/2018.
 */

public class Mac implements Parcelable {
    private String MAC;
    private int OTTID;
    public Mac() {

    }
    public Mac(String MAC) {
        this.MAC = MAC;
    }

    public Mac(Parcel in) {
        this.MAC = in.readString();
        this.OTTID = in.readInt();
    }

    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public int getOTTID() {
        return OTTID;
    }

    public void setOTTID(int OTTID) {
        this.OTTID = OTTID;
    }

    public static final Creator<Mac> CREATOR = new Creator<Mac>() {
        @Override
        public Mac createFromParcel(Parcel in) {
            return new Mac(in);
        }

        @Override
        public Mac[] newArray(int size) {
            return new Mac[size];
        }
    };

    public JSONObject toJSONObject() throws Exception {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("MAC", this.MAC);
        } catch (Exception e) {

        }

        return jsonObj;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.MAC);
        parcel.writeInt(this.OTTID);
    }
    public String toString() {
        JSONObject jsonObj = new JSONObject();
        try {
            Field[] fields = getClass().getDeclaredFields();
            for (Field f : fields) {
                f.setAccessible(true);
                jsonObj.put(f.getName(), f.get(this));
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return jsonObj.toString();
    }
}
