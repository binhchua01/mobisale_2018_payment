package isc.fpt.fsale.model;

import java.lang.reflect.Field;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONObject;

public class ReportRegistrationModel {
	/* 
 	Du lieu WEB ServiceType
 	 
	RowNumber (INT)	  Số thứ tự
	TotalPage (INT)	  Tổng trang
	CurrentPage (INT)	  Trang hien tai
	TotalRow (INT)		  Tổng dòng
	SaleName (STRING)	  Account sale
	Complete	(INT)	  Đã lên hợp đồng
	InComplete (INT)	  Chưa lên hợp đồng

 */

	private String RowNumber;
	private Integer TotalPage;
	private Integer CurrentPage;
	private Integer TotalRow;
	
	private String SaleName;
	private Integer TotalComplete;
	private Integer TotalInComplete;
	private Integer Total;
	
	
	public String getRowNumber() {
		return RowNumber;
	}
	
	public void setRowNumber(String rowNumber) {
		RowNumber = rowNumber;
	}
	
	public Integer getTotalPage() {
		return TotalPage;
	}
	
	public void setTotalPage(Integer totalPage) {
		TotalPage = totalPage;
	}
	
	public Integer getCurrentPage() {
		return CurrentPage;
	}
	
	public void setCurrentPage(Integer currentPage) {
		CurrentPage = currentPage;
	}
	
	public Integer getTotalRow() {
		return TotalRow;
	}
	
	public void setTotalRow(Integer totalRow) {
		TotalRow = totalRow;
	}
	
	public String getSaleName() {
		return SaleName;
	}
	
	public void setSaleName(String SaleName) {
		this.SaleName = SaleName;
	}
	
	public Integer getTotalComplete() {
		return TotalComplete;
	}
	
	public void setTotalComplete(Integer TotalComplete) {
		this.TotalComplete = TotalComplete;
	}
	
	public Integer getTotalInComplete() {
		return TotalInComplete;
	}
	
	public void setTotalInComplete(Integer TotalInComplete) {
		this.TotalInComplete = TotalInComplete;
	}
	
	public Integer getTotal() {
		return Total;
	}
	
	public void setTotal(Integer Total) {
		this.Total = Total;
	}
	
	public ReportRegistrationModel()
	{}
	
	public ReportRegistrationModel(String _RowNumber,Integer _TotalPage,Integer _CurrentPage,Integer _TotalRow, 
				String _SaleName, Integer _TotalComplete,Integer _TotalInComplete,Integer _Total)
	{
		this.RowNumber=_RowNumber;
		this.TotalPage=_TotalPage;
		this.CurrentPage=_CurrentPage;
		this.TotalRow=_TotalRow;
		this.SaleName= _SaleName;
		this.TotalComplete= _TotalComplete;
		this.TotalInComplete= _TotalInComplete;
		this.Total= _Total;
		
	}
	
	////////////////////////////////////////
	public static ReportRegistrationModel Parse(JSONObject json)
	{  
		ReportRegistrationModel object = new ReportRegistrationModel();
		try {
				Field[] fields = object.getClass().getDeclaredFields();
				for (Field field : fields) {
					String propertiesName = field.getName();
					if(json.has(propertiesName)){
						field.setAccessible(true);
						if(field.getType().getSimpleName().equals("String"))    					
							field.set(object, json.getString(propertiesName));    					 
						else if(field.getType().getSimpleName().equals("Integer"))
							field.set(object, json.getInt(propertiesName));
					}
				}
				return object;
		} catch (Exception e) {

			e.printStackTrace();
			//String message = e.getMessage();
		}
		return null;
	}
	//////////////////////////////////////////////
	
	@Override
    public String toString() {
    	 StringBuilder sb = new StringBuilder();
    	 Field[] fields = getClass().getDeclaredFields();    	
    	  for (Field f : fields) {
    	    sb.append(f.getName());
    	    sb.append(": ");
    	    try {
    	    	sb.append("[");
				sb.append(f.get(this));
				sb.append("]");
			} catch (Exception e) {
				// TODO: handle exception

			}
    	    
    	    sb.append(", ");
    	    sb.append('\n');
    	  }
    	  return sb.toString();
    }

}
