package isc.fpt.fsale.activity.cus_type;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.extra_ott.adapter.KeyPairValueAdapter;
import isc.fpt.fsale.utils.Constants;

public class CusTypeActivity extends BaseActivitySecond implements OnItemClickListener<KeyValuePairModel> {
    private RelativeLayout rltBack;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_cus_type;
    }

    @Override
    protected void initView() {
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_cus_type);
        ArrayList<KeyValuePairModel> lst = new ArrayList<>();
        lst.add(new KeyValuePairModel(1, "Cá nhân Việt Nam"));
        lst.add(new KeyValuePairModel(2, "Cá nhân nước ngoài"));
        lst.add(new KeyValuePairModel(3, "Cơ quan Việt Nam"));
        lst.add(new KeyValuePairModel(4, "Cơ quan nước ngoài"));
        lst.add(new KeyValuePairModel(5, "Công ty tư nhân"));
        lst.add(new KeyValuePairModel(6, "Nhân viên FPT"));
        lst.add(new KeyValuePairModel(7, "Nhà nước"));
        lst.add(new KeyValuePairModel(8, "Nước ngoài"));
        lst.add(new KeyValuePairModel(9, "Công ty"));
        lst.add(new KeyValuePairModel(10, "Giáo dục"));
        lst.add(new KeyValuePairModel(11, "Nhân viên FTEL"));
        lst.add(new KeyValuePairModel(12, "Nhân viên tập đoàn FPT"));
        lst.add(new KeyValuePairModel(13, "Nhân viên TIN/PNC"));
        KeyPairValueAdapter mAdapter = new KeyPairValueAdapter(this, lst, this);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onItemClick(KeyValuePairModel object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.KEY_PAIR_OBJ, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
