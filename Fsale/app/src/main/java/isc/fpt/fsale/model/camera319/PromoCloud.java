package isc.fpt.fsale.model.camera319;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PromoCloud  implements Parcelable {
    private int PromoID;
    private String PromoName;
    private boolean isSelected = false;

    public PromoCloud(int promoID, String promoName) {
        PromoID = promoID;
        PromoName = promoName;
    }

    protected PromoCloud(Parcel in) {
        PromoID = in.readInt();
        PromoName = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(PromoID);
        dest.writeString(PromoName);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PromoCloud> CREATOR = new Creator<PromoCloud>() {
        @Override
        public PromoCloud createFromParcel(Parcel in) {
            return new PromoCloud(in);
        }

        @Override
        public PromoCloud[] newArray(int size) {
            return new PromoCloud[size];
        }
    };

    public String getPromoName() {
        return PromoName;
    }

    public void setPromoName(String promoName) {
        PromoName = promoName;
    }

    public PromoCloud() { }

    public int getPromoID() {
        return PromoID;
    }

    public void setPromoID(int promoID) {
        PromoID = promoID;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("PromoID", getPromoID());
            jsonObject.put("PromoName", getPromoName() != null ? getPromoName() : "");
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


}
