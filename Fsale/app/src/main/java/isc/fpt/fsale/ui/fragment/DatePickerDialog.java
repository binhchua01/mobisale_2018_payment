package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.AutoCreatePreCheckListActivity;
import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.activity.DeployAppointmentActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class DatePickerDialog extends DialogFragment {
    private int mDay, mMonth, mYear;
    private android.app.DatePickerDialog dialog;
    private Calendar mMaxDate, mMinDate;
    private int mTitleStrID;
    private String title;
    private FragmentCreatePotentialObjDetail fragmentCreatePotential;
    private TextView txtValue;
    private boolean isNotMaxDate;

    public DatePickerDialog() {
        super();
    }

    @SuppressLint("ValidFragment")
    public DatePickerDialog(boolean isNotMaxDate) {
        this.isNotMaxDate = isNotMaxDate;
    }

    @SuppressLint("ValidFragment")
    public DatePickerDialog(FragmentCreatePotentialObjDetail fragment, Calendar minDate, Calendar maxDate, int titleID) {
        this.fragmentCreatePotential = fragment;
        this.mMaxDate = maxDate;
        this.mMinDate = minDate;
        this.mTitleStrID = titleID;
    }

    @SuppressLint("ValidFragment")
    public DatePickerDialog(TextView txtValue, Calendar minDate, Calendar maxDate, String title) {
        this.txtValue = txtValue;
        this.mMaxDate = maxDate;
        this.mMinDate = minDate;
        this.title = title;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        Calendar c = new GregorianCalendar();//Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        // Create a new instance of DatePickerDialog and return it
        dialog = new android.app.DatePickerDialog(getActivity(), myDateListener, mYear, mMonth, mDay);
        setMinDate();
        if (!isNotMaxDate) {
            setMaxDate();
        }
        setTitle();
        //Set minDay
       /* if(mMinDate == null)
            dialog.getDatePicker().setMinDate((c.getTime().getTime() - 1000));
        else
        	dialog.getDatePicker().setMinDate(mMinDate.getTime().getTime() - 1000);
        //Set maxDate
        if(mMaxDate == null){
	        c.add(Calendar.DATE, 30);
	        dialog.getDatePicker().setMaxDate(c.getTime().getTime());
        }else{
        	dialog.getDatePicker().setMaxDate(mMaxDate.getTime().getTime());
        }
        dialog.setTitle(getString(R.string.lbl_deploy_appointment_dialog_title));*/
        return dialog;
    }

    private void setMinDate() {
        if (dialog != null) {
            if (mMinDate == null) {
                Calendar c = new GregorianCalendar();//Calendar.getInstance();
                dialog.getDatePicker().setMinDate((c.getTime().getTime() - 1000));
            } else
                dialog.getDatePicker().setMinDate(mMinDate.getTime().getTime() - 1000);
        }
    }

    private void setMaxDate() {
        if (dialog != null) {
            if (mMaxDate == null) {
                Calendar c = new GregorianCalendar();//Calendar.getInstance();
                c.add(Calendar.DATE, 30);
                dialog.getDatePicker().setMaxDate(c.getTime().getTime());
            } else {
                dialog.getDatePicker().setMaxDate(mMaxDate.getTime().getTime());
            }
        }
    }

    private void setTitle() {
        try {
            if (dialog != null) {
                if (mTitleStrID > 0) {
                    dialog.setTitle(getString(mTitleStrID));
                } else if (title == null || title.equals("")) {
                    dialog.setTitle(getString(R.string.lbl_deploy_appointment_dialog_title));
                } else
                    dialog.setTitle(title);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    private android.app.DatePickerDialog.OnDateSetListener myDateListener = new android.app.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (getActivity().getClass().getSimpleName().equals(DeployAppointmentActivity.class.getSimpleName())) {
                if (checkTime(year, month, day)) {
                    DeployAppointmentActivity activity = (DeployAppointmentActivity) getActivity();
                    if (activity != null)
                        activity.updateDateSpinner(year, month, day);
                } else {
                    Common.alertDialog(getString(R.string.msg_new_date_incorrect), getActivity());
                    DeployAppointmentActivity activity = (DeployAppointmentActivity) getActivity();
                    if (activity != null)
                        activity.updateDateSpinner(0, 0, 0);
                }
            } else if (getActivity().getClass().getSimpleName().equals(CreatePreChecklistActivity.class.getSimpleName())) {
                CreatePreChecklistActivity activity = (CreatePreChecklistActivity) getActivity();
                if (activity != null)
                    activity.updateDateSpinner(year, month, day);
            }
            else if (getActivity().getClass().getSimpleName().equals(AutoCreatePreCheckListActivity.class.getSimpleName())) {
                AutoCreatePreCheckListActivity activity = (AutoCreatePreCheckListActivity) getActivity();
                if (activity != null)
                    activity.updateDateSpinner(year, month, day);
            }else if (fragmentCreatePotential != null) {
                Calendar c = Calendar.getInstance();
                c.set(year, month, day);
//                if (mTitleStrID == R.string.lbl_isp_start_date_dialog_title) {
//                    fragmentCreatePotential.setISPStartDate(c);
//                } else if (mTitleStrID == R.string.lbl_isp_end_date_dialog_title) {
//                    fragmentCreatePotential.setISPEndDate(c);
//                }
            } else if (txtValue != null) {
                Calendar c = Calendar.getInstance();
                c.set(year, month, day);
                txtValue.setText(Common.convertCalendarToString(c, Constants.DATE_FORMAT));
            }
        }
    };

    @Override
    public void onDismiss(DialogInterface dialog) {
        try {
            this.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (getActivity().getClass().getSimpleName().equals(DeployAppointmentActivity.class.getSimpleName())) {
            DeployAppointmentActivity activity = (DeployAppointmentActivity) getActivity();
            if (activity != null) {
                activity.updateDateSpinner(0, 0, 0);
            }
        }
    }


    private boolean checkTime(int year, int month, int day) {
        Calendar currentDate = Calendar.getInstance();
        Calendar appointmentDate = Calendar.getInstance();
        appointmentDate.set(year, month, day);
        currentDate.set(mYear, mMonth, mDay);
        if (appointmentDate.compareTo(currentDate) >= 0)
            return true;

        return false;

    }

}
