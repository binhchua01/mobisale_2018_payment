package isc.fpt.fsale.ui.fpt_camera.cloud_detail.cloud_package;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetAllCloudPackage;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import static isc.fpt.fsale.utils.Constants.CLASS_NAME;
import static isc.fpt.fsale.utils.Constants.CLOUD_DETAIL;
import static isc.fpt.fsale.utils.Constants.COMBO;
import static isc.fpt.fsale.utils.Constants.LIST_CLOUD_DETAIL_SELECTED;

public class CloudPackageActivity extends BaseActivitySecond implements OnItemClickListener<CloudDetail> {
    private View loBack;
    private CloudPackageAdapter mAdapter;
    private List<CloudDetail> mList;
    private List<CloudDetail> mListSelected;
    private CloudDetail mCloudDetail;
    private int COMBO_TYPE = 0;
    private boolean isSellMore = false;

    @Override
    protected void onResume() {
        super.onResume();
        if(mCloudDetail != null){
            new GetAllCloudPackage(this, mCloudDetail, COMBO_TYPE, isSellMore);
        }
    }

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_cloud_package;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.title_cloud_package));
        RecyclerView mRecyclerViewCloudPackage = (RecyclerView) findViewById(R.id.recycler_view_cloud_package_list);
        getDataIntent();
        mList = new ArrayList<>();
        mAdapter = new CloudPackageAdapter(this, mList, this);
        mRecyclerViewCloudPackage.setAdapter(mAdapter);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if(bundle.getParcelable(CLOUD_DETAIL) != null){
                mCloudDetail = bundle.getParcelable(CLOUD_DETAIL);
                COMBO_TYPE = bundle.getInt(COMBO);
                String str = bundle.getString(CLASS_NAME);
                assert str != null;
                switch (str) {
                    case "FptCameraService":
                        isSellMore = false;
                        break;
                    case "FragmentFptCamera":
                        isSellMore = true;
                        break;
                    default:
                        break;
                }
            }
            if(bundle.getParcelableArrayList(LIST_CLOUD_DETAIL_SELECTED) != null)
                mListSelected = bundle.getParcelableArrayList(LIST_CLOUD_DETAIL_SELECTED);
        }
    }

    public void loadCloudPackage(List<CloudDetail> mList, CloudDetail mCloudDetail){
        List<CloudDetail> mListSelected = new ArrayList<>();
        if(this.mListSelected != null){
            for(CloudDetail item : this.mListSelected){
                if(item.getTypeID() == mCloudDetail.getTypeID())
                    mListSelected.add(item);
            }
        }
        for(CloudDetail item : mList){
            item.setTypeID(mCloudDetail.getTypeID());
            item.setTypeName(mCloudDetail.getTypeName());
            if(mCloudDetail.getPackID() == item.getPackID()){
                item.setSelected(true);
            }
            else{
                for(CloudDetail itemSelected : mListSelected){
                    if(itemSelected.getPackID() == item.getPackID()){
                        item.setSelected(true);
                        break;
                    }
                }
            }
        }
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(CloudDetail mCloudDetail) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(CLOUD_DETAIL, mCloudDetail);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
