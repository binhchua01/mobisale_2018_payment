package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haulc3 on 01,November,2019
 */
public class RPCodeInfo implements Parcelable {
    @SerializedName("Code")
    @Expose
    private String code;
    @SerializedName("Description")
    @Expose
    private String description;

    protected RPCodeInfo(Parcel in) {
        code = in.readString();
        description = in.readString();
    }

    public static final Creator<RPCodeInfo> CREATOR = new Creator<RPCodeInfo>() {
        @Override
        public RPCodeInfo createFromParcel(Parcel in) {
            return new RPCodeInfo(in);
        }

        @Override
        public RPCodeInfo[] newArray(int size) {
            return new RPCodeInfo[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(code);
        parcel.writeString(description);
    }
}
