package isc.fpt.fsale.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.view.Menu;

import com.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPriceCamTotal;
import isc.fpt.fsale.action.TotalPriceEquipment;
import isc.fpt.fsale.action.TotalPriceEquipmentCamera;
import isc.fpt.fsale.adapter.ViewPagerRegisterContractAdapter;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.PriceCamTotal;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.SellMoreCameraModel;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.ui.fpt_camera.model.CameraServiceItem;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.ui.fragment.FragmentFptCamera;
import isc.fpt.fsale.ui.fragment.FragmentFptCameraTotal;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractCamera;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractCusInfo;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractDevice;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractIPTV;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractInternet;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractMaxy;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractTotal;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class RegisterContractActivity extends BaseActivity implements OnPageChangeListener {
    private ViewPager viewPager;
    private ObjectDetailModel mObject = null;
    public RegistrationDetailModel mRegister;
    private int mCurTabPosition;
    private ViewPagerRegisterContractAdapter vpAdapter;
    public SellMoreCameraModel mRegisterCamera;
    private boolean isSellMoreCamera = false;
    private int serviceType = 2;
    public int isCusType = 0;
    int mPriceCamera, mPriceDevice;

    public RegisterContractActivity() {
        super(R.string.lbl_create_registration);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_register_contract_activity));
        setContentView(R.layout.activity_register_contract);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            e.printStackTrace();
        }
        viewPager = (ViewPager) findViewById(R.id.pager);
        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(Constants.TAG_CONTRACT)) {
                mObject = intent.getParcelableExtra(Constants.TAG_CONTRACT);
                setDataToObjCamera();
                isSellMoreCamera = false;
            }
            if (intent.hasExtra(Constants.MODEL_REGISTER)) {
                mRegister = intent.getParcelableExtra(Constants.MODEL_REGISTER);
                isSellMoreCamera = false;
            }
            //use for update sell more camera
            if (intent.getParcelableExtra(Constants.MODEL_REGISTER) != null) {
                mRegister = intent.getParcelableExtra(Constants.MODEL_REGISTER);
                isSellMoreCamera = true;
                setDataToObjCamera();
            }
        }
        initViewPager();
    }

    private void setDataToObjCamera() {
        mRegisterCamera = new SellMoreCameraModel();
        mRegisterCamera.setUserName(((MyApp) getApplicationContext()).getUserName());
        if (isSellMoreCamera) {
            //cập nhật pđk bán thêm
            mRegisterCamera.setRegCode(mRegister.getRegCode());
            mRegisterCamera.setContract(mRegister.getContract());
            mRegisterCamera.setEmail(mRegister.getEmail());
            mRegisterCamera.setPhone_1(mRegister.getPhone_1());
            mRegisterCamera.setContact_1(mRegister.getContact_1());
            mRegisterCamera.setPayment(mRegister.getPayment());
            mRegisterCamera.setLocalType(mRegister.getLocalType());
            mRegisterCamera.setLocationID(String.valueOf(mRegister.getLocationID()));
            mRegisterCamera.setObjectCamera(mRegister.getObjectCamera());
            mRegisterCamera.setObjID(mRegister.getObjID());
            mRegisterCamera.setCusType(mRegister.getCusType());
            mRegisterCamera.setBaseObjID(mRegister.getBaseObjID());
            mRegisterCamera.setBaseContract(mRegister.getBaseContract());
            mRegisterCamera.setSupporter(mRegister.getSupporter());
            mRegisterCamera.setDevice(mRegister.getListDevice());
            mRegisterCamera.setDeviceTotal(mRegister.getDeviceTotal());

        } else {
            //tạo pđk bán thêm
            mRegisterCamera.setRegCode("");
            mRegisterCamera.setContract(mObject.getContract());
            mRegisterCamera.setEmail(mObject.getEmail());
            mRegisterCamera.setPhone_1(mObject.getPhone_1());
            mRegisterCamera.setContact_1(mObject.getContact_1());
            mRegisterCamera.setPayment(0);
            mRegisterCamera.setLocalType(mObject.getLocalType());
            mRegisterCamera.setLocationID(String.valueOf(mObject.getLocationID()));
            mRegisterCamera.setObjID(mObject.getObjID());
            mRegisterCamera.setCusType(mObject.getCusType());
            mRegisterCamera.setBaseObjID(mObject.getBaseObjID());
            mRegisterCamera.setBaseContract(mObject.getBaseContract());
            mRegisterCamera.setSupporter(Constants.IBB_MEMBER_ID);
        }
    }

    public ObjectDetailModel getObject() {
        return mObject;
    }

    public void setObject(ObjectDetailModel object) {
        mObject = object;
    }

    public RegistrationDetailModel getRegister() {
        return mRegister;
    }

    public void setRegister(RegistrationDetailModel register) {
        this.mRegister = register;
    }

    public SellMoreCameraModel getRegisterCamera() {
        return mRegisterCamera;
    }

    public void setRegisterCamera(SellMoreCameraModel mRegisterCamera) {
        this.mRegisterCamera = mRegisterCamera;
    }
    public void setIsCusType(int value) {
        this.isCusType = value;
    }

    public int getIsCusType() {
        return isCusType;
    }

    private void initViewPager() {
        //modify by haulc3
        if (isSellMoreCamera) {
            if (mRegister != null && (mRegister.getCheckListService().isCamera() || mRegister.getCategoryServiceGroupID() == 3)) {
                serviceType = 5;
            }
        } else {
            if (mObject != null) {
                serviceType = mObject.getServiceType();
            }
        }
        viewPager.setOffscreenPageLimit(5);
        vpAdapter = new ViewPagerRegisterContractAdapter(getSupportFragmentManager(), this, serviceType);
        viewPager.setAdapter(vpAdapter);
        viewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        if (preventTab(mCurTabPosition, position, true)) {
            mCurTabPosition = position;
            int tabIdNew = vpAdapter.getTagIdByPosition(position);

            //validate fragment cus info
            if (tabIdNew == vpAdapter.getTAG_TAP_INTERNET()) {
                if (getCusInfoFragment().spTypePhone1 != null && getCusInfoFragment().spTypePhone1.getSelectedItemPosition() == 0) {
                    viewPager.setCurrentItem(0);
                    Common.alertDialog("Chưa nhập thông tin số điện thoại", this);
                    return;
                }

                if (getCusInfoFragment().txtPhone1 != null && !Common.isEmpty(getCusInfoFragment().txtPhone1) &&
                        !Common.checkPhoneValid(getCusInfoFragment().txtPhone1.getText().toString())) {
                    viewPager.setCurrentItem(0);
                    Common.alertDialog("Số điện thoại không hợp lệ", this);
                    return;
                }

                if (getCusInfoFragment().txtContact1 != null && Common.isEmpty(getCusInfoFragment().txtContact1)) {
                    viewPager.setCurrentItem(0);
                    Common.alertDialog("Chưa nhập người liên hệ", this);
                    return;
                }

                if (getCusInfoFragment().txtEmail != null && !Common.isEmpty(getCusInfoFragment().txtEmail) &&
                        !Common.checkMailValid(getCusInfoFragment().txtEmail.getText().toString())) {
                    viewPager.setCurrentItem(0);
                    Common.alertDialog("Email không hợp lệ", this);
                    return;
                }
            }

            //validate fragment ip tv
//            if (tabIdNew == vpAdapter.getTAG_TAP_DEVICE()) {
//                int boxCount = Integer.valueOf(getIPTVFragment().lblBoxCount.getText().toString());
//                if (boxCount > 0) {
//                    if (getIPTVFragment().spPackage.getSelectedItemPosition() == 0) {
//                        viewPager.setCurrentItem(2);
//                        Common.alertDialog("Chưa chọn gói dịch vụ", this);
//                        return;
//                    }
//
//                    if (getIPTVFragment().isInternetOnly) {
//                        if (getIPTVFragment().promotionIPTVModelBox1 == null) {
//                            viewPager.setCurrentItem(2);
//                            Common.alertDialog("Chưa chọn CLKM box 1", this);
//                            return;
//                        }
//                    }
//
//                    if (boxCount > 1) {
//                        if (getIPTVFragment().promotionIPTVModelBox2 == null) {
//                            viewPager.setCurrentItem(2);
//                            Common.alertDialog("Chưa chọn CLKM box 2", this);
//                            return;
//                        }
//                    }
//                }
//            }

            if (tabIdNew == vpAdapter.getTAG_TAP_TOTAL()) {
                FragmentRegisterContractTotal fragmentTotal = getTotalFragment();
                FragmentRegisterContractDevice fragDevice = getDeviceFragment();

                //tinh tong tien camera
                getCameraFragment().getCameraOfNetTotal(this);

                //tinh tong tien maxy
//                if(getMaxyFragment().getMaxySelect() != null)
                getMaxyFragment().getMaxyTotal(this);

                //Đăng ký thiết bị nhưng không chọn giá
                if (getDeviceFragment().getListDeviceSelect() != null &&
                        getDeviceFragment().getListDeviceSelect().size() != 0) {
                    for (Device item : getDeviceFragment().getListDeviceSelect()) {
                        if (item.getPriceID() == 0) {
                            viewPager.setCurrentItem(3);
                            Common.alertDialog("Chưa chọn giá thiết bị", this);
                            return;
                        }
                    }
                }



                //set lại tổng tiền thiết bị khi không chọn thiết bị
                if (fragDevice.getListDeviceSelect() == null
                        || fragDevice.getListDeviceSelect().size() == 0) {
                    fragmentTotal.loadDevicePrice(0);
                } else {
                    new TotalPriceEquipment(this, mRegister.getListDevice(), 1, false);
                }

                if (fragmentTotal != null) {
                    fragmentTotal.getRegisterFromActivity();
                } else {
                    Common.alertDialog("Không thể cập nhật tổng tiền", this);
                }
            }

            //check bán camera nhưng không chọn dịch vụ
            if (serviceType == 5) {
                //check list camera package - just use for sell camera
                if (viewPager.getCurrentItem() == 2) {
                    if (validationFragService()) {
                        new GetPriceCamTotal(this, true,
                                new ObjectCamera(Constants.USERNAME, getListCameraService()).toJSONObjectTotalPrice()
                        );
                        if (mRegisterCamera.getDevice() != null && mRegisterCamera.getDevice().size() != 0) {
                            new TotalPriceEquipmentCamera(this, mRegisterCamera.getDevice(), 1, false);
                        } else {
                            loadDeviceCameraPrice(0);
                        }
                    } else {
                        viewPager.setCurrentItem(1);
                    }
                }
            }
        } else {
            viewPager.setCurrentItem(mCurTabPosition);
        }
    }


    public List<CameraServiceItem> getListCameraService() {
        List<CameraServiceItem> mList = new ArrayList<>();
        if (getFptCameraFragment().mListCamera != null && getFptCameraFragment().mListCamera.size() != 0) {
            for (CameraDetail item : getFptCameraFragment().mListCamera) {
                mList.add(new CameraServiceItem(item.getServiceType(), item.getPackID(), item.getQty()));
            }
        }
        if (getFptCameraFragment().mListCloud != null && getFptCameraFragment().mListCloud.size() != 0) {
            for (CloudDetail item : getFptCameraFragment().mListCloud) {
                mList.add(new CameraServiceItem(item.getServiceType(), item.getPackID(), item.getQty()));
            }
        }
        if (getFptCameraFragment().mSetupDetail != null) {
            SetupDetail mSetupDetail = getFptCameraFragment().mSetupDetail;
            mList.add(new CameraServiceItem(mSetupDetail.getServiceType(), mSetupDetail.getPackID(), mSetupDetail.getQty()));
        }
        return mList;
    }


    private boolean validationFragService() {
        if (mRegisterCamera.getObjectCamera().getCameraDetail().size() == 0 &&
                mRegisterCamera.getObjectCamera().getCloudDetail().size() == 0 &&
                mRegisterCamera.getDevice() == null
        ) {
            if (getFptCameraFragment().mSetupDetail == null && getFptCameraFragment().rdoNoDeploy.isChecked()) {
                Common.alertDialog(getString(R.string.msg_select_service), this);
                return false;
            } else if (getFptCameraFragment().mSetupDetail.getQty() == 0 && getFptCameraFragment().rdoDeploy.isChecked()) {
                Common.alertDialog(getString(R.string.msg_select_quantity_deploy), this);
                return false;
            }
        }
        else {
            if (getFptCameraFragment().mListCamera.size() != 0) {
                for (CameraDetail item : getFptCameraFragment().mListCamera) {
                    if (TextUtils.isEmpty(item.getPackName())) {
                        Common.alertDialog(getString(R.string.msg_select_camera_package), this);
                        return false;
                    }
                }
            }
            if (getFptCameraFragment().mListCloud.size() != 0) {
                for (CloudDetail item : getFptCameraFragment().mListCloud) {
                    if (TextUtils.isEmpty(item.getPackName())) {
                        Common.alertDialog(getString(R.string.msg_select_cloud_package), this);
                        return false;
                    }
                }
            }

        }

        if (getFptCameraFragment().mListDevice.size() != 0 || getFptCameraFragment().mListDevice != null) {
            for (Device item : getFptCameraFragment().mListDevice) {
                if (TextUtils.isEmpty(item.getPriceText())) {
                    Common.alertDialog(getString(R.string.msg_select_device_contract), this);
                    return false;
                } else if (item.getNumber() == 0) {
                    Common.alertDialog(getString(R.string.msg_select_device_number), this);
                    return false;
                }
            }
        }
        return true;
    }


    public void loadFptCameraTotal(PriceCamTotal mPriceCamTotal) {
        mRegisterCamera.setCameraTotal(String.valueOf(mPriceCamTotal.getTotal()));
        //mRegisterCamera.setTotal(mPriceCamTotal.getTotal());
        mRegisterCamera.getObjectCamera().setSetupDetail(getFptCameraFragment().mSetupDetail);
        mPriceCamera = mPriceCamTotal.getTotal();
        getFptCameraFragmentTotal().tvCameraTotal.setText(
                String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(mPriceCamTotal.getTotal()))
        );
        onLoadTotalPrice();
    }

    public void loadDeviceCameraPrice(int amount) {
        mPriceDevice = amount;
        mRegisterCamera.setDeviceTotal(Double.parseDouble(String.valueOf(amount)));
        getFptCameraFragmentTotal().tvDeviceTotal.setText(
                String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(amount)));
        onLoadTotalPrice();
    }

    private void onLoadTotalPrice() {
        int mPriceTotal = mPriceCamera + mPriceDevice;
        mRegisterCamera.setTotal(mPriceTotal);
        getFptCameraFragmentTotal().tvTotalPrice.setText(String.format(getString(R.string.txt_unit_price),
                Common.formatNumber(mPriceTotal)));
        mRegisterCamera.setTotal(mPriceTotal);
    }


    public boolean checkForUpdate() {
        int count = vpAdapter.getCount();
        for (int i = 0; i < count; i++) {
            if (!preventTab(i, -100, false))
                return false;
        }
        if (mRegister == null) {
            Common.alertDialog("MobiSale không tạo được TTKH", this);
            return false;
        }
        return true;
    }

    private boolean preventTab(int curPosition, int newPosition, boolean isUpdateReg) {
        if (curPosition != newPosition) {
            int tabId = vpAdapter.getTagIdByPosition(curPosition);
            if (tabId == vpAdapter.getTAG_TAP_CUS_INFO()) {
                FragmentRegisterContractCusInfo fragment = getCusInfoFragment();
                if (fragment != null) {
                    if (isUpdateReg)
                        fragment.updateRegister();
                }
            }

            else if (tabId == vpAdapter.getTAG_TAP_INTERNET()) {
                FragmentRegisterContractInternet fragment = getInternetFragment();
                if (fragment != null) {
                    if (fragment.checkForUpdate()) {
                        if (isUpdateReg)
                            fragment.updateRegister();
                    } else
                        return false;
                }
            }

            else if (tabId == vpAdapter.getTAG_TAP_MAXY_TV()) {
                FragmentRegisterContractMaxy fragment = getMaxyFragment();
                if (fragment != null) {
                    if (isUpdateReg)
                        fragment.updateRegister();
                }
            }

            else if (tabId == vpAdapter.getTAG_TAP_INTERNET_CAMERA()) {
                FragmentRegisterContractCamera fragment = getCameraFragment();
                if (fragment != null) {
                    if (isUpdateReg)
                        fragment.updateRegister();
                }
            }

            else if (tabId == vpAdapter.getTAG_TAP_DEVICE()) {
                FragmentRegisterContractDevice fragment = getDeviceFragment();
                if (fragment != null) {
                    if (fragment.checkForUpdate()) {
                        fragment.updateRegister();
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /*
     * Lấy các TabFragment từ Adapter dựa vào ID của Tab
     */
    public FragmentRegisterContractCusInfo getCusInfoFragment() {
        try {
            Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_CUS_INFO());
            if (fragment != null)
                return (FragmentRegisterContractCusInfo) fragment;
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public FragmentRegisterContractInternet getInternetFragment() {
        try {
            Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_INTERNET());
            if (fragment != null)
                return (FragmentRegisterContractInternet) fragment;
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

//    public FragmentRegisterContractIPTV getIPTVFragment() {
//        try {
//            Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_IP_TV());
//            if (fragment != null)
//                return (FragmentRegisterContractIPTV) fragment;
//            return null;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

    public FragmentRegisterContractMaxy getMaxyFragment() {
        try {
            Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_MAXY_TV());
            if (fragment != null)
                return (FragmentRegisterContractMaxy) fragment;
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public FragmentRegisterContractCamera getCameraFragment() {
        try {
            Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_INTERNET_CAMERA());
            if (fragment != null)
                return (FragmentRegisterContractCamera) fragment;
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public FragmentRegisterContractTotal getTotalFragment() {
        try {
            Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_TOTAL());
            if (fragment != null)
                return (FragmentRegisterContractTotal) fragment;
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public FragmentRegisterContractDevice getDeviceFragment() {
        try {
            Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_DEVICE());
            if (fragment != null)
                return (FragmentRegisterContractDevice) fragment;
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public FragmentFptCamera getFptCameraFragment() {
        try {
            Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_FPT_CAMERA());
            if (fragment != null)
                return (FragmentFptCamera) fragment;
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public FragmentFptCameraTotal getFptCameraFragmentTotal() {
        try {
            Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_FPT_CAMERA_TOTAL());
            if (fragment != null)
                return (FragmentFptCameraTotal) fragment;
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // report activity start
    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    // report activity stop
    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.msg_confirm_to_back))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.lbl_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.lbl_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FragmentRegisterContractIPTV.isBind = 0;
    }


}
