package isc.fpt.fsale.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GCM_RegisterPushNotification;
import isc.fpt.fsale.activity.BaseActivity;
import isc.fpt.fsale.activity.LoginActivity;
import isc.fpt.fsale.adapter.FPTBoxAdapter;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.GiftBox;
import isc.fpt.fsale.model.ImageDocument;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ListGiftBoxOTTResult;
import isc.fpt.fsale.model.PaidType;
import isc.fpt.fsale.model.PromotionDeviceModel;
import isc.fpt.fsale.model.PromotionFPTBoxModel;
import isc.fpt.fsale.model.PromotionIPTVModel;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.ui.fragment.DatePickerDialog;
import isc.fpt.fsale.utils.MyApp.TrackerName;

/*
 * CLASS: Common
 *
 * @Description: common functions
 * @author: vandn (created on 06/08/2013)
 */
public class Common {
    private static final Common ourInstance = new Common();

    public static Common getInstance() {
        return ourInstance;
    }

    public static boolean isNullOrEmpty(String s) {
        if (s == null)
            return true;
        if (s.equals(""))
            return true;
        return s.trim().equals("");
    }


    public void changeActivity(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }

    public void changeActivity(Context context, Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(context, cls);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public void changeActivityFinish(Context context, Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(context, cls);
        intent.putExtras(bundle);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    /*
     * check device internet connection
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            for (NetworkInfo networkInfo : info) {
                if (networkInfo.getState() == NetworkInfo.State.CONNECTED
                        || networkInfo.getState() == NetworkInfo.State.CONNECTING) {
                    return true;
                }
            }
        }
        return false;
    }

    /*
     * show alert dialog
     */

    public static void alertDialogUpgrade(String html, Context mContext) {
        // kiểm tra trạng thái context
        if (checkLifeActivity(mContext)) {
            AlertDialog alertDialog = new AlertDialog.Builder(mContext)
                    .setTitle("Thông Báo")
                    .setMessage(html).setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ((Activity) mContext).finish();
                        }
                    })
                    .setCancelable(true)
                    .create();
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }
    }

    public static void alertDialog(String html, Context mContext) {
        // kiểm tra trạng thái context
        if (checkLifeActivity(mContext)) {
            AlertDialog alertDialog = new AlertDialog.Builder(mContext)
                    .setTitle("Thông Báo")
                    .setMessage(html).setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .setCancelable(true)
                    .create();
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }
    }

    // scroll view to Yposition
    public static void smoothScrollView(final ScrollView scrollView, final int Yposition) {
        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollTo(0, Yposition);
            }
        }, 100);

    }

    //Click on Ok and close Activity
    public static void alertDialogResult(String html, Context mContext) {
        new AlertDialog.Builder(mContext)
                .setTitle(R.string.title_notification)
                .setMessage(html)
                .setPositiveButton(R.string.lbl_ok,
                        new Dialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                .setCancelable(true)
                .create()
                .show();
    }

    public static void alertDialogNotTitle(String html, Context mContext) {
        // kiểm tra trạng thái context
        if (checkLifeActivity(mContext)) {
            new AlertDialog.Builder(mContext)
                    .setMessage(html)
                    .setPositiveButton(R.string.lbl_ok, null)
                    .setCancelable(true)
                    .create()
                    .show();
        }
    }

    // Hiển thị dialog chọn fpt Box và lấy câu lệnh khuyến mãi từ server
    public static AlertDialog getAlertDialogShowFPTBox(final Activity activity, String titleDialog,
                                                       final boolean[] isCheckedList, CharSequence[] dialogList,
                                                       final List<FPTBox> listDeviceSelect, final List<FPTBox> listDevice,
                                                       final LinearLayout frmListDevice, final FPTBoxAdapter fptBoxAdapter,
                                                       final boolean showMessage, final Button btnScanMac) {
        final AlertDialog.Builder builderDialog = new AlertDialog.Builder(activity);
        builderDialog.setTitle(titleDialog);
        builderDialog.setPositiveButton("Đồng ý", null);
        builderDialog.setMultiChoiceItems(dialogList, isCheckedList, new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialog, int whichButton, boolean isChecked) {
                ListView list = ((AlertDialog) dialog).getListView();
                for (int i = 0; i < list.getCount(); i++) {
                    boolean checked = list.isItemChecked(i);
                    isCheckedList[i] = checked;
                }
            }
        });
        final AlertDialog alert = builderDialog.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        boolean isCheckOne = false;
                        ListView list = alert.getListView();
                        listDeviceSelect.clear();
                        for (int i = 0; i < list.getCount(); i++) {
                            boolean checked = list.isItemChecked(i);
                            if (checked) {
                                if (listDevice.get(i).getOTTCount() == 0)
                                    listDevice.get(i).setOTTCount(1);
                                listDeviceSelect.add(listDevice.get(i));
                                isCheckOne = true;
                            } else {
                                listDevice.get(i).setOTTCount(0);
                            }
                        }
                        if (isCheckOne) {
                            frmListDevice.setVisibility(View.VISIBLE);
                            btnScanMac.setVisibility(View.VISIBLE);
                        } else {
                            frmListDevice.setVisibility(View.GONE);
                            btnScanMac.setVisibility(View.GONE);
                        }
                        fptBoxAdapter.notifyDataSetChanged();
                        if (!isCheckOne && showMessage) {
                            Common.alertDialog("Phải có ít nhất 1 FPT Box được chọn!", activity);
                        } else {
                            alert.dismiss();
                        }
                    }
                });
            }
        });
        return alert;
    }

    // lọc lại danh sách fpt box cho cập nhật phiếu đăng ký
    public static void getFPTBoxSelected(List<FPTBox> listDeviceSelected, List<FPTBox> listNewDevices, boolean[] booleanSetChecked) {
        for (int i = 0; i < listDeviceSelected.size(); i++) {
            for (int j = 0; j < listNewDevices.size(); j++) {
                if (listDeviceSelected.get(i).getOTTName().equals(listNewDevices.get(j).getOTTName())) {
                    listNewDevices.set(j, listDeviceSelected.get(i));
                    booleanSetChecked[j] = true;
                    break;
                }
            }
        }
    }

    // lấy vị trí của câu lệnh khuyến mãi iptv box 1, box 2 từ phiếu đăng ký
    public static PromotionIPTVModel getItemPromotionIPTVBox(ArrayList<PromotionIPTVModel> lst, int box, RegistrationDetailModel modelDetail) {
        PromotionIPTVModel promotionIPTVModel = null;
        for (PromotionIPTVModel item : lst) {
            int idPromotionBoxSelected = box == 1 ? modelDetail.getIPTVPromotionID() : modelDetail.getIPTVPromotionIDBoxOrder();
            if (item.getPromotionID() == idPromotionBoxSelected) {
                promotionIPTVModel = item;
                break;
            }
        }
        return promotionIPTVModel;
    }

    // generic list find equals value param
    public static int getIndex(List<?> list, String nameField, String compare) {
        int i = -1;
        order:
        for (int index = 0; index < list.size(); index++) {
            for (Field field : list.get(0).getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getName().equals(nameField)) {
                    try {
                        Object value = field.get(list.get(index));
                        String temp = (String) value;
                        if (temp != null) {
                            if (temp.equals(compare)) {
                                i = index;
                                break order;
                            }
                            break;
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return i;
    }


    // kiểm tra ngày tháng nhập vào từ tạo lịch hẹn nếu sai ví dụ: 45/46/2017
    public static boolean checkDateIsValid(Context mContext, String input) {
        boolean isValid = true;
        if (input.matches("[0-9]{2}/[0-9]{2}/[0-9]{4}")) {
            try {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date date = sdf.parse(input);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                String[] parts = input.split("/");
                // Check each part to make sure it matches the components of the date
                if (Integer.parseInt(parts[0]) != cal.get(Calendar.DATE)) {
                    isValid = false;
                }
                if (Integer.parseInt(parts[1]) != cal.get(Calendar.MONTH) + 1) {
                    isValid = false;
                }
                if (Integer.parseInt(parts[2]) != cal.get(Calendar.YEAR)) {
                    isValid = false;
                }

            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        } else {
            isValid = false;
        }
        return isValid;
    }

    public static String formatNumber(int number) {
        if (number < 1000) {
            return String.valueOf(number);
        }
        try {
            NumberFormat formatter = new DecimalFormat("###,###");
            String resp = formatter.format(number);
            resp = resp.replaceAll(",", ".");
            return resp;
        } catch (Exception e) {
            return "";
        }
    }

    public static String formatNumberNegative(int number) {
        if (number < 1000) {
            return String.valueOf(number);
        }
        try {
            NumberFormat formatter = new DecimalFormat("###,###");
            String resp = formatter.format(number);
            resp = resp.replaceAll(",", ".");
            return "-" + resp;
        } catch (Exception e) {
            return "";
        }
    }

    public static String formatNumber(double number) {
        if (number < 1000) {
            int d = 0;
            if (String.valueOf(number).equals("0.0")) {
                return String.valueOf(d);
            } else {
                return String.valueOf(number);
            }
        }
        try {
            NumberFormat formatter = new DecimalFormat("###,###");
            String resp = formatter.format(number);
            resp = resp.replaceAll(",", ".");
            return resp;
        } catch (Exception e) {
            return "";
        }
    }

    public static String formatNumber(String number) {
        try {
            int num = Integer.valueOf(number);
            if (num < 1000) {
                return String.valueOf(num);
            }
            NumberFormat formatter = new DecimalFormat("###,###");
            String resp = formatter.format(num);
            resp = resp.replaceAll(",", ".");
            return resp;
        } catch (Exception e) {
            return "";
        }
    }

    /*
     * show progress bar
     */
    public static ProgressDialog showProgressBar(Context mContext, String message) {
        // kiểm tra trạng thái context
        ProgressDialog progressDialog = null;
        if (!((Activity) mContext).isFinishing()) {
            progressDialog = ProgressDialog.show(mContext, "", message, true, false);
        }
        return progressDialog;
    }

    /*
     * get app version
     */
    public static String GetAppVersion(Context mContext) {
        try {
            PackageInfo packageInfo = mContext.getPackageManager()
                    .getPackageInfo(mContext.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }

    }

    /*
     * show fragment dialog
     */
    public static void showFragmentDialog(FragmentManager fm, DialogFragment fdialog, String tag) {
        try {
            fdialog.show(fm, tag);
            fdialog.setCancelable(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Log out
     */
    public static void Logout(final Context mContext) {
        try {
            AlertDialog.Builder builder;
            Dialog dialog;
            builder = new AlertDialog.Builder(mContext);
            builder.setMessage("Bạn có chắc muốn đăng xuất không ?")
                    .setCancelable(false)
                    .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                ((MyApp) mContext.getApplicationContext()).setHeaderRequest(null);
                                ((MyApp) mContext.getApplicationContext()).setIsLogIn(false);
                                ((MyApp) mContext.getApplicationContext()).setIsLogOut(true);
                                mContext.startActivity(new Intent(mContext, LoginActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                ((Activity) mContext).finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //khởi tạo dialog Bạn có chắc muốn đăng xuất không?
    public static void quitApp(final Context mContext) {
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Bạn có muốn thoát không ?")
                .setCancelable(false)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((Activity) mContext).finish();
                    }
                })
                .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        dialog = builder.create();
        dialog.show();
    }

    public static void LogoutWithoutConfirm(final Context mContext) {
        try {
            ((MyApp) mContext.getApplicationContext()).setHeaderRequest(null);
            mContext.startActivity(new Intent(mContext, LoginActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            ((MyApp) mContext.getApplicationContext()).setIsLogIn(false);
            ((MyApp) mContext.getApplicationContext()).setIsLogOut(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // kiểm tra độ dài ký tự EditText
    public static boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() <= 0;
    }

    public static String checkIsEmpty(String input) {
        if (input.trim().length() <= 0)
            input = "-1";
        return input;
    }

    /*
     * hide soft keyboard
     */
    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            if (view != null) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void hideSoftKeyboard(Context mContext) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) mContext
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = ((Activity) mContext).getCurrentFocus();
            if (view != null) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showSoftKeyboard(Context mContext) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) mContext
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(((Activity) mContext).getCurrentFocus(), InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showToast(Context mContext, String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    /*
     * save session:
     */
    public static void savePreference(Context mContext, String prefName, String key, String value) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                prefName, 0);// 0 is MOD_PRIVATE for the default operation
        sharedPreferences.edit().putString(key, value).commit();
    }

    public static void savePreference(Context mContext, String prefName, String key, boolean value) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                prefName, 0);// 0 is MOD_PRIVATE for the default operation
        sharedPreferences.edit().putBoolean(key, value).commit();
    }

    /*
     * load session:
     */
    public static String loadPreference(Context mContext, String prefName, String key) {
        String userPassword;
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(prefName, 0);
        userPassword = sharedPreferences.getString(key, "");
        return userPassword;
    }

    public static boolean loadPreferenceBoolean(Context mContext, String prefName, String key) {
        boolean result;
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(prefName, 0);
        result = sharedPreferences.getBoolean(key, false);
        return result;
    }

    public static boolean hasPreference(Context mContext, String prefName, String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(prefName, 0);
        return sharedPreferences.contains(key);
    }

    public static int getIndex(Spinner spinner, String value) {
        try {
            if (value != null) {
                int index;
                int count = spinner.getCount();
                if (!value.equals("")) {
                    for (int i = 0; i < count; i++) {
                        KeyValuePairModel selectedItem = (KeyValuePairModel) spinner.getItemAtPosition(i);
                        if (selectedItem.getsID().equalsIgnoreCase(value)) {
                            index = i;
                            return index;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static int getIndex(Spinner spinner, Integer value) {
        try {
            if (value != null) {
                int index;
                int count = spinner.getCount();
                for (int i = 0; i < count; i++) {
                    Object selectedItemDefault = spinner.getItemAtPosition(i);
                    if (selectedItemDefault instanceof KeyValuePairModel) {
                        KeyValuePairModel selectedItem = (KeyValuePairModel) selectedItemDefault;
                        if (selectedItem.getID() == value) {
                            index = i;
                            return index;
                        }
                    } else if (selectedItemDefault instanceof PromotionDeviceModel) {
                        PromotionDeviceModel selectedItem = (PromotionDeviceModel) selectedItemDefault;
                        if (selectedItem.getPromotionID() == value) {
                            index = i;
                            return index;
                        }
                    } else if (selectedItemDefault instanceof PromotionFPTBoxModel) {
                        PromotionFPTBoxModel selectedItem = (PromotionFPTBoxModel) selectedItemDefault;
                        if (selectedItem.getPromotionID() == value) {
                            index = i;
                            return index;
                        }
                    } else if (selectedItemDefault instanceof PaidType) {
                        PaidType selectedItem = (PaidType) selectedItemDefault;
                        if (selectedItem.getKey() == value) {
                            index = i;
                            return index;
                        }
                    } else if (selectedItemDefault instanceof GiftBox) {
                        GiftBox selectedItem = (GiftBox) selectedItemDefault;
                        if (selectedItem.getGiftID() == value) {
                            index = i;
                            return index;
                        }
                    } else if (selectedItemDefault instanceof ListGiftBoxOTTResult) {
                        ListGiftBoxOTTResult selectedItem = (ListGiftBoxOTTResult) selectedItemDefault;
                        if (selectedItem.getGiftID().equals(value)) {
                            index = i;
                            return index;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static int getIndexDesc(Spinner spinner, String desc) {
        try {
            if (desc != null) {
                int index;
                int count = spinner.getCount();
                if (!desc.equals("")) {
                    for (int i = 0; i < count; i++) {
                        KeyValuePairModel selectedItem = (KeyValuePairModel) spinner.getItemAtPosition(i);
                        if (selectedItem.getDescription().equalsIgnoreCase(desc)) {
                            index = i;
                            return index;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static Bitmap decodeSampledBitmapFromFile(String path) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Config.RGB_565;
        options.inDither = true;
        return BitmapFactory.decodeFile(path, options);
    }

    // Kiểm tra đối tượng jsonobject trả về của service có hợp lệ không
    public static boolean isEmptyJSONObject(JSONObject jsObj) {
        try {
            return jsObj == null;
        } catch (Exception ex) {
            return true;
        }
    }

    // Check session
    public static boolean checkSession(Context mContext) {
        boolean flag = true;
        if (mContext != null) {
            String className = mContext.getClass().getSimpleName();
            if (!className.equals(LoginActivity.class.getSimpleName())
                    && (Constants.USERNAME == null || Constants.USERNAME
                    .isEmpty())) {
                flag = false;
            }
        }
        return flag;
    }

    public static String convertVietNamToEnglishChar(String input) {
        AlterVietNamString converter = new AlterVietNamString();
        try {
            return converter.convertToEngListChar(input);
        } catch (Exception e) {
            return "";
        }
    }

    /*
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean checkMailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";// "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }

    public static boolean checkPhoneValid(String phone) {
        phone = phone.trim();
        if (phone.length() < 10 || phone.length() > 11)
            return false;
        else {
            try {
                if (phone.toCharArray()[0] == '0'
                        && phone.toCharArray()[1] != '0')
                    return true;
            } catch (Exception e) {

                return false;
            }
        }
        return false;
    }

    public static void initDaySpinner(Context mContext, Spinner spDay, int month, int year) {
        KeyValuePairAdapter adapterMonth;
        // Lấy tháng và năm hiện tại
        Calendar c = Calendar.getInstance();
        c.set(year, month - 1, 1);
        int dayOfMonths = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        // Thang Search
        ArrayList<KeyValuePairModel> ListSpMonth = new ArrayList<>();
        ListSpMonth.add(new KeyValuePairModel(0, mContext
                .getString(R.string.lbl_report_all_day)));
        for (int i = 1; i <= dayOfMonths; i++) {
            ListSpMonth.add(new KeyValuePairModel(i, String.valueOf(i)));
        }
        adapterMonth = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpMonth, Gravity.LEFT);
        spDay.setAdapter(adapterMonth);
    }

    public static void initMonthSpinner(Context mContext, final Spinner spMonth) {
        KeyValuePairAdapter adapterMonth;
        Calendar c = Calendar.getInstance();
        final int month = c.get(Calendar.MONTH);
        ArrayList<KeyValuePairModel> ListSpMonth = new ArrayList<>();
        for (int i = 1; i < 13; i++) {
            ListSpMonth.add(new KeyValuePairModel(i, String.valueOf(i)));
        }
        adapterMonth = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, ListSpMonth, Gravity.LEFT);
        spMonth.setAdapter(adapterMonth);
        // Tuấn cập nhật code ngày 18/07/2017 lỗi chọn spinner mặc định cho các api 4.4.4, 5.1.1
        spMonth.post(new Runnable() {
            @Override
            public void run() {
                spMonth.setSelection(Common.getIndex(spMonth, month + 1), true);
            }
        });
    }

    public static void initYearSpinner(Context mContext, final Spinner spYear) {
        Calendar c = Calendar.getInstance();
        final int year = c.get(Calendar.YEAR);
        ArrayList<KeyValuePairModel> ListSpYear = new ArrayList<>();
        ListSpYear.add(new KeyValuePairModel((year - 1), String
                .valueOf(year - 1)));
        ListSpYear.add(new KeyValuePairModel(year, String.valueOf(year)));
        KeyValuePairAdapter adapterYear = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, ListSpYear, Gravity.LEFT);
        spYear.setAdapter(adapterYear);
        // Tuấn cập nhật code ngày 18/07/2017 lỗi chọn spinner mặc định cho các api 4.4.4, 5.1.1
        spYear.post(new Runnable() {
            @Override
            public void run() {
                spYear.setSelection(Common.getIndex(spYear, year), true);
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    public static void setupUI(final Activity activity, View view) {
        try {
            // Set up touch listener for non-text box views to hide keyboard.
            if (!(view instanceof EditText)) {
                view.setOnTouchListener(new OnTouchListener() {

                    @SuppressLint("LongLogTag")
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        try {
                            hideSoftKeyboard(activity);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }

                });
            }
            // If a layout container, iterate over children and seed recursion.
            if (view instanceof ViewGroup) {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    View innerView = ((ViewGroup) view).getChildAt(i);
                    try {
                        setupUI(activity, innerView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void reportActivityCreate(Application application, String srceenName) {
        try {
            Tracker t = ((MyApp) application).getTracker(TrackerName.APP_TRACKER);
            t.setScreenName(srceenName);
            t.send(new HitBuilders.ScreenViewBuilder().setNewSession().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void reportActivityStart(Context context, Activity activity) {
        try {
            GoogleAnalytics.getInstance(context).reportActivityStart(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void reportActivityStop(Context context, Activity activity) {
        try {
            GoogleAnalytics.getInstance(context).reportActivityStop(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // đăng ký regId với GCM
    public static void RegGCM(final Context mContext, final boolean status, final int funcType) {
        new AsyncTask<Object, String, String>() {

            @Override
            protected void onProgressUpdate(String... values) {
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(String regId) {
                // đăng ký regid mới với server
                new GCM_RegisterPushNotification(mContext, regId, funcType);
            }

            @Override
            protected String doInBackground(Object... params) {
                String regId = null;
                try {
                    if (status) {
                        // hủy bỏ regid hiện tại với google
                        InstanceID.getInstance(mContext).deleteInstanceID();
                    }
                    // đăng ký regid mới với google
                    regId = InstanceID.getInstance(mContext).getToken(Constants.GOOGLE_SENDER_ID,
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return regId;
            }
        }.execute(null, null, null);
    }

    @SuppressLint("ClickableViewAccessibility")
    public static void setupUI(final View view) {
        try {
            if (!(view instanceof EditText)) {
                view.setOnTouchListener(new OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        try {
                            hideSoftKeyboard(view);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        return false;
                    }

                });
            }
            if (view instanceof ViewGroup) {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    View innerView = ((ViewGroup) view).getChildAt(i);
                    setupUI(innerView);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideSoftKeyboard(final View caller) {
        try {
            caller.postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) caller.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(caller.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }, 100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static float distanceTwoLocation(Location locationA, Location locationB) {
        if (locationA != null && locationB != null) {
            return locationA.distanceTo(locationB);
        }
        return 0;
    }

    @SuppressLint("SimpleDateFormat")
    public static String convertDateToString(Date date, String format) {
        SimpleDateFormat format1 = new SimpleDateFormat(format);
        return format1.format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public static Calendar convertToCalendar(String EndDate) {
        Calendar result = null;
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_VN);
        try {
            Date date;
            date = format.parse(EndDate);
            result = Calendar.getInstance();
            if (result != null) {
                result.setTime(date);
            }
        } catch (Exception e) {
            try {
                format = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
                Date date;
                date = format.parse(EndDate);
                result = Calendar.getInstance();
                if (result != null) {
                    result.setTime(date);
                }
            } catch (Exception e2) {
                try {
                    format = new SimpleDateFormat(
                            Constants.DATE_TIME_FORMAT_VN);
                    Date date;
                    date = format.parse(EndDate);
                    result = Calendar.getInstance();
                    result.setTime(date);
                } catch (Exception e3) {
                    e.printStackTrace();
                }
                e.printStackTrace();
            }
            e.printStackTrace();
        }
        return result;
    }

    @SuppressLint("SimpleDateFormat")
    public static String convertCalendarToString(Calendar cal, String format) {
        try {
            if (cal != null) {
                SimpleDateFormat format1 = new SimpleDateFormat(format);
                return format1.format(cal.getTime());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void dropDownNavigation(LinearLayout frm, ImageView imgNav) {
        try {
            if (frm.getVisibility() == View.VISIBLE) {
                frm.setVisibility(View.GONE);
                imgNav.setImageResource(R.drawable.ic_navigation_drop_up);
            } else {
                frm.setVisibility(View.VISIBLE);
                imgNav.setImageResource(R.drawable.ic_navigation_drop_down);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dropDownNavigation(LinearLayout frm, ImageView imgNav, boolean show) {
        try {
            if (!show) {
                frm.setVisibility(View.GONE);
                imgNav.setImageResource(R.drawable.ic_navigation_drop_up);
            } else {
                frm.setVisibility(View.VISIBLE);
                imgNav.setImageResource(R.drawable.ic_navigation_drop_down);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static String getSimpleDateFormat(Calendar cal, String formatStr) {
        if (cal != null) {
            SimpleDateFormat format = new SimpleDateFormat(formatStr);
            return format.format(cal.getTime());
        }
        return null;
    }

    @SuppressLint("SimpleDateFormat")
    public static String getSimpleDateFormat(String date, String formatStr) {
        if (date != null && !TextUtils.isEmpty(date)) {
            try {
                SimpleDateFormat dateFormatOriginal = new SimpleDateFormat("dd/MM/yyyy");
                Date myDate = dateFormatOriginal.parse(date);
                SimpleDateFormat format = new SimpleDateFormat(formatStr);
                return format.format(myDate.getTime());
            } catch (ParseException e) {
                SimpleDateFormat dateFormatOriginal2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
                Date myDate;
                try {
                    myDate = dateFormatOriginal2.parse(date);
                    SimpleDateFormat format = new SimpleDateFormat(formatStr);
                    return format.format(myDate.getTime());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return date;
    }

    @SuppressLint("SimpleDateFormat")
    public static String getSimpleDateFormatTwo(String date, String formatStr) {
        if (date != null && !TextUtils.isEmpty(date)) {
            SimpleDateFormat dateFormatOriginal2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
            Date myDate;
            try {
                myDate = dateFormatOriginal2.parse(date);
                SimpleDateFormat format = new SimpleDateFormat(formatStr);
                assert myDate != null;
                return format.format(myDate.getTime());
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }
        return date;
    }

    @SuppressLint("SimpleDateFormat")
    public static String getSimpleDateFormatThree(String date, String formatStr) {
        if (date != null && !TextUtils.isEmpty(date)) {
            SimpleDateFormat dateFormatOriginal2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
            Date myDate;
            try {
                myDate = dateFormatOriginal2.parse(date);
                SimpleDateFormat format = new SimpleDateFormat(formatStr);
                assert myDate != null;
                return format.format(myDate);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }
        return date;
    }

    public static String ScaleBitmap(Bitmap mBitmap) {
        String bitmapBase64 = "";
        try {
            int maxHeight = 560;
            int maxWidth = 720;
            float scale = Math.min(((float) maxHeight / mBitmap.getWidth()),
                    ((float) maxWidth / mBitmap.getHeight()));
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            Bitmap bitmap = Bitmap.createBitmap(mBitmap, 0, 0,
                    mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
            bitmapBase64 = getStringImage(bitmap);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmapBase64;
    }

    public static Bitmap scaleBitmapSmaller(File file) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inJustDecodeBounds = true; // obtain the size of the image, without loading it in memory
        BitmapFactory.decodeFile(file.getAbsolutePath(), bitmapOptions);
        int desiredWidth = 720;
        int desiredHeight = 1280;
        if (bitmapOptions.outWidth > bitmapOptions.outHeight) {
            desiredWidth = 1280;
            desiredHeight = 720;
        }
        float widthScale = (float) bitmapOptions.outWidth / desiredWidth;
        float heightScale = (float) bitmapOptions.outHeight / desiredHeight;
        float scale = Math.min(widthScale, heightScale);
        int sampleSize = 1;
        while (sampleSize < scale) {
            sampleSize += 1;
        }
        bitmapOptions.inSampleSize = sampleSize;
        bitmapOptions.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bitmapOptions);
        return bitmap;
    }

    public static String getStringImage(Bitmap mBitmap) {
        String encodedImage;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        encodedImage = Base64.encodeToString(imageBytes, Base64.NO_WRAP);
        return encodedImage;
    }

    /*
     * @category Function
     * @FUNCTION String to Bitmap
     */
    public static Bitmap StringToBitMap(String image) {
        Bitmap bitmap = null;
        try {
            byte[] decodedByte = Base64.decode(image, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static int indexOf(ArrayList<PromotionModel> lst, int value) {
        for (int i = 0; i < lst.size(); i++) {
            PromotionModel item = lst.get(i);
            if (item.getPromotionID() == value)
                return i;
        }
        return -1;
    }

    public static boolean jsonObjectValidate(String json) {
        if (json != null && !json.equals("")) {
            try {
                new JSONObject(json);
            } catch (JSONException ex) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public static String getRealPathUrlImage(Context context, Uri contentUri) {
        String result = "";
        try {
            if (Build.VERSION.SDK_INT >= 19) {
                result = getRealPathFromURI_API19(context, contentUri);
            } else {
                result = getRealPathFromURI_API11to18(context, contentUri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @SuppressLint("NewApi")
    private static String getPath2(Context context, Uri uri) {
        String path = "";
        try {
            if (uri == null) {
                return null;
            }
            Cursor cursor = context.getContentResolver().query(uri, null, null,
                    null, null);
            if (cursor == null) {
                path = uri.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor
                        .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    @SuppressLint("NewApi")
    private static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            String id = wholeID.split(":")[1];
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = context.getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel,
                    new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = getPath2(context, uri);
        }
        return filePath;
    }

    private static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String filePath = "";
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader cursorLoader = new CursorLoader(context, contentUri,
                    proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();
            if (cursor != null) {
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                filePath = cursor.getString(column_index);
            }
        } catch (Exception e) {
            filePath = getPath2(context, contentUri);
        }
        return filePath;
    }

    // kiểm tra trạng thái notification
    public static boolean checkStatusNotification(Context context) {
        return NotificationManagerCompat.from(context).areNotificationsEnabled();
    }

    public static boolean checkLifeActivity(Context context) {
        Activity activity = (Activity) context;
        if (activity != null) {
            if (Build.VERSION.SDK_INT >= 17) {
                return !activity.isDestroyed();
            } else {
                return !activity.isFinishing();
            }
        }
        return true;
    }

    //chuyển đổi ký tự sau về trước có chứa ký tự đặc biệt
    public static String reverseString(String strInput, String symboy) {
        String strDes = "";
        StringTokenizer strToken = new StringTokenizer(strInput, symboy);
        ArrayList<String> textElements = new ArrayList<>();
        while (strToken.hasMoreTokens()) {
            textElements.add(strToken.nextToken());
        }
        for (int index = textElements.size() - 1; index >= 0; index--) {
            if (strDes.length() > 0) {
                strDes = strDes.concat(symboy);
            }
            strDes = strDes.concat(textElements.get(index));
        }
        return strDes;
    }

    //hiển thị Calendar chọn ngày
    public static void showTimePickerDialog(BaseActivity activity, boolean isNotMaxDate) {
        DialogFragment newFragment = new DatePickerDialog(isNotMaxDate);
        newFragment.setCancelable(false);
        newFragment.show(activity.getSupportFragmentManager(), "datePicker");
    }

    // Vùng thuật toán upload nhiều ảnh hồ sơ khách hàng
    //tạo 1 list ảnh từ một map ảnh
    public static ArrayList<ImageDocument> listRestImageDocument(HashMap<Integer, List<ImageDocument>> mapListDocument) {
        Collection<List<ImageDocument>> listImageDocument = mapListDocument.values();
        Iterator<List<ImageDocument>> iteratorListImageDocument = listImageDocument.iterator();
        ArrayList<ImageDocument> listRestImageDocument = new ArrayList<ImageDocument>();
        while (iteratorListImageDocument.hasNext()) {
            listRestImageDocument.addAll(iteratorListImageDocument.next());
        }
        return listRestImageDocument;
    }

    // tạo 1 map mới chứa trong 1 key có 2 phần tử ảnh trong danh sách List<ImageDocument>
    public static HashMap<Integer, List<ImageDocument>> mapListDocumentRest(List<ImageDocument> listRestImageDocument) {
        int sizeOfList = 2;
        int sizeOfMap = (listRestImageDocument.size() + 1) / sizeOfList;
        @SuppressLint("UseSparseArrays")
        HashMap<Integer, List<ImageDocument>> mapListDocumentRest = new HashMap<>();
        int indexList = 0;
        for (int key = 0; key < sizeOfMap; key++) {
            List<ImageDocument> listDocument = new ArrayList<>();
            for (int index = 0; index < 2; index++) {
                listDocument.add(listRestImageDocument.get(indexList));
                indexList++;
                if (indexList == listRestImageDocument.size())
                    break;
            }
            mapListDocumentRest.put(key, listDocument);
        }
        return mapListDocumentRest;
    }

    //lưu trữ ảnh tại cache thiết bị
    public static String getPathCacheImageFile(Context context, Bitmap bitmap) {
        if (bitmap != null) {
            long name = System.currentTimeMillis();
            String fileName = name + ".png";
            File f = new File(context.getCacheDir(), fileName);
            try {
                f.createNewFile();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
                byte[] bitmapData = bos.toByteArray();
                FileOutputStream fos = new FileOutputStream(f);
                fos.write(bitmapData);
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return f.getAbsoluteFile().toString();
        } else {
            return null;
        }
    }

    // xóa toàn bộ file tạm trong cache thiết bị
    public static void clearAllFilesCache(Context context) {
        File[] directory = context.getCacheDir().listFiles();
        if (directory != null) {
            for (File file : directory) {
                file.delete();
            }
        }
    }

    // hiển thị actionbar màn hình
    public static void showScreenActionBar(AppCompatActivity appCompatActivity) {
        try {
            ActionBar actionBar = appCompatActivity.getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(appCompatActivity.getResources().getDrawable(R.color.main_color_blue_new));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // chuyển đổi string sang list đối tượng hình ảnh
    public static ArrayList<ImageDocument> covertStringToListImageDocument(String strInput) {
        ArrayList<ImageDocument> listImageDocument = new ArrayList<>();
        if (strInput != null) {
            if (strInput.contains(",")) {
                StringTokenizer stElements = new StringTokenizer(strInput, ",");
                while (stElements.hasMoreElements()) {
                    listImageDocument.add(new ImageDocument(stElements.nextToken()));
                }
            } else {
                try {
                    listImageDocument.add(new ImageDocument(strInput));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return listImageDocument;
    }

    // chuyển đổi list ảnh sang string hình ảnh
    public static String covertListImageDocumentToString(ArrayList<ImageDocument> listImageDocument, String symBoy) {
        String strListImageDocument = "";
        if (listImageDocument != null) {
            for (int index = 0; index < listImageDocument.size(); index++) {
                strListImageDocument = strListImageDocument.concat(String.valueOf(listImageDocument.get(index).getID()));
                if (index < listImageDocument.size() - 1) {
                    strListImageDocument = strListImageDocument.concat(symBoy);
                }
            }
        }
        return strListImageDocument;
    }

    // hàm chụp ảnh
    public static File capture(Activity activity) {
        File tempFile = null;
        try {
            @SuppressLint("SimpleDateFormat")
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String fileName = "CMND_" + timeStamp + ".jpg";
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/Mobisale Images/");
            myDir.mkdirs();
            tempFile = new File(myDir, fileName);
            if (!tempFile.exists()) {
                tempFile = new File(myDir, fileName);
            }
            Uri selectedImageUri = Uri.fromFile(tempFile);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
            activity.startActivityForResult(cameraIntent, 0);
        } catch (Exception e) {
            Common.alertDialog(e.getMessage(), activity);
        }
        return tempFile;
    }

    // create file path
    public static File capture() {
        File tempFile = null;
        try {
            @SuppressLint("SimpleDateFormat")
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String fileName = "CMND_" + timeStamp + ".jpg";
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/Mobisale Images/");
            myDir.mkdirs();
            tempFile = new File(myDir, fileName);
            if (!tempFile.exists()) {
                tempFile = new File(myDir, fileName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tempFile;
    }

    // Xoay ảnh khi ảnh tự động xoay sau khi chụp ảnh từ camera
    public static Bitmap rotateBitmap(Bitmap bitmap, String path) {
        Bitmap rotatedBitmap;
        try {
            ExifInterface ei = new ExifInterface(path);

            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateBitmap(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateBitmap(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateBitmap(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return bitmap;
        }

        return rotatedBitmap;
    }

    private static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    // Sử dụng để show thông báo không hiển thị button
    public void showPopup(Context mContext, String message) {
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(mContext);
        builder.setMessage(message).setCancelable(false).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        dialog = builder.create();
        dialog.show();
    }

    //money parse VND
    public static String formatMoney(String string) {
        NumberFormat format =
                new DecimalFormat("#,##0.00");// #,##0.00 ¤ (¤:// Currency symbol)
        format.setCurrency(Currency.getInstance(Locale.US));//Or default locale

        string = (!TextUtils.isEmpty(string)) ? string : "0";
        string = string.trim();
        string = format.format(Double.parseDouble(string));
        string = string.replaceAll(",", "\\.");

        if (string.endsWith(".00")) {
            int centsIndex = string.lastIndexOf(".00");
            if (centsIndex != -1) {
                string = string.substring(0, centsIndex);
            }
        }
        string = String.format("%s VND", string);
        return string;
    }
}

// Add by: DuHK
// Convert VietNam char to Englist char
class AlterVietNamString {

    private final char[] charA = {'à', 'á', 'ạ', 'ả', 'ã', 'â', 'ầ', 'ấ', 'ậ',
            'ẩ', 'ẫ', 'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ'}; // 0->16
    private final char[] charE = {'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ', 'è', 'é', 'ẹ',
            'ẻ', 'ẽ'}; // 17->27
    private final char[] charI = {'ì', 'í', 'ị', 'ỉ', 'ĩ'}; // 28->32
    private final char[] charO = {'ò', 'ó', 'ọ', 'ỏ', 'õ', 'ô', 'ồ', 'ố', 'ộ',
            'ổ', 'ỗ', 'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ'}; // 33->49
    private final char[] charU = {'ù', 'ú', 'ụ', 'ủ', 'ũ', 'ư', 'ừ', 'ứ', 'ự',
            'ử', 'ữ'}; // 50->60
    private final char[] charY = {'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ'}; // 61->65
    private final char[] charD = {'đ'}; // 66->67
    private final String mConditionStr = String.valueOf(charA, 0, charA.length)
            + String.valueOf(charE, 0, charE.length)
            + String.valueOf(charI, 0, charI.length)
            + String.valueOf(charO, 0, charO.length)
            + String.valueOf(charU, 0, charU.length)
            + String.valueOf(charY, 0, charY.length)
            + String.valueOf(charD, 0, charD.length);

    AlterVietNamString() {

    }

    String convertToEngListChar(String input) {
        char[] temp = new char[1];
        if (input != null)
            temp = input.toLowerCase().toCharArray();
        boolean isUpper;
        for (int i = 0; i < temp.length; i++) {
            int index = isVietNamchar(temp[i]);
            if (index >= 0) {
                isUpper = Character.isUpperCase(input.charAt(i));
                if (index <= 16) {
                    if (isUpper)
                        temp[i] = 'A';
                    else
                        temp[i] = 'a';
                }
                if (index >= 17 && index <= 27) {
                    if (isUpper)
                        temp[i] = 'E';
                    else
                        temp[i] = 'e';
                }
                if (index >= 28 && index <= 32) {
                    if (isUpper)
                        temp[i] = 'I';
                    else
                        temp[i] = 'i';
                }
                if (index >= 33 && index <= 49) {
                    if (isUpper)
                        temp[i] = 'O';
                    else
                        temp[i] = 'o';
                }
                if (index >= 50 && index <= 60) {
                    if (isUpper)
                        temp[i] = 'U';
                    else
                        temp[i] = 'u';
                }
                if (index >= 61 && index <= 65) {
                    if (isUpper)
                        temp[i] = 'Y';
                    else
                        temp[i] = 'y';
                }
                if (index == 66) {
                    if (isUpper)
                        temp[i] = 'D';
                    else
                        temp[i] = 'd';
                }
            }
        }
        return String.copyValueOf(temp);
    }

    private int isVietNamchar(char iChar) {
        int i, maxLength = mConditionStr.length();
        for (i = 0; i < maxLength && iChar != mConditionStr.charAt(i); i++) {
        }
        if (iChar != mConditionStr.charAt(maxLength - 1))
            i++;
        if (i <= mConditionStr.length()) {
            return i;
        }
        return -1;
    }

}

