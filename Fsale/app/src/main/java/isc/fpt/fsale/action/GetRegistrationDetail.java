package isc.fpt.fsale.action;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ConfirmPaymentActivity;
import isc.fpt.fsale.activity.DeployAppointmentActivity;
import isc.fpt.fsale.activity.DepositActivity;
import isc.fpt.fsale.activity.DepositRegisterContractActivity;
import isc.fpt.fsale.activity.InvestiageActivity;
import isc.fpt.fsale.activity.PaymentRetailAdditionalActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.activity.RegisterDetailActivity;
import isc.fpt.fsale.activity.UpdateInternetReceiptActivity;
import isc.fpt.fsale.map.activity.BookPortManualActivity;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.extra_ott.ExtraOttActivity;
import isc.fpt.fsale.ui.fpt_camera.FptCameraActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// API lấy chi tiết thông tin phiếu đăng ký
public class GetRegistrationDetail implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private final String TAG_GET_REGISTRATION = "GetRegistrationDetail";
    private ObjectDetailModel mObject;
    public static String[] paramNames = new String[]{"UserName", "ID", "RegCode"};
    private boolean istoDetail;

    public GetRegistrationDetail(Context mContext, String sUsername, int RegId) {
        this.mContext = mContext;
        String[] paramValues = new String[]{sUsername, String.valueOf(RegId), null};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_registration);
        CallServiceTask service = new CallServiceTask(mContext, TAG_GET_REGISTRATION, paramNames,
                paramValues, Services.JSON_POST, message, GetRegistrationDetail.this);
        service.execute();
    }

    // về chi tiết pdk từ màn hình thanh toán online bán thêm
    public GetRegistrationDetail(Context mContext, String sUsername, int RegId, boolean isToDetail) {
        this.mContext = mContext;
        this.istoDetail = isToDetail;
        String[] paramValues = new String[]{sUsername, String.valueOf(RegId), null};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_registration);
        CallServiceTask service = new CallServiceTask(mContext, TAG_GET_REGISTRATION, paramNames,
                paramValues, Services.JSON_POST, message, GetRegistrationDetail.this);
        service.execute();
    }

    public GetRegistrationDetail(Context mContext, String sUsername, String RegCode) {
        this.mContext = mContext;
        String[] paramValues = new String[]{sUsername, RegCode, "0"};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_registration);
        CallServiceTask service = new CallServiceTask(mContext, TAG_GET_REGISTRATION, paramNames,
                paramValues, Services.JSON_POST, message, GetRegistrationDetail.this);
        service.execute();
    }

    public GetRegistrationDetail(Context mContext, String sUsername, String RegCode, boolean isRegCode) {
        this.mContext = mContext;
        String[] paramValues = new String[]{sUsername, null, RegCode};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_registration);
        CallServiceTask service = new CallServiceTask(mContext, TAG_GET_REGISTRATION, paramNames,
                paramValues, Services.JSON_POST, message, GetRegistrationDetail.this);
        service.execute();
    }

    public GetRegistrationDetail(Context mContext, String sUsername, ObjectDetailModel object) {
        this.mContext = mContext;
        this.mObject = object;
        String regID = "0";
        if (mObject != null)
            regID = String.valueOf(mObject.getRegIDNew());
        String[] paramValues = new String[]{sUsername, regID, null};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_registration);
        CallServiceTask service = new CallServiceTask(mContext, TAG_GET_REGISTRATION, paramNames,
                paramValues, Services.JSON_POST, message, GetRegistrationDetail.this);
        service.execute();
    }

    public void bindData(String result) {
        try {
            //modify by haulc3
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                RegistrationDetailModel obj = new Gson().fromJson(
                        jsObj.getJSONArray("ListObject").get(0).toString(), RegistrationDetailModel.class);
                //return to activity
                goToRegistrationDetail(obj);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        bindData(result);
    }

    private void goToRegistrationDetail(RegistrationDetailModel model) {
        try {
            Intent intent = new Intent(mContext, RegisterDetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (mObject != null)
                intent.putExtra(Constants.TAG_CONTRACT, mObject);
            intent.putExtra(Constants.MODEL_REGISTER, model);
            String className = mContext.getClass().getSimpleName();
            //check update camera sell more
            try {
                if (className != null && className.equals(BookPortManualActivity.class.getSimpleName())) {
                    BookPortManualActivity bookPortActivity = (BookPortManualActivity) mContext;
                    mContext.startActivity(intent);
                    bookPortActivity.finish();
                } else if (className != null && className.equals(InvestiageActivity.class.getSimpleName())) {
                    InvestiageActivity investiage = (InvestiageActivity) mContext;
                    mContext.startActivity(intent);
                    investiage.finish();
                } else if (className != null && className.equals(RegisterActivityNew.class.getSimpleName())) {
                    RegisterActivityNew registerNew = (RegisterActivityNew) mContext;
                    mContext.startActivity(intent);
                    registerNew.finish();
                } else if (className != null && className.equals(UpdateInternetReceiptActivity.class.getSimpleName())) {
                    UpdateInternetReceiptActivity register = (UpdateInternetReceiptActivity) mContext;
                    mContext.startActivity(intent);
                    register.finish();
                } else if (className != null && className.equals(DepositActivity.class.getSimpleName())) {
                    DepositActivity register = (DepositActivity) mContext;
                    mContext.startActivity(intent);
                    register.finish();
                } else if (className != null && className.equals(RegisterContractActivity.class.getSimpleName())) {
                    RegisterContractActivity activity = (RegisterContractActivity) mContext;
                    mContext.startActivity(intent);
                    activity.finish();
                } else if (className != null && className.equals(DepositRegisterContractActivity.class.getSimpleName())) {
                    DepositRegisterContractActivity activity = (DepositRegisterContractActivity) mContext;
                    mContext.startActivity(intent);
                    activity.finish();
                } else if (className != null && className.equals(PaymentRetailAdditionalActivity.class.getSimpleName()) && !istoDetail) {
                    PaymentRetailAdditionalActivity activity = (PaymentRetailAdditionalActivity) mContext;
                    activity.updateStatusPayment(model.getCheckListRegistration());
                } else if (className != null && className.equals(DeployAppointmentActivity.class.getSimpleName())) {
                    DeployAppointmentActivity activity = (DeployAppointmentActivity) mContext;
                    mContext.startActivity(intent);
                    activity.finish();
                } else if (className != null && className.equals(ExtraOttActivity.class.getSimpleName())) {
                    ExtraOttActivity activity = (ExtraOttActivity) mContext;
                    mContext.startActivity(intent);
                    activity.finish();
                }else if (className != null && className.equals(FptCameraActivity.class.getSimpleName())) {
                    FptCameraActivity activity = (FptCameraActivity) mContext;
                    mContext.startActivity(intent);
                    activity.finish();
                } else if (className != null && className.equals(ConfirmPaymentActivity.class.getSimpleName())) {
                    ConfirmPaymentActivity activity = (ConfirmPaymentActivity) mContext;
                    mContext.startActivity(intent);
                    activity.finish();
                }else {
                    mContext.startActivity(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
