package isc.fpt.fsale.adapter;

import isc.fpt.fsale.model.ReportCreateObjectDetailModel;
import java.util.ArrayList;
import isc.fpt.fsale.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReportCreateObjectDetailAdapter extends BaseAdapter {

	private ArrayList<ReportCreateObjectDetailModel> mList;	
	private Context mContext;
	
	public ReportCreateObjectDetailAdapter(Context context, ArrayList<ReportCreateObjectDetailModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportCreateObjectDetailModel item = mList.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_create_object_detail, null);
		}
		if(item != null){
			TextView lblContract = (TextView)convertView.findViewById(R.id.lbl_contract);
			TextView lblRegcode = (TextView)convertView.findViewById(R.id.lbl_reg_code);
			TextView lblFullName = (TextView)convertView.findViewById(R.id.lbl_full_name);
			TextView lblAddress = (TextView)convertView.findViewById(R.id.lbl_address);
			TextView lblPhoneNumber = (TextView)convertView.findViewById(R.id.lbl_phone_number);
			TextView lblStartDate = (TextView)convertView.findViewById(R.id.lbl_start_date);
			TextView lblCreateObjectStatusName = (TextView)convertView.findViewById(R.id.lbl_create_object_status_name);
			TextView lblRowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			lblContract.setText(item.getContract());
			lblRegcode.setText(item.getRegCode());
			lblFullName.setText(item.getFullName());
			lblAddress.setText(item.getAddress());
			lblPhoneNumber.setText(item.getPhoneNumber());
			lblStartDate.setText(item.getStart_Date());
			lblCreateObjectStatusName.setText(item.getStatusName());
			lblRowNumber.setText(String.valueOf(item.getRowNumber()));
		}
		
		return convertView;
	}


}
