package isc.fpt.fsale.map.action;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.w3c.dom.Document;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;
import isc.fpt.fsale.R;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.map.utils.MapConstants;
import isc.fpt.fsale.map.utils.RouteDirection;
import isc.fpt.fsale.utils.Common;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;

/**
 * CLASS: 			GetRouteTask
 * @description: 	handle route direction response data: draw route direction line on map
 * @author: 		vandn
 * @created on:		09/08/2013
 * */
public class GetRouteTask extends AsyncTask<String, Void, String>{
	private Context mContext;
	private ProgressDialog pd;
	private LatLng fromPosition, toPosition;
    private String response = "";
    private Document document; 
    private String TDName;
      
	public GetRouteTask(Context mContext, LatLng fromPostition, LatLng toPosition, String tdname){
    	this.mContext = mContext;
    	this.fromPosition = fromPostition;
    	this.toPosition = toPosition;
    	this.TDName = tdname;
    }
    @Override
    protected void onPreExecute() {
        pd = Common.showProgressBar(this.mContext, mContext.getResources().getString(R.string.msg_pd_search));
    }

    @Override
    protected String doInBackground(String... urls) {
          //Get All Route values
          document = RouteDirection.getDocument(this.fromPosition, this.toPosition, RouteDirection.MODE_WALKING);
          if(document!=null)
        	  response = "Success";
          return response;
    }

    @Override
    protected void onPostExecute(String result) {
    	try{
    	  pd.dismiss();
          MapConstants.MAP.clear();
          if(response.equalsIgnoreCase("Success")){
	          ArrayList<LatLng> directionPoint = RouteDirection.getDirection(document);
	       
	          // Adding route on the map	     
	          MapConstants.MAP.addPolyline((new PolylineOptions())
		                .addAll(directionPoint)
		                .width(3)
		                .color(Color.RED));
	          
	          String title; //snippet, address, sCoordinates, 
	          // adding marker route start point	
	          title = this.TDName ;//mContext.getString(R.string.title_TDlocation);
	          /*sCoordinates = MapCommon.getGPSCoordinates(this.fromPosition);
	          address = MapCommon.getAddressByLatlng(sCoordinates);
	  		  snippet = "RouteStart" +"@Địa chỉ: " + address;*/
	          
	  		  Marker marker = MapCommon.addMarkerOnMap(title,this.fromPosition, R.drawable.cuslocation_black, true);
	  		  marker.showInfoWindow();
	  		  
	  		  // adding marker route end point	
	          title = mContext.getString(R.string.title_cuslocation);
	          /*sCoordinates = MapCommon.getGPSCoordinates(this.toPosition);
	          address = MapCommon.getAddressByLatlng(sCoordinates);
	  		  snippet = "RouteEnd" +"@Địa chỉ: " + address;*/
	  		  
	          MapCommon.addMarkerOnMap(title, this.toPosition, R.drawable.icon_home, true);
	  		  
	          MapCommon.animeToLocation(toPosition);
	          MapCommon.setMapZoom(15);
          }
          else{
        	  Common.alertDialog("Kết nối đến trợ giúp chỉ đường của google thất bại. Vui lòng thử lại sau", mContext);
          }
    	}
    	catch(Exception e){

    		e.printStackTrace();
    	}
    }
}


