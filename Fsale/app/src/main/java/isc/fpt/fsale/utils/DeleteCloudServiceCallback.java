package isc.fpt.fsale.utils;

import isc.fpt.fsale.model.CloudPackageSelected;

/**
 * Created by haulc3 on 02,April,2019
 */
public interface DeleteCloudServiceCallback {
    void DeleteCallback(CloudPackageSelected obj);
}
