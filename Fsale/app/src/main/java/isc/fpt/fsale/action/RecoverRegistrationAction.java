package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

public class RecoverRegistrationAction  implements AsyncTaskCompleteListener<String>{ 

	private final String RecoverRegistration = "RecoverRegistration";
	private final String RecoverRegistration_RESULT = "RecoverRegistrationResult";
	private final String ERRORSERVICE = "ErrorService";
	private final String RESULT = "Result";
	
	private Context mContext;
	
	public RecoverRegistrationAction(Context mContext,String[] paramsValue) 
	{
		this.mContext = mContext;	
		
		RecoverRegistration(paramsValue);
	}
	
	public void RecoverRegistration(String[] paramsValue)
	{		
		String[] params = new String[]{"Type","RegCode","CreateBy","IP"};
		String message = "Vui lòng đợi giây lát"; 
		CallServiceTask service = new CallServiceTask(mContext,RecoverRegistration,params,paramsValue, Services.JSON_POST, message, RecoverRegistrationAction.this);
		service.execute();
	}
	
	public void handleUpdateRegistration(String json){		
		if(json != null && Common.jsonObjectValidate(json)){			
			 try {	
				   JSONArray jsArr;
				    JSONObject jsObj = JSONParsing.getJsonObj(json);
				    jsArr = jsObj.getJSONArray(RecoverRegistration_RESULT);
				    int l = jsArr.length();
				    if (l > 0) 
				    {
				    	 String error = jsArr.getJSONObject(0).getString(ERRORSERVICE);
					     if (error.equals("null")) 
					     {
						      String result = jsArr.getJSONObject(0).getString(RESULT);
						      /*int ResultID=jsArr.toJSONObject(0).getInt("ResultID");*/
						    
						      // new GetRegistrationDetail(mContext,"oanh254",this.ID);
						      Common.alertDialog(result, mContext);
						      
					     }
					     else
					    	 Common.alertDialog(error, mContext);
				    }
			 }
			 catch (Exception e)
			 {

				e.printStackTrace();
				Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
			 }
		}		
		else Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);		
	}
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);
	}

}
