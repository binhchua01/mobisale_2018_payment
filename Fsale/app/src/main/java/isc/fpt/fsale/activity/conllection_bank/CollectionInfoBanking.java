package isc.fpt.fsale.activity.conllection_bank;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.utils.Constants;

public class CollectionInfoBanking extends BaseActivitySecond {

    private View loBack, loClose, loHeader;
    private WebView webView;
    private String Url;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void initEvent() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            Url = bundle.getString(Constants.TAG_RESULT_COLLECTION_BANKING);
        }
        loBack.setOnClickListener(view -> onBackPressed());
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl(Url);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_collection_info_banking;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.img_back);
        loHeader = findViewById(R.id.text_header);
        webView = (WebView) findViewById(R.id.webGatewway);
    }


}