package isc.fpt.fsale.callback.upsell;

import java.util.List;

import isc.fpt.fsale.model.upsell.response.ObjUpgradeListModel;

public interface OnGetObjUpdgrade {
    void onGetListSuccess(List<ObjUpgradeListModel> mList);
}
