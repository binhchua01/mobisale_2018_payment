package isc.fpt.fsale.activity;

import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewOutlineProvider;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListRegistration;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.RegistrationListAdapter;
import isc.fpt.fsale.ui.choose_service.ChooseServiceActivity;
import isc.fpt.fsale.ui.fragment.MenuRightRegister;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ListRegistrationModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

// màn hình DANH SÁCH TTKH/HỢP ĐỒNG CHƯA DUYỆT
public class RegistrationListActivity extends BaseActivity implements OnItemClickListener {
    public ListView lvRegistration;
    private Context mContext;
    private String PageSize, ParamName;
    private int PAGE_COUNT = 1;
    public int iCurrentPage = 1;
    public Spinner SpPage;
    private Button btnPre, btnNext;
    private int Agent;
    private String AgentName;

    public RegistrationListActivity() {
        super(R.string.lbl_register_list);
        MyApp.setCurrentActivity(this);
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_registration_list_activity));
        setContentView(R.layout.list_registration);
        mContext = RegistrationListActivity.this;
        lvRegistration = (ListView) findViewById(R.id.lv_registration_list);
        lvRegistration.setItemsCanFocus(true);
        lvRegistration.setOnItemClickListener(this);
        lvRegistration.setTextFilterEnabled(true);
        lvRegistration.setSelection(0);
        //control số trang
        SpPage = (Spinner) this.findViewById(R.id.sp_page_num);
        //control nút back <
        btnPre = (Button) this.findViewById(R.id.btn_previous);
        //control nút next >
        btnNext = (Button) this.findViewById(R.id.btn_next);
        // khởi tạo sự kiện chọn số trang
        SpPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() != iCurrentPage) {
                    try {
                        iCurrentPage = selectedItem.getID();
                        String[] ParamNameModel = ParamName.split("/");
                        // kết nối API lấy danh sách thông tin khách hàng
                        new GetListRegistration(mContext, ParamNameModel[1],
                                iCurrentPage, Agent, AgentName, false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        // khởi tạo sự kiện nhấn nút previous
        btnPre.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                iCurrentPage--;
                CheckEnable();
            }
        });
        // khởi tạo sự kiện nhấn nút next
        btnNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                iCurrentPage++;
                CheckEnable();
            }
        });
        //control nút tạo thông tin khách hàng
        ImageButton imgCreate = (ImageButton) findViewById(R.id.img_create);
        if (Build.VERSION.SDK_INT >= 21) {//Lollipop
            try {
                imgCreate.setOutlineProvider(new ViewOutlineProvider() {
                    @Override
                    public void getOutline(View view, Outline outline) {
                        int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
                        outline.setOval(0, 0, diameter, diameter);
                    }
                });
                imgCreate.setClipToOutline(true);
                StateListAnimator sla = AnimatorInflater.loadStateListAnimator(
                        RegistrationListActivity.this, R.drawable.selector_button_add_material_design);
                imgCreate.setStateListAnimator(sla);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            imgCreate.setImageResource(android.R.color.transparent);
        }
        //khởi tạo sự kiện nhấn nút tạo thông tin khách hàng
        imgCreate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ChooseServiceActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(intent);
            }
        });
        getDataFromIntent();
        super.addRight(new MenuRightRegister(null));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                ArrayList<ListRegistrationModel> lstRegister =
                        myIntent.getParcelableArrayListExtra("list_registration");
                //
                this.Agent = myIntent.getIntExtra("Agent", 0);
                this.AgentName = myIntent.getStringExtra("AgentName");

                if (lstRegister != null) {
                    RegistrationListAdapter adapter =
                            new RegistrationListAdapter(RegistrationListActivity.this, lstRegister);
                    lvRegistration.setAdapter(adapter);
                }
                PageSize = myIntent.getStringExtra("PageSize");
                ParamName = myIntent.getStringExtra("LinkName");
            }
            String[] PageSizeModel = PageSize.split(";");
            int CurrentPage = Integer.parseInt(PageSizeModel[0]);
            int TotalPage = Integer.parseInt(PageSizeModel[1]);
            PAGE_COUNT = TotalPage;
            iCurrentPage = CurrentPage;
            initPageSpinner(TotalPage, CurrentPage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initPageSpinner(int TotalPage, int CurrentPage) {
        ArrayList<KeyValuePairModel> ListSpPage = new ArrayList<>();
        for (int i = 1; i <= TotalPage; i++) {
            String GT = String.valueOf(i);
            ListSpPage.add(new KeyValuePairModel(i, GT));
        }
        KeyValuePairAdapter adapterList = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, ListSpPage, Color.WHITE);
        SpPage.setAdapter(adapterList);
        SpPage.setSelection(Common.getIndex(SpPage, CurrentPage));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_right) {
            toggle(SlidingMenu.RIGHT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    //khóa mở nút tăng, giảm trang
    private void CheckEnable() {
        try {
            String[] ParamNameModel = ParamName.split("/");
            int page = iCurrentPage;
            if (PAGE_COUNT == 1) {
                btnPre.setEnabled(false);
                btnNext.setEnabled(false);
            } else {
                if (iCurrentPage >= PAGE_COUNT) {
                    btnNext.setEnabled(false);
                    btnPre.setEnabled(true);
                    if (iCurrentPage == PAGE_COUNT) {
                        new GetListRegistration(mContext, ParamNameModel[1], page, Agent, AgentName, false);

                    }
                } else if (iCurrentPage <= 1) {
                    btnPre.setEnabled(false);
                    btnNext.setEnabled(true);
                    if (iCurrentPage == 1) {
                        new GetListRegistration(mContext, ParamNameModel[1], page, Agent, AgentName, false);
                    }
                } else {
                    btnPre.setEnabled(true);
                    btnNext.setEnabled(true);
                    new GetListRegistration(mContext, ParamNameModel[1], page, Agent, AgentName, false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //khởi tạo sự kiện chọn 1 thông tin khách hàng
    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        ListRegistrationModel selectedItem = (ListRegistrationModel) parentView.getItemAtPosition(position);
        // kết nối API lấy thông tin chi tiết thông tin khách hàng
        new GetRegistrationDetail(this, Constants.USERNAME, selectedItem.getID());
        Log.d("TAG", "onItemClick: " + selectedItem.getID());
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
        try {
            String[] ParamNameModel = ParamName.split("/");
            new GetListRegistration(mContext, ParamNameModel[1], iCurrentPage, Agent, AgentName, false);
        } catch (Exception e) {
            Toast.makeText(this, "Không lấy được dữ liệu:" + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        getDataFromIntent();
    }
}
