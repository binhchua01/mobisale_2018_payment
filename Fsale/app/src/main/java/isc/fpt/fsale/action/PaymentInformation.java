package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PayContractActivity;
import isc.fpt.fsale.activity.PaymentRetailAdditionalActivity;
import isc.fpt.fsale.model.PaymentInformationPOST;
import isc.fpt.fsale.model.PaymentInformationResult;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// lấy thông tin thanh toán
public class PaymentInformation implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public PaymentInformation(Context context, PaymentInformationPOST mObject) {
        this.mContext = context;
        //Declares variables
        String[] arrParamName = new String[]{"SaleName", "RegCode"};
        String[] arrParamValue = new String[]{mObject.getSaleName(), mObject.getRegCode()};
        String message = mContext.getResources().getString(R.string.msg_pd_get_payment_infomation);
        String PAYMENT_INFORMATION = "PaymentInformation";
        CallServiceTask service = new CallServiceTask(mContext, PAYMENT_INFORMATION,
                arrParamName, arrParamValue, Services.JSON_POST, message, PaymentInformation.this);
        service.execute();
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<PaymentInformationResult> objList = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject("PaymentInformationResult");

                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    objList.add(new Gson().fromJson(jsonArray.get(i).toString(), PaymentInformationResult.class));
                }

                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    returnDataForPayContract(objList.get(0));
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void returnDataForPayContract(PaymentInformationResult objResult) {
        if (mContext != null) {
                if (mContext.getClass().getSimpleName().equals(PayContractActivity.class.getSimpleName())) {
                    PayContractActivity activity = (PayContractActivity) mContext;
                    activity.showPaymentInformation(objResult);
                }
                else {
                    PaymentRetailAdditionalActivity activity = (PaymentRetailAdditionalActivity) mContext;
                    activity.showPaymentInformation(objResult);
                }
        }
    }
}