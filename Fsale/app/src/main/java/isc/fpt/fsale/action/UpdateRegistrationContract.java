package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class UpdateRegistrationContract implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public UpdateRegistrationContract(Context mContext, JSONObject jsonObjParam) {
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.msg_pd_update);
        JSONObject json = new JSONObject();
        try {
            json.put("Register", jsonObjParam);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String UPDATE_REGISTRATION = "UpdateRegistrationContractV2";
        CallServiceTask service = new CallServiceTask(mContext, UPDATE_REGISTRATION,
                json, Services.JSON_POST_OBJECT, message, UpdateRegistrationContract.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    public void handleUpdateRegistration(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                boolean hasError = false;
                List<UpdResultModel> lst = null;
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(jsObj, UpdResultModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                } else {//ServiceType Error
                    hasError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!hasError)
                    loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onTaskComplete(String result) {
        handleUpdateRegistration(result);
    }

    private void loadData(List<UpdResultModel> lst) {
        if (lst.size() > 0) {
            final UpdResultModel item = lst.get(0);
            if (item.getResultID() > 0) {
                new AlertDialog.Builder(mContext)
                        .setTitle(mContext.getResources()
                                .getString(R.string.title_notification))
                        .setMessage(item.getResult())
                        .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new GetRegistrationDetail(mContext, Constants.USERNAME, item.getResultID());
                                dialog.cancel();
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            } else {
                Common.alertDialog(lst.get(0).getResult(), mContext);
            }
        }
    }
}