package isc.fpt.fsale.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetAllDeviceTypeConfirm;
import isc.fpt.fsale.action.GetListDeviceOTT;
import isc.fpt.fsale.adapter.TabAdapter;
import isc.fpt.fsale.model.ListDeviceOTT;
import isc.fpt.fsale.ui.fragment.ConfirmDeviceFragment;
import isc.fpt.fsale.ui.fragment.InventoryDeviceFragment;
import isc.fpt.fsale.ui.fragment.UnConfirmDeviceFragment;
import isc.fpt.fsale.model.DeviceTypeConfirm;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by haulc3 on 16,April,2019
 */
public class ReceiveDeviceActivity extends FragmentActivity implements ViewPager.OnPageChangeListener {
    private RelativeLayout mBack;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    public ArrayList<DeviceTypeConfirm> list;
    public ArrayList<ListDeviceOTT> listInventory;
    private ConfirmDeviceFragment mFragConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_device);
        bindView();
        setEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //call api get device type confirm...
        new GetAllDeviceTypeConfirm(this, this, ((MyApp) getApplication()).getUserName());
    }

    private void addTabView() {
        mFragConfirm = new ConfirmDeviceFragment();
        TabAdapter mAdapter = new TabAdapter(getSupportFragmentManager());
        mAdapter.addFragment(new UnConfirmDeviceFragment(), getResources().getString(R.string.label_un_confirm_device));
        mAdapter.addFragment(mFragConfirm, getResources().getString(R.string.label_confirm_device));
        mAdapter.addFragment(new InventoryDeviceFragment(), getResources().getString(R.string.label_inventory_device));
        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOnPageChangeListener(this);
    }

    private void setEvent() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void bindView() {
        mBack = (RelativeLayout) findViewById(R.id.back_button);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
    }

    public void deviceTypeConfirm(ArrayList<DeviceTypeConfirm> list) {
        this.list = new ArrayList<>();
        this.list.add(new DeviceTypeConfirm(-1, getResources().getString(R.string.label_choose_device_type)));
        this.list.addAll(list);
        new GetListDeviceOTT(this, this, ((MyApp) getApplicationContext()).getUserName());
    }

    public void deviceOTT(ArrayList<ListDeviceOTT> list) {
        this.listInventory = new ArrayList<>();
        this.listInventory.add(new ListDeviceOTT(-1, getResources().getString(R.string.label_choose_device_type)));
        this.listInventory.addAll(list);
        addTabView();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 1) {
            mFragConfirm.pageCurrent = 1;
            mFragConfirm.isLastPage = false;
            mFragConfirm.isLoadMore = true;
            mFragConfirm.list.clear();
            mFragConfirm.getListDeviceConfirm();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
