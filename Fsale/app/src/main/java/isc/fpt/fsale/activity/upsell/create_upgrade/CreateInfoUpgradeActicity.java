package isc.fpt.fsale.activity.upsell.create_upgrade;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.danh32.fontify.Button;
import com.danh32.fontify.EditText;
import com.danh32.fontify.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.upsell.GetAdvisoryResultUpgrade;
import isc.fpt.fsale.action.upsell.GetObjUpgradeForCare;
import isc.fpt.fsale.action.upsell.GetProgramUpgradeMoney;
import isc.fpt.fsale.action.upsell.GetPromotionIPTVUpgrade;
import isc.fpt.fsale.action.upsell.GetPromotionNetUpgrade;
import isc.fpt.fsale.action.upsell.GetUpgradeStatus;
import isc.fpt.fsale.action.upsell.UpdateObjUpgrade;
import isc.fpt.fsale.activity.upsell.history.HistoryUpsellActivity;
import isc.fpt.fsale.adapter.upsell.KeyUpgradeAdivisoryAdapter;
import isc.fpt.fsale.adapter.upsell.KeyUpgradeMoneyAdapter;
import isc.fpt.fsale.adapter.upsell.KeyUpgradePromotionIPTVAdapter;
import isc.fpt.fsale.adapter.upsell.KeyUpgradePromotionNETAdapter;
import isc.fpt.fsale.adapter.upsell.KeyUpgradeStatusAdapter;
import isc.fpt.fsale.model.upsell.response.AdivisoryResultUpgradeModel;
import isc.fpt.fsale.model.upsell.response.GetPromotionIPTVUpgradeModel;
import isc.fpt.fsale.model.upsell.response.GetPromotionNetUpgradeModel;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeForCare;
import isc.fpt.fsale.model.upsell.response.ProgramUpgradeMoney;
import isc.fpt.fsale.model.upsell.response.ProgramUpgradeStatusModel;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class CreateInfoUpgradeActicity extends BaseActivitySecond {
    public ScrollView scrollMain;
    TextView mNumberItem, mContract, mTittle, mName, mPhone, mAddress,
            mService, mCLKM, mProgram, mMoneyParent, mPaymentUpgrade, mViewHistory, mPeriod, mCountMoney;
    EditText mNote;
    Button mBtnUpdate;
    Spinner spnMoneyUpgrade, mSpnSurvey, mReason, mProNet, mProIPTV;
    RatingBar ratingBar;
    View loBack;
    String mProgramID, moneyUpgrade;
    String ObjID, Contract, ProgramUpgradeID;
    LinearLayout mLayoutKMNet, mLayoutKMPayTV, mLayoutCount, mLayoutReason;
    private int upgradeStatus, advisoryID;
    private Double feeToLocalType, countMoney, mMoneyNET, mMoneyIPTV;
    private ObjUpgradeForCare mUpgradeForCare;
    private List<ProgramUpgradeMoney> mMoney = new ArrayList<>();
    private List<ProgramUpgradeStatusModel> mStatus = new ArrayList<>();
    private List<AdivisoryResultUpgradeModel> mAdivisory = new ArrayList<>();
    private GetPromotionNetUpgradeModel mModelProNET;
    private GetPromotionIPTVUpgradeModel mModelProIPTV;

    int mNetProID, mNetProType, mNetPromoth, mNetMoney, mIPTVProID, mIPTVProType, mIPTVPromoth, mIPTVMoney;
    String mIPTVProName, mNetProName;

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(view -> onBackPressed());
        mViewHistory.setOnClickListener(v -> {
            if (mUpgradeForCare.getNumTrans() != 0) {
                Intent intent = new Intent(CreateInfoUpgradeActicity.this, HistoryUpsellActivity.class);
                intent.putExtra("PROGRAM_UPGRADE_ID", mUpgradeForCare.getProgramUpgradeID().toString());
                intent.putExtra("OBJ_ID", mUpgradeForCare.getObjID().toString());
                startActivity(intent);
            } else {
                return;
            }
        });
        Common.smoothScrollView(scrollMain, 0);
        mNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mNote.setError(null);
            }
        });
        mBtnUpdate.setOnClickListener(v -> new UpdateObjUpgrade(v.getContext(), CreateInfoUpgradeActicity.this, putDataToJson()));
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_create_info_upgrade_acticity;
    }

    @SuppressLint({"WrongViewCast", "SetTextI18n"})
    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        mTittle = (TextView) findViewById(R.id.tv_title_toolbar);
        mTittle.setText("TẠO THÔNG TIN UPGRADE");
        scrollMain = (ScrollView) findViewById(R.id.frm_main);

        mNumberItem = (TextView) findViewById(R.id.txt_number_item);
        mContract = (TextView) findViewById(R.id.txt_number_contract);
        mName = (TextView) findViewById(R.id.txt_name);
        mPhone = (TextView) findViewById(R.id.txt_phone);
        mAddress = (TextView) findViewById(R.id.txt_address);
        mService = (TextView) findViewById(R.id.txt_service);
        mCLKM = (TextView) findViewById(R.id.txt_clkm);

        mProgram = (TextView) findViewById(R.id.txt_select_programe);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        spnMoneyUpgrade = (Spinner) findViewById(R.id.spn_money_upgrade);
        mMoneyParent = (TextView) findViewById(R.id.txt_payment_parent);
        mPaymentUpgrade = (TextView) findViewById(R.id.txt_payment_upgrade);
        mViewHistory = (TextView) findViewById(R.id.txt_view_history);
        mSpnSurvey = (Spinner) findViewById(R.id.spn_survey_results_view);
        mReason = (Spinner) findViewById(R.id.spn_reason_view);
        mNote = (EditText) findViewById(R.id.txt_note_upsell);
        mNote.requestFocus();
        mBtnUpdate = (Button) findViewById(R.id.btn_update_upsell);
        mPeriod = (TextView) findViewById(R.id.text_period_upsell_notcare);

        mLayoutKMNet = (LinearLayout) findViewById(R.id.layout_km_net);
        mLayoutKMPayTV = (LinearLayout) findViewById(R.id.layout_km_paytv);
        mLayoutCount = (LinearLayout) findViewById(R.id.layout_count);
        mLayoutReason = (LinearLayout) findViewById(R.id.layout_reason);

        mProNet = (Spinner) findViewById(R.id.spn_km_net);
        mProIPTV = (Spinner) findViewById(R.id.spn_km_iptv);
        mCountMoney = (TextView) findViewById(R.id.txt_count_money);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            ObjID = (String) bundle.get("OBJ_ID");
            Contract = (String) bundle.get("CONTRACT");
            mProgramID = (String) bundle.get("PROGRAM");
        }
        getDataForCare(ObjID, Contract);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void loadMoneyType() {
        @SuppressLint("RtlHardcoded") KeyUpgradeMoneyAdapter adapter = new KeyUpgradeMoneyAdapter(this, R.layout.my_spinner_style, mMoney, Gravity.LEFT);
        spnMoneyUpgrade.setAdapter(adapter);
        spnMoneyUpgrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ProgramUpgradeMoney mListMoney = mMoney.get(position);
                if (mListMoney != null) {
                    moneyUpgrade = mListMoney.getMoneyUpgrade();
                    Double money = Double.valueOf(mListMoney.getMoneyUpgrade());
                    if (mUpgradeForCare != null) {
                        Double fromLocalType = Double.valueOf(mUpgradeForCare.getFeeFromLocalType());
                        feeToLocalType = money + fromLocalType;
                        mPaymentUpgrade.setText(Common.formatMoney(String.valueOf(feeToLocalType)));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadUpgradeStatus() {
        @SuppressLint("RtlHardcoded") KeyUpgradeStatusAdapter adapterStatus = new KeyUpgradeStatusAdapter(this, R.layout.my_spinner_style, mStatus, Gravity.LEFT);
        mSpnSurvey.setAdapter(adapterStatus);
        mSpnSurvey.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ProgramUpgradeStatusModel statusModel = mStatus.get(position);
                upgradeStatus = statusModel.getID();
                setupSurveyLayout(statusModel, view.getContext());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setupSurveyLayout(ProgramUpgradeStatusModel model, Context context) {
        if (upgradeStatus == 1) {
            mLayoutKMNet.setVisibility(View.GONE);
            mLayoutKMPayTV.setVisibility(View.GONE);
            mLayoutCount.setVisibility(View.GONE);
            mLayoutReason.setVisibility(View.VISIBLE);
            mModelProNET = null;
            mModelProIPTV = null;
            countMoney = null;
            String[] value = {model.getID().toString()};
            new GetAdvisoryResultUpgrade(context, value, mList -> {
                mAdivisory = mList;
                loadAdivisory();
            });
        } else if (upgradeStatus == 2) {
            mModelProNET = null;
            mModelProIPTV = null;
            countMoney = null;
            mLayoutKMNet.setVisibility(View.GONE);
            mLayoutKMPayTV.setVisibility(View.GONE);
            mLayoutCount.setVisibility(View.GONE);
            mLayoutReason.setVisibility(View.GONE);
        } else {
            //net only
            if (mUpgradeForCare.getServiceType() == 0) {
                mLayoutKMNet.setVisibility(View.VISIBLE);
                mLayoutKMPayTV.setVisibility(View.GONE);
                mLayoutCount.setVisibility(View.VISIBLE);
                mLayoutReason.setVisibility(View.GONE);
                onGetPromotionNET();
            }
            //combo
            else {
                onGetPromotionNET();
                mLayoutKMNet.setVisibility(View.VISIBLE);
                mLayoutCount.setVisibility(View.VISIBLE);
                mLayoutReason.setVisibility(View.GONE);
            }
        }
    }

    private void onGetPromotionNET() {
        String[] arrValue = {ProgramUpgradeID, ObjID};
        new GetPromotionNetUpgrade(this, arrValue, lst -> {
            KeyUpgradePromotionNETAdapter netAdapter = new KeyUpgradePromotionNETAdapter(this, R.layout.my_spinner_style, lst, Gravity.LEFT);
            mProNet.setAdapter(netAdapter);
            mProNet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mModelProNET = lst.get(position);
                    mMoneyNET = Double.valueOf(mModelProNET.getMoney());
                    if (mModelProNET.getPromotionType() == 0) {
                        mLayoutKMPayTV.setVisibility(View.GONE);
                        countMoney = mMoneyNET;
                        mModelProIPTV = null;
                        mCountMoney.setText(Common.formatMoney(countMoney.toString()));
                    } else {
                        //mLayoutKMPayTV.setVisibility(View.VISIBLE);
                        if (mUpgradeForCare.getServiceType() != 0) {
                            mLayoutKMPayTV.setVisibility(View.VISIBLE);
                            onGetPromotionIPTV(mMoneyNET);
                        } else {
                            countMoney = mMoneyNET;
                            mCountMoney.setText(Common.formatMoney(countMoney.toString()));
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        });
    }

    private void onGetPromotionIPTV(Double moneyNet) {
        String[] arrValue = {ProgramUpgradeID, ObjID};
        new GetPromotionIPTVUpgrade(this, arrValue, lst -> {
            KeyUpgradePromotionIPTVAdapter netAdapter = new KeyUpgradePromotionIPTVAdapter(this, R.layout.my_spinner_style, lst, Gravity.LEFT);
            mProIPTV.setAdapter(netAdapter);
            mProIPTV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mModelProIPTV = lst.get(position);
                    mMoneyIPTV = Double.valueOf(mModelProIPTV.getMoney());
                    countMoney = moneyNet + mMoneyIPTV;
                    mCountMoney.setText(Common.formatMoney(countMoney.toString()));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        });
    }

    private void loadAdivisory() {
        @SuppressLint("RtlHardcoded") KeyUpgradeAdivisoryAdapter adapter = new KeyUpgradeAdivisoryAdapter(this, R.layout.my_spinner_style, mAdivisory, Gravity.LEFT);
        mReason.setAdapter(adapter);
        mReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                advisoryID = mAdivisory.get(position).getiD();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @SuppressLint("StringFormatMatches")
    private void getDataForCare(String ObjID, String Contract) {
        String[] arrValue = {ObjID, mProgramID, Constants.USERNAME, Contract};
        new GetObjUpgradeForCare(this, arrValue, mList -> {
            if (mList != null) {
                mUpgradeForCare = mList.get(0);
                setupView(mUpgradeForCare);
                onGetData(mUpgradeForCare);
            }
        });
    }

    @SuppressLint("StringFormatMatches")
    private void onGetData(ObjUpgradeForCare model) {
        mViewHistory.setText(String.format("%s", getString(R.string.view_history_obj_upgrade, model.getNumTrans())));
        ProgramUpgradeID = model.getProgramUpgradeID().toString();
        String[] arrValueMoney = {mProgramID};
        new GetProgramUpgradeMoney(this, arrValueMoney, lst -> {
            if (lst != null) {
                mMoney = lst;
                loadMoneyType();
            }
        });
        String[] arrValueStatus = {model.getObjID().toString(), mProgramID};
        new GetUpgradeStatus(this, arrValueStatus, mList -> {
            if (mList != null) {
                mStatus = mList;
                loadUpgradeStatus();
            }
        });
    }

    private void validatePromotionNET() {
        if (mModelProNET == null) {
            mNetProID = 0;
            mNetProName = "";
            mNetProType = 0;
            mNetPromoth = 0;
            mNetMoney = 0;
        } else {
            mNetProID = mModelProNET.getPromotionID();
            mNetProName = mModelProNET.getPromotionName();
            mNetProType = mModelProNET.getPromotionType();
            mNetPromoth = mModelProNET.getPrepaidMonth();
            mNetMoney = mModelProNET.getMoney();
        }
    }

    private void validatePromotionIPTV() {
        if (mModelProIPTV == null) {
            mIPTVProID = 0;
            mIPTVProName = "";
            mIPTVProType = 0;
            mIPTVPromoth = 0;
            mIPTVMoney = 0;
        } else {
            mIPTVProID = mModelProIPTV.getPromotionID();
            mIPTVProName = mModelProIPTV.getPromotionName();
            mIPTVProType = mModelProIPTV.getPromotionType();
            mIPTVPromoth = mModelProIPTV.getPrepaidMonth();
            mIPTVMoney = mModelProIPTV.getMoney();
        }
    }

    private JSONObject putDataToJson() {
        //put data to json obj and call api
        validatePromotionNET();
        validatePromotionIPTV();


        JSONObject jsonObject = new JSONObject();
        JSONObject object = new JSONObject();
        try {
            object.put("Contract", mUpgradeForCare.getContract());
            object.put("FeeFromLocalType", mUpgradeForCare.getFeeFromLocalType());
            object.put("FeeToLocalType", feeToLocalType);
            object.put("FromLocalType", mUpgradeForCare.getFromLocalType());
            object.put("MoneyUpgrade", moneyUpgrade);
            object.put("ObjID", mUpgradeForCare.getObjID());
            object.put("PotentialRating", mUpgradeForCare.getPotentialRating());
            object.put("ProgramUpgradeID", mUpgradeForCare.getProgramUpgradeID());
            object.put("ToLocalType", mUpgradeForCare.getToLocalType());
            object.put("UpgradeStatus", upgradeStatus);
            object.put("AdvisoryUpgradeID", advisoryID);
            object.put("Note", mNote.getText().toString());
            object.put("UserName", Constants.USERNAME);
            object.put("Phone", mUpgradeForCare.getPhone());
            // Promotion NET
            object.put("NetPromotionID", mNetProID);
            object.put("NetPromotionName", mNetProName);
            object.put("NetPromotionType", mNetProType);
            object.put("NetPrepaidMonth", mNetPromoth);
            object.put("NetMoney", mNetMoney);
            // Promotion IPTV
            object.put("IPTVPromotionID", mIPTVProID);
            object.put("IPTVPromotionName", mIPTVProName);
            object.put("IPTVPromotionType", mIPTVProType);
            object.put("IPTVPrepaidMonth", mIPTVPromoth);
            object.put("IPTVMoney", mIPTVMoney);
            //Total Money
            if (countMoney != null) {
                object.put("Total", countMoney);
            } else {
                object.put("Total", 0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            jsonObject.put("ObjUpgrade", object);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    private void setupView(ObjUpgradeForCare mUpgradeForCare) {
        mNumberItem.setVisibility(View.GONE);
        mContract.setText(mUpgradeForCare.getContract());
        mName.setText(mUpgradeForCare.getFullName());
        mPhone.setText(mUpgradeForCare.getPhone());
        mAddress.setText(mUpgradeForCare.getAddress());
        mService.setText(mUpgradeForCare.getFromLocalTypeName());
        mCLKM.setText(mUpgradeForCare.getProgramUpgradeName());
        mProgram.setText(mUpgradeForCare.getProgramUpgradeName());
        ratingBar.setRating(mUpgradeForCare.getPotentialRating());
        mMoneyParent.setText(Common.formatMoney(mUpgradeForCare.getFeeFromLocalType() + ""));
        mPeriod.setText(mUpgradeForCare.getDueDate());

    }
}
