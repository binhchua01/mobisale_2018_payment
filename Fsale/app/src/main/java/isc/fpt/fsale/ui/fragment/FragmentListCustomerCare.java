package isc.fpt.fsale.ui.fragment;


import isc.fpt.fsale.action.GetCustomerCareList;
import isc.fpt.fsale.activity.CustomerCareDetailActivity;
import isc.fpt.fsale.activity.ListCustomerCareActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.CustomerCareAdapter;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.CustomerCareListModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

// màn hình LEVEL KS ĐỘ HÀI LÒNG KHÁCH HÀNG
public class FragmentListCustomerCare extends Fragment implements OnItemClickListener {

    private ListView lvObject;
    private Context mContext;
    private Spinner spPage;
    private static int FLAG_FIRST_LOAD = 0;
    private int mPage = 1;

    public static FragmentListCustomerCare newInstance() {
        FragmentListCustomerCare fragment = new FragmentListCustomerCare();
        return fragment;
    }

    public FragmentListCustomerCare() {

    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_customer_care, container, false);
        lvObject = (ListView) view.findViewById(R.id.lv_object);
        lvObject.setOnItemClickListener(this);
        mContext = getActivity();
        spPage = (Spinner) view.findViewById(R.id.sp_page);
        // sự kiện xem thông tin ở trang mới.
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getData(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        getData(0);

        return view;
    }

    private void getData(int page) {
        int levelID = -1;
        if (mContext != null) {
            String tabName = this.getTag();
            levelID = ListCustomerCareActivity.getLevelByName(tabName);
        }
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        new GetCustomerCareList(this, UserName, 0, "", page, levelID);
    }

    public void getData() {
        int levelID = -1;
        if (mContext != null) {
            String tabName = this.getTag();
            levelID = ListCustomerCareActivity.getLevelByName(tabName);
        }
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        new GetCustomerCareList(this, UserName, 0, "", mPage, levelID);
    }

    public void loadData(List<CustomerCareListModel> lst) {
        if (lst != null && lst.size() > 0) {
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            CustomerCareAdapter mAdapter = new CustomerCareAdapter(mContext, lst, this);
            lvObject.setAdapter(mAdapter);
        } else {
            showToast("Không có dữ liệu!");
        }
    }

    private Toast mToast;

    private void showToast(String s) {
        if (mToast == null)
            mToast = Toast.makeText(mContext, s, Toast.LENGTH_SHORT);
        else
            mToast.setText(s);
        mToast.show();
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub
        CustomerCareListModel selectedItem = (CustomerCareListModel) parent.getItemAtPosition(position);
        Intent intent = new Intent(mContext, CustomerCareDetailActivity.class);
        intent.putExtra("CUSTOMER_CARE", selectedItem);
        this.startActivity(intent);
    }
}
