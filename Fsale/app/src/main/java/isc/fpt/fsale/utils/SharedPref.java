package isc.fpt.fsale.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.model.District;
import isc.fpt.fsale.model.StreetOrCondo;
import isc.fpt.fsale.model.Ward;

/**
 * Created by haulc3 on 17,July,2019
 */
public class SharedPref {
    public enum Key {
    }

    private static SharedPreferences sharedPreferences(Context context) {
        return context.getSharedPreferences("SharedPreferencesUtils", Context.MODE_PRIVATE);
    }

    public static void put(Context context, Key key, String value) {
        savePref(context, key.name(), value);
    }

    public static void put(Context context, String key, String value) {
        savePref(context, key, value);
    }

    public static String get(Context context, Key key, String defValue) {
        return sharedPreferences(context).getString(key.name(), defValue);
    }

    public static String get(Context context, String key, String defValue) {
        return sharedPreferences(context).getString(key, defValue);
    }

    private static void savePref(Context context, String key, String value) {
        final SharedPreferences.Editor editor = sharedPreferences(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static List<District> getDistrictList(Context context, String locationId) {
        List<District> lst = null;
        try {
            String result = SharedPref.get(context, locationId, "");
            String TAG_GET_DISTRICTS_RESULT = "GetDistrictListMethodPostResult";
            if (!result.equals("null") && !TextUtils.isEmpty(result)) {
                lst = new ArrayList<>();
                JSONObject jsObj = new JSONObject(result);
                JSONArray jsonArray = jsObj.getJSONArray(TAG_GET_DISTRICTS_RESULT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), District.class));
                }
            }
        }catch(Exception ex){
           ex.printStackTrace();
        }
        return lst;
    }

    public static List<Ward> getWardList(Context context, String district) {
        List<Ward> lst = null;
        try {
            String result = SharedPref.get(context, district, "");
            String TAG_GET_WARD_RESULT = "GetWardListMethodPostResult";
            if (!result.equals("null") && !TextUtils.isEmpty(result)) {
                lst = new ArrayList<>();
                JSONObject jsObj = new JSONObject(result);
                JSONArray jsonArray = jsObj.getJSONArray(TAG_GET_WARD_RESULT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), Ward.class));
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return lst;
    }

    public static List<StreetOrCondo> getStreetOrCondoList(Context context, String district, String ward, int type) {
        List<StreetOrCondo> lst = null;
        try {
            String result = SharedPref.get(context, district + ward + type, "");
            String TAG_GET_STREET_OR_CONDO_RESULT = "GetStreetOrCondominiumListMethodPostResult";
            if (!result.equals("null") && !TextUtils.isEmpty(result)) {
                lst = new ArrayList<>();
                JSONObject jsObj = new JSONObject(result);
                JSONArray jsonArray = jsObj.getJSONArray(TAG_GET_STREET_OR_CONDO_RESULT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), StreetOrCondo.class));
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return lst;
    }
}
