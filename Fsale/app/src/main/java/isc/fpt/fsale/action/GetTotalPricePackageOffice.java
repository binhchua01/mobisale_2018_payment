package isc.fpt.fsale.action;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.model.PackageModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// API lấy tổng tiền office 365
public class GetTotalPricePackageOffice implements
        AsyncTaskCompleteListener<String> {

    private Context mContext;
    private FragmentRegisterStep4 fragmentRegisterStep3;
    public final String TAG_METHOD_NAME = "GetTotalOffice365";

    private final String TAG_OBJEC_LIST = "ListObject",

    TAG_AMOUNT = "Amount", TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";

	/**/

    public GetTotalPricePackageOffice(Context context,
                                      Map<Integer, PackageModel> mapPackage) {
        // TODO Auto-generated constructor stub
        mContext = context;
        String message = "Đang cập nhật...";
        CallServiceTask service;
        try {
            JSONArray arr = new JSONArray();
            arr = objectToJSONArray(mapPackage);
            JSONObject json = new JSONObject();
            json.put("UserName", Constants.USERNAME);
            json.put("ListPackage", arr);

            service = new CallServiceTask(mContext, TAG_METHOD_NAME, json,
                    Services.JSON_POST_OBJECT, message,
                    GetTotalPricePackageOffice.this);
            service.execute();

        } catch (Exception e) {
            // TODO: handle exception

        }
    }

    public GetTotalPricePackageOffice(Context context,
                                      FragmentRegisterStep4 fragment,
                                      Map<Integer, PackageModel> mapPackage) {
        // TODO Auto-generated constructor stub
        mContext = context;
        fragmentRegisterStep3 = fragment;
        String message = "Đang cập nhật...";
        CallServiceTask service;
        try {
            JSONArray arr = new JSONArray();
            arr = objectToJSONArray(mapPackage);
            JSONObject json = new JSONObject();
            json.put("UserName", Constants.USERNAME);
            json.put("ListPackage", arr);

            service = new CallServiceTask(mContext, TAG_METHOD_NAME, json,
                    Services.JSON_POST_OBJECT, message,
                    GetTotalPricePackageOffice.this);
            service.execute();

        } catch (Exception e) {
            // TODO: handle exception

        }
    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub

        handleUpdateRegistration(result);

    }

    public void handleUpdateRegistration(String json) {
        if (json != null && Common.jsonObjectValidate(json)) {
            JSONObject jsObj = getJsonObject(json);
            if (jsObj != null) {
                try {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    if (jsObj.has(TAG_ERROR_CODE)
                            && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        JSONArray array = jsObj.getJSONArray(TAG_OBJEC_LIST);
                        JSONObject item = array.getJSONObject(0);
                        int Amount = 0;

                        if (item.has(TAG_AMOUNT))
                            Amount = item.getInt(TAG_AMOUNT);
                        try {
                            // Cập nhật tổng tiền thiết bị, tổng tiền trả trước
                            // và tổng tiền IPTV trên form PĐK
                            loadData(Amount);
                        } catch (Exception e) {
                            // Common.alertDialog("", mContext);

                        }

                    } else {
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                } catch (Exception e) {

                }
            } else {
                Common.alertDialog(
                        mContext.getResources().getString(
                                R.string.msg_error_data)
                                + "-" + Constants.RESPONSE_RESULT, mContext);
            }
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            JSONObject jsObj = JSONParsing.getJsonObj(result);
            return jsObj;
        } catch (Exception e) {
            // TODO: handle exception

        }
        return null;
    }

    private void loadData(int Amount) {
        try {
            if (fragmentRegisterStep3 != null) {
//                fragmentRegisterStep3.loadOfficePrice(Amount);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JSONArray objectToJSONArray(Map<Integer, PackageModel> mapPackage) {

        JSONArray array = new JSONArray();
        if (mapPackage != null) {
            try {
                for (int i = 0; i < mapPackage.size(); i++) {

                    PackageModel obj = (PackageModel) mapPackage.values()
                            .toArray()[i];
                    if (obj != null) {
                        array.put(obj.toJSONObject());
                    }
                }
            } catch (Exception e) {
                // TODO: handle exception

            }
        }

        return array;
    }
}
