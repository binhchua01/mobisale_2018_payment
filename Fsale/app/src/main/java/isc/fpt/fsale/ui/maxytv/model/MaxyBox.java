package isc.fpt.fsale.ui.maxytv.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class MaxyBox implements Parcelable {
    private int BoxID;
    private String BoxName;
    //1: Box onnet, 4: Box offnet
    private int BoxType;
    private int PromotionID;
    private String PromotionDesc;
    private int BoxCount;
    private int ServiceCode;
    private int Price;
    private int BoxFirst;

    private boolean isSelected = false;

    public MaxyBox(int boxID, String boxName, int boxType, int promotionID, String promotionDesc, int boxCount, int serviceCode, int price, boolean isSelected) {
        BoxID = boxID;
        BoxName = boxName;
        BoxType = boxType;
        PromotionID = promotionID;
        PromotionDesc = promotionDesc;
        BoxCount = boxCount;
        ServiceCode = serviceCode;
        Price = price;
        this.isSelected = isSelected;
    }

    public MaxyBox(int boxID, String boxName, int boxType, int promotionID, String promotionDesc, int boxCount, int serviceCode, int price, int boxFirst, boolean isSelected) {
        BoxID = boxID;
        BoxName = boxName;
        BoxType = boxType;
        PromotionID = promotionID;
        PromotionDesc = promotionDesc;
        BoxCount = boxCount;
        ServiceCode = serviceCode;
        Price = price;
        BoxFirst = boxFirst;
        this.isSelected = isSelected;
    }

    public int getBoxFirst() {
        return BoxFirst;
    }

    public void setBoxFirst(int boxFirst) {
        BoxFirst = boxFirst;
    }

    public static Creator<MaxyBox> getCREATOR() {
        return CREATOR;
    }

    public MaxyBox() {
    }


    protected MaxyBox(Parcel in) {
        BoxID = in.readInt();
        BoxName = in.readString();
        BoxType = in.readInt();
        PromotionID = in.readInt();
        PromotionDesc = in.readString();
        BoxCount = in.readInt();
        ServiceCode = in.readInt();
        Price = in.readInt();
        BoxFirst = in.readInt();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(BoxID);
        dest.writeString(BoxName);
        dest.writeInt(BoxType);
        dest.writeInt(PromotionID);
        dest.writeString(PromotionDesc);
        dest.writeInt(BoxCount);
        dest.writeInt(ServiceCode);
        dest.writeInt(Price);
        dest.writeInt(BoxFirst);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MaxyBox> CREATOR = new Creator<MaxyBox>() {
        @Override
        public MaxyBox createFromParcel(Parcel in) {
            return new MaxyBox(in);
        }

        @Override
        public MaxyBox[] newArray(int size) {
            return new MaxyBox[size];
        }
    };

    public int getBoxID() {
        return BoxID;
    }

    public void setBoxID(int boxID) {
        BoxID = boxID;
    }

    public String getBoxName() {
        return BoxName;
    }

    public void setBoxName(String boxName) {
        BoxName = boxName;
    }

    public int getBoxType() {
        return BoxType;
    }

    public void setBoxType(int boxType) {
        BoxType = boxType;
    }

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int promotionID) {
        PromotionID = promotionID;
    }

    public String getPromotionDesc() {
        return PromotionDesc;
    }

    public void setPromotionDesc(String promotionDesc) {
        PromotionDesc = promotionDesc;
    }

    public int getBoxCount() {
        return BoxCount;
    }

    public void setBoxCount(int boxCount) {
        BoxCount = boxCount;
    }

    public int getServiceCode() {
        return ServiceCode;
    }

    public void setServiceCode(int serviceCode) {
        ServiceCode = serviceCode;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public JSONObject toJsonObjectMaxyBox() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("BoxID", getBoxID());
            jsonObject.put("BoxName", getBoxName());
            jsonObject.put("PromotionID", getPromotionID());
            jsonObject.put("PromotionDesc", getPromotionDesc());
            jsonObject.put("BoxCount", getBoxCount());
            jsonObject.put("ServiceCode", getServiceCode());
            jsonObject.put("Price", getPrice());
            jsonObject.put("BoxFirst", getBoxFirst());
            jsonObject.put("BoxType", getBoxType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
