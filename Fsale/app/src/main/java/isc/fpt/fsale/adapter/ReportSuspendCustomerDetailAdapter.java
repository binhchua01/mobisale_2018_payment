package isc.fpt.fsale.adapter;

import isc.fpt.fsale.model.ReportSuspendCustomerDetailModel;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;
import isc.fpt.fsale.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReportSuspendCustomerDetailAdapter extends BaseAdapter {

	private ArrayList<ReportSuspendCustomerDetailModel> mList;	
	private Context mContext;
	
	public ReportSuspendCustomerDetailAdapter(Context context, ArrayList<ReportSuspendCustomerDetailModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportSuspendCustomerDetailModel item = mList.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_suspend_customer_detail, null);
		}
		if(item != null){
			TextView lblContract, lblFullName, lblSale, lblDate, lblLocalType, lblAmount, lblPhoneNumber, lblRowNumber;
			lblRowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			lblContract = (TextView)convertView.findViewById(R.id.lbl_contract);
			lblFullName = (TextView)convertView.findViewById(R.id.lbl_full_name);
			lblSale = (TextView)convertView.findViewById(R.id.lbl_sale);
			lblDate = (TextView)convertView.findViewById(R.id.lbl_date);
			lblLocalType = (TextView)convertView.findViewById(R.id.lbl_local_type);
			lblAmount = (TextView)convertView.findViewById(R.id.lbl_amount);
			lblPhoneNumber = (TextView)convertView.findViewById(R.id.lbl_phone_number);
			
			lblRowNumber.setText(String.valueOf(item.getRowNumber()));
			lblContract.setText(item.getContract());
			lblFullName.setText(item.getFullname());
			lblSale.setText(item.getSaleName() + "(" + item.getSale() + ")");
			lblDate.setText(item.getDate());
			lblLocalType.setText(item.getLocalTypeName());
			lblAmount.setText(Common.formatNumber(item.getAmount()) + " đồng");
			lblPhoneNumber.setText(item.getLocation_Phone());			
		}
		
		return convertView;
	}


}
