package isc.fpt.fsale.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.UpdatePotentialObj;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;

public class FragmentCreatePotentialObjMap extends Fragment implements OnMapReadyCallback {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_TITLE = "section_title";
    private GoogleMap mMap;
    private Marker currentMark;
    private Location mapLocation;
    private CreatePotentialObjActivity activity;
    private View rootView;

    public static FragmentCreatePotentialObjMap newInstance(int sectionNumber, String sectionTitle) {
        FragmentCreatePotentialObjMap fragment = new FragmentCreatePotentialObjMap();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentCreatePotentialObjMap() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_create_potential_obj_map, container, false);
        } catch (InflateException e) {
            e.printStackTrace();
        }
        activity = (CreatePotentialObjActivity) getActivity();
        setUpMapIfNeeded();
        //Add by: DuHK
        Button btnUpdate = (Button) rootView.findViewById(R.id.btn_update);

        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, 2);
                    return;
                }
                onImgUpdateSubmit();
            }
        });

        try {
            activity.enableSlidingMenu(false);
            Common.hideSoftKeyboard(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootView;
    }

    private void onImgUpdateSubmit() {
        if (checkForUpdate()) {
            AlertDialog.Builder builder;
            Dialog dialog;
            builder = new AlertDialog.Builder(activity);
            builder.setMessage(getResources().getString(R.string.msg_confirm_update))
                    .setCancelable(false)
                    .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            update();
                        }
                    })
                    .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            dialog = builder.create();
            dialog.show();
        }
    }

    protected boolean checkForUpdate() {
        if (currentMark == null) {
            Common.alertDialog("Không lấy được tọa độ", activity);
            return false;
        }
        if (activity.getCurrentPotentialObj() == null) {
            Common.alertDialog("Vui lòng nhập thông tin Khách hàng", activity);
            return false;
        }
        if (activity.getCurrentPotentialObj().getFullName() == null || activity.getCurrentPotentialObj().getFullName().equals("")) {
            Common.alertDialog("Vui lòng nhập họ tên Khách hàng", activity);
            return false;
        }
        return true;
    }

    public void update() {
        if (activity.getCurrentPotentialObj() != null) {
            String mLatLng = "(" + currentMark.getPosition().latitude + "," + currentMark.getPosition().longitude + ")";
            activity.getCurrentPotentialObj().setLatlng(mLatLng);
            String UserName = ((MyApp) activity.getApplication()).getUserName();
            new UpdatePotentialObj(activity, UserName, activity.getCurrentPotentialObj(), true);
            //Cập nhật & tiến hành khảo sát
        } else {
            Common.alertDialog("Chưa nhập thông tin khách hàng", activity);
        }
    }

    private void setUpMapIfNeeded() {
        if (mMap != null)
            setUpMap();
        if (mMap == null) {
            try {
                ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null)
            setUpMap();
        else {
            Common.alertDialog("Sorry! unable to create maps", activity);
        }
    }

    private void setUpMap() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);//An nut dinh vi
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                try {
                    getCurrentLocation();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });
        mMap.setOnMarkerDragListener(new OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) { }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                currentMark = marker;
            }

            @Override
            public void onMarkerDrag(Marker marker) { }
        });

        mMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {

            @Override
            public void onMyLocationChange(Location location) {
                mapLocation = location;
            }
        });

        try {
            if (mMap != null) {
                if (activity != null && activity.getCurrentPotentialObj() != null) {
                    if (activity.getCurrentPotentialObj().getLatlng() != null && !activity.getCurrentPotentialObj().getLatlng().equals("")) {
                        try {
                            String[] mLatLng = activity.getCurrentPotentialObj()
                                    .getLatlng().replace("(", "").replace(")", "").split(",");
                            LatLng curLocation = new LatLng(Double.valueOf(mLatLng[0]), Double.valueOf(mLatLng[1]));
                            drawMarker(curLocation);
                            MapCommon.animeToLocation(mMap, curLocation);
                            MapCommon.setMapZoom(mMap, 18);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        getCurrentLocation();
                    }
                } else {
                    getCurrentLocation();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCurrentLocation() {
        try {
            if (mMap != null) {
                GPSTracker gps = new GPSTracker(getActivity());
                Location location = gps.getLocation(true);

                if ((location == null && mapLocation != null)) {
                    location = mapLocation;
                }

                if (Common.distanceTwoLocation(location, mapLocation) > 10) {
                    location = mapLocation;
                }

                if (location != null) {
                    LatLng curLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    drawMarker(curLocation);
                    MapCommon.animeToLocation(mMap, curLocation);
                    MapCommon.setMapZoom(mMap, 18);
                } else {
                    Common.alertDialog("Không lấy được tạo độ hiện tại", getActivity());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void drawMarker(LatLng location) {
        try {
            if (mMap != null) {
                if (location != null) {
                    String title = "Vị trí hiện tại";
                    if (currentMark != null)
                        currentMark.remove();
                    currentMark = MapCommon.addMarkerOnMap(mMap, title, location, R.drawable.cuslocation_red, true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LatLng getLocation() {
        return currentMark != null ? currentMark.getPosition() : null;
    }


    @Override
    public void onDestroyView() {
        if (mMap != null) {
            try {
                if (CreatePotentialObjActivity.fragmentManager.findFragmentById(R.id.map) != null)
                    CreatePotentialObjActivity.fragmentManager.beginTransaction()
                            .remove(CreatePotentialObjActivity.fragmentManager.findFragmentById(R.id.map)).commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mMap = null;
        }
        super.onDestroyView();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = activity.getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(activity, message);
                return;
            }
        }
        if (requestCode == 2) {
            onImgUpdateSubmit();
            return;
        }
    }
}
