package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportPrecheckListActivity;
import isc.fpt.fsale.model.ReportPrecheckListModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

public class GetReportPrecheckListAction implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	private final String TAG_METHOD_NAME = "ReportPrecheckList";
	private final String TAG_LIST_SALARY = "ListObject";
	private final String TAG_ERROR_CODE = "ErrorCode";
	private final String TAG_ERROR = "Error";
	
	
	//Báo cáo phiếu đăng ký
	private ListReportPrecheckListActivity acitvity = null;
	
	public GetReportPrecheckListAction(Context context, String SaleAccount, int Day, int Month, int Year, int Agent, String AgentName, int PageNumber){
		try {
			mContext = context;			
			try {
				acitvity = (ListReportPrecheckListActivity)mContext;
			} catch (Exception e) {
				
				acitvity = null;
			}
			String[] paramNames = new String[]{"UserName", "Day", "Month", "Year", "Agent", "AgentName", "PageNumber"};
			String[] paramValues = new String[]{SaleAccount, String.valueOf(Day), String.valueOf(Month), String.valueOf(Year), String.valueOf(Agent), AgentName, String.valueOf(PageNumber)};			
			String message = "Đang lấy dữ liệu...";
			CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportPrecheckListAction.this);
			service.execute();
		} catch (Exception e) {

		}		
	}
	
	/*================= Reponse Example =================
	{
		ResponseResult: {
		Error: null
		ErrorCode: 0
		ListObject: [1]
			0:  {}
	}	 
	=====================================================*/
	public void handleUpdateRegistration(String result){
		ArrayList<ReportPrecheckListModel> lstObject = new ArrayList<ReportPrecheckListModel>();
		if(result != null && Common.jsonObjectValidate(result)){
		JSONObject jsObj = getJsonObject(result);
		if(jsObj != null && jsObj.has(Constants.RESPONSE_RESULT)){							
			try{	
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);	//Get sub object json
				if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0)
				{
					JSONArray jsArr = jsObj.getJSONArray(TAG_LIST_SALARY);						
					for(int i=0; i< jsArr.length(); i++){
						lstObject.add(ReportPrecheckListModel.Parse(jsArr.getJSONObject(i)));
					}
					if(acitvity != null)
						acitvity.loadData(lstObject);						
				}else{
					Common.alertDialog("Lỗi WS:" + jsObj.getString(TAG_ERROR), mContext);	
				}
			}catch (Exception e) {

				//Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + "-" + TAG_METHOD_NAME, mContext);
				//setSpinnerEmpty();
			}	
		}
		}
		
		
	}
	
	private JSONObject getJsonObject(String result){
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);		
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception

		}
		return null;
	}
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);			
	}
		
	

}
