package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.PotentialObjSurveyModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

public class UpdatePotentialObjDetailList implements AsyncTaskCompleteListener<String> {
	private Context mContext;
	public final String TAG_METHOD_NAME = "UpdatePotentialObjDetailList";
	private PotentialObjModel Potential;
	private String support;
	public UpdatePotentialObjDetailList(Context context, String UserName, PotentialObjModel obj, List<PotentialObjSurveyModel> surverys) {	
		mContext = context;
		String message = "Đang cập nhật...";
		CallServiceTask service;
		try {
			Potential = obj;
			JSONArray arr = new JSONArray();
			for(PotentialObjSurveyModel item: surverys){
				arr.put(item.toJSONObject());
			}
			JSONObject json = new JSONObject();
			json.put("UserName", UserName);
			json.put("PotentialObjID", obj.getID());
			json.put("Surveys", arr);
			service = new CallServiceTask(mContext, TAG_METHOD_NAME, json, Services.JSON_POST_OBJECT, message, UpdatePotentialObjDetailList.this);
			service.execute();	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();			
		}
	}
	public UpdatePotentialObjDetailList(Context context, String UserName, PotentialObjModel obj, List<PotentialObjSurveyModel> surverys, String support) {
		mContext = context;
		String message = "Đang cập nhật...";
		CallServiceTask service;
		try {
			Potential = obj;
			this.support = support;
			JSONArray arr = new JSONArray();
			for(PotentialObjSurveyModel item: surverys){
				arr.put(item.toJSONObject());
			}
			JSONObject json = new JSONObject();
			json.put("UserName", UserName);
			json.put("PotentialObjID", obj.getID());
			json.put("Surveys", arr);
			service = new CallServiceTask(mContext, TAG_METHOD_NAME, json, Services.JSON_POST_OBJECT, message, UpdatePotentialObjDetailList.this);
			service.execute();

		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
	}
	
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(jsObj, UpdResultModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						if(resultObject.getListObject() != null && resultObject.getListObject().size() >0){
							UpdResultModel item = resultObject.getListObject().get(0);
							if(item.getResultID() >0){
							 new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(item.getMessage())
				 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
				 				    @Override
				 				    public void onClick(DialogInterface dialog, int which) {		  	
				 				    	try {
				 				    		String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
				 				    		new GetPotentialObjDetail(mContext, userName, Potential.getID(), support);
					 				    	dialog.cancel();
										} catch (Exception e) {

											e.printStackTrace();
										}
				 				      
				 				    }
				 				})
			 					.setCancelable(false).create().show();
							}else{
								Common.alertDialog(item.getMessage(), mContext);
							}
						}else{
							Common.alertDialog("Không có dữ liệu trả về!", mContext);
						}
							
						
						//Common.alertDialog(lst.get(0).getMessage(), mContext);
					 }else{//ServiceType Error
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			}
		} catch (JSONException e) {

			Log.i("UpdatePotentialObjDetailList:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
}
