package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.InsertInvestiageAction;
import isc.fpt.fsale.action.UploadImage;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.map.activity.CapturePhotoActivity;
import isc.fpt.fsale.map.activity.ExploreMapActivity;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class InvestiageActivity extends BaseActivity {

    public InvestiageActivity() {
        super(R.string.lbl_investigate);
    }

    private static final int SELECT_PICTURE = 1;
    private String selectedImagePath;

    private Button btnSubmit;
    private Button btnExit;
    private ImageView btnMapView;
    private Context mContext;
    private RegistrationDetailModel modelDetail;
    private TextView txtRegCode, txtCustomer, txtAddress, txtListPoint,
            txtImagePath;
    private EditText txtOutdoor, txtIndoor;
    private Spinner spCableType, spModem;
    private KeyValuePairAdapter adapter;
    private String sPathNamePicker;
    private Uri selectedImageUri;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_investiage_activity));
        this.mContext = InvestiageActivity.this;
        setContentView(R.layout.activity_investigate);

        txtRegCode = (TextView) findViewById(R.id.txt_registration_id);
        txtCustomer = (TextView) findViewById(R.id.txt_customer_name);
        txtAddress = (TextView) findViewById(R.id.txt_address);
        txtOutdoor = (EditText) findViewById(R.id.txt_outdoor_long);
        txtIndoor = (EditText) findViewById(R.id.txt_indoor_long);
        txtListPoint = (TextView) findViewById(R.id.txt_list_point);

        txtImagePath = (TextView) findViewById(R.id.txt_image_path);
        spModem = (Spinner) findViewById(R.id.sp_modem);
        spCableType = (Spinner) findViewById(R.id.sp_cable_type);
        btnSubmit = (Button) findViewById(R.id.btn_investigate);
        btnExit = (Button) findViewById(R.id.btn_Exit);
        btnExit.setVisibility(View.GONE);
        btnMapView = (ImageView) findViewById(R.id.btn_MapView);
        btnExit.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        btnMapView.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                try {
                    Intent intent = new Intent(mContext, ExploreMapActivity.class);
                    intent.putExtra(Constants.MODEL_REGISTER, modelDetail);
                    intent.putExtra("IndoorCable", ((KeyValuePairModel)
                            spCableType.getSelectedItem()).getDescription());
                    intent.putExtra("IndoorLog", txtIndoor.getText().toString());
                    intent.putExtra("OutdoorLog", txtOutdoor.getText().toString());
                    intent.putExtra("OutdoorCable", ((KeyValuePairModel)
                            spCableType.getSelectedItem()).getDescription());
                    mContext.startActivity(intent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        // nút KHẢO SÁT
        btnSubmit.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (checkForUpdate()) {
                    update();
                }
            }
        });

        spCableType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                txtOutdoor.setFocusable(true);
                txtOutdoor.requestFocus();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        setModemList();
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @SuppressLint("LongLogTag")
    private void getDataFromIntent() {
        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                if (myIntent.hasExtra(Constants.MODEL_REGISTER)
                        && myIntent.getParcelableExtra(Constants.MODEL_REGISTER) != null) {
                    modelDetail = myIntent.getParcelableExtra(Constants.MODEL_REGISTER);
                }
                if (modelDetail != null) {
                    txtCustomer.setText(modelDetail.getFullName());
                    txtAddress.setText(modelDetail.getAddress());
                    // Phiếu đăng ký
                    txtRegCode.setText(modelDetail.getRegCode());
                    txtListPoint.setText(modelDetail.getTDName());
                    txtOutdoor
                            .setText(String.valueOf(modelDetail.getOutDoor()));
                    txtIndoor.setText(String.valueOf(modelDetail.getInDoor()));
                    txtImagePath.setText(modelDetail.getImage());
                    if (spModem.getAdapter() != null) {
                        spModem.setSelection(Common.getIndex(spModem, modelDetail.getModem()));
                    } else {
                        setModemList();
                    }

                    int bookPortType = modelDetail.getBookPortType();
                    switch (bookPortType) {
                        case 0:
                            setCableList(1);
                            break;
                        case 1:
                            setCableList(2); // FTTH
                            break;
                        default:
                            break;
                    }

                    if (modelDetail.getTDName() != null && modelDetail.getTDName().contains("/")) {
                        setCableList(3);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkForUpdate() {
        try {
            if (txtOutdoor.getText().toString().equals("")) {
                Common.alertDialog("Chưa nhập mét cáp outdoor!", mContext);
                return false;
            }
            if (txtIndoor.getText().toString().equals("")) {
                Common.alertDialog("Chưa nhập mét cáp indoor!", mContext);
                return false;
            }
            int inDoor = Integer.valueOf(txtIndoor.getText().toString());
            int outDoor = Integer.valueOf(txtOutdoor.getText().toString());
            if (inDoor <= 0) {
                txtIndoor.setError("Số mét cáp phải lớn hơn 0!");
                txtIndoor.requestFocus();
                txtIndoor.setFocusable(true);
                return false;
            }
            if (outDoor <= 0) {
                txtOutdoor.setError("Số mét cáp phải lớn hơn 0!");
                txtOutdoor.requestFocus();
                txtOutdoor.setFocusable(true);
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void update() {
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(mContext);
        builder.setMessage(
                mContext.getResources().getString(R.string.msg_confirm_update))
                .setCancelable(false)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        investOrUpdateImage();
                    }
                })
                .setNegativeButton("Không",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        dialog = builder.create();
        dialog.show();
    }

    private void investOrUpdateImage() {
        if (modelDetail != null) {
            try {
                String UserName = ((MyApp) (InvestiageActivity.this
                        .getApplicationContext())).getUserName();
                String Image = txtImagePath.getText().toString().trim();
                if (!Image.equals("") && !Image.equals(modelDetail.getImage())) {
                    File imageFile = new File(Image);
                    Bitmap bm = Common.decodeSampledBitmapFromFile(imageFile.getPath());
                    String sBitmap = Common.ScaleBitmap(bm);
                    new UploadImage(mContext, new String[]{sBitmap, UserName, "", "3"});
                } else {
                    updateInvestiage(modelDetail.getImage());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //kết nối API khảo sát
    public void updateInvestiage(String imagePath) {
        if (modelDetail != null) {
            try {
                int outDType, inDType, modemType, outDoor, inDoor;
                outDType = inDType = ((KeyValuePairModel) spCableType.getSelectedItem()).getID();
                outDoor = Integer.valueOf(txtOutdoor.getText().toString());
                inDoor = Integer.valueOf(txtIndoor.getText().toString());
                modemType = ((KeyValuePairModel) spModem.getSelectedItem()).getID();

                new InsertInvestiageAction(this, modelDetail.getID(),
                        modelDetail.getRegCode(), outDType, outDoor, inDType,
                        inDoor, imagePath, "", modemType,
                        modelDetail.getTDName(), Constants.USERNAME,
                        modelDetail.getLatlng());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setModemList() {
        ArrayList<KeyValuePairModel> lstModem = new ArrayList<>();
        lstModem.add(new KeyValuePairModel(0, "Không lấy modem"));
        lstModem.add(new KeyValuePairModel(1, "Modem thuê"));
        lstModem.add(new KeyValuePairModel(2, "Modem tặng"));
        lstModem.add(new KeyValuePairModel(3, "Modem bán"));
        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstModem, Gravity.LEFT);
        spModem.setAdapter(adapter);
    }

    public void setCableList(int bookPortType) {
        ArrayList<KeyValuePairModel> lstCable = new ArrayList<>();
        switch (bookPortType) {
            case 1: // ADSL
                lstCable.add(new KeyValuePairModel(812, "0.5 mm"));
                break;
            case 2: // FTTH
                lstCable.add(new KeyValuePairModel(62, "2 core"));
                break;
            case 3: // FTTH new
                lstCable.add(new KeyValuePairModel(52, "1 core"));
                break;
            default:
                lstCable.add(new KeyValuePairModel(0, "[ Vui lòng chọn loại ]"));
                lstCable.add(new KeyValuePairModel(52, "1 core"));
                lstCable.add(new KeyValuePairModel(62, "2 core"));
                lstCable.add(new KeyValuePairModel(72, "4 core"));
                lstCable.add(new KeyValuePairModel(812, "0.5 mm"));
                break;
        }
        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstCable, Gravity.LEFT);
        // Loại cáp
        spCableType.setAdapter(adapter);
        try {
            if (modelDetail != null) {
                spCableType.setSelection(Common.getIndex(spCableType, modelDetail.getOutDType()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                selectedImageUri = data.getData();
                selectedImagePath = Common.getRealPathUrlImage(mContext, selectedImageUri);
                sPathNamePicker = selectedImagePath;
                if (sPathNamePicker == null || sPathNamePicker.equals("")) {
                    Common.alertDialog("Không thể chọn ảnh này!", this);
                }
                txtImagePath.setText(selectedImagePath);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

        if (intent.hasExtra(CapturePhotoActivity.TAG_IMAGE_PATH) &&
                !intent.getStringExtra(CapturePhotoActivity.TAG_IMAGE_PATH).equals("")) {
            sPathNamePicker = intent.getStringExtra(CapturePhotoActivity.TAG_IMAGE_PATH);
            txtImagePath.setText(sPathNamePicker);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}