package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.DeletePotentialObjSchedule;
import isc.fpt.fsale.activity.PotentialObjScheduleList;
import isc.fpt.fsale.model.PotentialSchedule;

/**
 * Created by HCM.TUANTT14 on 10/12/2017.
 */

public class PotentialObjScheduleAdapter extends BaseAdapter {
    private List<PotentialSchedule> mList;
    private Context mContext;
    private PotentialObjScheduleList potentialObjScheduleList;
    public PotentialObjScheduleAdapter(Context context, List<PotentialSchedule> lst) {
        this.mContext = context;
        this.potentialObjScheduleList =(PotentialObjScheduleList)context;
        this.mList = lst;
    }

    public void clearAll() {
        this.mList.clear();
        this.notifyDataSetChanged();
    }

    public void setListData(List<PotentialSchedule> lst) {
        this.mList.clear();
        this.mList.addAll(lst);
        this.notifyDataSetChanged();
    }

    public void addAll(List<PotentialSchedule> lst) {
        this.mList.addAll(lst);
        this.notifyDataSetChanged();
    }

    public List<PotentialSchedule> getListData() {
        return mList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (mList != null)
            return mList.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        if (mList != null)
            return mList.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final PotentialSchedule item = mList.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.row_potentail_schedule_obj, null);
        }
        if (item != null) {
            TextView lblContentSchedule = (TextView) convertView
                    .findViewById(R.id.lbl_content_schedule);
            lblContentSchedule.setText(item.getScheduleDescription());
            TextView lblTimeSchedule = (TextView) convertView
                    .findViewById(R.id.lbl_time_schedule);
            lblTimeSchedule.setText(item.getScheduleDate());

            TextView lblCancelSchedule = (TextView) convertView
                    .findViewById(R.id.img_cancel_potential_schedule);
            lblCancelSchedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmRemovePotentialSchedule(item.getPotentialObjScheduleID(),position);
                }
            });
        }
        return convertView;
    }

    private void confirmRemovePotentialSchedule(final int potentialObjScheduleID, final int position) {
        try {
            AlertDialog.Builder builder = null;
            Dialog dialog = null;
            builder = new AlertDialog.Builder(mContext);
            builder.setMessage(
                    "Bạn muốn xóa lịch hẹn này?")
                    .setCancelable(false)
                    .setPositiveButton(
                            mContext.getResources().getString(R.string.lbl_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    // kết nối API xóa lịch hẹn KHTN
                                    new DeletePotentialObjSchedule(mContext,potentialObjScheduleID,position);
                                }
                            })
                    .setNegativeButton(
                            mContext.getResources().getString(R.string.lbl_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });

            dialog = builder.create();
            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
