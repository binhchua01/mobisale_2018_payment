package isc.fpt.fsale.model;

public class QRCodeResult {
    //Declares variables
    private int code;
    private String link;
    private String qrcode;

    //Constructor
    public QRCodeResult(){}

    //Getters and Setters
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }
}
