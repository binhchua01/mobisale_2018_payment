package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hau Le on 2018-12-27.
 */
public class CameraType implements Parcelable {
    private int TypeID;
    private String TypeName;
    private boolean isSelected = false;

    public CameraType() {
    }

    public CameraType(int typeID, String typeName, boolean isSelected) {
        TypeID = typeID;
        TypeName = typeName;
        this.isSelected = isSelected;
    }

    protected CameraType(Parcel in) {
        TypeID = in.readInt();
        TypeName = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<CameraType> CREATOR = new Creator<CameraType>() {
        @Override
        public CameraType createFromParcel(Parcel in) {
            return new CameraType(in);
        }

        @Override
        public CameraType[] newArray(int size) {
            return new CameraType[size];
        }
    };

    public int getTypeID() {
        return TypeID;
    }

    public void setTypeID(int typeID) {
        TypeID = typeID;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(TypeID);
        parcel.writeString(TypeName);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
    }
}
