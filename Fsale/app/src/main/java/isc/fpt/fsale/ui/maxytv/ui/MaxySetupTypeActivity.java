package isc.fpt.fsale.ui.maxytv.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.maxy.GetSetupMaxy;
import isc.fpt.fsale.action.maxy.GetTypeDeploy;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.adapter.MaxySetupTypeAdapter;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyTypeDeployAdapter;
import isc.fpt.fsale.ui.maxytv.model.MaxySetupType;
import isc.fpt.fsale.ui.maxytv.model.MaxyTypeDeploy;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.LOCAL_TYPE;
import static isc.fpt.fsale.utils.Constants.REGTYPE;
import static isc.fpt.fsale.utils.Constants.TYPE_DEPLOY;

public class MaxySetupTypeActivity extends BaseActivitySecond implements OnItemClickListener<MaxySetupType> {
    private View loBack;
    private List<MaxySetupType> mList;
    private MaxySetupTypeAdapter mAdapter;
    private String mClassName;
    private int RegType, localType, typeDeploy;

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_setup_type;
    }

    @Override
    protected void initView() {
        getDataIntent();
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.lbl_screen_name_setup_type));
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_setup_type);
        mAdapter = new MaxySetupTypeAdapter(this, mList != null ? mList : new ArrayList<>(), this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        RegType = bundle.getInt(REGTYPE);
        localType = bundle.getInt(LOCAL_TYPE);
        typeDeploy = bundle.getInt(TYPE_DEPLOY);
        if (bundle != null && bundle.getString(Constants.CLASS_NAME) != null) {
            mClassName = bundle.getString(Constants.CLASS_NAME);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(mClassName)) {
            new GetSetupMaxy(this, RegType, localType, "", typeDeploy);
        }
    }

    public void loadData(List<MaxySetupType> lst) {
        this.mList = lst;
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(MaxySetupType object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.OBJECT_MAXY_SETUP_TYPE, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
