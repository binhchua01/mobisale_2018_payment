package isc.fpt.fsale.model;
import java.lang.reflect.Field;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class MessageModel implements Parcelable{

	    private Long id;
	    private String message;
	    private String image;
	    private String category;
	    private String sendfrom;
	    private String message_id;
	    private String to;
	    private String sendFromRegID;
	    private String datetime;
	    private Boolean isRead = false;
	    private Boolean isBookmark = false;
	    private Boolean isDelete = false;
	    private String title;
	    public MessageModel() {
	    	isRead = false;
	    	isBookmark = false;
	    	isDelete = false;
	    }
	    public MessageModel(Long id) {
	        this.id = id;
	        isRead = false;
	    	isBookmark = false;
	    	isDelete = false;
	    }
	    public MessageModel(Long id, String message, String image, String category, String sendfrom, String message_id, String to, String sendFromRegID, String datetime, Boolean isRead, Boolean isBookmark, Boolean isDelete, String title) {
	        this.id = id;
	        this.message = message;
	        this.image = image;
	        this.category = category;
	        this.sendfrom = sendfrom;
	        this.message_id = message_id;
	        this.to = to;
	        this.sendFromRegID = sendFromRegID;
	        this.datetime = datetime;
	        this.isRead = isRead;
	        this.isBookmark = isBookmark;
	        this.isDelete = isDelete;
	        this.title = title;
	    }
	    public Long getId() {
	        return id;
	    }

	    public void setId(Long id) {
	        this.id = id;
	    }

	    public String getMessage() {
	        return message;
	    }

	    public void setMessage(String message) {
	        this.message = message;
	    }

	    public String getImage() {
	        return image;
	    }

	    public void setImage(String image) {
	        this.image = image;
	    }

	    public String getCategory() {
	        return category;
	    }

	    public void setCategory(String category) {
	        this.category = category;
	    }

	    public String getSendfrom() {
	        return sendfrom;
	    }

	    public void setSendfrom(String sendfrom) {
	        this.sendfrom = sendfrom;
	    }

	    public String getMessage_id() {
	        return message_id;
	    }

	    public void setMessage_id(String message_id) {
	        this.message_id = message_id;
	    }

	    public String getTo() {
	        return to;
	    }

	    public void setTo(String to) {
	        this.to = to;
	    }

	    public String getSendFromRegID() {
	        return sendFromRegID;
	    }

	    public void setSendFromRegID(String sendFromRegID) {
	        this.sendFromRegID = sendFromRegID;
	    }

	    public String getDatetime() {
	        return datetime;
	    }

	    public void setDatetime(String datetime) {
	        this.datetime = datetime;
	    }

	    public Boolean getIsRead() {
	        return isRead==null ? false : isRead;
	    }

	    public void setIsRead(Boolean isRead) {
	        this.isRead = isRead;
	    }

	    public Boolean getIsBookmark() {
	        return isBookmark==null ? false :isBookmark;
	    }

	    public void setIsBookmark(Boolean isBookmark) {
	        this.isBookmark = isBookmark;
	    }

	    public Boolean getIsDelete() {
	        return isDelete == null ? false :isDelete;
	    }

	    public void setIsDelete(Boolean isDelete) {
	        this.isDelete = isDelete;
	    }
	    public String getTitle() {
	        return title;
	    }

	    public void setTitle(String title) {
	        this.title = title;
	    }
	public JSONObject toJSONObject()throws Exception {	
    	JSONObject jsonObj = new JSONObject();
    	try {    
    		Field[] fields = getClass().getDeclaredFields();  
    		for (Field f : fields) {
        		f.setAccessible(true);
        		jsonObj.put(f.getName(),f.get(this));
    		}
		} catch (Exception e) {

		}
    	
    	return jsonObj;    			
    }
	@Override
	public void writeToParcel(Parcel pc, int flags) {
		// TODO Auto-generated method stub
		pc.writeString(this.sendFromRegID);
		pc.writeString(this.category);
		pc.writeString(this.message);
		pc.writeString(this.image);
		pc.writeString(this.datetime);
		pc.writeString(this.sendfrom);
		pc.writeString(this.to);
		pc.writeString(this.message_id);
		pc.writeByte((byte) (isBookmark ? 1 : 0));
		pc.writeByte((byte) (isDelete ? 1 : 0));
		pc.writeByte((byte) (isRead ? 1 : 0));
		pc.writeLong(this.id);
		pc.writeString(this.title);
	}
	
	public MessageModel(Parcel source) {
		sendFromRegID = source.readString();
		category = source.readString();
		message = source.readString();
		image = source.readString();
		datetime = source.readString();
		sendfrom = source.readString();
		to = source.readString();
		message_id = source.readString();
		isBookmark = source.readByte() != 0; 
		isDelete = source.readByte() != 0; 
		isRead = source.readByte() != 0; 
		id = source.readLong();
		title = source.readString();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public static final Creator<MessageModel> CREATOR = new Creator<MessageModel>() {
		@Override     
		public MessageModel createFromParcel(Parcel source) {
	             return new MessageModel(source);
		}
		@Override
		public MessageModel[] newArray(int size) {
             return new MessageModel[size];
        }
	};
}
