package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.UpdateInternetReceiptActivity;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractTotal;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.model.GiftBox;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 18,June,2019
 */
public class GetListGiftBox implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterStep4 mFragment;
    private UpdateInternetReceiptActivity mActivity;
    private FragmentRegisterContractTotal mFragTotal;
    private final String GET_LIST_GIFT_BOX = "GetListGiftBox";

    //bán mới
    public GetListGiftBox(Context mContext, FragmentRegisterStep4 mFragment, JSONObject jsonObject) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        String message = mContext.getResources().getString(R.string.msg_pd_gift_info);
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_GIFT_BOX, jsonObject,
                Services.JSON_POST_OBJECT, message, GetListGiftBox.this);
        service.execute();
    }

    //cập nhật tổng tiền
    public GetListGiftBox(Context mContext, UpdateInternetReceiptActivity mActivity, JSONObject jsonObject) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        String message = mContext.getResources().getString(R.string.msg_pd_gift_info);
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_GIFT_BOX, jsonObject,
                Services.JSON_POST_OBJECT, message, GetListGiftBox.this);
        service.execute();
    }

    //bán thêm
    public GetListGiftBox(Context mContext, FragmentRegisterContractTotal mFragTotal, JSONObject jsonObject) {
        this.mContext = mContext;
        this.mFragTotal = mFragTotal;
        String message = mContext.getResources().getString(R.string.msg_pd_gift_info);
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_GIFT_BOX, jsonObject,
                Services.JSON_POST_OBJECT, message, GetListGiftBox.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<GiftBox> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), GiftBox.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mActivity != null) {
                        mActivity.loadDataSpGiftBox(lst);
                    } else if (mFragTotal != null) {
                        mFragTotal.loadDataSpGiftBox(lst);
                    } else {
                        mFragment.loadDataSpGiftBox(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
