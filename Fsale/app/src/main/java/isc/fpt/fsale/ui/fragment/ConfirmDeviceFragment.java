package isc.fpt.fsale.ui.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListDeviceConfirm;
import isc.fpt.fsale.activity.ReceiveDeviceActivity;
import isc.fpt.fsale.adapter.AdapterConfirmDevice;
import isc.fpt.fsale.adapter.SpinDeviceTypeConfirmAdapter;
import isc.fpt.fsale.model.DeviceConfirm;
import isc.fpt.fsale.model.DeviceTypeConfirm;
import isc.fpt.fsale.model.ListDeviceConfirm;
import isc.fpt.fsale.utils.MyApp;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

/**
 * Created by haulc3 on 16,April,2019
 */
public class ConfirmDeviceFragment extends Fragment
        implements AdapterConfirmDevice.CallbackDeviceConfirm {
    private Spinner spnDeviceTypeConfirm;
    private ReceiveDeviceActivity activity;
    private SpinDeviceTypeConfirmAdapter spnAdapter;
    private DeviceTypeConfirm mDeviceTypeConfirm;
    public int typeId;
    private RecyclerView mRecyclerView;
    public List<ListDeviceConfirm> list;
    private AdapterConfirmDevice mAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    public boolean isLastPage = false;
    public boolean isLoadMore = true;
    public int pageCurrent = 1;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ReceiveDeviceActivity) {
            this.activity = (ReceiveDeviceActivity) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_confirm_device, container, false);
        bindView(view);
        return view;
    }

    private void bindView(View view) {
        spnDeviceTypeConfirm = (Spinner) view.findViewById(R.id.spinner_type_search_confirm);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_confirm_device);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initEvent();
    }

    private void initEvent() {

    }

    private void initData() {
        //init data to spinner device type confirm
        spnAdapter = new SpinDeviceTypeConfirmAdapter(activity,
                R.layout.item_spinner, R.id.text_view_0, activity.list) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textview = (TextView) view;
                if (position == 0) {
                    textview.setTextColor(Color.GRAY);
                } else {
                    textview.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spnDeviceTypeConfirm.setAdapter(spnAdapter);

        spnDeviceTypeConfirm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                mDeviceTypeConfirm = spnAdapter.getItem(position);
                assert mDeviceTypeConfirm != null;
                typeId = mDeviceTypeConfirm.getTypeID();
                getListDeviceConfirm();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        list = new ArrayList<>();
        mAdapter = new AdapterConfirmDevice(getContext(),
                (ArrayList<ListDeviceConfirm>) list, this, 0);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        //load more
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLinearLayoutManager.getChildCount();
                int totalItemCount = mLinearLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();

                if (!isLastPage && isLoadMore) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {
                        isLoadMore = false;
                        pageCurrent++;
                        //call api get list device confirm by type id
                        getListDeviceConfirm();
                    }
                }
            }
        });
    }

    public void getListDeviceConfirm() {
        //chưa chọn loại thiết bị cần xác nhận
        if (typeId == -1) return;

        //call api get list device confirm by type id
        new GetListDeviceConfirm(activity, ConfirmDeviceFragment.this,
                ((MyApp) getActivity().getApplication()).getUserName(),
                typeId, 1, pageCurrent);
    }

    public void listDeviceConfirm(ArrayList<ListDeviceConfirm> list) {
        if (list.size() == 0) {
            isLastPage = true;
        }
        isLoadMore = true;
        this.list.addAll(list);
        mAdapter.notifyDataChanged((ArrayList<ListDeviceConfirm>) this.list);
    }

    @Override
    public void deviceConfirm(ArrayList<DeviceConfirm> listDevice) {
    }
}
