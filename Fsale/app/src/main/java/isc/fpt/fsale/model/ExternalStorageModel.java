package isc.fpt.fsale.model;

import isc.fpt.fsale.utils.Constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import net.hockeyapp.android.ExceptionHandler;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;


public class ExternalStorageModel {
	private static boolean mExternalStorageAvailable = false;
    private static boolean mExternalStorageWriteable = false;
	
    private final String RootFolder = "/" + Constants.DIRECTORY_ROOT;
    private File mFile;
    private String mDirName = "", mFileName ="";
    
    public ExternalStorageModel(String dirName, String fileName){    	
    	//this.mFile = createFile(dirName, fileName);
    	this.mDirName = dirName;
    	this.mFileName = fileName;
    	this.mFile =  new File(Environment.getExternalStorageDirectory().getAbsolutePath() + RootFolder + "/" + mDirName +"/"+ mFileName);
    }
    public void createNewFile(boolean override){
    	mFile = createFile(mDirName, mFileName, override);
    }
   
	private void checkExternalMedia(){	    
	    String state = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(state)) {
	        // Can read and write the media
	        mExternalStorageAvailable = mExternalStorageWriteable = true;
	    } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
	        // Can only read the media
	        mExternalStorageAvailable = true;
	        mExternalStorageWriteable = false;
	    } else {
	        // Can't read or write
	        mExternalStorageAvailable = mExternalStorageWriteable = false;
	    }   
	   
	}
	
	public boolean isExists(){
		return mFile.exists();
	}
	private boolean checkWriteable(){
		checkExternalMedia();
		return (mExternalStorageAvailable == true && mExternalStorageWriteable == true);
	}
	
	public boolean checkReadable(){
		checkExternalMedia();
		return mExternalStorageAvailable;
	}
	
	private File createFile(String dirName,String fileName, boolean override ){
		//String dirPath = "";		
		if(checkWriteable()){
			if(createDirectory(dirName,true)!=null)
				try{				
					if(mFile.exists() && override == true){
			    		mFile.delete();
			    		mFile.createNewFile();
			    	}else
						mFile.createNewFile();				
					return mFile;						
				}catch(Exception ex){

					Log.e("LOG_TAG_FESTIVAL_IMAGE_RESULT", ex.getMessage());
				}	
		}
		return null;
	}
	
	private File createDirectory(String dirName, boolean OpenOrCreate){	
		File subDir = null;
		File dir = new File (Environment.getExternalStorageDirectory().getAbsolutePath() + RootFolder);
		try{
			//Tao thu muc ROOT cua MobiPay(/MobiPay)
			if(!dir.exists() || !dir.isDirectory())
				dir.mkdirs();					
					//Tao thu muc con(trong ROOT MobiPay)
			subDir = new File(dir.getAbsolutePath()+"/"+dirName);
			if(!subDir.exists() || !subDir.isDirectory())
				subDir.mkdirs();
				
		}catch(Exception ex){

			subDir = null;
		}		
		return subDir;
	}
	
	public boolean writeFile(String value, boolean override){

	    /* Find the root of the external storage.
	    // See http://developer.android.com/guide/topics/data/data-  storage.html#filesExternal

	    File root = Environment.getExternalStorageDirectory(); 	    
	    // See http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder
	    File dir = new File (root.getAbsolutePath() + "/download");
	    dir.mkdirs();
	    File file = new File(dir, "myData.txt");*/
		if(checkWriteable()){
		    if(mFile == null || mFile.isDirectory())
		    	return false;
		    try {
		    	if(mFile.exists() && override == true){
		    		mFile.delete();
		    		mFile.createNewFile();
		    	}
		    		
		        FileOutputStream stream = new FileOutputStream(mFile);
		        PrintWriter pw = new PrintWriter(stream);
		        pw.println(value);	       
		        pw.flush();
		        pw.close();
		        stream.close();
		        return true;
		    } catch (FileNotFoundException e) {

		        e.printStackTrace();
		        Log.i("TAG", "******* File not found. Did you" +
		                " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");		        
		    } catch (IOException e) {

		        e.printStackTrace();		       
		    }   
		}
	    return false;	   
	}
	
	public boolean writeFile(Bitmap bmp, boolean override){

	    /* Find the root of the external storage.
	    // See http://developer.android.com/guide/topics/data/data-  storage.html#filesExternal

	    File root = Environment.getExternalStorageDirectory(); 	    
	    // See http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder
	    File dir = new File (root.getAbsolutePath() + "/download");
	    dir.mkdirs();
	    File file = new File(dir, "myData.txt");*/
		if(checkWriteable()){
		    if(mFile == null || mFile.isDirectory())
		    	return false;
		    try {
		    	if(mFile.exists() && override == true){
		    		mFile.delete();
		    		mFile.createNewFile();
		    	}
		        FileOutputStream stream = new FileOutputStream(mFile);
		        bmp.compress(CompressFormat.PNG, 80, stream);
		        stream.flush();
		        stream.close();		        
		        return true;
		    } catch (FileNotFoundException e) {

		        e.printStackTrace();
		        Log.i("TAG", "******* File not found. Did you" +
		                " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");		        
		    } catch (IOException e) {

		        e.printStackTrace();		       
		    }   
		}
	    return false;	   
	}
	public String readTextFile(){		
		String result = null;
		StringBuffer output = null;	   
	    BufferedReader br = null;
	    if(checkReadable()){
		    if(mFile == null || !mFile.exists() || mFile.isDirectory())
		    	result = null;
		    else{
			    try {
			    	output = new StringBuffer();	
					br = new BufferedReader(new FileReader(mFile)); 
					String line = "";
					while ((line = br.readLine()) != null) {
				          output.append(line);
				        }
					br.close();
					result = output.toString();
				} catch (Exception e) {

					e.printStackTrace();
					result = null;
				}
		    }
	    }
	    return result;
	}
	
	public Bitmap readBitmapFile(){		
		Bitmap result = null;
	    //BufferedReader br = null;
	    if(checkReadable()){
		    if(mFile == null || !mFile.exists() || mFile.isDirectory())
		    	result = null;
		    else{
			    try {
			    	result = BitmapFactory.decodeFile(mFile.getAbsolutePath());				
				} catch (Exception e) {

					e.printStackTrace();
					result = null;
				}
		    }
	    }
	    return result;
	   
	}
	
}
