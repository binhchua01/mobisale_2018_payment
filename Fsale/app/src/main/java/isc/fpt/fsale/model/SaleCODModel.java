package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SaleCODModel implements Parcelable {
    private String Link;
    private String Message;
    private String Title;

    public SaleCODModel(String link, String message, String title) {
        Link = link;
        Message = message;
        Title = title;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public static Creator<SaleCODModel> getCREATOR() {
        return CREATOR;
    }


    public SaleCODModel() {
    }

    protected SaleCODModel(Parcel in) {
        Link = in.readString();
        Message = in.readString();
        Title = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Link);
        dest.writeString(Message);
        dest.writeString(Title);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SaleCODModel> CREATOR = new Creator<SaleCODModel>() {
        @Override
        public SaleCODModel createFromParcel(Parcel in) {
            return new SaleCODModel(in);
        }

        @Override
        public SaleCODModel[] newArray(int size) {
            return new SaleCODModel[size];
        }
    };

}
