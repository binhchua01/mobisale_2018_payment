package isc.fpt.fsale.ui.maxytv.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MaxySetupType implements Parcelable {
    private int TypeSetupID;
    private String TypeSetupName;

    private boolean isSelected = false;

    public MaxySetupType(int typeSetupID, String typeSetupName) {
        TypeSetupID = typeSetupID;
        TypeSetupName = typeSetupName;
    }

    public MaxySetupType(int typeSetupID, String typeSetupName, boolean isSelected) {
        TypeSetupID = typeSetupID;
        TypeSetupName = typeSetupName;
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public MaxySetupType() {
    }

    public int getTypeSetupID() {
        return TypeSetupID;
    }

    public void setTypeSetupID(int typeSetupID) {
        TypeSetupID = typeSetupID;
    }

    public String getTypeSetupName() {
        return TypeSetupName;
    }

    public void setTypeSetupName(String typeSetupName) {
        TypeSetupName = typeSetupName;
    }

    public static Creator<MaxySetupType> getCREATOR() {
        return CREATOR;
    }

    protected MaxySetupType(Parcel in) {
        TypeSetupID = in.readInt();
        TypeSetupName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(TypeSetupID);
        dest.writeString(TypeSetupName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MaxySetupType> CREATOR = new Creator<MaxySetupType>() {
        @Override
        public MaxySetupType createFromParcel(Parcel in) {
            return new MaxySetupType(in);
        }

        @Override
        public MaxySetupType[] newArray(int size) {
            return new MaxySetupType[size];
        }
    };
}
