package isc.fpt.fsale.model;

public class StartPaymentResult {
    //Declares variables
    private int Id;
    private String Message;
    private QRCodeResult qrCodeResult;
    private String MBBankGateway;

    //Constructor
    public StartPaymentResult(){}

    //Getters and Setters

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public QRCodeResult getQrCodeResult() {
        return qrCodeResult;
    }

    public void setQrCodeResult(QRCodeResult qrCodeResult) {
        this.qrCodeResult = qrCodeResult;
    }

    public String getMBBankGateway() {
        return MBBankGateway;
    }

    public void setMBBankGateway(String MBBankGateway) {
        this.MBBankGateway = MBBankGateway;
    }
}
