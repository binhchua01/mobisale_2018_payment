package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.AutoPreCheckListCheckCL;
import isc.fpt.fsale.action.AutoPreCheckListGetTimezone;
import isc.fpt.fsale.action.GetFirstStatus;
import isc.fpt.fsale.action.InsertPreCheckList;
import isc.fpt.fsale.action.PreCheckListGetSupporter;
import isc.fpt.fsale.action.SearchObject;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.TimeZoneAdapter;
import isc.fpt.fsale.model.AutoPreCheckListInfo;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.model.SubTeamID1Model;
import isc.fpt.fsale.model.TimeZone;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;


public class CreatePreChecklistActivity extends BaseActivity {
    private Context mContext;
    private TextView lblContract;
    private TextView lblObjectId;
    private EditText txtContact;
    private TextView lblFullName, lblAddress;
    private Spinner spFirstStatus;
    private String FirstStatusValue;
    private EditText txtNote, txtPhone;
    private ImageView btnFind;
    private Spinner spDivision, SpSearchType,
            spCreatePreCheckListHandworkPartner, spCreatePreChecklistHandworkSubTeam;
    private String sDivisionValue, SearchType;
    private ListView lvObjectList;
    private ScrollView frmPreCheckListInfo;
    private CreatePreChecklistActivity createPreChecklistActivity;
    private LinearLayout frmContainerRequiredCheckList;
    private Spinner spCreatePreCheckListDate, spCreatePreCheckListTimeZone;
    private ArrayList<KeyValuePairModel> lstDateCreatePreCheckList;
    private KeyValuePairAdapter adapterDateCreatePreCheckList;
    private AutoPreCheckListInfo supportInfo;
    private boolean isRequiredCheckList;
    /* Id hợp đồng đang xử lý yêu cầu prechecklist */
    private String currentContractID = "";

    public CreatePreChecklistActivity() {
        super(R.string.lbl_create_prechecklist);
        MyApp.setCurrentActivity(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_create_precheck_list_activity));
        setContentView(R.layout.activity_create_prechecklist);
        createPreChecklistActivity = this;
        lblContract = (EditText) findViewById(R.id.txt_contract_num);
        lblObjectId = (TextView) findViewById(R.id.txt_object_id);
        txtContact = (EditText) findViewById(R.id.txt_contact_name);
        spFirstStatus = (Spinner) findViewById(R.id.sp_first_status);
        spCreatePreCheckListHandworkPartner = (Spinner) findViewById(R.id.sp_create_pre_checklist_handwork_partner);
        spCreatePreChecklistHandworkSubTeam = (Spinner) findViewById(R.id.sp_create_pre_checklist_handwork_sub_team);
        txtNote = (EditText) findViewById(R.id.txt_note);
        txtPhone = (EditText) findViewById(R.id.txt_phone);
        btnFind = (ImageView) findViewById(R.id.btn_find_contract);
        Button btnCreate = (Button) findViewById(R.id.btn_create_prechecklist);
        spDivision = (Spinner) findViewById(R.id.sp_division);
        SpSearchType = (Spinner) findViewById(R.id.sp_search_type);
        lvObjectList = (ListView) findViewById(R.id.lv_object_list);
        frmPreCheckListInfo = (ScrollView) findViewById(R.id.frm_prechecklist_info);
        lblFullName = (TextView) findViewById(R.id.tv_full_name);
        lblAddress = (TextView) findViewById(R.id.txt_address);
        frmContainerRequiredCheckList = (LinearLayout) findViewById(R.id.frm_container_required_check_list);
        spCreatePreCheckListDate = (Spinner) findViewById(R.id.sp_create_pre_checklist_date);
        spCreatePreCheckListTimeZone = (Spinner) findViewById(R.id.sp_create_pre_checklist_time_zone);
        spCreatePreCheckListDate.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Common.showTimePickerDialog(CreatePreChecklistActivity.this, true);
                }
                return false;
            }
        });
        this.mContext = CreatePreChecklistActivity.this;
        loadSearchType();

        lblContract.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnFind.performClick();
                    return true;
                }
                return false;
            }
        });

        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lblContract.getText() == null || lblContract.getText().toString().trim().equals("")) {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_search_value_empty), mContext);
                    return;
                }
                Common.hideSoftKeyboard(mContext);
                new SearchObject(mContext, lblContract.getText().toString(), Constants.USERNAME, SearchType,
                        lblObjectId, txtContact);
            }
        });

        spDivision.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                sDivisionValue = selectedItem.getsID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spFirstStatus.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FirstStatusValue = selectedItem.getsID();
                if (selectedItem.getAction().length() != 0) {
                    isRequiredCheckList = true;
                    frmContainerRequiredCheckList.setVisibility(View.VISIBLE);
                    String[] paramValue = new String[]{lblObjectId.getText().toString()};
                    new PreCheckListGetSupporter(mContext, createPreChecklistActivity, paramValue);
                } else {
                    isRequiredCheckList = false;
                    frmContainerRequiredCheckList.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPreCheckListManual();
            }
        });

        SpSearchType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                SearchType = selectedItem.getsID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spCreatePreCheckListTimeZone.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                TimeZone item = (TimeZone) spCreatePreCheckListTimeZone.getSelectedItem();
                if (item.getAbilityDesc() == 0) {
                    Common.alertDialog("Múi giờ dự kiến full năng lực, vui lòng tư vấn KH sang múi giờ màu xanh", mContext);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        lvObjectList.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                ObjectModel selectedItem = (ObjectModel) adapter.getItemAtPosition(position);
                if (selectedItem != null) {
                    //Kiểm tra có tạo được checklist tự động
                    String[] paramValue = new String[]{String.valueOf(selectedItem.getObjID()), selectedItem.getContract(), Constants.USERNAME};
                    new AutoPreCheckListCheckCL(mContext, createPreChecklistActivity, selectedItem, paramValue);
                }
            }
        });

        ignoreErrorCode(txtContact);
        ignoreErrorCode(txtNote);
        ignoreErrorCode(txtPhone);
        setDivision();
        setFirstStatus();
        initSpinnerDateCreatePreCheckList();
    }

    private void createPreCheckListManual() {
        String appointmentDate = ""; // ngày tạo
        String timeZone = ""; // thời gian bảo trì dự kiến
        String supporter = ""; // Đối tác
        String subID = ""; // Tổ con
        String abilityDesc = "-2";
        // Trường hợp Tình trạng : yêu cầu nhập Prechecklist (2),
        // yêu cầu cập nhật thêm 4 variable trên vào Api
        // Nếu không phải option (2) , set 4 var trên mặc định null.
        if (isRequiredCheckList) {
            if (spCreatePreCheckListDate.getAdapter() != null && spCreatePreCheckListDate.getAdapter().getCount() > 0)
                appointmentDate = ((KeyValuePairModel) spCreatePreCheckListDate.getSelectedItem()).getsID();
            if (spCreatePreCheckListTimeZone.getAdapter() != null && spCreatePreCheckListTimeZone.getAdapter().getCount() > 0)
                timeZone = ((TimeZone) spCreatePreCheckListTimeZone.getSelectedItem()).getTimezoneValue();
            if (spCreatePreCheckListHandworkPartner.getAdapter() != null && spCreatePreCheckListHandworkPartner.getAdapter().getCount() > 0)
                supporter = ((KeyValuePairModel) spCreatePreCheckListHandworkPartner.getSelectedItem()).getsID();
            if (spCreatePreChecklistHandworkSubTeam.getAdapter() != null && spCreatePreChecklistHandworkSubTeam.getAdapter().getCount() > 0)
                subID = ((KeyValuePairModel) spCreatePreChecklistHandworkSubTeam.getSelectedItem()).getsID();
            if (spCreatePreCheckListTimeZone.getAdapter() != null && spCreatePreCheckListTimeZone.getAdapter().getCount() > 0)
                abilityDesc = String.valueOf(((TimeZone) spCreatePreCheckListTimeZone.getSelectedItem()).getAbilityDesc());
        }
        if (VerifiedData()) {
            Dialog dialog;
            final String finalAbilityDesc = abilityDesc;
            final String[] paramValue = new String[]{
                    txtContact.getText().toString(),
                    txtPhone.getText().toString().trim(),
                    FirstStatusValue,
                    txtNote.getText().toString().trim(),
                    sDivisionValue,
                    Constants.USERNAME,
                    lblObjectId.getText().toString(),
                    currentContractID,
                    supporter,
                    subID,
                    appointmentDate,
                    timeZone,
                    finalAbilityDesc
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                    .setMessage(mContext.getResources().getString(R.string.msg_confirm_create_prechecklist))
                    .setCancelable(false)
                    .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (Integer.parseInt(finalAbilityDesc) == 0) {
                                new AlertDialog.Builder(mContext).setTitle("Thông Báo")
                                        .setMessage("Sẽ hẹn lại nếu không đáp ứng múi giờ này")
                                        .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                new InsertPreCheckList(mContext, paramValue);
                                            }
                                        }).setCancelable(false).create().show();
                            } else {
                                // tạo 1 checklist thủ công
                                new InsertPreCheckList(mContext, paramValue);
                            }
                        }
                    })
                    .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int id) {
                            dialog.cancel();
                        }
                    });
            dialog = builder.create();
            dialog.show();
        }
    }

    // hiển thị màn hình nhập checklist
    public void showCreatePrecheckList(ObjectModel objectModel) {
        currentContractID = objectModel.getContract();
        frmPreCheckListInfo.setVisibility(View.VISIBLE);
        lblObjectId.setText(String.valueOf(objectModel.getObjID()));
        lblFullName.setText(String.valueOf(objectModel.getFullName()));
        lblAddress.setText(String.valueOf(objectModel.getAddress()));
        initSpinnerDateCreatePreCheckList();
        clearDataCheckList();
        lvObjectList.setVisibility(View.GONE);
    }

    // khôi phục lại các giá trị ban đầu
    private void clearDataCheckList() {
        txtContact.setText("");
        txtPhone.setText("");
        spFirstStatus.setSelection(0, true);
        loadTimeZone(null);
        txtNote.setText("");
        spDivision.setSelection(0, true);
        isRequiredCheckList = false;
    }

    // Tải thông tin precheck list
    public void loadSupportInfo(AutoPreCheckListInfo supportInfo) {
        this.supportInfo = supportInfo;
        loadDataSupport(supportInfo);
    }

    // load đối tác và tổ con
    public void loadDataSupport(AutoPreCheckListInfo autoPreCheckListInfo) {
        SubTeamID1Model supporter = new SubTeamID1Model();
        if (autoPreCheckListInfo.getSupporter() != null) {
            supporter.setPartnerList(new ArrayList<KeyValuePairModel>());
            supporter.getPartnerList().add(new KeyValuePairModel(autoPreCheckListInfo.getSupporter(), autoPreCheckListInfo.getSupporter()));
        }
        if (autoPreCheckListInfo.getSubSupporter() != null) {
            supporter.setSubTeamList(new ArrayList<KeyValuePairModel>());
            supporter.getSupTeamList().add(new KeyValuePairModel(autoPreCheckListInfo.getSubSupporter(), autoPreCheckListInfo.getSubSupporter()));
        }
        if (supporter.getPartnerList().size() > 0 || supporter.getSupTeamList().size() > 0) {
            if (supporter.getPartnerList().size() > 0) {
                spCreatePreCheckListHandworkPartner.setAdapter(new KeyValuePairAdapter(this, R.layout.my_spinner_style, supporter.getPartnerList(), Gravity.LEFT));
            }
            if (supporter.getSupTeamList().size() > 0) {
                spCreatePreChecklistHandworkSubTeam.setAdapter(new KeyValuePairAdapter(this, R.layout.my_spinner_style, supporter.getSupTeamList(), Gravity.LEFT));
            }
        } else {
            new AlertDialog.Builder(this).setTitle("Thông Báo").setMessage(getString(R.string.msg_not_fount_partner_and_subteam))
                    .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setCancelable(false).create().show();
        }
    }

    // Tải danh sách timezone
    public void loadTimeZone(ArrayList<TimeZone> timeZone) {
        ArrayList<TimeZone> lst = new ArrayList<>();
        lst.add(new TimeZone("Chọn múi giờ", "", -1, ""));
        if (timeZone != null) {
            //add item default
            lst.addAll(timeZone);
        }
        spCreatePreCheckListTimeZone.setAdapter(new TimeZoneAdapter(this, R.layout.my_spinner_style, lst));
    }


    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void setDivision() {
        ArrayList<KeyValuePairModel> lstDivision = new ArrayList<>();
        lstDivision.add(new KeyValuePairModel("", "[ Vui lòng chọn phòng ban ]"));
        lstDivision.add(new KeyValuePairModel("4", "IBB"));
        /*lstDivision.add(new KeyValuePairModel("7", "CUS"));
        lstDivision.add(new KeyValuePairModel("43", "CS"));
		lstDivision.add(new KeyValuePairModel("39", "BDD"));*/
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstDivision, Gravity.LEFT);
        spDivision.setAdapter(adapter);

    }


    private void setFirstStatus() {
        new GetFirstStatus(mContext, spFirstStatus);
    }

    private void ignoreErrorCode(final EditText txt) {
        if (txt != null)
            txt.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    txt.setError(null);
                }
            });
    }

    private Boolean VerifiedData() {
        if (txtContact.getText() == null || txtContact.getText().toString().trim().equals("")) {
            txtContact.requestFocus();
            Common.alertDialog("Vui lòng nhập người liên hệ", mContext);
            return false;
        }
        if (lblObjectId.getText() == null || lblObjectId.getText().toString().trim().equals("")) {
            Common.alertDialog("Vui lòng nhập số hợp đồng", mContext);
            return false;
        }
        if (txtPhone.getText() == null || txtPhone.getText().toString().trim().equals("")) {
            Common.alertDialog("Vui lòng nhập số điện thoại", mContext);
            txtPhone.requestFocus();
            return false;
        }
        if (FirstStatusValue == null || FirstStatusValue.trim().equals("")) {
            Common.alertDialog("Vui lòng chọn tình trạng ban đầu!", mContext);
            return false;
        }
        if (isRequiredCheckList) {
            if (spCreatePreCheckListHandworkPartner.getAdapter() == null) {
                Common.alertDialog(getResources().getString(R.string.msg_empty_data_partner_checklist), mContext);
                return false;
            }
            if (spCreatePreChecklistHandworkSubTeam.getAdapter() == null) {
                Common.alertDialog(getResources().getString(R.string.msg_empty_data_partner_sub_team_checklist), mContext);
                return false;
            }
            if (spCreatePreCheckListHandworkPartner.getAdapter() != null || spCreatePreChecklistHandworkSubTeam.getAdapter() != null) {
                if (spCreatePreCheckListDate.getAdapter() != null && spCreatePreCheckListDate.getAdapter().getCount() > 0) {
                    String dateSelectedValue = ((KeyValuePairModel) spCreatePreCheckListDate.getSelectedItem()).getsID();
                    if (dateSelectedValue.length() == 0) {
                        Common.alertDialog(getResources().getString(R.string.msg_not_selected_date_create_checklist), mContext);
                        return false;
                    } else {
                        if (spCreatePreCheckListTimeZone.getAdapter() == null) {
                            Common.alertDialog(getResources().getString(R.string.msg_not_found_time_zone_create_checklist), mContext);
                            return false;
                        }
                    }

                    if (spCreatePreCheckListTimeZone.getAdapter() != null && spCreatePreCheckListTimeZone.getAdapter().getCount() > 0 &&
                            TextUtils.isEmpty(((TimeZone) spCreatePreCheckListTimeZone.getSelectedItem()).getTimezoneValue())) {
                        Common.alertDialog(getResources().getString(R.string.label_message_choose_time_zone), mContext);
                        return false;
                    }
                }
            }
        }

        if (txtNote.getText() == null || txtNote.getText().toString().trim().equals("")) {
            Common.alertDialog("Vui lòng nhập ghi chú", mContext);
            txtNote.requestFocus();
            return false;
        }
        if (sDivisionValue == null || sDivisionValue.trim().equals("")) {
            Common.alertDialog("Vui lòng chọn phòng ban", mContext);
            return false;
        }
        return true;
    }

    // Load dữ liệu loại tìm kiếm
    private void loadSearchType() {
        ArrayList<KeyValuePairModel> lstSearchType = new ArrayList<>();
        lstSearchType.add(new KeyValuePairModel("1", "Số HD"));
        lstSearchType.add(new KeyValuePairModel("2", "Tên KH"));
        lstSearchType.add(new KeyValuePairModel("3", "Phone"));
        lstSearchType.add(new KeyValuePairModel("4", "Địa Chỉ"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstSearchType, Gravity.LEFT);
        SpSearchType.setAdapter(adapter);
    }

    //Load dữ liệu chọn ngày tạo PreCheckList
    private void initSpinnerDateCreatePreCheckList() {
        lstDateCreatePreCheckList = new ArrayList<>();
        lstDateCreatePreCheckList.add(new KeyValuePairModel("", "Chọn ngày"));
        adapterDateCreatePreCheckList = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstDateCreatePreCheckList, Gravity.LEFT);
        spCreatePreCheckListDate.setAdapter(adapterDateCreatePreCheckList);
    }

    // cập nhật dữ liệu vào spinner ngày tạo
    public void updateDateSpinner(int newYear, int newMonth, int newDay) {
        //update by tuantt14
        spCreatePreCheckListTimeZone.setAdapter(null);
        if (spCreatePreCheckListDate != null) {
            if (newYear > 0) {
                String customDateDesc = "Ngày " + newDay + " tháng " + (newMonth + 1) + " năm " + (newYear);
                Calendar date = Calendar.getInstance();
                date.set(newYear, newMonth, newDay);
                String customDate = Common.getSimpleDateFormat(date, Constants.DATE_FORMAT);
                lstDateCreatePreCheckList.clear();
                lstDateCreatePreCheckList.add(new KeyValuePairModel(customDate, customDateDesc));
                adapterDateCreatePreCheckList.notifyDataSetChanged();
            }
            spCreatePreCheckListDate.setSelection(0, true);
            Method method = null;
            try {
                method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            assert method != null;
            method.setAccessible(true);
            try {
                method.invoke(spCreatePreCheckListDate);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            String supporter = "", subSupporter = "", appointmentDate = "", objId = "";
            if (spCreatePreCheckListHandworkPartner.getAdapter() != null && spCreatePreCheckListHandworkPartner.getAdapter().getCount() > 0)
                supporter = ((KeyValuePairModel) spCreatePreCheckListHandworkPartner.getSelectedItem()).getsID();
            if (spCreatePreChecklistHandworkSubTeam.getAdapter() != null && spCreatePreChecklistHandworkSubTeam.getAdapter().getCount() > 0)
                subSupporter = ((KeyValuePairModel) spCreatePreChecklistHandworkSubTeam.getSelectedItem()).getsID();
            if (spCreatePreCheckListDate.getAdapter() != null && spCreatePreCheckListDate.getAdapter().getCount() > 0)
                appointmentDate = ((KeyValuePairModel) spCreatePreCheckListDate.getSelectedItem()).getsID();
            if (!lblObjectId.getText().toString().equals(""))
                objId = lblObjectId.getText().toString();
            if (supportInfo != null) {
                //updated by haulc3
                String[] paramsValue = new String[]{String.valueOf(supportInfo.getInit_Status()),
                        supporter, subSupporter, appointmentDate, Constants.USERNAME, objId};
                // lấy múi giờ để tạo checklist thủ công
                new AutoPreCheckListGetTimezone(mContext, createPreChecklistActivity, paramsValue);
            } else {
                Common.alertDialog(getResources().getString(R.string.msg_empty_input_get_timezone_info), mContext);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (lvObjectList != null && lvObjectList.getVisibility() == View.GONE) {
            lvObjectList.setVisibility(View.VISIBLE);
            frmPreCheckListInfo.setVisibility(View.GONE);
        } else
            finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
