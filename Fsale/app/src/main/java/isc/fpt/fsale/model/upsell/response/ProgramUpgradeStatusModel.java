package isc.fpt.fsale.model.upsell.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProgramUpgradeStatusModel implements Parcelable {

    @SerializedName("ID")
    @Expose
    private Integer ID;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("ErrorService")
    @Expose
    private String ErrorService;


    public ProgramUpgradeStatusModel(Integer ID, String name, String errorService) {
        this.ID = ID;
        Name = name;
        ErrorService = errorService;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getErrorService() {
        return ErrorService;
    }

    public void setErrorService(String errorService) {
        ErrorService = errorService;
    }

    protected ProgramUpgradeStatusModel(Parcel in) {
        if (in.readByte() == 0) {
            ID = null;
        } else {
            ID = in.readInt();
        }
        Name = in.readString();
        ErrorService = in.readString();
    }

    public static final Creator<ProgramUpgradeStatusModel> CREATOR = new Creator<ProgramUpgradeStatusModel>() {
        @Override
        public ProgramUpgradeStatusModel createFromParcel(Parcel in) {
            return new ProgramUpgradeStatusModel(in);
        }

        @Override
        public ProgramUpgradeStatusModel[] newArray(int size) {
            return new ProgramUpgradeStatusModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (ID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(ID);
        }
        dest.writeString(Name);
        dest.writeString(ErrorService);
    }
}
