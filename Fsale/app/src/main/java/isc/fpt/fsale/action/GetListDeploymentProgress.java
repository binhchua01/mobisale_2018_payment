package isc.fpt.fsale.action;

import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.DeploymentProgressActivity;
import isc.fpt.fsale.adapter.DeploymentProgressAdapter;
import isc.fpt.fsale.model.ListDeploymentProgressModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

//API lấy HỖ TRỢ TRIỂN KHAI
public class GetListDeploymentProgress implements AsyncTaskCompleteListener<String> {
    private ArrayList<ListDeploymentProgressModel> lstDeploy;
    private Context mContext;
    private Boolean isReload;
    private String[] arrParams;

    public GetListDeploymentProgress(Context mContext, String userName, String Agent,
                                     String AgentName, String PageNum, Boolean isReload) {
        this.mContext = mContext;
        this.isReload = isReload;
        String message = mContext.getResources().getString(R.string.msg_pd_get_list_deployment_progress);
        String[] params = new String[]{"UserName", "Agent", "AgentName", "PageNumber"};
        arrParams = new String[]{userName, Agent, AgentName, PageNum};
        String GET_LIST_DEPLOYMENT = "GetListDeploymentProgress";
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_DEPLOYMENT, params, arrParams,
                Services.JSON_POST, message, GetListDeploymentProgress.this);
        service.execute();
    }

    private void handleGetListDeployment(String json) {
        lstDeploy = new ArrayList<>();
        try {
            if (Common.jsonObjectValidate(json)) {
                JSONObject jsObj = JSONParsing.getJsonObj(json);
                bindData(jsObj);
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {
            String PageSize = "1;1";
            String TAG_LIST_DEPLOYMENT_RESULT = "GetListDeploymentProgressMethodPostResult";
            jsArr = jsObj.getJSONArray(TAG_LIST_DEPLOYMENT_RESULT);
            int l = jsArr.length();
            if (l > 0) {
                String TAG_ERROR = "ErrorService";
                String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                if (error.equals("null")) {
                    for (int i = 0; i < l; i++) {
                        JSONObject iObj = jsArr.getJSONObject(i);

                        String _strRowNumber = "", _strTotalPage = "", _strCurrentPage = "", _strTotalRow = "",
                                _strObjePhone = "", _strAllot = "", fullName = "";
                        int sendMail = 0;
                        // Add by GiauTQ 30-04-2014
                        // add by GiauTQ 01-05-2014
                        String TAG_ROW_NUMBER = "RowNumber";
                        if (iObj.has(TAG_ROW_NUMBER))
                            _strRowNumber = iObj.getString(TAG_ROW_NUMBER);

                        String TAG_TOTAL_PAGE = "TotalPage";
                        if (iObj.has(TAG_TOTAL_PAGE))
                            _strTotalPage = iObj.getString(TAG_TOTAL_PAGE);

                        String TAG_CURRENT_PAGE = "CurrentPage";
                        if (iObj.has(TAG_CURRENT_PAGE))
                            _strCurrentPage = iObj.getString(TAG_CURRENT_PAGE);

                        String TAG_TOTAL_ROW = "TotalRow";
                        if (iObj.has(TAG_TOTAL_ROW))
                            _strTotalRow = iObj.getString(TAG_TOTAL_ROW);

                        // add by GiauTQ 16-07-2014
                        String TAG_OBJECT_PHONE = "ObjectPhone";
                        if (iObj.has(TAG_OBJECT_PHONE))
                            _strObjePhone = iObj.getString(TAG_OBJECT_PHONE);

                        String TAG_ALLOT = "Allot";
                        if (iObj.has(TAG_ALLOT))
                            _strAllot = iObj.getString(TAG_ALLOT);
                        String TAG_SEND_MAIL = "SendMail";
                        if (iObj.has(TAG_SEND_MAIL))
                            sendMail = iObj.getInt(TAG_SEND_MAIL);
                        //Add by DuHK
                        String TAG_FULL_NAME = "FullName";
                        if (iObj.has(TAG_FULL_NAME))
                            fullName = iObj.getString(TAG_FULL_NAME);

                        PageSize = _strCurrentPage + ";" + _strTotalPage;
                        String DeployAppointmentDate = "";
                        //DuHK, 04/04/016
                        String TAG_DEPLOY_APPOINTMENT_DATE = "DeployAppointmentDate";
                        if (iObj.has(TAG_DEPLOY_APPOINTMENT_DATE))
                            DeployAppointmentDate = iObj.getString(TAG_DEPLOY_APPOINTMENT_DATE);
                        String TAG_REG_CODE = "RegCode";
                        String TAG_CONTRACT = "Contract";
                        String TAG_OBJ_DATE = "ObjDate";
                        String TAG_DEPLOYMENT_DATE = "DeploymentDate";
                        String TAG_FINISH_DATE = "FinishDate";
                        String TAG_WAIT_TIME = "WaitTime";
                        lstDeploy.add(
                                new ListDeploymentProgressModel(iObj.getString(TAG_REG_CODE), iObj.getString(TAG_CONTRACT),
                                        iObj.getString(TAG_OBJ_DATE), iObj.getString(TAG_DEPLOYMENT_DATE), iObj.getString(TAG_WAIT_TIME),
                                        iObj.getString(TAG_FINISH_DATE), _strRowNumber, _strTotalPage, _strCurrentPage, _strTotalRow,
                                        _strObjePhone, _strAllot, sendMail, fullName, DeployAppointmentDate)
                        );
                    }
                    if (!this.isReload)
                        showDeploymentList(lstDeploy, PageSize);
                    else {
                        DeploymentProgressActivity DeploymentProgress = (DeploymentProgressActivity) mContext;
                        DeploymentProgressAdapter adapter = new DeploymentProgressAdapter(mContext, lstDeploy);
                        DeploymentProgress.lvDeploy.setAdapter(adapter);
                        DeploymentProgress.SpPage.setSelection(Common.getIndex(DeploymentProgress.SpPage, DeploymentProgress.iCurrentPage), true);
                    }
                } else {
                    Common.alertDialog("Lỗi WS:" + error, mContext);
                }
            } else {
                Common.alertDialog("Không tìm thấy dữ liệu", mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDeploymentList(ArrayList<ListDeploymentProgressModel> list, String PageSize) {
        try {
            Intent intent = new Intent(mContext, DeploymentProgressActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("list_deployment", list);
            intent.putExtra("PageSize", PageSize);
            intent.putExtra("ArrayParam", arrParams);
            mContext.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleGetListDeployment(result);
    }
}