package isc.fpt.fsale.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.DownloadApp;
import isc.fpt.fsale.model.AllCloudType;
import isc.fpt.fsale.model.CameraPackage;
import isc.fpt.fsale.model.CameraSelected;
import isc.fpt.fsale.model.CameraType;
import isc.fpt.fsale.model.CloudPackageSelected;

/**
 * Created by Hau Le on 2018-12-25.
 */
public class DialogUtils {
    private static ArrayList<CameraSelected> lsCamera = new ArrayList<>();
    private static CameraPackage cameraPackage = new CameraPackage();
    private static AllCloudType oldObjCloudType;
    private static CloudPackageSelected oldObjCloudPackage;
    //this is position of list view camera type
    private static int pos;
    private static TextView tvCloudPackage, tvQuantity, tvTotal;

    @SuppressLint("NewApi")
    public static Dialog dialogCameraDevice(final Context context, final ArrayList<CameraType> list,
                                            final CameraDeviceCallback callbackToActivity,
                                            final ArrayList<CameraSelected> oldListSelected) {
        // Use the Builder class for convenient dialog construction
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_camera_device);
        dialog.setCancelable(false);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.list_camera_device);
        TextView tvAccept = (TextView) dialog.findViewById(R.id.tv_accept);
        RelativeLayout loBack = (RelativeLayout) dialog.findViewById(R.id.layout_back_action);
//        CameraDeviceAdapter adapter = new CameraDeviceAdapter(
//                context,
//                list,
//                new OnItemCameraClickListener() {
//                    @Override
//                    public void CallbackListDevice(ArrayList<CameraSelected> listCameraDevice, int position) {
//                        lsCamera = listCameraDevice;
//                    }
//                },
//                oldListSelected
//        );
//        recyclerView.setAdapter(adapter);
        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lsCamera.size() == 0) {
                    callbackToActivity.CallbackCameraDeviceListSelected(oldListSelected);
                } else {
                    callbackToActivity.CallbackCameraDeviceListSelected(lsCamera);
                }
                dialog.dismiss();
            }
        });

        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lsCamera.clear();
                callbackToActivity.CallbackCameraDeviceListSelected(oldListSelected);
                dialog.dismiss();
            }
        });
        return dialog;
    }


    private static Dialog dialogCloudType(final Context context, ArrayList<AllCloudType> list,
                                          final CloudTypeCallback callback,
                                          final AllCloudType oldObject) {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_cloud_service);
        dialog.setCancelable(false);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.list_cloud_type);
        RelativeLayout loBack = (RelativeLayout) dialog.findViewById(R.id.layout_back_action);
//        CloudTypeAdapter adapter = new CloudTypeAdapter(
//                context,
//                list,
//                new CloudTypeCallback() {
//                    @Override
//                    public void CallbackCloudTypeSelected(AllCloudType obj) {
//                        oldObjCloudType = obj;
//                        callback.CallbackCloudTypeSelected(oldObjCloudType);
//                        dialog.dismiss();
//                    }
//                },
//                oldObject
//        );
//        recyclerView.setAdapter(adapter);

        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                oldObjCloudType = new AllCloudType();
                dialog.dismiss();
            }
        });
        return dialog;
    }

    @SuppressLint("SetTextI18n")
    private static Dialog dialogChooseCloudPackage(final Context context,
                                                   ArrayList<CloudPackageSelected> list,
                                                   final CloudPackageCallback callback,
                                                   final CloudPackageSelected oldObject) {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_cloud_service);
        dialog.setCancelable(false);
        android.widget.TextView tvHeader = (TextView) dialog.findViewById(R.id.tv_header_dialog);
        tvHeader.setText("Thời hạn");
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.list_cloud_type);
        RelativeLayout loBack = (RelativeLayout) dialog.findViewById(R.id.layout_back_action);
//        CloudPackageAdapter adapter = new CloudPackageAdapter(context, list, new CloudPackageCallback() {
//            @Override
//            public void PackageCallback(CloudPackageSelected obj) {
//                oldObjCloudPackage = obj;
//                callback.PackageCallback(oldObjCloudPackage);
//                dialog.dismiss();
//            }
//        }, oldObject);
//        recyclerView.setAdapter(adapter);
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                oldObjCloudPackage = new CloudPackageSelected();
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog dialogChooseCloudService(final Context context,
                                                  final CloudServiceCallback callback,
                                                  final ArrayList<AllCloudType> listCloudType) {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_choose_cloud);
        dialog.setCancelable(false);
        oldObjCloudPackage = new CloudPackageSelected();
        oldObjCloudType = new AllCloudType();
        final Button btnUpdate = (Button) dialog.findViewById(R.id.btn_update);
        final Button tvCancel = (Button) dialog.findViewById(R.id.btn_cancel);
        final TextView tvCloudType = (TextView) dialog.findViewById(R.id.tv_package_name);
        tvCloudPackage = (TextView) dialog.findViewById(R.id.tv_date_name);
        final TextView btnLess = (TextView) dialog.findViewById(R.id.btn_less);
        tvQuantity = (TextView) dialog.findViewById(R.id.tv_quantity);
        final TextView btnPlus = (TextView) dialog.findViewById(R.id.btn_plus);
        tvTotal = (TextView) dialog.findViewById(R.id.tv_total);
        final RelativeLayout loBack = (RelativeLayout) dialog.findViewById(R.id.layout_back_action);

        //choose cloud type
        tvCloudType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = DialogUtils.dialogCloudType(context, listCloudType,
                        new CloudTypeCallback() {
                            @Override
                            public void CallbackCloudTypeSelected(AllCloudType obj) {
                                if (obj != null) {
                                    tvCloudType.setText(obj.getTypeName());
                                    //clear data cloud package
                                    tvCloudPackage.setText("");
                                    tvQuantity.setText("0");
                                    tvTotal.setText("0");
                                }
                            }
                        }, oldObjCloudType);
                dialog.show();
                ResizeDialogPercent.resize(context, dialog);
            }
        });

        //choose cloud package
        tvCloudPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvCloudType.getText().toString().equals("")) {
                    Common.alertDialog("Vui lòng chọn tên gói dịch vụ!", context);
                    return;
                }
                if (oldObjCloudType != null) {
                    //call api get cloud package
//                    new GetAllCloudPackage(
//                            context,
//                            ((MyApp) ((Activity) context).getApplication()).getUserName(),
//                            String.valueOf(oldObjCloudType.getTypeID())
//                    );
                }
            }
        });

        btnLess.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                int quantity = oldObjCloudPackage.getQty();
                if (quantity != 0 && quantity > 1) {
                    quantity--;
                    oldObjCloudPackage.setQty(quantity);
                    tvQuantity.setText(String.valueOf(oldObjCloudPackage.getQty()));
                    tvTotal.setText(Common.formatNumber(oldObjCloudPackage.getQty() * oldObjCloudPackage.getCost()) + " đồng");
                }

            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                int quantity = oldObjCloudPackage.getQty();
                if (quantity != 0) {
                    quantity++;
                    oldObjCloudPackage.setQty(quantity);
                    tvQuantity.setText(String.valueOf(oldObjCloudPackage.getQty()));
                    tvTotal.setText(Common.formatNumber(oldObjCloudPackage.getQty() * oldObjCloudPackage.getCost()) + " đồng");
                }
            }
        });

        //update
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!tvCloudType.getText().toString().equals("")
                        && !tvCloudPackage.getText().toString().equals("")) {
                    oldObjCloudPackage.setTypeID(oldObjCloudType.getTypeID());
                    oldObjCloudPackage.setTypeName(oldObjCloudType.getTypeName());
                    callback.CallbackCloudServiceListSelected(oldObjCloudPackage);
                    dialog.dismiss();
                } else {
                    Common.alertDialog("Vui lòng chọn đầy đủ thông tin!", context);
                }
            }
        });

        //close dialog
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog dialogCameraPackage(final Context context, ArrayList<CameraPackage> list,
                                             final CameraPackageCallback cameraPackageCallback,
                                             final int position, CameraSelected cameraSelected) {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_camera_package);
        dialog.setCancelable(false);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.list_camera_package);
        TextView tvAccept = (TextView) dialog.findViewById(R.id.tv_accept);
        RelativeLayout loBack = (RelativeLayout) dialog.findViewById(R.id.layout_back_action);
//        CameraPackageAdapter adapter = new CameraPackageAdapter(context, list, new CameraPackageCallback() {
//            @Override
//            public void CameraPackageCallback(CameraPackage obj, int position) {
//                cameraPackage = obj;
//                pos = position;
//            }
//        }, position, cameraSelected);
//        recyclerView.setAdapter(adapter);
        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraPackageCallback.CameraPackageCallback(cameraPackage, pos);
                dialog.dismiss();
            }
        });
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog dialogUpdateVersion(final Context context, final String url) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update_version);
        dialog.setCancelable(false);
        android.widget.TextView tvUpdate = (TextView) dialog.findViewById(R.id.tv_update);
        android.widget.TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new DownloadApp(context).execute(url);
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((Activity) context).finish();
            }
        });
        return dialog;
    }

    public static Dialog showLoadingDialog(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_progress);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public static void showDialogCloudPackage(final Context context, ArrayList<CloudPackageSelected> list) {
        //show dialog
        Dialog dialog = DialogUtils.dialogChooseCloudPackage(
                context,
                list,
                new CloudPackageCallback() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void PackageCallback(CloudPackageSelected obj) {
                        tvCloudPackage.setText(obj.getPackName());
                        tvQuantity.setText(String.valueOf(oldObjCloudPackage.getQty()));
                        tvTotal.setText(
                                String.format(context.getString(R.string.txt_unit_price),
                                        Common.formatNumber(oldObjCloudPackage.getQty() * oldObjCloudPackage.getCost()))
                        );
                    }
                },
                oldObjCloudPackage
        );
        dialog.show();
        ResizeDialogPercent.resize(context, dialog);
    }
}
