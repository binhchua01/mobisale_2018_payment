package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import static io.fabric.sdk.android.services.network.HttpRequest.trace;

public class CameraSelected implements Parcelable {
    private int TypeID;
    private String TypeName;
    private int PackID;
    private String PackName;
    private int MinCam;
    private int MaxCam;
    private int Cost;
    private int Qty;
    private int ServiceType;

    public CameraSelected() {
    }

    public CameraSelected(int typeID, String typeName, int packID,
                          String packName, int minCam, int maxCam, int cost,
                          int qty, int serviceType) {
        TypeID = typeID;
        TypeName = typeName;
        PackID = packID;
        PackName = packName;
        MinCam = minCam;
        MaxCam = maxCam;
        Cost = cost;
        Qty = qty;
        ServiceType = serviceType;
    }

    protected CameraSelected(Parcel in) {
        TypeID = in.readInt();
        TypeName = in.readString();
        PackID = in.readInt();
        PackName = in.readString();
        MinCam = in.readInt();
        MaxCam = in.readInt();
        Cost = in.readInt();
        Qty = in.readInt();
        ServiceType = in.readInt();
    }

    public static final Creator<CameraSelected> CREATOR = new Creator<CameraSelected>() {
        @Override
        public CameraSelected createFromParcel(Parcel in) {
            return new CameraSelected(in);
        }

        @Override
        public CameraSelected[] newArray(int size) {
            return new CameraSelected[size];
        }
    };

    public int getTypeID() {
        return TypeID;
    }

    public void setTypeID(int typeID) {
        TypeID = typeID;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }

    public int getPackID() {
        return PackID;
    }

    public void setPackID(int packID) {
        PackID = packID;
    }

    public String getPackName() {
        return PackName;
    }

    public void setPackName(String packName) {
        PackName = packName;
    }

    public int getMinCam() {
        return MinCam;
    }

    public void setMinCam(int minCam) {
        MinCam = minCam;
    }

    public int getMaxCam() {
        return MaxCam;
    }

    public void setMaxCam(int maxCam) {
        MaxCam = maxCam;
    }

    public int getCost() {
        return Cost;
    }

    public void setCost(int cost) {
        Cost = cost;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }

    public JSONObject getJsonObject(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("TypeID", getTypeID());
            obj.put("TypeName", getTypeName());
            obj.put("PackID", getPackID());
            obj.put("PackName", getPackName());
            obj.put("MinCam", getMinCam());
            obj.put("MaxCam", getMaxCam());
            obj.put("Cost", getCost());
            obj.put("Qty", getQty());
            obj.put("ServiceType", getServiceType());
        } catch (JSONException e) {
            trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(TypeID);
        parcel.writeString(TypeName);
        parcel.writeInt(PackID);
        parcel.writeString(PackName);
        parcel.writeInt(MinCam);
        parcel.writeInt(MaxCam);
        parcel.writeInt(Cost);
        parcel.writeInt(Qty);
        parcel.writeInt(ServiceType);
    }
}
