package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by haulc3 on 17,July,2019
 */
public class Ward implements Serializable {
    @SerializedName("ErrorService")
    @Expose
    private Object errorService;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("NameVN")
    @Expose
    private String nameVN;

    public Object getErrorService() {
        return errorService;
    }

    public void setErrorService(Object errorService) {
        this.errorService = errorService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameVN() {
        return nameVN;
    }

    public void setNameVN(String nameVN) {
        this.nameVN = nameVN;
    }
}
