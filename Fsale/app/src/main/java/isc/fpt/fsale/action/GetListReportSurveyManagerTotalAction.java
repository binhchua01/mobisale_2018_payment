package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportSurveyManagerTotalActivity;

import isc.fpt.fsale.model.ReportSurveyManagerTotalModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

// API báo cáo khảo sát
public class GetListReportSurveyManagerTotalAction implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public GetListReportSurveyManagerTotalAction(Context _mContext, int day, int month, int year,
                                                 int agent, String agentName, int pageNumber) {
        this.mContext = _mContext;
        String message = "Đang lấy dữ liệu...";
        String[] arrParamName = new String[]{"UserName", "Day", "Month", "Year", "Agent", "AgentName", "PageNumber"};
        String[] arrParamValue = new String[]{Constants.USERNAME, String.valueOf(day), String.valueOf(month),
                String.valueOf(year), String.valueOf(agent), agentName, String.valueOf(pageNumber)};
        String TAG_METHOD_NAME = "ReportTotalSurvey";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue,
                Services.JSON_POST, message, GetListReportSurveyManagerTotalAction.this);
        service.execute();
    }

    private JSONObject getJsonObject(String result) {
        try {
            return JSONParsing.getJsonObj(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void handleUpdate(String json) {
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = getJsonObject(json);
            ArrayList<ReportSurveyManagerTotalModel> lstReport = new ArrayList<>();
            if (jsObj != null) {
                try {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    String TAG_ERROR_CODE = "ErrorCode";
                    if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        String TAG_SBI_LIST = "ListObject";
                        JSONArray array = jsObj.getJSONArray(TAG_SBI_LIST);
                        if (array != null) {
                            for (int index = 0; index < array.length(); index++) {
                                lstReport.add(ReportSurveyManagerTotalModel.Parse(array.getJSONObject(index)));
                            }
                            try {
                                ListReportSurveyManagerTotalActivity activity = (ListReportSurveyManagerTotalActivity) mContext;
                                activity.LoadData(lstReport);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        String TAG_ERROR = "Error";
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleUpdate(result);
    }
}