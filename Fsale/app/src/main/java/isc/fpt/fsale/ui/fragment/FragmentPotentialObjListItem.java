package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPotentialObjDetail;
import isc.fpt.fsale.action.GetPotentialObjList;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.activity.ListPotentialObjActivity;

import android.support.v4.widget.SwipeRefreshLayout;

import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.PotentialObjAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//fragment DANH SÁCH - DANH SÁCH KHÁCH HÀNG TIỀM NĂNG
public class FragmentPotentialObjListItem extends Fragment implements
        OnItemClickListener, OnScrollListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_TITLE = "section_title";
    private ListPotentialObjActivity activity;
    private int mCurrentPage = 1, mTotalPage = 1;
    private ListView mListView;
    private SwipeRefreshLayout swipeContainer;
    private EditText txtAgentName;
    private Spinner spAgent;
    private ImageView imgCreate, imgFind;

    public static FragmentPotentialObjListItem newInstance(int sectionNumber, String sectionTitle) {
        FragmentPotentialObjListItem fragment = new FragmentPotentialObjListItem();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentPotentialObjListItem() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_potential_obj_list_item, container, false);
        activity = (ListPotentialObjActivity) getActivity();
        if (activity != null)
            activity.enableSlidingMenu(true);
        Common.setupUI(getActivity(), rootView);
        bindViews(rootView);
        addEvents();
        if (activity.getPotentialList() == null) {
            getData(0, "", 1);
        } else {
            loadData(activity.getPotentialList());
        }
        return rootView;
    }

    private void addEvents() {
        imgCreate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getApplicationContext());
                if (resultCode == ConnectionResult.SUCCESS) {
                    Intent intent = new Intent(activity, CreatePotentialObjActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                } else if (resultCode == ConnectionResult.SERVICE_MISSING ||
                        resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED ||
                        resultCode == ConnectionResult.SERVICE_DISABLED) {
                    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), 1);
                    dialog.show();
                }
            }
        });

        txtAgentName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtAgentName.setError(null);
            }
        });

        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyValuePairModel item = (KeyValuePairModel) spAgent.getItemAtPosition(position);
                if (item != null && item.getID() > 0) {
                    // chọn # Tất cả
                    txtAgentName.setEnabled(true);
                    txtAgentName.requestFocus();
                    // hiện bàn phím nhập
                    Common.showSoftKeyboard(activity);
                    if (item.getID() == 5) {
                        // chọn Thời gian kết thúc Dịch Vụ ISP
                        //cập nhật giá trị edit text Giá trị cần tìm thành VD: 31/12/2015
                        txtAgentName.setHint("VD: 31/12/2015");
                    } else if (item.getID() == 7) {
                        //chọn Lịch hẹn KHTN
                        //khóa edit text Giá trị cần tìm
                        txtAgentName.setEnabled(false);
                    } else {
                        //các trường hợp còn lại cập nhật giá trị edit text thành Giá trị cần tìm
                        txtAgentName.setHint("Giá trị cần tìm");
                    }
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setHint("Giá trị cần tìm");
                    txtAgentName.setError(null);
                }
                txtAgentName.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imgFind.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // ẩn bàn phím
                Common.hideSoftKeyboard(activity);
                KeyValuePairModel agent = (KeyValuePairModel) spAgent.getSelectedItem();
                //lấy Giá trị cần tìm
                String agentName = txtAgentName.getText().toString().trim();
                //chọn option # Tất cả
                if (agent.getID() > 0) {
                    validateValue(agentName, agent);
                } else {
                    getData(agent.getID(), agentName, 1);
                }
            }
        });
    }

    @SuppressLint("SimpleDateFormat")
    private void validateValue(String agentName, KeyValuePairModel agent) {
        if (!agentName.equals("")) {
            if (agent.getID() == 5) {
                try {
                    Date endTime = convertToDate(agentName);
                    if (endTime != null) {
                        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
                        agentName = format.format(endTime);
                        getData(agent.getID(), agentName, 1);
                    } else {
                        Common.alertDialog("Ngày tháng không hợp lệ", getActivity());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                getData(agent.getID(), agentName, 1);
            }
        } else {
            if (agent.getID() != 7) {
                Common.alertDialog("Nhập giá trị cần tìm", getActivity());
            } else {
                getData(agent.getID(), agentName, 1);
            }
        }
    }

    private void bindViews(View rootView) {
        mListView = (ListView) rootView.findViewById(R.id.lv_object);
        txtAgentName = (EditText) rootView.findViewById(R.id.txt_agent_name);
        imgCreate = (ImageView) rootView.findViewById(R.id.img_create_potential_obj);
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.container);
        spAgent = (Spinner) rootView.findViewById(R.id.sp_agent);
        imgFind = (ImageView) rootView.findViewById(R.id.img_find);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main));

        swipeContainer.setOnRefreshListener(this);
        mListView.setOnItemClickListener(this);
        mListView.setOnScrollListener(this);

        ArrayList<KeyValuePairModel> lstObjType = new ArrayList<>();
        lstObjType.add(new KeyValuePairModel(0, "Tất cả"));
        lstObjType.add(new KeyValuePairModel(2, "Tên KH"));
        lstObjType.add(new KeyValuePairModel(3, "Số điện thoại"));
        lstObjType.add(new KeyValuePairModel(4, "Địa chỉ"));
        lstObjType.add(new KeyValuePairModel(5, "Thời gian kết thúc Dịch Vụ ISP"));
        lstObjType.add(new KeyValuePairModel(7, "Lịch hẹn KHTN"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(activity, R.layout.my_spinner_style, lstObjType, Gravity.CENTER);
        spAgent.setAdapter(adapter);
    }

    @SuppressLint("SimpleDateFormat")
    private Date convertToDate(String EndDate) {
        Date result = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_VN);
            result = format.parse(EndDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        try {
            Common.hideSoftKeyboard(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PotentialObjModel selectedItem = (PotentialObjModel) parentView.getItemAtPosition(position);
        if (selectedItem != null) {
            String supporter = selectedItem.getSupporter() == null ? Constants.USERNAME : selectedItem.getSupporter();
            new GetPotentialObjDetail(activity, Constants.USERNAME, selectedItem.getID(), supporter);
        }
    }

    private void getData(int agent, String agentName, int page) {
        new GetPotentialObjList(this, activity, agent, agentName, page);
    }

    //hàm callback cập nhật danh sách khtn
    public void loadData(List<PotentialObjModel> list) {
        try {
            if (list != null && list.size() > 0) {
                mTotalPage = list.get(list.size() - 1).getTotalPage();
                mCurrentPage = list.get(list.size() - 1).getCurrentPage();
                if (mListView.getAdapter() == null || mListView.getAdapter().getCount() <= 0) {
                    activity.setPotentialList(list);
                    PotentialObjAdapter adapter = new PotentialObjAdapter(activity, list);
                    mListView.setAdapter(adapter);
                } else {
                    try {
                        if (mCurrentPage > 1) {
                            ((PotentialObjAdapter) mListView.getAdapter()).addAll(list);
                            activity.setPotentialList(((PotentialObjAdapter) mListView.getAdapter()).getListData());
                        } else {
                            ((PotentialObjAdapter) mListView.getAdapter()).setListData(list);
                            activity.setPotentialList(list);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    if (mListView.getAdapter() != null)
                        ((PotentialObjAdapter) mListView.getAdapter()).clearAll();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Common.alertDialog("Không có dữ liệu", activity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int currentVisibleItemCount;
    private int currentScrollState;
    private int currentFirstVisibleItem;
    private int totalItem;

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItem = totalItemCount;
    }

    private void isScrollCompleted() {
        if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                && this.currentScrollState == SCROLL_STATE_IDLE && currentVisibleItemCount > 0) {
            if (mCurrentPage < mTotalPage) {
                mCurrentPage++;
                int agent = 0;
                String agentName = "";
                if (spAgent.getAdapter() != null) {
                    agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
                    agentName = txtAgentName.getText().toString();
                }
                getData(agent, agentName, mCurrentPage);
            }
        }
    }

    @Override
    public void onRefresh() {
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                int agent = 0;
                String agentName = "";
                if (spAgent.getAdapter() != null) {
                    agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
                    agentName = txtAgentName.getText().toString();
                }
                getData(agent, agentName, 1);
                swipeContainer.setRefreshing(false);
            }
        });
    }
}
