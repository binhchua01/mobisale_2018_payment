package isc.fpt.fsale.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.PromotionIPTVAutoCompleteAdapter;
import isc.fpt.fsale.model.PromotionIPTVModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

// màn hình lọc clkm iptv
public class PromotionIPTVFilterActivity extends BaseActivity implements OnItemClickListener {
    public static final String TAG_PROMOTION_TYPE_BOX = "TAG_PROMOTION_TYPE_BOX,";
    public static final String TAG_PROMOTION_LIST = "TAG_PROMOTION_LIST";
    public static final String TAG_PROMOTION_ITEM = "TAG_PROMOTION_ITEM";
    private EditText txtFilter;
    private ImageButton imgClear;
    private ListView lvPromotion;
    private ArrayList<PromotionIPTVModel> lst;
    private PromotionIPTVAutoCompleteAdapter adapter;
    private int typeBox;

    public PromotionIPTVFilterActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.lbl_screen_name_promotion_filter);
        MyApp.setCurrentActivity(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_promotion_filter));
        setContentView(R.layout.activity_promotion_iptv_filter);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        txtFilter = (EditText) findViewById(R.id.txt_iptv_filter);
        txtFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() > 0)
                    imgClear.setVisibility(View.VISIBLE);
                else
                    imgClear.setVisibility(View.GONE);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                try {
                    if (s.length() > 0) {
                        ((PromotionIPTVAutoCompleteAdapter) lvPromotion.getAdapter()).getFilter().filter(s);
                    } else {
                        setFilterAdapter();
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        });
        imgClear = (ImageButton) findViewById(R.id.btn_clear_text_filter_iptv_promotion);
        imgClear.setVisibility(View.GONE);
        imgClear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                txtFilter.setText("");
            }
        });
        lvPromotion = (ListView) findViewById(R.id.lv_promotion_iptv);
        lvPromotion.setOnItemClickListener(this);
        getDataFromIntent();
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(TAG_PROMOTION_LIST))
                this.lst = intent.getParcelableArrayListExtra(TAG_PROMOTION_LIST);
            if (intent.hasExtra(TAG_PROMOTION_TYPE_BOX)) {
                this.typeBox = intent.getIntExtra(TAG_PROMOTION_TYPE_BOX, 0);
            }
        }
        if (this.lst != null) {
            setFilterAdapter();
        }
    }

    private void setFilterAdapter() {
        adapter = new PromotionIPTVAutoCompleteAdapter(this, R.layout.row_auto_complete, lst);
        lvPromotion.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        getDataFromIntent();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub
        PromotionIPTVModel item = (PromotionIPTVModel) parent.getItemAtPosition(position);
        if (item != null) {
            Intent intent = new Intent();
            intent.putExtra(TAG_PROMOTION_TYPE_BOX, this.typeBox);
            intent.putExtra(TAG_PROMOTION_ITEM, item);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
}
