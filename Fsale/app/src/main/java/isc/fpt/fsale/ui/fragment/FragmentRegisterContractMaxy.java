package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetIPTVTotal;
import isc.fpt.fsale.action.maxy.GetMaxyTotal;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.activity.choose_mac_fpt_box.ChooseMacActivity;
import isc.fpt.fsale.adapter.MacAdapter;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.MacOTT;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV4;
import isc.fpt.fsale.ui.callback.OnItemClickUpdateCount;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyDeviceContractsAdapter;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyExtraAdapter;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyTVDeviceAdapter;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyTVExtraAdapter;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.ui.maxytv.model.MaxyExtra;
import isc.fpt.fsale.ui.maxytv.model.MaxyGeneral;
import isc.fpt.fsale.ui.maxytv.model.MaxySetupType;
import isc.fpt.fsale.ui.maxytv.model.MaxyTotal;
import isc.fpt.fsale.ui.maxytv.model.MaxyTypeDeploy;
import isc.fpt.fsale.ui.maxytv.model.ObjectMaxy;
import isc.fpt.fsale.ui.maxytv.ui.MaxyBox1Activity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyDeviceActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyExtraActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPackageActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPromotionBox1Activity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPromotionDeviceActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPromotionPackageActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxySetupTypeActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyTypeDeployActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.BOX_FIRST;
import static isc.fpt.fsale.utils.Constants.CLASS_NAME;
import static isc.fpt.fsale.utils.Constants.CONTRACT_EXTRA;
import static isc.fpt.fsale.utils.Constants.COUNT_DEVICE;
import static isc.fpt.fsale.utils.Constants.CUSTYPE;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_DEPLOY_CLEAR;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_EXTRA_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_PACKAGE_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_PROMOTION_BOX_1;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_PROMOTION_PACKAGE;
import static isc.fpt.fsale.utils.Constants.LOCAL_TYPE;
import static isc.fpt.fsale.utils.Constants.MAXY_GENERAL;
import static isc.fpt.fsale.utils.Constants.MAXY_PROMOTION_ID;
import static isc.fpt.fsale.utils.Constants.MAXY_PROMOTION_TYPE;
import static isc.fpt.fsale.utils.Constants.NUM_BOX;
import static isc.fpt.fsale.utils.Constants.NUM_BOX_NET;
import static isc.fpt.fsale.utils.Constants.NUM_EXTRA;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_DEVICE_BOX_1_DETAIL;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_DEVICE_DETAIL;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_EXTRA_DETAIL;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_PACKAGE_DETAIL;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_PROMOTION_BOX_1;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_PROMOTION_DEVICE;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_SETUP_TYPE;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_TYPE_DEPLOY;
import static isc.fpt.fsale.utils.Constants.POSITION;
import static isc.fpt.fsale.utils.Constants.REGTYPE;
import static isc.fpt.fsale.utils.Constants.SERVICE_TYPE;
import static isc.fpt.fsale.utils.Constants.TAG_CONTRACT;
import static isc.fpt.fsale.utils.Constants.TYPE_DEPLOY;
import static isc.fpt.fsale.utils.Constants.USE_COMBO;
import static java.lang.String.valueOf;

public class FragmentRegisterContractMaxy extends Fragment implements OnItemClickListenerV4<Object, Integer, Integer>, OnItemClickListener<Boolean>, OnItemClickUpdateCount<Boolean> {
    private Context mContext;
    private final int regType = 1;
    private RegistrationDetailModel mRegister = new RegistrationDetailModel();
    private ObjectMaxy mMaxy;
    private LinearLayout frmMaxyContainer, frmMaxyMac, frmDeviceMaxy, viewPromotionPackage, viewDevicesBox1, viewPromotionDevicesBox1;
    public TextView textPromotion, textMaxyGeneral, textInfo, textSetting, textWall, textImplement, textSelectedService,
            textCombo, textPackage, textPackagePromotion, textTotalPrice, textPriceBox1, textNameBox1, textNamePromotionBox1, textNamePriceBox1;
    public RadioButton rdoRegisterComboYes, rdoRegisterComboNo;
    public EditText selectPackage, selectPackagePromotion, mTypeDeploy, mSetupType, mBox1, mPromotionBox1;
    public Button btnSelectDeviceMaxyTV, btnSelectExtraMaxyTV, btnSelectMacMaxy;
    public RecyclerView recDeviceMaxyTV, recPackageMaxyTV, recMacMaxy;
    private MaxyDeviceContractsAdapter mMaxyDeviceAdapter;
    private MaxyTVExtraAdapter mMaxyExtraAdapter;
    private final int CODE_MAXY_DEVICE_DETAIL = 1;
    private final int CODE_MAXY_PACKAGE_DETAIL = 2;
    private final int CODE_MAXY_PROMOTION_PACKAGE_DETAIL = 3;
    private final int CODE_MAXY_PROMOTION_DEVICE = 4;
    private final int CODE_MAXY_EXTRA_DETAIL = 5;
    public List<MaxyBox> mListMaxyDevice = new ArrayList<>();
    public List<MaxyExtra> mListMaxyExtra = new ArrayList<>();
    public List<MaxyBox> listDevice;
    public MaxyTypeDeploy mMaxyDeploy;
    public MaxySetupType mMaxySetup;
    public MaxyGeneral mListMaxyGeneral = new MaxyGeneral();
    private MaxyBox maxyBox1;
    private final int CODE_MAXY_TYPE_DEPLOY = 10;
    private final int CODE_MAXY_SETUP_TYPE = 11;
    private final int CODE_MAXY_BOX_1 = 12;
    private final int CODE_MAXY_PROMOTION_BOX_1 = 13;
    private final int CODE_CHOOSE_MAC_MAXY = 14;
    public int count = 0, numBox = 0;
    public int serviceType;
    private String Contract;
    private int localTypeId;
    private List<MacOTT> macList;
    private MacAdapter macAdapter;
    public List<FPTBox> listFptBoxSelect;
    private RegisterContractActivity activity;

    public static FragmentRegisterContractMaxy newInstance() {
        FragmentRegisterContractMaxy fragment = new FragmentRegisterContractMaxy();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_contract_maxy, container, false);
        try {
            Common.setupUI(getActivity(), view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mContext = getActivity();
        getRegisterFromActivity();
        initView(view);
        initEvent();
        return view;
    }

    //0: Internet Only
    //2 : Internet + IPTV
    //3: FPT Playbox
    //5: Camera OTT
    //6: Extra OTT
    private void getRegisterFromActivity() {
        if (mContext != null && mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
            mRegister = ((RegisterContractActivity) mContext).getRegister() == null ? new RegistrationDetailModel()
                    : ((RegisterContractActivity) mContext).getRegister();
            ObjectDetailModel mObject = ((RegisterContractActivity) mContext).getObject();
            if (mObject != null) {
                Contract = mObject.getContract();
                localTypeId = mObject.getLocalType();
                serviceType = mObject.getServiceType();
            } else {
                if (mRegister != null) {
                    Contract = mRegister.getContract();
                    localTypeId = mRegister.getLocalType();
                    serviceType = mRegister.getServiceType();
                }
            }
        }
    }

    public void setupMaxyGeneral(MaxyGeneral maxyGeneral) {
        if (maxyGeneral != null) {
            if (maxyGeneral.getUseCombo() != 0) {
                rdoRegisterComboYes.setChecked(true);
            } else {
                rdoRegisterComboNo.setChecked(true);
            }
            selectPackage.setText(maxyGeneral.getPackage());
            selectPackagePromotion.setText(maxyGeneral.getPromotionDesc());
            textMaxyGeneral.setText(Common.formatMoney(valueOf(maxyGeneral.getPrice())));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case CODE_MAXY_PACKAGE_DETAIL:
                    MaxyGeneral mMaxyGeneral = data.getParcelableExtra(OBJECT_MAXY_PACKAGE_DETAIL);
                    if (mMaxyGeneral != null) {
                        listDevice.clear();
                        selectPackage.setText(mMaxyGeneral.getPackage());
                        mListMaxyGeneral = mMaxyGeneral;
                        selectPackagePromotion.setText("");
                        textMaxyGeneral.setText(Common.formatMoney(valueOf(0)));
                    }
                    clearBox();
                    clearExtra();
                    numBox = mListMaxyGeneral.getNumBox();
                    break;
                case CODE_MAXY_PROMOTION_PACKAGE_DETAIL:
                    MaxyGeneral mMaxyGe = data.getParcelableExtra(OBJECT_MAXY_PACKAGE_DETAIL);
                    if (mMaxyGe != null) {
                        selectPackagePromotion.setText(mMaxyGe.getPromotionDesc());
                        mListMaxyGeneral = mMaxyGe;
                        textMaxyGeneral.setText(Common.formatMoney(valueOf(mMaxyGe.getPrice())));
                    }
                    //clear box
                    clearBox();
                    break;
                case CODE_MAXY_DEVICE_DETAIL:
                    MaxyBox mMaxyBox = data.getParcelableExtra(OBJECT_MAXY_DEVICE_DETAIL);
                    mListMaxyDevice.add(mMaxyBox);
                    mMaxyDeviceAdapter.notifyDataChanged(mListMaxyDevice);
                    break;
                case CODE_MAXY_PROMOTION_DEVICE:
                    MaxyBox mBox = data.getParcelableExtra(OBJECT_MAXY_PROMOTION_DEVICE);
                    int pos1 = data.getIntExtra(POSITION, -1);
                    if (mBox != null) {
                        if (mListMaxyDevice.size() > 1) {
                            for (MaxyBox item : mListMaxyDevice) {
                                if (item.getBoxID() == mBox.getBoxID()) {
                                    if (item.getPromotionID() == mBox.getPromotionID()) {
                                        Common.alertDialog("Vui lòng chọn CLKM khác với CLKM thiết bị khác", this.getActivity());
                                        return;
                                    }
                                }
                            }
                        }
                        mListMaxyDevice.set(pos1, mBox);
                        mMaxyDeviceAdapter.notifyDataChanged(mListMaxyDevice);
                        removeBox2();
                        listDevice.addAll(mListMaxyDevice);
                        updateExtraOnBox();
                        updateCountDevice();
                    }
                    break;

                case CODE_MAXY_EXTRA_DETAIL:
                    MaxyExtra mMaxyExtra = data.getParcelableExtra(OBJECT_MAXY_EXTRA_DETAIL);
                    if (mMaxyExtra != null) {
                        if (this.mListMaxyExtra.size() >= 1) {
                            for (MaxyExtra item : mListMaxyExtra) {
                                if (item.getName().equals(mMaxyExtra.getName())) {
                                    mListMaxyExtra.remove(item);
                                    mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
                                    return;
                                }
                            }
                        }
                        mListMaxyExtra.add(mMaxyExtra);
                        mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
                    }
                    break;
                case CODE_MAXY_TYPE_DEPLOY:
                    MaxyTypeDeploy mMaxyTypeDeploy = data.getParcelableExtra(OBJECT_MAXY_TYPE_DEPLOY);
                    if (mMaxyTypeDeploy != null) {
                        clearMaxyContainer(false);
                        mMaxyDeploy = mMaxyTypeDeploy;
                        mTypeDeploy.setText(mMaxyTypeDeploy.getTypeDeployName());
                        frmMaxyContainer.setVisibility(View.VISIBLE);
                        mListMaxyGeneral.setTypeDeployID(mMaxyTypeDeploy.getTypeDeployID());
                        mListMaxyGeneral.setTypeDeployName(mMaxyTypeDeploy.getTypeDeployName());
                    }
                    if (mMaxyDeploy.getTypeDeployID() == 4 || mMaxyDeploy.getTypeDeployID() == 2) {
                        frmDeviceMaxy.setVisibility(View.GONE);
                        clearBox();
                    } else if (mMaxyDeploy.getTypeDeployID() == -1) {
                        clearMaxyContainer(true);
                    } else {
                        frmDeviceMaxy.setVisibility(View.VISIBLE);
                    }
                    break;
                case CODE_MAXY_SETUP_TYPE:
                    MaxySetupType mSetup = data.getParcelableExtra(OBJECT_MAXY_SETUP_TYPE);
                    if (mSetup != null) {
                        mMaxySetup = mSetup;
                        mSetupType.setText(mSetup.getTypeSetupName());
                        mListMaxyGeneral.setTypeSetupID(mSetup.getTypeSetupID());
                        mListMaxyGeneral.setTypeSetupName(mSetup.getTypeSetupName());
                        if (mMaxySetup.getTypeSetupID() == 3) {
                            frmMaxyMac.setVisibility(View.VISIBLE);
                            btnSelectMacMaxy.setVisibility(View.VISIBLE);
                        } else {
                            frmMaxyMac.setVisibility(View.GONE);
                            btnSelectMacMaxy.setVisibility(View.GONE);
                        }
                    }
                    break;
                case CODE_MAXY_BOX_1:
                    MaxyBox mBoxFirst = data.getParcelableExtra(OBJECT_MAXY_DEVICE_BOX_1_DETAIL);
                    maxyBox1 = mBoxFirst;
                    assert mBoxFirst != null;
                    removeBox1();
                    mPromotionBox1.getText().clear();
                    textPriceBox1.setText(Common.formatMoney(valueOf(0)));
                    mBox1.setText(mBoxFirst.getBoxName());
                    break;
                case CODE_MAXY_PROMOTION_BOX_1:
                    MaxyBox mProBoxFirst = data.getParcelableExtra(OBJECT_MAXY_PROMOTION_BOX_1);
                    if (mProBoxFirst != null) {
                        maxyBox1 = mProBoxFirst;
                        removeBox1();
                        listDevice.add(mProBoxFirst);
                        mPromotionBox1.setText(mProBoxFirst.getPromotionDesc());
                        textPriceBox1.setText(Common.formatMoney(valueOf(mProBoxFirst.getPrice())));
                        if (mProBoxFirst.getBoxType() == 1) {
                            updateExtraOnBox();
                        }
                    }
                    updateExtraOnBox();
                    updateCountDevice();
                    break;
                case CODE_CHOOSE_MAC_MAXY:
                    if (macList.size() > 0) {
                        macList.clear();
                    }
                    macList = data.getParcelableArrayListExtra(Constants.LIST_MAC_SELECTED);
                    mListMaxyGeneral.setListMACOTT(macList);
                    macAdapter.notifyData(macList);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + requestCode);
            }
        }
    }

    private void initView(View view) {
        //ui
        textSelectedService = view.findViewById(R.id.text_selected_service);
        textCombo = view.findViewById(R.id.text_register_combo);
        textPackage = view.findViewById(R.id.text_select_package);
        textPackagePromotion = view.findViewById(R.id.text_promotion);
        textTotalPrice = view.findViewById(R.id.text_money_total);
        rdoRegisterComboYes = view.findViewById(R.id.rdo_combo_yes);
        rdoRegisterComboNo = view.findViewById(R.id.rdo_combo_no);
        textNameBox1 = view.findViewById(R.id.text_device_box1);
        textNamePromotionBox1 = view.findViewById(R.id.text_promotion_device_box1);
        textNamePriceBox1 = view.findViewById(R.id.text_price_box1);

        textMaxyGeneral = view.findViewById(R.id.text_total_price_maxy_genegal);
        selectPackage = view.findViewById(R.id.txt_trienkhai);
        selectPackagePromotion = view.findViewById(R.id.txt_clkm);
        btnSelectDeviceMaxyTV = view.findViewById(R.id.btn_add_device_maxy_tv);
        btnSelectExtraMaxyTV = view.findViewById(R.id.btn_add_package_extra);
        recDeviceMaxyTV = view.findViewById(R.id.list_device_maxy_tv);
        recPackageMaxyTV = view.findViewById(R.id.list_package_extra);
        viewPromotionPackage = view.findViewById(R.id.view_promotion_package);
        viewDevicesBox1 = view.findViewById(R.id.view_devices_box1);
        viewPromotionDevicesBox1 = view.findViewById(R.id.view_promotion_devices_box1);

        mTypeDeploy = view.findViewById(R.id.txt_type_deploy);
        frmDeviceMaxy = view.findViewById(R.id.view_devices);
        frmMaxyContainer = view.findViewById(R.id.view_maxy_container);
        frmMaxyContainer.setVisibility(View.GONE);
        mSetupType = view.findViewById(R.id.txt_type_deployment);
        btnSelectMacMaxy = view.findViewById(R.id.btn_scan_mac_maxy);

        mBox1 = view.findViewById(R.id.txt_device_box1);
        mPromotionBox1 = view.findViewById(R.id.txt_promotion_box1);
        textPriceBox1 = view.findViewById(R.id.txt_total_price_box1);

        frmMaxyMac = view.findViewById(R.id.view_list_mac_maxy);
        recMacMaxy = view.findViewById(R.id.lv_list_mac_maxy);
        listFptBoxSelect = new ArrayList<>();
        if (mRegister.getObjectMaxy() == null) {
            mRegister.setObjectMaxy(new ObjectMaxy());
        }

        macList = new ArrayList<>();
        macAdapter = new MacAdapter(getActivity(), macList);
        recMacMaxy.setAdapter(macAdapter);

        listDevice = new ArrayList<>();
        listDevice = mRegister.getObjectMaxy().getMaxyBox() != null ?
                mRegister.getObjectMaxy().getMaxyBox() : new ArrayList<>();
        mListMaxyDevice = new ArrayList<>();
        mMaxyDeviceAdapter = new MaxyDeviceContractsAdapter(getActivity(), mListMaxyDevice, this, this);
        recDeviceMaxyTV.setAdapter(mMaxyDeviceAdapter);
        maxyBox1 = new MaxyBox();
        for (MaxyBox item : listDevice) {
            if (item.getBoxFirst() != 1) {
                mListMaxyDevice.add(item);
            } else {
                maxyBox1 = item;
                mBox1.setText(item.getBoxName());
                mPromotionBox1.setText(item.getPromotionDesc());
                textPriceBox1.setText(Common.formatMoney(valueOf(item.getPrice())));
            }
        }
        mListMaxyExtra = mRegister.getObjectMaxy().getMaxyExtra() != null ?
                mRegister.getObjectMaxy().getMaxyExtra() : new ArrayList<>();
        mMaxyExtraAdapter = new MaxyTVExtraAdapter(getActivity(), mListMaxyExtra, this, this);
        recPackageMaxyTV.setAdapter(mMaxyExtraAdapter);
        updateCountDevice();
        mMaxyDeploy = new MaxyTypeDeploy();
        if (mRegister.getObjectMaxy().getMaxyGeneral() != null) {
            mListMaxyGeneral = mRegister.getObjectMaxy().getMaxyGeneral();
            //Hình thức bán
            mTypeDeploy.setText(mListMaxyGeneral.getTypeDeployName());
            numBox = mListMaxyGeneral.getNumBox();
            mMaxyDeploy.setTypeDeployID(mListMaxyGeneral.getTypeDeployID());
            mMaxyDeploy.setTypeDeployName(mListMaxyGeneral.getTypeDeployName());
            frmMaxyContainer.setVisibility(View.VISIBLE);
            if (mListMaxyGeneral.getTypeDeployID() == 4 || mListMaxyGeneral.getTypeDeployID() == 2) {
                frmDeviceMaxy.setVisibility(View.GONE);
            } else {
                frmDeviceMaxy.setVisibility(View.VISIBLE);
            }
            //triển khai
            if (mListMaxyGeneral.getListMACOTT() != null) {
                macList = mListMaxyGeneral.getListMACOTT();
            } else {
                macList = new ArrayList<>();
            }
            mSetupType.setText(mListMaxyGeneral.getTypeSetupName());
            if (mListMaxyGeneral.getTypeSetupID() == 3) {
                frmMaxyMac.setVisibility(View.VISIBLE);
                macAdapter.notifyData(macList);
            } else {
                frmMaxyMac.setVisibility(View.GONE);
            }
            selectPackage.setText(mListMaxyGeneral.getPackage());
            selectPackagePromotion.setText(mListMaxyGeneral.getPromotionDesc());
            textMaxyGeneral.setText(Common.formatMoney(valueOf(mListMaxyGeneral.getPrice())));

            if (mRegister.getObjectMaxy().getMaxyGeneral().getUseCombo() > 0) {
                rdoRegisterComboYes.setChecked(true);
            } else {
                rdoRegisterComboNo.setChecked(true);
            }
        } else {
            mRegister.getObjectMaxy().setMaxyGeneral(new MaxyGeneral());
            this.mListMaxyGeneral = new MaxyGeneral();
        }
        updateServiceTypeMaxy(serviceType);
    }

    private void initEvent() {
        btnSelectDeviceMaxyTV.setOnClickListener(v -> {
            if (mListMaxyGeneral.getPackageID() != 0) {
                if (serviceType == 0 && getBox1().getBoxID() == 0 && getBox1().getPromotionID() == 0) {
                    Toast.makeText(getContext(), getString(R.string.msg_select_box1_before_box_2), Toast.LENGTH_LONG).show();
                } else {
                    if (count < mListMaxyGeneral.getNumBox()) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList(LIST_MAXY_DETAIL_SELECTED,
                                (ArrayList<? extends Parcelable>) mListMaxyDevice);
                        bundle.putInt(REGTYPE, 1);
                        bundle.putInt(BOX_FIRST, 2);
                        bundle.putInt(LOCAL_TYPE, localTypeId);
                        bundle.putInt(MAXY_PROMOTION_ID, mListMaxyGeneral.getPromotionID());
                        bundle.putInt(MAXY_PROMOTION_TYPE, mListMaxyGeneral.getPromotionType());
                        bundle.putString(TAG_CONTRACT, Contract);
                        bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractMaxy.class.getSimpleName());
                        Intent intent = new Intent(getActivity(), MaxyDeviceActivity.class);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, CODE_MAXY_DEVICE_DETAIL);
                    } else {
                        Toast.makeText(getContext(), getString(R.string.msg_select_num_box), Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.text_select_service_tv), Toast.LENGTH_SHORT).show();
            }
        });

        btnSelectExtraMaxyTV.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(LIST_MAXY_EXTRA_SELECTED,
                    (ArrayList<? extends Parcelable>) mListMaxyExtra);
            bundle.putInt(REGTYPE, 1);
            bundle.putInt(LIST_MAXY_DETAIL_SELECTED, listDevice.size());
            bundle.putParcelable(MAXY_GENERAL, mListMaxyGeneral);
            bundle.putString(CONTRACT_EXTRA, Contract);
            bundle.putInt(NUM_EXTRA, countExtraBox());
            bundle.putInt(NUM_BOX_NET, countBoxOnNet());
            bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractMaxy.class.getSimpleName());
            Intent intent = new Intent(getActivity(), MaxyExtraActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_MAXY_EXTRA_DETAIL);
        });

        selectPackage.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractMaxy.class.getSimpleName());
            bundle.putParcelable(LIST_MAXY_PACKAGE_SELECTED, mListMaxyGeneral);
            bundle.putInt(REGTYPE, 1);
            bundle.putInt(USE_COMBO, mListMaxyGeneral.getUseCombo());
            bundle.putInt(LOCAL_TYPE, localTypeId);
            bundle.putString(TAG_CONTRACT, Contract);
            Intent intent = new Intent(getActivity(), MaxyPackageActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_MAXY_PACKAGE_DETAIL);
        });
        if (serviceType == 0) {
            selectPackagePromotion.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                if (mListMaxyGeneral.getPackageID() != 0) {
                    bundle.putParcelable(LIST_MAXY_PROMOTION_PACKAGE, mListMaxyGeneral);
                    bundle.putInt(REGTYPE, 1);
                    bundle.putInt(LOCAL_TYPE, localTypeId);
                    bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractMaxy.class.getSimpleName());
                    Intent intent = new Intent(getActivity(), MaxyPromotionPackageActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_MAXY_PROMOTION_PACKAGE_DETAIL);
                } else {
                    Common.alertDialog(getString(R.string.text_select_promotion_service_maxy), getContext());
                }
            });

            rdoRegisterComboYes.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    mListMaxyGeneral.setUseCombo(3);
                }
            });

            rdoRegisterComboNo.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    mListMaxyGeneral.setUseCombo(0);
                }
            });

            mBox1.setOnClickListener(v -> {
                if (mListMaxyGeneral.getPackageID() != 0) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                    bundle.putParcelable(LIST_MAXY_DETAIL_SELECTED, maxyBox1);
                    bundle.putInt(REGTYPE, regType);
                    bundle.putInt(BOX_FIRST, 1);
                    bundle.putInt(LOCAL_TYPE, localTypeId);
                    bundle.putInt(MAXY_PROMOTION_ID, mListMaxyGeneral.getPromotionID());
                    bundle.putInt(MAXY_PROMOTION_TYPE, mListMaxyGeneral.getPromotionType());
                    bundle.putString(TAG_CONTRACT, Contract);
                    Intent intent = new Intent(getActivity(), MaxyBox1Activity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_MAXY_BOX_1);
                } else {
                    Toast.makeText(getContext(), getString(R.string.text_select_service_tv), Toast.LENGTH_SHORT).show();
                }
            });

            mPromotionBox1.setOnClickListener(v -> {
                assert maxyBox1 != null;
                if (maxyBox1.getBoxID() != 0) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(LIST_MAXY_PROMOTION_BOX_1, maxyBox1);
                    bundle.putInt(REGTYPE, regType);
                    bundle.putInt(BOX_FIRST, 1);
                    bundle.putInt(LOCAL_TYPE, localTypeId);
                    bundle.putInt(MAXY_PROMOTION_ID, mListMaxyGeneral.getPromotionID());
                    bundle.putString(TAG_CONTRACT, Contract);
                    bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractMaxy.class.getSimpleName());
                    Intent intent = new Intent(getActivity(), MaxyPromotionBox1Activity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_MAXY_PROMOTION_BOX_1);
                } else {
                    Toast.makeText(getContext(), getString(R.string.text_select_promotion_service_maxy_device), Toast.LENGTH_SHORT).show();
                }
            });
        }
        mTypeDeploy.setOnClickListener(v -> {
            if (localTypeId != 0) {
                Bundle bundle = new Bundle();
                bundle.putInt(REGTYPE, regType);
                bundle.putInt(LOCAL_TYPE, localTypeId);
                bundle.putString(TAG_CONTRACT, Contract);
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractMaxy.class.getSimpleName());
                Intent intent = new Intent(getContext(), MaxyTypeDeployActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_MAXY_TYPE_DEPLOY);
            } else {
                Toast.makeText(getContext(), getString(R.string.msg_select_service_intenet), Toast.LENGTH_SHORT).show();
            }
        });

        mSetupType.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt(REGTYPE, regType);
            bundle.putInt(LOCAL_TYPE, localTypeId);
            bundle.putInt(TYPE_DEPLOY, mListMaxyGeneral.getTypeDeployID());
            bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractMaxy.class.getSimpleName());
            Intent intent = new Intent(getContext(), MaxySetupTypeActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_MAXY_SETUP_TYPE);
        });

        btnSelectMacMaxy.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ChooseMacActivity.class);
            intent.putParcelableArrayListExtra(Constants.LIST_FPT_BOX_SELECTED,
                    (ArrayList<? extends Parcelable>) listDevice);
            intent.putParcelableArrayListExtra(Constants.LIST_MAC_SELECTED,
                    (ArrayList<? extends Parcelable>) macList);
            startActivityForResult(intent, CODE_CHOOSE_MAC_MAXY);
        });

    }

    @SuppressLint("ResourceAsColor")
    public void updateServiceTypeMaxy(int serviceType) {
        if (serviceType != 0) {
            textCombo.setTextColor(R.color.gray);
            textPackagePromotion.setTextColor(R.color.gray);
            textTotalPrice.setTextColor(R.color.gray);
            rdoRegisterComboYes.setTextColor(R.color.gray);
            rdoRegisterComboNo.setTextColor(R.color.gray);
            textNameBox1.setTextColor(R.color.gray);
            textNamePromotionBox1.setTextColor(R.color.gray);
            textNamePriceBox1.setTextColor(R.color.gray);
            rdoRegisterComboYes.setEnabled(true);
            rdoRegisterComboYes.setClickable(false);
            rdoRegisterComboNo.setClickable(false);

            viewPromotionPackage.setBackgroundResource(R.drawable.background_radius_disable);
            viewDevicesBox1.setBackgroundResource(R.drawable.background_radius_disable);
            viewPromotionDevicesBox1.setBackgroundResource(R.drawable.background_radius_disable);
        }
    }

    public void updateRegister() {
        try {
            if (mContext.getClass().getSimpleName()
                    .equals(RegisterContractActivity.class.getSimpleName())) {
                RegisterContractActivity activity = (RegisterContractActivity) mContext;
                RegistrationDetailModel register = activity.getRegister();
                register.setObjectMaxy(new ObjectMaxy(mListMaxyGeneral, listDevice, mListMaxyExtra));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private MaxyBox getBox1() {
        MaxyBox box = new MaxyBox();
        for (MaxyBox item : listDevice) {
            if (item.getBoxFirst() == 1) {
                box = item;
            }
        }
        return box;
    }

    public int countBoxOnNet() {
        int count = 0;
        for (MaxyBox item : listDevice) {
            if (item.getBoxType() == 1) {
                count += item.getBoxCount();
            }
        }
        return count;
    }

    private void updateExtraOnBox() {
        for (MaxyExtra item : mListMaxyExtra) {
            item.setChargeTimes(1);
        }
        mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
    }

    private void clearExtra() {
        mListMaxyExtra.clear();
        mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
    }

    private void removeBox1() {
        List<MaxyBox> mBox = new ArrayList<>();
        for (MaxyBox item : listDevice) {
            if (item.getBoxFirst() == 1) {
                mBox.add(item);
                Log.e("listDeviceBox1", mBox.size() + "");
            }
        }
        listDevice.removeAll(mBox);
    }

    private void removeBox2() {
        List<MaxyBox> mBox = new ArrayList<>();
        for (MaxyBox item : listDevice) {
            if (item.getBoxFirst() == 2) {
                mBox.add(item);
                Log.e("listDevice", mBox.size() + "");
            }
        }
        listDevice.removeAll(mBox);
        updateCountDevice();
    }

    public int countExtraBox() {
        int count = 0;
        for (MaxyExtra item : mListMaxyExtra) {
            if (item.getExtraType() == 1) {
                count += 1;
            }
        }
        return count;
    }

    public List<MacOTT> getMacList() {
        return macList;
    }

    @Override
    public void onItemClickV4(Object mObject, Integer position, Integer type) {
        Bundle bundle = new Bundle();
        Intent intent;
        switch (type) {
            case 6:
                MaxyBox maxyBox = (MaxyBox) mObject;
                mListMaxyDevice.remove(maxyBox);
                listDevice.remove(maxyBox);
                mMaxyDeviceAdapter.notifyDataChanged(this.mListMaxyDevice);
                if (maxyBox.getBoxType() == 1) {
                    updateExtraOnBox();
                }
                updateCountDevice();
                break;
            case 7:
                MaxyBox mBox = (MaxyBox) mObject;
                bundle.putParcelable(LIST_MAXY_DETAIL_SELECTED, mBox);
                bundle.putInt(POSITION, position);
                bundle.putInt(REGTYPE, 1);
                bundle.putInt(BOX_FIRST, 2);
                bundle.putInt(LOCAL_TYPE, localTypeId);
                bundle.putString(TAG_CONTRACT, Contract);
                bundle.putInt(MAXY_PROMOTION_ID, mListMaxyGeneral.getPromotionID());
                bundle.putInt(MAXY_PROMOTION_TYPE, mListMaxyGeneral.getPromotionType());
                bundle.putInt(NUM_BOX, numBox);
                bundle.putInt(COUNT_DEVICE, count);
                bundle.putString(CLASS_NAME, FragmentRegisterContractMaxy.class.getSimpleName());
                intent = new Intent(getContext(), MaxyPromotionDeviceActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_MAXY_PROMOTION_DEVICE);
                break;
            case 8:
                MaxyExtra maxyExtra = (MaxyExtra) mObject;
                mListMaxyExtra.remove(maxyExtra);
                mMaxyExtraAdapter.notifyDataChanged(this.mListMaxyExtra);
                break;
            default:
                break;
        }
    }

    public void getMaxyTotal(Context mContext) {
        mRegister = updateRegisterFromView(mRegister);
        if (mListMaxyGeneral.getTypeDeployID() != 0 && mListMaxyGeneral.getTypeDeployID() != -1) {
            if (mListMaxyDevice.size() > 0) {
                List<MaxyBox> maxyBox = mListMaxyDevice;
                for (MaxyBox item : maxyBox) {
                    if (item.getBoxID() != 0 && item.getPromotionID() == 0) {
                        Common.alertDialog("Vui lòng chọn CLKM thiết bị dịch vụ FPT Play", this.getActivity());
                        return;
                    }
                }
            }
            if (mListMaxyGeneral.getPackageID() == 0) {
                Common.alertDialog(getString(R.string.text_select_service_tv), getActivity());
                return;
            }
            if (serviceType == 0) {
                if (mListMaxyGeneral.getPromotionID() == 0) {
                    Common.alertDialog(getString(R.string.text_select_promotion_service_maxy), getActivity());
                    return;
                }
            }
            if (mListMaxyGeneral.getTypeSetupID() == 0) {
                Common.alertDialog(getString(R.string.text_select_setup_maxy), this.getActivity());
                return;
            }
            if (maxyBox1 != null && maxyBox1.getBoxID() != 0) {
                if (maxyBox1.getPromotionID() == 0) {
                    Common.alertDialog("Vui lòng chọn CLKM Box 1", getActivity());
                    return;
                }
            }
            mRegister.setContract(Contract);
            new GetMaxyTotal(mContext, mRegister.toJSONObjectGetMaxyTotal(), this);
        }
    }

    private RegistrationDetailModel updateRegisterFromView(RegistrationDetailModel register) {
        try {
            if (register == null)
                register = new RegistrationDetailModel();

            if (register.getObjectMaxy() == null)
                register.setObjectMaxy(new ObjectMaxy());

            register.setRegType(1);

            if (mListMaxyGeneral.getPackageID() != 0 || mListMaxyGeneral != null) {
                register.getObjectMaxy().setMaxyGeneral(mListMaxyGeneral);
            } else {
                register.getObjectMaxy().setMaxyGeneral(new MaxyGeneral());
                mRegister.getObjectMaxy().getMaxyGeneral().setUseCombo(3);
            }
            if (listDevice.size() > 0) {
                register.getObjectMaxy().setMaxyBox(listDevice);
            } else {
                register.getObjectMaxy().setMaxyBox(new ArrayList<>());
            }

            if (mListMaxyExtra.size() > 0) {
                register.getObjectMaxy().setMaxyExtra(mListMaxyExtra);
            } else {
                register.getObjectMaxy().setMaxyExtra(new ArrayList<>());
            }

            if (macList != null || macList.size() > 0) {
                register.getObjectMaxy().getMaxyGeneral().setListMACOTT(macList);
            } else {
                register.getObjectMaxy().getMaxyGeneral().setListMACOTT(new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return register;
    }

    private void updateCountDevice() {
        count = 0;
        for (MaxyBox item : listDevice) {
            count += item.getBoxCount();
        }
    }

    private boolean checkMaxyOnDevice() {
        int countMaxy = 0;
        for (MaxyExtra item : mListMaxyExtra) {
            //1:Theo box  6:hợp đồng
            if (item.getExtraType() == 1) {
                countMaxy += 1;
            }
        }
        if (mListMaxyGeneral.getTypeDeployID() == 2
                || mListMaxyGeneral.getTypeDeployID() == 4) {
            return true;
        } else {
            if (countMaxy < count || count == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    private void clearMaxyContainer(boolean flag) {
        if (flag) {
            frmMaxyContainer.setVisibility(View.GONE);
            mMaxyDeploy = new MaxyTypeDeploy();
            mTypeDeploy.setText("");
        }
        clearBox();
        clearMaxy();
        mListMaxyGeneral = new MaxyGeneral();
        selectPackage.setText("");
        selectPackagePromotion.setText("");
        textMaxyGeneral.setText(Common.formatMoney(valueOf(0)));
        mListMaxyGeneral.setUseCombo(3);
        mRegister.getObjectMaxy().getMaxyGeneral().setUseCombo(3);
        rdoRegisterComboYes.setChecked(true);
        mMaxySetup = new MaxySetupType();
        mSetupType.setText("");
        macList.clear();
        macAdapter.notifyData(macList);
        btnSelectMacMaxy.setVisibility(View.GONE);
    }

    private void clearBox() {
        updateCountDevice();
        listDevice.clear();
        maxyBox1 = new MaxyBox();
        mBox1.setText("");
        mPromotionBox1.setText("");
        textPriceBox1.setText(Common.formatMoney(valueOf(0)));
        clearBox2();
    }

    private void clearBox2() {
        updateCountDevice();
        mListMaxyDevice.clear();
        mMaxyDeviceAdapter.notifyDataSetChanged();
    }

    @SuppressLint("SetTextI18n")
    private void clearMaxy() {
        mRegister.getObjectMaxy().setMaxyGeneral(new MaxyGeneral());
        mListMaxyGeneral = new MaxyGeneral();
        mRegister.getObjectMaxy().getMaxyGeneral().setUseCombo(3);
        rdoRegisterComboYes.setChecked(true);
        mMaxyDeploy = new MaxyTypeDeploy();
        mMaxySetup = new MaxySetupType();
        clearBox();
        //----------------------//
        mListMaxyDevice.clear();
        mListMaxyExtra.clear();
        mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
        mMaxyDeviceAdapter.notifyDataChanged(mListMaxyDevice);
        //--------------------//
        selectPackage.setText("");
        selectPackagePromotion.setText("");
        textMaxyGeneral.setText(Common.formatMoney(valueOf(0)));
    }

    @Override
    public void onItemClick(Boolean object) {

    }

    @Override
    public void onItemClickUpdateCount(Boolean object) {
        if (object) {
            for (MaxyExtra item : mListMaxyExtra) {
                item.setChargeTimes(1);
            }
            mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
            updateCountDevice();
        }
    }

}
