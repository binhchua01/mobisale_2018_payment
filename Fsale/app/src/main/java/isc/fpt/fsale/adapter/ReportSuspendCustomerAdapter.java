package isc.fpt.fsale.adapter;

import isc.fpt.fsale.activity.ListReportSuspendCustomerDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportSuspendCustomerModel;
import isc.fpt.fsale.utils.Common;
import java.util.ArrayList;
import java.util.List;

import com.danh32.fontify.TextView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;


public class ReportSuspendCustomerAdapter extends BaseAdapter{
	public static final String TAG_SALE_NAME = "TAG_SALE_NAME";
	public static final String TAG_MONTH = "TAG_MONTH";
	public static final String TAG_YEAR = "TAG_YEAR";
	public static final String TAG_LOCAL_TYPE = "TAG_LOCAL_TYPE";
	public static final String TAG_PACKAGE = "TAG_PACKAGE";
	
	private List<ReportSuspendCustomerModel> mList;	
	private Context mContext;
	private int month, year;
	
	public ReportSuspendCustomerAdapter(Context context, ArrayList<ReportSuspendCustomerModel> lst, int month, int year){
		this.mContext = context;
		this.mList = lst;		
		this.month = month;
		this.year = year;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportSuspendCustomerModel item = mList.get(position);		

		if (convertView == null) {			
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_sbi_total, null);
		}
		try {
			TextView rowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			TextView saleName = (TextView)convertView.findViewById(R.id.lbl_report_sale_name);
			LinearLayout frmData = (LinearLayout)convertView.findViewById(R.id.frm_report_data);
			if(frmData.getChildCount()>0){
				frmData.removeAllViews();
			}			
			if(item != null){					
				rowNumber.setText(String.valueOf(item.getRowNumber()));
				saleName.setText(item.getSaleName());		
				frmData.addView(initRowItem(item.getSaleName(), "Tổng số HĐ: ", item.getTotal()));
			}				
		} catch (Exception e) {

			Common.alertDialog("ReportPotentialObjTotalAdapter.getView():" + e.getMessage(), mContext);
		}
		
		return convertView;
	}
	
	@SuppressLint("InflateParams")
	private LinearLayout initRowItem(final String saleName, String title, int total){
		try {
			final int value = total;
			View viewChild = LayoutInflater.from(mContext).inflate( R.layout.sub_row_report_total, null );
			LinearLayout frmDataRow = (LinearLayout)viewChild.findViewById(R.id.frm_report_data_row);
			frmDataRow.setVisibility(View.VISIBLE);		
			TextView lblTitle = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_title);
			TextView lblTotal = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_total);
			lblTitle.setText(title);
			lblTotal.setText(String.valueOf(value));
			lblTotal.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub					
					try {
						if(value > 0)
							startAcitivity(saleName);	
					} catch (Exception e) {

						e.printStackTrace();
					}
									
				}
			});
			return frmDataRow;
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			return null;
		}		
	}
	
	private void startAcitivity(String saleName){
		Intent intent = new Intent(mContext, ListReportSuspendCustomerDetailActivity.class);
		if(saleName != null && !saleName.equals(""))
			intent.putExtra(TAG_SALE_NAME, saleName);
		if(this.month >0)
			intent.putExtra(TAG_MONTH, this.month);
		if(this.year >0)
			intent.putExtra(TAG_YEAR, this.year);
		/*if(item != null){
			intent.putExtra(TAG_LOCAL_TYPE, item.getLocalType());
			intent.putExtra(TAG_PACKAGE, item.getPackage());
		}*/
		mContext.startActivity(intent);
	}
}
