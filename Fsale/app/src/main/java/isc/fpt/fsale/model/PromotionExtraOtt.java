package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haulc3 on 26,July,2019
 */
public class PromotionExtraOtt implements Parcelable {
    @SerializedName("CommandText")
    @Expose
    private String commandText;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("MaxCount")
    @Expose
    private Integer maxCount;
    @SerializedName("MinCount")
    @Expose
    private Integer minCount;
    @SerializedName("TotalAmount")
    @Expose
    private Integer totalAmount;
    @SerializedName("TotalAmountVAT")
    @Expose
    private Integer totalAmountVAT;

    protected PromotionExtraOtt(Parcel in) {
        commandText = in.readString();
        if (in.readByte() == 0) {
            iD = null;
        } else {
            iD = in.readInt();
        }
        if (in.readByte() == 0) {
            maxCount = null;
        } else {
            maxCount = in.readInt();
        }
        if (in.readByte() == 0) {
            minCount = null;
        } else {
            minCount = in.readInt();
        }
        if (in.readByte() == 0) {
            totalAmount = null;
        } else {
            totalAmount = in.readInt();
        }
        if (in.readByte() == 0) {
            totalAmountVAT = null;
        } else {
            totalAmountVAT = in.readInt();
        }
    }

    public static final Creator<PromotionExtraOtt> CREATOR = new Creator<PromotionExtraOtt>() {
        @Override
        public PromotionExtraOtt createFromParcel(Parcel in) {
            return new PromotionExtraOtt(in);
        }

        @Override
        public PromotionExtraOtt[] newArray(int size) {
            return new PromotionExtraOtt[size];
        }
    };

    public String getCommandText() {
        return commandText;
    }

    public void setCommandText(String commandText) {
        this.commandText = commandText;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }

    public Integer getMinCount() {
        return minCount;
    }

    public void setMinCount(Integer minCount) {
        this.minCount = minCount;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalAmountVAT() {
        return totalAmountVAT;
    }

    public void setTotalAmountVAT(Integer totalAmountVAT) {
        this.totalAmountVAT = totalAmountVAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(commandText);
        if (iD == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(iD);
        }
        if (maxCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(maxCount);
        }
        if (minCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(minCount);
        }
        if (totalAmount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalAmount);
        }
        if (totalAmountVAT == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalAmountVAT);
        }
    }
}
