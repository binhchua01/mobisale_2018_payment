package isc.fpt.fsale.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetDistrictList;
import isc.fpt.fsale.action.GetStreetOrCondo;
import isc.fpt.fsale.action.GetWardList;
import isc.fpt.fsale.activity.DetailRegistrationDocumentActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.UploadRegistrationDocumentActivity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.IdentityCard;
import isc.fpt.fsale.model.ImageDocument;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.activity.RegisterDetailActivity.TAG_REG_ID;
import static isc.fpt.fsale.utils.DateTimeHelper.formatDateOfBirthRegister;

public class FragmentRegisterStep1 extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    public ScrollView scrollMain;
    public LinearLayout TaxNumLayout, SDT1SpinnerLayout, AddressLayoutLast;
    public EditText txtCustomerName, txtIdentityCard, txtAddressId,
            txtTaxNum, txtEmail, txtLot, txtFloor, txtRoom, txtHouseNum, txtPhone1,
            txtContactPhone1, txtHouseDesc, txtDescriptionIBB, txtPhone2, txtContactPhone2;
    public EditText txtBirthDay;
    public Calendar myCalendar;
    public DatePickerReportDialog mDateDialog;
    public Spinner spPhone1, spPhone2, spCusType, spCusDetail, spDistricts, spWards,
            spHouseTypes, spStreets, spApartment, spHousePosition, spSourceType;
    public ViewGroup apartmentPanel, housePositionPanel, houseNumberPanel;
    public ImageView imgUpdateCacheDistrict, imgUpdateCacheWard;
    public Button btnShowHouseNumFormat, btnDetailImageDocument, btnUploadImageDocument,
            btOpenLocalIdentityCard, btnCaptureIdentityCard;
    public RegistrationDetailModel modelDetail = null;
    public String pathImageIdentityCard;
    public static final int SELECT_PICTURE = 2;
    // danh sách ảnh hồ sơ
    public ArrayList<ImageDocument> listImageDocument = new ArrayList<>();
    // danh sách ảnh cmnd
    private ArrayList<ImageDocument> listImageDocumentIdentityCard = new ArrayList<>();
    // trạng thái cập nhật pdk
    public static final String TAG_UPDATE_REGISTRATION = "TAG_UPDATE_REGISTRATION";
    public static final String TAG_REGISTRATION_CREATE_FORM = "TAG_REGISTRATION_CREATE_FORM";
    public static final String TAG_VIEW_SIGNATURE = "TAG_VIEW_SIGNATURE";
    public static final String TAG_IMAGE_DOCUMENT_IDENTITY_CARD_SELECTED = "TAG_IMAGE_DOCUMENT_IDENTITY_CARD_SELECTED";
    public static final String TAG_UPLOAD_IMAGE_TYPE = "TAG_UPLOAD_IMAGE_TYPE";
    public static final String TAG_LIST_IMAGE_DOCUMENT_SELECTED = "TAG_LIST_IMAGE_DOCUMENT_SELECTED";
    // trạng thái cập nhật pdk
    private boolean statusUpdateRegistration;
    //ID các ảnh đã upload
    public String strListImageDocumentInfo = "";
    // file ảnh sau khi chụp ảnh cmnd
    public File fileIdentityCardImageDocument;
    private boolean isFirstLoading = true;
    private RegisterActivityNew activity;

    public static FragmentRegisterStep1 newInstance(RegistrationDetailModel modelDetail) {
        return new FragmentRegisterStep1(modelDetail);
    }

    @SuppressLint("ValidFragment")
    public FragmentRegisterStep1(RegistrationDetailModel modelDetail) {
        super();
        this.modelDetail = modelDetail;
    }

    public FragmentRegisterStep1() {
    }

    @Override
    protected void bindData() {
        initSpinners();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_register_step1;
    }

    protected void initView(View view) {
        activity = (RegisterActivityNew) getActivity();
        scrollMain = (ScrollView) view.findViewById(R.id.frm_main);
        TaxNumLayout = (LinearLayout) view.findViewById(R.id.frm_tax_num);
        SDT1SpinnerLayout = (LinearLayout) view.findViewById(R.id.frm_phone_number_1);
        AddressLayoutLast = (LinearLayout) view.findViewById(R.id.frm_address_input);
        apartmentPanel = (ViewGroup) view.findViewById(R.id.frm_apartment);
        apartmentPanel.setVisibility(View.GONE);
        housePositionPanel = (ViewGroup) view.findViewById(R.id.frm_house_position);
        housePositionPanel.setVisibility(View.GONE);
        houseNumberPanel = (ViewGroup) view.findViewById(R.id.frm_house_num);
        houseNumberPanel.setVisibility(View.VISIBLE);
        txtCustomerName = (EditText) view.findViewById(R.id.txt_customer_name);
        txtCustomerName.requestFocus();
        txtIdentityCard = (EditText) view.findViewById(R.id.txt_identity_card);
        txtBirthDay = (EditText) view.findViewById(R.id.txt_birthday);
        myCalendar = Calendar.getInstance();
        txtAddressId = (EditText) view.findViewById(R.id.txt_address_in_id);
        txtTaxNum = (EditText) view.findViewById(R.id.txt_tax_num);
        txtEmail = (EditText) view.findViewById(R.id.txt_email);
        spApartment = (Spinner) view.findViewById(R.id.tv_apartment_name);
        txtRoom = (EditText) view.findViewById(R.id.txt_room);
        txtFloor = (EditText) view.findViewById(R.id.txt_floor);
        txtLot = (EditText) view.findViewById(R.id.txt_apartment_group);
        txtHouseNum = (EditText) view.findViewById(R.id.txt_house_num);
        txtPhone1 = (EditText) view.findViewById(R.id.txt_phone_1);
        txtHouseDesc = (EditText) view.findViewById(R.id.txt_note_address);
        txtDescriptionIBB = (EditText) view.findViewById(R.id.txt_description_ibb);
        txtPhone2 = (EditText) view.findViewById(R.id.txt_phone_2);
        txtContactPhone2 = (EditText) view.findViewById(R.id.txt_contact_phone_2);
        spPhone1 = (Spinner) view.findViewById(R.id.tv_phone_1);
        spPhone2 = (Spinner) view.findViewById(R.id.tv_phone_2);
        spCusType = (Spinner) view.findViewById(R.id.sp_type);
        btnCaptureIdentityCard = (Button) view.findViewById(R.id.btn_capture_identity_card);
        btOpenLocalIdentityCard = (Button) view.findViewById(R.id.btn_open_local_identity_card);
        spCusDetail = (Spinner) view.findViewById(R.id.tv_cus_detail);
        spDistricts = (Spinner) view.findViewById(R.id.tv_districts);
        spWards = (Spinner) view.findViewById(R.id.tv_wards);
        spStreets = (Spinner) view.findViewById(R.id.tv_streets);
        spHouseTypes = (Spinner) view.findViewById(R.id.tv_house_types);
        spHousePosition = (Spinner) view.findViewById(R.id.tv_house_position);
        imgUpdateCacheDistrict = (ImageView) view.findViewById(R.id.img_update_cache_district);
        imgUpdateCacheWard = (ImageView) view.findViewById(R.id.img_update_cache_ward);
        btnShowHouseNumFormat = (Button) view.findViewById(R.id.btn_show_house_num_format);
        btnUploadImageDocument = (Button) view.findViewById(R.id.btn_upload_image_document);
        txtContactPhone1 = (EditText) view.findViewById(R.id.txt_contact_phone_1);
        btnDetailImageDocument = (Button) view.findViewById(R.id.btn_detail_image_document);
        //version 3.23
        spSourceType = view.findViewById(R.id.sp_source_type);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initEvent() {
        btnCaptureIdentityCard.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                            || getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
                    } else {
                        fileIdentityCardImageDocument = Common.capture(getActivity());
                    }
                } else {
                    fileIdentityCardImageDocument = Common.capture(getActivity());
                }
            }
        });
        txtPhone2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtPhone2.setError(null);
            }
        });
        txtContactPhone2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtContactPhone2.setError(null);
            }
        });
        btOpenLocalIdentityCard.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // mở file ảnh từ thiết bị
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                    } else {
                        openGallery();
                    }
                } else {
                    openGallery();
                }
            }
        });
        imgUpdateCacheDistrict.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getDistrict();
            }
        });
        imgUpdateCacheWard.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String districtID = (spDistricts.getAdapter() != null && spDistricts
                        .getAdapter().getCount() > 0) ? ((KeyValuePairModel) spDistricts
                        .getSelectedItem()).getsID() : "";
                getWard(districtID);
            }
        });
        txtCustomerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtCustomerName.setError(null);
            }
        });

        btnDetailImageDocument.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DetailRegistrationDocumentActivity.class);
                intent.putExtra(TAG_UPDATE_REGISTRATION, statusUpdateRegistration);
                intent.putParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT_SELECTED, listImageDocument);
                getActivity().startActivityForResult(intent, 2);
            }
        });

        txtIdentityCard.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtIdentityCard.setError(null);
            }
        });
        txtBirthDay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    mDateDialog = initDatePickerDialog(txtBirthDay, getString(R.string.title_dialog_birth_day));
                    if (getActivity() != null) {
                        mDateDialog.setCancelable(false);
                        mDateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
                    }
                    txtBirthDay.setError(null);
                    return true;
                }
                return false;
            }
        });
        txtTaxNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtTaxNum.setError(null);
            }
        });

        //upload ảnh hồ sơ khách hàng lên server
        btnUploadImageDocument.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UploadRegistrationDocumentActivity.class);
                intent.putExtra(TAG_IMAGE_DOCUMENT_IDENTITY_CARD_SELECTED, pathImageIdentityCard);
                intent.putExtra(TAG_UPDATE_REGISTRATION, statusUpdateRegistration);
                intent.putExtra(TAG_REGISTRATION_CREATE_FORM, true);
                if (modelDetail != null) {
                    intent.putExtra(TAG_REG_ID, modelDetail.getID());
                }
                // biến định danh upload hồ sơ (3)
                intent.putExtra(TAG_UPLOAD_IMAGE_TYPE, 3);
                intent.putParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT_SELECTED, listImageDocument);
                getActivity().startActivityForResult(intent, 2);
            }
        });

        txtPhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtPhone1.setError(null);
            }
        });
        txtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtEmail.setError(null);
            }
        });
        txtHouseNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtHouseNum.setError(null);
            }
        });
        txtContactPhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtContactPhone1.setError(null);
            }
        });
        btnShowHouseNumFormat.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                HouseNumFormatRuleDialog dialog = new HouseNumFormatRuleDialog();
                Common.showFragmentDialog(getFragmentManager(), dialog, "fragment_house_num_format_dialog");
            }
        });
        spPhone1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                int sPhone1Value = selectedItem.getID();
                if (sPhone1Value > 0) {
                    String phone1Temp = "", contactPhone1Temp = "";
                    try {
                        if (modelDetail != null) {
                            phone1Temp = modelDetail.getPhone_1();
                            contactPhone1Temp = modelDetail.getContact_1();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    txtPhone1.setEnabled(true);
                    txtContactPhone1.setEnabled(true);
                    txtPhone1.setText(phone1Temp);
                    txtContactPhone1.setText(contactPhone1Temp);
                } else {
                    txtPhone1.setEnabled(false);
                    txtPhone1.setText("");
                    txtContactPhone1.setEnabled(false);
                    txtContactPhone1.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spPhone2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                int itemID = selectedItem.getID();
                if (itemID > 0) {
                    String phone2Temp = "", contactPhone2Temp = "";
                    try {
                        if (modelDetail != null) {
                            phone2Temp = modelDetail.getPhone_2();
                            contactPhone2Temp = modelDetail.getContact_2();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    txtPhone2.setEnabled(true);
                    txtContactPhone2.setEnabled(true);
                    txtPhone2.setText(phone2Temp);
                    txtContactPhone2.setText(contactPhone2Temp);

                } else {
                    txtPhone2.setEnabled(false);
                    txtPhone2.setText("");
                    txtContactPhone2.setEnabled(false);
                    txtContactPhone2.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spDistricts.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                String districtSelected = selectedItem.getsID();
                spWards.setAdapter(null);
                spStreets.setAdapter(null);
                spApartment.setAdapter(null);
                txtLot.setText("");
                txtFloor.setText("");
                txtRoom.setText("");
                if (!selectedItem.getsID().equals("-1")) {
                    if (modelDetail != null) {
                        modelDetail.setBillTo_District(districtSelected);
                    }
                    loadWard();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spWards.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                spStreets.setAdapter(null);
                spApartment.setAdapter(null);
                txtLot.setText("");
                txtFloor.setText("");
                txtRoom.setText("");
                if (modelDetail != null) {
                    modelDetail.setBillTo_Ward(selectedItem.getsID());
                }
                if (isFirstLoading) {
                    initControlElse();
                    isFirstLoading = false;
                }
                setStreetsOrCondos();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spHouseTypes.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (modelDetail != null) {
                    modelDetail.setCurrentHouse(String.valueOf(selectedItem.getID()));
                }
                switch (selectedItem.getID()) {
                    case 1:// chọn nhà phố
                        //vùng chứa control Số chung cư
                        apartmentPanel.setVisibility(View.GONE);
                        //vùng chứa control Vị trí nhà
                        housePositionPanel.setVisibility(View.GONE);
                        //vùng chứa control Số nhà
                        houseNumberPanel.setVisibility(View.VISIBLE);
                        // Tầng, Lô, Phòng
                        txtFloor.setText("");
                        txtLot.setText("");
                        txtRoom.setText("");
                        if (modelDetail != null) {
                            modelDetail.setNameVilla("");
                        }
                        break;
                    case 2:// cu xa, chung cu, cho, villa, thuong xa
                        apartmentPanel.setVisibility(View.VISIBLE);
                        housePositionPanel.setVisibility(View.GONE);
                        houseNumberPanel.setVisibility(View.GONE);
                        txtHouseNum.setText("");
                        txtHouseNum.setText("");
                        if (modelDetail != null) {
                            modelDetail.setBillTo_Number("");
                        }
                        break;
                    case 3://Nhà không địa chỉ
                        apartmentPanel.setVisibility(View.GONE);
                        housePositionPanel.setVisibility(View.VISIBLE);
                        houseNumberPanel.setVisibility(View.VISIBLE);
                        txtFloor.setText("");
                        txtLot.setText("");
                        txtRoom.setText("");
                        if (modelDetail != null) {
                            modelDetail.setNameVilla("");
                        }
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private DatePickerReportDialog initDatePickerDialog(EditText txtValue, String dialogTitle) {
        Calendar minDate = Calendar.getInstance(), starDate = Calendar.getInstance(), maxDate = Calendar.getInstance();
        starDate.set(Calendar.DAY_OF_MONTH, 1);
        starDate.set(Calendar.MONTH, 0);
        starDate.set(Calendar.YEAR, 1990);
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        minDate.set(Calendar.MONTH, 0);
        minDate.set(Calendar.YEAR, 1900);
        return new DatePickerReportDialog(this, starDate, minDate, maxDate,
                dialogTitle, txtValue, true);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar date = Calendar.getInstance();
        date.set(year, monthOfYear, dayOfMonth);
        if (mDateDialog.getEditText() == txtBirthDay && txtBirthDay != null) {
            myCalendar = date;
            txtBirthDay.setText(Common.getSimpleDateFormat(myCalendar, Constants.DATE_FORMAT_VN));
        }
    }

    // mở file ảnh từ thiết bị
    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        getActivity().startActivityForResult(Intent.createChooser(intent, "Select Picture"), 4);
    }

    // thiết lập đường dẫn ảnh cmnd và lưu ảnh cmnd mới vào danh sách ảnh cmnd
    public void setPathImageDocument(String imagePath) {
        this.pathImageIdentityCard = imagePath;
        listImageDocumentIdentityCard.clear();
        listImageDocumentIdentityCard.add(new ImageDocument(pathImageIdentityCard, true, 2));
    }

    private void initSpinners() {
        setPhone();
        initSourceType();
        initCusType();
        initCusDetail();
        loadDistrict();
        setHouseTypes();
        setHousePositions();
    }

    public void setStreetsOrCondos() {
        String sDistrictSelected, sWardSelected;
        try {
            sDistrictSelected = (spDistricts.getAdapter() != null ? ((KeyValuePairModel) spDistricts.getSelectedItem()).getsID() : "");
            sWardSelected = (spWards.getAdapter() != null ? ((KeyValuePairModel) spWards.getSelectedItem()).getsID() : "");
            if (modelDetail != null) {
                modelDetail.setBillTo_District(sDistrictSelected);
                modelDetail.setBillTo_Ward(sWardSelected);
                new GetStreetOrCondo(getActivity(), sDistrictSelected,
                        sWardSelected, GetStreetOrCondo.street,
                        spStreets, modelDetail.getBillTo_Street());
                new GetStreetOrCondo(getActivity(), sDistrictSelected, sWardSelected, GetStreetOrCondo.condo,
                        spApartment, modelDetail.getNameVillaVN() == null ? modelDetail.getNameVilla() : modelDetail.getNameVillaVN());
            } else {
                new GetStreetOrCondo(getActivity(), sDistrictSelected,
                        sWardSelected, GetStreetOrCondo.street,
                        spStreets, null);
                new GetStreetOrCondo(getActivity(), sDistrictSelected,
                        sWardSelected, GetStreetOrCondo.condo,
                        spApartment, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadWard() {
        String districtID = ((KeyValuePairModel) spDistricts.getSelectedItem()).getsID();
        if (Common.hasPreference(getActivity(), Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID)) {
            loadSpinnerWard(districtID);
        } else {
            getWard(districtID);
        }
    }

    //kết nối Api lấy danh sách Phường(Xã)
    private void getWard(String districtID) {
        String wardID = null;
        if (modelDetail != null) {
            wardID = modelDetail.getBillTo_Ward();
        }
        new GetWardList(getActivity(), districtID, spWards, wardID);
    }

    //cập nhật danh sách Phường(Xã)
    public void loadSpinnerWard(String districtID) {
        ArrayList<KeyValuePairModel> lst = new ArrayList<>();
        if (Common.hasPreference(getActivity(),
                Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID)) {
            final String TAG_GET_WARDS_RESULT = "GetWardListMethodPostResult";
            final String TAG_NAME = "Name";
            final String TAG_NAME_VN = "NameVN";
            final String TAG_ERROR = "ErrorService";

            String jsonStr = Common.loadPreference(getActivity(),
                    Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID);
            try {
                JSONObject json = new JSONObject(jsonStr);
                JSONArray jsArr = json.getJSONArray(TAG_GET_WARDS_RESULT);
                for (int i = 0; jsArr != null && i < jsArr.length(); i++) {
                    String code = "", error = "", desc = "";
                    JSONObject item = jsArr.getJSONObject(i);

                    if (item.has(TAG_ERROR))
                        error = item.getString(TAG_ERROR);
                    if (item.isNull(TAG_ERROR) || error.equals("null")) {
                        if (item.has(TAG_NAME)) {
                            code = item.getString(TAG_NAME);
                        }
                        if (item.has(TAG_NAME_VN)) {
                            desc = item.getString(TAG_NAME_VN);
                        }
                        lst.add(new KeyValuePairModel(code, desc));
                    } else {
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            @SuppressLint("RtlHardcoded")
            KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(
                    getActivity(), R.layout.my_spinner_style, lst, Gravity.RIGHT);
            spWards.setAdapter(adapterStatus);

            String id = (modelDetail != null) ? modelDetail.getBillTo_Ward() : "";
            int index = Common.getIndex(spWards, id);
            if (index > 0)
                spWards.setSelection(index, true);
        }
    }

    private void loadDistrict() {
        if (Common.hasPreference(getActivity(),
                Constants.SHARE_PRE_CACHE_REGISTER,
                Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT)) {
            loadSpinnerDistrict();
        } else {
            getDistrict();
        }
        Common.smoothScrollView(scrollMain, 0);
    }

    private void getDistrict() {
        String id = null;
        if (modelDetail != null) {
            id = modelDetail.getBillTo_District();
        }
        //kết nối api lấy danh sách Quận(Huyện)
        new GetDistrictList(getActivity(), Constants.LOCATION_ID, spDistricts, id);
    }

    public void loadSpinnerDistrict() {
        ArrayList<KeyValuePairModel> lst = new ArrayList<>();
        lst.add(new KeyValuePairModel("-1", "[ Chọn Quận/Huyện ]"));

        if (Common.hasPreference(getActivity(),
                Constants.SHARE_PRE_CACHE_REGISTER,
                Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT)) {
            final String TAG_GET_DISTRICTS_RESULT = "GetDistrictListMethodPostResult";
            final String TAG_FULL_NAME = "FullNameVN";
            final String TAG_NAME = "Name";
            final String TAG_ERROR = "ErrorService";
            String jsonStr = Common.loadPreference(getActivity(),
                    Constants.SHARE_PRE_CACHE_REGISTER,
                    Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT);
            try {
                JSONObject json = new JSONObject(jsonStr);
                JSONArray jsArr;
                jsArr = json.getJSONArray(TAG_GET_DISTRICTS_RESULT);
                for (int i = 0; jsArr != null && i < jsArr.length(); i++) {
                    String code = "", desc = "", error = "";
                    JSONObject item = jsArr.getJSONObject(i);

                    if (item.has(TAG_ERROR))
                        error = item.getString(TAG_ERROR);
                    if (item.isNull(TAG_ERROR) || error.equals("null")) {
                        if (item.has(TAG_NAME))
                            code = item.getString(TAG_NAME);
                        if (item.has(TAG_FULL_NAME))
                            desc = item.getString(TAG_FULL_NAME);
                        lst.add(new KeyValuePairModel(code, desc));
                    } else {
                        Common.alertDialog("Lỗi WS:" + error, getActivity());
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            @SuppressLint("RtlHardcoded")
            KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(
                    getActivity(), R.layout.my_spinner_style, lst, Gravity.RIGHT);
            spDistricts.setAdapter(adapterStatus);
            String districtId = (modelDetail != null) ? modelDetail.getBillTo_District() : "";
            int index = Common.getIndex(spDistricts, districtId);
            if (index > 0)
                spDistricts.setSelection(index, true);
        }
    }

    //version 3.23 nhannh26
    private void initSourceType(){
        ArrayList<KeyValuePairModel> lstSourceType = new ArrayList<>();
        lstSourceType.add(new KeyValuePairModel(0, "[Chọn nguồn bán]"));
        lstSourceType.add(new KeyValuePairModel(1, "Nguồn D2D"));
        lstSourceType.add(new KeyValuePairModel(2, "Nguồn Online"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(),
                R.layout.my_spinner_style,
                lstSourceType,
                Gravity.RIGHT);
        spSourceType.setAdapter(adapter);
        if (modelDetail != null) {
            spSourceType.setSelection(Common.getIndex(spSourceType, modelDetail.getSourceType()), true);
        }
    }

    /*
     * Init Loại KH: Cong ty, HS-SV
     */
    private void initCusDetail() {
        ArrayList<KeyValuePairModel> lstObjType = new ArrayList<>();
        lstObjType.add(new KeyValuePairModel(12, "Hộ gia đình"));
        lstObjType.add(new KeyValuePairModel(10, "Công ty"));
        lstObjType.add(new KeyValuePairModel(11, "Học sinh, sinh viên"));
        lstObjType.add(new KeyValuePairModel(13, "Hộ kinh doanh"));
        lstObjType.add(new KeyValuePairModel(14, "Khách hàng nước ngoài"));
        lstObjType.add(new KeyValuePairModel(15, "KXD"));
        lstObjType.add(new KeyValuePairModel(16, "Ký túc xá"));
        lstObjType.add(new KeyValuePairModel(17, "Đại lý Internet"));
        @SuppressLint("RtlHardcoded")
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(),
                R.layout.my_spinner_style, lstObjType, Gravity.RIGHT);
        spCusDetail.setAdapter(adapter);
        if (modelDetail != null) {
            // Tuan kiem tra null, "" 22032017
            if (modelDetail.getCusTypeDetail() != null && !modelDetail.getCusTypeDetail().equals("")) {
                try {
                    spCusDetail.setSelection(Common.getIndex(spCusDetail, Integer.parseInt(modelDetail.getCusTypeDetail())), true);
                } catch (Exception e) {
                    e.getStackTrace();
                }
            }
        }
    }

    //Loai hinh KH
    private void initCusType() {
        ArrayList<KeyValuePairModel> lst = new ArrayList<>();
        lst.add(new KeyValuePairModel(1, "Cá nhân Việt Nam"));
        lst.add(new KeyValuePairModel(2, "Cá nhân nước ngoài"));
        lst.add(new KeyValuePairModel(3, "Cơ quan Việt Nam"));
        lst.add(new KeyValuePairModel(4, "Cơ quan nước ngoài"));
        lst.add(new KeyValuePairModel(5, "Công ty tư nhân"));
        lst.add(new KeyValuePairModel(6, "Nhân viên FPT"));
        lst.add(new KeyValuePairModel(7, "Nhà nước"));
        lst.add(new KeyValuePairModel(8, "Nước ngoài"));
        lst.add(new KeyValuePairModel(9, "Công ty"));
        lst.add(new KeyValuePairModel(10, "Giáo dục"));
        lst.add(new KeyValuePairModel(11, "Nhân viên FTEL"));
        lst.add(new KeyValuePairModel(12, "Nhân viên tập đoàn FPT"));
        lst.add(new KeyValuePairModel(13, "Nhân viên TIN/PNC"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(), R.layout.my_spinner_style, lst, Gravity.RIGHT);
        spCusType.setAdapter(adapter);
        if (modelDetail != null) {
            if(Common.getIndex(spCusType, modelDetail.getCusType()) != -1){
                activity.setIsCusType(modelDetail.getCusType());
                spCusType.setSelection(Common.getIndex(spCusType, modelDetail.getCusType()), true);
            }
        }
    }



    //Vị trí nhà
    public void setHousePositions() {
        ArrayList<KeyValuePairModel> lstHousePositions = new ArrayList<>();
        lstHousePositions.add(new KeyValuePairModel(0, "[ Vui lòng chọn vị trí ]"));
        lstHousePositions.add(new KeyValuePairModel(1, "Kế bên phải"));
        lstHousePositions.add(new KeyValuePairModel(2, "Kế bên trái"));
        lstHousePositions.add(new KeyValuePairModel(3, "Đối diện"));
        lstHousePositions.add(new KeyValuePairModel(4, "Phía sau"));
        lstHousePositions.add(new KeyValuePairModel(5, "Cách"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(),
                R.layout.my_spinner_style, lstHousePositions, Gravity.RIGHT);
        spHousePosition.setAdapter(adapter);
        if (modelDetail != null) {
            spHousePosition.setSelection(Common.getIndex(spHousePosition, modelDetail.getPosition()), true);
        }
    }

    //Loại nhà
    public void setHouseTypes() {
        ArrayList<KeyValuePairModel> lstHouseTypes = new ArrayList<>();
        lstHouseTypes.add(new KeyValuePairModel(1, "Nhà phố"));
        lstHouseTypes.add(new KeyValuePairModel(2, "C.Cư, C.Xá, Villa, Chợ, Thương Xá"));
        lstHouseTypes.add(new KeyValuePairModel(3, "Nhà không địa chỉ"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(), R.layout.my_spinner_style, lstHouseTypes, Gravity.RIGHT);
        spHouseTypes.setAdapter(adapter);
        if (modelDetail != null) {
            spHouseTypes.setSelection(Common.getIndex(spHouseTypes, modelDetail.getTypeHouse()), true);
        }
    }

    // Khởi tạo danh sách ban đầu Loại điện thoại 1 và 2
    public void setPhone() {
        ArrayList<KeyValuePairModel> lstPhone = new ArrayList<>();
        lstPhone.add(new KeyValuePairModel(0, "[ Chọn loại điện thoại ]"));
        lstPhone.add(new KeyValuePairModel(1, "Cơ quan"));
        lstPhone.add(new KeyValuePairModel(2, "Fax"));
        lstPhone.add(new KeyValuePairModel(3, "Nhà riêng"));
        lstPhone.add(new KeyValuePairModel(4, "Di động"));
        lstPhone.add(new KeyValuePairModel(5, "Nhắn tin"));
//        @SuppressLint("RtlHardcoded")
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(), R.layout.my_spinner_style, lstPhone, Gravity.RIGHT);
        spPhone1.setAdapter(adapter);
        spPhone2.setAdapter(adapter);
        if (modelDetail != null) {
            spPhone1.setSelection(Common.getIndex(spPhone1, modelDetail.getType_1()), true);
            spPhone2.setSelection(Common.getIndex(spPhone2, modelDetail.getType_2()), true);
        }
        else {
            spPhone1.setSelection(4, true);
            spPhone2.setSelection(4, true);
        }
    }

    private void initControlElse() {
        if (modelDetail != null) {
            txtCustomerName.setText(modelDetail.getFullName());
            txtIdentityCard.setText(modelDetail.getPassport());
            txtBirthDay.setText(modelDetail.getBirthDay() == null ? "" :
                    Common.getSimpleDateFormat(modelDetail.getBirthDay(), Constants.DATE_FORMAT_VN));
            txtAddressId.setText(modelDetail.getAddressPassport());
            txtTaxNum.setText(modelDetail.getTaxId());
            txtEmail.setText(modelDetail.getEmail());
            txtLot.setText(modelDetail.getLot());
            txtFloor.setText(modelDetail.getFloor());
            txtRoom.setText(modelDetail.getRoom());
            txtHouseNum.setText(modelDetail.getBillTo_Number());
            txtPhone1.setText(modelDetail.getPhone_1());
            txtPhone2.setText(modelDetail.getPhone_2());
            txtContactPhone1.setText(modelDetail.getContact_1());
            txtContactPhone2.setText(modelDetail.getContact_2());
            txtHouseDesc.setText(modelDetail.getNote());
            txtDescriptionIBB.setText(modelDetail.getDescriptionIBB());
            setListImageDocument(Common.covertStringToListImageDocument(modelDetail.getImageInfo()));
        }
    }

    // điền thông tin chứng minh nhân dân tự động
    public void autoFieldDataIdentityCard(IdentityCard identityCard) {
        if (identityCard != null) {
            txtCustomerName.setText(identityCard.getName() == null ? "" : identityCard.getName());
            txtIdentityCard.setText(identityCard.getId() == null ? "" : identityCard.getId());
            txtBirthDay.setText(identityCard.getDob() == null ? "" : formatDateOfBirthRegister(identityCard.getDob()));
            txtAddressId.setText(identityCard.getAddress() == null ? "" : identityCard.getAddress());
        } else {
            Common.alertDialogNotTitle(getResources().getString(R.string.title_empty_data), getActivity());
        }
    }

    // cập nhật danh sách hồ sơ khách hàng
    public void setListImageDocument(ArrayList<ImageDocument> listImageDocumentInput) {
        statusUpdateRegistration = listImageDocumentInput.size() > 0;
        this.listImageDocument = listImageDocumentInput;
        this.strListImageDocumentInfo = Common.covertListImageDocumentToString(listImageDocument, ",");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = getActivity().getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(getActivity(), message);
                return;
            }
        }
        switch (requestCode) {
            case 2:
                fileIdentityCardImageDocument = Common.capture(getActivity());
                break;
            case 3:
                openGallery();
                break;
        }
    }
}
