package isc.fpt.fsale.services;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import isc.fpt.fsale.activity.LoginActivity;
import isc.fpt.fsale.model.GCMUserModel;
import isc.fpt.fsale.model.HeaderRequestModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import isc.fpt.fsale.utils.Util;

import static isc.fpt.fsale.utils.Constants.AUTHORIZATION;

/*
 * CLASS: Services
 * @description: define service variables and functions
 * @author: vandn
 * @created on: 12/08/2013
 */

public class Services {
//    public static final String SERVICE_URL = Constants.WS_URL_STAGING;
//    private static final String SERVICE_URL_UPLOAD_MULTIPART = Constants.WS_UPLOAD_IMAGE_STAGING;
    public static final String SERVICE_URL = Constants.WS_URL_PRODUCTION;
    private static final String SERVICE_URL_UPLOAD_MULTIPART = Constants.WS_UPLOAD_IMAGE_PRODUCTION;
    private static final String SERVICE_URL_UPLOAD = "http://beta.wsmobisale.fpt.net/MobiSaleService.svc/";
    // API SCAN CMND
    private static final String SERVICE_URL_SCAN_IDENTITY_CARD = "https://api.fpt.ai/vision/idr/";
    private final static int timeoutConnection = 40000;
    private final static int timeoutConnectionUpload = 120000;
    // Set the default socket timeout (SO_TIMEOUT)
    private final static int timeoutSocket = 120000;
    private final static int timeoutSocketUpload = 120000;
    static final String POST = "POST";
    public static final String GET = "GET";
    public static final String JSON_POST = "JSON_POST";
    public static final String JSON_POST_UPLOAD = "JSON_POST_UPLOAD";
    public static final String JSON_POST_UPLOAD_MULTIPART = "JSON_POST_UPLOAD_MULTIPART";
    public static final String JSON_POST_OBJECT = "JSON_POST_OBJECT";
    private static final String TAG_REQUEST_HEADER_COOKIE = "CookieFTEL";
    private static final String TAG_REQUEST_HEADER_USER_NAME = "UserName";
    private static final String TAG_REQUEST_HEADER_DEVICE_IMEI = "DeviceIMEI";
    private static final String TAG_REQUEST_HEADER_TOKEN = "Token";
    private static final String TAG_RESULT_HEADER_NAME = "FTEL_MOBISALE_HEADER";
    private static final String TAG_REQUEST_HEADER_SIGNATURE = "checksum";

    /**
     * return list request params with string format
     */
    public static String getParams(String[] p) {
        StringBuilder params = new StringBuilder();
        for (String aP : p) {
            params.append("/").append(aP);
        }
        return params.toString();
    }

    public static String get(String methodName, String params, Context context) {
        String result = null;
        // Add by: DuHK
        HeaderRequestModel headerRequest = new HeaderRequestModel();
        if (context != null
                && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest()
                    .getListObject().size() > 0)
                headerRequest = ((MyApp) context.getApplicationContext())
                        .getHeaderRequest().getListObject().get(0);
        try {
            String url = SERVICE_URL + methodName + params;
            url = url.replaceAll(" ", "%20");
            HttpGet request = new HttpGet(url);
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            // Add by: DuHK
            request.setHeader(TAG_REQUEST_HEADER_COOKIE,
                    headerRequest.getSessionID());
            request.setHeader(TAG_REQUEST_HEADER_TOKEN,
                    headerRequest.getToken());
            request.setHeader(TAG_REQUEST_HEADER_USER_NAME,
                    headerRequest.getUserName());
            request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI,
                    headerRequest.getDeviceIMEI());
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(request);
            HttpEntity responseEntity = response.getEntity();
            InputStream stream = responseEntity.getContent();
            Header[] headers = response.getAllHeaders();
            for (Header header : headers) {
                if (header.getName().equals(TAG_RESULT_HEADER_NAME)) {
                    JSONObject jsHeader = new JSONObject(header.getValue());
                    try {
                        WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<>(
                                jsHeader, HeaderRequestModel.class);
                        checkHeader(headerObj, context);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
            return getStringFromInputStream(stream);
        } catch (Exception ex) {
            ex.printStackTrace();
            return result;
        }
    }

    public static String post(String methodName, String params) {
        String result = null;
        try {
            String url = SERVICE_URL + methodName + params;
            url = url.replaceAll(" ", "%20");
            // Send GET request to <service>
            HttpPost request = new HttpPost(url);
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(request);
            HttpEntity responseEntity = response.getEntity();
            InputStream stream = responseEntity.getContent();
            result = getStringFromInputStream(stream);
        } catch (Exception ex) {
            ex.printStackTrace();
            return result;
        }
        return result;
    }

    /**
     * Convert String from input stream
     */
    private static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    public static String postJson(String methodName, String[] params, String[] paramsValue, Context context) throws Exception {
        // Add by: DuHK
        HeaderRequestModel headerRequest = new HeaderRequestModel();
        if (context != null && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest().getListObject().size() > 0)
                headerRequest = ((MyApp) context.getApplicationContext()).getHeaderRequest().getListObject().get(0);

        HttpPost request = new HttpPost(SERVICE_URL + methodName);

        JSONStringer json = new JSONStringer().object();
        for (int i = 0; i < params.length; i++) {
            if (paramsValue[i] == null)
                paramsValue[i] = "";
            json.key(params[i]).value(paramsValue[i].trim());
        }
        json.endObject();
        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
        request.setEntity(entity);
        // ======================================================================
        // Add by: DuHK
        request.setHeader(TAG_REQUEST_HEADER_COOKIE, headerRequest.getSessionID());
        request.setHeader(TAG_REQUEST_HEADER_TOKEN, headerRequest.getToken());
        request.setHeader(TAG_REQUEST_HEADER_USER_NAME, headerRequest.getUserName());
        request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI, headerRequest.getDeviceIMEI());
        // ======================================================================
        // new code
        String tempStr = json.toString().replace("\\", "");
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        if (methodName.equals(Constants.autoBookPort) && Constants.AutoBookPort_Timeout > 0) {
            HttpConnectionParams.setSoTimeout(httpParameters, Constants.AutoBookPort_Timeout);
        } else {
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        }
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME)) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<>(
                            jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        return getStringFromInputStream(stream);
    }

    //upload single file
    public static String postJsonUpload(String methodName, String[] params, Object[] paramsValue, Context context) throws Exception {
        if (context != null && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest().getListObject().size() > 0)
                ;
        HttpPost request = new HttpPost(SERVICE_URL_UPLOAD + methodName);
        JSONStringer json = new JSONStringer().object();
        for (int i = 0; i < params.length; i++) {
            json.key(params[i]).value(paramsValue[i]);
        }
        json.endObject();
        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
        request.setEntity(entity);
        String tempStr = json.toString().replace("\\", "");
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnectionUpload);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocketUpload);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        Header[] headers = response.getAllHeaders();

        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME)) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<>(jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        return getStringFromInputStream(stream);
    }

    //scan cmnd
    public static String getInfoIdentityCard(String methodName, Bitmap bitmap) throws Exception {
        HttpPost request = new HttpPost(SERVICE_URL_SCAN_IDENTITY_CARD + methodName);
        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create()
                .setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] imageBytes = byteArrayOutputStream.toByteArray();
        ByteArrayBody bab = new ByteArrayBody(imageBytes, "image.png");
        multipartEntityBuilder.addPart("image", bab);
        HttpEntity httpEntity = multipartEntityBuilder.build();
        request.setHeader("api_key", Constants.KEY_SCAN_IDENTITY_CARD);
        request.setEntity(httpEntity);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnectionUpload);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocketUpload);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        return getStringFromInputStream(stream);
    }

    //upload more file
    static String postFileMultipart(String methodName, Bitmap bitmap, String[] params, String[] paramsValue, Context context) throws Exception {
        HttpPost request = new HttpPost(SERVICE_URL_UPLOAD_MULTIPART + methodName);
        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create()
                .setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] imageBytes = byteArrayOutputStream.toByteArray();
        ByteArrayBody bab = new ByteArrayBody(imageBytes, "image.png");
        multipartEntityBuilder.addPart("images", bab);
        for (int index = 0; index < params.length; index++) {
            multipartEntityBuilder.addTextBody(params[index], paramsValue[index]);
        }
        HttpEntity httpEntity = multipartEntityBuilder.build();
        request.setHeader("Authorization", AUTHORIZATION);
        request.setEntity(httpEntity);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnectionUpload);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocketUpload);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME)) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<>(jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        return getStringFromInputStream(stream);
    }

    // get file Bitmap
    public static Bitmap getBitmapImageDocument(String methodName, String[] paramNames, String[] paramsValue) {
        Bitmap bmp = null;
        try {
            String inputRequest = SERVICE_URL_UPLOAD_MULTIPART + methodName + "?";
            for (int index = 0; index < paramNames.length; index++) {
                inputRequest = inputRequest.concat(paramNames[index]).concat("=").concat(paramsValue[index]);
                if (index < paramNames.length - 1) {
                    inputRequest = inputRequest.concat("&");
                }
            }
            HttpGet request = new HttpGet(inputRequest);
            request.setHeader("Authorization", AUTHORIZATION);
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 120000);
            HttpConnectionParams.setSoTimeout(httpParameters, 120000);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(request);
            HttpEntity responseEntity = response.getEntity();
            InputStream stream = responseEntity.getContent();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(stream);
            bmp = BitmapFactory.decodeStream(bufferedInputStream);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return bmp;
    }

    /*
     * TODO: POST Json Object
     *
     * @param methodName
     * @param jsonParams
     * @param context
     * @return
     * @throws Exception
     * @author ISC_DuHK
     */
    public static String postJsonObject(String methodName, JSONObject jsonParams, Context context) throws Exception {
        HeaderRequestModel headerRequest = new HeaderRequestModel();
        if (context != null && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest().getListObject().size() > 0)
                headerRequest = ((MyApp) context.getApplicationContext()).getHeaderRequest().getListObject().get(0);

        HttpPost request = new HttpPost(SERVICE_URL + methodName);
        StringEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
        request.setEntity(entity);
        // ======================================================================
        // Add by: DuHK
        request.setHeader(TAG_REQUEST_HEADER_COOKIE, headerRequest.getSessionID());
        request.setHeader(TAG_REQUEST_HEADER_TOKEN, headerRequest.getToken());
        request.setHeader(TAG_REQUEST_HEADER_USER_NAME, headerRequest.getUserName());
        request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI, headerRequest.getDeviceIMEI());
        // ======================================================================
        // new code
        String tempStr = jsonParams.toString().replace("\\", "");
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME)) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<>(jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        return getStringFromInputStream(stream);
    }

    public static String postJsonClientService(String methodName, String[] params, String[] paramsValue) throws Exception {
        // Add by: DuHK
        HttpPost request = new HttpPost(SERVICE_URL + methodName);
        JSONStringer json = new JSONStringer().object();
        for (int i = 0; i < params.length; i++) {
            if (paramsValue[i] == null)
                paramsValue[i] = "";
            json.key(params[i]).value(paramsValue[i].trim());
        }
        json.endObject();
        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
        request.setEntity(entity);
        // new code
        String tempStr = json.toString().replace("\\", "");
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        return getStringFromInputStream(stream);
    }

    public static String sendGCMMessage(List<GCMUserModel> sendTo, JSONObject dataContent) throws Exception {
        // Add by: DuHK
        HttpPost request = new HttpPost("https://gcm-http.googleapis.com/gcm/send");
        JSONObject data = new JSONObject();
        data.put("to", (sendTo != null && sendTo.size() > 0) ? sendTo.get(0).getRegID() : "");
        data.put("data", dataContent);
        data.put("priority", "high");
        data.put("content_available", true);
        JSONObject notification = new JSONObject();

        if (dataContent.has("message")) {
            notification.put("body", dataContent.getString("message"));
        }
        if (dataContent.has("title") && !dataContent.getString("title").equals("")) {
            notification.put("title", dataContent.getString("to"));
        } else if (dataContent.has("to")) {
            notification.put("title", dataContent.getString("to"));
        }
        notification.put("sound", "default");
        data.put("notification", notification);
        StringEntity entity = new StringEntity(data.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;"));
        request.setEntity(entity);
        // Add by: DuHK
        request.setHeader("Authorization", Constants.GCM_SENDER_APP_ID);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        return getStringFromInputStream(stream);
    }

    /*
     * TODO: Convert header Reponse to Object
     * Add by: DuHK
     */
    public static String sendMessage(List<GCMUserModel> sendTo, JSONObject message) throws Exception {
        String methodName = "GCM_SendNotification";
        HttpPost request = new HttpPost(SERVICE_URL + methodName);
        JSONArray arr = new JSONArray();
        for (GCMUserModel gcmUserModel : sendTo) {
            arr.put(gcmUserModel.toJSonObject());
        }
        JSONObject data = new JSONObject();
        data.put("Message", message);
        data.put("SendToList", arr);
        StringEntity entity = new StringEntity(data.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
        request.setEntity(entity);
        String tempStr = data.toString().replace("\\", "");
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        return getStringFromInputStream(stream);
    }

    public static String postJsonAutoHeader(String methodName, String[] params, String[] paramsValue, Context context) throws Exception {
        HeaderRequestModel headerRequest = new HeaderRequestModel();
        if (context != null && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest().getListObject().size() > 0)
                headerRequest = ((MyApp) context.getApplicationContext()).getHeaderRequest().getListObject().get(0);

        HttpPost request = new HttpPost(SERVICE_URL + methodName);
        JSONStringer json = new JSONStringer().object();
        for (int i = 0; i < params.length; i++) {
            if (paramsValue[i] == null)
                paramsValue[i] = "";
            json.key(params[i]).value(paramsValue[i].trim());
        }
        json.endObject();

        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
        request.setEntity(entity);
        request.setHeader(TAG_REQUEST_HEADER_COOKIE, headerRequest.getSessionID());
        request.setHeader(TAG_REQUEST_HEADER_TOKEN, headerRequest.getToken());
        request.setHeader(TAG_REQUEST_HEADER_USER_NAME, headerRequest.getUserName());
        request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI, headerRequest.getDeviceIMEI());
        String tempStr = json.toString().replace("\\", "");
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        Header[] headers = response.getAllHeaders();
        InputStream stream = responseEntity.getContent();
        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME)) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<>(
                            jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        return getStringFromInputStream(stream);
    }

    private static void checkHeader(WSObjectsModel<HeaderRequestModel> header, Context context) {
        ((MyApp) context.getApplicationContext()).setHeaderRequest(header);
        if (header != null) {
            if (header.getErrorCode() != 0) {
                // Log out và show message nếu hết session hoặc token không khớp
                Intent intent = new Intent(context, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("HEADER_ERROR", header.getError());
                if (!context.getClass().getSimpleName().equals(LoginActivity.class.getSimpleName())) {
                    ((MyApp) context.getApplicationContext()).setIsLogOut(true);
                    ((MyApp) context.getApplicationContext()).setIsLogIn(false);
                    context.startActivity(intent);
                } else {
                    Common.alertDialog("Hết phiên làm việc. Vui lòng đăng nhập lại", context);
                }
            }
        }
    }
}