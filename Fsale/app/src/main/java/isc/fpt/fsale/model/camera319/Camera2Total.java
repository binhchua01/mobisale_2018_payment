package isc.fpt.fsale.model.camera319;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.model.ServiceCode;

public class Camera2Total implements Parcelable {

    private List<ServiceCode> ServiceCodes;
    private int Total;

    public Camera2Total() {
    }

    protected Camera2Total(Parcel in) {
        ServiceCodes = in.createTypedArrayList(ServiceCode.CREATOR);
        Total = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(ServiceCodes);
        dest.writeInt(Total);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Camera2Total> CREATOR = new Creator<Camera2Total>() {
        @Override
        public Camera2Total createFromParcel(Parcel in) {
            return new Camera2Total(in);
        }

        @Override
        public Camera2Total[] newArray(int size) {
            return new Camera2Total[size];
        }
    };

    public List<ServiceCode> getServiceCodes() {
        return ServiceCodes;
    }

    public void setServiceCodes(List<ServiceCode> serviceCodes) {
        this.ServiceCodes = serviceCodes;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

}
