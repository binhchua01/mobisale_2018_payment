package isc.fpt.fsale.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 11,September,2019
 */
public class DialogCallback {
    private static DialogCallback sSoleInstance;

    private DialogCallback() {
    }  //private constructor.

    public static DialogCallback getInstance() {
        if (sSoleInstance == null) { //if there is no instance available... create new one
            sSoleInstance = new DialogCallback();
        }

        return sSoleInstance;
    }

    public AlertDialog.Builder dialogBuilder(final Context mContext, String message,
                                             final int itemResult, final DCallback cb){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.title_notification))
                .setMessage(message)
                .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cb.callback(itemResult);
                        dialog.cancel();
                    }
                })
                .setCancelable(false)
                .create();
        return builder;
    }
}


