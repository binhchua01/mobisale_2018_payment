package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.FptCameraActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 21,October,2019
 */
public class TotalPriceEquipmentCamera implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FptCameraActivity cameraActivityNew;
    private RegisterContractActivity registerContractActivity;
    private boolean isSellMore;

    public TotalPriceEquipmentCamera(Context mContext, List<Device> mList, int cusStatus, boolean isSellMore) {
        this.mContext = mContext;
        this.isSellMore = isSellMore;
        if (mContext.getClass().getSimpleName()
                .equals(RegisterContractActivity.class.getSimpleName())) {
            registerContractActivity = (RegisterContractActivity) mContext;
        } else if (mContext.getClass().getSimpleName().equals(FptCameraActivity.class.getSimpleName())) {
            cameraActivityNew = (FptCameraActivity) mContext;
        }
        JSONArray jsonArray = new JSONArray();
        for (Device item : mList) {
            jsonArray.put(item.toJsonObjectTotal());
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("LocationID", Constants.LOCATION_ID);
            jsonObject.put("CustomerStatus", cusStatus);/* 0 - bán mới , 1 - bán thêm*/
            jsonObject.put("Devices", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.msg_pd_get_device_total);
        String GET_DEVICE_TOTAL = "TotalPriceEquipmentCamera";
        CallServiceTask service = new CallServiceTask(mContext, GET_DEVICE_TOTAL,
                jsonObject, Services.JSON_POST_OBJECT, message, TotalPriceEquipmentCamera.this);
        service.execute();
    }

    private JSONObject getJsonObject(String result) {
        try {
            return JSONParsing.getJsonObj(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onTaskComplete(String result) {
        if (Common.jsonObjectValidate(result)) {
            JSONObject jsObj = getJsonObject(result);
            if (jsObj != null) {
                try {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    String TAG_ERROR_CODE = "ErrorCode";
                    if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        String TAG_OBJECT_LIST = "ListObject";
                        JSONArray array = jsObj.getJSONArray(TAG_OBJECT_LIST);
                        JSONObject item = array.getJSONObject(0);
                        int Amount = 0;
                        if (item.has("Amount"))
                            Amount = item.getInt("Amount");
                        try {
                            if (cameraActivityNew != null) {
                                cameraActivityNew.fragTotal.loadDevicePrice(Amount);
                            } else if (registerContractActivity != null) {
                                registerContractActivity.loadDeviceCameraPrice(Amount);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Common.alertDialog(jsObj.getString("Error"), mContext);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
