package isc.fpt.fsale.ui.extra_ott.extra_ott_package;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.DetailPackage;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

/**
 * Created by haulc3 on 19,July,2019
 */
public class ExtraOttPackageAdapter extends RecyclerView.Adapter<ExtraOttPackageAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<DetailPackage> mList;
    private OnItemClickListener mListener;
    private List<DetailPackage> mListSelected;

    ExtraOttPackageAdapter(Context mContext, List<DetailPackage> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
        mListSelected = new ArrayList<>();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_key_value, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.bindData(mList.get(position));
        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }

        public void bindData(DetailPackage object) {
            tvName.setText(object.getName());
            if(object.isSelected()){
                tvName.setBackgroundResource(R.drawable.background_radius_selected);
            }else{
                tvName.setBackgroundResource(R.drawable.background_radius);
            }
        }
    }

    public void notifyData(List<DetailPackage> mList){
        this.mList = mList;
        notifyDataSetChanged();
    }

    private void onItemClick(int position){
        if(mList.get(position).isSelected()){
            mList.get(position).setSelected(false);
            mListSelected.remove(mList.get(position));
        }else{
            mList.get(position).setSelected(true);
            mListSelected.add(mList.get(position));
        }
        mListener.onItemClick(mListSelected);
        notifyData(this.mList);
    }
}
