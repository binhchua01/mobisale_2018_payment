package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ListRegistrationModel implements Parcelable {
    private int ID;
    private String registrationId;
    private String fullName;
    private String address;
    private String statusBookPort;
    private String statusInVest;
    private String statusBilling;
    private String localType;
    private String RowNumber;
    private String TotalPage;
    private String CurrentPage;
    private String TotalRow;
    private int statusDeposit;
    private int AutoContractStatus;
    private String AutoContractStatusDesc;
    private String Contract;
    private String strStatusBookPort;
    private String PaidStatusName;

    public ListRegistrationModel(int ID, String registrationId, String fullName, String address,
                                 String statusBookPort, String statusInVest, String statusBilling,
                                 String localType, String rowNumber, String totalPage,
                                 String currentPage, String totalRow, int statusDeposit,
                                 int AutoContractStatus, String AutoContractStatusDesc,
                                 String Contract, String strStatusBookPort, String paidStatusName) {
        this.setID(ID);
        this.setRegistrationId(registrationId);
        this.setFullName(fullName);
        this.setStatusBilling(statusBilling);
        this.setStatusBookPort(statusBookPort);
        this.setStatusInVest(statusInVest);
        this.setAddress(address);
        this.setLocalType(localType);
        this.setRowNumber(rowNumber);
        this.setTotalPage(totalPage);
        this.setCurrentPage(currentPage);
        this.setTotalRow(totalRow);
        this.statusDeposit = statusDeposit;
        this.AutoContractStatus = AutoContractStatus;
        this.AutoContractStatusDesc = AutoContractStatusDesc;
        this.Contract = Contract;
        this.strStatusBookPort = strStatusBookPort;
        this.PaidStatusName = paidStatusName;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStatusBookPort() {
        return statusBookPort;
    }

    public void setStatusBookPort(String statusBookPort) {
        this.statusBookPort = statusBookPort;
    }

    public String getStatusInVest() {
        return statusInVest;
    }

    public void setStatusInVest(String statusInVest) {
        this.statusInVest = statusInVest;
    }

    public String getStatusBilling() {
        return statusBilling;
    }

    public void setStatusBilling(String statusBilling) {
        this.statusBilling = statusBilling;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocalType() {
        return localType;
    }

    public void setLocalType(String localType) {
        this.localType = localType;
    }

    public String getRowNumber() {
        return RowNumber;
    }

    public void setRowNumber(String rowNumber) {
        RowNumber = rowNumber;
    }

    public String getTotalPage() {
        return TotalPage;
    }

    public void setTotalPage(String totalPage) {
        TotalPage = totalPage;
    }

    public void setCurrentPage(String currentPage) {
        CurrentPage = currentPage;
    }

    public String getCurrentPage() {
        return CurrentPage;
    }

    public void setTotalRow(String totalRow) {
        TotalRow = totalRow;
    }

    public String getTotalRow() {
        return TotalRow;
    }

    public int getStatusDeposit() {
        return statusDeposit;
    }

    public void setStatusDeposit(int statusDeposit) {
        this.statusDeposit = statusDeposit;
    }

    public int getAutoContractStatus() {
        return AutoContractStatus;
    }

    public void setAutoContractStatus(int AutoContractStatus) {
        this.AutoContractStatus = AutoContractStatus;
    }

    public String getAutoContractStatusDesc() {
        return AutoContractStatusDesc;
    }

    public void setAutoContractStatusDesc(String AutoContractStatusDesc) {
        this.AutoContractStatusDesc = AutoContractStatusDesc;
    }

    public String getContract() {
        return Contract;
    }

    public void setContract(String Contract) {
        this.Contract = Contract;
    }

    public String getStrStatusBookPort() {
        return strStatusBookPort;
    }

    public void setStrStatusBookPort(String strStatusBookPort) {
        this.strStatusBookPort = strStatusBookPort;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel pc, int flags) {
        pc.writeInt(ID);
        pc.writeString(registrationId);
        pc.writeString(fullName);
        pc.writeString(address);
        pc.writeString(localType);
        pc.writeString(statusBilling);
        pc.writeString(statusBookPort);
        pc.writeString(statusInVest);
        pc.writeString(RowNumber);
        pc.writeString(TotalPage);
        pc.writeString(CurrentPage);
        pc.writeString(TotalRow);
        pc.writeInt(statusDeposit);
        pc.writeInt(AutoContractStatus);
        pc.writeString(AutoContractStatusDesc);
        pc.writeString(Contract);
        pc.writeString(strStatusBookPort);
        pc.writeString(PaidStatusName);

    }

    private ListRegistrationModel(Parcel source) {
        ID = source.readInt();
        registrationId = source.readString();
        fullName = source.readString();
        address = source.readString();
        localType = source.readString();
        statusBilling = source.readString();
        statusBookPort = source.readString();
        statusInVest = source.readString();
        RowNumber = source.readString();
        TotalPage = source.readString();
        CurrentPage = source.readString();
        TotalRow = source.readString();
        statusDeposit = source.readInt();
        AutoContractStatus = source.readInt();
        AutoContractStatusDesc = source.readString();
        Contract = source.readString();
        strStatusBookPort = source.readString();
        PaidStatusName = source.readString();
    }


    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public String getPaidStatusName() {
        return PaidStatusName;
    }

    public void setPaidStatusName(String paidStatusName) {
        PaidStatusName = paidStatusName;
    }

    public static final Creator<ListRegistrationModel> CREATOR = new Creator<ListRegistrationModel>() {
        @Override
        public ListRegistrationModel createFromParcel(Parcel source) {
            return new ListRegistrationModel(source);
        }

        @Override
        public ListRegistrationModel[] newArray(int size) {
            return new ListRegistrationModel[size];
        }
    };
}
