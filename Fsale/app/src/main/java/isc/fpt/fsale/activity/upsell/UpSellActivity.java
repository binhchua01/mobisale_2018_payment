package isc.fpt.fsale.activity.upsell;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TabHost;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.upsell.fragment.FragmentUpsellCare;
import isc.fpt.fsale.activity.upsell.fragment.FragmentUpsellNotCare;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class UpSellActivity extends BaseActivitySecond implements TabHost.OnTabChangeListener {

    private View loBack;
    public static final String TAB_CARE = "TAB_CARE";
    public static final String TAB_NOT_CARE = "TAB_NOT_CARE";

    public static FragmentManager fragmentManager;
    private FragmentTabHost mTabHost;
    @Override
    protected void initEvent() {
        loBack.setOnClickListener(view -> onBackPressed());
        setupTabsHost();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_up_sell;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        fragmentManager = getSupportFragmentManager();
        mTabHost = (FragmentTabHost) findViewById(R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.tabFrameLayout);
    }
    private void setupTabsHost(){
        mTabHost.addTab(mTabHost.newTabSpec(TAB_CARE).setIndicator(getString(R.string.lbl_tab_care).toUpperCase()), FragmentUpsellCare.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_NOT_CARE).setIndicator(getString(R.string.lbl_tab_notcare).toUpperCase()), FragmentUpsellNotCare.class, null);

        mTabHost.setOnTabChangedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    public static int getLevelByName(String fragmentTabName) {
        if (fragmentTabName.equals(TAB_CARE))
            return 1;
        if (fragmentTabName.equals(TAB_NOT_CARE))
            return 2;
        return -1;
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public void onTabChanged(String tabId) {
        Common.hideSoftKeyboard(this);
    }

//    public List<CategoryUpsellList> getListUpsell() {
//        return mListUpsell != null ? mListUpsell : new ArrayList<CategoryUpsellList>();
//    }
}
