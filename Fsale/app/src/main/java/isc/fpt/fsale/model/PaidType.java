package isc.fpt.fsale.model;

/**
 * Created by HCM.TUANTT14 on 4/27/2018.
 */
// model hình thức thanh toán
public class PaidType {
    private int Key;
    private String Value;

    public int getKey() {
        return Key;
    }

    public void setKey(int key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
