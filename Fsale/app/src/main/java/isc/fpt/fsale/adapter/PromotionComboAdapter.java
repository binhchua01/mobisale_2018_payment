package isc.fpt.fsale.adapter;

import java.util.ArrayList;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionComboModel;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class PromotionComboAdapter extends ArrayAdapter<PromotionComboModel>{
	private ArrayList<PromotionComboModel> lstObj;	
	private Context mContext;
	private boolean hasInitText = false;
	/*private int iColor = 2;
    private Typeface tf; */

	public PromotionComboAdapter(Context context, int textViewResourceId, ArrayList<PromotionComboModel> lstObj) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.lstObj = lstObj;
		/*this.tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");*/
	}
	
	public PromotionComboAdapter(Context context,	int textViewResourceId, ArrayList<PromotionComboModel> lstObj, int color) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.lstObj = lstObj;
		/*this.tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");
		this.iColor = color;*/
	}
	
	public PromotionComboAdapter(Context context,	int textViewResourceId, ArrayList<PromotionComboModel> lstObj, boolean hasInitText) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.lstObj = lstObj;
		this.hasInitText = true;
	}
	

	public int getCount(){
		if(lstObj != null)
	       return lstObj.size();
		return 0;
	}
		
	public PromotionComboModel getItem(int position){
		if(lstObj != null)
			return lstObj.get(position);
		return null;
    }
	
	public long getItemId(int position){
       return position;
    }	
	
    @SuppressLint("InflateParams")
	@Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
    	PromotionComboModel item = lstObj.get(position);    	
    	if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_list_view_promotion_internet, null);
		}
    	TextView label = (TextView)convertView.findViewById(R.id.lbl_internet_promotion);
    	if(item != null){	        
	        if(hasInitText && position == 0){
	        	label.setVisibility(View.GONE);  
	        }
	        else{
	            //label.setText(lstObj.get(position).getDescription());
	        	label.setText(item.getDescription());
	        }	        
        }
        int padding = (int) mContext.getResources().getDimension(R.dimen.padding_medium);
        convertView.setPadding(padding,padding,padding,padding);    
    	
        return convertView;    	
    }	
	
	@SuppressLint("InflateParams")
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		PromotionComboModel item = lstObj.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_list_view_promotion_internet, null);
		}
		TextView label = (TextView)convertView.findViewById(R.id.lbl_internet_promotion);
		if(item != null){
	        if(item != null)
	        	label.setText(item.getDescription()); 	      
		}
        return label;
    }

}
