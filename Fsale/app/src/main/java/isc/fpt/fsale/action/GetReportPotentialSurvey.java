package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportPotentialSurveyActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportItemModel;
import isc.fpt.fsale.model.ReportPotentialSurveyModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetReportPotentialSurvey implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "ReportPotentialSurvey";
	private String[] paramValues;
	private String FromDate, ToDate;
	
	public static final String[] paramNames = new String[]{"UserName", "Agent", "AgentName", "Dept", "FromDate", "ToDate","PageNumber"};
	//
	
	public GetReportPotentialSurvey(Context context, int Agent, String AgentName, int Dept, String FromDate, String ToDate, int PageNumber) {	
		mContext = context;
		String UserName = "";
		this.FromDate = FromDate;
		this.ToDate = ToDate;
		if(mContext != null)
			UserName = ((MyApp)mContext.getApplicationContext()).getUserName();
		this.paramValues = new String[]{UserName, String.valueOf(Agent), AgentName, String.valueOf(Dept), this.FromDate, this.ToDate, 
				String.valueOf(PageNumber)};			
		execute();
	}
	
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportPotentialSurvey.this);
		service.execute();		
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			ArrayList<ReportPotentialSurveyModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ReportPotentialSurveyModel> resultObject = 
						 new WSObjectsModel<ReportPotentialSurveyModel>(jsObj,
								 ReportPotentialSurveyModel.class, ReportItemModel.class.getName());
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();						
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetCustomerCareList:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	
	
	private void loadData(ArrayList<ReportPotentialSurveyModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListReportPotentialSurveyActivity.class.getSimpleName())){
				ListReportPotentialSurveyActivity activity = (ListReportPotentialSurveyActivity)mContext;
				activity.LoadData(lst, this.FromDate, this.ToDate);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetCustomerCareList.loadRpCodeInfo()", e.getMessage());
		}
	}
	

		
	

}
