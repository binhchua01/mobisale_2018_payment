package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

public class InsertBookPortADSL implements AsyncTaskCompleteListener<String>{ 

	private final String InsertBookPortADSL = "BookPortADSL";
	private final String InsertBookPortADSL_RESULT = "BookPortADSLResult";
	private final String ERRORSERVICE = "ErrorService";
	private final String RESULT = "Result";
	
	private Context mContext;
	private int ID;
	
	public InsertBookPortADSL(Context context,String[] paramsValue,String _ID) 
	{
		mContext = context;
		try {
			this.ID= Integer.valueOf(_ID);
		} catch (Exception e) {
			// TODO: handle exception

			ID = 0;
		}
		
		//String[] paramsValue=new String[]{"36","5","oanh254","5","1","SGD175914","123456","590446","192.168.123.123"};
		InsertADSL(paramsValue);
	}
	
	public void InsertADSL(String[] paramsValue)
	{		
		String[] params = new String[]{"LocationID", "BranchID", "UserName", "IDTD", "TypeSevice",
				"RegCode", "CardNumber", "IDPortTD", "IP", "TDName", "Latlng", "LatlngDevice", "LatlngMarker"};
		String message = "Vui lòng đợi giây lát";
		CallServiceTask service = new CallServiceTask(mContext,InsertBookPortADSL,params,paramsValue, Services.JSON_POST, message, InsertBookPortADSL.this);
		service.execute();
	}
	
	public void handleUpdateRegistration(String json){		
		if(json != null && Common.jsonObjectValidate(json)){			
			 try {	
				   JSONArray jsArr;
				    JSONObject jsObj = JSONParsing.getJsonObj(json);
				    jsArr = jsObj.getJSONArray(InsertBookPortADSL_RESULT);
				    int l = jsArr.length();
				    if (l > 0) 
				    {
					     String error = jsArr.getJSONObject(0).getString(ERRORSERVICE);
					     if (error.equals("null")) 
					     {
					    	 String result = jsArr.getJSONObject(0).getString(RESULT);
						      int ResultID=jsArr.getJSONObject(0).getInt("ResultID");
						      if(ResultID==1)
						      {
						    	  new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage("BookPort thành công !")
					 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
					 				    @Override
					 				    public void onClick(DialogInterface dialog, int which) {					 				    	
					 				    	
					 				    	try {
					 				    		new GetRegistrationDetail(mContext,Constants.USERNAME,ID);
					 				    		dialog.cancel();
												//((Activity)mContext).finish();
											} catch (Exception e) {

												Log.i("InsertBookPortADSL", "_handleUpdateRegistration=" + e.getMessage());
											}
					 				        
					 				    }
					 				})
				 					.setCancelable(false).create().show();
						    	  
						    	 
						      }
						      else
						      {
						    	 // new GetRegistrationDetail(mContext,"oanh254",this.ID);
						    	 Common.alertDialog(result, mContext);
						      }
					     }
					     else  Common.alertDialog(error, mContext);
					    	 
				    }
			 }
			 catch (Exception e)
			 {

				e.printStackTrace();
				Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
			 }
		}		
		else Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);		
	}
	
	
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);
	}

}
