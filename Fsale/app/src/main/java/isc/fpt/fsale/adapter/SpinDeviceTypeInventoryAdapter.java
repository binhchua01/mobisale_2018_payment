package isc.fpt.fsale.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ListDeviceOTT;

public class SpinDeviceTypeInventoryAdapter extends ArrayAdapter<ListDeviceOTT> {
    private ArrayList<ListDeviceOTT> list;

    protected SpinDeviceTypeInventoryAdapter(@NonNull Context context, int resource, int textViewResourceId,
                                           @NonNull ArrayList<ListDeviceOTT> list) {
        super(context, resource, textViewResourceId, list);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Nullable
    @Override
    public ListDeviceOTT getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getOttId();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = (TextView) super.getView(position, convertView, parent).findViewById(R.id.text_view_0);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(list.get(position).getOttName());
        label.setPadding(10, 15, 10, 15);

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent).findViewById(R.id.text_view_0);
        label.setTextColor(Color.BLACK);
        label.setText(list.get(position).getOttName());
        label.setPadding(10, 15, 10, 15);
        return label;
    }
}
