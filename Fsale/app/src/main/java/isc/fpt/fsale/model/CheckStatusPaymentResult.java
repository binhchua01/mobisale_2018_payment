package isc.fpt.fsale.model;

/**
 * Created by HCM.TUANTT14 on 5/2/2018.
 */

public class CheckStatusPaymentResult {
    private int PaymentStatus;
    private String PaymentTitle, PaymentMessage;

    public int getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(int paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getPaymentTitle() {
        return PaymentTitle;
    }

    public void setPaymentTitle(String paymentTitle) {
        PaymentTitle = paymentTitle;
    }

    public String getPaymentMessage() {
        return PaymentMessage;
    }

    public void setPaymentMessage(String paymentMessage) {
        PaymentMessage = paymentMessage;
    }
}
