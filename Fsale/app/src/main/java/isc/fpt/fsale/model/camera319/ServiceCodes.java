package isc.fpt.fsale.model.camera319;

import android.os.Parcel;
import android.os.Parcelable;

public class ServiceCodes implements Parcelable {

    private double BoxNumber;
    private int ID;
    private String Name;
    private int TotalAfterTax;
    private int TotalPreTax;

    public ServiceCodes(double boxNumber, int ID, String name,
                        int totalAfterTax, int totalPreTax) {
        BoxNumber = boxNumber;
        this.ID = ID;
        Name = name;
        TotalAfterTax = totalAfterTax;
        TotalPreTax = totalPreTax;
    }

    public ServiceCodes() {
    }

    public double getBoxNumber() {
        return BoxNumber;
    }

    public void setBoxNumber(double boxNumber) {
        BoxNumber = boxNumber;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getTotalAfterTax() {
        return TotalAfterTax;
    }

    public void setTotalAfterTax(int totalAfterTax) {
        TotalAfterTax = totalAfterTax;
    }

    public int getTotalPreTax() {
        return TotalPreTax;
    }

    public void setTotalPreTax(int totalPreTax) {
        TotalPreTax = totalPreTax;
    }

    public static Creator<ServiceCodes> getCREATOR() {
        return CREATOR;
    }

    protected ServiceCodes(Parcel in) {
        BoxNumber = in.readDouble();
        ID = in.readInt();
        Name = in.readString();
        TotalAfterTax = in.readInt();
        TotalPreTax = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(BoxNumber);
        dest.writeInt(ID);
        dest.writeString(Name);
        dest.writeInt(TotalAfterTax);
        dest.writeInt(TotalPreTax);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServiceCodes> CREATOR = new Creator<ServiceCodes>() {
        @Override
        public ServiceCodes createFromParcel(Parcel in) {
            return new ServiceCodes(in);
        }

        @Override
        public ServiceCodes[] newArray(int size) {
            return new ServiceCodes[size];
        }
    };
}
