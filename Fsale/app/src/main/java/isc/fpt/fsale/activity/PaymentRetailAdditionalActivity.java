package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListPaidType;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.action.PaymentInformation;
import isc.fpt.fsale.action.SaleMore;
import isc.fpt.fsale.adapter.PaidTypeAdapter;
import isc.fpt.fsale.model.CheckListRegistration;
import isc.fpt.fsale.model.PaidType;
import isc.fpt.fsale.model.PaymentInformationPOST;
import isc.fpt.fsale.model.PaymentInformationResult;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// màn hình thanh toán online bán thêm
public class PaymentRetailAdditionalActivity extends BaseActivity {
    private TextView tvRegCodePayContract, tvContractCodePayment, tvCusNamePayment,
            tvPhonePayment, tvTotalPayment, tvStatusPayContract;
    private Spinner spPayContract;
    private Button btnPayContract, btnGoToAppointmentScreen;
    private RegistrationDetailModel mRegister;
    private PaymentInformationPOST paymentInformationPOST;
    private ImageView ivCustomerSignatureDepositContract;
    private String linkImage;
    private LinearLayout layoutStatusPayment, layoutSignature;

    public PaymentRetailAdditionalActivity() {
        super(R.string.pay_contract_header_activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.pay_contract_header_activity));
        setContentView(R.layout.activity_pay_contract);
        initWidget();
        showDataOnText();
        createEvents();
    }

    private void initWidget() {
        layoutSignature = (LinearLayout) findViewById(R.id.frm_container_customer_signature);
        layoutSignature.setVisibility(View.GONE);
        ivCustomerSignatureDepositContract = (ImageView) findViewById(R.id.iv_customer_signature_deposit_contract);
        tvRegCodePayContract = (TextView) findViewById(R.id.tv_reg_code_pay_contract);
        tvContractCodePayment = (TextView) findViewById(R.id.tv_code_pay_contract);
        tvCusNamePayment = (TextView) findViewById(R.id.tv_cus_name_pay_contract);
        tvPhonePayment = (TextView) findViewById(R.id.tv_phone_pay_contract);
        tvTotalPayment = (TextView) findViewById(R.id.tv_total_pay_contract);
        spPayContract = (Spinner) findViewById(R.id.sp_pay_contract);
        tvStatusPayContract = (TextView) findViewById(R.id.tv_status_pay_contract);
        btnPayContract = (Button) findViewById(R.id.btn_pay_contract);
        layoutStatusPayment = (LinearLayout) findViewById(R.id.layout_status_payment);
        btnGoToAppointmentScreen = (Button) findViewById(R.id.btn_tick_deploy);
        layoutSignature = (LinearLayout) findViewById(R.id.frm_container_customer_signature);
        layoutSignature.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
        new PaymentInformation(this, paymentInformationPOST);
    }

    /**
     * Show data on text
     */
    private void showDataOnText() {
        mRegister = getIntent().getParcelableExtra(Constants.MODEL_REGISTER);
        paymentInformationPOST = new PaymentInformationPOST();
        layoutStatusPayment.setVisibility(View.GONE);
        if (mRegister != null) {
            tvContractCodePayment.setText(mRegister.getContract());
            tvRegCodePayContract.setText(mRegister.getRegCode());
            tvCusNamePayment.setText(mRegister.getFullName());
            paymentInformationPOST.setSaleName(mRegister.getUserName());
            paymentInformationPOST.setRegCode(mRegister.getRegCode());
            tvTotalPayment.setText(Common.formatNumber(mRegister.getTotal()));
            tvPhonePayment.setText(mRegister.getPhone_1());
            getPaidTypeList();
        } else {
            Common.alertDialog("Thông tin chi tiết phiếu đăng ký truyền đi thất bại", this);
        }
    }

    // kết nối API lấy danh sách loại thanh toán
    private void getPaidTypeList() {
        new GetListPaidType(this, this, mRegister.getUserName(), mRegister.getRegCode());
    }

    /**
     * Create Events
     */
    private void createEvents() {
        //ImageView Signature
//        ivCustomerSignatureDepositContract.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(PaymentRetailAdditionalActivity.this, SignatureActivity.class);
//                intent.putExtra("RegID", mRegister.getID());
//                startActivityForResult(intent, 1);
//            }
//        });

        //Button contract
        btnPayContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (linkImage != null) {
                    if (mRegister != null) {
                        int paidType = spPayContract.getAdapter() != null ? ((PaidType) spPayContract.getSelectedItem()).getKey() : 0;
                        String paidValue = spPayContract.getAdapter() != null ? ((PaidType) spPayContract.getSelectedItem()).getValue() : "";
                        new SaleMore(PaymentRetailAdditionalActivity.this, paidValue, paidType, linkImage, mRegister);
                    } else {
                        Common.alertDialog("Thông tin chi tiết phiếu đăng ký truyền đi thất bại", PaymentRetailAdditionalActivity.this);
                    }
            }
        });

        btnGoToAppointmentScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PaymentRetailAdditionalActivity.this, DeployAppointmentActivity.class);
                intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                PaymentRetailAdditionalActivity.this.finish();
            }
        });

        spPayContract.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("image");
                linkImage = data.getStringExtra("Id");
                Bitmap decodedByte = Common.StringToBitMap(result);
                BitmapDrawable ob = new BitmapDrawable(getResources(), decodedByte);
                ivCustomerSignatureDepositContract.setBackground(ob);
            }
        }
    }

    //call back lấy danh sách thanh toán từ api
    public void loadPaidTypeList(ArrayList<PaidType> paidTypeList) {
        if (paidTypeList.size() > 0) {
            spPayContract.setAdapter(new PaidTypeAdapter(this, R.layout.my_spinner_payment_style, paidTypeList));
        } else {
            Common.alertDialog("Không có dữ liệu danh sách thanh toán", this);
        }
    }

    @SuppressLint("SetTextI18n")
    public void updateStatusPayment(CheckListRegistration statusCheckList) {
        if (statusCheckList != null) {
            if (statusCheckList.getTrangThaiThanhToan() == 0) {
                tvStatusPayContract.setText("Đã thanh toán");
            } else {
                tvStatusPayContract.setText("Chưa thanh toán");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public void onBackPressed() {
        if (mRegister != null) {
            new GetRegistrationDetail(this, mRegister.getUserName(), mRegister.getID(), true);
        } else {
            Common.showToast(this, "Thông tin khách hàng không tồn tại");
            finish();
        }
    }

    public void showPaymentInformation(PaymentInformationResult objResult) {
        if (objResult == null) {
            return;
        }
        if (objResult.getPaymentStatus() == 1 || objResult.getPaymentStatus() == 0) {
            String paidValue = spPayContract.getAdapter() != null ? ((PaidType) spPayContract.getSelectedItem()).getValue() : "";
            Intent confirmIntent = new Intent(this, ConfirmPaymentRetailAdditionalActivity.class);
            confirmIntent.putExtra(Constants.MODEL_REGISTER, mRegister);
            confirmIntent.putExtra("PAID_TYPE_VALUE", paidValue);
            this.startActivity(confirmIntent);
        }
    }
}
