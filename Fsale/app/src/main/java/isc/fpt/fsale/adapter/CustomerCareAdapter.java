package isc.fpt.fsale.adapter;

import isc.fpt.fsale.action.UpdateConfirmNotification;
import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentListCustomerCare;
import isc.fpt.fsale.model.CustomerCareListModel;
import isc.fpt.fsale.model.CustomerCareSubDetailModel;
import isc.fpt.fsale.model.ParseJsonModel;
import isc.fpt.fsale.utils.Common;

import java.util.List;

import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomerCareAdapter extends BaseAdapter {
	private List<CustomerCareListModel> mList;
	private Context mContext;
	private FragmentListCustomerCare mFragment;

	public CustomerCareAdapter(Context context,
			List<CustomerCareListModel> lst, FragmentListCustomerCare fragment) {
		this.mContext = context;
		this.mList = lst;
		mFragment = fragment;
	}

	public void clearAll() {
		this.mList.clear();
		this.notifyDataSetChanged();
	}

	public void setListData(List<CustomerCareListModel> lst) {
		this.mList.clear();
		this.mList.addAll(lst);
		this.notifyDataSetChanged();
	}

	public void addAll(List<CustomerCareListModel> lst) {
		this.mList.addAll(lst);
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressWarnings("deprecation")
	@SuppressLint({ "InflateParams", "NewApi" })
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		final CustomerCareListModel item = mList.get(position);
		ViewHolder holder = null;
		if (view == null) {
			view = LayoutInflater.from(mContext).inflate(
					R.layout.row_customer_care, null);
			holder = new ViewHolder();
			holder.wv = (WebView) view.findViewById(R.id.web_view);
			holder.frmTitle = (LinearLayout) view.findViewById(R.id.frm_title);
			holder.lblFullName = (TextView) view
					.findViewById(R.id.lbl_full_name);
			holder.lblContract = (TextView) view
					.findViewById(R.id.lbl_contract);
			holder.lblEmail = (TextView) view.findViewById(R.id.lbl_email);
			holder.lblPhoneNumber = (TextView) view
					.findViewById(R.id.lbl_phone);
			holder.lblAddress = (TextView) view.findViewById(R.id.lbl_address);
			holder.lblObjStatusName = (TextView) view
					.findViewById(R.id.lbl_obj_status_name);
			holder.lblCreateDate = (TextView) view
					.findViewById(R.id.lbl_create_date);
			holder.btnComfirmNotifi = (Button) view
					.findViewById(R.id.btn_comfirm_info);
			holder.btnComfirmNotifi.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String Note = null;
					String code = item.getCode();
					try {
						JSONObject json = new JSONObject(item
								.getObjStatusDesc());
						CustomerCareSubDetailModel desc = ParseJsonModel.parse(
								json, CustomerCareSubDetailModel.class);
						code = desc.getConfirm_code();
					} catch (Exception e) {
						// TODO: handle exception

						code = item.getCode();
						e.printStackTrace();
					}
					new UpdateConfirmNotification(mContext, code, Note,
							mFragment);
				}
			});
			holder.frmSale = (LinearLayout) view
					.findViewById(R.id.frm_sale_info);
			holder.lblSaleName = (TextView) view
					.findViewById(R.id.lbl_sale_name);
			holder.lblSaleFullName = (TextView) view
					.findViewById(R.id.lbl_sale_full_name);
			holder.lblSaleEmail = (TextView) view
					.findViewById(R.id.lbl_sale_email);
			holder.lblSalePhone = (TextView) view
					.findViewById(R.id.lbl_sale_phone);
			//
			holder.frmDesc = (LinearLayout) view.findViewById(R.id.frm_desc);
			holder.lblDescCusFeedback = (TextView) view
					.findViewById(R.id.lbl_desc_customer_feedback);
			holder.lblDescSurveyManNote = (TextView) view
					.findViewById(R.id.lbl_desc_survey_man_note);
			holder.lblDescSurveyTask = (TextView) view
					.findViewById(R.id.lbl_desc_survey_task);
			// holder.imgCastPoint =
			// (ImageView)view.findViewById(R.id.img_cast);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		Drawable drwTitle;
		switch (item.getLevelID()) {

		case 3:
			drwTitle = mContext.getResources().getDrawable(
					R.drawable.border_title_level_1);
			break;
		case 2:
			drwTitle = mContext.getResources().getDrawable(
					R.drawable.border_title_level_2);
			break;
		case 1:
			drwTitle = mContext.getResources().getDrawable(
					R.drawable.border_title_level_3);
			break;

		default:
			drwTitle = mContext.getResources().getDrawable(
					R.drawable.border_title_level_3);
			break;
		}

		final int sdk = android.os.Build.VERSION.SDK_INT;

		if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			holder.frmTitle.setBackgroundDrawable(drwTitle);
			// holder.btnComfirmNotifi.setBackgroundDrawable(drwTitle);
		} else {
			holder.frmTitle.setBackground(drwTitle);
			// holder.btnComfirmNotifi.setBackground(drwTitle);
		}

		holder.lblFullName.setText(item.getFullName());
		holder.lblContract.setText(item.getContract());
		holder.lblEmail.setText(item.getEmail());
		holder.lblPhoneNumber.setText(item.getPhoneNumber());
		holder.lblAddress.setText(item.getAddress());
		holder.lblObjStatusName.setText(item.getObjStatusName());
		holder.lblCreateDate.setText(item.getCreateDate());
		holder.wv.loadData(item.getObjStatusDesc(), "text/html; charset=utf-8",
				"UTF-8");
		holder.wv.getSettings().setSupportZoom(false);
		if (item.getDivisionID() == 82) {
			holder.frmSale.setVisibility(View.VISIBLE);
			holder.lblSaleEmail.setText(item.getSaleEmail());
			holder.lblSaleFullName.setText(item.getSaleFullName());
			holder.lblSaleName.setText(item.getSaleName());
			holder.lblSalePhone.setText(item.getSalePhone());

		} else {
			holder.frmSale.setVisibility(View.GONE);
		}
		//

		try {
			holder.wv.setVisibility(View.GONE);
			holder.frmDesc.setVisibility(View.VISIBLE);
			if (item.getObjStatusDesc() != null
					&& Common.jsonObjectValidate(item.getObjStatusDesc())) {
				JSONObject json = new JSONObject(item.getObjStatusDesc());
				CustomerCareSubDetailModel desc = ParseJsonModel.parse(json,
						CustomerCareSubDetailModel.class);
				holder.lblDescCusFeedback.setText(desc.getCsat() + " (CSAT = "
						+ desc.getPoint() + ")");
				holder.lblDescSurveyManNote.setText(desc.getNote());
				holder.lblDescSurveyTask.setText(desc.getTime() + " - Mã KS: "
						+ desc.getCode());
				int drawID = 0, point = 0;
				try {
					point = Integer.valueOf(desc.getPoint());
				} catch (Exception e) {
					// TODO: handle exception
					//
					e.printStackTrace();
					point = -1;
				}
				switch (point) {
				case 1:
					drawID = R.drawable.ic_cast_01;
					break;
				case 2:
					drawID = R.drawable.ic_cast_02;
					break;
				case 3:
					drawID = R.drawable.ic_cast_03;
					break;
				case 4:
					drawID = R.drawable.ic_cast_04;
					break;
				case 5:
					drawID = R.drawable.ic_cast_05;
					break;

				default:
					break;
				}

				if (drawID > 0) {
					try {
						// holder.imgCastPoint.setImageResource(drawID);
						holder.lblDescCusFeedback
								.setCompoundDrawablesWithIntrinsicBounds(0, 0,
										drawID, 0);
					} catch (Exception e) {
						// TODO: handle exception
//
						e.printStackTrace();
					}

				}
			}
		} catch (Exception e) {
			// TODO: handle exception

			holder.frmDesc.setVisibility(View.GONE);
			holder.wv.setVisibility(View.VISIBLE);
			holder.wv.loadData(item.getObjStatusDesc(),
					"text/html; charset=utf-8", "UTF-8");
		}
		return view;
	}

	private class ViewHolder {
		TextView lblFullName, lblContract, lblEmail, lblPhoneNumber,
				lblAddress, lblObjStatusName, lblCreateDate, lblSaleName,
				lblSaleFullName, lblSaleEmail, lblSalePhone, lblDescSurveyTask,
				lblDescCusFeedback, lblDescSurveyManNote;
		WebView wv;
		LinearLayout frmTitle, frmSale, frmDesc;
		Button btnComfirmNotifi;
		// ImageView imgCastPoint;

	}
}
