package isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_package;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetAllCameraPackage;
import isc.fpt.fsale.model.CameraPackage;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;

import static isc.fpt.fsale.utils.Constants.CAMERA_DETAIL;
import static isc.fpt.fsale.utils.Constants.CLASS_NAME;
import static isc.fpt.fsale.utils.Constants.COMBO;
import static isc.fpt.fsale.utils.Constants.OBJECT_CAMERA_DETAIL;
import static isc.fpt.fsale.utils.Constants.POSITION;

public class CameraPackageActivity extends BaseActivitySecond implements OnItemClickListener<CameraDetail> {
    private View loBack;
    private CameraPackageAdapter mAdapter;
    private List<CameraPackage> mList;
    private int position;
    private CameraDetail mCameraDetail;
    private boolean isSellMore = false;
    private int combo = 0;

    @Override
    protected void onResume() {
        super.onResume();
        if(mCameraDetail != null){
            new GetAllCameraPackage(this, mCameraDetail, combo, isSellMore);
        }
    }

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_camera_package;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.msg_spinner_item_default));
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_camera_package);

        getDataIntent();

        mList = new ArrayList<>();
        mAdapter = new CameraPackageAdapter(this, mList, mCameraDetail, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(CAMERA_DETAIL) != null) {
            mCameraDetail = bundle.getParcelable(CAMERA_DETAIL);
            position = bundle.getInt(POSITION);
            combo = bundle.getInt(COMBO);
            String className = bundle.getString(CLASS_NAME);
            assert className != null;
            switch (className) {
                case "FptCameraService":
                    isSellMore = false;
                    break;
                case "FragmentFptCamera":
                    isSellMore = true;
                    break;
                default:
                    break;
            }
        }
    }

    public void loadCameraPackage(List<CameraPackage> mList, CameraDetail mCameraDetail){
        this.mList.clear();
        for(CameraPackage item : mList){
            if(mCameraDetail.getPackID() == item.getPackID()){
                item.setSelected(true);
            }
        }
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(CameraDetail object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(OBJECT_CAMERA_DETAIL, object);
        returnIntent.putExtra(POSITION, position);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
