package isc.fpt.fsale.ui.fragment;
import isc.fpt.fsale.activity.CustomerCareDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.utils.Common;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
// màn hình Thông tin CHĂM SÓC KHÁCH HÀNG
public class FragmentCustomerCareInfo extends Fragment {

    private CustomerCareDetailActivity activity;
    private TextView lblContract, lblLocalType, lblEmail, lblPhone1, lblPhone2, lblAddress;
    
	public FragmentCustomerCareInfo() {
	}
	
	 // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_customer_care_info, container, false);
		activity = (CustomerCareDetailActivity)getActivity();
		lblContract = (TextView)view.findViewById(R.id.lbl_contract);
		lblLocalType = (TextView)view.findViewById(R.id.lbl_local_type);
		lblEmail = (TextView)view.findViewById(R.id.lbl_email);
		lblPhone1 = (TextView)view.findViewById(R.id.lbl_phone_1);
		lblPhone2 = (TextView)view.findViewById(R.id.lbl_phone_2);
		lblAddress = (TextView)view.findViewById(R.id.lbl_address);
		loadData();
		return view;
	}
	
	private void loadData(){
		if(activity != null && activity.getCustomer() != null){
			lblContract.setText(activity.getCustomer().getContract());
			lblLocalType.setText(activity.getCustomer().getLocalType());
			
			lblEmail.setText(activity.getCustomer().getEmail());
			if(activity.getCustomer().getEmail() != null && !activity.getCustomer().getEmail().trim().equals("")){
				if(Common.checkMailValid(activity.getCustomer().getEmail())){
					String a = "<a href=\"mailto:" + activity.getCustomer().getEmail().trim() + "\">"+ activity.getCustomer().getEmail() +"</a>";
					lblEmail.setText(Html.fromHtml(a));
					lblEmail.setMovementMethod(LinkMovementMethod.getInstance());
				}
			}
			lblPhone1.setText(activity.getCustomer().getPhoneNumber());
			if(activity.getCustomer().getPhoneNumber() != null && !activity.getCustomer().getPhoneNumber().trim().equals("")){
				String a = "<a href=\"tel:" + activity.getCustomer().getPhoneNumber().trim() + "\">"+ activity.getCustomer().getPhoneNumber() +"</a>";
				lblPhone1.setText(Html.fromHtml(a));
				lblPhone1.setMovementMethod(LinkMovementMethod.getInstance());	
			}
				//<a href="tel:18475555555">1-847-555-5555</a>
			lblPhone2.setText("");
			lblAddress.setText(activity.getCustomer().getAddress());
		}
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
	}
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
  
    
    
}
