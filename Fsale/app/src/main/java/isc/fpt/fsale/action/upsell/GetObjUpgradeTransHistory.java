package isc.fpt.fsale.action.upsell;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.model.upsell.response.ObjUpgradeTransHistoryModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetObjUpgradeTransHistory implements AsyncTaskCompleteListener<String>{

    private Context mContext;
    private OnGetObjUpgradeHistory mCallBack;

    public GetObjUpgradeTransHistory(Context mContext, String[] arrParams, OnGetObjUpgradeHistory mCallBack) {
        this.mContext = mContext;
        this.mCallBack = mCallBack;
        String[] params = new String[]{"ProgramUpgradeID","ObjID", "PageSize","PageNum"};
        String message = "Xin vui long cho giay lat...";
        String GET_OBJ_UPGRADE_HISTORY = "GetObjUpgradeTransHistory";
        CallServiceTask service = new CallServiceTask(mContext, GET_OBJ_UPGRADE_HISTORY, params, arrParams,
                Services.JSON_POST, message, GetObjUpgradeTransHistory.this);
        service.execute();
    }

    public interface OnGetObjUpgradeHistory{
        void onGetObjUpgradeHistorySuccess(List<ObjUpgradeTransHistoryModel> mList);
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<ObjUpgradeTransHistoryModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ObjUpgradeTransHistoryModel.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        mCallBack.onGetObjUpgradeHistorySuccess(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
