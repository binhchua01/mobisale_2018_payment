package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class ListOTT {
    @SerializedName("OTTID")
    @Expose
    private Integer OTTID;
    @SerializedName("PromotionID")
    @Expose
    private Integer promotionID;
    @SerializedName("Qty")
    @Expose
    private Integer qty;

    public ListOTT(Integer OTTID, Integer promotionID, Integer qty) {
        this.OTTID = OTTID;
        this.promotionID = promotionID;
        this.qty = qty;
    }

    public Integer getOTTID() {
        return OTTID;
    }

    public void setOTTID(Integer OTTID) {
        this.OTTID = OTTID;
    }

    public Integer getPromotionID() {
        return promotionID;
    }

    public void setPromotionID(Integer promotionID) {
        this.promotionID = promotionID;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OTTID", getOTTID());
            jsonObject.put("PromotionID", getPromotionID());
            jsonObject.put("Qty", getQty());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
