package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import isc.fpt.fsale.ui.fragment.FragmentListRegistration;

public class ViewPagerListRegistration extends FragmentPagerAdapter {
    private final String TAG_NAME_REG_NEW = "TTKH mới";//"Phiếu đăng ký mới";
    private final String TAG_NAME_REG_OLD = "TTKH bán thêm";//"Phiếu đăng ký bán thêm";
    private final String TAG_NAME_REG_NOT_APPROVAL = "TTKH chưa được duyệt";//"Phiếu đăng ký chưa được duyệt";

    public ViewPagerListRegistration(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FragmentListRegistration.newInstance(0, TAG_NAME_REG_NEW);
            case 1:
                return FragmentListRegistration.newInstance(2, TAG_NAME_REG_OLD);
            case 2:
                return FragmentListRegistration.newInstance(1, TAG_NAME_REG_NOT_APPROVAL);
            default:
                break;
        }
        return new Fragment();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        try {
            switch (position) {
                case 0:
                    title = TAG_NAME_REG_NEW;
                    break;
                case 1:
                    title = TAG_NAME_REG_OLD;
                    break;
                case 2:
                    title = TAG_NAME_REG_NOT_APPROVAL;
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return title.toUpperCase();
    }
}