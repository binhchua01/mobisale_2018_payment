package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionBrochureModel;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PromotionAdTextAdapter  extends BaseAdapter{
	private List<PromotionBrochureModel> mList;	
	private Context mContext;
	
	public PromotionAdTextAdapter(Context context, List<PromotionBrochureModel> lst){
		this.mContext = context;
		this.mList = lst;	
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public PromotionBrochureModel getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		final PromotionBrochureModel item = mList.get(position);		
		if (view == null) {
			view = LayoutInflater.from(mContext).inflate(R.layout.row_promotion_ad_text_list, null);
		}
		if(item != null){
			TextView lblTitle = (TextView)view.findViewById(R.id.lbl_promotion_ad_title);
			lblTitle.setText(item.getTitle());
			/*TextView lblDesc = (TextView)view.findViewById(R.id.lbl_promotion_ad_desc);
			lblDesc.setText(item.getMediaUrl());*/
			/*WebView webView = (WebView)view.findViewById(R.id.webview);
			
			try {
				webView.loadRpCodeInfo(item.getMediaUrl(), "text/html; charset=utf-8","UTF-8");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}*/
		}		
		
		return view;
	}

}
