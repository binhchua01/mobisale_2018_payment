package isc.fpt.fsale.model;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table SBIMODEL.
 */
public class DepositValueModel {

    private String Title;
    private Integer Total;

    public DepositValueModel() {
    }

    public DepositValueModel(String Title,  Integer Total) {
        this.Title = Title;
        this.Total = Total;
       
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public Integer getTotal() {
        return Total;
    }

    public void setTotal(Integer Total) {
        this.Total = Total;
    }

}
