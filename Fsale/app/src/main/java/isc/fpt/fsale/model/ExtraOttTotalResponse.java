package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haulc3 on 22,July,2019
 */
public class ExtraOttTotalResponse implements Parcelable {
    @SerializedName("TotalAmount")
    @Expose
    private Integer totalAmount;
    @SerializedName("TotalAmountVAT")
    @Expose
    private Integer totalAmountVAT;

    protected ExtraOttTotalResponse(Parcel in) {
        if (in.readByte() == 0) {
            totalAmount = null;
        } else {
            totalAmount = in.readInt();
        }
        if (in.readByte() == 0) {
            totalAmountVAT = null;
        } else {
            totalAmountVAT = in.readInt();
        }
    }

    public static final Creator<ExtraOttTotalResponse> CREATOR = new Creator<ExtraOttTotalResponse>() {
        @Override
        public ExtraOttTotalResponse createFromParcel(Parcel in) {
            return new ExtraOttTotalResponse(in);
        }

        @Override
        public ExtraOttTotalResponse[] newArray(int size) {
            return new ExtraOttTotalResponse[size];
        }
    };

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalAmountVAT() {
        return totalAmountVAT;
    }

    public void setTotalAmountVAT(Integer totalAmountVAT) {
        this.totalAmountVAT = totalAmountVAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (totalAmount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalAmount);
        }
        if (totalAmountVAT == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalAmountVAT);
        }
    }
}
