package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.ViewPagerPromotionAdAdapter;
import isc.fpt.fsale.ui.fragment.FragmentPromotionAdImageList;
import isc.fpt.fsale.model.ZoomOutPageTransformer;
import isc.fpt.fsale.utils.Constants;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;

public class PromotionAdList extends BaseActivity {

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    public PromotionAdList() {
        super(R.string.lbl_screen_name_promotion_ad_list);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSlidingMenu().setSlidingEnabled(false);
        setContentView(R.layout.activity_promotion_ad_list);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        initViewPagerAdapter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void initViewPagerAdapter() {
        Bundle imageBundle = new Bundle(), videoBundle = new Bundle(), textBundle = new Bundle();
        imageBundle.putInt(FragmentPromotionAdImageList.ARG_MEDIA_TYPE, FragmentPromotionAdImageList.TAG_MEDIA_TYPE_IMAGE);
        videoBundle.putInt(FragmentPromotionAdImageList.ARG_MEDIA_TYPE, FragmentPromotionAdImageList.TAG_MEDIA_TYPE_VIDEO);
        textBundle.putInt(FragmentPromotionAdImageList.ARG_MEDIA_TYPE, FragmentPromotionAdImageList.TAG_MEDIA_TYPE_TEXT);
        mPagerAdapter = new ViewPagerPromotionAdAdapter(getSupportFragmentManager(), imageBundle, videoBundle, textBundle);
        mPager.setAdapter(mPagerAdapter);
    }
    
   /* private FragmentPromotionImageList getFragment(){
    	return (FragmentPromotionImageList)getSupportFragmentManager()
    			.findFragmentByTag("android:switcher:" + R.id.pager + ":" + mPager.getCurrentItem());
         if (mPager.getCurrentItem() == 0 && page != null) {
              ((FragmentPromotionImageList)page).updateList("new item");     
         } 
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}