package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ActivityModel;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ActivityListAdapter  extends BaseAdapter{
	private List<ActivityModel> mList;	
	private Context mContext;
	
	public ActivityListAdapter(Context context, List<ActivityModel> lst){
		this.mContext = context;
		this.mList = lst;	
	}
	
	public void clearAll(){
		this.mList.clear();
		this.notifyDataSetChanged();
	}
	
	public void setListData(List<ActivityModel> lst){
		this.mList.clear();
		this.mList.addAll(lst);
		this.notifyDataSetChanged();		
	}
	
	public void addAll(List<ActivityModel> lst){
		this.mList.addAll(lst);
		this.notifyDataSetChanged();		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ActivityModel item = mList.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_customer_care_activity_list_item, null);
		}
		if(item != null){
			TextView lblActivityTypeName = (TextView) convertView.findViewById(R.id.lbl_activity_type_name);
			lblActivityTypeName.setText(item.getActivity_type_name());
			
			TextView lblActivityName = (TextView) convertView.findViewById(R.id.lbl_activity_name);
			lblActivityName.setText(item.getActivity_name());
			
			TextView lblContract = (TextView) convertView.findViewById(R.id.lbl_contract);
			lblContract.setText(item.getContract());
			
			TextView lblFullName = (TextView) convertView.findViewById(R.id.lbl_full_name);
			lblFullName.setText(item.getCustomer_fullname());
			
			TextView lblPhone = (TextView) convertView.findViewById(R.id.lbl_phone);
			lblPhone.setText(item.getCustomer_phone());
			
			TextView lblAddress = (TextView) convertView.findViewById(R.id.lbl_address);
			lblAddress.setText(item.getCustomer_address());
			
			TextView lblID = (TextView) convertView.findViewById(R.id.lbl_id);
			lblID.setText(item.getActivity_code());
			
			TextView lblCreateDate = (TextView) convertView.findViewById(R.id.lbl_activity_date);
			lblCreateDate.setText(item.getActivity_date());
			
			TextView lblAssignDate = (TextView) convertView.findViewById(R.id.lbl_assign_date);
			lblAssignDate.setText(item.getAssign_date());
			
			TextView lblCreateBy = (TextView) convertView.findViewById(R.id.lbl_activity_by);
			lblCreateBy.setText(item.getActivity_by());
			
			/*TextView lblContent = (TextView) convertView.findViewById(R.id.lbl_content);
			lblContent.setText(item.getContent());*/
			
			TextView lblRemark= (TextView) convertView.findViewById(R.id.lbl_remark);
			if(item.getRemark() != null && !item.getRemark().equals("")){
				lblRemark.setText(item.getRemark());
			}else{
				lblRemark.setVisibility(View.GONE);
			}
			WebView wv = (WebView)convertView.findViewById(R.id.web_view);
			wv.loadData(item.getContent(), "text/html; charset=utf-8","UTF-8");
		}
		
		return convertView;
	}

}
