package isc.fpt.fsale.activity.ward;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetWardList;
import isc.fpt.fsale.model.Ward;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.SharedPref;

public class WardActivity extends BaseActivitySecond
        implements OnItemClickListener<Ward> {
    private RelativeLayout rltBack;
    private ImageView imgRefresh;
    private List<Ward> mList;
    private WardAdapter mAdapter;
    private String districtId = null;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (districtId != null) {
                    //get list ward
                    new GetWardList(WardActivity.this, districtId);
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_ward;
    }

    @Override
    protected void initView() {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_ward);
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        imgRefresh = (ImageView) findViewById(R.id.img_refresh);
        mList = new ArrayList<>();
        mAdapter = new WardAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);
        getDataIntent();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getString(Constants.DISTRICT_ID) != null) {
            districtId = bundle.getString(Constants.DISTRICT_ID);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<Ward> mList = SharedPref.getWardList(this, districtId);
        if (mList != null && mList.size() > 0) {
            this.mList = mList;
            mAdapter.notifyData(this.mList);
        } else {
            if (districtId != null) {
                //get list ward
                new GetWardList(this, districtId);
            }
        }
    }

    public void loadWardList() {
        List<Ward> mList = SharedPref.getWardList(this, districtId);
        if (mList != null && mList.size() > 0) {
            this.mList = mList;
            mAdapter.notifyData(this.mList);
        }
    }

    @Override
    public void onItemClick(Ward object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.WARD, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
