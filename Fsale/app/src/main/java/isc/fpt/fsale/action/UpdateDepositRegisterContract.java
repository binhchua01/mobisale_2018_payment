package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class UpdateDepositRegisterContract implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "DepositRegisterContract";
	public static String[] paramNames = new String[]{"UserName", "RegCode", "Total", "SBI","ImageSignature","PaidType"};
	private String userName = "";
	private RegistrationDetailModel mRegister;
	public UpdateDepositRegisterContract(Context context, RegistrationDetailModel register, int SBI, String ImageSignature, int payType) {
		mContext = context;
		this.mRegister = register;
		String message = "Đang cập nhật...";
		userName = ((MyApp)mContext.getApplicationContext()).getUserName();
		String[] paramValues = new String[]{userName, register.getRegCode(), String.valueOf(register.getTotal()), String.valueOf(SBI), ImageSignature, String.valueOf(payType)};

		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, UpdateDepositRegisterContract.this);
		service.execute();
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<UpdResultModel> lst = null;
			boolean isError = false;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(jsObj, UpdResultModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 if(lst != null && lst.size() > 0){
					 if(lst.get(0).getResultID() > 0){
						 AlertDialog.Builder builder = null;
						 Dialog dialog = null;
						 builder = new AlertDialog.Builder(mContext);
						 builder.setTitle("Thông báo")
						  .setMessage(lst.get(0).getResult()).setCancelable(false)		  
						  .setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
							   public void onClick(DialogInterface dialog, int id) {
							     try {
							    	 new GetRegistrationDetail(mContext, userName, mRegister.getID());
							     } catch (Exception e) {
							      // TODO Auto-generated catch block
							    	 Common.alertDialog(e.getMessage(), mContext);
							     }
							   }
						   });
						   dialog = builder.create();
						   dialog.show();
					 }else{
						 Common.alertDialog(lst.get(0).getResult(), mContext);
					 }
				 }else
					 Common.alertDialog("Không có dữ liệu trả về!", mContext);
			}
		} catch (JSONException e) {

			e.printStackTrace();
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}		
	
}
