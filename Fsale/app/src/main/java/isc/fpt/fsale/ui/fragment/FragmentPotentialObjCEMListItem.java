package isc.fpt.fsale.ui.fragment;


import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Outline;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPotentialObjCEMList;
import isc.fpt.fsale.action.GetPotentialObjDetail;
import isc.fpt.fsale.activity.CreatePotentialCEMObjActivity;
import isc.fpt.fsale.activity.ListPotentialObjActivity;

import android.support.v4.widget.SwipeRefreshLayout;

import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.PotentialObjCEMAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

//fragment 18006000 - DANH SÁCH KHÁCH HÀNG TIỀM NĂNG
public class FragmentPotentialObjCEMListItem extends Fragment implements OnItemClickListener, OnScrollListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_TITLE = "section_title";
    //private PotentialObjModel potentialObj = null;
    private ListPotentialObjActivity activity;
    // Phan trang
    private int mCurrentPage = 1, mTotalPage = 1, mSource = 1;
    private ListView mListView;
    private AsyncTask<Object, String, String> async;
    private WSObjectsModel<PotentialObjModel> wsObject = null;
    private GetPotentialObjCEMList action = null;
    private LinearLayout frmProgress = null;
    private ProgressBar pgBar = null;
    private TextView lblLoadding;
    private ImageView imgCreate, imgFind;
    private EditText txtAgentName;
    private Spinner spAgent;
    private SwipeRefreshLayout swipeContainer;

    /**
     * Returns a new instance of this fragment for the given section number.
     */
    public static FragmentPotentialObjCEMListItem newInstance(int sectionNumber, String sectionTitle) {
        FragmentPotentialObjCEMListItem fragment = new FragmentPotentialObjCEMListItem();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentPotentialObjCEMListItem() {

    }


    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_potential_obj_cem_list_item, container, false);
        activity = (ListPotentialObjActivity) getActivity();
        if (activity != null)
            activity.enableSlidingMenu(true);
        Common.setupUI(getActivity(), rootView);
        frmProgress = (LinearLayout) rootView.findViewById(R.id.frm_progress_bar);
        mListView = (ListView) rootView.findViewById(R.id.lv_object_cem);
        mListView.setOnItemClickListener(this);
        mListView.setOnScrollListener(this);
        pgBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        lblLoadding = (TextView) rootView.findViewById(R.id.lbl_loading);
        imgCreate = (ImageView) rootView.findViewById(R.id.img_create_potential_obj);
        swipeContainer = (SwipeRefreshLayout) rootView
                .findViewById(R.id.potential_obj_cem_list_item_container);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main));
        swipeContainer.setOnRefreshListener(this);
        if (Build.VERSION.SDK_INT >= 21) {//Lollipop
            imgCreate.setOutlineProvider(new ViewOutlineProvider() {

                @Override
                public void getOutline(View view, Outline outline) {
                    int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
                    outline.setOval(0, 0, diameter, diameter);
                }
            });
            imgCreate.setClipToOutline(true);
            StateListAnimator sla = AnimatorInflater.loadStateListAnimator(activity, R.drawable.selector_button_add_material_design);

            imgCreate.setStateListAnimator(sla);
        }
        imgCreate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (async != null && !async.isCancelled())
                    async.cancel(true);
                int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getApplicationContext());
                if (resultCode == ConnectionResult.SUCCESS) {
                    Intent intent = new Intent(activity, CreatePotentialCEMObjActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                } else if (resultCode == ConnectionResult.SERVICE_MISSING ||
                        resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED ||
                        resultCode == ConnectionResult.SERVICE_DISABLED) {
                    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), 1);
                    dialog.show();
                }

            }
        });
        // edit text nhập giá trị cần tìm kiếm
        txtAgentName = (EditText) rootView.findViewById(R.id.txt_agent_name);
        txtAgentName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtAgentName.setError(null);
            }
        });
        // spinner loại tìm kiếm
        spAgent = (Spinner) rootView.findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyValuePairModel item = (KeyValuePairModel) spAgent.getItemAtPosition(position);
                // chọn # Tất cả
                if (item != null && item.getID() > 0) {
                    txtAgentName.setEnabled(true);
                    txtAgentName.requestFocus();
                    // hiện bàn phím nhập
                    Common.showSoftKeyboard(activity);
                    if (item.getID() == 5) {
                        // chọn Thời gian kết thúc Dịch Vụ ISP
                        //cập nhật giá trị edit text Giá trị cần tìm thành VD: 31/12/2015
                        txtAgentName.setHint("VD: 31/12/2015");
                    } else
                        txtAgentName.setHint("Giá trị cần tìm");
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setHint("Giá trị cần tìm");
                    //txtAgentName.setEnabled(false);
                    txtAgentName.setError(null);
                }
                txtAgentName.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //nút tìm kiếm
        imgFind = (ImageView) rootView.findViewById(R.id.img_find);
        imgFind.setOnClickListener(new OnClickListener() {

            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View v) {
                // ẩn bàn phím
                Common.hideSoftKeyboard(activity);
                KeyValuePairModel agent = (KeyValuePairModel) spAgent.getSelectedItem();
                String agentName = txtAgentName.getText().toString().trim();
                //chọn option # Tất cả
                if (agent.getID() > 0) {
                    // kiểm tra giá Giá trị cần tìm khác ""
                    if (!agentName.equals("")) {
                        // chọn option Thời gian kết thúc Dịch Vụ ISP
                        if (agent.getID() == 5) {
                            try {
                                Date endTime = convertToDate(agentName);
                                if (endTime != null) {
                                    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
                                    agentName = format.format(endTime);
                                    getData(agent.getID(), agentName, 1);
                                } else {
                                    txtAgentName.setError("Ngày tháng không hợp lệ!");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else
                            getData(agent.getID(), agentName, 1);
                    } else {
                        txtAgentName.setError("Nhập giá trị cần tìm");
                    }
                } else {
                    getData(agent.getID(), agentName, 1);
                }
            }
        });
        initAgent();
        if (activity.getPotentialCEMList() == null) {
            getData(0, "", 1);
        } else {
            frmProgress.setVisibility(View.GONE);
            loadData(activity.getPotentialCEMList());
        }
        return rootView;
    }


    //
    @SuppressLint("SimpleDateFormat")
    private Date convertToDate(String EndDate) {
        Date result = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_VN);
            result = format.parse(EndDate);
        } catch (java.text.ParseException e) {

            e.printStackTrace();
        }
        return result;
    }

    //khởi tạo option loại tìm kiếm
    private void initAgent() {
        if (spAgent.getAdapter() == null) {
            ArrayList<KeyValuePairModel> lstObjType = new ArrayList<>();
            lstObjType.add(new KeyValuePairModel(0, "Tất cả"));
            lstObjType.add(new KeyValuePairModel(2, "Tên KH"));
            lstObjType.add(new KeyValuePairModel(3, "Số điện thoại"));
            lstObjType.add(new KeyValuePairModel(4, "Địa chỉ"));
            lstObjType.add(new KeyValuePairModel(5, "Thời gian kết thúc Dịch Vụ ISP"));
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(activity, R.layout.my_spinner_style, lstObjType, Gravity.CENTER);
            spAgent.setAdapter(adapter);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    // sự kiện nhấn 1 item list view danh sách khtn
    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        try {
            Common.hideSoftKeyboard(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PotentialObjModel selectedItem = (PotentialObjModel) parentView.getItemAtPosition(position);
        if (selectedItem != null) {
            String userName = ((MyApp) activity.getApplication()).getUserName();
            String supporter = selectedItem.getSupporter() == null ? Constants.USERNAME : selectedItem.getSupporter();
            // kết nối api GetPotentialObjDetail lấy thông tin chi tiết khtn
            new GetPotentialObjDetail(activity, userName, selectedItem.getID(), supporter);
        }
    }

    // function api lấy danh sách khtn 18006000(option tìm kiếm, giá trị tìm kiếm, trang tìm kiếm)
    private void getData(int agent, String agentName, int page) {
        String userName = ((MyApp) activity.getApplication()).getUserName();
        action = new GetPotentialObjCEMList(activity, userName, agent, agentName, page, mSource);
        frmProgress.setVisibility(View.VISIBLE);
        pgBar.setVisibility(View.VISIBLE);
        async = new AsyncTask<Object, String, String>() {

            @Override
            protected void onProgressUpdate(String... values) {
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(String json) {
                pgBar.setVisibility(View.GONE);
                wsObject = action.getData(json);
                if (wsObject != null) {
                    if (wsObject.getErrorCode() == 0) {
						/*if(wsObject.getListObject().size() > 0){
							frmProgress.setVisibility(View.GONE);
							LoadData(wsObject.getListObject());
						}else{
							lblLoadding.setText("Không có dữ liệu!");
							LoadData(wsObject.getListObject());
						}*/
                        if (mCurrentPage <= 1 && wsObject.getListObject().size() == 0) {
                            lblLoadding.setText("Không có dữ liệu!");
                        } else {
                            frmProgress.setVisibility(View.GONE);
                            loadData(wsObject.getListObject());
                        }
                    } else {
                        lblLoadding.setText(wsObject.getError());
                    }
                } else {
                    lblLoadding.setText(json);
                }
            }

            ;

            @Override
            protected String doInBackground(Object... params) {
                String json = "";
                try {
                    json = Services.postJsonAutoHeader(action.TAG_METHOD_NAME, action.getParamName(), action.getParamValue(), activity);
                } catch (Exception ex) {

                    json = "Error :" + ex.getMessage();
                }
                return json;
            }
        }.execute(null, null, null);

    }

    //hàm callback cập nhật danh sách khtn
    private void loadData(List<PotentialObjModel> list) {
        try {
            if (list != null && list.size() > 0) {
                mTotalPage = list.get(list.size() - 1).getTotalPage();
                mCurrentPage = list.get(list.size() - 1).getCurrentPage();
                if (mListView.getAdapter() == null || mListView.getAdapter().getCount() <= 0) {
                    activity.setPotentialCEMList(list);
                    PotentialObjCEMAdapter adapter = new PotentialObjCEMAdapter(activity, list);
                    mListView.setAdapter(adapter);
                } else {
                    try {
                        if (mCurrentPage > 1) {
                            ((PotentialObjCEMAdapter) mListView.getAdapter()).addAll(list);
                            activity.setPotentialCEMList(((PotentialObjCEMAdapter) mListView.getAdapter()).getListData());
                        } else {
                            ((PotentialObjCEMAdapter) mListView.getAdapter()).setListData(list);
                            activity.setPotentialCEMList(list);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    ((PotentialObjCEMAdapter) mListView.getAdapter()).clearAll();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Common.alertDialog("Không có dữ liệu!", activity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int currentVisibleItemCount;
    private int currentScrollState;
    private int currentFirstVisibleItem;
    private int totalItem;

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItem = totalItemCount;

    }

    private void isScrollCompleted() {
        if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                && this.currentScrollState == SCROLL_STATE_IDLE && currentVisibleItemCount > 0) {
            if (mCurrentPage < mTotalPage) {
                mCurrentPage++;
                int agent = 0;
                String agentName = "";
                if (spAgent.getAdapter() != null) {
                    agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
                    agentName = txtAgentName.getText().toString();
                }
                getData(agent, agentName, mCurrentPage);
            }
        }
    }

    @Override
    public void onRefresh() {
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                getData(0, "", 1);
                swipeContainer.setRefreshing(false);
            }
        });
    }
}
