package isc.fpt.fsale.action.Banking;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.action.upsell.GetAdvisoryResultUpgrade;
import isc.fpt.fsale.callback.banking.ListBankingCallback;
import isc.fpt.fsale.model.Banking.ListBankingModel;
import isc.fpt.fsale.model.upsell.response.AdivisoryResultUpgradeModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetListBanking implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    ListBankingCallback mCallBack;

    public GetListBanking(Context mContext, String[] value, ListBankingCallback mCallBack) {
        this.mContext = mContext;
        this.mCallBack = mCallBack;
        String[] params = new String[]{"SaleName"};
        String message = "Đang lấy danh sách ngân hàng ...";
        String GET_LIST_BANKING= "GetListBanking";
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_BANKING, params, value, Services.JSON_POST, message, GetListBanking.this);
        service.execute();
    }


    @Override
    public void onTaskComplete(String result) throws JSONException {
        try {
            List<ListBankingModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ListBankingModel.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        mCallBack.onGetBankingSuccess(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}


