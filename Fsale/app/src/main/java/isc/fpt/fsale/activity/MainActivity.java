package isc.fpt.fsale.activity;

import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Outline;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewOutlineProvider;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.SlidingMenu.OnOpenListener;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListDeploymentProgress;
import isc.fpt.fsale.action.GetListPrechecklist;
import isc.fpt.fsale.action.GetListRegistration;
import isc.fpt.fsale.action.GetSaleCOD;
import isc.fpt.fsale.activity.conllection_bank.ConllectionListBank;
import isc.fpt.fsale.activity.upsell.UpSellActivity;
import isc.fpt.fsale.ui.choose_service.ChooseServiceActivity;
import isc.fpt.fsale.ui.fragment.SBIManageDialog;
import isc.fpt.fsale.ui.fragment.SupportDialog;
import isc.fpt.fsale.map.activity.MapSearchTDActivity;
import isc.fpt.fsale.model.SBIModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

//màn hình trang chủ
public class MainActivity extends BaseActivity {
    private Button tileAbout;
    private Button tileSale;
    private Button tileSupportDev;
    private Button tileSupportCus;
    private Button tileReport;
    private TextView txtUserName;
    private FragmentManager fm;
    private static PopupWindow popupWindow;
    private boolean isPopup = false;
    private TextView txtSBIPlus, txtSBILess;
    AsyncTask<Void, Void, Void> mRegisterTask;
    private ImageButton imgCreate;

    public MainActivity() {
        super(R.string.title_mainpage);
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_main_activity));
        setContentView(R.layout.activity_main);
        fm = this.getSupportFragmentManager();
        initView();
        initViewEvent();
        setUpPopupWindow();
        setDataView();
    }

    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void setDataView() {
        txtUserName.setText(Constants.USERNAME);
    }

    private void setUpPopupWindow() {
        popupWindow = new PopupWindow(this);
        popupWindow.setBackgroundDrawable(getResources().getDrawable(R.color.main_color_blue_new));
        Constants.SLIDING_MENU.setOnOpenListener(new OnOpenListener() {

            @Override
            public void onOpen() {
                if (popupWindow != null)
                    popupWindow.dismiss();
            }
        });
    }

    private void initViewEvent() {
        tileAbout.setOnClickListener(onClickListener);
        tileSale.setOnClickListener(onClickListener);
        tileSupportDev.setOnClickListener(onClickListener);
        tileSupportCus.setOnClickListener(onClickListener);
        tileReport.setOnClickListener(onClickListener);
        try {
            if (Build.VERSION.SDK_INT >= 21) {// Lollipop
                try {
                    imgCreate.setOutlineProvider(new ViewOutlineProvider() {
                        @Override
                        public void getOutline(View view, Outline outline) {
                            int diameter = getResources()
                                    .getDimensionPixelSize(R.dimen.diameter);
                            outline.setOval(0, 0, diameter, diameter);
                        }
                    });
                    StateListAnimator sla = AnimatorInflater.loadStateListAnimator(
                            MainActivity.this,
                            R.drawable.selector_button_add_material_design
                    );
                    imgCreate.setStateListAnimator(sla);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                imgCreate.setImageResource(android.R.color.transparent);
            }
            imgCreate.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, ChooseServiceActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        //control HỖ TRỢ MOBISALE
        tileAbout = (Button) this.findViewById(R.id.btn_support);
        // control USER đăng nhập
        txtUserName = (TextView) this.findViewById(R.id.txt_user_login);
        //control BÁN HÀNG
        tileSale = (Button) this.findViewById(R.id.tile_sale);
        //control HỖ TRỢ TRIỂN KHAI
        tileSupportDev = (Button) this.findViewById(R.id.tile_support_develop);
        //control CHĂM SÓC KHÁCH HÀNG
        tileSupportCus = (Button) this.findViewById(R.id.tile_customer_support);
        //control BÁO CÁO
        tileReport = (Button) this.findViewById(R.id.tile_report);
        imgCreate = (ImageButton) findViewById(R.id.img_create);
    }

    public void getSBIList() {
        int Status = -1;// lấy SBI hết hạn và còn hạn sd
        //kết nối API lấy danh sách SBI
//        new GetSBIList(this, fm, null, Status);
    }

    //khởi tạo sự kiện nhấn các control
    private OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isPopup) {
                // kiểm tra popupWindow trước khi đóng
                if (popupWindow != null)
                    popupWindow.dismiss();
                isPopup = false;
            }
            int id = v.getId();
            if (id == R.id.btn_support) {
                //hiển thị màn hình HỖ TRỢ
                SupportDialog dialog = new SupportDialog(MainActivity.this);
                Common.showFragmentDialog(fm, dialog, "fragment_about_us_dialog");
            } else if (id == R.id.tile_sale) {
                if (!isPopup)
                    showPopUpMenuSale(v);
                else
                    isPopup = false;
            } else if (id == R.id.tile_support_develop) {
                if (isPopup)
                    isPopup = false;
                showPopUpMenuCustomerCare(v);
            } else if (id == R.id.tile_customer_support) {
                if (!isPopup)
                    showPopUpMenuPrechecklist(v);
                else
                    isPopup = false;
            } else if (id == R.id.tile_report) {
                if (!isPopup)
                    showPopUpMenuManagerList(v);
                else
                    isPopup = false;

            }
//            else if (id == R.id.btn_quit_mainpage) {
//                Common.Exit(MainActivity.this, MainActivity.this);
//            } else if (id == R.id.btn_logout_mainpage) {
//                Common.Logout(MainActivity.this);
//            }
        }
    };

    // ============================================ Add SBI Menu to Action bar
    // ============================================
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_sbi, menu);
        //Set layout for ActionBar Menu
        LayoutInflater baseInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = baseInflater.inflate(R.layout.menu_item_sbi_status, null);
        menu.findItem(R.id.menu_flags_sbi_less).setActionView(v);
        //Set item for ActionBar Menu
        //layout chứa SBI
        LinearLayout frm_sbi = (LinearLayout) v.findViewById(R.id.frm_sbi_main_menu);
        //control SBI xanh
        txtSBILess = (TextView) v.findViewById(R.id.txt_sbi_less);
        //control SBI đỏ
        txtSBIPlus = (TextView) v.findViewById(R.id.txt_sbi_plus);
        //kết nối API lấy danh sách SBI
//        getSBIList();
        frm_sbi.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SBIManageDialog sbiDialog = new SBIManageDialog(MainActivity.this);
                Common.showFragmentDialog(fm, sbiDialog, "fragment_manage_sbi_dialog");
            }
        });
        //control notification
        ImageView imgNotification = (ImageView) v.findViewById(R.id.img_notification);
        //khởi tạo sự kiện nhấn control notification
        imgNotification.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //mở màn hình DANH SÁCH THÔNG BÁO
                Intent intent = new Intent(MainActivity.this, ListNotificationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        return true;
    }

    //call back tải danh sách SBI
    public void loadSBIStatus(ArrayList<SBIModel> lstSBI) {
        try {
            int sbiLessCount = 0, sbiPlusCount = 0;
            for (int i = 0; i < lstSBI.size(); i++) {
                if (lstSBI.get(i).getStatus() > 0)
                    sbiPlusCount++;
                else
                    sbiLessCount++;
            }
            txtSBILess.setText(String.valueOf(sbiLessCount));
            txtSBIPlus.setText(String.valueOf(sbiPlusCount));
            txtSBILess.setText((sbiLessCount != 0 ? String
                    .valueOf(sbiLessCount) : "0"));
            txtSBIPlus.setText((sbiPlusCount != 0 ? String
                    .valueOf(sbiPlusCount) : "0"));
        } catch (Exception e) {
            Toast.makeText(this, "ERROR!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
//        Common.Logout(MainActivity.this);
        Common.quitApp(this);
    }

    @Override
    public boolean onOptionsItemSelected(
            MenuItem item) {
        if (popupWindow != null)
            popupWindow.dismiss();
        if ((item.getItemId() == android.R.id.home)) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else if (item.getItemId() == R.id.menu_flags_sbi_less) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // hiển thị menu con BÁN HÀNG
    public void showPopUpMenuSale(View v) {
        //Kiem tra neu activity bi huỷ thi ko chay
        if (!Common.checkLifeActivity(this)) {
            return;
        }
        isPopup = true;
        // Inflate the popup_layout
        final LinearLayout viewGroup = (LinearLayout) this
                .findViewById(R.id.mainpage_popup_sale);
        LayoutInflater layoutInflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.mainpage_popup_sale,
                viewGroup);
        // Creating the PopupWindow
        popupWindow.setContentView(layout);
        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        // Some offset to align the popup a bit to the right, and a bit down
        // int OFFSET_X = 30;
        // Displaying the popup at the specified location, + offsets.
        int[] location = new int[2];
        // Get the x, y location and store it in the location[] array
        v.getLocationOnScreen(location);
        // Initialize the Point with x, and y positions
        final Point p = new Point();
        p.x = location[0];
        p.y = location[1];
        // show popup window
        popupWindow.showAtLocation(getWindow().getDecorView(), Gravity.TOP | Gravity.LEFT, p.x, p.y);
        //control QUẢN LÝ TTKH
        TextView listRegistration = (TextView) layout
                .findViewById(R.id.popup_item_1);
        listRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //mở màn hình tạo tạo thông tin khách hàng
                Intent intent = new Intent(MainActivity.this, RegistrationListNewActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        //control TẠO TTKH
        TextView createRegistration = (TextView) layout.findViewById(R.id.popup_item_2);
        createRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ChooseServiceActivity.class));
                popupWindow.dismiss();
            }
        });
        //control TẬP ĐIỂM
        TextView onetv = (TextView) layout.findViewById(R.id.popup_item_iptv);
        onetv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    int resultCode = GooglePlayServicesUtil
                            .isGooglePlayServicesAvailable(MainActivity.this);
                    if (resultCode == ConnectionResult.SUCCESS) {
                        Intent intent = new Intent(MainActivity.this, MapSearchTDActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (resultCode == ConnectionResult.SERVICE_MISSING
                            || resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED
                            || resultCode == ConnectionResult.SERVICE_DISABLED) {
                        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                                resultCode, MainActivity.this, 1);
                        dialog.show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                popupWindow.dismiss();
            }
        });
        //control BÁN THÊM DỊCH VỤ
        TextView lblSearchObject = (TextView) layout.findViewById(R.id.popup_item_list_object);
        lblSearchObject.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainActivity.this, ListObjectSearchActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                popupWindow.dismiss();
            }
        });

        //control XÁC NHẬN THIẾT BỊ
        TextView lblReceiveDevice = (TextView) layout.findViewById(R.id.popup_item_receive_device);
        lblReceiveDevice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ReceiveDeviceActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        LinearLayout frmPotentialList = (LinearLayout) layout.findViewById(R.id.frm_potential_list);
        if (((MyApp) getApplication()).HCM_VERSION) {
            frmPotentialList.setVisibility(View.VISIBLE);
        } else {
            frmPotentialList.setVisibility(View.GONE);
        }

        //control KHÁCH HÀNG TIỀM NĂNG
        TextView lblPotentailObjList = (TextView) layout.findViewById(R.id.popup_item_potentail_obj_list);
        lblPotentailObjList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListPotentialObjActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        //control DANH SÁCH TTKH/HỢP ĐỒNG CHƯA DUYỆT
        TextView lblListCreateObject = (TextView) layout.findViewById(R.id.popup_item_list_create_object);
        lblListCreateObject.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Lấy DS các PĐK đã lên HĐ tự động
                new GetListRegistration(
                        MainActivity.this,
                        Constants.USERNAME,
                        1,
                        1,
                        "",
                        false
                );
                popupWindow.dismiss();
            }
        });
        //control bán COD
        LinearLayout viewSaleCOD = layout.findViewById(R.id.view_sale_cod);
        if (Constants.IS_SALE_COD) {
            viewSaleCOD.setVisibility(View.VISIBLE);
            TextView lblSaleCOD = (TextView) layout.findViewById(R.id.popup_item_sale_cod);
            lblSaleCOD.setOnClickListener(v1 -> {
                // Lấy COD
                new GetSaleCOD(MainActivity.this, Constants.USERNAME);
                popupWindow.dismiss();
            });
        }
        else {
            viewSaleCOD.setVisibility(View.GONE);
        }
        //control CHƯƠNG TRÌNH KHUYẾN MÃI
        TextView lblPromotionAd = (TextView) layout.findViewById(R.id.popup_item_promotion_ad);
        lblPromotionAd.setOnClickListener(new OnClickListener() {

            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PromotionAdList.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        //control NGÂN HÀNG THU HỘ
        TextView lblPaymentBanking = (TextView) layout.findViewById(R.id.popup_item_payment_banking);
        lblPaymentBanking.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ConllectionListBank.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
    }

    // hiện menu con CHĂM SÓC KHÁCH HÀNG
    public void showPopUpMenuPrechecklist(View v) {
        if (!Common.checkLifeActivity(this)) {
            return;
        }
        isPopup = true;
        // Inflate the popup_layout
        LinearLayout viewGroup = (LinearLayout) this
                .findViewById(R.id.mainpage_popup_prechecklist);
        LayoutInflater layoutInflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(
                R.layout.mainpage_popup_prechecklist, viewGroup);
        // Creating the PopupWindow
        popupWindow.setContentView(layout);
        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        int[] location = new int[2];
        v.getLocationOnScreen(location);
        final Point p = new Point();
        p.x = location[0];
        p.y = location[1];
        popupWindow.showAtLocation(getWindow().getDecorView(), Gravity.TOP | Gravity.LEFT, p.x, p.y);
        // handle popup menu item clicked event
        TextView listRegistration = (TextView) layout
                .findViewById(R.id.popup_item_1);
        listRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // add by GiauTQ 30-04-2014
                String[] arrayParrams = new String[]{Constants.USERNAME, "1"};
                new GetListPrechecklist(MainActivity.this, arrayParrams);
                popupWindow.dismiss();
            }
        });
        TextView createRegistration = (TextView) layout
                .findViewById(R.id.popup_item_2);
        createRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CreatePreChecklistActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        LinearLayout frmCusCare = (LinearLayout) layout.findViewById(R.id.frm_customer_care);
        if (((MyApp) getApplication()).HCM_VERSION) {
            frmCusCare.setVisibility(View.VISIBLE);
        } else {
            frmCusCare.setVisibility(View.GONE);
        }
        TextView lblCustomerCare = (TextView) layout.findViewById(R.id.popup_item_customer_care);
        lblCustomerCare.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListCustomerCareActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        TextView lblCustomerActivity = (TextView) layout.findViewById(R.id.popup_item_customer_activity);
        lblCustomerActivity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivityCustomerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

        //nhannh26--UpSell--27/5/2020
        TextView lblUpSellActivity = (TextView) layout.findViewById(R.id.popup_item_upsell);
        lblUpSellActivity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UpSellActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

    }

    //hiện menu con BÁO CÁO
    public void showPopUpMenuManagerList(View v) {
        if (!Common.checkLifeActivity(this)) {
            return;
        }
        isPopup = true;
        LinearLayout viewGroup = (LinearLayout) this
                .findViewById(R.id.mainpage_popup_manager);
        LayoutInflater layoutInflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.mainpage_popup_manager,
                viewGroup);
        popupWindow.setContentView(layout);
        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        int[] location = new int[2];
        v.getLocationOnScreen(location);
        final Point p = new Point();
        p.x = location[0];
        p.y = location[1];
        popupWindow.showAtLocation(getWindow().getDecorView(), Gravity.TOP | Gravity.LEFT, p.x, p.y);
        //control KHẢO SÁT
        TextView listRegistration = (TextView) layout.findViewById(R.id.popup_item_1);
        listRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //hiển thị màn hình BÁO CÁO KHẢO SÁT
                Intent intent = new Intent(MainActivity.this, ListReportSurveyManagerTotalActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        TextView createRegistration = (TextView) layout
                .findViewById(R.id.popup_item_2);
        createRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ReportSubscriberGrowthForManagerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        TextView reportPreCheckList = (TextView) layout
                .findViewById(R.id.popup_report_preckecklist);
        reportPreCheckList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListReportPrecheckListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        TextView reportSalary = (TextView) layout
                .findViewById(R.id.popup_report_salary);
        reportSalary.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        ListReportSalaryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        TextView reportRegister = (TextView) layout
                .findViewById(R.id.popup_report_register);
        reportRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListReportRegistrationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        // Bao cao SBI
        TextView reportSBI = (TextView) layout.findViewById(R.id.popup_report_sbi);
        reportSBI.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListReportSBITotalActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        TextView reportPotentialObj = (TextView) layout.findViewById(R.id.popup_report_potential_obj);
        reportPotentialObj.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListReportPotentialObjTotalActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        TextView reportCreateObject = (TextView) layout
                .findViewById(R.id.popup_report_create_object);
        reportCreateObject.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListReportCreateObjectTotalActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        TextView reportPayTV = (TextView) layout
                .findViewById(R.id.popup_report_pay_tv);
        reportPayTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListReportPayTVActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

        TextView reportSurveyPotential = (TextView) layout.findViewById(R.id.popup_report_potential_survey);
        reportSurveyPotential.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListReportPotentialSurveyActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        /* BAO CAO KH ROI MANG */
        TextView lblReportSuspendCustomer = (TextView) layout
                .findViewById(R.id.popup_report_suspend_customer);
        lblReportSuspendCustomer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListReportSuspendCustomerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        // Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }

        try {
            GCMRegistrar.onDestroy(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    //hiện menu con HỖ TRỢ TRIỂN KHAI
    public void showPopUpMenuCustomerCare(View v) {
        if (!Common.checkLifeActivity(this)) {
            return;
        }
        isPopup = true;
        // Inflate the popup_layout
        LinearLayout viewGroup = (LinearLayout) this
                .findViewById(R.id.mainpage_popup_customer_care);
        LayoutInflater layoutInflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(
                R.layout.mainpage_popup_support_deployment, viewGroup);
        // Creating the PopupWindow
        popupWindow.setContentView(layout);
        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        int[] location = new int[2];
        v.getLocationOnScreen(location);
        final Point p = new Point();
        p.x = location[0];
        p.y = location[1];
        popupWindow.showAtLocation(getWindow().getDecorView(), Gravity.TOP | Gravity.LEFT, p.x, p.y);
        //control HỖ TRỢ TRIỂN KHAI
        TextView lblSupportDeployment = (TextView) layout.findViewById(R.id.popup_item_support_deployment);
        lblSupportDeployment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = ((MyApp) MainActivity.this.getApplication()).getUserName();
                // kết nối API lấy HỖ TRỢ TRIỂN KHAI
                new GetListDeploymentProgress(MainActivity.this, userName, "0", "0",
                        "1", false);
            }
        });
        //control PHIẾU THI CÔNG TRẢ VỀ
        TextView lblPTCReturnList = (TextView) layout
                .findViewById(R.id.popup_item_ptc_return_list);
        lblPTCReturnList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //hiển thị màn hình PHIẾU THI CÔNG TRẢ VỀ
                Intent intent = new Intent(MainActivity.this, ListPTCReturnActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
    }
}
