package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by HCM.TUANTT14 on 10/23/2018.
 */

public class UploadMultipartResult implements Parcelable {
    private String Status, Description;
    private int Amount, Code;
    private List<IdImageDocumentResult> Data;

    public UploadMultipartResult() {

    }

    public UploadMultipartResult(String status, String description, int amount, int code, List<IdImageDocumentResult> data) {
        Status = status;
        Description = description;
        Amount = amount;
        Code = code;
        Data = data;
    }

    protected UploadMultipartResult(Parcel in) {
        Status = in.readString();
        Description = in.readString();
        Amount = in.readInt();
        Code = in.readInt();
        Data = in.createTypedArrayList(IdImageDocumentResult.CREATOR);
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int amount) {
        Amount = amount;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public List<IdImageDocumentResult> getData() {
        return Data;
    }

    public void setData(List<IdImageDocumentResult> data) {
        Data = data;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Status);
        dest.writeString(Description);
        dest.writeInt(Amount);
        dest.writeInt(Code);
        dest.writeTypedList(Data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UploadMultipartResult> CREATOR = new Creator<UploadMultipartResult>() {
        @Override
        public UploadMultipartResult createFromParcel(Parcel in) {
            return new UploadMultipartResult(in);
        }

        @Override
        public UploadMultipartResult[] newArray(int size) {
            return new UploadMultipartResult[size];
        }
    };
}
