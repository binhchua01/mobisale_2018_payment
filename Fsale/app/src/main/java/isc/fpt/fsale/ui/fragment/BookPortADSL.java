package isc.fpt.fsale.ui.fragment;

import isc.fpt.fsale.action.GetIPV4Action;
import isc.fpt.fsale.action.GetPortFreeADSLAction;
import isc.fpt.fsale.action.InsertBookPortADSL;
import isc.fpt.fsale.action.RecoverRegistrationAction;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import isc.fpt.fsale.utils.MyApp.TrackerName;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.danh32.fontify.Button;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class BookPortADSL extends DialogFragment {

    private ImageButton btnClose;
    private Button btnBookPort, btnRecoverRegistration;
    public TextView txt_NumberCard;
    public String params;
    private Context mContext;
    private KeyValuePairAdapter adapter;
    private Spinner SpTypeService, SpDistrist, SpDistristMaster, SpPOP, SPPortFree;
    private int iTypeService;
    private RowBookPortModel selectedItemParams;
    private String SOPHIEUDK, ID;
    private String TYPESERVICE = "1";
    private ImageView imgReload;
    private String sTDName;
    private String IDPORTTD = "0";
    private String[] paramsValue;

    // Port khả dụng
    private TextView txtPortAvailability, lblTDName, lblRegCode;
    private RegistrationDetailModel modelRegister;
    private Location locationCurrentDevice;
    private Location locationMarker;
    private String latlngDevice = "";
    private String latlngMarker = "";

    public BookPortADSL() {
    }

    @SuppressLint("ValidFragment")
    public BookPortADSL(Context _mContext, RowBookPortModel _selectedItem, String _SOPHIEUDK, String _ID, RegistrationDetailModel modelRegister) {
        mContext = _mContext;
        this.selectedItemParams = _selectedItem;
        this.SOPHIEUDK = _SOPHIEUDK;
        this.ID = _ID;
        this.modelRegister = modelRegister;
    }

    @SuppressLint("ValidFragment")
    public BookPortADSL(Context _mContext, RowBookPortModel _selectedItem, String _SOPHIEUDK, String _ID,
                        RegistrationDetailModel modelRegister, Location locationCurrentDevice, Location locationMarker) {
        mContext = _mContext;
        this.selectedItemParams = _selectedItem;
        this.SOPHIEUDK = _SOPHIEUDK;
        this.ID = _ID;
        this.modelRegister = modelRegister;
        this.locationCurrentDevice = locationCurrentDevice;
        this.locationMarker = locationMarker;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View view = inflater.inflate(R.layout.book_port_adsl, container);
        //mContext=inflater.getContext();
        btnClose = (ImageButton) view.findViewById(R.id.btn_close);
        btnBookPort = (Button) view.findViewById(R.id.btn_book_port);
        mContext = getActivity();
        // Thu hồi phiếu đăng ký
        btnRecoverRegistration = (Button) view.findViewById(R.id.btn_recover_registration);
        btnRecoverRegistration.setVisibility(View.GONE);
        // Neu tap diem da duoc book va ODCCable khac ten tap diem ==> hien nut thu hoi
        try {
            //if(this.modelRegister.getODCCableType().equals("") && !this.modelRegister.getTDName().equals("") )
            if ((this.modelRegister.getTDName() != null && !this.modelRegister.getTDName().equals("")))
                btnRecoverRegistration.setVisibility(View.VISIBLE);
        } catch (Exception e) {

            //Common.alertDialog("BookPortADSL: " + e.getMessage(), mContext);
        }

        btnRecoverRegistration.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                AlertDialog.Builder builder = null;
                Dialog dialog = null;
                builder = new AlertDialog.Builder(mContext);
                builder.setMessage(mContext.getResources().getString(R.string.msg_confirm_recover_registration)).setCancelable(false).setPositiveButton("Có",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                recovertyPort();
                            }
                        }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.cancel();
                    }
                });
                dialog = builder.create();
                dialog.show();

            }
        });

        // BookPortModel=new TestThien(Listmodel.get(0).GetBranchID(), Listmodel.get(0).GetLocationID(), Listmodel.get(0).GetLocationName(),Listmodel.get(0).GetNameBranch(),Listmodel.get(0).GetPopName());

        lblRegCode = (TextView) view.findViewById(R.id.lbl_reg_code);
        txt_NumberCard = (TextView) view.findViewById(R.id.txt_bookport_number_card);
        SpTypeService = (Spinner) view.findViewById(R.id.sp_bookport_types_services);
        SpDistrist = (Spinner) view.findViewById(R.id.sp_bookport_regions);
        SpDistristMaster = (Spinner) view.findViewById(R.id.sp_bookport_branch);
        SpPOP = (Spinner) view.findViewById(R.id.sp_bookport_pop);
        lblTDName = (TextView) view.findViewById(R.id.lbl_td_name);
        SPPortFree = (Spinner) view.findViewById(R.id.sp_bookport_port_free);
        imgReload = (ImageView) view.findViewById(R.id.img_reload);
        // Port khả dụng
        txtPortAvailability = (TextView) view.findViewById(R.id.txt_port_availability);

        //sp_bookport_port_free
        //String []ChuoiParams=params.split(";");

        //txtLocationName.setText(ChuoiParams[0]);
        //txtNameBranch.setText(ChuoiParams[1]);

        ShowData();

        SpTypeService.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                sTDName = selectedItemParams.getTDName();
                TYPESERVICE = String.valueOf(selectedItem.getID());
                iTypeService = selectedItem.getID();
                LoadPort();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        SPPortFree.setEnabled(false);
        SPPortFree.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                IDPORTTD = String.valueOf(selectedItem.getID());
                txt_NumberCard.setText(selectedItem.getDescription());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        imgReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadPort();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });


        btnBookPort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String[] params = new String[]{"LocationID","BranchID","UserName","IDTD","TypeSevice","RegCode","CardNumber","IDportTD","IP"};

                String LocationID = String.valueOf(selectedItemParams.getLocationID());
                String BranchID = String.valueOf(selectedItemParams.getBranchID());
                String UserName = Constants.USERNAME;
                String IDTD = String.valueOf(selectedItemParams.getTDID());
                GetIPV4Action NewIP = new GetIPV4Action();
                String IP = NewIP.GetIP();
                String TDName = selectedItemParams.getTDName();
                String Latlng = selectedItemParams.getLatlngPort();
                if (locationCurrentDevice != null)
                    latlngDevice = locationCurrentDevice.getLatitude() + "," + locationCurrentDevice.getLongitude();
                if (locationMarker != null)
                    latlngMarker = locationMarker.getLatitude() + "," + locationMarker.getLongitude();

                // Kiểm tra port
                if (IDPORTTD == null || IDPORTTD.equals("-1")) {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_port_empty), mContext);
                    return;
                }

                // Kiểm tra port khả dụng
                int iPortAvailability = 0;
                try {
                    iPortAvailability = Integer.parseInt(txtPortAvailability.getText().toString().trim());
                } catch (Exception ex) {

                    iPortAvailability = 0;
                }

                // Nếu port khả dụng = 0 ==> không cho bookport
                if (iPortAvailability == 0) {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_port_availability_zero), mContext);
                    return;
                }

                // Kiểm tra số thẻ
                int iCardNumber = 0;
                try {
                    iCardNumber = Integer.parseInt(txt_NumberCard.getText().toString().trim());
                } catch (Exception ex) {

                    iCardNumber = 0;
                }
                if (iCardNumber <= 0) {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_number_card_adsl_invalid), mContext);
                    txt_NumberCard.setError(mContext.getResources().getString(R.string.msg_number_card_adsl_invalid));
                    txt_NumberCard.setFocusable(true);
                    txt_NumberCard.requestFocus();
                    return;
                }

                //String result=LocationID+";"+BranchID+";"+UserName+";"+IDTD+";"+TYPESERVICE+";"+SOPHIEUDK+";"+CardNumber+";"+IDPORTTD+";"+IP;

                paramsValue = new String[]{LocationID, BranchID, UserName, IDTD, TYPESERVICE, SOPHIEUDK,
                        String.valueOf(iCardNumber), IDPORTTD, IP, TDName, Latlng, latlngDevice, latlngMarker};

                //Common.alertDialog(result, mContext);


                AlertDialog.Builder builder = null;
                Dialog dialog = null;
                builder = new AlertDialog.Builder(mContext);
                builder.setMessage(mContext.getResources().getString(R.string.msg_confirm_update)).setCancelable(false).setPositiveButton("Có",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                new InsertBookPortADSL(mContext, paramsValue, ID);
                                onClickTracker("Book-Port ADSL");
                            }
                        }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.cancel();
                    }
                });
                dialog = builder.create();
                dialog.show();


            }
        });


        return view;
    }

    public void ShowData() {
        if (selectedItemParams != null) {
            //Vung mien
            ArrayList<KeyValuePairModel> ListDistrist = new ArrayList<KeyValuePairModel>();
            ListDistrist.add(new KeyValuePairModel(1, selectedItemParams.GetLocationName()));
            adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListDistrist, Gravity.LEFT);
            SpDistrist.setAdapter(adapter);

            //chi nhanh quan ly
            ArrayList<KeyValuePairModel> ListDistristMaster = new ArrayList<KeyValuePairModel>();
            ListDistristMaster.add(new KeyValuePairModel(1, selectedItemParams.GetNameBranch()));
            adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListDistristMaster, Gravity.LEFT);
            SpDistristMaster.setAdapter(adapter);

            //POP
            ArrayList<KeyValuePairModel> ListPOP = new ArrayList<KeyValuePairModel>();
            ListPOP.add(new KeyValuePairModel(1, selectedItemParams.GetPopName()));
            adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListPOP, Gravity.LEFT);
            SpPOP.setAdapter(adapter);

            //Tap Diem
			/*ArrayList<KeyValuePairModel> ListPoint = new ArrayList<KeyValuePairModel>();
			ListPoint.add(new KeyValuePairModel(1, selectedItemParams.getTDName()));
			abc = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListPoint);
			SpCollectionPoint.setAdapter(abc);*/
            lblTDName.setText(selectedItemParams.getTDName());

            //Loai dich vu
            ArrayList<KeyValuePairModel> ListLoaiDV = new ArrayList<KeyValuePairModel>();
            ListLoaiDV.add(new KeyValuePairModel(1, "ADSL"));
            ListLoaiDV.add(new KeyValuePairModel(2, "VDSL"));
            adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListLoaiDV, Gravity.LEFT);
            SpTypeService.setAdapter(adapter);


            //Ma phieu dang ki
		  	/*txt_NumberRegister.setText(this.SOPHIEUDK);
		  	txt_NumberRegister.setEnabled(isVisible());*/
            lblRegCode.setText(this.SOPHIEUDK);
        }
    }


    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Common.reportActivityCreate(getActivity().getApplication(), getString(R.string.lbl_screen_name_dialog_book_port_adsl));
        return dialog;
    }

    private void LoadPort() {
        //PortFree SpPortFree
        new GetPortFreeADSLAction(mContext, sTDName, SPPortFree, iTypeService, txtPortAvailability);

    }

    //Thu hoi port
    private void recovertyPort() {
        GetIPV4Action NewIP = new GetIPV4Action();
        String IP = NewIP.GetIP();
        String UserName = Constants.USERNAME;
        paramsValue = new String[]{"1", SOPHIEUDK, UserName, IP};
        new RecoverRegistrationAction(mContext, paramsValue);
        onClickTracker("Thu hồi port ADSL");
    }

    private void onClickTracker(String lable) {
        try {
            // Get tracker.
            Tracker t = ((MyApp) getActivity().getApplication()).getTracker(
                    TrackerName.APP_TRACKER);
            // Set screen name.
            t.setScreenName(getString(R.string.lbl_screen_name_dialog_book_port_adsl) + ".onClick()");
            // Send a screen view.
            t.send(new HitBuilders.ScreenViewBuilder().build());

            // This event will also be sent with the most recently set screen name.
            // Build and send an Event.
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("BOOK_PORT_ONCLICK")
                    .setAction("onClick")
                    .setLabel(lable)
                    .build());

            // Clear the screen name field when we're done.
            t.setScreenName(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
