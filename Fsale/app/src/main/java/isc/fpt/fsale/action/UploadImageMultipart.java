package isc.fpt.fsale.action;

import android.content.Context;
import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.SignatureActivity;
import isc.fpt.fsale.activity.UploadRegistrationDocumentActivity;
import isc.fpt.fsale.model.IdImageDocumentResult;
import isc.fpt.fsale.model.ImageDocument;
import isc.fpt.fsale.model.ParseJsonModel;
import isc.fpt.fsale.model.UploadMultipartResult;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

/**
 * Created by HCM.TUANTT14 on 10/11/2018.
 * API upload nhiều hình ảnh
 */

public class UploadImageMultipart implements AsyncTaskCompleteListener<String> {
    private final String UPLOAD_FILE_PROTOCOL_MULTIPART = "Upload";
    private Context mContext;
    private ImageDocument imageDocument;

    public UploadImageMultipart(Context mContext, ImageDocument imageDocument,
                                Bitmap bitmap, String[] arrParamName, String[] arrParamValue) {
        this.mContext = mContext;
        this.imageDocument = imageDocument;
        String message = mContext.getResources().getString(R.string.label_process_upload_multipart_file_to_server);
        CallServiceTask service = new CallServiceTask(mContext,
                UPLOAD_FILE_PROTOCOL_MULTIPART, bitmap, arrParamName, arrParamValue,
                Services.JSON_POST_UPLOAD_MULTIPART, message, UploadImageMultipart.this);
        service.execute();
    }

    public UploadImageMultipart(Context mContext, Bitmap bitmap, String[] arrParamName, String[] arrParamValue) {
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.label_process_upload_multipart_file_to_server);
        CallServiceTask service = new CallServiceTask(mContext,
                UPLOAD_FILE_PROTOCOL_MULTIPART, bitmap, arrParamName, arrParamValue,
                Services.JSON_POST_UPLOAD_MULTIPART, message, UploadImageMultipart.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        String className = mContext.getClass().getSimpleName();
        if (Common.jsonObjectValidate(result)) {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(result);
                UploadMultipartResult uploadMultipartResult = ParseJsonModel.parse(jsonObject,
                        UploadMultipartResult.class, IdImageDocumentResult.class.getName());
                if (className != null && className.equals(UploadRegistrationDocumentActivity.class.getSimpleName())) {
                    UploadRegistrationDocumentActivity uploadRegistrationDocumentActivity = (UploadRegistrationDocumentActivity) mContext;
                    if(uploadMultipartResult != null) {
                        if (uploadMultipartResult.getData() != null) {
                            IdImageDocumentResult idImageDocument = uploadMultipartResult.getData().get(0);
                            imageDocument.setID(String.valueOf(idImageDocument.getId()));
                            imageDocument.setStatusUpload(1);
                            imageDocument.setUpdated(false);
                            uploadRegistrationDocumentActivity.numberSuccessUpload++;
                        } else {
                            imageDocument.setStatusUpload(2);
                        }
                    }
                    uploadRegistrationDocumentActivity.updateResultUpload();
                } else if (className != null && className.equals(SignatureActivity.class.getSimpleName())) {
                    SignatureActivity signatureActivity = (SignatureActivity) mContext;
                    signatureActivity.updateSignature(uploadMultipartResult);
                } else {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
