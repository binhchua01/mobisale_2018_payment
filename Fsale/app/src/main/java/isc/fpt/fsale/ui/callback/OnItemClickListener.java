package isc.fpt.fsale.ui.callback;

/**
 * Created by haulc3 on 15,July,2019
 */
public interface OnItemClickListener<T> {
    void onItemClick(T object);
}


