package isc.fpt.fsale.callback.upsell;

import isc.fpt.fsale.model.upsell.response.ObjUpgradeForCare;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeListModel;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeTransListModel;

public interface OnItemListenerUpsell {
    void CareOnItemListener(ObjUpgradeTransListModel careModel);
    void NotCareOnItemListener(ObjUpgradeListModel careModel);
}
