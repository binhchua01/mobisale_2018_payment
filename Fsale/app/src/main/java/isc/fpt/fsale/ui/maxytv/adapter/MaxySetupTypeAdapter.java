package isc.fpt.fsale.ui.maxytv.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.model.MaxySetupType;
import isc.fpt.fsale.ui.maxytv.model.MaxyTypeDeploy;

public class MaxySetupTypeAdapter extends RecyclerView.Adapter<MaxySetupTypeAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<MaxySetupType> mList;
    private OnItemClickListener mListener;

    public MaxySetupTypeAdapter(Context mContext, List<MaxySetupType> mList, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public MaxySetupTypeAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_key_value, parent, false);
        final MaxySetupTypeAdapter.SimpleViewHolder mViewHolder = new MaxySetupTypeAdapter.SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClick(mList.get(mViewHolder.getAdapterPosition()));
            }
        });
        mViewHolder.setIsRecyclable(false);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(MaxySetupTypeAdapter.SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
        }

        public void bindView(MaxySetupType mBox) {
            tvName.setText(mBox.getTypeSetupName());
            if(mBox.isSelected()){
                tvName.setBackgroundResource(R.drawable.background_radius_selected);
            }
        }
    }

    public void notifyData(List<MaxySetupType> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}
