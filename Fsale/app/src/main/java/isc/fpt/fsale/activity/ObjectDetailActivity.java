package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetObjectDetail;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.ui.fragment.MenuRightObjectDetail;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.slidingmenu.lib.SlidingMenu;

//màn hình THÔNG TIN HỢP ĐỒNG
public class ObjectDetailActivity extends BaseActivity {
    private ObjectDetailModel mObject;
    private String mContract;
    private TextView lblFullName, lblContract, lblAddress, lblRegCode, lblEmail, lblNote,
            lblLocalTypeName, lblDate, lblPhone1, lblContact1,
            lblBoxCount, lblServiceType, lblComboStatusName, lblRegCodeNew;
    private Button btnCreate;
    private MenuRightObjectDetail menuRight;

    public ObjectDetailActivity() {
        super(R.string.lbl_screen_name_object_detail_activity);
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_object_detail_activity));
        setContentView(R.layout.activity_object_detail);
        lblFullName = (TextView) findViewById(R.id.lbl_full_name);
        lblContract = (TextView) findViewById(R.id.lbl_contract);
        lblAddress = (TextView) findViewById(R.id.lbl_address);
        lblRegCode = (TextView) findViewById(R.id.lbl_reg_code);
        lblEmail = (TextView) findViewById(R.id.lbl_email);
        lblNote = (TextView) findViewById(R.id.lbl_note);
        lblLocalTypeName = (TextView) findViewById(R.id.lbl_local_type_name);
        lblDate = (TextView) findViewById(R.id.lbl_date);
        lblPhone1 = (TextView) findViewById(R.id.lbl_phone_1);
        lblContact1 = (TextView) findViewById(R.id.lbl_contact_1);
        lblBoxCount = (TextView) findViewById(R.id.lbl_box_count);
        lblServiceType = (TextView) findViewById(R.id.lbl_service_type);
        lblComboStatusName = (TextView) findViewById(R.id.lbl_combo_status_name);
        lblRegCodeNew = (TextView) findViewById(R.id.lbl_reg_code_new);
        lblRegCodeNew.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mObject.getRegIDNew() > 0)
                    new GetRegistrationDetail(ObjectDetailActivity.this, Constants.USERNAME, mObject);
            }
        });

        btnCreate = (Button) findViewById(R.id.btn_create);
        btnCreate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mObject.getCheckListService().isExtraOTT()){//HD Extra khong dc ban them
                    Common.alertDialog("Hợp đồng Extra OTT không được bán thêm", ObjectDetailActivity.this);
                    return;
                }
                Intent intent = new Intent(ObjectDetailActivity.this, RegisterContractActivity.class);
                intent.putExtra(Constants.TAG_CONTRACT, mObject);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ObjectDetailActivity.this.startActivity(intent);
            }
        });

        menuRight = new MenuRightObjectDetail(mObject);
        super.addRight(menuRight);
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else if (itemId == R.id.action_right) {
            toggle(SlidingMenu.RIGHT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    private void getDataFromIntent() {
        try {
            Intent myIntent = getIntent();
            if (myIntent != null) {
                if (myIntent.hasExtra(Constants.TAG_CONTRACT)) {
                    mContract = myIntent.getStringExtra(Constants.TAG_CONTRACT);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateView() {
        if (mObject != null) {
            lblFullName.setText(mObject.getFullName());
            lblContract.setText(mObject.getContract());
            lblAddress.setText(mObject.getAddress());
            lblRegCode.setText(mObject.getRegCode());
            lblEmail.setText(mObject.getEmail());
            lblNote.setText(mObject.getNote());
            lblLocalTypeName.setText(mObject.getLocalTypeName());
            lblDate.setText(Common.getSimpleDateFormatTwo(mObject.getDate(), "dd/MM/yyyy"));
            lblPhone1.setText(mObject.getPhone_1());
            lblContact1.setText(mObject.getContact_1());
            lblBoxCount.setText(String.valueOf(mObject.getBoxCount()));
            lblServiceType.setText(mObject.getServiceTypeName());
            lblComboStatusName.setText(mObject.getComboStatusName());
            lblRegCodeNew.setText(Html.fromHtml("<u>" + mObject.getRegCodeNew() + "</u>"));
            if (mObject.getRegIDNew() <= 0) {
                btnCreate.setVisibility(View.VISIBLE);
            } else {
                btnCreate.setVisibility(View.GONE);
            }
            menuRight.initAdapter(mObject);
        }
    }

    /*
     * Gọi API load lại thông tin HĐ để cập nhật lại giao diện, tránh trường hợp HĐ đã có PĐK mà view chưa hiện
     */
    private void getData() {
        new GetObjectDetail(this, mContract);
    }

    public void loadData(ObjectDetailModel object) {
        mObject = object;
        updateView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
        getData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }
}