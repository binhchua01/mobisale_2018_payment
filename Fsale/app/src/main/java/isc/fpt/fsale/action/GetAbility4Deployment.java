package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.DeployAppointmentActivity;
import isc.fpt.fsale.model.Ability4Deployment;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 19,April,2019
 * lấy DS time zone
 */

public class GetAbility4Deployment implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private DeployAppointmentActivity activity;

    public GetAbility4Deployment(Context mContext, DeployAppointmentActivity activity,
                                 int iPartnerId, int iSubId, String appointmentDate, String regCode) {
        this.mContext = mContext;
        this.activity = activity;
        String[] arrParamName = new String[]{"iPartnerID", "iSubID", "AppointmentDate", "RegCode"};
        String[] arrParamValue = new String[]{String.valueOf(iPartnerId), String.valueOf(iSubId),
                Common.reverseString(appointmentDate, "-"), regCode};
        String message = mContext.getResources().getString(R.string.msg_show_get_time_zone);
        String GET_ABILITY_4_DEPLOYMENT = "GetAbility4Deployment";
        CallServiceTask service = new CallServiceTask(mContext, GET_ABILITY_4_DEPLOYMENT, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetAbility4Deployment.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<Ability4Deployment> lst;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<Ability4Deployment> resultObject = new WSObjectsModel<>(jsObj, Ability4Deployment.class);
                lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    activity.initSpinnerTimeZone((ArrayList<Ability4Deployment>) lst);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
