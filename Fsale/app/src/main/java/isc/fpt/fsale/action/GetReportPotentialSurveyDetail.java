package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportPotentialSurveyDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportPotentialSurveyDetailItemModel;
import isc.fpt.fsale.model.ReportPotentialSurveyDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetReportPotentialSurveyDetail implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "ReportPotentialSurveyDetail";
	private String[] paramValues;
	public static final String[] paramNames = new String[]{"UserName", "Agent", "AgentName", "FromDate", "ToDate", "PageNumber"};
	//
	
	public GetReportPotentialSurveyDetail(Context context, String UserName, int Agent, String AgentName, String FromDate, String ToDate, int PageNumber) {	
		mContext = context;		
		this.paramValues = new String[]{UserName, String.valueOf(Agent), AgentName, FromDate, ToDate, String.valueOf(PageNumber)};			
		execute();
	}
	
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportPotentialSurveyDetail.this);
		service.execute();		
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			ArrayList<ReportPotentialSurveyDetailModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ReportPotentialSurveyDetailModel> resultObject = 
						 new WSObjectsModel<ReportPotentialSurveyDetailModel>(jsObj, ReportPotentialSurveyDetailModel.class, 
								 ReportPotentialSurveyDetailItemModel.class.getName());
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();				
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetCustomerCareList:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	
	
	private void loadData(ArrayList<ReportPotentialSurveyDetailModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListReportPotentialSurveyDetailActivity.class.getSimpleName())){
				ListReportPotentialSurveyDetailActivity activity = (ListReportPotentialSurveyDetailActivity)mContext;
				activity.loadData(lst);
						
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetCustomerCareList.loadRpCodeInfo()", e.getMessage());
		}
	}
	

		
	

}
