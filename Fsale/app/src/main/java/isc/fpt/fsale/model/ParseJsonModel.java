package isc.fpt.fsale.model;

import android.annotation.SuppressLint;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;


public class ParseJsonModel<T> {

	/*public static ParseJsonModel Parse(JSONObject json, Class<T> object){
            try {
	    		Field[] fields = object.getClass().getDeclaredFields();
	    		 for (Field field : fields) {
	    			 String propertiesName = field.getName();
	    			 if(json.has(propertiesName)){
	    				 field.setAccessible(true);
	    				 if(field.getType().getSimpleName().equals("String"))
						 field.set(object, json.getString(propertiesName));
					 else if(field.getType().getSimpleName().equals("Integer"))
						 field.set(object, json.getInt(propertiesName));
				 }
			 }
			 return this;
		} catch (Exception e) {
			Log.i("ParseJsonModel_Parse: ", e.getMessage());
		}
		return null;
	}*/

    public static <T> T parse(JSONObject json, Class<T> theClass) {
        try {
            T result = theClass.newInstance();
            Field[] fields = result.getClass().getDeclaredFields();
            for (Field field : fields) {
                String propertiesName = field.getName();
                if (json.has(propertiesName) && !json.isNull(propertiesName)) {
                    field.setAccessible(true);
                    if (field.getType().getSimpleName().equals("String")) {
                        field.set(result, json.getString(propertiesName) == "null" ? "" : json.getString(propertiesName));
                    } else if (field.getType().getSimpleName().equals("int"))
                        field.set(result, json.getInt(propertiesName));
                    else if (field.getType().getSimpleName().equals("Integer")) {
                        if (json.isNull(propertiesName))
                            field.set(result, 0);
                        else
                            field.set(result, json.getInt(propertiesName));
                    } else if (field.getType().getSimpleName().equals("boolean"))
                        field.set(result, json.getBoolean(propertiesName));
                    else if (field.getType().getSimpleName().equals("Boolean"))
                        field.set(result, json.getBoolean(propertiesName));
                }
            }
            return result;
        } catch (Exception e) {
            Log.i("ParseJsonModel_Parse: ", e.getMessage());
        }
        return null;
    }

    public static <T> T parse(JSONObject json, Class<T> theClass, String subClass) {
        try {
            T result = theClass.newInstance();
            Field[] fields = result.getClass().getDeclaredFields();
            for (Field field : fields) {
                String propertiesName = field.getName();
                field.setAccessible(true);
                if (Collection.class.isAssignableFrom(field.getType())) {
                    Class<?> cls = Class.forName(subClass);
                    if (cls != null)
                        field.set(result, parse(json.getJSONArray(propertiesName), cls));
                } else {
                    if (json.has(propertiesName)) {
                        if (field.getType().getSimpleName().equals("String")) {
                            if (json.isNull(propertiesName)) {
                                field.set(result, "");
                            } else {
                                field.set(result, json.getString(propertiesName) == "null" ? "" : json.getString(propertiesName));
                            }

                        } else if (field.getType().getSimpleName().equals("int")) {

                            field.set(result, json.getInt(propertiesName));
                        } else if (field.getType().getSimpleName().equals("Integer")) {
                            if (json.isNull(propertiesName))
                                field.set(result, 0);
                            else
                                field.set(result, json.getInt(propertiesName));
                        } else if (field.getType().getSimpleName().equals("boolean"))
                            field.set(result, json.getBoolean(propertiesName));
                        else if (field.getType().getSimpleName().equals("Boolean"))
                            field.set(result, json.getBoolean(propertiesName));
                        else if (field.getType().getPackage().getName().equals("isc.fpt.fsale.model")) {
                            if (json.has(propertiesName)) {
                                Class<?> cls = Class.forName(subClass);
                                if (cls != null) {
                                    if (!json.isNull(propertiesName)) {
                                        field.set(result, parse(json.getJSONObject(propertiesName), cls));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        } catch (Exception e) {

            Log.i("ParseJsonModel_Parse: ", e.getMessage());
        }
        return null;
    }

    public static <T> T parse(JSONObject json, Class<T> theClass, String subClassPackage,
                              String subClassListDevice, String subClassListDeviceOTT,
                              String subClassListMACOTT, String subClassCheckListRegistration) {
        T result = null;
        try {
            result = theClass.newInstance();
            Field[] fields = result.getClass().getDeclaredFields();
            for (Field field : fields) {
                String propertiesName = field.getName();
                field.setAccessible(true);
                if (Collection.class.isAssignableFrom(field.getType())) {
                    Class<?> clsPackage = Class.forName(subClassPackage);
                    if (clsPackage != null && propertiesName.equals("ListPackage"))
                        field.set(result, parse(json.getJSONArray(propertiesName), clsPackage));
                    Class<?> clsListDevice = Class.forName(subClassListDevice);
                    if (clsListDevice != null && propertiesName.equals("ListDevice")) {
                        field.set(result, parse(json.getJSONArray(propertiesName), clsListDevice));
                    }
                    if (json.has("ListDeviceMACOTT")) {
                        Class<?> clsListDeviceOTT = Class.forName(subClassListDeviceOTT);
                        if (clsListDeviceOTT != null && propertiesName.equals("listDeviceOTT")) {
                            field.set(result, parse(json.getJSONObject("ListDeviceMACOTT").getJSONArray(propertiesName), clsListDeviceOTT));
                        }
                        Class<?> clsListMACOTT = Class.forName(subClassListMACOTT);
                        if (clsListMACOTT != null && propertiesName.equals("listMACOTT")) {
                            field.set(result, parse(json.getJSONObject("ListDeviceMACOTT").getJSONArray(propertiesName), clsListMACOTT));
                        }
                    }
                } else {
                    if (json.has(propertiesName)) {
                        if (field.getType().getSimpleName().equals("String")) {
                            if (json.isNull(propertiesName)) {
                                field.set(result, "");
                            } else {
                                field.set(result, json.getString(propertiesName) == "null" ? "" : json.getString(propertiesName));
                            }

                        } else if (field.getType().getSimpleName().equals("int")) {

                            field.set(result, json.getInt(propertiesName));
                        } else if (field.getType().getSimpleName().equals("Integer")) {
                            if (json.isNull(propertiesName))
                                field.set(result, 0);
                            else
                                field.set(result, json.getInt(propertiesName));
                        } else if (field.getType().getSimpleName().equals("boolean"))
                            field.set(result, json.getBoolean(propertiesName));
                        else if (field.getType().getSimpleName().equals("Boolean"))
                            field.set(result, json.getBoolean(propertiesName));
                        else if (field.getType().getPackage().getName().equals("isc.fpt.fsale.model")) {
                            if (json.has(propertiesName)) {
                                Class<?> cls = Class.forName(subClassCheckListRegistration);
                                if (cls != null) {
                                    if (!json.isNull(propertiesName)) {
                                        field.set(result, parse(json.getJSONObject(propertiesName), cls));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {

            Log.i("ParseJsonModel_Parse: ", e.getMessage());
        }
        return result;
    }

    public static <T> ArrayList<T> parse(JSONArray jsonArr, Class<T> theClass) {
        try {
            ArrayList<T> resultArr = new ArrayList<T>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject json = jsonArr.getJSONObject(i);
                resultArr.add(parse(json, theClass));
            }
            return resultArr;
        } catch (Exception e) {

            Log.i("ParseJsonModel_Parse: ", e.getMessage());
            return null;
        }
        //return null;
    }

    @SuppressLint("LongLogTag")
    public static <T> JSONObject toJSONObject(T resouce) throws Exception {
        JSONObject jsonObj = new JSONObject();
        try {
            Field[] fields = resouce.getClass().getDeclaredFields();
            for (Field f : fields) {
                f.setAccessible(true);
                jsonObj.put(f.getName(), (f.get(resouce) != null) ? f.get(resouce) : JSONObject.NULL);
            }
        } catch (Exception e) {

            Log.i("ParseJsonModel_toJSONObject: ", e.getMessage());
        }

        return jsonObj;
    }

    public static <T> JSONArray toJSONArray(ArrayList<T> resouce)
            throws Exception {
        JSONArray array = new JSONArray();
        for (T obj : resouce) {
            array.put(toJSONObject(obj));
        }
        return array;
    }


    /*public String toString(){
         StringBuilder sb = new StringBuilder();
    	 Field[] fields = getClass().getDeclaredFields();
    	  for (Field f : fields) {
    	    try {
    	    	f.setAccessible(true);
    	    	sb.append(f.getName()).append(": ");
    	    	String value = f.get(this).toString();
    	    	if(value == null)
    	    		value = "";
    	    	sb.append("[").append(value).append("]").append(", ").append('\n');
			} catch (Exception e) {
				// TODO: handle exception
			}
    	  }

    	   *Field[] fields = this.getClass().getDeclaredFields();
	    		 for (Field field : fields) {
	    			 String propertiesName = field.getName();
	    			 if(json.has(propertiesName)){
	    				 field.setAccessible(true);
	    				 if(field.getType().getSimpleName().equals("String"))
						 field.set(this, json.getString(propertiesName));
					 else if(field.getType().getSimpleName().equals("Integer"))
						 field.set(this, json.getInt(propertiesName));
				 }

    	  return sb.toString();
    }*/
}
