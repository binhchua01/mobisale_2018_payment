package isc.fpt.fsale.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.UpdateRegistrationImage;
import isc.fpt.fsale.action.UploadImageMultipart;
import isc.fpt.fsale.adapter.ImageDocumentAdapter;
import isc.fpt.fsale.model.ImageDocument;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import static isc.fpt.fsale.activity.RegisterDetailActivity.TAG_REG_ID;
import static isc.fpt.fsale.ui.fragment.FragmentRegisterStep1.SELECT_PICTURE;
import static isc.fpt.fsale.utils.Common.listRestImageDocument;
import static isc.fpt.fsale.utils.Common.scaleBitmapSmaller;
import static isc.fpt.fsale.utils.Common.showScreenActionBar;
import static isc.fpt.fsale.utils.Constants.IDENTITY_MBS_VN;
import static isc.fpt.fsale.utils.Constants.TAG_IMAGE_DOCUMENT_IDENTITY_CARD_SELECTED;
import static isc.fpt.fsale.utils.Constants.TAG_LIST_IMAGE_DOCUMENT;
import static isc.fpt.fsale.utils.Constants.TAG_LIST_IMAGE_DOCUMENT_SELECTED;
import static isc.fpt.fsale.utils.Constants.TAG_REGISTRATION_CREATE_FORM;
import static isc.fpt.fsale.utils.Constants.TAG_UPDATE_REGISTRATION;
import static isc.fpt.fsale.utils.Constants.TAG_UPLOAD_IMAGE_TYPE;
import static isc.fpt.fsale.utils.Constants.TOKEN;

//màn hình upload nhiều file tạo thông tin khách hàng
public class UploadRegistrationDocumentActivity extends AppCompatActivity {
    private ListView lvImageDocument;
    private HashMap<Integer, List<ImageDocument>> listDocument;
    private ImageDocumentAdapter imageDocumentAdapter;
    private ImageButton btnSelectImageDocument;
    private LinearLayout frmContainerListImageDocument;
    private Button btnConfirmUploadListImageDocument;
    private int keyIndex = -1, index = -1, uploadImageType, regID,
            numberUpload, countNumberTimeUpload;
    public int numberSuccessUpload;
    private ArrayList<ImageDocument> listImageDocument = new ArrayList<>();
    private ArrayList<ImageDocument> listImageDocumentTemp = new ArrayList<>();
    private ArrayList<ImageDocument> listDefaultImageDocumentRegistration = new ArrayList<>();
    private TextView tvNumberUploadSuccess;
    private String labelTotalUploadSuccess = "Đã up được ";
    private boolean isDelete, statusUpdate, updateRegistration, statusRegistrationFormCreate;
    private final static int REQUEST_CODE_PERMISSION_MULTI_CHOICE = 2;
    private final static int REQUEST_CODE_PERMISSION_SINGLE_CHOICE = 3;

    @SuppressLint("UseSparseArrays")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_registration_document);
        //hiển thị actionbar
        showScreenActionBar(this);
        initView();
        initEvent();
        getDataIntent();
    }

    private void initEvent() {
        btnSelectImageDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery(true);
            }
        });
        btnConfirmUploadListImageDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ImageDocument> listImageDocumentUpdated =
                        getListImageDocument(Common.listRestImageDocument(listDocument));
                if (isDelete) {
                    if ((numberUpload = listImageDocumentUpdated.size()) > 0) {
                        listImageDocument = Common.listRestImageDocument(listDocument);
                        uploadAllImageDocument(listImageDocumentUpdated);
                    } else {
                        String desListID = Common.covertListImageDocumentToString(
                                Common.listRestImageDocument(listDocument), ",");
                        if (regID > 0 && !statusRegistrationFormCreate) {
                            if (desListID.contains("(null)") || desListID.contains("/")) {
                                confirmUpdateImageDocumentInfo(desListID, true);
                            } else {
                                new UpdateRegistrationImage(
                                        UploadRegistrationDocumentActivity.this,
                                        regID,
                                        desListID
                                );
                            }

                        } else {
                            if (desListID.contains("(null)") || desListID.contains("/")) {
                                confirmUpdateImageDocumentInfo(desListID, true);
                            } else {
                                Intent intent = new Intent();
                                intent.putExtra(TAG_LIST_IMAGE_DOCUMENT, Common.listRestImageDocument(listDocument));
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }
                    }
                } else {
                    // danh sách ID cuối sau khi cập nhận, upload
                    listImageDocument = Common.listRestImageDocument(listDocument);
                    listImageDocumentTemp = listImageDocument;
                    numberUpload = listImageDocument.size();
                    if (statusUpdate) {
                        // lọc danh sách ảnh update ảnh mới.
                        listImageDocumentTemp = getListImageDocument(listImageDocument);
                        numberUpload = listImageDocumentTemp.size();
                    }
                    if (listImageDocumentTemp.size() > 0) {
                        //xử lý quá trình cập nhật danh sách ảnh mới
                        uploadAllImageDocument(listImageDocumentTemp);
                    } else {
                        sendIntentBack();
                    }
                }
            }
        });
    }

    @SuppressLint("UseSparseArrays")
    private void initView() {
        btnSelectImageDocument = (ImageButton) findViewById(R.id.btn_select_image_document);
        frmContainerListImageDocument = (LinearLayout) findViewById(R.id.frm_container_list_image_document);
        listDocument = new HashMap<>();
        lvImageDocument = (ListView) findViewById(R.id.lv_image_document);
        tvNumberUploadSuccess = (TextView) findViewById(R.id.tv_number_image_upload_success);
        imageDocumentAdapter = new ImageDocumentAdapter(this, listDocument);
        lvImageDocument.setAdapter(imageDocumentAdapter);
        btnConfirmUploadListImageDocument = (Button) findViewById(R.id.btn_confirm_upload_multi_file_image_document);
    }

    private void uploadAllImageDocument(ArrayList<ImageDocument> listImageDocumentTemp) {
        String saleName = ((MyApp) getApplicationContext()).getUserName();
        String[] paramNames = new String[]{"LinkId", "Source", "Type", "SaleName", "Token"};
        String[] paramValues = new String[]{String.valueOf(regID), String.valueOf(IDENTITY_MBS_VN),
                String.valueOf(uploadImageType), saleName, TOKEN};
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (ImageDocument imageDocument : listImageDocumentTemp) {
            numberSuccessUpload = 0;
            Bitmap bitmap = imageDocument.getPath() != null ?
                    scaleBitmapSmaller(new File(imageDocument.getPath())) :
                    scaleBitmapSmaller(new File(imageDocument.getPathCache()));
            // Kết nối API Upload ảnh multipart
            newThreadUploadFile(executorService, imageDocument, bitmap, paramNames, paramValues);
        }
    }

    //lấy đường dẫn ảnh chứng minh nhân dân và hiển thị giao diện
    public void getDataIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(TAG_REG_ID)) {
                regID = intent.getIntExtra(TAG_REG_ID, -1);
            }
            if (intent.hasExtra(TAG_REGISTRATION_CREATE_FORM)) {
                statusRegistrationFormCreate = intent.getBooleanExtra(TAG_REGISTRATION_CREATE_FORM, false);
            }
            String pathIdentityCard = intent.getStringExtra(TAG_IMAGE_DOCUMENT_IDENTITY_CARD_SELECTED);
            int uploadImageTypeRequest = intent.getIntExtra(TAG_UPLOAD_IMAGE_TYPE, 0);
            updateRegistration = intent.getBooleanExtra(TAG_UPDATE_REGISTRATION, false);
            ArrayList<ImageDocument> listImageDocumentRegistration =
                    intent.getParcelableArrayListExtra(TAG_LIST_IMAGE_DOCUMENT_SELECTED);
            List<ImageDocument> listImageIdentityCard = new ArrayList<>();
            listImageIdentityCard.add(new ImageDocument(pathIdentityCard, true, 2));
            if (pathIdentityCard != null && !updateRegistration) {
                listDocument.put(0, listImageIdentityCard);
            }
            if (uploadImageTypeRequest > 0) {
                uploadImageType = uploadImageTypeRequest;
            }
            if (updateRegistration && listImageDocumentRegistration.size() > 0) {
                statusUpdate = true;
                listImageDocument = listImageDocumentRegistration;
                if (listImageDocument.size() > 0) {
                    if (listImageDocument.get(0).getPath() != null || listImageDocument.get(0).getPathCache() != null) {
                        cloneImageDocument(listDefaultImageDocumentRegistration, listImageDocument);
                        refreshAdapterImageDocument(Common.mapListDocumentRest(listImageDocument), false);
                    } else {
                        String[] param = new String[]{"Id", "Token"};
                        String[] paramValues = new String[2];
                        paramValues[1] = TOKEN;
                        String methodDownload = "Download";
                        downloadAllFileImageDocument(methodDownload, this.listImageDocument, param, paramValues);
                    }
                }
            }
            if (!updateRegistration) {
                setUpView(listDocument);
            }
        }
    }

    // mở file ảnh từ thiết bị
    @SuppressLint("InlinedApi")
    public void openGallery(boolean multiChoice) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                if (multiChoice) {
                    requestPermissions(new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_MULTI_CHOICE);
                } else {
                    requestPermissions(new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_SINGLE_CHOICE);
                }
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, multiChoice);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
            }
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, multiChoice);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
        }
    }

    // cập nhật file ảnh từ thiết bị
    public void openGallerySetKeyIndex(int keyIndex, int index) {
        this.keyIndex = keyIndex;
        this.index = index;
        openGallery(false);
    }

    private void newThreadUploadFile(ExecutorService executorService, final ImageDocument imageDocument,
                                     final Bitmap bitmap, final String[] paramNames, final String[] paramValues) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                new UploadImageMultipart(UploadRegistrationDocumentActivity.this,
                        imageDocument, bitmap, paramNames, paramValues);
            }
        });
    }

    // cập nhật lại map danh sách đường dẫn các ảnh sau khi chọn 1 ảnh từ thiết bị
    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int sizeFinaListImageDocument;
        if (requestCode == 2 && data != null) {
            ClipData clipData = data.getClipData();
            Uri selectedImageUri;
            String selectedImagePath;
            if (clipData != null) {
                int sizeItemSelected = clipData.getItemCount();
                // số lượng tối ta file cho phép chọn khi upload
                int maxNumberFileSelected = 5;
                if (sizeItemSelected > maxNumberFileSelected) {
                    Common.alertDialogNotTitle(getResources().getString(R.string.lbl_over_image_document_select_message), this);
                } else {
                    List<ImageDocument> listImageDocument = new ArrayList<>();
                    for (int i = 0; i < sizeItemSelected; i++) {
                        selectedImageUri = clipData.getItemAt(i).getUri();
                        // lấy đường dẫn thực tế sau khi bấm chọn ảnh từ thiết bị
                        selectedImagePath = Common.getRealPathUrlImage(
                                UploadRegistrationDocumentActivity.this, selectedImageUri);
                        listImageDocument.add(new ImageDocument(selectedImagePath, true, 2));
                    }
                    sizeFinaListImageDocument = listImageDocument.size();
                    // kiểm tra số phần tử trong list ảnh đã chọn trước đó.
                    if (listDocument.size() > 0) {
                        List<ImageDocument> beforeListImageDocumentSelected = listRestImageDocument(listDocument);
                        sizeFinaListImageDocument = (beforeListImageDocumentSelected.size() + listImageDocument.size());
                        if (sizeFinaListImageDocument > maxNumberFileSelected) {
                            Common.alertDialogNotTitle(getResources().getString(R.string.lbl_over_image_document_select_message), this);
                        } else {
                            List<ImageDocument> newListImageDocument = new ArrayList<>();
                            newListImageDocument.addAll(beforeListImageDocumentSelected);
                            newListImageDocument.addAll(listImageDocument);
                            listImageDocument = newListImageDocument;
                            listDocument.clear();
                        }
                    }
                    if (sizeFinaListImageDocument <= maxNumberFileSelected) {
                        listDocument.putAll(Common.mapListDocumentRest(listImageDocument));
                        setUpView(listDocument);
                    }
                }
                isDelete = false;
            } else {
                isDelete = false;
                selectedImageUri = data.getData();
                // lấy đường dẫn thực tế sau khi bấm chọn ảnh từ thiết bị
                selectedImagePath = Common.getRealPathUrlImage(this, selectedImageUri);
                if (selectedImagePath != null && !selectedImagePath.equals("")) {
                    if (keyIndex == -1) {
                        switch (listDocument.size()) {
                            case 0: {
                                List<ImageDocument> listImageDocument1 = new ArrayList<>();
                                listImageDocument1.add(new ImageDocument(selectedImagePath, true, 2));
                                listDocument.put(0, listImageDocument1);
                                imageDocumentAdapter.notifyDataSetChanged();
                                frmContainerListImageDocument.setVisibility(View.VISIBLE);
                                btnSelectImageDocument.setVisibility(View.GONE);
                                break;
                            }
                            case 1: {
                                if (listDocument.get(0).size() == 1) {
                                    listDocument.get(0).add(new ImageDocument(selectedImagePath, true, 2));
                                } else {
                                    List<ImageDocument> listImageDocument2 = new ArrayList<>();
                                    listImageDocument2.add(new ImageDocument(selectedImagePath, true, 2));
                                    listDocument.put(1, listImageDocument2);
                                }
                                refreshAdapterImageDocument(listDocument, false);
                                break;
                            }
                            case 2: {
                                if (listDocument.get(1).size() == 1) {
                                    listDocument.get(1).add(new ImageDocument(selectedImagePath, true, 2));
                                } else {
                                    List<ImageDocument> listImageDocument3 = new ArrayList<>();
                                    listImageDocument3.add(new ImageDocument(selectedImagePath, true, 2));
                                    listDocument.put(2, listImageDocument3);
                                }
                                refreshAdapterImageDocument(listDocument, false);
                                break;
                            }
                        }
                    } else {
                        // cập nhật lại ảnh mới sau khi chọn 1 ảnh từ thiết bị
                        if (listDocument.get(keyIndex) != null) {
                            ImageDocument imageDocument = listDocument.get(keyIndex).get(index);
                            imageDocument.setPath(selectedImagePath);
                            imageDocument.setUpdated(true);
                            imageDocument.setStatusUpload(2);
                            refreshAdapterImageDocument(listDocument, false);
                            keyIndex = -1;
                        }
                    }
                } else {
                    Common.alertDialog(getResources().getString(R.string.lbl_error_image_document_select_message), this);
                }
            }
        }
    }

    // cập nhật lại danh sách ảnh đã chọn
    public void refreshAdapterImageDocument(HashMap<Integer, List<ImageDocument>> listDocument, boolean isDelete) {
        if (listDocument.size() == 0 && keyIndex == 0) {
            keyIndex = -1;
        }
        if (listDocument.size() == 0) {
            Common.alertDialogNotTitle(getResources().getString(R.string.lbl_status_un_selected_identity_card_image_document_message), this);
        }
        this.isDelete = isDelete;
        this.listDocument = listDocument;
        setUpView(this.listDocument);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            sendIntentBack();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
        getApplication().registerReceiver(mMessageReceiver,
                new IntentFilter("UPLOAD_FILE_DISCONNECT_TO_INTERNET"));
    }

    @Override
    public void onBackPressed() {
        sendIntentBack();
    }

    // truyền dữ liệu sang màn hình tạo TTKH hoặc chi tiết TTKH
    private void sendIntentBack() {
        Intent intent = new Intent();
        intent.putExtra(TAG_UPDATE_REGISTRATION, updateRegistration);
        intent.putExtra(TAG_LIST_IMAGE_DOCUMENT, listDefaultImageDocumentRegistration);
        setResult(RESULT_OK, intent);
        finish();
    }

    // khôi phục màn hình khi chưa chọn ảnh
    public void resetView(boolean status) {
        if (status) {
            btnSelectImageDocument.setVisibility(View.VISIBLE);
            frmContainerListImageDocument.setVisibility(View.GONE);
        } else {
            btnSelectImageDocument.setVisibility(View.GONE);
            frmContainerListImageDocument.setVisibility(View.VISIBLE);
        }
    }

    // thiết lập giao diện cho màn hình danh sách ảnh hồ sơ
    public void setUpView(HashMap<Integer, List<ImageDocument>> listDocument) {
        imageDocumentAdapter = new ImageDocumentAdapter(this, listDocument);
        lvImageDocument.setAdapter(imageDocumentAdapter);
        if (listDocument.size() == 0) {
            // khôi phục lại màn hình chưa chọn ảnh
            resetView(true);
        } else {
            resetView(false);
        }
    }

    //trả về kết quả upload các file ảnh hồ sơ
    public void updateResultUpload() {
        imageDocumentAdapter.notifyDataSetChanged();
        // kiểm tra đủ số lượng khi tạo mới hoặc upload
        countNumberTimeUpload++;
        if (countNumberTimeUpload == numberUpload) {
            countNumberTimeUpload = 0;
            if (numberSuccessUpload < numberUpload) {
                listDefaultImageDocumentRegistration = listImageDocument;
                tvNumberUploadSuccess.setText(
                        labelTotalUploadSuccess.concat(
                                String.valueOf(numberSuccessUpload).concat("/").concat(
                                        String.valueOf(numberUpload)
                                ).concat(" ảnh")
                        )
                );
            }

        }
        if (numberSuccessUpload == numberUpload) {
            String listID = Common.covertListImageDocumentToString(Common.listRestImageDocument(listDocument), ",");
            if (regID > 0 && !statusRegistrationFormCreate) {
                if (listID.contains("(null)") || listID.contains("/")) {
                    confirmUpdateImageDocumentInfo(listID, false);
                } else {
                    new UpdateRegistrationImage(this, regID, listID);
                }
            } else {
                if (listID.contains("(null)") || listID.contains("/")) {
                    confirmUpdateImageDocumentInfo(listID, true);
                } else {
                    Intent intent = new Intent();
                    intent.putExtra(TAG_LIST_IMAGE_DOCUMENT, listImageDocument);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        }
    }

    // tải về toàn bộ file tài liệu hồ sơ phiếu thông tin khách hàng

    @SuppressLint("StaticFieldLeak")
    private void downloadAllFileImageDocument(final String methodName, final List<ImageDocument> listImageDocumentInput, final String[] params, final String[] paramsValue) {
        String message = getResources().getString(R.string.lbl_process_load_image_document_message);
        final ProgressDialog pd = Common.showProgressBar(this, message);
        new AsyncTask<Object, String, List<ImageDocument>>() {
            @Override
            protected void onPostExecute(List<ImageDocument> listRestImageDocument) {
                if (pd != null && pd.isShowing())
                    pd.dismiss();
                cloneImageDocument(listDefaultImageDocumentRegistration, listRestImageDocument);
                listDocument = Common.mapListDocumentRest(listRestImageDocument);
                setUpView(listDocument);
            }

            @Override
            protected List<ImageDocument> doInBackground(Object... object) {
                try {
                    // kết nối API load ảnh hoặc kiểm tra ảnh sau khi upload
                    for (int index = 0; index < listImageDocumentInput.size(); index++) {
                        ImageDocument imageDocument = listImageDocumentInput.get(index);
                        paramsValue[0] = imageDocument.getID();
                        // Kết nối API load ảnh
                        String pathCacheFile = Common.getPathCacheImageFile(UploadRegistrationDocumentActivity.this, Services.getBitmapImageDocument(methodName, params, paramsValue));
                        imageDocument.setPathCache(pathCacheFile);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return listImageDocumentInput;
            }
        }.execute(null, null, null);
    }

    // lấy danh sách ảnh mới update để upload lên server
    private ArrayList<ImageDocument> getListImageDocument(List<ImageDocument> listImageDocumentInput) {
        ArrayList<ImageDocument> listDes = new ArrayList<>();
        for (ImageDocument imageDocument : listImageDocumentInput) {
            if (imageDocument.isUpdated()) {
                listDes.add(imageDocument);
            }
        }
        return listDes;
    }

    @Override
    protected void onPause() {
        super.onPause();
        getApplication().unregisterReceiver(mMessageReceiver);
    }

    // nhận Broadcast thông báo số ảnh upload thành công
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @SuppressLint("DefaultLocale")
        @Override
        public void onReceive(Context context, Intent intent) {
            tvNumberUploadSuccess.setText(labelTotalUploadSuccess.concat(String.valueOf(numberSuccessUpload).concat("/").concat(String.valueOf(numberUpload)).concat(" ảnh")));
        }
    };

    // dialog hiển thị thông báo xác nhận người dùng cập nhật danh sách ảnh mới.
    private void confirmUpdateImageDocumentInfo(final String listID, final boolean isCreateRegistration) {
        try {
            AlertDialog.Builder builder;
            Dialog dialog;
            builder = new AlertDialog.Builder(this);
            builder.setMessage(
                    "Phiếu TTKH có ảnh cũ bị lỗi và sẽ bị xóa . Bạn có muốn tiếp tục không?")
                    .setCancelable(false)
                    .setPositiveButton(
                            this.getResources().getString(R.string.lbl_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    if (isCreateRegistration) {
                                        listImageDocument.remove(0);
                                        Intent intent = new Intent();
                                        intent.putExtra(TAG_LIST_IMAGE_DOCUMENT, listImageDocument);
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    } else {
                                        String desListID = "";
                                        if (listID.contains("(null),")) {
                                            desListID = listID.replace("(null),", "");
                                        }
                                        if (listID.contains("/")) {
                                            desListID = listID.substring(listID.indexOf(",") + 1);
                                        }
                                        new UpdateRegistrationImage(UploadRegistrationDocumentActivity.this, regID, desListID);
                                    }
                                }
                            })
                    .setNegativeButton(
                            this.getResources().getString(R.string.lbl_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });

            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //copy các phần tử ảnh ban đầu
    private void cloneImageDocument(ArrayList<ImageDocument> listImageDocumentDefault, List<ImageDocument> listImageDocumentClone) {
        for (ImageDocument imageDocument : listImageDocumentClone) {
            try {
                listImageDocumentDefault.add((ImageDocument) imageDocument.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(this, message);
                return;
            }
        }
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTI_CHOICE: {
                openGallery(true);
                break;
            }

            case REQUEST_CODE_PERMISSION_SINGLE_CHOICE: {
                openGallery(false);
                break;
            }
        }
    }
}
