package isc.fpt.fsale.model;

import java.lang.reflect.Field;

import net.hockeyapp.android.ExceptionHandler;

import android.os.Parcel;
import android.os.Parcelable;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "PTCDETAIL_MODEL".
 */
public class PTCDetailModel implements Parcelable{

    private int PTCId;
    private String CreateDate;
    private String CreateBy;
    private String UpdateDate;
    private String UpdateBy;
    private int PTCStatus;
    private String PTCStatusName;
    private String StaffName;
    private String StaffEmail;
    private String StaffPhone;
    private String Contract;
    private String FullName;
    private String Address;
    private String CustomerPhone;

    public PTCDetailModel() {
    }

    public PTCDetailModel(int PTCId, String CreateDate, String CreateBy, String UpdateDate, String UpdateBy, int PTCStatus, String PTCStatusName, String StaffName, String StaffEmail, String StaffPhone, String Contract, String FullName, String Address, String CustomerPhone) {
        this.PTCId = PTCId;
        this.CreateDate = CreateDate;
        this.CreateBy = CreateBy;
        this.UpdateDate = UpdateDate;
        this.UpdateBy = UpdateBy;
        this.PTCStatus = PTCStatus;
        this.PTCStatusName = PTCStatusName;
        this.StaffName = StaffName;
        this.StaffEmail = StaffEmail;
        this.StaffPhone = StaffPhone;
        this.Contract = Contract;
        this.FullName = FullName;
        this.Address = Address;
        this.CustomerPhone = CustomerPhone;
    }

    public int getPTCId() {
        return PTCId;
    }

    public void setPTCId(int PTCId) {
        this.PTCId = PTCId;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String CreateBy) {
        this.CreateBy = CreateBy;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String UpdateDate) {
        this.UpdateDate = UpdateDate;
    }

    public String getUpdateBy() {
        return UpdateBy;
    }

    public void setUpdateBy(String UpdateBy) {
        this.UpdateBy = UpdateBy;
    }

    public int getPTCStatus() {
        return PTCStatus;
    }

    public void setPTCStatus(int PTCStatus) {
        this.PTCStatus = PTCStatus;
    }

    public String getPTCStatusName() {
        return PTCStatusName;
    }

    public void setPTCStatusName(String PTCStatusName) {
        this.PTCStatusName = PTCStatusName;
    }

    public String getStaffName() {
        return StaffName;
    }

    public void setStaffName(String StaffName) {
        this.StaffName = StaffName;
    }

    public String getStaffEmail() {
        return StaffEmail;
    }

    public void setStaffEmail(String StaffEmail) {
        this.StaffEmail = StaffEmail;
    }

    public String getStaffPhone() {
        return StaffPhone;
    }

    public void setStaffPhone(String StaffPhone) {
        this.StaffPhone = StaffPhone;
    }

    public String getContract() {
        return Contract;
    }

    public void setContract(String Contract) {
        this.Contract = Contract;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getCustomerPhone() {
        return CustomerPhone;
    }

    public void setCustomerPhone(String CustomerPhone) {
        this.CustomerPhone = CustomerPhone;
    }

    @Override
    public String toString() {
    	 StringBuilder sb = new StringBuilder();
    	 Field[] fields = getClass().getDeclaredFields();    	
    	  for (Field f : fields) {
    	    sb.append(f.getName());
    	    sb.append(": ");
    	    try {
    	    	sb.append("[");
				sb.append(f.get(this));
				sb.append("]");
			} catch (Exception e) {
				// TODO: handle exception

			}
    	    
    	    sb.append(", ");
    	    sb.append('\n');
    	  }
    	  return sb.toString();
    }

    public PTCDetailModel(Parcel in){
    	this.Address = in.readString();
    	this.Contract  = in.readString();
    	this.CreateBy = in.readString();
    	this.CreateDate = in.readString();
    	this.CustomerPhone = in.readString();
    	this.FullName = in.readString();
    	this.PTCId = in.readInt();
    	this.PTCStatus = in.readInt();
    	this.PTCStatusName = in.readString();
    	this.StaffEmail = in.readString();
    	this.StaffName = in.readString();
    	this.StaffPhone = in.readString();
    	this.UpdateBy = in.readString();
    	this.UpdateDate = in.readString();
    }
    
    
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags)  {
		// TODO Auto-generated method stub
		out.writeString(this.Address);
		out.writeString(this.Contract);
		out.writeString(this.CreateBy);
		out.writeString(this.CreateDate);
		out.writeString(this.CustomerPhone);
		out.writeString(this.FullName);
		out.writeInt(this.PTCId);
		out.writeInt(this.PTCStatus);
		out.writeString(this.PTCStatusName);
		out.writeString(this.StaffEmail);
		out.writeString(this.StaffName);
		out.writeString(this.StaffPhone);
		out.writeString(this.UpdateBy);
		out.writeString(this.UpdateDate);
		
	}
	
	public static final Creator<PTCDetailModel> CREATOR = new Creator<PTCDetailModel>() {
		@Override     
		public PTCDetailModel createFromParcel(Parcel source) {
	             return new PTCDetailModel(source);
		}
		@Override
		public PTCDetailModel[] newArray(int size) {
             return new PTCDetailModel[size];
        }
	};
}
