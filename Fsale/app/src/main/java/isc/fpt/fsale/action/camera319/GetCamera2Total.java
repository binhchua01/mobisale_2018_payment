package isc.fpt.fsale.action.camera319;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.InternetTotal;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.camera319.Camera2Total;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractCamera;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractTotal;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by nhannh26 on 23/03/2021
 */
public class GetCamera2Total implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterStep4 fragment;
    private FragmentRegisterContractCamera fragmentContract;

    //bán mới
    public GetCamera2Total(Context mContext, RegistrationDetailModel
            mRegistrationDetailModel, FragmentRegisterStep4 fragment) {
        this.fragment = fragment;
        this.mContext = mContext;
        String message = "Tính tổng tiền Camera...";
        String GET_CAMERA_TOTAL = "GetCameraOfNetTotal";
        CallServiceTask service = new CallServiceTask(mContext, GET_CAMERA_TOTAL, mRegistrationDetailModel.toJSONObjectGetCameraTotal(),
                Services.JSON_POST_OBJECT, message, GetCamera2Total.this);
        service.execute();
    }
    //bán thêm
    public GetCamera2Total(Context mContext, RegistrationDetailModel
            mRegistrationDetailModel, FragmentRegisterContractCamera fragment) {
        this.fragmentContract = fragment;
        this.mContext = mContext;
        String message = "Tính tổng tiền Camera...";
        String GET_CAMERA_TOTAL = "GetCameraOfNetTotal";
        CallServiceTask service = new CallServiceTask(mContext, GET_CAMERA_TOTAL, mRegistrationDetailModel.toJSONObjectGetCameraTotal(),
                Services.JSON_POST_OBJECT, message, GetCamera2Total.this);
        service.execute();
    }


    @Override
    public void onTaskComplete(String result) {
        try {
            List<Camera2Total> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), Camera2Total.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (fragment != null) {
                        fragment.loadCameraPrice(lst.get(0));
                    } else {
                        ((RegisterContractActivity) mContext)
                                .getTotalFragment().loadCameraPrice(lst.get(0));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
