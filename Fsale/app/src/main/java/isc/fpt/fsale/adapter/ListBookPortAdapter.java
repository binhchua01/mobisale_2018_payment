package isc.fpt.fsale.adapter;


import isc.fpt.fsale.model.RowBookPortModel;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListBookPortAdapter extends BaseAdapter {
	 
	private ArrayList<RowBookPortModel> mlist;
	private Context mContext;
	
	
	public ListBookPortAdapter(Context mContext,ArrayList<RowBookPortModel> mlist)
	{
		this.mContext=mContext;
		this.mlist=mlist;
	}
	
	@Override
	public int getCount()
	{
		return this.mlist.size();
	}
	
	@Override
	public Object getItem(int position)
	{
		return this.mlist.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	
		RowBookPortModel ItemBookPort = mlist.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.list_row_book_port,null);
		}		
		
		TextView txtStt = (TextView) convertView.findViewById(R.id.txt_stt_billing_detail);
		txtStt.setText(String.valueOf(ItemBookPort.getSTT()));
		
		
		TextView txtName = (TextView) convertView.findViewById(R.id.txt_trang);
		txtName.setText(ItemBookPort.getTDName());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(ItemBookPort.getTDName()))
		{
			txtName.setVisibility(View.GONE);
		}
		
		TextView txtisemptyport = (TextView) convertView.findViewById(R.id.txt_isempty_port);
		txtisemptyport.setText(String.valueOf(ItemBookPort.getPortFree()));
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(String.valueOf(ItemBookPort.getPortFree())))
		{
			txtisemptyport.setVisibility(View.GONE);
		}
		
		TextView txtdistance = (TextView) convertView.findViewById(R.id.txt_distance);
		txtdistance.setText(String.valueOf(ItemBookPort.getPortTotal()));
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(String.valueOf(ItemBookPort.getPortTotal())))
		{
			txtdistance.setVisibility(View.GONE);
		}
		return convertView;
	}
	
}
