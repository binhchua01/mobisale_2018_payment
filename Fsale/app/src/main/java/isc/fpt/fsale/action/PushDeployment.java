package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
// API gửi mail TIẾN ĐIỆN TRIỂN KHAI
public class PushDeployment implements AsyncTaskCompleteListener<String> {

    private String[] arrParamName, arrParamValue;
    private Context mContext;
    private final String TAG_METHOD_NAME = "PushDeployment";
    private final String TAG_RESULT_LIST = "ListObject", TAG_RESULT = "Result", /*TAG_RESULT_ID = "ResultID",*/
            TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";


    public PushDeployment(Context context, String Contract) {
        try {
            mContext = context;
            arrParamName = new String[]{"UserName", "Contract"};
            this.arrParamValue = new String[]{Constants.USERNAME, Contract};

            String message = "Đang cập nhật...";
            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, PushDeployment.this);
            service.execute();
        } catch (Exception e) {

            e.getMessage();
        }
    }

    public void handleUpdateRegistration(String json) {
        if (json != null && Common.jsonObjectValidate(json)) {
            JSONObject jsObj = getJsonObject(json);
            if (jsObj != null) {
                try {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        JSONArray resultArray = jsObj.getJSONArray(TAG_RESULT_LIST);
                        if (resultArray != null && resultArray.length() > 0) {
                            Common.alertDialog(resultArray.getJSONObject(0).getString(TAG_RESULT), mContext);
                        }

                    } else {
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                } catch (Exception e) {

                }
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) +
                        "-" + Constants.RESPONSE_RESULT, mContext);
            }
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            JSONObject jsObj = JSONParsing.getJsonObj(result);
            return jsObj;
        } catch (Exception e) {
            // TODO: handle exception

        }
        return null;
    }


    @Override
    public void onTaskComplete(String result) {
        handleUpdateRegistration(result);
    }


}
