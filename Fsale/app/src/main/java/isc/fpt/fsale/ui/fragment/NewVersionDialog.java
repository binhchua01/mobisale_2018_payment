package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckSIMIMEI;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.MyApp;
import isc.fpt.fsale.utils.MyApp.TrackerName;

/**
 * DIALOG FRAGMENT: NewVersionDialog
 *
 * @Description: show notification and link download new version for app
 * @author: vandn, on 14/08/2013
 */
//màn hình phiên bản mới
public class NewVersionDialog extends DialogFragment {
    //private Context mContext;
    private TextView txtLinkDownload;
    private ImageButton btnClose;
    private String sLink;
    private FragmentActivity activity;
    private Context mContext;
    private DialogFragment prevDialog;

    public NewVersionDialog() {
    }

    @SuppressLint("ValidFragment")
    public NewVersionDialog(Context mContext, String sLink, FragmentActivity activity) {
        //this.mContext = mContext;
        this.sLink = sLink;
        this.activity = activity;
        this.mContext = mContext;
    }

    @SuppressLint("ValidFragment")
    public NewVersionDialog(Context mContext, String sLink, DialogFragment dialog) {
        //this.mContext = mContext;
        this.sLink = sLink;
        this.activity = null;
        this.prevDialog = dialog;
        this.mContext = mContext;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View view = inflater.inflate(R.layout.dialog_new_version_notification, container);
        getDialog().setCancelable(false);
        txtLinkDownload = (TextView) view.findViewById(R.id.txt_new_version_link);
        txtLinkDownload.setText(sLink);
        Linkify.addLinks(txtLinkDownload, Linkify.ALL);
        txtLinkDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onClickTracker(txtLinkDownload.getText().toString());
                getDialog().dismiss();
                if (activity != null)
                    activity.finish();
                if (prevDialog != null)
                    try {
                        prevDialog.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception

                    }
            }
        });

        btnClose = (ImageButton) view.findViewById(R.id.btn_close_dialog_new_version);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return view;
    }

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Common.reportActivityCreate(getActivity().getApplication(), getString(R.string.lbl_screen_name_dialog_new_version));
        return dialog;
    }

    private void onClickTracker(String lable) {
        try {
            // Get tracker.
            Tracker t = ((MyApp) getActivity().getApplication()).getTracker(
                    TrackerName.APP_TRACKER);
            // Set screen name.
            t.setScreenName(getString(R.string.lbl_screen_name_dialog_new_version) + ".onClick()");
            // Send a screen view.
            t.send(new HitBuilders.ScreenViewBuilder().build());

            // This event will also be sent with the most recently set screen name.
            // Build and send an Event.
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("NEW_VERSION_ONCLICK")
                    .setAction("onClick")
                    .setLabel(lable)
                    .build());

            // Clear the screen name field when we're done.
            t.setScreenName(null);
        } catch (Exception e) {
            Log.i("NEW_VERSION_ONCLICK Tracker: ", e.getMessage());
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        // TODO Auto-generated method stub
        // kết nối API kiểm tra IMEI
        new CheckSIMIMEI(mContext, new String[]{DeviceInfo.SIM_IMEI, DeviceInfo.DEVICE_IMEI, DeviceInfo.ANDROID_OS_VERSION, DeviceInfo.MODEL_NUMBER});
        super.onDismiss(dialog);
    }
}
