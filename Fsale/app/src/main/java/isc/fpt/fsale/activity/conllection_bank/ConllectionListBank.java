package isc.fpt.fsale.activity.conllection_bank;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.Banking.GetInfoBanking;
import isc.fpt.fsale.action.Banking.GetListBanking;
import isc.fpt.fsale.adapter.conllection_bank.ConllectionBankAdapter;
import isc.fpt.fsale.callback.banking.onCollectionBanking;
import isc.fpt.fsale.model.Banking.ListBankingModel;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class ConllectionListBank extends BaseActivitySecond implements onCollectionBanking {

    private View loBack, loClose, loHeader;
    private ConllectionBankAdapter mAdapter;
    private RecyclerView mRecycler;
    List<ListBankingModel> mModel;

    @Override
    protected void initEvent() {
        loHeader.setVisibility(View.GONE);
        loBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_conllection_list_bank;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.img_back);
        loHeader = findViewById(R.id.text_header);
        mRecycler = (RecyclerView) findViewById(R.id.recycler_banking_list);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String[] value = {Constants.USERNAME};
        new GetListBanking(this, value, model -> {
            assert model != null;
            mModel = model;
            setupListBank();
        });
    }

    private void setupListBank() {
        mAdapter = new ConllectionBankAdapter(this, mModel, this);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void onCollectionBankingSuccess(String id) {
        String[] value = {Constants.USERNAME, id};
        new GetInfoBanking(this,value,models -> {
            String url = models.getResult();
            int urlID = models.getResultID();
            if(!url.equals("")) {
                Intent intent = new Intent(this, CollectionInfoBanking.class);
                intent.putExtra(Constants.TAG_RESULT_COLLECTION_BANKING, url);
                intent.putExtra(Constants.TAG_RESULT_COLLECTION_BANKING_ID, urlID);
                startActivity(intent);
            }
            else {
                Common.alertDialog(getString(R.string.msg_collection_banking_not_url), this);
            }
        });
    }
}
