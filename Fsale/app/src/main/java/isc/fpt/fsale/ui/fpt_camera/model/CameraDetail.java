package isc.fpt.fsale.ui.fpt_camera.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by haulc3 on 05,September,2019
 */
public class CameraDetail implements Parcelable {
    private int TypeID;
    private String TypeName;
    private int PackID;
    private String PackName;
    private int MinCam;
    private int MaxCam;
    private int Cost;
    private int Qty;
    private int ServiceType;
    private int Discount;
    private int TypePromotion;

    private boolean isSelected = false;

    public CameraDetail() {
    }

    public CameraDetail(int typeID, String typeName, int packID, String packName, int minCam, int maxCam, int cost, int qty, int serviceType, boolean isSelected) {
        TypeID = typeID;
        TypeName = typeName;
        PackID = packID;
        PackName = packName;
        MinCam = minCam;
        MaxCam = maxCam;
        Cost = cost;
        Qty = qty;
        ServiceType = serviceType;
        this.isSelected = isSelected;
    }

    public CameraDetail(int typeID, String typeName, int packID, String packName, int minCam, int maxCam, int cost, int qty, int serviceType, int discount, int typePromotion, boolean isSelected) {
        TypeID = typeID;
        TypeName = typeName;
        PackID = packID;
        PackName = packName;
        MinCam = minCam;
        MaxCam = maxCam;
        Cost = cost;
        Qty = qty;
        ServiceType = serviceType;
        Discount = discount;
        TypePromotion = typePromotion;
        this.isSelected = isSelected;
    }

    protected CameraDetail(Parcel in) {
        TypeID = in.readInt();
        TypeName = in.readString();
        PackID = in.readInt();
        PackName = in.readString();
        MinCam = in.readInt();
        MaxCam = in.readInt();
        Cost = in.readInt();
        Qty = in.readInt();
        ServiceType = in.readInt();
        Discount = in.readInt();
        TypePromotion = in.readInt();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(TypeID);
        dest.writeString(TypeName);
        dest.writeInt(PackID);
        dest.writeString(PackName);
        dest.writeInt(MinCam);
        dest.writeInt(MaxCam);
        dest.writeInt(Cost);
        dest.writeInt(Qty);
        dest.writeInt(ServiceType);
        dest.writeInt(Discount);
        dest.writeInt(TypePromotion);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CameraDetail> CREATOR = new Creator<CameraDetail>() {
        @Override
        public CameraDetail createFromParcel(Parcel in) {
            return new CameraDetail(in);
        }

        @Override
        public CameraDetail[] newArray(int size) {
            return new CameraDetail[size];
        }
    };

    public int getTypeID() {
        return TypeID;
    }

    public void setTypeID(int typeID) {
        TypeID = typeID;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }

    public int getPackID() {
        return PackID;
    }

    public void setPackID(int packID) {
        PackID = packID;
    }

    public String getPackName() {
        return PackName;
    }

    public void setPackName(String packName) {
        PackName = packName;
    }

    public int getMinCam() {
        return MinCam;
    }

    public void setMinCam(int minCam) {
        MinCam = minCam;
    }

    public int getMaxCam() {
        return MaxCam;
    }

    public void setMaxCam(int maxCam) {
        MaxCam = maxCam;
    }

    public int getCost() {
        return Cost;
    }

    public void setCost(int cost) {
        Cost = cost;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getDiscount() {
        return Discount;
    }

    public void setDiscount(int discount) {
        Discount = discount;
    }

    public int getTypePromotion() {
        return TypePromotion;
    }

    public void setTypePromotion(int typePromotion) {
        TypePromotion = typePromotion;
    }

    public JSONObject toJsonObjectCamera() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("TypeID", getTypeID());
            jsonObject.put("TypeName", getTypeName());
            jsonObject.put("PackID", getPackID());
            jsonObject.put("PackName", getPackName());
            jsonObject.put("MinCam", getMinCam());
            jsonObject.put("MaxCam", getMaxCam());
            jsonObject.put("Cost", getCost());
            jsonObject.put("Qty", getQty());
            jsonObject.put("ServiceType", getServiceType());
            jsonObject.put("Discount", String.valueOf(getDiscount()));
            jsonObject.put("TypePromotion", String.valueOf(getTypePromotion()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
