package isc.fpt.fsale.adapter;


import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportSubscriberGrowthForManagerModel;
import isc.fpt.fsale.utils.Common;
import java.util.ArrayList;

import isc.fpt.fsale.activity.ListReportNewObjectActivity;
import isc.fpt.fsale.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ReportSubscriberGrowthForManagerAdapter extends BaseAdapter {
	 
	private ArrayList<ReportSubscriberGrowthForManagerModel> mlist;
	private Context mContext;
	/*private LinearLayout frm_title_adsl,frm_title_ftth, frm_title_fti, frm_title_gpon,frm_title_onetv, 
	 frm_adsl_detail, frm_ftth_detail, frm_fti_detail, frm_gpon_detail, frm_onetv_detail;
	private View vw_line_adsl,vw_line_ftth, vw_line_fti, vw_line_gpon, vw_line_onetv;
	private TextView txt_adsl_title, txt_ftth_title, txt_fti_title, txt_gpon_title, txt_onetv_title;*/
	//Add by DuHK: Thêm thông tin Tháng và Năm lấy báo cáo để lấy danh sách hợp đồng theo gói dịch vụ(LocalType)
	private int mDay = 0;
	private int mMonth = 0;
	private int mYear = 0;
	private int mAgent = 0;
	private String mAgentName = "";
	
	private String FromDate, ToDate;
	
	/*public ReportSubscriberGrowthForManagerAdapter(Context mContext,ArrayList<ReportSubscriberGrowthForManagerModel> mlist)
	{
		this.mContext=mContext;
		this.mlist=mlist;
	
	}*/
	
	public ReportSubscriberGrowthForManagerAdapter(Context mContext,ArrayList<ReportSubscriberGrowthForManagerModel> mlist, int Day, int Month, int Year, int Agent, String AgentName)
	{
		this.mContext=mContext;
		this.mlist=mlist;
		this.mDay = Day;
		this.mMonth = Month;
		this.mYear = Year;
		this.mAgentName = AgentName;
		this.mAgent = Agent;
	}
	
	public ReportSubscriberGrowthForManagerAdapter(Context mContext,ArrayList<ReportSubscriberGrowthForManagerModel> mlist, String FromDate, String ToDate, int Agent, String AgentName)
	{
		this.mContext=mContext;
		this.mlist=mlist;
		this.FromDate = FromDate;
		this.ToDate = ToDate;
		this.mAgentName = AgentName;
		this.mAgent = Agent;
	}
	
	
	@Override
	public int getCount()
	{
		return this.mlist.size();
	}
	
	@Override
	public Object getItem(int position)
	{
		return this.mlist.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	
		final ReportSubscriberGrowthForManagerModel item = mlist.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_subscriber_growth_for_manger,null);
		}
		try {
			TextView rowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			TextView saleName = (TextView)convertView.findViewById(R.id.lbl_report_sale_name);
			LinearLayout frmData = (LinearLayout)convertView.findViewById(R.id.frm_report_data);
			if(frmData.getChildCount()>0){
				frmData.removeAllViews();
			}
			rowNumber.setText(String.valueOf(position + 1));
			
			if(item != null){				
				
				saleName.setText(item.getSaleName());				
				
				for(int i = 0; i<item.getReportList().size() ;i++){
					final KeyValuePairModel subItem = item.getReportList().get(i);
					View viewChild = LayoutInflater.from(mContext).inflate( R.layout.sub_row_report_total, null );
					LinearLayout frmDataRow = (LinearLayout)viewChild.findViewById(R.id.frm_report_data_row);
					TextView lblTitle = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_title);
					TextView lblTotal = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_total);
					
					frmDataRow.setVisibility(View.VISIBLE);				
					
					lblTotal.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							/*Intent intent = new Intent(mContext, ListReportSBIDetailActivity.class);
							intent.putExtra("USER_NAME", item.getSaleName());
							intent.putExtra("DAY", mDay);
							intent.putExtra("MONTH", mMonth);
							intent.putExtra("YEAR", mYear);							
							intent.putExtra("LOCAL_TYPE", subItem.getID());
							mContext.startActivity(intent);*/
							Intent intent = new Intent(mContext, ListReportNewObjectActivity.class);
							intent.putExtra("Day", mDay);//Ngày lấy báo cáo
							intent.putExtra("Month", mMonth);//Tháng lấy báo cáo;
							intent.putExtra("Year", mYear);
							intent.putExtra("SaleAccount", item.getSaleName());
							intent.putExtra("LocalType", subItem.getID());
							intent.putExtra("Agent", mAgent);
							intent.putExtra("AgentName", mAgentName);
							intent.putExtra("FromDate", FromDate);
							intent.putExtra("ToDate", ToDate);
							mContext.startActivity(intent);	
							
						}
					});
					
					lblTitle.setText(subItem.getDescription() + ":");
					lblTotal.setText(subItem.getHint());
					frmData.addView(frmDataRow);
				}
			}
				
		} catch (Exception e) {

			Common.alertDialog("ReportSubscriberGrowthForManagerAdapter.getView():" + e.getMessage(), mContext);
		}				
		return convertView;
	}
		
	
}

