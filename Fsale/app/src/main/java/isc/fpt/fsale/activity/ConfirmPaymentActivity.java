package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import com.danh32.fontify.TextView;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.model.PaymentInformationResult;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// màn hình xác nhận thanh toán bán mới
public class ConfirmPaymentActivity extends BaseActivity {
    private PaymentInformationResult payInfoPublic = null;
    private String paidTypeValue = "";
    private Context mContext;
    private TextView tvRegCode, tvContract, tvCusName, tvPhone, tvPayType, tvTotal, tvStatus;
    private Button btnBackToMainScreen, btnGoToAppointmentScreen;
    public static int backHome = 0;
    private RegistrationDetailModel mRegister;

    public ConfirmPaymentActivity() {
        super(R.string.confirm_payment_header_activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.confirm_payment_header_activity));
        setContentView(R.layout.activity_confirm_payment);
        this.mContext = this;
        //Get data Intent
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        payInfoPublic = intent.getParcelableExtra("CONFIRM_PAYMENT_INFO");
        paidTypeValue = intent.getStringExtra("PAID_TYPE_VALUE");
        mRegister = intent.getParcelableExtra(Constants.MODEL_REGISTER);
        //Set view

        //Create widget
        createWidgets();
        //Create events
        createEvents();
        //Show data on text
        showDataOnText();
    }

    private void createWidgets() {
        tvRegCode = (TextView) findViewById(R.id.tvRegCodeConfirmPayment);
        tvContract = (TextView) findViewById(R.id.tvContractCodeConfirmPayment);
        tvCusName = (TextView) findViewById(R.id.tvCusNameConfirmPayment);
        tvPhone = (TextView) findViewById(R.id.tvPhoneConfirmPayment);
        tvPayType = (TextView) findViewById(R.id.tvPayTypeConfirmPayment);
        tvTotal = (TextView) findViewById(R.id.tvTotalConfirmPayment);
        tvStatus = (TextView) findViewById(R.id.tvStatusConfirmPayment);
        btnBackToMainScreen = (Button) findViewById(R.id.btn_back_to_main_screen);
        btnGoToAppointmentScreen = (Button) findViewById(R.id.btn_go_to_appointment);
    }

    private void createEvents() {
        //Back button
        btnBackToMainScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                //Finish confirm payment activity
                ConfirmPaymentActivity.this.finish();
            }
        });

        //added by haulc3
        btnGoToAppointmentScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backHome = 1;
                Intent intent = new Intent(mContext, DeployAppointmentActivity.class);
                intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                //Finish confirm payment activity
                ConfirmPaymentActivity.this.finish();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void showDataOnText() {
        tvRegCode.setText(payInfoPublic.getRegCode());
        tvContract.setText(payInfoPublic.getContract());
        tvCusName.setText(payInfoPublic.getFullName());
        tvPhone.setText(payInfoPublic.getPhoneNumber());
        tvPayType.setText(paidTypeValue);
        tvTotal.setText(Common.formatNumber(payInfoPublic.getTotalUnpaidReceipts()));
        tvStatus.setText("Thanh toán thành công");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    public void onBackPressed() {
        if (mRegister != null) {
            new GetRegistrationDetail(this, mRegister.getUserName(), mRegister.getID(), true);
        } else {
            finish();
        }
    }
}
