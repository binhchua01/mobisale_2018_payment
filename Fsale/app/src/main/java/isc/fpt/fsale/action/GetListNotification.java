package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListNotificationActivity;
import isc.fpt.fsale.model.BaseObjRes;
import isc.fpt.fsale.model.Notification;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by HCM.TUANTT14 on 3/14/2018.
 * <p>
 * update by haulc3
 */

public class GetListNotification implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private ListNotificationActivity activity;

    public GetListNotification(Context mContext, int pageCurrent) {
        this.mContext = mContext;
        this.activity = (ListNotificationActivity) mContext;
        String[] arrParamName = new String[]{"UserName", "PageNumber"};
        String[] arrParamValue = new String[]{Constants.USERNAME, String.valueOf(pageCurrent)};
        String message = mContext.getResources().getString(
                R.string.msg_pd_get_list_notification);
        String GET_LIST_NOTIFICATION = "GetListNotification";
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_NOTIFICATION, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetListNotification.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<Notification> lstNotification = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj.has(Constants.RESPONSE_RESULT)) {
                    BaseObjRes objRes = new Gson().fromJson(jsObj.getJSONObject(Constants.RESPONSE_RESULT).toString(), BaseObjRes.class
                    );
                    if (objRes.getErrorCode() == 0) {
                        for (int i = 0; i < objRes.getListObject().size(); i++) {
                            lstNotification.add(new Gson().fromJson(
                                    new Gson().toJsonTree(objRes.getListObject().get(i)).getAsJsonObject(),
                                    Notification.class
                            ));
                        }
                        if (activity != null) {
                            activity.loadListNotification(lstNotification);
                        }
                    } else {
                        Common.alertDialog(objRes.getError(), mContext);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}