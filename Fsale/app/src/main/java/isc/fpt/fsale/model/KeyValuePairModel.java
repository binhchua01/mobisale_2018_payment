package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/*
 * MODEL: 		KeyValuePairModel
 * include 2 attribute: ID and description
 *
 * @author: vandn
 * @created on:	12/08/2013
 */
public class KeyValuePairModel implements Parcelable {
    private int ID;//iID
    private String sID;
    private String Name;//description
    private String hint; // to use location list with code of region
    private int type;
    private String action;
    private boolean isAction;
    private int color = -1;
    private int ability;

    public KeyValuePairModel() { }

    protected KeyValuePairModel(Parcel in) {
        ID = in.readInt();
        sID = in.readString();
        Name = in.readString();
        hint = in.readString();
        type = in.readInt();
        action = in.readString();
        isAction = in.readByte() != 0;
        color = in.readInt();
        ability = in.readInt();
    }

    public static final Creator<KeyValuePairModel> CREATOR = new Creator<KeyValuePairModel>() {
        @Override
        public KeyValuePairModel createFromParcel(Parcel in) {
            return new KeyValuePairModel(in);
        }

        @Override
        public KeyValuePairModel[] newArray(int size) {
            return new KeyValuePairModel[size];
        }
    };

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getsID() {
        return sID;
    }

    public void setsID(String sID) {
        this.sID = sID;
    }

    public KeyValuePairModel(int iD, String desc) {
        this.ID = iD;
        this.Name = desc;
    }

    public KeyValuePairModel(String iD, String desc, int color, int ability) {
        this.sID = iD;
        this.Name = desc;
        this.color = color;
        this.ability = ability;
    }

    public KeyValuePairModel(String sID, String desc) {
        this.sID = sID;
        this.Name = desc;
    }

    public KeyValuePairModel(String sID, String desc, String action, boolean isAction) {
        this.sID = sID;
        this.Name = desc;
        this.action = action;
        this.isAction = isAction;
    }

    public KeyValuePairModel(String sID, String desc, String hint) {
        this.sID = sID;
        this.Name = desc;
        this.hint = hint;
    }

    public KeyValuePairModel(int iD, String desc, String hint) {
        this.ID = iD;
        this.Name = desc;
        this.hint = hint;
    }

    public KeyValuePairModel(int iD, String desc, String hint, int type) {
        this.ID = iD;
        this.Name = desc;
        this.hint = hint;
        this.type = type;
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        this.ID = iD;
    }

    public String getDescription() {
        return Name;
    }

    public void setDescription(String description) {
        this.Name = description;
    }

    public String getAction() {
        return action;
    }

    public void setAction(int action) {
        action = action;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public int getAbility() {
        return ability;
    }

    public void setAbility(int ability) {
        this.ability = ability;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeString(sID);
        dest.writeString(Name);
        dest.writeString(hint);
        dest.writeInt(type);
        dest.writeString(action);
        dest.writeByte((byte) (isAction ? 1 : 0));
        dest.writeInt(color);
        dest.writeInt(ability);
    }
}
