package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ConfirmPaymentActivity;
import isc.fpt.fsale.activity.PaymentOnlineActivity;
import isc.fpt.fsale.activity.conllection_bank.CollectionInfoBanking;
import isc.fpt.fsale.model.PaymentInformationResult;
import isc.fpt.fsale.model.QRCodeResult;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.StartPaymentPOST;
import isc.fpt.fsale.model.StartPaymentResult;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// API thanh toán online tiền mặt, QRCODE, TPBANK, HIFPT bán mới
public class StartPayment implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    // loại thanh toán
    private String paidTypeValue;
    private RegistrationDetailModel registrationDetailModel;
    private PaymentInformationResult payInfoPublic = null;

    //Constructor
    public StartPayment(Context context, StartPaymentPOST mObject, String paidTypeValue,
                        PaymentInformationResult payObject, RegistrationDetailModel registrationDetailModel) {
        try {
            this.mContext = context;
            this.paidTypeValue = paidTypeValue;
            this.payInfoPublic = payObject;
            this.registrationDetailModel = registrationDetailModel;
            //Declares variables
            String[] arrParamName = new String[]{"SaleName", "RegCode", "Contract", "PaidType", "Total"};
            String[] arrParamValue = new String[]{mObject.getSaleMan(), mObject.getRegCode(),
                    mObject.getContract(), String.valueOf(mObject.getPaidType()), String.valueOf(mObject.getTotal())};
            String message = mContext.getResources().getString(R.string.msg_pd_process_complete_payment);
            //    private final String START_PAYMENT = "StartPayment";
            String START_PAYMENT = "CreateReceiptRegistration";
            CallServiceTask service = new CallServiceTask(mContext, START_PAYMENT,
                    arrParamName, arrParamValue, Services.JSON_POST, message, StartPayment.this);
            service.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<StartPaymentResult> objList;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject("ResponseResult");//StartPaymentResult
                WSObjectsModel<StartPaymentResult> resultObject = new
                        WSObjectsModel<>(jsObj, StartPaymentResult.class, QRCodeResult.class.getName());
                if (resultObject.getErrorCode() == 0) {
                    objList = resultObject.getArrayListObject();
                    if (objList.size() > 0) {
                        StartPaymentResult startPaymentResult = objList.get(0);
                        if (startPaymentResult.getId() > 0) {
                            switch (startPaymentResult.getId()) {
                                //kết quả thanh toán thẻ quốc tế và nội địa
                                case 30:
                                case 40:
                                case 80:
                                    Intent intentQrCode = new Intent(mContext, PaymentOnlineActivity.class);
                                    intentQrCode.putExtra("PAID_TYPE_SELECTED", startPaymentResult.getId());
                                    intentQrCode.putExtra("PAID_TYPE_VALUE", this.paidTypeValue);
                                    intentQrCode.putExtra("BASE64_QR_CODE", startPaymentResult.getQrCodeResult().getQrcode());
                                    intentQrCode.putExtra("CONFIRM_PAYMENT_INFO", this.payInfoPublic);
                                    mContext.startActivity(intentQrCode);
                                    break;
                                //kết quả thanh toán TPBANK
                                case 50:
                                    Intent intentTPBank = new Intent(mContext, PaymentOnlineActivity.class);
                                    intentTPBank.putExtra("PAID_TYPE_SELECTED", startPaymentResult.getId());
                                    intentTPBank.putExtra("PAID_TYPE_VALUE", this.paidTypeValue);
                                    intentTPBank.putExtra("CONFIRM_PAYMENT_INFO", this.payInfoPublic);
                                    mContext.startActivity(intentTPBank);
                                    break;
                                //kết quả thanh toán tiền mặt
                                case 10:
                                case 100:
                                    if (Common.checkLifeActivity(mContext)) {
                                        new AlertDialog.Builder(mContext)
                                                .setTitle("Thông Báo")
                                                .setMessage(objList.get(0).getMessage())
                                                .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        Intent confirmIntent = new Intent(mContext, ConfirmPaymentActivity.class);
                                                        confirmIntent.putExtra(Constants.MODEL_REGISTER, registrationDetailModel);
                                                        confirmIntent.putExtra("PAID_TYPE_VALUE", paidTypeValue);
                                                        confirmIntent.putExtra("CONFIRM_PAYMENT_INFO", payInfoPublic);
                                                        mContext.startActivity(confirmIntent);
                                                        dialog.cancel();
                                                        ((Activity) mContext).finish();
                                                    }
                                                }).setCancelable(false).create().show();
                                    }
                                    break;
                                case 60:
                                    Intent intent = new Intent(mContext, CollectionInfoBanking.class);
                                    intent.putExtra(Constants.TAG_RESULT_COLLECTION_BANKING, startPaymentResult.getMBBankGateway());
                                    mContext.startActivity(intent);
                                    break;
                                //kết quả thanh toán HiFpt hoặc các loại thanh toán chưa biết
                                case 20:
                                case 70:
                                default:
                                    Common.alertDialog(objList.get(0).getMessage(), mContext);
                                    break;
                            }
                        } else {
                            Common.alertDialog(resultObject.getError(), mContext);
                        }
                    } else {
                        Common.alertDialog(resultObject.getError(), mContext);
                    }
                } else {
                    Common.alertDialog(resultObject.getError(), mContext);
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }
}