package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PTCDetailModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.action.GetPTCDetail;
import isc.fpt.fsale.action.UpdatePTCStatusForNotApproval;
import isc.fpt.fsale.utils.Constants;


import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

//màn hình THÔNG TIN PTC
public class PTCDetailActivity extends BaseActivity {
    private TextView lblFullName, lblContract, lblPhoneNumber, lblAddress;
    private TextView lblPTCStatus, lblPTCCreateBy, lblPTCCreateDate;
    private TextView lblSupporterName, lblSupporterPhone, lblSupporterEmail;
    private RegistrationDetailModel mRegister;
    private PTCDetailModel mPTC;

    public PTCDetailActivity() {
        super(R.string.lbl_screen_name_ptc_detail);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_ptc_detail));
        setContentView(R.layout.activity_ptc_detail);
        lblFullName = (TextView) findViewById(R.id.lbl_full_name);
        lblContract = (TextView) findViewById(R.id.lbl_contract);
        lblPhoneNumber = (TextView) findViewById(R.id.lbl_phone_number);
        lblAddress = (TextView) findViewById(R.id.lbl_address);
        lblPTCStatus = (TextView) findViewById(R.id.lbl_ptc_status);
        lblPTCCreateBy = (TextView) findViewById(R.id.lbl_ptc_create_by);
        lblPTCCreateDate = (TextView) findViewById(R.id.lbl_ptc_create_date);
        lblSupporterName = (TextView) findViewById(R.id.lbl_supporter_name);
        lblSupporterPhone = (TextView) findViewById(R.id.lbl_supporter_phone);
        lblSupporterEmail = (TextView) findViewById(R.id.lbl_supporter_email);
        Button btnChangeStatus = (Button) findViewById(R.id.btn_change_ptc_status);
        btnChangeStatus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmToUpdate();
            }
        });
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(RegisterDetailActivity.TAG_REGISTER)) {
            mRegister = intent.getParcelableExtra(RegisterDetailActivity.TAG_REGISTER);
        }
        getData();
    }

    public void getData() {
        if (mRegister != null) {
            new GetPTCDetail(this, mRegister.getContract());
        }
    }

    public void loadData(ArrayList<PTCDetailModel> lst) {
        if (lst != null && lst.size() > 0) {
            mPTC = lst.get(0);
            if (mPTC != null) {
                lblAddress.setText(mPTC.getAddress());
                lblContract.setText(mPTC.getContract());
                lblFullName.setText(mPTC.getFullName());
                lblPhoneNumber.setText(mPTC.getCustomerPhone());
                lblPTCCreateBy.setText(mPTC.getCreateBy());
                lblPTCCreateDate.setText(mPTC.getCreateDate());
                lblPTCStatus.setText(mPTC.getPTCStatusName());
                lblSupporterEmail.setText(mPTC.getStaffEmail());
                lblSupporterName.setText(mPTC.getStaffName());
                lblSupporterPhone.setText(mPTC.getStaffPhone());
            }
        } else {
            Common.alertDialog("Không có dữ liệu", this);
        }
    }

    private void confirmToUpdate() {
        String Message = "Bạn có muốn chuyển tình trạng Phiếu thi công?";
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Thông báo")
                .setMessage(Message)
                .setCancelable(false)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        update();
                    }
                })
                .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        dialog = builder.create();
        dialog.show();
    }

    private void update() {
        if (mPTC != null) {
            int Status;
            if (mPTC.getPTCStatus() == 0) {
                Status = 1;
            } else {
                Status = 2;
            }
            new UpdatePTCStatusForNotApproval(this, mPTC.getContract(), mPTC.getPTCId(), Status);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }
}
