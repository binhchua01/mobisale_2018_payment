package isc.fpt.fsale.ui.extra_ott.fragment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.ui.extra_ott.ExtraOttActivity;
import isc.fpt.fsale.ui.extra_ott.RegisterExtraOttModel;

/**
 * Created by haulc3 on 15,July,2019
 */
public class FragmentExtraOttTotal extends BaseFragment {
    public TextView tvEntertainmentTotal, tvTotal;
    private Button btnUpdate;
    private RadioButton rdoSendSMS, rdoLoadAuto;
    private RadioGroup grpAutoActive;
    private ExtraOttActivity activity;
    private RegisterExtraOttModel mRegisterExtraOttModel;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (ExtraOttActivity) getActivity();
        mRegisterExtraOttModel = activity.getRegisterModel();
    }

    @Override
    protected void initView(View mView) {
        tvEntertainmentTotal = (TextView) mView.findViewById(R.id.tv_entertainment_total);
        tvTotal = (TextView) mView.findViewById(R.id.tv_total);
        btnUpdate = (Button) mView.findViewById(R.id.btn_update_extra_ott);
        rdoSendSMS = (RadioButton) mView.findViewById(R.id.rdo_send_sms);
        rdoLoadAuto = (RadioButton) mView.findViewById(R.id.rdo_load_auto);
        grpAutoActive = (RadioGroup) mView.findViewById(R.id.grp_auto_active);
    }

    @Override
    protected void initEvent() {
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(activity != null){
                    activity.onUpdateRegistration(rdoSendSMS.isChecked()?0:1);
                }
            }
        });
    }

    @Override
    protected void bindData() {
        if(mRegisterExtraOttModel.getCartExtraOtt() != null && mRegisterExtraOttModel.getCartExtraOtt().getIsAutoActive() != null){
            if(mRegisterExtraOttModel.getCartExtraOtt().getIsAutoActive() == 1)
                grpAutoActive.check(R.id.rdo_load_auto);

        }
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_extra_ott_total;
    }
}
