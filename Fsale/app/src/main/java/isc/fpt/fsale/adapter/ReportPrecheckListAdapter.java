package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportPrecheckListModel;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReportPrecheckListAdapter  extends BaseAdapter {

	private ArrayList<ReportPrecheckListModel> mList;	
	private Context mContext;
	
	public ReportPrecheckListAdapter(Context context, ArrayList<ReportPrecheckListModel> lst){
		this.mContext = context;
		this.mList = lst;	
		
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportPrecheckListModel ReportPrecheckList = mList.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_prechecklist, null);
		}
		
		TextView txtRowNumber = (TextView) convertView.findViewById(R.id.txt_row_number);
		txtRowNumber.setText(ReportPrecheckList.getRowNumber());
		
		// Ten sale
		TextView txtSaleName = (TextView) convertView.findViewById(R.id.txt_SaleName);
		txtSaleName.setText(ReportPrecheckList.getSaleName());
		
		// So luong tong
		TextView txtTotal = (TextView) convertView.findViewById(R.id.txt_Total);
		txtTotal.setText(String.valueOf(ReportPrecheckList.getTotal()));
		
		// So luong phieu dang ky da len hop dong
		TextView txtTotalComplete = (TextView) convertView.findViewById(R.id.txt_TotalComplete);
		txtTotalComplete.setText(String.valueOf(ReportPrecheckList.getTotalComplete()));
		
		// So luong phieu dang ky chua len hop dong
		TextView txtTotalInComplete = (TextView) convertView.findViewById(R.id.txt_TotalInComplete);
		txtTotalInComplete.setText(String.valueOf(ReportPrecheckList.getTotalInComplete()));
		
		return convertView;
	}

}
