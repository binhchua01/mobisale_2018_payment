package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.ViewPagerListRegistration;
import isc.fpt.fsale.ui.fragment.FragmentListRegistration;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;

import com.slidingmenu.lib.SlidingMenu;

//màn hình chính DANH SÁCH THÔNG TIN KHÁCH HÀNG
public class RegistrationListNewActivity extends BaseActivity implements OnPageChangeListener {
    private ViewPager viewPager;

    public RegistrationListNewActivity() {
        super(R.string.lbl_screen_name_registration_list_activity);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_registration_list_activity));
        setContentView(R.layout.list_registration_new);
        viewPager = (ViewPager) findViewById(R.id.pager);
        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        initViewPager();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void initViewPager() {
        viewPager.setOffscreenPageLimit(3);
        ViewPagerListRegistration vpAdapter = new ViewPagerListRegistration(getSupportFragmentManager());
        viewPager.setAdapter(vpAdapter);
        viewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }


    @Override
    public void onPageSelected(int position) {
        /*Kiểm tra Valid các Tab & cập nhật lại PĐK nếu kiểm tra OK (isUpdateReg = true)*/
        FragmentListRegistration fragment = getSelectedFragment();
        if (fragment != null) {
            fragment.getData();
        }
    }


    public FragmentListRegistration getSelectedFragment() {
        try {
            return (FragmentListRegistration) getSupportFragmentManager()
                    .findFragmentByTag("android:switcher:" + R.id.pager + ":" + viewPager.getCurrentItem());
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
