package isc.fpt.fsale.ui.fragment;

import java.util.List;

import isc.fpt.fsale.action.GetPotentialObjDetail;
import isc.fpt.fsale.action.UpdatePotentialObjDetailList;
import isc.fpt.fsale.activity.ListPotentialObjSurveyListActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.PotentialObjSurveyListAdapter;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.PotentialObjSurveyModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

//các fragment KHẢO SÁT THÔNG TIN KHTN
public class FragmentPotentialObjSurveyList extends Fragment implements OnItemClickListener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_TITLE = "section_title";
    private ListPotentialObjSurveyListActivity activity;
    private ListView mListView;

    public static FragmentPotentialObjSurveyList newInstance(int sectionNumber, String sectionTitle) {
        FragmentPotentialObjSurveyList fragment = new FragmentPotentialObjSurveyList();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentPotentialObjSurveyList() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_potential_obj_survey_list, container, false);
        activity = (ListPotentialObjSurveyListActivity) getActivity();
        Common.setupUI(getActivity(), rootView);
        mListView = (ListView) rootView.findViewById(R.id.lv_object);
        mListView.setOnItemClickListener(this);
        Button btnUpdate = (Button) rootView.findViewById(R.id.btn_update);
        btnUpdate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkForUpdate()) {
                    update();
                }
            }
        });

        loadData();
        return rootView;
    }

    private void update() {
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(activity);
        builder.setMessage(getResources().getString(R.string.msg_confirm_update))
                .setCancelable(false)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new UpdatePotentialObjDetailList(
                                activity,
                                Constants.USERNAME,
                                activity.getPotentialObj(),
                                activity.getSurveyListAllTab(),
                                activity.getSupport()
                        );
                    }
                })
                .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        dialog = builder.create();
        dialog.show();
    }


    protected boolean checkForUpdate() {
        List<PotentialObjSurveyModel> lst = ((PotentialObjSurveyListAdapter) mListView.getAdapter()).getList();
        return lst != null && lst.size() > 0;
    }

    private void loadData() {
        try {
            if (activity != null) {
                PotentialObjSurveyListAdapter adapter = new PotentialObjSurveyListAdapter(
                        activity,
                        this,
                        activity.getSurveySubList(this.getTag())
                );
                mListView.setAdapter(adapter);
                updateCount();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<PotentialObjSurveyModel> getSurveyList() {
        if (mListView != null && mListView.getAdapter() != null) {
            return ((PotentialObjSurveyListAdapter) mListView.getAdapter()).getList();
        }
        return null;
    }

    public void updateCount() {
        if (activity != null) {
            activity.updateCount(getTag());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        try {
            Common.hideSoftKeyboard(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PotentialObjModel selectedItem = (PotentialObjModel) parentView.getItemAtPosition(position);
        if (selectedItem != null) {
            String userName = ((MyApp) activity.getApplication()).getUserName();
            new GetPotentialObjDetail(activity, userName, selectedItem.getID());
        }
    }
}