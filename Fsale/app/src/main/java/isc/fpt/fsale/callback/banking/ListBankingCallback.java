package isc.fpt.fsale.callback.banking;

import java.util.List;

import isc.fpt.fsale.model.Banking.ListBankingModel;

public interface ListBankingCallback {
    void onGetBankingSuccess(List<ListBankingModel> model);
}
