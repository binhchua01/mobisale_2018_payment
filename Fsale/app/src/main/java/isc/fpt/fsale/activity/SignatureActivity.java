package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.UploadImageMultipart;
import isc.fpt.fsale.model.UploadMultipartResult;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import static isc.fpt.fsale.utils.Common.getStringImage;
import static isc.fpt.fsale.utils.Constants.IDENTITY_MBS_VN;
import static isc.fpt.fsale.utils.Constants.TOKEN;

// màn hình ký tên
public class SignatureActivity extends BaseActivitySecond {
    private SignatureDraw signatureDraw;
    private int regID;
    private String sBitmap;
    private Context context;
    private RelativeLayout rltBack;
    private Button btnClear, btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        getDataIntent();
    }

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnClear.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                signatureDraw.clear();
            }
        });
        btnSave.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                    } else {
                        saveSignature();
                    }
                } else {
                    saveSignature();
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_customer_signature_deposit;
    }

    @Override
    protected void initView() {
        LinearLayout parent = (LinearLayout) findViewById(R.id.signature_deposit);
        signatureDraw = new SignatureDraw(this);
        parent.addView(signatureDraw);
        parent.setDrawingCacheEnabled(true);
        btnClear = (Button) findViewById(R.id.btn_signature_clear);
        btnSave = (Button) findViewById(R.id.btn_signature_confirm);
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
    }

    private void getDataIntent() {
        Intent intent = getIntent();
        regID = intent.getIntExtra("RegID", 0);
    }

    private void saveSignature() {
        if (signatureDraw.isDrawed()) {
            Bitmap bm = signatureDraw.getBitmap();
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/Mobisale Images/");
            myDir.mkdirs();
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = "Image-" + n + ".jpg";
            File file = new File(myDir, fname);
            if (file.exists())
                file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                bm.compress(Bitmap.CompressFormat.PNG, 60, out);
                out.flush();
                out.close();
            } catch (Exception e) {

                e.printStackTrace();
            }
            bm = Common.scaleBitmapSmaller(file);
            sBitmap = getStringImage(bm);
            String saleName = ((MyApp) context.getApplicationContext()).getUserName();
            String[] paramNames = new String[]{"LinkId", "Source", "Type", "SaleName", "Token"};
            int TYPE_FILE_UPLOAD = 2;
            String[] paramValues = new String[]{String.valueOf(regID), String.valueOf(IDENTITY_MBS_VN),
                    String.valueOf(TYPE_FILE_UPLOAD), saleName, TOKEN};
            // Kết nối API Upload ảnh multipart
            new UploadImageMultipart(context, bm, paramNames, paramValues);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    public void updateSignature(UploadMultipartResult uploadMultipartResult) {
        alertDialogResult(uploadMultipartResult, this);
    }

    public void alertDialogResult(final UploadMultipartResult uploadMultipartResult, Context mContext) {
        String html;
        if (uploadMultipartResult != null) {
            final String status = uploadMultipartResult.getStatus();
            if (status != null && status.equals("Success")) {
                html = "Cập nhật chữ ký thành công.";
            } else {
                html = "Cập nhật chữ ký thất bại.Bạn có muốn ký lại.";
            }
            new AlertDialog.Builder(mContext)
                    .setTitle(R.string.title_notification)
                    .setMessage(html)
                    .setPositiveButton(R.string.lbl_ok,
                            new Dialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    assert status != null;
                                    if (status.equals("Success")) {
                                        Intent returnIntent = new Intent();
                                        returnIntent.putExtra("image", sBitmap);
                                        returnIntent.putExtra("Id", String.valueOf(uploadMultipartResult.getData().get(0).getId()));
                                        setResult(Activity.RESULT_OK, returnIntent);
                                        finish();
                                    }
                                    dialog.dismiss();
                                }
                            })
                    .setCancelable(true)
                    .create()
                    .show();
        } else {
            Common.alertDialogNotTitle("Cập nhật chữ ký thất bại.", mContext);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.signature, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = context.getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(context, message);
                return;
            }
        }
        if (requestCode == 2) {
            saveSignature();
        }
    }
}
