package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by haulc3 on 17,July,2019
 */
public class District implements Serializable {
    @SerializedName("ErrorService")
    @Expose
    private Object errorService;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("FullNameVN")
    @Expose
    private String fullNameVN;
    @SerializedName("Name")
    @Expose
    private String name;

    public Object getErrorService() {
        return errorService;
    }

    public void setErrorService(Object errorService) {
        this.errorService = errorService;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullNameVN() {
        return fullNameVN;
    }

    public void setFullNameVN(String fullNameVN) {
        this.fullNameVN = fullNameVN;
    }
}
