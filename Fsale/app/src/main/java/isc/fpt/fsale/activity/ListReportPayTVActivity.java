package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetReportPayTV;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ReportPayTVAdapter;
import isc.fpt.fsale.ui.fragment.DatePickerReportDialog;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportItemModel;
import isc.fpt.fsale.model.ReportPayTVModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class ListReportPayTVActivity extends BaseActivity implements OnDateSetListener {
    private Spinner spAgent;
    private ListView lvResult;
    private LinearLayout frmFind;
    private ImageView imgNavigation;
    private Spinner spPage;
    private EditText txtAgentName;
    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
    private int Agent = 0;
    private String AgentName;
    private EditText txtFromDate, txtToDate;
    private DatePickerReportDialog mDateDialog;
    private Calendar calFromDate, calToDate;
    private TextView lblSumTotal;

    public ListReportPayTVActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.menu_report_pay_tv);
    }

    @SuppressLint("InflateParams")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.menu_report_pay_tv));
        setContentView(R.layout.activity_report_pay_tv);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        lvResult = (ListView) findViewById(R.id.lv_report);
        txtAgentName = (EditText) findViewById(R.id.txt_agent_name);

        final Button btnFind = (Button) findViewById(R.id.btn_find);
        imgNavigation = (ImageView) findViewById(R.id.img_navigation_drop_down);
        frmFind = (LinearLayout) findViewById(R.id.frm_find);
        btnFind.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (FLAG_FIRST_LOAD > 0)
                    FLAG_FIRST_LOAD = 0;
                getReport(1);
            }
        });
        imgNavigation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dropDownNavigation();
            }
        });
        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getReport(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        spAgent = (Spinner) findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() > 0) {
                    txtAgentName.setEnabled(true);
                    txtAgentName.requestFocus();
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        txtFromDate = (EditText) findViewById(R.id.txt_from_date);
        txtFromDate.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (hasFocus) {
                    if (txtFromDate.getText().toString().equals("")) {
                        Calendar date = Calendar.getInstance();
                        date.set(Calendar.DAY_OF_MONTH, 1);
                        calFromDate = date;
                        String s = Common.getSimpleDateFormat(date, Constants.DATE_FORMAT_VN);
                        txtFromDate.setText(s);
                    } else {
                        String title = getString(R.string.title_dialog_from_date);
                        initDatePickerDialog(txtFromDate, txtToDate, title);
                        if (mDateDialog != null) {
                            mDateDialog.setCancelable(false);
                            mDateDialog.show(getSupportFragmentManager(), "datePicker");
                        }
                    }
                    txtFromDate.setError(null);
                }
            }
        });
        txtFromDate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String title = getString(R.string.title_dialog_from_date);
                initDatePickerDialog(txtFromDate, txtToDate, title);
                if (mDateDialog != null) {
                    mDateDialog.setCancelable(false);
                    mDateDialog.show(getSupportFragmentManager(), "datePicker");
                }
            }
        });
        txtFromDate.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtFromDate.setError(null);
            }
        });
        txtToDate = (EditText) findViewById(R.id.txt_to_date);
        txtToDate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String title = getString(R.string.title_dialog_to_date);
                initDatePickerDialog(txtFromDate, txtToDate, title);
                if (mDateDialog != null) {
                    mDateDialog.setCancelable(false);
                    mDateDialog.show(getSupportFragmentManager(), "datePicker");
                }
            }
        });

        txtToDate.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (hasFocus) {
                    String title = getString(R.string.title_dialog_to_date);
                    initDatePickerDialog(txtFromDate, txtToDate, title);
                    if (mDateDialog != null) {
                        mDateDialog.setCancelable(false);
                        mDateDialog.show(getSupportFragmentManager(), "datePicker");
                    }
                    txtToDate.setError(null);
                }
            }
        });

        txtToDate.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtToDate.setError(null);
            }
        });
        lblSumTotal = (TextView) findViewById(R.id.lbl_sum_total);
        initSpinnerAgent();
        setToDateText();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void setToDateText() {
        if (txtToDate != null) {
            Calendar date = Calendar.getInstance();
            calToDate = date;
            String s = Common.getSimpleDateFormat(date, Constants.DATE_FORMAT_VN);
            txtToDate.setText(s);
        }
    }

    private void initSpinnerAgent() {
        ArrayList<KeyValuePairModel> listAgent = new ArrayList<>();
        listAgent.add(new KeyValuePairModel(0, "Tất cả"));
        listAgent.add(new KeyValuePairModel(1, "Nhân viên"));
        KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(this, R.layout.my_spinner_style, listAgent, Gravity.LEFT);
        spAgent.setAdapter(adapterStatus);
    }

    private void initDatePickerDialog(EditText txtValue, EditText txtValue1, String dialogTitle) {
        Calendar minDate = Calendar.getInstance(), starDate = Calendar.getInstance(), maxDate = Calendar.getInstance();
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        minDate.set(Calendar.MONTH, 0);
        minDate.add(Calendar.YEAR, -1);
        if (txtValue == txtFromDate && txtValue != null) {
            starDate.set(Calendar.DAY_OF_MONTH, 1);
        }
        maxDate.add(Calendar.DAY_OF_MONTH, 1);
        mDateDialog = new DatePickerReportDialog(this, starDate, minDate, maxDate, dialogTitle, txtValue, txtValue1);
    }

    //========================================= Get/Load data ============================================
    private boolean checkValidDate() {
        if (calFromDate == null || txtFromDate.getText().toString().trim().equals("")) {
            txtFromDate.requestFocus();
            txtFromDate.setError("Chưa nhập giá trị");
            return false;
        }
        if (Agent > 0 && AgentName.equals("")) {
            txtAgentName.requestFocus();
            txtAgentName.setError(getString(R.string.msg_search_value_empty));
            return false;
        }
        return true;
    }

    private void getReport(int PageNumber) {
        if (checkValidDate()) {
            mPage = PageNumber;
            String fromDateValue = txtFromDate.getText().toString();
            String toDateValue = txtToDate.getText().toString();
//			int Month = calFromDate.get(Calendar.MONTH) + 1;
//			int Year = calFromDate.get(Calendar.YEAR);
            StringTokenizer stFromDate = new StringTokenizer(fromDateValue, "/");
            int month = 0, year = 0;
            String fromDate = "", toDate = "";
            while (stFromDate.hasMoreElements()) {
                String tFromDate = stFromDate.nextElement().toString();
                String tMonth = stFromDate.nextElement().toString();
                fromDate = tFromDate.substring(0, 1).equals("0") ? tFromDate.substring(1, 2) : tFromDate;
                month = tMonth.substring(0, 1).equals("0") ? Integer.valueOf(tMonth.substring(1, 2)) : Integer.valueOf(tMonth);
                year = Integer.valueOf(stFromDate.nextElement().toString());
            }
            String tToDate = toDateValue.substring(0, 2);
            toDate = tToDate.substring(0, 1).equals("0") ? tToDate.substring(1, 2) : tToDate;
            AgentName = txtAgentName.getText().toString().trim();
            Agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
            new GetReportPayTV(this, month, year, Agent, AgentName, PageNumber, fromDate, toDate);
        }
    }

    public void loadData(ArrayList<ReportPayTVModel> lst, String fromDate, String toDate, int month, int year) {
        if (lst != null && lst.size() > 0) {
            lblSumTotal.setText(String.valueOf(countTotal(lst)));
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                dropDownNavigation();
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            ReportPayTVAdapter adapter = new ReportPayTVAdapter(this, lst, fromDate, toDate, month, year);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
            lvResult.setAdapter(new ReportPayTVAdapter(this, new ArrayList<ReportPayTVModel>(), 0, 0));
        }
    }

    private int countTotal(ArrayList<ReportPayTVModel> lst) {
        int total = 0;
        if (lst != null && lst.size() > 0) {
            for (ReportPayTVModel item : lst) {
                if (item.getItems() != null && item.getItems().size() > 0) {
                    for (ReportItemModel subItem : item.getItems()) {
                        total += subItem.getTotal();
                    }
                }
            }
        }
        return total;
    }

    //TODO: dropDown Search frm
    private void dropDownNavigation() {
        if (frmFind.getVisibility() == View.VISIBLE) {
            frmFind.setVisibility(View.GONE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
        } else {
            frmFind.setVisibility(View.VISIBLE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        // TODO Auto-generated method stub
        Calendar date = Calendar.getInstance();
        date.set(year, monthOfYear, dayOfMonth);
        if (mDateDialog.getEditText() == txtFromDate && txtFromDate != null) {
            calFromDate = date;
            calFromDate.set(Calendar.DAY_OF_MONTH, 1);
            txtFromDate.setText(Common.getSimpleDateFormat(calFromDate, Constants.DATE_FORMAT_VN));
        }
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
