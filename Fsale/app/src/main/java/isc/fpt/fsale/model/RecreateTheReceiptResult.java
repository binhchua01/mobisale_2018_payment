package isc.fpt.fsale.model;

/**
 * Created by HCM.TUANTT14 on 4/27/2018.
 */

public class RecreateTheReceiptResult {
    private int ResultID;
    private String Result;

    public int getResultID() {
        return ResultID;
    }

    public void setResultID(int resultID) {
        ResultID = resultID;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }
}
