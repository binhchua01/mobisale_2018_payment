package isc.fpt.fsale.model;

/**
 * MODEL: 			User
 * @description: 	the model represents login user info 				  	
 * @author: 		vandn, on 08/08/2013
 * */
public class UserModel {
	private String username;
	private int isActive;//0: false, 1: true
	private int errorCode;
	private String description;	
	//Add by: DuHK, 25/01/2016
	private int IsCreateContact = 0;
	private int UseMPOS = 0;
	
	//
	private String LocationParent;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	public int IsCreateContact() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public int getErrorCode() {
		return errorCode;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	//
	public void setIsCreateContact(int IsCreateContact) {
		this.IsCreateContact = IsCreateContact;
	}
	public int getIsCreateContact() {
		return IsCreateContact;
	}
	//
	public String getLocationParent() {
		return LocationParent;
	}
	public void setLocationParent(String LocationParent) {
		this.LocationParent = LocationParent;
	}
	
	//
	public void setUseMPOS(int UseMPOS) {
		this.UseMPOS = UseMPOS;
	}
	public int getUseMPOS() {
		return UseMPOS;
	}
}
