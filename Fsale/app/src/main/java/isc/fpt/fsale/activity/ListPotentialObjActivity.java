package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.view.Menu;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentPotentialObjCEMListItem;
import isc.fpt.fsale.ui.fragment.FragmentPotentialObjListItem;
import isc.fpt.fsale.ui.fragment.FragmentPotentialObjListMap;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// giao diện chính màn hình DANH SÁCH KHÁCH HÀNG TIỀM NĂNG
public class ListPotentialObjActivity extends BaseActivity {
    public final String TAB_MAP = "POTENTIAL_MAP_FRAGMENT";
    public final String TAB_LIST = "POTENTIAL_LIST_ITEM_FRAGMENT";
    public final String TAB_CEM_LIST = "POTENTIAL_LIST_CEM_ITEM_FRAGMENT";

    public static FragmentManager fragmentManager;

    private List<PotentialObjModel> mListObject;
    private List<PotentialObjModel> mListCEMObject;

    public ListPotentialObjActivity() {
        super(R.string.lbl_screen_name_potential_obj_list);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_potential_obj_list));
        setContentView(R.layout.list_potentail_obj_list_map);
        fragmentManager = getSupportFragmentManager();
        //getSlidingMenu().setSlidingEnabled(false);

        FragmentTabHost mTabHost = (FragmentTabHost) findViewById(R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.tabFrameLayout);
        //tab DANH SÁCH KHÁCH HÀNG TIỀM NĂNG
        mTabHost.addTab(mTabHost.newTabSpec(TAB_LIST)
                .setIndicator(getString(R.string.lbl_potential_obj_list_item).toUpperCase()), FragmentPotentialObjListItem.class, null);
        // tab 18006000 KHÁCH HÀNG TIỀM NĂNG
        mTabHost.addTab(mTabHost.newTabSpec(TAB_CEM_LIST)
                .setIndicator(getString(R.string.lbl_potential_obj_list_cem_item).toUpperCase()), FragmentPotentialObjCEMListItem.class, null);
        //tab BẢN ĐỒ KHÁCH HÀNG TIỀM NĂNG
        mTabHost.addTab(mTabHost.newTabSpec(TAB_MAP)
                .setIndicator(getString(R.string.lbl_potential_obj_list_map).toUpperCase()), FragmentPotentialObjListMap.class, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    public List<PotentialObjModel> getPotentialList() {
        return this.mListObject;
    }

    public void setPotentialList(List<PotentialObjModel> lst) {
        this.mListObject = lst;
    }

    public List<PotentialObjModel> getPotentialCEMList() {
        return this.mListCEMObject;
    }

    public void setPotentialCEMList(List<PotentialObjModel> lst) {
        this.mListCEMObject = lst;
    }

    public void enableSlidingMenu(boolean enable) {
        getSlidingMenu().setSlidingEnabled(enable);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
