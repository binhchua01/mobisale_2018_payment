package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetDeptList;
import isc.fpt.fsale.action.GetReportPotentialObjTotal;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ReportPotentialObjTotalAdapter;
import isc.fpt.fsale.ui.fragment.DatePickerReportDialog;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportPotentialObjTotalModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class ListReportPotentialObjTotalActivity extends BaseActivity implements
        OnDateSetListener {

    private Spinner /* spDay, spMonth, spYear, */spAgent, spStatus;
    private ListView lvResult;
    private LinearLayout frmFind;
    private ImageView imgNavigation;
    private Spinner spPage, spDept;
    private EditText txtAgentName;

    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;// Cập nhật lại Spinner chỉ khi load
    // mới dữ liệu(Bấm nút Find)
    private int /* Day = 0, Month = 0, Year = 0, */Agent = 0, Status = 0;
    private String AgentName;
    //
    private EditText txtFromDate, txtToDate;
    private DatePickerReportDialog mDateDialog;
    private Calendar calFromDate, calToDate;
    private ImageButton imgReloadDept;
    //
    private TextView lblSumTotal;

    public ListReportPotentialObjTotalActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.menu_report_potential_obj);
    }

    /*
     * @Override public void startActivity(Intent intent) { // TODO
     * Auto-generated method stub finish(); super.startActivity(intent); }
     */

    @SuppressLint("InflateParams")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(),
                getString(R.string.lbl_screen_name_report_potential_total));
        setContentView(R.layout.activity_report_potential_obj_total);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        lvResult = (ListView) findViewById(R.id.lv_report);
        txtAgentName = (EditText) findViewById(R.id.txt_agent_name);

        final Button btnFind = (Button) findViewById(R.id.btn_find);
        imgNavigation = (ImageView) findViewById(R.id.img_navigation_drop_down);
        frmFind = (LinearLayout) findViewById(R.id.frm_find);
        btnFind.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (FLAG_FIRST_LOAD > 0)
                    FLAG_FIRST_LOAD = 0;
                getReport(1);
            }
        });

        imgNavigation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dropDownNavigation();
            }
        });

        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getReport(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spAgent = (Spinner) findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                if (selectedItem.getID() > 0) {
                    txtAgentName.setEnabled(true);
                    txtAgentName.requestFocus();
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spStatus = (Spinner) findViewById(R.id.sp_status);

        txtFromDate = (EditText) findViewById(R.id.txt_from_date);
        txtFromDate.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (hasFocus) {
                    if (txtFromDate.getText().toString().equals("")) {
                        Calendar date = Calendar.getInstance();
                        date.set(Calendar.DAY_OF_MONTH, 1);
                        calFromDate = date;
                        String s = Common.getSimpleDateFormat(date,
                                Constants.DATE_FORMAT_VN);
                        txtFromDate.setText(s);
                    } else {
                        String title = getString(R.string.title_dialog_from_date);
                        initDatePickerDialog(txtFromDate, title);
                        if (mDateDialog != null) {
                            mDateDialog.setCancelable(false);
                            mDateDialog.show(getSupportFragmentManager(),
                                    "datePicker");
                        }
                    }
                    txtFromDate.setError(null);
                }
            }
        });
        txtFromDate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String title = getString(R.string.title_dialog_from_date);
                initDatePickerDialog(txtFromDate, title);
                if (mDateDialog != null) {
                    mDateDialog.setCancelable(false);
                    mDateDialog.show(getSupportFragmentManager(), "datePicker");
                }
            }
        });

        txtFromDate.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtFromDate.setError(null);
            }
        });
        txtToDate = (EditText) findViewById(R.id.txt_to_date);
        txtToDate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String title = getString(R.string.title_dialog_to_date);
                initDatePickerDialog(txtToDate, title);
                if (mDateDialog != null) {
                    mDateDialog.setCancelable(false);
                    mDateDialog.show(getSupportFragmentManager(), "datePicker");
                }
            }
        });

        txtToDate.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (hasFocus) {
                    String title = getString(R.string.title_dialog_to_date);
                    initDatePickerDialog(txtToDate, title);
                    if (mDateDialog != null) {
                        mDateDialog.setCancelable(false);
                        mDateDialog.show(getSupportFragmentManager(),
                                "datePicker");
                    }
                    txtToDate.setError(null);
                }
            }
        });

        txtToDate.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtToDate.setError(null);
            }
        });

        //
        spDept = (Spinner) findViewById(R.id.sp_dept);
        imgReloadDept = (ImageButton) findViewById(R.id.img_reload_dept);
        imgReloadDept.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getDeptList();
            }
        });

        lblSumTotal = (TextView) findViewById(R.id.lbl_sum_total);

        setToDateText();
        initSpinnerAgent();
        initSpinnerStatus();
        //
        loadDept();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void setToDateText() {
        if (txtToDate != null) {
            Calendar date = Calendar.getInstance();
            calToDate = date;
            String s = Common.getSimpleDateFormat(date,
                    Constants.DATE_FORMAT_VN);
            txtToDate.setText(s);
        }
    }

    private void initSpinnerAgent() {
        ArrayList<KeyValuePairModel> listAgent = new ArrayList<KeyValuePairModel>();
        listAgent.add(new KeyValuePairModel(0, "Tất cả"));
        listAgent.add(new KeyValuePairModel(1, "Nhân viên"));
        KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(this,
                R.layout.my_spinner_style, listAgent, Gravity.LEFT);
        spAgent.setAdapter(adapterStatus);
    }

    private void initSpinnerStatus() {
        ArrayList<KeyValuePairModel> listAgent = new ArrayList<KeyValuePairModel>();
        listAgent.add(new KeyValuePairModel(2, "Tất cả"));
        listAgent.add(new KeyValuePairModel(0, "Chưa lên PĐK"));
        listAgent.add(new KeyValuePairModel(1, "Đã lên PĐK"));
        KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(this, R.layout.my_spinner_style, listAgent, Gravity.LEFT);
        spStatus.setAdapter(adapterStatus);
    }

    private void initDatePickerDialog(EditText txtValue, String dialogTitle) {
        Calendar minDate = Calendar.getInstance(), starDate = Calendar
                .getInstance(), maxDate = Calendar.getInstance();
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        minDate.set(Calendar.MONTH, 0);
        minDate.add(Calendar.YEAR, -1);
        if (txtValue == txtFromDate && txtValue != null) {
            starDate.set(Calendar.DAY_OF_MONTH, 1);
        }
        maxDate.add(Calendar.DAY_OF_MONTH, 1);
        mDateDialog = new DatePickerReportDialog(this, starDate, minDate,
                maxDate, dialogTitle, txtValue);
    }

    // ========================================= Get/Load data
    // ============================================
    private boolean checkValidDate() {
        if (calFromDate == null
                || txtFromDate.getText().toString().trim().equals("")) {
            txtFromDate.requestFocus();
            txtFromDate.setError("Chưa nhập giá trị");
            return false;
        }
        if (calToDate == null
                || txtToDate.getText().toString().trim().equals("")) {
            // txtToDate.requestFocus();
            txtToDate.setError("Chưa nhập giá trị");
            return false;
        }
        if (((KeyValuePairModel) spAgent.getSelectedItem()).getID() > 0
                && txtAgentName.getText().toString().trim().equals("")) {
            txtAgentName.requestFocus();
            txtAgentName.setError(getString(R.string.msg_search_value_empty));
            return false;
        } else {
            txtAgentName.setError(null);
        }
        /*
         * if(calFromDate.compareTo(calToDate) >= 0){
         * Common.alertDialog("Ngày bắt đầu lớn hơn hoặc bằng ngày kết thúc",
         * this); return false; }
         */
        return true;
    }

    private void getReport(int PageNumber) {
        if (checkValidDate()) {
            mPage = PageNumber;
            String FromDate = "", ToDate = "";
            FromDate = Common.getSimpleDateFormat(calFromDate,
                    Constants.DATE_FORMAT);
            if (calToDate != null) {
                ToDate = Common.getSimpleDateFormat(calToDate,
                        Constants.DATE_FORMAT);
            }
            Status = ((KeyValuePairModel) spStatus.getSelectedItem()).getID();
            AgentName = txtAgentName.getText().toString().trim();
            Agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
            int dept = 0;
            try {
                if (spDept.getAdapter() != null
                        && spDept.getSelectedItem() != null) {
                    dept = ((KeyValuePairModel) spDept.getSelectedItem())
                            .getID();
                }
            } catch (Exception e) {

                e.printStackTrace();
            }
            new GetReportPotentialObjTotal(this, Constants.USERNAME, FromDate,
                    ToDate, Status, Agent, AgentName, PageNumber, dept);
        }
    }

    public void LoadData(List<ReportPotentialObjTotalModel> lst,
                         String fromDate, String toDate, int Status) {
        if (lst != null && lst.size() > 0) {
            lblSumTotal.setText(String.valueOf(lst.get(0).getSumTotal()));
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                dropDownNavigation();
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this,
                        R.layout.my_spinner_style, lstPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            ReportPotentialObjTotalAdapter adapter = new ReportPotentialObjTotalAdapter(
                    this, lst, fromDate, toDate, Status);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
            lvResult.setAdapter(new ReportPotentialObjTotalAdapter(this,
                    new ArrayList<ReportPotentialObjTotalModel>()));
        }
    }

    // TODO: dropDown Search frm
    private void dropDownNavigation() {
        if (frmFind.getVisibility() == View.VISIBLE) {
            frmFind.setVisibility(View.GONE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
        } else {
            frmFind.setVisibility(View.VISIBLE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear,
                          int dayOfMonth) {
        // TODO Auto-generated method stub
        Calendar date = Calendar.getInstance();
        date.set(year, monthOfYear, dayOfMonth);
        if (mDateDialog.getEditText() == txtFromDate && txtFromDate != null) {
            calFromDate = date;
            txtFromDate.setText(Common.getSimpleDateFormat(calFromDate,
                    Constants.DATE_FORMAT_VN));
        } else if (mDateDialog.getEditText() == txtToDate && txtToDate != null) {
            calToDate = date;
            txtToDate.setText(Common.getSimpleDateFormat(calToDate,
                    Constants.DATE_FORMAT_VN));
        }
    }

    /* ============================== Phòng IBB ======================== */
    // Neu chưa load Dept lần nào thì gọi API/ có thì load SharPrefercence
    private void loadDept() {
        if (Common.hasPreference(this, Constants.SHARE_PRE_OBJECT,
                Constants.SHARE_PRE_OBJECT_DEPT_LIST)) {
            loadSpinnerDept();
        } else {
            getDeptList();
        }
    }

    //kết nối API lấy DS Phòng IBB
    private void getDeptList() {
        String UserName = ((MyApp) this.getApplication()).getUserName();
        new GetDeptList(this, UserName);
    }

    // load data cho Spinner từ API
    public void loadSpinnerDept() {
        String deptStr = Common.loadPreference(this,
                Constants.SHARE_PRE_OBJECT,
                Constants.SHARE_PRE_OBJECT_DEPT_LIST);
        JSONObject jObj = null;
        WSObjectsModel<KeyValuePairModel> wsObject = null;
        ArrayList<KeyValuePairModel> listAgent = null;
        try {
            jObj = new JSONObject(deptStr);
            jObj = jObj.getJSONObject(Constants.RESPONSE_RESULT);
            wsObject = new WSObjectsModel<KeyValuePairModel>(jObj,
                    KeyValuePairModel.class);

        } catch (JSONException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        if (wsObject != null) {
            listAgent = wsObject.getArrayListObject();
        } else {
            listAgent = new ArrayList<KeyValuePairModel>();
            listAgent.add(new KeyValuePairModel(0, "[Không có dữ liệu]"));
        }

        KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(this,
                R.layout.my_spinner_style, listAgent, Gravity.LEFT);
        spDept.setAdapter(adapterStatus);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Get an Analytics tracker to report app starts and uncaught exceptions
        // etc.
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
