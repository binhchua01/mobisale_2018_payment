package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by haulc3 on 21,July,2019
 */
public class CalculatorExtraOttTotalRequest implements Parcelable {
    private String userName;
    private List<CartExtraOttTotal> cart;

    public CalculatorExtraOttTotalRequest(String userName, List<CartExtraOttTotal> cart) {
        this.userName = userName;
        this.cart = cart;
    }

    protected CalculatorExtraOttTotalRequest(Parcel in) {
        userName = in.readString();
        cart = in.createTypedArrayList(CartExtraOttTotal.CREATOR);
    }

    public static final Creator<CalculatorExtraOttTotalRequest> CREATOR = new Creator<CalculatorExtraOttTotalRequest>() {
        @Override
        public CalculatorExtraOttTotalRequest createFromParcel(Parcel in) {
            return new CalculatorExtraOttTotalRequest(in);
        }

        @Override
        public CalculatorExtraOttTotalRequest[] newArray(int size) {
            return new CalculatorExtraOttTotalRequest[size];
        }
    };

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<CartExtraOttTotal> getCart() {
        return cart;
    }

    public void setCart(List<CartExtraOttTotal> cart) {
        this.cart = cart;
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            for(CartExtraOttTotal item : getCart()){
                jsonArray.put(item.toJsonObject());
            }
            jsonObject.put("UserName", getUserName());
            jsonObject.put("Cart", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeTypedList(cart);
    }
}
