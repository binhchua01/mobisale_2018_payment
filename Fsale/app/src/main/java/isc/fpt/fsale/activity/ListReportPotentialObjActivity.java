package isc.fpt.fsale.activity;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPotentialObjDetail;
import isc.fpt.fsale.action.GetPotentialObjList;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.PotentialObjAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

public class ListReportPotentialObjActivity extends BaseActivity {
    private int mPage = 1, status = 2;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
    private TextView lblSaleName;
    private String saleName = "", fromDate = "", toDate = "";
    private Spinner spPage;
    private ListView lvResult;


    public ListReportPotentialObjActivity() {
        super(R.string.lbl_screen_name_report_potential_detail);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_report_potential_detail));
        setContentView(R.layout.activity_report_potential_obj);
        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem != null) {
                    FLAG_FIRST_LOAD++;
                    if (FLAG_FIRST_LOAD > 1) {
                        if (mPage != selectedItem.getID()) {
                            mPage = selectedItem.getID();
                            getData(mPage);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        lvResult = (ListView) findViewById(R.id.lv_report_sbi_detail);
        lblSaleName = (TextView) findViewById(R.id.lbl_sale);
        lvResult.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PotentialObjModel item = (PotentialObjModel) parent.getItemAtPosition(position);
                String userName = ((MyApp) ListReportPotentialObjActivity.this.getApplication()).getUserName();
                new GetPotentialObjDetail(ListReportPotentialObjActivity.this, userName, item.getID());
            }
        });
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        // TODO Auto-generated method stub
        Intent intent = getIntent();
        if (intent != null) {
            this.fromDate = intent.getStringExtra("FROM_DATE");
            this.toDate = intent.getStringExtra("TO_DATE");
            this.saleName = intent.getStringExtra("SALE_NAME");
            this.status = intent.getIntExtra("STATUS", 2);
            lblSaleName.setText(this.saleName);
            getData(0);
        }
    }

    private void getData(int PageNumber) {
        new GetPotentialObjList(this, 0, "", PageNumber, this.fromDate, this.toDate, this.status, this.saleName);
    }

    public void loadData(ArrayList<PotentialObjModel> lst) {
        if (lst != null && lst.size() > 0) {
            int mTotalPage = lst.get(0).getTotalPage();
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            PotentialObjAdapter adapter = new PotentialObjAdapter(this, lst);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
            lvResult.setAdapter(new PotentialObjAdapter(this, null));
        }

    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
