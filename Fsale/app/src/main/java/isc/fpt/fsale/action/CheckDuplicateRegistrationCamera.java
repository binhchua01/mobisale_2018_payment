package isc.fpt.fsale.action;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.model.RegisterFptCameraModel;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 16,September,2019
 */
public class CheckDuplicateRegistrationCamera implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private RegisterFptCameraModel mRegisterFptCameraModel;

    public CheckDuplicateRegistrationCamera(Context mContext, RegisterFptCameraModel mRegisterFptCameraModel) {
        this.mContext = mContext;
        this.mRegisterFptCameraModel = mRegisterFptCameraModel;
        String message = mContext.getResources().getString(R.string.msg_pd_update);
        String CHECK_DUPLICATE_CAMERA = "CheckDuplicate_Camera";
        CallServiceTask service = new CallServiceTask(mContext, CHECK_DUPLICATE_CAMERA, mRegisterFptCameraModel.toJsonObject(),
                Services.JSON_POST_OBJECT, message, CheckDuplicateRegistrationCamera.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                boolean hasError = false;
                List<UpdResultModel> lst = null;
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(jsObj, UpdResultModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                } else {//ServiceType Error
                    hasError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!hasError) {
                    if (lst.size() > 0) {
                        final UpdResultModel item = lst.get(0);
                        if (item.getResultID() == 0) {//cho phép tạo không cần hỏi
                            new UpdateRegistrationCamera(mContext, mRegisterFptCameraModel);
                        } else if (item.getResultID() > 0) {//hiển thị popup xác nhận tạo PDK
                            showPopupConfirm(lst.get(0).getResult());
                        } else {//hiển thị popup thông báo không cho tạo
                            Common.alertDialog(lst.get(0).getResult(), mContext);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showPopupConfirm(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext)
                .setTitle("Thông Báo")
                .setMessage(message)
                .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new UpdateRegistrationCamera(mContext, mRegisterFptCameraModel);
                    }
                })
                .setNegativeButton(R.string.lbl_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setCancelable(false)
                .create();
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }
}
