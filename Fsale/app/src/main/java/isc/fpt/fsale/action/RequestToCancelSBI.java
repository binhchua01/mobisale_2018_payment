package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.MainActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.SBIManageDialog;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class RequestToCancelSBI implements AsyncTaskCompleteListener<String> {

	private String[] arrParamName, arrParamValue;	
	private Context mContext;
	private final String TAG_METHOD_NAME = "RequestToCancelSBI";
	private final String TAG_SBI_LIST = "ListObject", TAG_RESULT = "Result", /*TAG_RESULTID = "ResultID",*/ TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";
	/*private SBIManageDialog mSBIDialog = null;*/
	
	public RequestToCancelSBI(Context context, String sbi){	
		try {
			String message = "Đang lấy hủy SBI...";
			mContext = context;			
			arrParamName = new String[]{"UserName", "SBI"};			
			this.arrParamValue = new String[]{Constants.USERNAME, sbi};
			CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, RequestToCancelSBI.this);
			service.execute();
		} catch (Exception e) {

			Common.alertDialog(e.getMessage(), mContext);
			
		}		
	}
	
	public RequestToCancelSBI(Context context, String sbi, SBIManageDialog dialog){	
		try {
			String message = "Đang lấy hủy SBI...";
			mContext = context;			
			/*mSBIDialog = dialog;*/
			arrParamName = new String[]{"UserName", "SBI"};			
			this.arrParamValue = new String[]{Constants.USERNAME, sbi};
			CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, RequestToCancelSBI.this);
			service.execute();
		} catch (Exception e) {

			Common.alertDialog(e.getMessage(), mContext);
			
		}		
	}
	
	public void handleUpdateRegistration(String json){
		if(json != null && Common.jsonObjectValidate(json)){
		JSONObject jsObj = getJsonObject(json);		
		if(jsObj != null){
			try {				
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);								
				if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
					JSONArray resultArr = jsObj.getJSONArray(TAG_SBI_LIST);
					if(resultArr != null){
						String result = "";
						JSONObject item = resultArr.getJSONObject(0);
						if(item.has(TAG_RESULT)){
							result = item.getString(TAG_RESULT);
							
							 new AlertDialog.Builder(mContext).setTitle("THÔNG BÁO").setMessage(result)
				 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
				 				    @Override
				 				    public void onClick(DialogInterface dialog, int which) {			 				      
										if(mContext.getClass().getSimpleName().equals(MainActivity.class.getSimpleName())){
											try {
												//UPdate lai Flag SBI trên MainActivity sau khi cancel SBI
												MainActivity activity = (MainActivity)mContext;
												activity.getSBIList();
												/*if(mSBIDialog != null)
													mSBIDialog.getData();*/
											} catch (Exception e) {
												// TODO: handle exception

											}
											
											
										}
				 				       dialog.cancel();
				 				    }
				 				})
			 					.setCancelable(false).create().show();	
						}
					}
						
				}else{
					Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
				}
			} catch (Exception e) {

			}			
		}else{
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + 
					"-" + Constants.RESPONSE_RESULT, mContext);
		}
		}
	}
	
	private JSONObject getJsonObject(String result){
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);		
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception

		}
		return null;
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);			
	}
		
	

}
