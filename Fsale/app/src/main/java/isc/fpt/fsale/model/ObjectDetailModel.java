package isc.fpt.fsale.model;

import java.lang.reflect.Field;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class ObjectDetailModel implements Parcelable{
    private String Address;
    private String BillTo_Street;
    private String BillTo_Ward;
    private int BoxCount;
    private int ComboStatus;
    private String ComboStatusName;
    private String Contact_1;
    private String Contact_2;
    private String Contract;
    private int CusTypeDetail;
    private String Date;
    private String Email;
    private String FullName;
    private String Image;
    private String ImageInfo;
    private String ImageSignature;
    private String InputTypeName;
    private int LocalType;
    private String LocalTypeName;
    private String LocationID;
    private String Name;
    private String Note;
    private int ObjID;
    private String Phone_1;
    private String Phone_2;
    private int PromotionID;
    private String PromotionName;
    private String RegCode;
    private String RegCodeNew;
    private int RegIDNew;
    private String SaleName;
    private int ServiceType;
    private String ServiceTypeName;
    private String SourceCreate;
    private CheckListService checkListService;
    private String BaseImageInfo;
    private int BaseRegIDImageInfo;
    private String Birthday;
    private String AddressPassport;
    private String Passport;
    private int CusType;
    private int TypeHouse;
    private String Lot;
    private String Floor;
    private String Room;
    private String NameVilla;
    private String BillTo_Number;
    private String BillTo_District;
    private String TaxID;
    private int Position;
    private String NameVillaDes;
    private int Type_1;
    private int Type_2;
    private String DescriptionIBB;
    private String BillTo_CityVN;
    private String BillTo_DistrictVN;
    private String BillTo_StreetVN;
    private String BillTo_WardVN;
    private String NameVillaVN;
    private int BaseObjID;
    private String BaseContract;
    private List<Device> Device;
    private int SourceType;

    protected ObjectDetailModel(Parcel in) {
        Address = in.readString();
        BillTo_Street = in.readString();
        BillTo_Ward = in.readString();
        BoxCount = in.readInt();
        ComboStatus = in.readInt();
        ComboStatusName = in.readString();
        Contact_1 = in.readString();
        Contact_2 = in.readString();
        Contract = in.readString();
        CusTypeDetail = in.readInt();
        Date = in.readString();
        Email = in.readString();
        FullName = in.readString();
        Image = in.readString();
        ImageInfo = in.readString();
        ImageSignature = in.readString();
        InputTypeName = in.readString();
        LocalType = in.readInt();
        LocalTypeName = in.readString();
        LocationID = in.readString();
        Name = in.readString();
        Note = in.readString();
        ObjID = in.readInt();
        Phone_1 = in.readString();
        Phone_2 = in.readString();
        PromotionID = in.readInt();
        PromotionName = in.readString();
        RegCode = in.readString();
        RegCodeNew = in.readString();
        RegIDNew = in.readInt();
        SaleName = in.readString();
        ServiceType = in.readInt();
        ServiceTypeName = in.readString();
        SourceCreate = in.readString();
        checkListService = in.readParcelable(CheckListService.class.getClassLoader());
        BaseImageInfo = in.readString();
        BaseRegIDImageInfo = in.readInt();
        Birthday = in.readString();
        AddressPassport = in.readString();
        Passport = in.readString();
        CusType = in.readInt();
        TypeHouse = in.readInt();
        Lot = in.readString();
        Floor = in.readString();
        Room = in.readString();
        NameVilla = in.readString();
        BillTo_Number = in.readString();
        BillTo_District = in.readString();
        TaxID = in.readString();
        Position = in.readInt();
        NameVillaDes = in.readString();
        Type_1 = in.readInt();
        Type_2 = in.readInt();
        DescriptionIBB = in.readString();
        BillTo_CityVN = in.readString();
        BillTo_DistrictVN = in.readString();
        BillTo_StreetVN = in.readString();
        BillTo_WardVN = in.readString();
        NameVillaVN = in.readString();
        BaseObjID = in.readInt();
        BaseContract = in.readString();
        SourceType = in.readInt();
    }

    public static final Creator<ObjectDetailModel> CREATOR = new Creator<ObjectDetailModel>() {
        @Override
        public ObjectDetailModel createFromParcel(Parcel in) {
            return new ObjectDetailModel(in);
        }

        @Override
        public ObjectDetailModel[] newArray(int size) {
            return new ObjectDetailModel[size];
        }
    };

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getBillTo_Street() {
        return BillTo_Street;
    }

    public void setBillTo_Street(String billTo_Street) {
        BillTo_Street = billTo_Street;
    }

    public String getBillTo_Ward() {
        return BillTo_Ward;
    }

    public void setBillTo_Ward(String billTo_Ward) {
        BillTo_Ward = billTo_Ward;
    }

    public int getBoxCount() {
        return BoxCount;
    }

    public void setBoxCount(int boxCount) {
        BoxCount = boxCount;
    }

    public int getComboStatus() {
        return ComboStatus;
    }

    public void setComboStatus(int comboStatus) {
        ComboStatus = comboStatus;
    }

    public String getComboStatusName() {
        return ComboStatusName;
    }

    public void setComboStatusName(String comboStatusName) {
        ComboStatusName = comboStatusName;
    }

    public String getContact_1() {
        return Contact_1;
    }

    public void setContact_1(String contact_1) {
        Contact_1 = contact_1;
    }

    public String getContact_2() {
        return Contact_2;
    }

    public void setContact_2(String contact_2) {
        Contact_2 = contact_2;
    }

    public String getContract() {
        return Contract;
    }

    public void setContract(String contract) {
        Contract = contract;
    }

    public int getCusTypeDetail() {
        return CusTypeDetail;
    }

    public void setCusTypeDetail(int cusTypeDetail) {
        CusTypeDetail = cusTypeDetail;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getImageInfo() {
        return ImageInfo;
    }

    public void setImageInfo(String imageInfo) {
        ImageInfo = imageInfo;
    }

    public String getImageSignature() {
        return ImageSignature;
    }

    public void setImageSignature(String imageSignature) {
        ImageSignature = imageSignature;
    }

    public String getInputTypeName() {
        return InputTypeName;
    }

    public void setInputTypeName(String inputTypeName) {
        InputTypeName = inputTypeName;
    }

    public int getLocalType() {
        return LocalType;
    }

    public void setLocalType(int localType) {
        LocalType = localType;
    }

    public String getLocalTypeName() {
        return LocalTypeName;
    }

    public void setLocalTypeName(String localTypeName) {
        LocalTypeName = localTypeName;
    }

    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String locationID) {
        LocationID = locationID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public int getObjID() {
        return ObjID;
    }

    public void setObjID(int objID) {
        ObjID = objID;
    }

    public String getPhone_1() {
        return Phone_1;
    }

    public void setPhone_1(String phone_1) {
        Phone_1 = phone_1;
    }

    public String getPhone_2() {
        return Phone_2;
    }

    public void setPhone_2(String phone_2) {
        Phone_2 = phone_2;
    }

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int promotionID) {
        PromotionID = promotionID;
    }

    public String getPromotionName() {
        return PromotionName;
    }

    public void setPromotionName(String promotionName) {
        PromotionName = promotionName;
    }

    public String getRegCode() {
        return RegCode;
    }

    public void setRegCode(String regCode) {
        RegCode = regCode;
    }

    public String getRegCodeNew() {
        return RegCodeNew;
    }

    public void setRegCodeNew(String regCodeNew) {
        RegCodeNew = regCodeNew;
    }

    public int getRegIDNew() {
        return RegIDNew;
    }

    public void setRegIDNew(int regIDNew) {
        RegIDNew = regIDNew;
    }

    public String getSaleName() {
        return SaleName;
    }

    public void setSaleName(String saleName) {
        SaleName = saleName;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }

    public String getServiceTypeName() {
        return ServiceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        ServiceTypeName = serviceTypeName;
    }

    public String getSourceCreate() {
        return SourceCreate;
    }

    public void setSourceCreate(String sourceCreate) {
        SourceCreate = sourceCreate;
    }

    public CheckListService getCheckListService() {
        return checkListService;
    }

    public void setCheckListService(CheckListService checkListService) {
        this.checkListService = checkListService;
    }

    public String getBaseImageInfo() {
        return BaseImageInfo;
    }

    public void setBaseImageInfo(String baseImageInfo) {
        BaseImageInfo = baseImageInfo;
    }

    public int getBaseRegIDImageInfo() {
        return BaseRegIDImageInfo;
    }

    public void setBaseRegIDImageInfo(int baseRegIDImageInfo) {
        BaseRegIDImageInfo = baseRegIDImageInfo;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getAddressPassport() {
        return AddressPassport;
    }

    public void setAddressPassport(String addressPassport) {
        AddressPassport = addressPassport;
    }

    public String getPassport() {
        return Passport;
    }

    public void setPassport(String passport) {
        Passport = passport;
    }

    public int getCusType() {
        return CusType;
    }

    public void setCusType(int cusType) {
        CusType = cusType;
    }

    public int getTypeHouse() {
        return TypeHouse;
    }

    public void setTypeHouse(int typeHouse) {
        TypeHouse = typeHouse;
    }

    public String getLot() {
        return Lot;
    }

    public void setLot(String lot) {
        Lot = lot;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getRoom() {
        return Room;
    }

    public void setRoom(String room) {
        Room = room;
    }

    public String getNameVilla() {
        return NameVilla;
    }

    public void setNameVilla(String nameVilla) {
        NameVilla = nameVilla;
    }

    public String getBillTo_Number() {
        return BillTo_Number;
    }

    public void setBillTo_Number(String billTo_Number) {
        BillTo_Number = billTo_Number;
    }

    public String getBillTo_District() {
        return BillTo_District;
    }

    public void setBillTo_District(String billTo_District) {
        BillTo_District = billTo_District;
    }

    public String getTaxID() {
        return TaxID;
    }

    public void setTaxID(String taxID) {
        TaxID = taxID;
    }

    public int getPosition() {
        return Position;
    }

    public void setPosition(int position) {
        Position = position;
    }

    public String getNameVillaDes() {
        return NameVillaDes;
    }

    public void setNameVillaDes(String nameVillaDes) {
        NameVillaDes = nameVillaDes;
    }

    public int getType_1() {
        return Type_1;
    }

    public void setType_1(int type_1) {
        Type_1 = type_1;
    }

    public int getType_2() {
        return Type_2;
    }

    public void setType_2(int type_2) {
        Type_2 = type_2;
    }

    public String getDescriptionIBB() {
        return DescriptionIBB;
    }

    public void setDescriptionIBB(String descriptionIBB) {
        DescriptionIBB = descriptionIBB;
    }

    public String getBillTo_CityVN() {
        return BillTo_CityVN;
    }

    public void setBillTo_CityVN(String billTo_CityVN) {
        BillTo_CityVN = billTo_CityVN;
    }

    public String getBillTo_DistrictVN() {
        return BillTo_DistrictVN;
    }

    public void setBillTo_DistrictVN(String billTo_DistrictVN) {
        BillTo_DistrictVN = billTo_DistrictVN;
    }

    public String getBillTo_StreetVN() {
        return BillTo_StreetVN;
    }

    public void setBillTo_StreetVN(String billTo_StreetVN) {
        BillTo_StreetVN = billTo_StreetVN;
    }

    public String getBillTo_WardVN() {
        return BillTo_WardVN;
    }

    public void setBillTo_WardVN(String billTo_WardVN) {
        BillTo_WardVN = billTo_WardVN;
    }

    public String getNameVillaVN() {
        return NameVillaVN;
    }

    public void setNameVillaVN(String nameVillaVN) {
        NameVillaVN = nameVillaVN;
    }

    public int getBaseObjID() {
        return BaseObjID;
    }

    public void setBaseObjID(int baseObjID) {
        BaseObjID = baseObjID;
    }

    public String getBaseContract() {
        return BaseContract;
    }

    public void setBaseContract(String baseContract) {
        BaseContract = baseContract;
    }

    public int getSourceType() {
        return SourceType;
    }

    public void setSourceType(int sourceType) {
        SourceType = sourceType;
    }

    @Override
    public String toString() {
    	 StringBuilder sb = new StringBuilder();
    	 Field[] fields = getClass().getDeclaredFields();    	
    	  for (Field f : fields) {
    	    sb.append(f.getName());
    	    sb.append(": ");
    	    try {
    	    	sb.append("[");
				sb.append(f.get(this));
				sb.append("]");
			} catch (Exception e) {
    	        e.printStackTrace();
			}
    	    
    	    sb.append(", ");
    	    sb.append('\n');
    	  }
    	  return sb.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Address);
        parcel.writeString(BillTo_Street);
        parcel.writeString(BillTo_Ward);
        parcel.writeInt(BoxCount);
        parcel.writeInt(ComboStatus);
        parcel.writeString(ComboStatusName);
        parcel.writeString(Contact_1);
        parcel.writeString(Contact_2);
        parcel.writeString(Contract);
        parcel.writeInt(CusTypeDetail);
        parcel.writeString(Date);
        parcel.writeString(Email);
        parcel.writeString(FullName);
        parcel.writeString(Image);
        parcel.writeString(ImageInfo);
        parcel.writeString(ImageSignature);
        parcel.writeString(InputTypeName);
        parcel.writeInt(LocalType);
        parcel.writeString(LocalTypeName);
        parcel.writeString(LocationID);
        parcel.writeString(Name);
        parcel.writeString(Note);
        parcel.writeInt(ObjID);
        parcel.writeString(Phone_1);
        parcel.writeString(Phone_2);
        parcel.writeInt(PromotionID);
        parcel.writeString(PromotionName);
        parcel.writeString(RegCode);
        parcel.writeString(RegCodeNew);
        parcel.writeInt(RegIDNew);
        parcel.writeString(SaleName);
        parcel.writeInt(ServiceType);
        parcel.writeString(ServiceTypeName);
        parcel.writeString(SourceCreate);
        parcel.writeParcelable(checkListService, i);
        parcel.writeString(BaseImageInfo);
        parcel.writeInt(BaseRegIDImageInfo);
        parcel.writeString(Birthday);
        parcel.writeString(AddressPassport);
        parcel.writeString(Passport);
        parcel.writeInt(CusType);
        parcel.writeInt(TypeHouse);
        parcel.writeString(Lot);
        parcel.writeString(Floor);
        parcel.writeString(Room);
        parcel.writeString(NameVilla);
        parcel.writeString(BillTo_Number);
        parcel.writeString(BillTo_District);
        parcel.writeString(TaxID);
        parcel.writeInt(Position);
        parcel.writeString(NameVillaDes);
        parcel.writeInt(Type_1);
        parcel.writeInt(Type_2);
        parcel.writeString(DescriptionIBB);
        parcel.writeString(BillTo_CityVN);
        parcel.writeString(BillTo_DistrictVN);
        parcel.writeString(BillTo_StreetVN);
        parcel.writeString(BillTo_WardVN);
        parcel.writeString(NameVillaVN);
        parcel.writeInt(BaseObjID);
        parcel.writeString(BaseContract);
        parcel.writeInt(SourceType);
    }
}
