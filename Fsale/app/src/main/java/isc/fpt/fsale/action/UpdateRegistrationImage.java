package isc.fpt.fsale.action;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by HCM.TUANTT14 on 10/31/2018.
 */

public class UpdateRegistrationImage implements AsyncTaskCompleteListener<String> {
    private Context context;
    private int regID;

    public UpdateRegistrationImage(Context context, int regId, String strImageDocument) {
        this.context = context;
        this.regID = regId;
        String[] arrParamName = new String[]{"RegID", "UserName", "Image"};
        String[] arrParamValue = new String[]{String.valueOf(regId), Constants.USERNAME, strImageDocument};
        String message = context.getResources().getString(R.string.lbl_process_update_registration_image_image_document);
        String UPDATE_REGISTRATION_IMAGE = "UpdateRegistrationImage";
        CallServiceTask service = new CallServiceTask(context, UPDATE_REGISTRATION_IMAGE, arrParamName, arrParamValue,
                Services.JSON_POST, message, UpdateRegistrationImage.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) throws JSONException {
        if (Common.jsonObjectValidate(result)) {
            JSONObject jsObj = new JSONObject(result);
            try {
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                String resultID1 = "ResultID";
                if (jsObj.has(resultID1)) {
                    int resultID = jsObj.getInt(resultID1);
                    String RESULT_MESSAGE = "ResultMessage";
                    String message = jsObj.has(RESULT_MESSAGE) ? jsObj.getString(RESULT_MESSAGE) :
                            context.getResources().getString(R.string.lbl_empty_message);
                    if (resultID > 0) {
                        new AlertDialog.Builder(context).setTitle(context.getResources()
                                .getString(R.string.title_notification)).setMessage(message)
                                .setPositiveButton(R.string.lbl_ok,
                                        new Dialog.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                try {
                                                    new GetRegistrationDetail(context, Constants.USERNAME, regID);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                dialog.cancel();
                                            }
                                        })
                                .setCancelable(false)
                                .create()
                                .show();
                    } else {
                        Common.alertDialogNotTitle(context.getResources().getString(R.string.msg_update_fail), context);
                    }
                } else {
                    Common.alertDialog(jsObj.toString(), context);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
