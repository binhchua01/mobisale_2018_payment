package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haulc3 on 19,April,2019
 */
public class Ability4Deployment {
    @SerializedName("AbilityDesc")
    @Expose
    private Integer AbilityDesc;
    @SerializedName("Desc")
    @Expose
    private String Desc;
    @SerializedName("TimeZone")
    @Expose
    private String TimeZone;
    @SerializedName("ColorCode")
    @Expose
    private String ColorCode;

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public String getTimeZone() {
        return TimeZone;
    }

    public void setTimeZone(String timeZone) {
        TimeZone = timeZone;
    }

    public Integer getAbilityDesc() {
        return AbilityDesc;
    }

    public void setAbilityDesc(Integer abilityDesc) {
        AbilityDesc = abilityDesc;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }
}
