package isc.fpt.fsale.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import isc.fpt.fsale.ui.fragment.FragmentCaptureImage;

public class ViewPagerCaptureImageAdapter extends FragmentPagerAdapter {

	//private static int NUM_ITEMS = 2;
	List<String> imgsPath;
	List<FragmentCaptureImage> mFragments;
	
	public ViewPagerCaptureImageAdapter(FragmentManager fm, List<String> lst) {
		super(fm);
		this.imgsPath = lst;
		mFragments = new ArrayList<FragmentCaptureImage>();
		if(imgsPath != null && imgsPath.size() >0){
			for(int i=0 ;i<imgsPath.size();i++)
				mFragments.add(FragmentCaptureImage.newInstance(imgsPath.get(i)));
		}
	}
	
	/*public void setData(List<String> lst){
		imgsPath = lst;		
		mFragments.clear();
		mFragments = new ArrayList<FragmentCaptureImage>();
		if(imgsPath != null && imgsPath.size() >0){
			for(int i=0 ;i<imgsPath.size();i++)
				mFragments.add(FragmentCaptureImage.newInstance(imgsPath.get(i)));
		}
		getItem(0);
	}*/
	
	@Override
	public int getItemPosition(Object object) {
	    return POSITION_NONE;
	}
	
	@Override
	public FragmentCaptureImage getItem(int position) {
		if(mFragments != null && position <= mFragments.size()-1){
			return mFragments.get(position);
		}
		return null;
	}
		
	@Override
	public int getCount() {
		// Show 3 total pages.
		if(imgsPath != null)
			return imgsPath.size();
		return 0;
	}

	public List<String> getImgsPath() {
		return imgsPath;
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		Locale l = Locale.getDefault();
		String title = "Page " + String.valueOf(position);		
		return title.toUpperCase(l);
	}
}
