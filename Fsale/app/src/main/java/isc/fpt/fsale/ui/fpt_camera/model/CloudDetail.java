package isc.fpt.fsale.ui.fpt_camera.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nhannh26 on 05,September,2019
 */
public class CloudDetail implements Parcelable {
    private int TypeID;
    private String TypeName;
    private int PackID;
    private String PackName;
    private int Cost;
    private int Qty;
    private int ServiceType;
    private int TypePromotion;
    private boolean isSelected = false;

    public CloudDetail() {
    }

    public CloudDetail(int typeID, String typeName, int packID, String packName,
                       int cost, int qty, int serviceType, boolean isSelected) {
        TypeID = typeID;
        TypeName = typeName;
        PackID = packID;
        PackName = packName;
        Cost = cost;
        Qty = qty;
        ServiceType = serviceType;
        this.isSelected = isSelected;
    }


    protected CloudDetail(Parcel in) {
        TypeID = in.readInt();
        TypeName = in.readString();
        PackID = in.readInt();
        PackName = in.readString();
        Cost = in.readInt();
        Qty = in.readInt();
        ServiceType = in.readInt();
        TypePromotion = in.readInt();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(TypeID);
        dest.writeString(TypeName);
        dest.writeInt(PackID);
        dest.writeString(PackName);
        dest.writeInt(Cost);
        dest.writeInt(Qty);
        dest.writeInt(ServiceType);
        dest.writeInt(TypePromotion);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    public static final Creator<CloudDetail> CREATOR = new Creator<CloudDetail>() {
        @Override
        public CloudDetail createFromParcel(Parcel in) {
            return new CloudDetail(in);
        }

        @Override
        public CloudDetail[] newArray(int size) {
            return new CloudDetail[size];
        }
    };

    public int getTypeID() {
        return TypeID;
    }

    public void setTypeID(int typeID) {
        TypeID = typeID;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }

    public int getPackID() {
        return PackID;
    }

    public void setPackID(int packID) {
        PackID = packID;
    }

    public String getPackName() {
        return PackName;
    }

    public void setPackName(String packName) {
        PackName = packName;
    }

    public int getCost() {
        return Cost;
    }

    public void setCost(int cost) {
        Cost = cost;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getTypePromotion() {
        return TypePromotion;
    }

    public void setTypePromotion(int typePromotion) {
        TypePromotion = typePromotion;
    }

    public JSONObject toJsonObjectVer2() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("TypeID", getTypeID());
            jsonObject.put("TypeName", getTypeName());
            jsonObject.put("PackID", getPackID());
            jsonObject.put("PackName", getPackName());
            jsonObject.put("Cost", getCost());
            jsonObject.put("Qty", getQty());
            jsonObject.put("ServiceType", getServiceType());
            jsonObject.put("TypePromotion", getTypePromotion());
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject toJsonObjectCloud() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("TypeID", getTypeID());
            jsonObject.put("TypeName", getTypeName());
            jsonObject.put("PackID", getPackID());
            jsonObject.put("PackName", getPackName());
            jsonObject.put("Cost", getCost());
            jsonObject.put("Qty", getQty());
            jsonObject.put("ServiceType", getServiceType());
            jsonObject.put("TypePromotion", getTypePromotion() + "");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
