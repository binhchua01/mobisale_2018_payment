package isc.fpt.fsale.adapter;

import isc.fpt.fsale.model.PotentialAdvisoryResultDetailModel;
import java.util.ArrayList;
import isc.fpt.fsale.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PotentialAdvisoryResultDetailListAdapter extends BaseAdapter {

	private ArrayList<PotentialAdvisoryResultDetailModel> mList;	
	private Context mContext;
	
	public PotentialAdvisoryResultDetailListAdapter(Context context, ArrayList<PotentialAdvisoryResultDetailModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		PotentialAdvisoryResultDetailModel item = mList.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_potential_advisory_result_list, null);
		}
		if(item != null){
			TextView lblResultTimes = (TextView)convertView.findViewById(R.id.lbl_potential_advisory_result_times);
			TextView lblResult = (TextView)convertView.findViewById(R.id.lbl_potential_advisory_result);
			TextView lblCreateDate = (TextView)convertView.findViewById(R.id.lbl_create_date);
			TextView lblRowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			if(item != null){
				lblResultTimes.setText(item.getAdvisoryTimes());
				lblResult.setText(item.getAdvisoryResult());
				lblCreateDate.setText(item.getCreateDate());
				lblRowNumber.setText(String.valueOf(item.getRowNumber()));
			}
		}
		
		return convertView;
	}


}
