package isc.fpt.fsale.ui.maxytv.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.maxy.GetPromotionByMaxyPackage;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyPromotionPackageAdapter;
import isc.fpt.fsale.ui.maxytv.model.MaxyGeneral;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.LOCAL_TYPE;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_PACKAGE_DETAIL;
import static isc.fpt.fsale.utils.Constants.POSITION;
import static isc.fpt.fsale.utils.Constants.REGTYPE;

public class MaxyPromotionPackageActivity extends BaseActivitySecond implements OnItemClickListener<MaxyGeneral> {
    private View loBack;
    private MaxyPromotionPackageAdapter mAdapter;
    private MaxyGeneral mMaxy;
    private List<MaxyGeneral> maxyGenerals;
    private int pos, localtype;
    private int regType;

    @Override
    protected void onResume() {
        super.onResume();
        if (mMaxy != null)
            new GetPromotionByMaxyPackage(this, mMaxy.getPackageID(), regType, localtype);
    }

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_maxy_package;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText("Danh sách chọn CLKM dịch vụ Truyền hình");
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_maxy_package);
        maxyGenerals = new ArrayList<>();
        mAdapter = new MaxyPromotionPackageAdapter(this, maxyGenerals, this);
        mRecyclerView.setAdapter(mAdapter);
        getDataIntent();

    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(Constants.LIST_MAXY_PROMOTION_PACKAGE) != null) {
            mMaxy = bundle.getParcelable(Constants.LIST_MAXY_PROMOTION_PACKAGE);
            pos = bundle.getInt(POSITION);
            regType = bundle.getInt(REGTYPE);
            localtype = bundle.getInt(LOCAL_TYPE);
        }
    }

    @Override
    public void onItemClick(MaxyGeneral object) {
        object.setUseCombo(mMaxy.getUseCombo());
        object.setPackageID(mMaxy.getPackageID());
        object.setPackage(mMaxy.getPackage());
        object.setNumBox(mMaxy.getNumBox());
        object.setTypeSetupID(mMaxy.getTypeSetupID());
        object.setTypeSetupName(mMaxy.getTypeSetupName());
        object.setTypeDeployID(mMaxy.getTypeDeployID());
        object.setTypeDeployName(mMaxy.getTypeDeployName());
        Intent returnIntent = new Intent();
        returnIntent.putExtra(OBJECT_MAXY_PACKAGE_DETAIL, object);
        returnIntent.putExtra(POSITION, pos);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    public void mLoadPromotionPackage(List<MaxyGeneral> lst) {
        this.maxyGenerals = lst;
        mAdapter.notifyData(this.maxyGenerals);
    }
}
