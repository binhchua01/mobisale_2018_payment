package isc.fpt.fsale.adapter;

import java.util.Locale;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import isc.fpt.fsale.ui.fragment.FragmentCreatePotentialObjDetail;
import isc.fpt.fsale.ui.fragment.FragmentCreatePotentialObjMap;
import isc.fpt.fsale.ui.fragment.FragmentCreatePotentialObjNote;

public class ViewPagerCreatePotentialObjAdapter extends FragmentPagerAdapter {

	private static int NUM_ITEMS = 3;
	
	public ViewPagerCreatePotentialObjAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// getItem is called to instantiate the fragment for the given page.
		// Return a PlaceholderFragment (defined as a static inner class
		// below).
		
		//return CreatePotentialObjMapFragment.newInstance(position + 1);
		switch (position) {
        case 0: // Fragment # 0 - This will show FirstFragment
        	return FragmentCreatePotentialObjNote.newInstance(1, "Ghi chú");
        	//return SupportMapFragment.newInstance();
        case 1: // Fragment # 0 - This will show FirstFragment different title
            //return CreatePotentialObjMapFragment.newInstance(1, "Page # 2");
        	return FragmentCreatePotentialObjDetail.newInstance(2, "Thông tin chi tiết");
        	//return SupportMapFragment.newInstance();
        case 2: // Fragment # 1 - This will show SecondFragment
            //return CreatePotentialObjMapFragment.newInstance(2, "Page # 3");        	
        	 return FragmentCreatePotentialObjMap.newInstance(0, "Bản đồ");
        	//return SupportMapFragment.newInstance();
        default:
            return null;
        }
	}

	@Override
	public int getCount() {
		// Show 3 total pages.
		return NUM_ITEMS;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		Locale l = Locale.getDefault();
		String title = "Đây là page " + String.valueOf(position);
		
		return title.toUpperCase(l);
	}
}
