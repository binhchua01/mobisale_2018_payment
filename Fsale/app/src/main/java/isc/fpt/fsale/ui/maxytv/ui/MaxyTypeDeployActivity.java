package isc.fpt.fsale.ui.maxytv.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.maxy.GetTypeDeploy;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyTypeDeployAdapter;
import isc.fpt.fsale.ui.maxytv.model.MaxyTypeDeploy;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.LIST_MAXY_DEPLOY_CLEAR;
import static isc.fpt.fsale.utils.Constants.LOCAL_TYPE;
import static isc.fpt.fsale.utils.Constants.REGTYPE;
import static isc.fpt.fsale.utils.Constants.TAG_CONTRACT;

public class MaxyTypeDeployActivity extends BaseActivitySecond implements OnItemClickListener<MaxyTypeDeploy> {
    private View loBack;
    private List<MaxyTypeDeploy> mList;
    private List<MaxyTypeDeploy> mListSelected;
    private MaxyTypeDeployAdapter mAdapter;
    private String mClassName, contract;
    private int RegType, localType;


    @Override
    protected void initEvent() {
        loBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_type_deploy;
    }

    @Override
    protected void initView() {
        getDataIntent();
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.lbl_screen_name_type_sale));
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_type_deploy);
        mAdapter = new MaxyTypeDeployAdapter(this, mList != null ? mList : new ArrayList<>(), this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        RegType = bundle.getInt(REGTYPE);
        localType = bundle.getInt(LOCAL_TYPE);
        contract = bundle.getString(TAG_CONTRACT);
        if (bundle.getString(Constants.CLASS_NAME) != null) {
            mClassName = bundle.getString(Constants.CLASS_NAME);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!TextUtils.isEmpty(mClassName)){
            new GetTypeDeploy(this, RegType, localType, contract);
        }
    }

    public void loadData(List<MaxyTypeDeploy> lst) {
        this.mList = lst;
        if(RegType == 1){
            mList.add(new MaxyTypeDeploy(-1, "Bỏ chọn"));
        }
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(MaxyTypeDeploy object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.OBJECT_MAXY_TYPE_DEPLOY, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
