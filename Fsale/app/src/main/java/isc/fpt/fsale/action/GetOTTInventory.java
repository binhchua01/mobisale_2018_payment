package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.OTTInventory;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.InventoryDeviceFragment;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetOTTInventory implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private InventoryDeviceFragment activity;

    public GetOTTInventory(Context mContext, InventoryDeviceFragment activity, String saleName, int ottId) {
        this.mContext = mContext;
        this.activity = activity;
        String[] arrParamName = new String[]{"SaleName", "OTTID"};
        String[] arrParamValue = new String[]{saleName, String.valueOf(ottId)};
        String message = mContext.getResources().getString(R.string.msg_progress_get_device_inventory);
        String GET_DEVICE_TYPE_INVENTORY = "GetOTTInventory";
        CallServiceTask service = new CallServiceTask(mContext, GET_DEVICE_TYPE_INVENTORY, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetOTTInventory.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<OTTInventory> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), OTTInventory.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    activity.ottInventory(lst);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
