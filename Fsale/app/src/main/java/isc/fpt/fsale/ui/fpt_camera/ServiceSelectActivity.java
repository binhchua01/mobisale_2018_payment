package isc.fpt.fsale.ui.fpt_camera;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;

public class ServiceSelectActivity extends BaseActivitySecond
        implements OnItemClickListener<CategoryServiceList> {

    @Override
    protected void initEvent() {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_camera_service;
    }

    @Override
    protected void initView() {

    }

    @Override
    public void onItemClick(CategoryServiceList object) {

    }
}
