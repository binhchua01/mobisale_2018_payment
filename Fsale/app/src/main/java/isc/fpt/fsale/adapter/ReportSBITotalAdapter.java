package isc.fpt.fsale.adapter;

import isc.fpt.fsale.activity.ListReportSBIDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportSBITotalModel;
import isc.fpt.fsale.utils.Common;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ReportSBITotalAdapter  extends BaseAdapter{
	private ArrayList<ReportSBITotalModel> mList;	
	private Context mContext;
	private int mDay = 0, mMonth = 0, mYear = 0, mAgent = 0, mPage = 0;
	private String mAgentName = "";
	
	public ReportSBITotalAdapter(Context context, ArrayList<ReportSBITotalModel> lst, int Day, int Month, int Year, int Agent, String AgentName, int Page){
		this.mContext = context;
		this.mList = lst;
		this.mDay = Day;
		this.mMonth = Month;
		this.mYear = Year;
		this.mAgent = Agent;
		this.mAgentName = AgentName;
		this.mPage = Page;
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ReportSBITotalModel item = mList.get(position);	
		
		if (convertView == null) {			
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_sbi_total, null);
		}
		
		
		try {
			TextView rowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			TextView saleName = (TextView)convertView.findViewById(R.id.lbl_report_sale_name);
			LinearLayout frmData = (LinearLayout)convertView.findViewById(R.id.frm_report_data);
			if(frmData.getChildCount()>0){
				frmData.removeAllViews();
			}
			int rowNum = position;
			if(mPage>1)
				rowNum = (mPage * 10) + position;
			else
				rowNum += 1;
			rowNumber.setText(String.valueOf(rowNum));
			
			if(item != null){				
				
				saleName.setText(item.getSaleName());				
				
				for(int i = 0; i<item.getReportList().size() ;i++){
					final KeyValuePairModel subItem = item.getReportList().get(i);
					View viewChild = LayoutInflater.from(mContext).inflate( R.layout.sub_row_report_total, null );
					LinearLayout frmDataRow = (LinearLayout)viewChild.findViewById(R.id.frm_report_data_row);
					TextView lblTitle = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_title);
					TextView lblTotal = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_total);
					
					frmDataRow.setVisibility(View.VISIBLE);				
					
					lblTotal.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(mContext, ListReportSBIDetailActivity.class);
							intent.putExtra("USER_NAME", item.getSaleName());
							intent.putExtra("DAY", mDay);
							intent.putExtra("MONTH", mMonth);
							intent.putExtra("YEAR", mYear);
							intent.putExtra("AGENT", mAgent);
							intent.putExtra("AGENT_NAME", mAgentName);
							intent.putExtra("REPORT_TYPE", subItem.getID());
							mContext.startActivity(intent);
							
						}
					});
					
					lblTitle.setText(subItem.getDescription() + ":");
					lblTotal.setText(subItem.getHint());
					frmData.addView(frmDataRow);
				}
			}
				
		} catch (Exception e) {

			Common.alertDialog("ReportSBITotalAdapter.getView():" + e.getMessage(), mContext);
		}
		
		
		return convertView;
	}

}
