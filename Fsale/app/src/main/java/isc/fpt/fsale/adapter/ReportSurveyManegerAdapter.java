package isc.fpt.fsale.adapter;


import isc.fpt.fsale.model.ReportSurveyManagerModel;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReportSurveyManegerAdapter extends BaseAdapter {
	 
	private ArrayList<ReportSurveyManagerModel> mlist;
	private Context mContext;
	
	
	public ReportSurveyManegerAdapter(Context mContext,ArrayList<ReportSurveyManagerModel> mlist)
	{
		this.mContext=mContext;
		this.mlist=mlist;
	}
	
	@Override
	public int getCount()
	{
		return this.mlist.size();
	}
	
	@Override
	public Object getItem(int position)
	{
		return this.mlist.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	
		ReportSurveyManagerModel ItemBookPort = mlist.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_survey_manger,null);
		}		
		
		TextView txtStt = (TextView) convertView.findViewById(R.id.txt_stt_billing_detail);
		txtStt.setText(String.valueOf(ItemBookPort.getRowNumber()));
		
		TextView txtRegCode = (TextView) convertView.findViewById(R.id.txt_Manger_RegCode);
		txtRegCode.setText(ItemBookPort.getRegCode());
		
		TextView txtMangerContract = (TextView) convertView.findViewById(R.id.txt_Manger_Contract);
		txtMangerContract.setText(ItemBookPort.getContract());
		
		TextView txtMangerFullName = (TextView) convertView.findViewById(R.id.txt_Manger_FullName);
		txtMangerFullName.setText(ItemBookPort.getFullName());
		
		TextView txtMangerFirstTotalCab = (TextView) convertView.findViewById(R.id.txt_Manger_FirstTotalCab);
		txtMangerFirstTotalCab.setText(String.valueOf(ItemBookPort.getFirstTotalCab()));
		
		TextView txtMangerBalance = (TextView) convertView.findViewById(R.id.txt_Manger_Balance);
		txtMangerBalance.setText(String.valueOf(ItemBookPort.getBalance()));
		
		TextView txtAfterTotalCab = (TextView) convertView.findViewById(R.id.txt_AfterTotalCab);
		txtAfterTotalCab.setText(String.valueOf(ItemBookPort.getAfterTotalCab()));
		
		return convertView;
	}
	
}
