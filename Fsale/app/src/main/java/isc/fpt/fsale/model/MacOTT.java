package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

/**
 * Created by haulc3 on 11,February,2020
 */
public class MacOTT implements Parcelable {
    private String DeviceName;
    private String MAC;
    private int OTTID;
    private boolean isSelected;

    public MacOTT(String mac){
        this.MAC = mac;
    }

    public MacOTT(Parcel in) {
        DeviceName = in.readString();
        MAC = in.readString();
        OTTID = in.readInt();
    }

    public static final Creator<MacOTT> CREATOR = new Creator<MacOTT>() {
        @Override
        public MacOTT createFromParcel(Parcel in) {
            return new MacOTT(in);
        }

        @Override
        public MacOTT[] newArray(int size) {
            return new MacOTT[size];
        }
    };

    public String getDeviceName() {
        return DeviceName;
    }

    public void setDeviceName(String deviceName) {
        DeviceName = deviceName;
    }

    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public int getOTTID() {
        return OTTID;
    }

    public void setOTTID(int OTTID) {
        this.OTTID = OTTID;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(DeviceName);
        parcel.writeString(MAC);
        parcel.writeInt(OTTID);
    }

    public JSONObject toJSONObject() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("MAC", this.MAC);
            jsonObj.put("OTTID", this.OTTID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObj;
    }
}
