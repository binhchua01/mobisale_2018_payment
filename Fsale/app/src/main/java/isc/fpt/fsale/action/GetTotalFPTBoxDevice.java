package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by HCM.TUANTT14 on 1/19/2018.
 * Update by haulc3 on 11/05/2019
 */
// API lấy tổng tiền FPT BOX
public class GetTotalFPTBoxDevice implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterStep4 fragment;

    public GetTotalFPTBoxDevice(Context mContext, RegistrationDetailModel mRegister, FragmentRegisterStep4 fragment) {
        this.mContext = mContext;
        this.fragment = fragment;
        String message = mContext.getResources().getString(R.string.msg_pd_get_fpt_play_box_total);
        String GET_TOTAL_FPT_BOX_DEVICE = "GetTotalOTT";
        CallServiceTask service = new CallServiceTask(mContext, GET_TOTAL_FPT_BOX_DEVICE,
                mRegister.toJSONObjectGetFptPlayTotal(), Services.JSON_POST_OBJECT, message, GetTotalFPTBoxDevice.this);
        service.execute();
    }

    private JSONObject getJsonObject(String result) {
        try {
            return JSONParsing.getJsonObj(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onTaskComplete(String result) {
        if (Common.jsonObjectValidate(result)) {
            JSONObject jsObj = getJsonObject(result);
            if (jsObj != null) {
                try {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    String TAG_ERROR_CODE = "ErrorCode";
                    if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        String TAG_OBJECT_LIST = "ListObject";
                        JSONArray array = jsObj.getJSONArray(TAG_OBJECT_LIST);
                        JSONObject item = array.getJSONObject(0);
                        int Amount = 0;
                        String TAG_AMOUNT = "Amount";
                        if (item.has(TAG_AMOUNT))
                            Amount = item.getInt(TAG_AMOUNT);
                        if (fragment != null) {
                            fragment.loadFptBoxPrice(Amount);
                        }
                    } else {
                        String TAG_ERROR = "Error";
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
