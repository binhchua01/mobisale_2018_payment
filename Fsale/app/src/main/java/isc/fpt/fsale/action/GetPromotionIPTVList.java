package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionIPTVModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractIPTV;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3v2;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// API lấy danh sách câu lệnh khuyến mãi iptv
public class GetPromotionIPTVList implements AsyncTaskCompleteListener<String> {
    private String[] arrParamName, arrParamValue;
    private Context mContext;
    private final String TAG_METHOD_NAME = "GetPromotionIPTVList";
    private int boxOrder;
    private FragmentRegisterStep3 fragmentRegisterStep3;
    //ver3.19
    private FragmentRegisterStep3v2 fragmentRegisterStep3v2;
    private FragmentRegisterContractIPTV fragmentRegisterContractIPTV;
    private int customerType;

    public GetPromotionIPTVList(Context context, String iptvPackage, int boxOther) {
        try {
            this.boxOrder = boxOther;
            mContext = context;
            arrParamName = new String[]{"Username", "Package", "BoxOrder"};
            this.arrParamValue = new String[]{Constants.USERNAME, iptvPackage, String.valueOf(boxOther)};
            String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_iptv_list);
            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionIPTVList.this);
            service.execute();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public GetPromotionIPTVList(Context context, FragmentRegisterStep3 fragment, String iptvPackage, int boxOther, int customerType, String contract, String regCode) {
        try {
            this.boxOrder = boxOther;
            fragmentRegisterStep3 = fragment;
            mContext = context;
            this.customerType = customerType;
            arrParamName = new String[]{"Username", "Package", "BoxOrder", "CustomerType", "Contract", "RegCode"};
            this.arrParamValue = new String[]{Constants.USERNAME, iptvPackage, String.valueOf(boxOther), String.valueOf(this.customerType), contract, regCode};
            String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_iptv_list);
            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionIPTVList.this);
            service.execute();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public GetPromotionIPTVList(Context context, FragmentRegisterStep3v2 fragment, String iptvPackage, int boxOther, int customerType, String contract, String regCode) {
        try {
            this.boxOrder = boxOther;
            fragmentRegisterStep3v2 = fragment;
            mContext = context;
            this.customerType = customerType;
            arrParamName = new String[]{"Username", "Package", "BoxOrder", "CustomerType", "Contract", "RegCode"};
            this.arrParamValue = new String[]{Constants.USERNAME, iptvPackage, String.valueOf(boxOther), String.valueOf(this.customerType), contract, regCode};
            String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_iptv_list);
            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionIPTVList.this);
            service.execute();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public GetPromotionIPTVList(Context context, FragmentRegisterContractIPTV fragment, String iptvPackage, int boxOther, int customerType, String contract, String regCode) {
        try {
            this.boxOrder = boxOther;
            mContext = context;
            this.fragmentRegisterContractIPTV = fragment;
            this.customerType = customerType;
            arrParamName = new String[]{"Username", "Package", "BoxOrder", "CustomerType", "Contract", "RegCode"};
            this.arrParamValue = new String[]{Constants.USERNAME, iptvPackage, String.valueOf(boxOther), String.valueOf(this.customerType), contract, regCode};
            String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_iptv_list);
            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionIPTVList.this);
            service.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleUpdateRegistration(String json) {
        try {
            ArrayList<PromotionIPTVModel> lst;
            if (json != null && Common.jsonObjectValidate(json)) {
                JSONObject jsObj = new JSONObject(json);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray("ListObject");
                lst = new ArrayList<>();
                for(int i = 0; i < jsonArray.length(); i++){
                    lst.add(new Gson().fromJson(jsObj.getJSONArray("ListObject").get(i).toString(), PromotionIPTVModel.class));
                }
                if (lst.size() != 0) {
                    loadData1(lst);
                }
            }
        } catch (JSONException e) {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            return JSONParsing.getJsonObj(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onTaskComplete(String result) {
        handleUpdateRegistration(result);
    }

    private void loadData1(ArrayList<PromotionIPTVModel> lst) {
            if (fragmentRegisterStep3v2 != null) {
                if (boxOrder <= 1)
                    fragmentRegisterStep3v2.initIPTVPromotionIPTVBox1(lst);
                else
                    fragmentRegisterStep3v2.initIPTVPromotionIPTVBox2(lst);
        } else if (fragmentRegisterContractIPTV != null) {
            if (boxOrder <= 1) {
                fragmentRegisterContractIPTV.initIPTVPromotionIPTVBox1(lst);
            } else {
                fragmentRegisterContractIPTV.initIPTVPromotionIPTVBox2(lst);
            }
        }
    }
}