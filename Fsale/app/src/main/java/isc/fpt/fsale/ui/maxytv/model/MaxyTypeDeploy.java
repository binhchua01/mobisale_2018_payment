package isc.fpt.fsale.ui.maxytv.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import isc.fpt.fsale.model.ServiceCode;

public class MaxyTypeDeploy implements Parcelable {

    private int TypeDeployID;
    private String TypeDeployName;

    private boolean isSelected = false;

    public MaxyTypeDeploy(int typeDeployID, String typeDeployName, boolean isSelected) {
        TypeDeployID = typeDeployID;
        TypeDeployName = typeDeployName;
        this.isSelected = isSelected;
    }

    public MaxyTypeDeploy(int typeDeployID, String typeDeployName) {
        TypeDeployID = typeDeployID;
        TypeDeployName = typeDeployName;
    }

    public MaxyTypeDeploy() {
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    protected MaxyTypeDeploy(Parcel in) {
        TypeDeployID = in.readInt();
        TypeDeployName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(TypeDeployID);
        dest.writeString(TypeDeployName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MaxyTypeDeploy> CREATOR = new Creator<MaxyTypeDeploy>() {
        @Override
        public MaxyTypeDeploy createFromParcel(Parcel in) {
            return new MaxyTypeDeploy(in);
        }

        @Override
        public MaxyTypeDeploy[] newArray(int size) {
            return new MaxyTypeDeploy[size];
        }
    };

    public int getTypeDeployID() {
        return TypeDeployID;
    }

    public void setTypeDeployID(int typeDeployID) {
        TypeDeployID = typeDeployID;
    }

    public String getTypeDeployName() {
        return TypeDeployName;
    }

    public void setTypeDeployName(String typeDeployName) {
        TypeDeployName = typeDeployName;
    }

}
