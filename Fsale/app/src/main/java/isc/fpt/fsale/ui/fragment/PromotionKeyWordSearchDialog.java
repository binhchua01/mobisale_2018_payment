package isc.fpt.fsale.ui.fragment;

import isc.fpt.fsale.R;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageButton;

public class PromotionKeyWordSearchDialog extends DialogFragment{

	private ImageButton imgClose;
	private WebView webView;
	//private Context mContext;

	public PromotionKeyWordSearchDialog(){}

	@SuppressLint("ValidFragment")
	public PromotionKeyWordSearchDialog(Context mContext) {
		// TODO Auto-generated constructor stub
		//this.mContext = mContext;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	    // Inflate the layout to use as dialog or embedded fragment
		  
		View view = inflater.inflate(R.layout.dialog_promotion_search_format, container);
		imgClose = (ImageButton)view.findViewById(R.id.btn_close);
		imgClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getDialog().dismiss();
			}
		});
		webView = (WebView)view.findViewById(R.id.web_view);
		setText();
		return view;
	}
	
	private void setText(){		
		webView.loadUrl("file:///android_asset/html/keyword.htm");
	}
	
	/** The system calls this only when creating the layout in a dialog. */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {	   
	    Dialog dialog = super.onCreateDialog(savedInstanceState);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    return dialog;
	}
	
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		 
	}
}
