package isc.fpt.fsale.action;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.SaleCODModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetSaleCOD implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public GetSaleCOD(Context mContext, String username) {
        this.mContext = mContext;
        String[] params = new String[]{"Username"};
        String[] value = new String[]{username};
        String message = "Vui lòng chờ ...";
        String TAG_METHOD_NAME = "SaleCOD";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, params, value, Services.JSON_POST, message, GetSaleCOD.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) throws JSONException {
        try {
            List<SaleCODModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), SaleCODModel.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        new AlertDialog.Builder(mContext)
                                .setTitle(lst.get(0).getTitle())
                                .setMessage(lst.get(0).getMessage())
                                .setPositiveButton(R.string.lbl_ok, (dialog, which) -> {
                                    String url = lst.get(0).getLink();
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    mContext.startActivity(i);
                                    dialog.cancel();
                                })
                                .setNegativeButton(R.string.lbl_no, (dialog, which) -> {
                                    dialog.cancel();
                                    lst.clear();
                                })
                                .setCancelable(false)
                                .create()
                                .show();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
