package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ToDoListDetailModel;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ToDoListDetailAdapter  extends BaseAdapter{
	private List<ToDoListDetailModel> mList;	
	private Context mContext;
	
	public ToDoListDetailAdapter(Context context, List<ToDoListDetailModel> lst){
		this.mContext = context;
		this.mList = lst;	
	}
	
	public void setListData(List<ToDoListDetailModel> lst){
		this.mList.clear();
		this.mList.addAll(lst);
		this.notifyDataSetChanged();		
	}
	
	public void addAll(List<ToDoListDetailModel> lst){
		this.mList.addAll(lst);
		this.notifyDataSetChanged();		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint({ "InflateParams"})
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ToDoListDetailModel item = mList.get(position);		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_todolist_detail_item, null);
		}
		if(item != null){
			TextView lblCareType = (TextView)convertView.findViewById(R.id.lbl_care_type);
			lblCareType.setText(item.getCareType());
			TextView lblDescription = (TextView)convertView.findViewById(R.id.lbl_desc);
			lblDescription.setText(item.getDescription());
			TextView lblCreateDate = (TextView)convertView.findViewById(R.id.lbl_support_date);
			lblCreateDate.setText(item.getDate());
			TextView lblStatus = (TextView)convertView.findViewById(R.id.lbl_status);
			if(item.getStatus() > 0)
				lblStatus.setText("Xử lý hoàn tất.");
			else
				lblStatus.setText("Xử lý chưa hoàn tất.");
		}
		return convertView;
	}

}
