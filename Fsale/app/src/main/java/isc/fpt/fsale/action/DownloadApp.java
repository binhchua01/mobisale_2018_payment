package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.FileProvider;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import java.io.File;
import java.io.IOException;
import isc.fpt.fsale.utils.DialogUtils;
import static isc.fpt.fsale.utils.Util.writeFileApkToDisk;

/**
 * Created by haulc3 on 11,March,2019
 */
public class DownloadApp extends AsyncTask<String, Integer, String> {
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private Dialog dialog;

    public DownloadApp(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = DialogUtils.showLoadingDialog(context);
        if(!dialog.isShowing()){
            dialog.show();
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            HttpGet httpGet = new HttpGet(String.valueOf(strings[0]));
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(httpGet);
            int status = response.getStatusLine().getStatusCode();
            if (status == 200) {
                return writeFileApkToDisk(response.getEntity().getContent());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String fileLocation) {
        super.onPostExecute(fileLocation);
        if (dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
        installNewApp(fileLocation);
    }

    private void installNewApp(String location) {
        File file = new File(location, "app.apk");
        String mimeType = "application/vnd.android.package-archive";
        Intent promptInstall = new Intent(Intent.ACTION_VIEW);
        promptInstall.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        promptInstall.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            Uri apkURI = FileProvider.getUriForFile(
                    context,
                    context.getApplicationContext().getPackageName() + ".provider", file);
            promptInstall.setDataAndType(apkURI, mimeType);
        }else{
            promptInstall.setDataAndType(Uri.fromFile(file), mimeType);
        }
        context.startActivity(promptInstall);
    }
}
