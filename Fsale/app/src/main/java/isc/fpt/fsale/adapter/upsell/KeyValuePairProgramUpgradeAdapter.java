package isc.fpt.fsale.adapter.upsell;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.upsell.response.ProgramUpgradeModel;

public class KeyValuePairProgramUpgradeAdapter extends ArrayAdapter<ProgramUpgradeModel> {
    private List<ProgramUpgradeModel> lstObj;
    private Context mContext;
    private boolean hasInitText = false;
    private int iColor = 2;
    private Typeface tf;


    public KeyValuePairProgramUpgradeAdapter(Context context, int my_spinner_style, List<ProgramUpgradeModel> mProgram, int left) {
        super(context, my_spinner_style);
        this.mContext = context;
        this.lstObj = mProgram;
        this.tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Bold.ttf");
        this.hasInitText = true;
    }


    public List<ProgramUpgradeModel> getList() {
        return lstObj;
    }

    public int getCount() {
        if (lstObj != null)
            return lstObj.size();
        return 0;
    }

    public ProgramUpgradeModel getItem(int position) {
        if (lstObj != null)
            return lstObj.get(position);
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(final int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setGravity(Gravity.CENTER);
        label.setText(lstObj.get(position).getDescription());
        label.setTypeface(tf);
        int padding = (int) mContext.getResources().getDimension(R.dimen.padding_medium);
        label.setPadding(padding, padding, padding, padding);
        label.setTextColor(Color.parseColor("#000000"));
        return label;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setTypeface(tf);
        label.setTextColor(Color.parseColor("#000000"));
        label.setText(lstObj.get(position).getDescription());
        return label;
    }
}
