package isc.fpt.fsale.ui.fpt_camera;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.extra_ott.adapter.TabAdapter;
import isc.fpt.fsale.ui.fpt_camera.fragment.FptCameraCusInfo;
import isc.fpt.fsale.ui.fpt_camera.fragment.FptCameraService;
import isc.fpt.fsale.ui.fpt_camera.fragment.FptCameraTotal;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.ui.fpt_camera.model.CameraServiceItem;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera;
import isc.fpt.fsale.ui.fpt_camera.model.PromotionDetail;
import isc.fpt.fsale.ui.fpt_camera.model.RegisterFptCameraModel;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.LIST_CAT_SERVICE;
import static isc.fpt.fsale.utils.Constants.MODEL_REGISTER;
import static isc.fpt.fsale.utils.Constants.POTENTIAL_OBJECT;
import static isc.fpt.fsale.utils.Constants.TAG_CONTRACT;

public class FptCameraActivity extends BaseActivitySecond implements ViewPager.OnPageChangeListener {
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RelativeLayout rltBack;

    private FptCameraCusInfo fragCusInfo;
    private FptCameraService fragService;
    public FptCameraTotal fragTotal;
    public RegisterFptCameraModel mRegisterFptCameraModel;
    public List<CategoryServiceList> mListService;
    private RegistrationDetailModel mRegistrationModel;
    private PotentialObjModel mPotentialObjModel;
    private ObjectDetailModel mObjectModel;
    private boolean isUpdatePromotion = false;
    private boolean isUpdateCusType = false;

    private int[] tabIcons = {
            R.drawable.tab_customer_info,
            R.drawable.tab_service,
            R.drawable.tab_money_total
    };

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_fpt_camera;
    }

    @Override
    public void showError(String errorMessage) {
        Common.alertDialog(errorMessage, this);
    }

    public void setIsUpdatePromotion(boolean isUpdatePromotion) {
        this.isUpdatePromotion = isUpdatePromotion;
    }

    public boolean getIsUpdatePromotion() {
        return isUpdatePromotion;
    }

    public boolean getIsUpdateCusType() {
        return isUpdateCusType;
    }

    public void setUpdateCusType(boolean updateCusType) {
        isUpdateCusType = updateCusType;
    }

    public RegisterFptCameraModel getRegisterModel() {
        return mRegisterFptCameraModel != null ? mRegisterFptCameraModel : new RegisterFptCameraModel();
    }

    public ObjectDetailModel getObjectDetailModel() {
        return mObjectModel;
    }

    public List<CategoryServiceList> getListService() {
        return mListService != null ? mListService : new ArrayList<CategoryServiceList>();
    }

    @Override
    protected void initView() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);

        mRegisterFptCameraModel = new RegisterFptCameraModel();
        adapter = new TabAdapter(getSupportFragmentManager(), this);
        fragCusInfo = new FptCameraCusInfo();
        fragService = new FptCameraService();
        fragTotal = new FptCameraTotal();
        adapter.addFragment(fragCusInfo, getResources().getString(R.string.lbl_tab_cus_info), tabIcons[0]);
        adapter.addFragment(fragService, getResources().getString(R.string.lbl_tab_service), tabIcons[1]);
        adapter.addFragment(fragTotal, getResources().getString(R.string.lbl_tab_total), tabIcons[2]);

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOnPageChangeListener(this);
        highLightCurrentTab(0);
        getDataIntent();
        bindDataRegistrationModelToRegister();
        bindDataPotentialCustomer();
        bindDataContractToRegister();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelableArrayList(LIST_CAT_SERVICE) != null) {//lấy TT PĐK khi tạo mới
            mListService = bundle.getParcelableArrayList(LIST_CAT_SERVICE);
        }
        if (bundle != null && bundle.getParcelable(POTENTIAL_OBJECT) != null) {//lấy thông tin từ KHTN
            mPotentialObjModel = bundle.getParcelable(POTENTIAL_OBJECT);
        }
        if (getIntent().getParcelableExtra(MODEL_REGISTER) != null) {//lấy thông tin PĐK khi cập nhật
            mRegistrationModel = getIntent().getParcelableExtra(MODEL_REGISTER);
        }
        if (bundle != null && bundle.getParcelable(TAG_CONTRACT) != null) {//object thông tin HĐ bán Combo
            mObjectModel = bundle.getParcelable(TAG_CONTRACT);
        }
    }

    private void bindDataContractToRegister() {
        if (mObjectModel == null) {
            return;
        }
        mRegisterFptCameraModel.setBaseObjID(mObjectModel.getObjID());
        mRegisterFptCameraModel.setBaseContract(mObjectModel.getContract());
        mRegisterFptCameraModel.setAddress(mObjectModel.getAddress() == null ? "" :
                mObjectModel.getAddress());
        mRegisterFptCameraModel.setBillTo_Street(mObjectModel.getBillTo_Street() == null ?
                "" : mObjectModel.getBillTo_Street());
        mRegisterFptCameraModel.setBillTo_Ward(mObjectModel.getBillTo_Ward() == null ?
                "" : mObjectModel.getBillTo_Ward());
        mRegisterFptCameraModel.setContact_1(mObjectModel.getContact_1() != null ?
                mObjectModel.getContact_1() : "");
        mRegisterFptCameraModel.setContact_2(mObjectModel.getContact_2() != null ?
                mObjectModel.getContact_2() : "");
        mRegisterFptCameraModel.setCusTypeDetail(String.valueOf(mObjectModel.getCusTypeDetail()));
        mRegisterFptCameraModel.setNote(mObjectModel.getNote() != null ?
                mObjectModel.getNote() : "");
        mRegisterFptCameraModel.setEmail(mObjectModel.getEmail() != null ?
                mObjectModel.getEmail() : "");
        mRegisterFptCameraModel.setFullName(mObjectModel.getFullName() != null ?
                mObjectModel.getFullName() : "");
        mRegisterFptCameraModel.setImageInfo(mObjectModel.getImageInfo() != null ?
                mObjectModel.getImageInfo() : "");
        mRegisterFptCameraModel.setLocationID(String.valueOf(mObjectModel.getLocationID()));
        mRegisterFptCameraModel.setPhone_1(mObjectModel.getPhone_1() != null ?
                mObjectModel.getPhone_1() : "");
        mRegisterFptCameraModel.setPhone_2(mObjectModel.getPhone_2() != null ?
                mObjectModel.getPhone_2() : "");
        mRegisterFptCameraModel.setObjectCamera(
                new ObjectCamera(
                        new ArrayList<CameraDetail>(), new ArrayList<CloudDetail>(),
                        new SetupDetail(), new PromotionDetail()
                )
        );
        mRegisterFptCameraModel.setLocationID(mObjectModel.getLocationID() == null ? "" :
                mObjectModel.getLocationID());
        mRegisterFptCameraModel.setUserName(mObjectModel.getSaleName() == null ? "" :
                mObjectModel.getSaleName());
        mRegisterFptCameraModel.setBaseImageInfo(mObjectModel.getBaseImageInfo() == null ? "" :
                mObjectModel.getBaseImageInfo());
        mRegisterFptCameraModel.setBaseRegIDImageInfo(mObjectModel.getBaseRegIDImageInfo());
        mRegisterFptCameraModel.setBirthday(mObjectModel.getBirthday() == null ? "" :
                mObjectModel.getBirthday());
        mRegisterFptCameraModel.setAddressPassport(mObjectModel.getAddressPassport() == null ? "" :
                mObjectModel.getAddressPassport());
        mRegisterFptCameraModel.setPassport(mObjectModel.getPassport() == null ? "" :
                mObjectModel.getPassport());
        mRegisterFptCameraModel.setCusType(mObjectModel.getCusType());
        mRegisterFptCameraModel.setTypeHouse(mObjectModel.getTypeHouse());
        mRegisterFptCameraModel.setLot(mObjectModel.getLot() == null ? "" :
                mObjectModel.getLot());
        mRegisterFptCameraModel.setFloor(mObjectModel.getFloor() == null ? "" :
                mObjectModel.getFloor());
        mRegisterFptCameraModel.setRoom(mObjectModel.getRoom() == null ? "" :
                mObjectModel.getRoom());
        mRegisterFptCameraModel.setNameVilla(mObjectModel.getNameVilla() == null ? "" :
                mObjectModel.getNameVilla());
        mRegisterFptCameraModel.setBillTo_Number(mObjectModel.getBillTo_Number() == null ? "" :
                mObjectModel.getBillTo_Number());
        mRegisterFptCameraModel.setBillTo_District(mObjectModel.getBillTo_District() == null ? "" :
                mObjectModel.getBillTo_District());
        mRegisterFptCameraModel.setTaxId(mObjectModel.getTaxID() == null ?
                "" : mObjectModel.getTaxID());
        mRegisterFptCameraModel.setPosition(mObjectModel.getPosition());
        mRegisterFptCameraModel.setNameVillaDes(mObjectModel.getNameVillaDes() == null ?
                "" : mObjectModel.getNameVillaDes());
        mRegisterFptCameraModel.setType_1(mObjectModel.getType_1());
        mRegisterFptCameraModel.setType_2(mObjectModel.getType_2());
        mRegisterFptCameraModel.setDescriptionIBB(mObjectModel.getDescriptionIBB());
        mRegisterFptCameraModel.setBillTo_CityVN(mObjectModel.getBillTo_CityVN());
        mRegisterFptCameraModel.setBillTo_DistrictVN(mObjectModel.getBillTo_DistrictVN());
        mRegisterFptCameraModel.setBillTo_StreetVN(mObjectModel.getBillTo_StreetVN());
        mRegisterFptCameraModel.setBillTo_WardVN(mObjectModel.getBillTo_WardVN());
        mRegisterFptCameraModel.setNameVillaVN(mObjectModel.getNameVillaVN());
        mRegisterFptCameraModel.setRegType(0);// 0 = bán mới, 1 = bán thêm
        mRegisterFptCameraModel.setListDevice(new ArrayList<>());
        mRegisterFptCameraModel.setSourceType(mObjectModel.getSourceType());
    }

    private void bindDataPotentialCustomer() {
        if (mPotentialObjModel != null) {
            mRegisterFptCameraModel = new RegisterFptCameraModel();
            mRegisterFptCameraModel.setCusTypeDetail(String.valueOf(mPotentialObjModel.getCustomerType()));
            mRegisterFptCameraModel.setAddress(mPotentialObjModel.getAddress() != null ?
                    mPotentialObjModel.getAddress() : "");
            mRegisterFptCameraModel.setBillTo_City(mPotentialObjModel.getBillTo_City() != null ?
                    mPotentialObjModel.getBillTo_City() : "");
            mRegisterFptCameraModel.setBillTo_CityVN(mPotentialObjModel.getBillTo_CityVN() != null ?
                    mPotentialObjModel.getBillTo_CityVN() : "");
            mRegisterFptCameraModel.setBillTo_District(mPotentialObjModel.getBillTo_District() != null ?
                    mPotentialObjModel.getBillTo_District() : "");
            mRegisterFptCameraModel.setBillTo_DistrictVN(mPotentialObjModel.getBillTo_DistrictVN() != null ?
                    mPotentialObjModel.getBillTo_DistrictVN() : "");
            mRegisterFptCameraModel.setBillTo_Number(mPotentialObjModel.getBillTo_Number() != null ?
                    mPotentialObjModel.getBillTo_Number() : "");
            mRegisterFptCameraModel.setBillTo_Street(mPotentialObjModel.getBillTo_Street() != null ?
                    mPotentialObjModel.getBillTo_Street() : "");
            mRegisterFptCameraModel.setBillTo_StreetVN(mPotentialObjModel.getBillTo_StreetVN() != null ?
                    mPotentialObjModel.getBillTo_StreetVN() : "");
            mRegisterFptCameraModel.setBillTo_Ward(mPotentialObjModel.getBillTo_Ward() != null ?
                    mPotentialObjModel.getBillTo_Ward() : "");
            mRegisterFptCameraModel.setBillTo_WardVN(mPotentialObjModel.getBillTo_WardVN() != null ?
                    mPotentialObjModel.getBillTo_WardVN() : "");
            mRegisterFptCameraModel.setContact_1(mPotentialObjModel.getContact1() != null ?
                    mPotentialObjModel.getContact1() : "");
            mRegisterFptCameraModel.setContact_2(mPotentialObjModel.getContact2() != null ?
                    mPotentialObjModel.getContact2() : "");
            mRegisterFptCameraModel.setNote(mPotentialObjModel.getNote() != null ?
                    mPotentialObjModel.getNote() : "");
            mRegisterFptCameraModel.setEmail(mPotentialObjModel.getEmail() != null ?
                    mPotentialObjModel.getEmail() : "");
            mRegisterFptCameraModel.setFloor(mPotentialObjModel.getFloor() != null ?
                    mPotentialObjModel.getFloor() : "");
            mRegisterFptCameraModel.setFullName(mPotentialObjModel.getFullName() != null ?
                    mPotentialObjModel.getFullName() : "");
            mRegisterFptCameraModel.setLocationID(String.valueOf(mPotentialObjModel.getLocationID()));
            mRegisterFptCameraModel.setLot(mPotentialObjModel.getLot() != null ?
                    mPotentialObjModel.getLot() : "");
            mRegisterFptCameraModel.setNameVilla(mPotentialObjModel.getNameVilla() != null ?
                    mPotentialObjModel.getNameVilla() : "");
            mRegisterFptCameraModel.setNameVillaVN(mPotentialObjModel.getNameVillaVN() != null ?
                    mPotentialObjModel.getNameVillaVN() : "");
            mRegisterFptCameraModel.setNameVillaDes(mPotentialObjModel.getNameVillaDes() != null ?
                    mPotentialObjModel.getNameVillaDes() : "");
            mRegisterFptCameraModel.setPassport(mPotentialObjModel.getPassport() != null ?
                    mPotentialObjModel.getPassport() : "");
            if (!mPotentialObjModel.getPhone1().trim().equals("")) {
                mRegisterFptCameraModel.setPhone_1(mPotentialObjModel.getPhone1() != null ?
                        mPotentialObjModel.getPhone1() : "");
                mRegisterFptCameraModel.setType_1(4);
            }
            if (!mPotentialObjModel.getPhone2().trim().equals("")) {
                mRegisterFptCameraModel.setPhone_2(mPotentialObjModel.getPhone2() != null ?
                        mPotentialObjModel.getPhone2() : "");
                mRegisterFptCameraModel.setType_2(4);
            }
            mRegisterFptCameraModel.setPosition(mPotentialObjModel.getPosition());
            mRegisterFptCameraModel.setRoom(mPotentialObjModel.getRoom() != null ?
                    mPotentialObjModel.getRoom() : "");
            mRegisterFptCameraModel.setSupporter(mPotentialObjModel.getSupporter() != null ?
                    mPotentialObjModel.getSupporter() : "");
            mRegisterFptCameraModel.setTaxId(mPotentialObjModel.getTaxID() != null ?
                    mPotentialObjModel.getTaxID() : "");
            mRegisterFptCameraModel.setPotentialID(mPotentialObjModel.getID());
            mRegisterFptCameraModel.setTypeHouse(mPotentialObjModel.getTypeHouse());
            mRegisterFptCameraModel.setObjectCamera(
                    new ObjectCamera(
                            new ArrayList<CameraDetail>(), new ArrayList<CloudDetail>(), new SetupDetail(), new PromotionDetail()
                    )
            );
            mRegisterFptCameraModel.setRegType(0);// 0 = bán mới, 1 = bán thêm
            mRegisterFptCameraModel.setListDevice(new ArrayList<>());
            mRegisterFptCameraModel.setSourceType(mPotentialObjModel.getSourceType());
        }
    }

    private void bindDataRegistrationModelToRegister() {
        if (mRegistrationModel == null) {
            return;
        }
        mRegisterFptCameraModel.setID(mRegistrationModel.getID());
        mRegisterFptCameraModel.setAddress(mRegistrationModel.getAddress() != null ?
                mRegistrationModel.getAddress() : "");
        mRegisterFptCameraModel.setAddressPassport(mRegistrationModel.getAddressPassport() != null ?
                mRegistrationModel.getAddressPassport() : "");
        mRegisterFptCameraModel.setBirthday(mRegistrationModel.getBirthday() != null ?
                mRegistrationModel.getBirthday() : "");
        mRegisterFptCameraModel.setBillTo_City(mRegistrationModel.getBillTo_City() != null ?
                mRegistrationModel.getBillTo_City() : "");
        mRegisterFptCameraModel.setBillTo_District(mRegistrationModel.getBillTo_District() != null ?
                mRegistrationModel.getBillTo_District() : "");
        mRegisterFptCameraModel.setBillTo_Number(mRegistrationModel.getBillTo_Number() != null ?
                mRegistrationModel.getBillTo_Number() : "");
        mRegisterFptCameraModel.setBillTo_Street(mRegistrationModel.getBillTo_Street() != null ?
                mRegistrationModel.getBillTo_Street() : "");
        mRegisterFptCameraModel.setBillTo_Ward(mRegistrationModel.getBillTo_Ward() != null ?
                mRegistrationModel.getBillTo_Ward() : "");
        mRegisterFptCameraModel.setContact_1(mRegistrationModel.getContact_1() != null ?
                mRegistrationModel.getContact_1() : "");
        mRegisterFptCameraModel.setContact_2(mRegistrationModel.getContact_2() != null ?
                mRegistrationModel.getContact_2() : "");
        mRegisterFptCameraModel.setCusType(mRegistrationModel.getCusType());
        mRegisterFptCameraModel.setCusTypeDetail(mRegistrationModel.getCusTypeDetail() != null ?
                mRegistrationModel.getCusTypeDetail() : "");
        mRegisterFptCameraModel.setNote(mRegistrationModel.getNote() != null ?
                mRegistrationModel.getNote() : "");
        mRegisterFptCameraModel.setEmail(mRegistrationModel.getEmail() != null ?
                mRegistrationModel.getEmail() : "");
        mRegisterFptCameraModel.setFloor(mRegistrationModel.getFloor() != null ?
                mRegistrationModel.getFloor() : "");
        mRegisterFptCameraModel.setFullName(mRegistrationModel.getFullName() != null ?
                mRegistrationModel.getFullName() : "");
        mRegisterFptCameraModel.setLocationID(String.valueOf(mRegistrationModel.getLocationID()));
        mRegisterFptCameraModel.setLot(mRegistrationModel.getLot() != null ?
                mRegistrationModel.getLot() : "");
        mRegisterFptCameraModel.setNameVilla(mRegistrationModel.getNameVilla());
        mRegisterFptCameraModel.setNameVillaDes(mRegistrationModel.getNameVillaVN() != null ?
                mRegistrationModel.getNameVillaVN() : "");
        mRegisterFptCameraModel.setPassport(mRegistrationModel.getPassport() != null ?
                mRegistrationModel.getPassport() : "");
        mRegisterFptCameraModel.setPhone_1(mRegistrationModel.getPhone_1() != null ?
                mRegistrationModel.getPhone_1() : "");
        mRegisterFptCameraModel.setType_1(mRegistrationModel.getType_1());
        mRegisterFptCameraModel.setPhone_2(mRegistrationModel.getPhone_2() != null ?
                mRegistrationModel.getPhone_2() : "");
        mRegisterFptCameraModel.setType_2(mRegistrationModel.getType_2());
        mRegisterFptCameraModel.setPosition(mRegistrationModel.getPosition());
        mRegisterFptCameraModel.setRoom(mRegistrationModel.getRoom() != null ?
                mRegistrationModel.getRoom() : "");
        mRegisterFptCameraModel.setSupporter(mRegistrationModel.getSupporter() != null ?
                mRegistrationModel.getSupporter() : "");
        mRegisterFptCameraModel.setTaxId(mRegistrationModel.getTaxId() != null ?
                mRegistrationModel.getTaxId() : "");
        mRegisterFptCameraModel.setPotentialID(mRegistrationModel.getPotentialID());
        mRegisterFptCameraModel.setTypeHouse(mRegistrationModel.getTypeHouse());
        mRegisterFptCameraModel.setImageInfo(mRegistrationModel.getImageInfo());
        mRegisterFptCameraModel.setObjectCamera(mRegistrationModel.getObjectCamera() == null ?
                new ObjectCamera(
                        new ArrayList<CameraDetail>(), new ArrayList<CloudDetail>(), new SetupDetail(), new PromotionDetail()
                ) :
                mRegistrationModel.getObjectCamera()
        );
        mRegisterFptCameraModel.setLocalType(mRegistrationModel.getLocalType());
        mRegisterFptCameraModel.setLocalTypeName(mRegistrationModel.getLocalTypeName());
        mRegisterFptCameraModel.setCategoryServiceList(mRegistrationModel.getCategoryServiceList());
        mRegisterFptCameraModel.setDescriptionIBB(mRegistrationModel.getDescriptionIBB());
        mRegisterFptCameraModel.setBaseObjID(mRegistrationModel.getBaseObjID());
        mRegisterFptCameraModel.setBaseContract(mRegistrationModel.getBaseContract());
        mRegisterFptCameraModel.setBaseImageInfo(mRegistrationModel.getBaseImageInfo());
        mRegisterFptCameraModel.setBaseRegIDImageInfo(mRegistrationModel.getBaseRegIDImageInfo());
        mRegisterFptCameraModel.setBillTo_CityVN(mRegistrationModel.getBillTo_CityVN());
        mRegisterFptCameraModel.setBillTo_DistrictVN(mRegistrationModel.getBillTo_DistrictVN());
        mRegisterFptCameraModel.setBillTo_StreetVN(mRegistrationModel.getBillTo_StreetVN());
        mRegisterFptCameraModel.setBillTo_WardVN(mRegistrationModel.getBillTo_WardVN());
        mRegisterFptCameraModel.setNameVillaVN(mRegistrationModel.getNameVillaVN());
        mRegisterFptCameraModel.setRegType(0);// 0 = bán mới, 1 = bán thêm
        mRegisterFptCameraModel.setListDevice(mRegistrationModel.getListDevice() == null ? new ArrayList<Device>() : mRegistrationModel.getListDevice());
        mRegisterFptCameraModel.setSourceType(mRegistrationModel.getSourceType());
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        highLightCurrentTab(position);
        switch (position) {
            case 0://frag cus info
                break;
            case 1:// frag service
                if (!validationFragCusInfo()) {//validation fragment customer info
                    setPageViewPager(0);
                    return;
                }
                if (getIsUpdateCusType()) {
                    fragService.isNoDeploy();
                    setUpdateCusType(false);
                }
                break;
            case 2://frag total
//                if (fragTotal != null) {
//                    fragTotal.is_have_device = fragService.is_have_device;
//                    if (fragTotal.is_have_device && fragService.getListDeviceSelect() != null) {
//                        for (Device item : fragService.getListDeviceSelect()) {
//                            if (item.getPriceID() == 0) {
//                                viewPager.setCurrentItem(1);
//                                Common.alertDialog("Chưa chọn giá thiết bị!", this);
//                                return;
//                            }
//                        }
//                    }
//                }
                assert fragTotal != null;

                if (!validationFragCusInfo()) {
                    setPageViewPager(0);
                    return;
                }

                if (!validationFragService()) {//validation fragment service
                    setPageViewPager(1);
                    return;
                }

                if(!validationDevice())
                {
                    setPageViewPager(1);
                    return;
                }
                //put data to object
                submitDataToRegistration();
                fragTotal.getTotal();
                fragTotal.bindData();
                break;
        }
    }

    public List<CameraServiceItem> getListCameraService() {
        List<CameraServiceItem> mList = new ArrayList<>();
        if (fragService.mListCamera != null && fragService.mListCamera.size() != 0) {
            for (CameraDetail item : fragService.mListCamera) {
                mList.add(new CameraServiceItem(item.getServiceType(), item.getPackID(), item.getQty()));
            }
        }
        if (fragService.mListCloud != null && fragService.mListCloud.size() != 0) {
            for (CloudDetail item : fragService.mListCloud) {
                mList.add(new CameraServiceItem(item.getServiceType(), item.getPackID(), item.getQty()));
            }
        }
        if (fragService.mSetupDetail != null) {
            SetupDetail mSetupDetail = fragService.mSetupDetail;
            mList.add(new CameraServiceItem(mSetupDetail.getServiceType(), mSetupDetail.getPackID(), mSetupDetail.getQty()));
        }

        if (fragTotal.mPromotionDetail != null && !getIsUpdatePromotion()) {
            PromotionDetail mPromotionDetail = fragTotal.mPromotionDetail;
            mList.add(new CameraServiceItem(0, mPromotionDetail.getPromoID(), mPromotionDetail.getQty()));
        } else {
            fragTotal.mPromotionDetail = null;
        }
        return mList;
    }

    private boolean validationFragService() {
        if (fragService.mListCamera.size() == 0 &&
                fragService.mListDevice.size() == 0 &&
                fragService.mListCloud.size() == 0) {
            Common.alertDialog(getString(R.string.msg_select_service_camera), this);
            return false;
        } else {
            if (fragService.mListCamera.size() != 0) {
                for (CameraDetail item : fragService.mListCamera) {
                    if (TextUtils.isEmpty(item.getPackName())) {
                        Common.alertDialog(getString(R.string.msg_select_camera_package), this);
                        return false;
                    }
                }
            }
            if (fragService.mListCloud.size() != 0) {
                for (CloudDetail item : fragService.mListCloud) {
                    if (TextUtils.isEmpty(item.getPackName())) {
                        Common.alertDialog(getString(R.string.msg_select_cloud_package), this);
                        return false;
                    }
                }
            }
            if (fragService.mListDevice.size() != 0) {
                for (Device item : fragService.mListDevice) {
                    if (TextUtils.isEmpty(item.getPriceText())) {
                        Common.alertDialog(getString(R.string.msg_select_device_contract), this);
                        return false;
                    } else if (item.getNumber() == 0) {
                        Common.alertDialog(getString(R.string.msg_select_device_number), this);
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void setPageViewPager(final int index) {
        viewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(index);
            }
        }, 10);
    }

    private void submitDataToRegistration() {
        mRegisterFptCameraModel.setLocationID(Constants.LOCATION_ID);
        mRegisterFptCameraModel.setBillTo_City(Constants.LOCATION_NAME);
        mRegisterFptCameraModel.setUserName(Constants.USERNAME);
        mRegisterFptCameraModel.setFullName(fragCusInfo.txtCustomerName.getText().toString());
        mRegisterFptCameraModel.setPassport(fragCusInfo.txtIdentityCard.getText().toString());
        mRegisterFptCameraModel.setBirthday(fragCusInfo.txtBirthDay.getText().toString());
        mRegisterFptCameraModel.setAddressPassport(fragCusInfo.txtAddressId.getText().toString());
        mRegisterFptCameraModel.setTaxId(fragCusInfo.txtTaxNum.getText().toString());
        mRegisterFptCameraModel.setEmail(fragCusInfo.txtEmail.getText().toString());
        mRegisterFptCameraModel.setPhone_1(fragCusInfo.txtPhone1.getText().toString());
        mRegisterFptCameraModel.setContact_1(fragCusInfo.txtContactPhone1.getText().toString());
        mRegisterFptCameraModel.setPhone_2(fragCusInfo.txtPhone2.getText().toString());
        mRegisterFptCameraModel.setContact_2(fragCusInfo.txtContactPhone2.getText().toString());
        mRegisterFptCameraModel.setLot(fragCusInfo.txtLot.getText().toString());
        mRegisterFptCameraModel.setFloor(fragCusInfo.txtFloor.getText().toString());
        mRegisterFptCameraModel.setRoom(fragCusInfo.txtRoom.getText().toString());
        mRegisterFptCameraModel.setBillTo_Number(fragCusInfo.txtHouseNum.getText().toString());
        mRegisterFptCameraModel.setNote(fragCusInfo.txtHouseDesc.getText().toString());
        mRegisterFptCameraModel.setDescriptionIBB(fragCusInfo.txtDescriptionIBB.getText().toString());
        mRegisterFptCameraModel.setAddress(getAddress());
        mRegisterFptCameraModel.setImageInfo(fragCusInfo.strListImageDocumentInfo);
        mRegisterFptCameraModel.getObjectCamera().setCameraDetail(fragService.mListCamera);
        mRegisterFptCameraModel.getObjectCamera().setCloudDetail(fragService.mListCloud);
        mRegisterFptCameraModel.getObjectCamera().setSetupDetail(fragService.mSetupDetail);
        mRegisterFptCameraModel.setListDevice(fragService.mListDevice);
    }

    private boolean validationDevice() {
        boolean isValidate = false;
        if (fragService.is_have_device && fragService.mListDevice.size() == 0) {
            showError("Vui lòng chọn thiết bị");
        } else {
            isValidate = true;
        }
        return isValidate;
    }

    private boolean validationFragCusInfo() {
        boolean isValidate = false;
        if(fragCusInfo.tvSourceType != null &&
                TextUtils.isEmpty(fragCusInfo.tvSourceType.getText().toString())){
            showError(getString(R.string.txt_message_choose_source_type));
        } else if (fragCusInfo.txtCusType != null &&
                TextUtils.isEmpty(fragCusInfo.txtCusType.getText().toString())) {
            showError(getString(R.string.txt_message_choose_cus_type));
        } else if (fragCusInfo.txtCusTypeDetail != null &&
                TextUtils.isEmpty(fragCusInfo.txtCusTypeDetail.getText().toString())) {
            showError(getString(R.string.txt_message_choose_cus_type_detail));
        } else if (fragCusInfo.txtCusTypeDetail != null &&
                !TextUtils.isEmpty(fragCusInfo.txtCusTypeDetail.getText().toString()) &&
                Integer.parseInt(mRegisterFptCameraModel.getCusTypeDetail()) == 10 &&
                TextUtils.isEmpty(fragCusInfo.txtTaxNum.getText())) {//KH Công Ty
            showError(getString(R.string.txt_message_enter_tax_id));
        } else if (fragCusInfo.txtEmail != null &&
                !TextUtils.isEmpty(fragCusInfo.txtEmail.getText().toString()) &&
                !Common.checkMailValid(fragCusInfo.txtEmail.getText().toString())) {
            // CHECK EMAIL
            showError(getString(R.string.txt_message_invalid_email));
        } else if (fragCusInfo.txtIdentityCard != null &&
                TextUtils.isEmpty(fragCusInfo.txtIdentityCard.getText().toString()) &&
                Integer.parseInt(mRegisterFptCameraModel.getCusTypeDetail()) != 10) {
            showError(getString(R.string.txt_message_enter_identity_card));
        } else if (fragCusInfo.txtAddressId != null &&
                TextUtils.isEmpty(fragCusInfo.txtAddressId.getText().toString()) &&
                Integer.parseInt(mRegisterFptCameraModel.getCusTypeDetail()) != 10) {
            showError(getString(R.string.txt_message_enter_address_id));
        } else if (fragCusInfo.txtCustomerName != null &&
                TextUtils.isEmpty(fragCusInfo.txtCustomerName.getText().toString())) {
            showError(getString(R.string.txt_message_enter_cus_name));
        } else if (fragCusInfo.txtBirthDay != null &&
                TextUtils.isEmpty(fragCusInfo.txtBirthDay.getText().toString())) {
            showError(getString(R.string.txt_message_choose_birthday));
        } else if (fragCusInfo.tvPhone1 != null &&
                TextUtils.isEmpty(fragCusInfo.tvPhone1.getText().toString())) {
            showError(getString(R.string.txt_message_choose_phone_type));
        } else if (fragCusInfo.txtPhone1 != null &&
                TextUtils.isEmpty(fragCusInfo.txtPhone1.getText().toString())) {
            showError(getString(R.string.txt_message_enter_phone_number));
        } else if (fragCusInfo.txtContactPhone1 != null &&
                TextUtils.isEmpty(fragCusInfo.txtContactPhone1.getText().toString())) {
            showError(getString(R.string.txt_message_enter_contact_people));
        } else if (fragCusInfo.tvDistrict != null &&
                TextUtils.isEmpty(fragCusInfo.tvDistrict.getText().toString())) {
            showError(getString(R.string.txt_message_choose_district));
        } else if (fragCusInfo.tvWard != null &&
                TextUtils.isEmpty(fragCusInfo.tvWard.getText().toString())) {
            showError(getString(R.string.txt_message_choose_ward));
        } else if (fragCusInfo.tvHouseType != null &&
                TextUtils.isEmpty(fragCusInfo.tvHouseType.getText().toString())) {
            showError(getString(R.string.txt_message_choose_house_type));
        } else if (fragCusInfo.strListImageDocumentInfo != null &&
                TextUtils.isEmpty(fragCusInfo.strListImageDocumentInfo) &&
                (mObjectModel == null || mRegisterFptCameraModel.getBaseObjID() == 0)) {
            showError(getString(R.string.txt_message_upload_document_image));
        } else if (fragCusInfo.tvHouseType != null &&
                !TextUtils.isEmpty(fragCusInfo.tvHouseType.getText().toString())) {
            if (mRegisterFptCameraModel.getTypeHouse() == 1) {//nhà phố
                if (fragCusInfo.tvStreet != null &&
                        TextUtils.isEmpty(fragCusInfo.tvStreet.getText().toString())) {
                    showError(getString(R.string.txt_message_choose_street));
                } else if (fragCusInfo.txtHouseNum != null &&
                        TextUtils.isEmpty(fragCusInfo.txtHouseNum.getText().toString())) {
                    showError(getString(R.string.txt_message_enter_house_number));
                } else {
                    isValidate = true;
                }
            } else if (mRegisterFptCameraModel.getTypeHouse() == 2) {// cu xa, chung cu, cho, villa, thuong xa
                if (fragCusInfo.tvStreet != null &&
                        TextUtils.isEmpty(fragCusInfo.tvStreet.getText().toString())) {
                    showError(getString(R.string.txt_message_choose_street));
                } else if (fragCusInfo.txtFloor != null &&
                        TextUtils.isEmpty(fragCusInfo.txtFloor.getText().toString())) {
                    showError(getString(R.string.txt_message_enter_floor));
                } else if (fragCusInfo.txtRoom != null &&
                        TextUtils.isEmpty(fragCusInfo.txtRoom.getText().toString())) {
                    showError(getString(R.string.txt_message_enter_room));
                } else if (fragCusInfo.tvApartment != null &&
                        TextUtils.isEmpty(fragCusInfo.tvApartment.getText().toString())) {
                    showError(getString(R.string.txt_message_choose_apartment));
                } else {
                    isValidate = true;
                }
            } else if (mRegisterFptCameraModel.getTypeHouse() == 3) {//Nhà không địa chỉ
                if (fragCusInfo.tvStreet != null &&
                        TextUtils.isEmpty(fragCusInfo.tvStreet.getText().toString())) {
                    showError(getString(R.string.txt_message_choose_street));
                } else if (fragCusInfo.tvHousePosition != null &&
                        TextUtils.isEmpty(fragCusInfo.tvHousePosition.getText().toString())) {
                    showError(getString(R.string.txt_message_choose_house_position));
                } else if (fragCusInfo.txtHouseNum != null &&
                        TextUtils.isEmpty(fragCusInfo.txtHouseNum.getText().toString())) {
                    showError(getString(R.string.txt_message_enter_house_number));
                } else {
                    isValidate = true;
                }
            } else {
                isValidate = true;
            }
        } else {
            isValidate = true;
        }
        return isValidate;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void highLightCurrentTab(int position) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(adapter.getTabView(i));
        }
        TabLayout.Tab tab = tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(adapter.getSelectedTabView(position));
    }

    public String getAddress() {
        String result = "";
        String sStreetSelected = "";
        String sApartmentSelected = "";
        int sHousePositionSelected = 0;
        String sCityNameValue = "";
        String sDistrictSelected = "";
        int sHouseType = -1;

        try {
            sDistrictSelected = fragCusInfo.tvDistrict != null ?
                    fragCusInfo.tvDistrict.getText().toString() : "";
            sStreetSelected = fragCusInfo.tvStreet != null ?
                    fragCusInfo.tvStreet.getText().toString() : "";
            sApartmentSelected = fragCusInfo.tvApartment != null ?
                    fragCusInfo.tvApartment.getText().toString() : "";
            sHouseType = mRegisterFptCameraModel.getTypeHouse();
            if (Constants.LST_REGION.size() > 0) {
                sCityNameValue = Constants.LST_REGION.get(0).getDescription();
            }
            if (fragCusInfo.housePositionPanel.getVisibility() == View.VISIBLE) {
                sHousePositionSelected = mRegisterFptCameraModel.getPosition();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sHouseType == 1) /*nhà Phố*/ {
            // diachi = số nhà + tổ ấp + , Phường + , Quận + , Tỉnh
            if (!Common.isEmpty(fragCusInfo.txtHouseNum))
                result += fragCusInfo.txtHouseNum.getText();
            if (!sStreetSelected.isEmpty())
                result += " " + sStreetSelected;

        } else if (sHouseType == 2) /*Chung Cư*/ {
            // dia chi = lo + tang + phòng + tên chung cư
            if (!Common.isEmpty(fragCusInfo.txtLot))
                result += "Lo " + fragCusInfo.txtLot.getText() + ", ";

            if (!Common.isEmpty(fragCusInfo.txtFloor))
                result += "T. " + fragCusInfo.txtFloor.getText() + ", ";

            if (!Common.isEmpty(fragCusInfo.txtRoom))
                result += "P. " + fragCusInfo.txtRoom.getText() + " ";

            if (!sApartmentSelected.isEmpty())
                result += sApartmentSelected + " ";

            if (!sStreetSelected.isEmpty())
                result += sStreetSelected;

        } else if (sHouseType == 3) /*Nhà không địa chỉ*/ {
            if (!Common.isEmpty(fragCusInfo.txtHouseNum))
                result += fragCusInfo.txtHouseNum.getText() + " ";

            if (!sStreetSelected.isEmpty())
                result += sStreetSelected;

            if (sHousePositionSelected > 0) {
                try {
                    String position = fragCusInfo.tvHousePosition != null ?
                            fragCusInfo.tvHousePosition.getText().toString() : "";
                    result += "(" + Common.convertVietNamToEnglishChar(position) + ")";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        String sWardSelected = fragCusInfo.tvWard != null ?
                fragCusInfo.tvWard.getText().toString() : "";
        if (!sWardSelected.isEmpty())
            result += ", " + sWardSelected;
        if (!sDistrictSelected.equals("-1"))
            result += ", " + sDistrictSelected + ", " + sCityNameValue;

        return result;
    }

    @Override
    public void onBackPressed() {
        try {
            Dialog dialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.txt_message_exits_create_registration))
                    .setCancelable(false)
                    .setPositiveButton(
                            getResources().getString(R.string.lbl_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                }
                            })
                    .setNegativeButton(
                            getResources().getString(R.string.lbl_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
