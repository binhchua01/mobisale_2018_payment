package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListPotentialObjSurveyListActivity;
import isc.fpt.fsale.model.PotentialObjSurveyModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.google.gson.Gson;

public class GetPotentialObjSurveyList implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public GetPotentialObjSurveyList(Context context, int PotentialObjID, int Agent, String AgentName, int PageNumber) {
        mContext = context;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("PotentialObjID", PotentialObjID);
            jsonObject.put("Agent", Agent);
            jsonObject.put("AgentName", AgentName);
            jsonObject.put("PageNumber", PageNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = "Đang lấy dữ liệu...";
        String TAG_METHOD_NAME = "GetPotentialObjSurveyList";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, jsonObject, Services.JSON_POST_OBJECT,
                message, GetPotentialObjSurveyList.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<PotentialObjSurveyModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                if (jsObj.getInt(Constants.ERROR_CODE) == 0) {//OK not Error
                    JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        lst.add(new Gson().fromJson(jsonArray.get(i).toString(), PotentialObjSurveyModel.class));
                    }
                } else {
                    Common.alertDialog(jsObj.getString(Constants.ERROR), mContext);
                }
                loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(List<PotentialObjSurveyModel> lst) {
        try {
            if (mContext.getClass().getSimpleName().equals(ListPotentialObjSurveyListActivity.class.getSimpleName())) {
                ListPotentialObjSurveyListActivity activity = (ListPotentialObjSurveyListActivity) mContext;
                activity.loadData(lst);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}