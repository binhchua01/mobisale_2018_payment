package isc.fpt.fsale.adapter.conllection_bank;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.danh32.fontify.TextView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.callback.banking.onCollectionBanking;
import isc.fpt.fsale.model.Banking.ListBankingModel;

public class ConllectionBankAdapter extends RecyclerView.Adapter<ConllectionBankAdapter.SimpleViewHolder>{

    private Context context;
    private List<ListBankingModel> mModel;
    onCollectionBanking mCallback;

    public ConllectionBankAdapter(Context mContext, List<ListBankingModel> model, onCollectionBanking callback) {
        this.context = mContext;
        this.mModel = model;
        this.mCallback = callback;
    }

    @Override
    public ConllectionBankAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_conlection_bank, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ConllectionBankAdapter.SimpleViewHolder holder, int position) {
        holder.bindView(mModel.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mModel.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder{
        TextView mNumberBank, mStatusActive;
        ImageView mLogoBank;
        CardView cardView;
        public SimpleViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardviewContainer);
            mNumberBank = itemView.findViewById(R.id.txt_number_card_bank);
            mStatusActive = itemView.findViewById(R.id.txt_status_active);
            mLogoBank = itemView.findViewById(R.id.img_bank);
        }

        @SuppressLint("SetTextI18n")
        public void bindView(final ListBankingModel model, final int position){
            mNumberBank.setText(model.getCardNumber());
            mStatusActive.setText(model.getStatusName());

            //Log.d("tag", model.getImgUrl() + model.getCardNumber());
            String url = model.getImgUrl().replace("http", "https");
            Picasso.get().load(url).into(mLogoBank);

            cardView.setOnClickListener(v-> mCallback.onCollectionBankingSuccess(model.getId().toString()));
        }
    }
}
