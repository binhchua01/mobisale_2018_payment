package isc.fpt.fsale.ui.extra_ott.extra_ott_package;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPackageListExtraOTT;
import isc.fpt.fsale.model.DetailPackage;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.extra_ott.RegisterExtraOttModel;
import isc.fpt.fsale.utils.Constants;

public class ExtraOttPackageActivity extends BaseActivitySecond
        implements OnItemClickListener<List<DetailPackage>> {
    private RelativeLayout rltBack;
//    private Button btnUpdate, btnCancel;

    private RegisterExtraOttModel mRegisterExtraOttModel;
    private ExtraOttPackageAdapter mAdapter;
    private List<DetailPackage> mList;
//    private List<DetailPackage> mListSelected;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        btnUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mListSelected != null) {
//                    Intent returnIntent = new Intent();
//                    returnIntent.putParcelableArrayListExtra(Constants.EXTRA_OTT_PACKAGE,
//                            (ArrayList<? extends Parcelable>) mListSelected);
//                    setResult(Activity.RESULT_OK, returnIntent);
//                    finish();
//                } else {
//                    showError(getString(R.string.txt_message_not_selected_package));
//                }
//            }
//        });
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_extra_ott_package;
    }

    @Override
    protected void initView() {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_extra_ott_package);
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
//        btnUpdate = (Button) findViewById(R.id.btn_update);
//        btnCancel = (Button) findViewById(R.id.btn_cancel);
        mList = new ArrayList<>();
        mAdapter = new ExtraOttPackageAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);
        getDataIntent();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(Constants.REGISTRATION_EXTRA_OTT) != null) {
            mRegisterExtraOttModel = bundle.getParcelable(Constants.REGISTRATION_EXTRA_OTT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRegisterExtraOttModel != null) {
            new GetPackageListExtraOTT(this, mRegisterExtraOttModel);
        }
    }

    public void loadListPackage(List<DetailPackage> mList) {
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(List<DetailPackage> object) {
//        mListSelected = object;
        Intent returnIntent = new Intent();
        returnIntent.putParcelableArrayListExtra(Constants.EXTRA_OTT_PACKAGE,
                (ArrayList<? extends Parcelable>) object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
