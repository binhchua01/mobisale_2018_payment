package isc.fpt.fsale.callback.upsell;

import java.util.List;

import isc.fpt.fsale.model.upsell.response.GetPromotionIPTVUpgradeModel;
import isc.fpt.fsale.model.upsell.response.GetPromotionNetUpgradeModel;

public interface PromotionIPTVUpgradeCallBack {
    void onGetSuccess(List<GetPromotionIPTVUpgradeModel> lst);
}
