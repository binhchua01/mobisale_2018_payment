package isc.fpt.fsale.action.upsell;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.callback.upsell.GetObjUpgradeTransCallback;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeTransListModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetObjUpgradeTransList implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private GetObjUpgradeTransCallback mCallback;

    public GetObjUpgradeTransList(Context mContext, String[] arrParams, GetObjUpgradeTransCallback mCallback) {
        this.mCallback = mCallback;
        this.mContext = mContext;
        String[] params = new String[]{"ProgramUpgradeID", "UserName", "Agent", "AgentName", "PageSize", "PageNum"};
        String message = "Xin vui long cho giay lat...";
        String GET_OBJ_UPGRADE_TRANS = "GetObjUpgradeTransList";
         CallServiceTask service = new CallServiceTask(mContext, GET_OBJ_UPGRADE_TRANS, params, arrParams,
                Services.JSON_POST, message, GetObjUpgradeTransList.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<ObjUpgradeTransListModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ObjUpgradeTransListModel.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        mCallback.loadListSuccess(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
