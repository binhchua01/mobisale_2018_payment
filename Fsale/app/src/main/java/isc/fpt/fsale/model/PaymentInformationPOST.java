package isc.fpt.fsale.model;

public class PaymentInformationPOST {
    //Declares variables
    private String SaleName;
    private String RegCode;

    //Constructor
    public PaymentInformationPOST() {
    }

    //Getters and Setters

    public String getSaleName() {
        return SaleName;
    }

    public void setSaleName(String saleName) {
        SaleName = saleName;
    }

    public String getRegCode() {
        return RegCode;
    }

    public void setRegCode(String regCode) {
        RegCode = regCode;
    }
}
