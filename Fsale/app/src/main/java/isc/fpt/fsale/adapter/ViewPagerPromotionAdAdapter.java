package isc.fpt.fsale.adapter;

import java.util.Locale;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import isc.fpt.fsale.ui.fragment.FragmentPromotionAdImageList;
import isc.fpt.fsale.ui.fragment.FragmentPromotionAdTextList;

public class ViewPagerPromotionAdAdapter extends FragmentPagerAdapter {

	private static int NUM_ITEMS = 3;
	private Bundle mImageBundle, mVideoBundle, textBundle;
	
	public ViewPagerPromotionAdAdapter(FragmentManager fm, Bundle imageBundle, Bundle videoBundle, Bundle textBundle) {
		super(fm);
		this.mImageBundle = imageBundle;
		this.mVideoBundle = videoBundle;
		this.textBundle = textBundle;
	}

	@Override
	public Fragment getItem(int position) {
		// getItem is called to instantiate the fragment for the given page.
		// Return a PlaceholderFragment (defined as a static inner class
		// below).
		
		//return CreatePotentialObjMapFragment.newInstance(position + 1);
		switch (position) {
        case 0: // Fragment # 0 - This will show FirstFragment        	
        	//return FragmentPromotionImageView.newInstance(mImageBundle);
        	return FragmentPromotionAdImageList.newInstance(mImageBundle);
        case 1: // Fragment # 0 - This will show FirstFragment different title
            //return CreatePotentialObjMapFragment.newInstance(1, "Page # 2");
        	//return FragmentPromotionVideo.newInstance(mVideoBundle);
        	return FragmentPromotionAdImageList.newInstance(mVideoBundle);
        case 2: // Fragment # 0 - This will show FirstFragment different title
            //return CreatePotentialObjMapFragment.newInstance(1, "Page # 2");
        	//return FragmentPromotionVideo.newInstance(mVideoBundle);
        	return FragmentPromotionAdTextList.newInstance(textBundle);
        default:
            return null;
        }
	}

	@Override
	public int getCount() {
		// Show 3 total pages.
		return NUM_ITEMS;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		Locale l = Locale.getDefault();
		switch (position) {
        case 0: // Fragment # 0 - This will show FirstFragment        	
        	//return FragmentPromotionImageView.newInstance(mImageBundle);
        	return "HÌNH ẢNH".toUpperCase(l);
        case 1: // Fragment # 0 - This will show FirstFragment different title
            //return CreatePotentialObjMapFragment.newInstance(1, "Page # 2");
        	//return FragmentPromotionVideo.newInstance(mVideoBundle);
        	return "VIDEO".toUpperCase(l);
        case 2: // Fragment # 0 - This will show FirstFragment different title
            //return CreatePotentialObjMapFragment.newInstance(1, "Page # 2");
        	//return FragmentPromotionVideo.newInstance(mVideoBundle);
        	return "THÔNG TIN".toUpperCase(l);
        default:
            return "PAGE".toUpperCase(l);
        }
		//String title = "Page " + String.valueOf(position);		
		//return title.toUpperCase(l);
	}
}
