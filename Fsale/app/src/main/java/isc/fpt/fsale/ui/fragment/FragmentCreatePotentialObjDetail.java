package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetDistrictList;
import isc.fpt.fsale.action.GetStreetOrCondo;
import isc.fpt.fsale.action.GetWardList;
import isc.fpt.fsale.action.UpdatePotentialObj;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

//fragment tạo khách hàng tiềm năng, cập nhật khách hàng tiềm năng
public class FragmentCreatePotentialObjDetail extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_TITLE = "section_title";
    private CreatePotentialObjActivity activity;
    private EditText txtFullName, txtPassport, txtTaxID, txtEmail,
            txtFax, txtPhone1, txtPhone2, txtContact1, txtContact2,
            txtHomeNumber, txtLot, txtFloor, txtRoom, txtNote;
    private Spinner spDistrict, spWard, spStreet, spHouseType,
            spNameVilla, spPosition;
    private LinearLayout frmNameVilla;
    private LinearLayout frmPosition;
    private LinearLayout frmHomeNumber;
    private final static int TAG_HOME_STREET = 1;
    private final static int TAG_HOME_CONDO = 2;
    private final static int TAG_HOME_NO_ADDRESS = 3;

    public static FragmentCreatePotentialObjDetail newInstance(int sectionNumber, String sectionTitle) {
        FragmentCreatePotentialObjDetail fragment = new FragmentCreatePotentialObjDetail();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentCreatePotentialObjDetail() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_potential_obj_detail, container, false);
        Common.setupUI(getActivity(), view);
        activity = (CreatePotentialObjActivity) getActivity();
        try {
            activity.enableSlidingMenu(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        frmNameVilla = (LinearLayout) view.findViewById(R.id.frm_name_villa);
        frmPosition = (LinearLayout) view.findViewById(R.id.frm_position);
        frmHomeNumber = (LinearLayout) view.findViewById(R.id.frm_home_number);
        txtFullName = (EditText) view.findViewById(R.id.txt_full_name);
        txtPassport = (EditText) view.findViewById(R.id.txt_passport);
        txtTaxID = (EditText) view.findViewById(R.id.txt_tax_id);
        txtEmail = (EditText) view.findViewById(R.id.txt_email);
        txtFax = (EditText) view.findViewById(R.id.txt_fax);
        txtPhone1 = (EditText) view.findViewById(R.id.txt_phone_1);
        txtPhone2 = (EditText) view.findViewById(R.id.txt_phone_2);
        txtContact1 = (EditText) view.findViewById(R.id.txt_contact_1);
        txtContact2 = (EditText) view.findViewById(R.id.txt_contact_2);
        txtHomeNumber = (EditText) view.findViewById(R.id.txt_home_number);
        txtLot = (EditText) view.findViewById(R.id.txt_lot);
        txtFloor = (EditText) view.findViewById(R.id.txt_floor);
        txtRoom = (EditText) view.findViewById(R.id.txt_room);
        txtNote = (EditText) view.findViewById(R.id.txt_note);
        spDistrict = (Spinner) view.findViewById(R.id.sp_district);
        spDistrict.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyValuePairModel item = (KeyValuePairModel) spDistrict.getItemAtPosition(position);
                if (spDistrict.getSelectedItemPosition() > 0) {
                    initWard(item.getsID());
                } else {
                    if (spWard.getAdapter() != null && spWard.getAdapter().getCount() > 0)
                        spWard.setSelection(0, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spWard = (Spinner) view.findViewById(R.id.sp_ward);
        spWard.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyValuePairModel district = (KeyValuePairModel) spDistrict.getSelectedItem();
                KeyValuePairModel ward = (KeyValuePairModel) spWard.getItemAtPosition(position);
                KeyValuePairModel houseType = (KeyValuePairModel) spHouseType.getSelectedItem();
                if (ward != null) {
                    initStreet(district.getsID(), ward.getsID());
                    if (houseType != null && houseType.getID() == TAG_HOME_CONDO)
                        initNameVilla(district.getsID(), ward.getsID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spStreet = (Spinner) view.findViewById(R.id.sp_street);
        spNameVilla = (Spinner) view.findViewById(R.id.sp_name_villa);
        spPosition = (Spinner) view.findViewById(R.id.sp_position);
        spHouseType = (Spinner) view.findViewById(R.id.sp_house_type);
        spHouseType.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyValuePairModel item = (KeyValuePairModel) spHouseType.getItemAtPosition(position);
                if (item != null) {
                    switch (item.getID()) {
                        case TAG_HOME_STREET:
                            frmHomeNumber.setVisibility(View.VISIBLE);
                            frmNameVilla.setVisibility(View.GONE);
                            frmPosition.setVisibility(View.GONE);
                            break;
                        case TAG_HOME_NO_ADDRESS:
                            frmHomeNumber.setVisibility(View.VISIBLE);
                            frmNameVilla.setVisibility(View.GONE);
                            frmPosition.setVisibility(View.VISIBLE);
                            break;
                        case TAG_HOME_CONDO:
                            frmHomeNumber.setVisibility(View.GONE);
                            frmNameVilla.setVisibility(View.VISIBLE);
                            frmPosition.setVisibility(View.GONE);
                            break;
                        default:
                            break;
                    }
                    if (spDistrict.getAdapter() != null && spWard.getAdapter() != null) {
                        KeyValuePairModel district = (KeyValuePairModel) spDistrict.getSelectedItem();
                        KeyValuePairModel ward = (KeyValuePairModel) spWard.getSelectedItem();
                        if (item.getID() == TAG_HOME_CONDO) {
                            initNameVilla(district.getsID(), ward.getsID());
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Button btnUpdate = (Button) view.findViewById(R.id.btn_update);
        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkForUpdate()) {
                    AlertDialog.Builder builder;
                    Dialog dialog;
                    builder = new AlertDialog.Builder(activity);
                    builder.setMessage(getResources().getString(R.string.msg_confirm_update))
                            .setCancelable(false)
                            .setPositiveButton("Có",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            update();
                                        }
                                    })
                            .setNegativeButton("Không",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    dialog = builder.create();
                    dialog.show();
                }
            }
        });

        initCloseErrorEditText();
        loadData();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void loadData() {
        if (activity.getCurrentPotentialObj() != null) {
            txtFullName.setText(activity.getCurrentPotentialObj().getFullName());
            txtPassport.setText(activity.getCurrentPotentialObj().getPassport());
            txtTaxID.setText(activity.getCurrentPotentialObj().getTaxID());
            txtEmail.setText(activity.getCurrentPotentialObj().getEmail());
            txtFax.setText(activity.getCurrentPotentialObj().getFax());
            txtPhone1.setText(activity.getCurrentPotentialObj().getPhone1());
            if (activity.getCurrentPotentialObj().getFullName().length() > 0) {
                txtPhone1.setEnabled(false);
            }
            txtPhone2.setText(activity.getCurrentPotentialObj().getPhone2());
            txtContact1.setText(activity.getCurrentPotentialObj().getContact1());
            txtContact2.setText(activity.getCurrentPotentialObj().getContact2());
            txtHomeNumber.setText(activity.getCurrentPotentialObj().getBillTo_Number());
            txtLot.setText(activity.getCurrentPotentialObj().getLot());
            txtFloor.setText(activity.getCurrentPotentialObj().getFloor());
            txtRoom.setText(activity.getCurrentPotentialObj().getRoom());
            txtNote.setText(activity.getCurrentPotentialObj().getNote());
        }

        initHouseType();
        initDistrict();
        initPosition();
    }

    private void initHouseType() {
        if (spHouseType.getAdapter() == null) {
            ArrayList<KeyValuePairModel> lstHouseTypes = new ArrayList<>();
            lstHouseTypes.add(new KeyValuePairModel(TAG_HOME_STREET, "Nhà phố"));
            lstHouseTypes.add(new KeyValuePairModel(TAG_HOME_CONDO, "C.Cư, C.xá, Villa, Chợ, Thương Xá"));
            lstHouseTypes.add(new KeyValuePairModel(TAG_HOME_NO_ADDRESS, "Nhà không địa chỉ"));
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(activity,
                    R.layout.my_spinner_style, lstHouseTypes, Gravity.RIGHT);
            spHouseType.setAdapter(adapter);
            if (activity.getCurrentPotentialObj() != null) {
                spHouseType.setSelection(Common.getIndex(spHouseType, activity.getCurrentPotentialObj().getTypeHouse()), true);
            }
        }
    }

    private void initPosition() {
        if (spPosition.getAdapter() == null) {
            ArrayList<KeyValuePairModel> lstHousePositions = new ArrayList<>();
            lstHousePositions.add(new KeyValuePairModel(1, "Kế bên phải"));
            lstHousePositions.add(new KeyValuePairModel(2, "Kế bên trái"));
            lstHousePositions.add(new KeyValuePairModel(3, "Đối diện"));
            lstHousePositions.add(new KeyValuePairModel(4, "Phía sau"));
            lstHousePositions.add(new KeyValuePairModel(5, "Cách"));

            KeyValuePairAdapter adapter = new KeyValuePairAdapter(activity,
                    R.layout.my_spinner_style, lstHousePositions, Gravity.LEFT);
            spPosition.setAdapter(adapter);
            if (activity.getCurrentPotentialObj() != null) {
                spPosition.setSelection(Common.getIndex(spPosition, activity.getCurrentPotentialObj().getPosition()), true);
            }
        }
    }

    private void initDistrict() {
        if (spDistrict.getAdapter() == null) {
            String id = "";
            if (activity.getCurrentPotentialObj() != null)
                id = activity.getCurrentPotentialObj().getBillTo_District();
            new GetDistrictList(activity, Constants.LOCATION_ID, spDistrict, id);
        }
    }

    private void initWard(String district) {
        String id = "";
        if (activity.getCurrentPotentialObj() != null)
            id = activity.getCurrentPotentialObj().getBillTo_Ward();
        new GetWardList(activity, district, spWard, id);
    }

    private void initStreet(String district, String ward) {
        String id = "";
        if (activity.getCurrentPotentialObj() != null)
            id = activity.getCurrentPotentialObj().getBillTo_Street();
        new GetStreetOrCondo(activity, district, ward, GetStreetOrCondo.street, spStreet, id);
    }

    private void initNameVilla(String district, String ward) {
        String id = "";
        if (activity.getCurrentPotentialObj() != null)
            id = activity.getCurrentPotentialObj().getNameVilla();
        new GetStreetOrCondo(activity, district, ward, GetStreetOrCondo.condo, spNameVilla, id);
    }

    private boolean checkForUpdate() {
        int sHouseTypeSelected = ((KeyValuePairModel) spHouseType
                .getSelectedItem()).getID();
        String sWardSelected = null, sStreetSelected = null, sApartmentSelected = null, sDistrictSelected = null;
        try {
            if (spWard.getAdapter() != null && spWard.getSelectedItem() != null) {// Tri check null
                sWardSelected = spWard.getSelectedItem() != null ? ((KeyValuePairModel) spWard.getSelectedItem()).getsID() : "";
            }
            if (spStreet.getAdapter() != null)
                if (spStreet.getSelectedItem() != null)
                    sStreetSelected = ((KeyValuePairModel) spStreet.getSelectedItem()).getsID();
            if (spNameVilla.getAdapter() != null)
                if (spNameVilla.getSelectedItem() != null)
                    sApartmentSelected = ((KeyValuePairModel) spNameVilla.getSelectedItem()).getsID();
            if (spDistrict.getAdapter() != null)
                if (spDistrict.getSelectedItem() != null)
                    sDistrictSelected = ((KeyValuePairModel) spDistrict.getSelectedItem()).getsID();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(txtFullName.getText().toString().trim())) {
            Common.alertDialog("Vui lòng nhập tên KH", activity);
            txtFullName.setFocusable(true);
            txtFullName.requestFocus();
            return false;
        }

        if (!TextUtils.isEmpty(txtEmail.getText().toString()) &&
                !Common.checkMailValid(txtEmail.getText().toString())) {
            Common.alertDialog("Email không đúng định dạng", activity);
            txtEmail.setFocusable(true);
            txtEmail.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(txtPhone1.getText().toString().trim()) ||
                !TextUtils.isEmpty(txtPhone1.getText().toString().trim()) &&
                        !Common.checkPhoneValid(txtPhone1.getText().toString())) {
            Common.alertDialog("Chưa nhập SĐT hoặc SĐT không đúng định dạng", activity);
            txtPhone1.setFocusable(true);
            txtPhone1.requestFocus();
            return false;
        }

        if(TextUtils.isEmpty(txtContact1.getText().toString().trim())){
            Common.alertDialog("Vui lòng nhập tên liên lạc", activity);
            txtContact1.setFocusable(true);
            txtContact1.requestFocus();
            return false;
        }
        if (sDistrictSelected == null || sDistrictSelected.equals("-1")) {
            Common.alertDialog("Vui lòng chọn quận huyện", activity);
            spDistrict.requestFocus();
            return false;
        }

        if (sWardSelected == null || sWardSelected.isEmpty()) {
            Common.alertDialog("Vui lòng chọn phường xã", activity);
            spWard.requestFocus();
            return false;
        }

        if ((sHouseTypeSelected == 1 || sHouseTypeSelected == 3)
                && (sStreetSelected == null || sStreetSelected.isEmpty())) {
            Common.alertDialog("Vui lòng chọn tên đường", activity);
            spStreet.requestFocus();
            return false;
        }
        if (sHouseTypeSelected == 1 || sHouseTypeSelected == 3) {
            if (Common.isEmpty(txtHomeNumber)) {
                Common.alertDialog("Vui lòng nhập vào số nhà", activity);
                txtHomeNumber.requestFocus();
                return false;
            }
        } else if (sHouseTypeSelected == 2) {
            if (sApartmentSelected == null || sApartmentSelected.isEmpty()) {
                Common.alertDialog("Vui lòng chọn chung cư", activity);
                txtHomeNumber.requestFocus();
                return false;
            }
        }
        return true;
    }

    private String getAddress(PotentialObjModel obj, int addressType) {
        try {
            if (obj != null) {
                String address = "";
                if (obj.getBillTo_District() != null && !obj.getBillTo_District().trim().equals("-1")) {
                    switch (addressType) {
                        case TAG_HOME_CONDO:
                            if (!obj.getLot().trim().equals(""))
                                address = "Lo " + obj.getLot() + ", ";

                            if (!obj.getFloor().trim().equals(""))
                                address += "T." + obj.getFloor() + ", ";

                            if (!obj.getRoom().trim().equals(""))
                                address += "P." + obj.getRoom();

                            address += " " + obj.getNameVilla() + ",";
                            address += " " + obj.getBillTo_Street() + ",";
                            address += " " + obj.getBillTo_Ward() + ",";
                            address += " " + obj.getBillTo_District() + ",";
                            address += " " + obj.getBillTo_City();
                            break;
                        default:
                            address += obj.getBillTo_Number() + ",";
                            address += " " + obj.getBillTo_Street() + ",";
                            address += " " + obj.getBillTo_Ward() + ",";
                            address += " " + obj.getBillTo_District() + ",";
                            address += " " + obj.getBillTo_City();
                            break;
                    }
                }
                return address;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initCloseErrorEditText() {
        txtFullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtFullName.setError(null);
            }
        });

        txtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtEmail.setError(null);
            }
        });

        txtPhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtPhone1.setError(null);
            }
        });
    }

    public void initPotentialObj() {
        try {
            if (activity.getCurrentPotentialObj() == null) {
                activity.setCurrentPotentialObj(new PotentialObjModel());
            }
            activity.getCurrentPotentialObj().setFullName(txtFullName.getText().toString());
            activity.getCurrentPotentialObj().setPassport(txtPassport.getText().toString());
            activity.getCurrentPotentialObj().setTaxID(txtTaxID.getText().toString());
            activity.getCurrentPotentialObj().setEmail(txtEmail.getText().toString());
            activity.getCurrentPotentialObj().setFax(txtFax.getText().toString());
            activity.getCurrentPotentialObj().setPhone1(txtPhone1.getText().toString());
            activity.getCurrentPotentialObj().setPhone2(txtPhone2.getText().toString());
            activity.getCurrentPotentialObj().setContact1(txtContact1.getText().toString());
            activity.getCurrentPotentialObj().setContact2(txtContact2.getText().toString());
            activity.getCurrentPotentialObj().setBillTo_Number(txtHomeNumber.getText().toString());
            activity.getCurrentPotentialObj().setNote(
                    txtNote.getText().toString().trim().replace("\n", ""));
            activity.getCurrentPotentialObj().setLocationID(((MyApp) activity.getApplication()).getLocationID());
            activity.getCurrentPotentialObj().setBillTo_City(((MyApp) activity.getApplication()).getCityName());

            if (spDistrict.getAdapter() != null) {
                KeyValuePairModel item = (KeyValuePairModel) spDistrict.getSelectedItem();
                activity.getCurrentPotentialObj().setBillTo_District(item.getsID());
            }
            if (spWard.getAdapter() != null) {
                KeyValuePairModel item = (KeyValuePairModel) spWard.getSelectedItem();
                activity.getCurrentPotentialObj().setBillTo_Ward(item.getsID());
            }
            if (spStreet.getAdapter() != null) {
                KeyValuePairModel item = (KeyValuePairModel) spStreet.getSelectedItem();
                activity.getCurrentPotentialObj().setBillTo_Street(item.getsID());
            }

            if (spHouseType.getAdapter() != null) {
                KeyValuePairModel houseType = (KeyValuePairModel) spHouseType.getSelectedItem();
                activity.getCurrentPotentialObj().setTypeHouse(houseType.getID());
                switch (houseType.getID()) {
                    case TAG_HOME_STREET:
                        activity.getCurrentPotentialObj().setLot("");
                        activity.getCurrentPotentialObj().setFloor("");
                        activity.getCurrentPotentialObj().setRoom("");
                        activity.getCurrentPotentialObj().setNameVilla("");
                        activity.getCurrentPotentialObj().setPosition(0);
                        activity.getCurrentPotentialObj().setAddress(
                                getAddress(activity.getCurrentPotentialObj(), houseType.getID()));
                        break;
                    case TAG_HOME_CONDO:
                        activity.getCurrentPotentialObj().setLot(txtLot.getText().toString());
                        activity.getCurrentPotentialObj().setFloor(txtFloor.getText().toString());
                        activity.getCurrentPotentialObj().setRoom(txtRoom.getText().toString());
                        if (spNameVilla.getAdapter() != null) {
                            KeyValuePairModel item = (KeyValuePairModel) spNameVilla.getSelectedItem();
                            activity.getCurrentPotentialObj().setNameVilla(item.getsID());
                        }
                        activity.getCurrentPotentialObj().setPosition(0);
                        activity.getCurrentPotentialObj().setAddress(
                                getAddress(activity.getCurrentPotentialObj(), houseType.getID()));
                        break;
                    case TAG_HOME_NO_ADDRESS:
                        activity.getCurrentPotentialObj().setLot("");
                        activity.getCurrentPotentialObj().setFloor("");
                        activity.getCurrentPotentialObj().setRoom("");
                        activity.getCurrentPotentialObj().setNameVilla("");
                        if (spPosition.getAdapter() != null) {
                            KeyValuePairModel item = (KeyValuePairModel) spPosition.getSelectedItem();
                            activity.getCurrentPotentialObj().setPosition(item.getID());
                        }
                        activity.getCurrentPotentialObj().setAddress(
                                getAddress(activity.getCurrentPotentialObj(), houseType.getID()));
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void update() {
        initPotentialObj();
        String UserName = ((MyApp) activity.getApplication()).getUserName();
        new UpdatePotentialObj(activity, UserName, activity.getCurrentPotentialObj(), true, activity.getSupport());
        // Cập nhật & tiến hành khảo sát
    }
}