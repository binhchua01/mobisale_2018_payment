package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PotentialObjDetailActivity;
import isc.fpt.fsale.activity.PotentialObjScheduleList;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Constants;

public class PotentialObjCEMAdapter extends BaseAdapter {
    private List<PotentialObjModel> mList;
    private Context mContext;

    public PotentialObjCEMAdapter(Context context, List<PotentialObjModel> lst) {
        this.mContext = context;
        this.mList = lst;
    }

    public void clearAll() {
        this.mList.clear();
        this.notifyDataSetChanged();
    }

    public void setListData(List<PotentialObjModel> lst) {
        this.mList.clear();
        this.mList.addAll(lst);
        this.notifyDataSetChanged();
    }

    public void addAll(List<PotentialObjModel> lst) {
        this.mList.addAll(lst);
        this.notifyDataSetChanged();
    }

    public List<PotentialObjModel> getListData() {
        return mList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (mList != null)
            return mList.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        if (mList != null)
            return mList.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final PotentialObjModel item = mList.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_potentail_obj_cem, null);
        }
        if (item != null) {
            TextView lblRowNumber = (TextView) convertView.findViewById(R.id.lbl_row_number);
            lblRowNumber.setText(String.valueOf(item.getRowNumber()));

            TextView lblFullName = (TextView) convertView.findViewById(R.id.lbl_full_name);
            lblFullName.setText(item.getFullName());
            TextView lblTotalSchedule = (TextView) convertView
                    .findViewById(R.id.lbl_total_schedule);
            if (item.getTotalSchedule() > 0) {
                lblTotalSchedule.setText(String.valueOf(item.getTotalSchedule()));
                lblTotalSchedule.setVisibility(View.VISIBLE);
            } else {
                lblTotalSchedule.setVisibility(View.GONE);
            }
            lblTotalSchedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String supporter = item.getSupporter() == null ? Constants.USERNAME : item.getSupporter();
                    Intent intent = new Intent(mContext,
                            PotentialObjScheduleList.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(PotentialObjDetailActivity.TAG_POTENTIAL_OBJECT, item);
                    intent.putExtra(PotentialObjDetailActivity.TAG_SUPPORTER, supporter);
                    mContext.startActivity(intent);
                }
            });
            TextView lblPhone1 = (TextView) convertView.findViewById(R.id.lbl_phone_1);
            lblPhone1.setText(item.getPhone1());
            LinearLayout frmLabelPhone1 = (LinearLayout) convertView.findViewById(R.id.frm_lbl_phone_1);
            if (item.getDisplayPhone() > 0) {
                frmLabelPhone1.setVisibility(View.VISIBLE);
            } else {
                frmLabelPhone1.setVisibility(View.GONE);
            }
            TextView lblEmail = (TextView) convertView.findViewById(R.id.lbl_email);
            lblEmail.setText(item.getEmail());

            TextView lblNote = (TextView) convertView.findViewById(R.id.lbl_note);
            lblNote.setText(item.getNote());

            TextView lblCreateDate = (TextView) convertView.findViewById(R.id.lbl_create_date);
            lblCreateDate.setText(item.getCreateDate());

            TextView lblAddress = (TextView) convertView.findViewById(R.id.lbl_address);
            lblAddress.setText(item.getAddress());
            TextView lblRegcode = (TextView) convertView.findViewById(R.id.lbl_reg_code);
            lblRegcode.setText(item.getRegCode());

            TextView lblAcceptStatus = (TextView) convertView.findViewById(R.id.lbl_accept_status);
            if (item.getCodeStatus() == 0)
                lblAcceptStatus.setText("Chưa nhận");
            else {
                lblAcceptStatus.setText("Đã nhận");
            }
            TextView lblTimeProcess = (TextView) convertView.findViewById(R.id.lbl_remainder);
            lblTimeProcess.setText(item.getRemainder());

        }
        return convertView;
    }

}
