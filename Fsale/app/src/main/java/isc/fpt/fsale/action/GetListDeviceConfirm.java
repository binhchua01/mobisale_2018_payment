package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.ConfirmDeviceFragment;
import isc.fpt.fsale.ui.fragment.UnConfirmDeviceFragment;
import isc.fpt.fsale.model.ListDeviceConfirm;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 18,April,2019
 */
public class GetListDeviceConfirm implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private UnConfirmDeviceFragment mUnConfirmDeviceFragment;
    private ConfirmDeviceFragment mConfirmDeviceFragment;

    public GetListDeviceConfirm(Context mContext, UnConfirmDeviceFragment mUnConfirmDeviceFragment,
                                String userName, int typeId, int status, int pageNumber) {
        this.mContext = mContext;
        this.mUnConfirmDeviceFragment = mUnConfirmDeviceFragment;
        String[] arrParamName = new String[]{"SaleName", "TypeID", "Status", "PageNumber"};
        String[] arrParamValue = new String[]{userName, String.valueOf(typeId),
                String.valueOf(status), String.valueOf(pageNumber)};
        String message = mContext.getResources().getString(R.string.msg_progress_get_all_device_confirm);
        String GET_LIST_DEVICE_CONFIRM = "GetListDeviceConfirm";
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_DEVICE_CONFIRM, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetListDeviceConfirm.this);
        service.execute();
    }

    public GetListDeviceConfirm(Context mContext, ConfirmDeviceFragment mConfirmDeviceFragment,
                                String userName, int typeId, int status, int pageNumber) {
        this.mContext = mContext;
        this.mConfirmDeviceFragment = mConfirmDeviceFragment;
        String[] arrParamName = new String[]{"SaleName", "TypeID", "Status", "PageNumber"};
        String[] arrParamValue = new String[]{userName, String.valueOf(typeId),
                String.valueOf(status), String.valueOf(pageNumber)};
        String message = mContext.getResources().getString(R.string.msg_progress_get_all_device_confirm);
        String GET_LIST_DEVICE_CONFIRM = "GetListDeviceConfirm";
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_DEVICE_CONFIRM, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetListDeviceConfirm.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<ListDeviceConfirm> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ListDeviceConfirm.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mUnConfirmDeviceFragment != null) {
                        mUnConfirmDeviceFragment.listDeviceConfirm(lst);
                    } else {
                        mConfirmDeviceFragment.listDeviceConfirm(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
