package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ListGiftBoxOTT {
    @SerializedName("ListOTT")
    @Expose
    private List<ListOTT> listOTT;
    @SerializedName("LocationID")
    @Expose
    private Integer locationID;
    @SerializedName("BranchCode")
    @Expose
    private Integer branchCode;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("CusTypeDetail")
    @Expose
    private String cusTypeDetail;

    public ListGiftBoxOTT(List<ListOTT> listOTT, Integer locationID, Integer branchCode, String userName, String cusTypeDetail) {
        this.listOTT = listOTT;
        this.locationID = locationID;
        this.branchCode = branchCode;
        this.userName = userName;
        this.cusTypeDetail = cusTypeDetail;
    }

    public List<ListOTT> getListOTT() {
        return listOTT;
    }

    public void setListOTT(List<ListOTT> listOTT) {
        this.listOTT = listOTT;
    }

    public Integer getLocationID() {
        return locationID;
    }

    public void setLocationID(Integer locationID) {
        this.locationID = locationID;
    }

    public Integer getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(Integer branchCode) {
        this.branchCode = branchCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCusTypeDetail() {
        return cusTypeDetail;
    }

    public void setCusTypeDetail(String cusTypeDetail) {
        this.cusTypeDetail = cusTypeDetail;
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            for(ListOTT item : getListOTT()){
                jsonArray.put(item.toJsonObject());
            }
            jsonObject.put("ListOTT", jsonArray);
            jsonObject.put("LocationID", getLocationID());
            jsonObject.put("BranchCode", getBranchCode());
            jsonObject.put("UserName", getUserName());
            jsonObject.put("CusTypeDetail", getCusTypeDetail());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
