package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ReceiveDeviceActivity;
import isc.fpt.fsale.model.DeviceTypeConfirm;
import isc.fpt.fsale.model.ListDeviceOTT;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.InventoryDeviceFragment;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetListDeviceOTT implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private ReceiveDeviceActivity activity;

    public GetListDeviceOTT(Context mContext, ReceiveDeviceActivity activity, String userName) {
        this.mContext = mContext;
        this.activity = activity;
        String[] arrParamName = new String[]{"UserName"};
        String[] arrParamValue = new String[]{userName};
        String message = mContext.getResources().getString(R.string.msg_progress_get_all_device_inventory);
        String GET_ALL_DEVICE_TYPE_INVENTORY = "GetListDeviceOTT";
        CallServiceTask service = new CallServiceTask(mContext, GET_ALL_DEVICE_TYPE_INVENTORY, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetListDeviceOTT.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<ListDeviceOTT> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ListDeviceOTT.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    activity.deviceOTT(lst);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
