package isc.fpt.fsale.services;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.WakefulBroadcastReceiver;

import java.util.Map;

import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 9/7/2017.
 */

public class AlarmReceiver extends WakefulBroadcastReceiver {
    private Map<Integer, Ringtone> listRingtone;
    private NotificationManager alarmNotificationManager;

    public void onReceive(final Context context, Intent intent) {
        listRingtone = ((MyApp) context.getApplicationContext()).getListRingtone();
        alarmNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        String action = intent.getAction();
        int potentialObjScheduleID = intent.getIntExtra("potentialObjScheduleID", 0);
//        if (action != null || listRingtone.containsKey(potentialObjScheduleID)) {
//            if (listRingtone.containsKey(potentialObjScheduleID)) {
//                alarmNotificationManager.cancel(potentialObjScheduleID);
//                Ringtone ring = listRingtone.get(potentialObjScheduleID);
//                if (ring.isPlaying())
//                    ring.stop();
//            }
//        } else {
            Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            Ringtone ringtone = RingtoneManager.getRingtone(context, alarmUri);
            ringtone.play();
            listRingtone.put(potentialObjScheduleID, ringtone);
            ComponentName comp = new ComponentName(context.getPackageName(), AlarmService.class.getName());
            startWakefulService(context, (intent.setComponent(comp)));
            setResultCode(Activity.RESULT_OK);
//        }
    }
}
