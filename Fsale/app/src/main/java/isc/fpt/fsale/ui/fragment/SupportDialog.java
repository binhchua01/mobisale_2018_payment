package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GCM_GetRegisteredID;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.database.MessageTable;
import isc.fpt.fsale.model.GCMUserModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.MessageModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
//màn hình HỖ TRỢ
public class SupportDialog extends DialogFragment {

    private Context mContext;
    private Spinner spCategory;
    private EditText txtContent, txtImageUrl, txtSendTo;
    private TextView lblSendTo;
    private Button btnSend, btnCheckName;
    private List<GCMUserModel> lstSendTo;
    private ProgressDialog pd;
    private ImageButton imgClose;
    private LinearLayout frmSendTo;
    private boolean isAdmin = false;

    public SupportDialog() {
    }

    @SuppressLint("ValidFragment")
    public SupportDialog(Context mContext) {
        // TODO Auto-generated constructor stub
        this.mContext = mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_support, container);
        try {
            Common.setupUI(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        frmSendTo = (LinearLayout) view.findViewById(R.id.frm_support_send_to);
        txtSendTo = (EditText) view.findViewById(R.id.txt_support_send_to);
        lblSendTo = (TextView) view.findViewById(R.id.lbl_support_send_to);
        lblSendTo.setText("");
        btnCheckName = (Button) view.findViewById(R.id.btn_support_check_name);
        btnCheckName.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getGCMRegIDList(txtSendTo.getText().toString());
            }
        });

        spCategory = (Spinner) view.findViewById(R.id.sp_support_category);
        txtContent = (EditText) view.findViewById(R.id.txt_support_content);
        txtImageUrl = (EditText) view.findViewById(R.id.txt_support_image_url);
        imgClose = (ImageButton) view.findViewById(R.id.btn_close);
        imgClose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getDialog().dismiss();
            }
        });
        btnSend = (Button) view.findViewById(R.id.btn_support_send);
        btnSend.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Common.hideSoftKeyboard(mContext);
                if (lstSendTo == null || lstSendTo.size() == 0)
                    Common.alertDialog("Chưa có người nhận!", mContext);
                else if (txtContent.getText().toString().trim().equals(""))
                    Common.alertDialog(
                            "Vui lòng nhập thông tin phản hồi của bạn!",
                            mContext);
                else
                    comfirmToUpdate();
            }
        });
        initCategory();
        return view;
    }

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void initCategory() {
        ArrayList<KeyValuePairModel> lstModem = new ArrayList<KeyValuePairModel>();
        lstModem.add(new KeyValuePairModel("Error", "Lỗi"));
        lstModem.add(new KeyValuePairModel("Update", "Nâng cấp chức năng"));
        lstModem.add(new KeyValuePairModel("Support", "Hỗ trợ"));
        lstModem.add(new KeyValuePairModel("Others", "Khác..."));
        // kiểm tra context không null
        if (mContext != null) {
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                    R.layout.my_spinner_style, lstModem, Gravity.LEFT);
            spCategory.setAdapter(adapter);
        }

    }

    private void getGCMRegIDList(String name) {
        new GCM_GetRegisteredID(mContext, this, 0, name);
    }

    public void loadData(List<GCMUserModel> lst) {
        if (lst != null && lst.size() > 0) {
            String sendTo = lblSendTo.getText().toString();
            sendTo += txtSendTo.getText() + ",";
            lblSendTo.setText(sendTo);
            if (isAdmin) {
                if (lstSendTo != null)
                    for (GCMUserModel item : lst)
                        lstSendTo.add(item);
                else
                    lstSendTo = lst;
            } else {
                lstSendTo = lst;
            }
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), mContext);
            lstSendTo = null;
        }

    }

    private void comfirmToUpdate() {
        AlertDialog.Builder builder = null;
        Dialog dialog = null;
        builder = new AlertDialog.Builder(mContext);
        builder.setTitle(
                mContext.getResources().getString(R.string.msg_confirm_update))
                .setMessage(getString(R.string.msg_confirm_send))
                .setCancelable(false)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sendNotification();
                    }
                })
                .setNegativeButton("Không",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        try {
            isAdmin = ((MyApp) mContext.getApplicationContext()).getIsAdmin();

            if (isAdmin)
                frmSendTo.setVisibility(View.VISIBLE);
            else {
                frmSendTo.setVisibility(View.GONE);
                getGCMRegIDList("HCM.GiauTQ");
            }
            // if(!isAdmin)
            // getGCMRegIDList("HCM.GiauTQ");

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    @SuppressLint("SimpleDateFormat")
    private void sendNotification() {
        pd = Common.showProgressBar(mContext, "Đang gửi tin nhắn...");
        new AsyncTask<Object, String, String>() {
            MessageModel message = new MessageModel();

            @Override
            protected void onPostExecute(String result) {
                JSONObject jsObj = null;
                // int ResultID = 0;
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>();
                try {
                    if (result != null && Common.jsonObjectValidate(result)) {
                        jsObj = new JSONObject(result);
                        jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                        resultObject = new WSObjectsModel<UpdResultModel>(jsObj,
                                UpdResultModel.class);
                        if (resultObject != null) {
                            if (resultObject.getErrorCode() == 0) {// OK not Error
                                if (resultObject.getListObject().size() > 0
                                        && resultObject.getListObject().get(0)
                                        .getResultID() > 0) {
                                    try {
                                        MessageTable table = new MessageTable(
                                                mContext);
                                        table.add(message);
                                        updateSuccess(resultObject.getListObject()
                                                .get(0).getResult());
                                    } catch (Exception e) {
//
                                        e.printStackTrace();
                                    }
                                }
                            } else {// ServiceType Error
                                Common.alertDialog(resultObject.getError(),
                                        mContext);
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
//
                    e.printStackTrace();
                }
                if (pd.isShowing())
                    pd.dismiss();
            }

            ;

            @Override
            protected String doInBackground(Object... params) {
                String msg = "";
                try {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat format1 = new SimpleDateFormat(
                            "dd-MM-yyyy HH:mm:ss");
                    String formatted = format1.format(c.getTime());
                    message.setMessage(txtContent.getText().toString());
                    message.setImage(txtImageUrl.getText().toString());
                    message.setCategory(((KeyValuePairModel) spCategory
                            .getSelectedItem()).getsID());
                    message.setDatetime(formatted);
                    message.setSendfrom(Constants.USERNAME);
                    message.setTo(lstSendTo.get(0).getUserName());
                    message.setSendFromRegID(Common.loadPreference(mContext,
                            Constants.SHARE_PRE_GCM,
                            Constants.SHARE_PRE_GCM_REG_ID));
                    msg = Services.sendMessage(lstSendTo,
                            message.toJSONObject());
                } catch (Exception ex) {

                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }
        }.execute(null, null, null);
    }

    private void updateSuccess(String message) {
        AlertDialog.Builder builder = null;
        builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Thông báo").setMessage(message).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getDialog().dismiss();
                    }
                }).create().show();
    }

}
