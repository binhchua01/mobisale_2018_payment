package isc.fpt.fsale.ui.fpt_camera.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Hau Le on 2019-01-02.
 */
public class CameraServiceItem {
    private int ServiceType;
    private int ServicePkg;
    private int ServiceQty;

    public CameraServiceItem() {
    }

    public CameraServiceItem(int serviceType, int servicePkg, int serviceQty) {
        ServiceType = serviceType;
        ServicePkg = servicePkg;
        ServiceQty = serviceQty;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }

    public int getServicePkg() {
        return ServicePkg;
    }

    public void setServicePkg(int servicePkg) {
        ServicePkg = servicePkg;
    }

    public int getServiceQty() {
        return ServiceQty;
    }

    public void setServiceQty(int serviceQty) {
        ServiceQty = serviceQty;
    }

    public JSONObject toJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("ServiceType", getServiceType());
            obj.put("ServicePkg", getServicePkg());
            obj.put("ServiceQty", getServiceQty());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }
}
