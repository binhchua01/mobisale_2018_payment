package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportSalaryActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportSalaryModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONObject;

import android.content.Context;

public class GetReportSalaryList implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	private final String TAG_METHOD_NAME = "ReportSalary";
	private final String TAG_LIST_SALARY = "ListObject";
	private final String TAG_ERROR_CODE = "ErrorCode";
	private final String TAG_ERROR = "Error";
	// Tuấn thêm code 28082017
	private final String TAG_OBJECT = "Object";
	private String version;
	//For test
	private ListReportSalaryActivity activity = null;
	
	public GetReportSalaryList(Context context, String SaleAccount, int Day, int Month, int Year, String Agent, String AgentName, int PageNumber){
		try {
			mContext = context;
			try {
				activity = (ListReportSalaryActivity)mContext;
			} catch (Exception e) {

				activity = null;
			}
			version ="1";
			String[] paramNames = new String[]{"UserName", "Day", "Month", "Year", "Agent", "AgentName", "PageNumber","Version"};
			String[] paramValues = new String[]{SaleAccount, String.valueOf(Day), String.valueOf(Month), String.valueOf(Year), Agent, AgentName, String.valueOf(PageNumber),version};
			String message = "Đang lấy dữ liệu...";
			CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportSalaryList.this);
			service.execute();
		} catch (Exception e) {

		}		
	}
	
	/*================= Reponse Example =================
	{
		ResponseResult: {
		Error: null
		ErrorCode: 0
		ListSalary: [1]
			0:  {}
	}	 
	=====================================================*/
	// xử lý show list danh sách lương cũ
//	public void handleUpdateRegistration(String result){
//		ArrayList<ReportSalaryModel> lstSalary = new ArrayList<ReportSalaryModel>();
//		if(result != null && Common.jsonObjectValidate(result)){
//		JSONObject jsObj = getJsonObject(result);
//		if(jsObj != null && jsObj.has(Constants.RESPONSE_RESULT)){
//			try{
//				jsObj = jsObj.toJSONObject(Constants.RESPONSE_RESULT);	//Get sub object json
//				if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
//					JSONArray jsArr = jsObj.getJSONArray(TAG_LIST_SALARY);
//					for(int i=0; i< jsArr.length(); i++){
//						lstSalary.add(ReportSalaryModel.Parse(jsArr.toJSONObject(i)));
//					}
//					if(activity != null)
//						activity.LoadData(lstSalary);
//				}else{
//					Common.alertDialog("Lỗi WS:" + jsObj.getString(TAG_ERROR), mContext);
//				}
//			}catch (Exception e) {
//
//				e.printStackTrace();
//				Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + "-" + TAG_METHOD_NAME, mContext);
//				//setSpinnerEmpty();
//			}
//
//		}
//		}
//
//	}
	// xử lý show chi tiết lương với html
	public void handleUpdateRegistration(String result){
		ArrayList<ReportSalaryModel> lstSalary = new ArrayList<ReportSalaryModel>();
		if(result != null && Common.jsonObjectValidate(result)){
			JSONObject jsObj = getJsonObject(result);
			if(jsObj != null && jsObj.has(Constants.RESPONSE_RESULT)){
				try{
					jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);	//Get sub object json
					if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
						JSONObject jsObjDetail = jsObj.getJSONObject(TAG_OBJECT);
							lstSalary.add(ReportSalaryModel.Parse(jsObjDetail));
						if(activity != null)
							activity.LoadData(lstSalary);
					}else{
						Common.alertDialog("Lỗi WS:" + jsObj.getString(TAG_ERROR), mContext);
					}
				}catch (Exception e) {

					e.printStackTrace();
					Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + "-" + TAG_METHOD_NAME, mContext);
					//setSpinnerEmpty();
				}

			}
		}

	}
	private JSONObject getJsonObject(String result){
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);		
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception

		}
		return null;
	}
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);			
	}
		

}
