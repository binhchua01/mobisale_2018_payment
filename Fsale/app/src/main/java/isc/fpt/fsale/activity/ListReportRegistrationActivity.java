package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetReportRegistrationAction;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ReportRegistrationAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportRegistrationModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

public class ListReportRegistrationActivity extends BaseActivity {

    private Spinner spDay, spMonth, spYear, spAgent;
    private ListView lvResult;
    private LinearLayout frmFind;
    private ImageView imgNavigation;
    private Spinner spPage;
    private EditText txtAgentName;

    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
    private int Day = 0, Month = 0, Year = 0, Agent = 0;
    private String AgentName;


    public ListReportRegistrationActivity() {
        super(R.string.title_ReportRegistration);
    }

    @Override
    public void startActivity(Intent intent) {
        // TODO Auto-generated method stub
        //Finish this activity after start another activity
        finish();
        super.startActivity(intent);
    }

    @SuppressLint("InflateParams")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_report_registration_activity));
        setContentView(R.layout.list_reportregistration);

        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        spDay = (Spinner) findViewById(R.id.sp_day);
        spMonth = (Spinner) findViewById(R.id.sp_month);
        spYear = (Spinner) findViewById(R.id.sp_year);
        lvResult = (ListView) findViewById(R.id.lv_report);
        txtAgentName = (EditText) findViewById(R.id.txt_agent_name);

        final Button btnFind = (Button) findViewById(R.id.btn_find);
        imgNavigation = (ImageView) findViewById(R.id.img_navigation_drop_down);
        frmFind = (LinearLayout) findViewById(R.id.frm_find);
        btnFind.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (FLAG_FIRST_LOAD > 0)
                    FLAG_FIRST_LOAD = 0;
                getReport(1);
            }
        });

        imgNavigation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dropDownNavigation();
            }
        });
        spYear.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                int month = 0, year = 0;
                try {
                    year = item.getID();
                    month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //common.
                Common.initDaySpinner(ListReportRegistrationActivity.this, spDay, month, year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spMonth.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                int month = 0, year = 0;
                try {
                    month = item.getID();
                    year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Common.initDaySpinner(ListReportRegistrationActivity.this, spDay, month, year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getReport(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spAgent = (Spinner) findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() > 0) {
                    txtAgentName.setEnabled(true);
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        Common.initYearSpinner(this, spYear);
        Common.initMonthSpinner(this, spMonth);
        intAgentSpinner();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
	/*@Override
  	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {		
  		int itemId = item.getItemId();
		if (itemId == android.R.id.home) {
			toggle(SlidingMenu.LEFT);
			return true;
		} else if (itemId == R.id.action_right) {
			toggle(SlidingMenu.RIGHT);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
  	}
	

			
	
	@Override
	public void onItemClick(AdapterView<?>  parentView, View convertView, int position, long id) 
	{
		
	}
	
	
	 
	 //=========================================== Search Module =========================================
	 public void initDaySpinner(int month, int year)
	 {
		KeyValuePairAdapter adapterMonth;
	 	// Lấy tháng và năm hiện tại
		Calendar c = Calendar.getInstance();
		c.set(year, month-1, 1);
		int dayOfMonths =  c.getActualMaximum(Calendar.DAY_OF_MONTH);
		//Thang Search
	 	ArrayList<KeyValuePairModel> ListSpMonth = new ArrayList<KeyValuePairModel>();
	 	ListSpMonth.add( new KeyValuePairModel(0, "Tất cả"));
	 	for(int i=1; i<=dayOfMonths; i++)
	 	{	 		
	 		ListSpMonth.add(new KeyValuePairModel(i, String.valueOf(i)));
	 	}
	 	adapterMonth = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpMonth);
		spDay.setAdapter(adapterMonth);		
	 }
	 public void initMonthSpinner()
	 {
		KeyValuePairAdapter adapterMonth;
	 	// Lấy tháng và năm hiện tại
		Calendar c = Calendar.getInstance();		
		int month = c.get(Calendar.MONTH);		
		//Thang Search
	 	ArrayList<KeyValuePairModel> ListSpMonth = new ArrayList<KeyValuePairModel>();
	 	for(int i=1;i<13;i++)
	 	{	 		
	 		ListSpMonth.add(new KeyValuePairModel(i, String.valueOf(i)));
	 	}
	 	adapterMonth = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpMonth);
		spMonth.setAdapter(adapterMonth);		
		// Set tháng là tháng hiện tại
		spMonth.setSelection(Common.getIndex(spMonth, month+1));
		
		
		
		
	 }
	 private void initYearSpinner(){
		 
		Calendar c = Calendar.getInstance();	
		int year = c.get(Calendar.YEAR);
		ArrayList<KeyValuePairModel> ListSpYear = new ArrayList<KeyValuePairModel>();
		int iYear = getCurrentYear(getCurrentDate());
		int jYear=iYear-1;
		ListSpYear.add(new KeyValuePairModel(iYear,String.valueOf(iYear)));
		ListSpYear.add(new KeyValuePairModel(jYear,String.valueOf(jYear)));
		KeyValuePairAdapter adapterYear = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpYear);
		spYear.setAdapter(adapterYear);	
		
		// Set năm là năm hiện tại
		spYear.setSelection(Common.getIndex(spYear, year));
	 }
	 
	 private void intAgentSpinner(){
		//Loai Tim kiem
		ArrayList<KeyValuePairModel> ListSpAgent= new ArrayList<KeyValuePairModel>();
		ListSpAgent.add(new KeyValuePairModel(0,"Tất cả"));
		ListSpAgent.add(new KeyValuePairModel(1,"Nhân viên"));
		KeyValuePairAdapter adapterAgent = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpAgent);
		spAgent.setAdapter(adapterAgent);					
				
	 }
	 @SuppressLint("SimpleDateFormat")
	private String getCurrentDate(){
		String currentDate="";
		Calendar c= Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		currentDate =  df.format(c.getTime());
		return currentDate;
	}
	 
	private int getCurrentYear(String sCurrentDate){
		return Integer.parseInt((String) sCurrentDate.subSequence(6, 10));
	}	
	*/
    //=============================================
	
	/*private void getData(){
		
		String agentName = txtAgentName.getText().toString();
		if(agentName.equals(""))
			agentName= "0";
		int agent = ((KeyValuePairModel)spAgent.getSelectedItem()).getID();
		int day = ((KeyValuePairModel)spDay.getSelectedItem()).getID();
		int month = ((KeyValuePairModel)spMonth.getSelectedItem()).getID();
		int year =  ((KeyValuePairModel)spYear.getSelectedItem()).getID();
		
		//GetReportRegistrationAction(Context context, String SaleAccount, int Month, int Year, String Agent, String AgentName, int PageNumber){
		new GetReportRegistrationAction(this ,Constants.USERNAME, day, month, year,String.valueOf(agent), agentName, mCurrentPage);
		
		
	}*/

    private void intAgentSpinner() {
        //Loai Tim kiem
        ArrayList<KeyValuePairModel> ListSpAgent = new ArrayList<KeyValuePairModel>();
        ListSpAgent.add(new KeyValuePairModel(0, "Tất cả"));
        ListSpAgent.add(new KeyValuePairModel(1, "Nhân viên"));
        KeyValuePairAdapter adapterAgent = new KeyValuePairAdapter(this, R.layout.my_spinner_style, ListSpAgent, Gravity.LEFT);
        spAgent.setAdapter(adapterAgent);

    }

    private void getReport(int PageNumber) {
        mPage = PageNumber;
        Day = ((KeyValuePairModel) spDay.getSelectedItem()).getID();
        Month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
        Year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();
        AgentName = txtAgentName.getText().toString().trim();
        Agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
        if (Agent > 0 && AgentName.equals("")) {
            Common.alertDialog(getString(R.string.msg_search_value_empty), this);
        } else {
            new GetReportRegistrationAction(this, Constants.USERNAME, Day, Month, Year, String.valueOf(Agent), AgentName, mPage);
        }
    }
	
	/*public void LoadData(ArrayList<ReportRegistrationModel> list){
		try {
			lstReport = list;
			if(lstReport != null && lstReport.size() > 0){
				ReportRegistrationAdapter abc = new ReportRegistrationAdapter(mContext, lstReport);
				lvObject.setAdapter(abc);
				dropDownNavigation();
				ReportRegistrationModel item1 = lstReport.get(0);		
				
				mTotalPage = item1.getTotalPage();	
				int totalPage = mTotalPage;
				
				txt_row_total.setText(String.valueOf(item1.getTotalRow()));
				if(SpPage.getAdapter() == null || FLAG_FIRST_LOAD == 0){
					ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
					for(int i=1; i <= totalPage; i++){
						lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));				
					}										
					KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPage, Color.WHITE);
					
					SpPage.setAdapter(pageAdapter);
				}
			}else{
				txt_row_total.setText("0");
				lvObject.setAdapter(new ReportRegistrationAdapter(mContext, new ArrayList<ReportRegistrationModel>()));
				Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}*/

    public void LoadData(ArrayList<ReportRegistrationModel> lst) {
        if (lst != null && lst.size() > 0) {
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                dropDownNavigation();
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
                //adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            ReportRegistrationAdapter adapter = new ReportRegistrationAdapter(this, lst);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
            lvResult.setAdapter(new ReportRegistrationAdapter(this, new ArrayList<ReportRegistrationModel>()));
        }

    }

    //TODO: dropDown Search frm
    private void dropDownNavigation() {
        if (frmFind.getVisibility() == View.VISIBLE) {
            frmFind.setVisibility(View.GONE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
        } else {
            frmFind.setVisibility(View.VISIBLE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
        }
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
