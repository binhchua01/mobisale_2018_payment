package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PotentialObjModel;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PotentialObjListAdapter  extends BaseAdapter{
	private List<PotentialObjModel> mList;	
	private Context mContext;
	
	public PotentialObjListAdapter(Context context, List<PotentialObjModel> lst){
		this.mContext = context;
		this.mList = lst;	
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		PotentialObjModel item = mList.get(position);		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_potentail_obj, null);
		}
		
		TextView lblRowNumber = (TextView) convertView.findViewById(R.id.lbl_row_number);
		lblRowNumber.setText(String.valueOf(item.getRowNumber()));
		
		TextView lblFullName = (TextView) convertView.findViewById(R.id.lbl_full_name);
		if(item.getFullName() != null)
			lblFullName.setText(item.getFullName());		
		
		TextView lblPhone1 = (TextView) convertView.findViewById(R.id.lbl_phone_1);		
		if(item.getPhone1() != null)
			lblPhone1.setText(item.getPhone1());
		
		TextView lblEmail = (TextView)convertView.findViewById(R.id.lbl_email);
		if(item.getEmail()!= null)
			lblEmail.setText(item.getEmail());
		
		TextView lblNote = (TextView)convertView.findViewById(R.id.lbl_note);
		if(item.getEmail()!= null)
			lblNote.setText(item.getNote());
		
		TextView lblCreateDate = (TextView)convertView.findViewById(R.id.lbl_create_date);
		if(item.getCreateDate()!= null)
			lblCreateDate.setText(item.getCreateDate());
		
		TextView lblAddress = (TextView) convertView.findViewById(R.id.lbl_address);
		if(item.getAddress() != null)
			lblAddress.setText(item.getAddress());
		
		return convertView;
	}

}
