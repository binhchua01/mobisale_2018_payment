package isc.fpt.fsale.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListReportSurveyManagerAction;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ReportSurveyManegerAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportSurveyManagerModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class ListReportSurveyManagerActivity extends BaseActivity {
    private ListView lvSBI;
    private Spinner spPage;
    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;

    private String UserName = "";
    private int Day = 0, Month = 0, Year = 0/*, PageNumber = 0*/;

    //
    public ListReportSurveyManagerActivity() {
        super(R.string.lbl_ReportSurvey_Manger);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_report_survey_manager_activity));
        setContentView(R.layout.activity_report_survey_detaill);
        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getData(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        lvSBI = (ListView) findViewById(R.id.lv_report_survey_detail);
        getDataFromIntent();
         /*try{
			Intent myIntent = getIntent();
			if (myIntent != null && myIntent.getExtras() != null)
			{				
				mDay = myIntent.getIntExtra("DAY", 0);
				mMonth = myIntent.getIntExtra("MONTH", 1);
				mYear = myIntent.getIntExtra("YEAR", 2015);	
				String saleName = myIntent.getStringExtra("SALE_NAME");
				txt_sale_name.setText(saleName);
				getData(saleName);
			}
			ReportSurveyManegerAdapter abc = new ReportSurveyManegerAdapter(mContext, lstReport);
			lvObject.setAdapter(abc);
		}
		catch(Exception e){}
		 if(lstReport != null && lstReport.size()>0)
			{
				txt_row_total.setText(String.valueOf(lstReport.get(0).getTotalRow()));
			}
			
		
		 */

        // super.addRight(new MenuRightReport());


    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        try {
            Intent intent = getIntent();
			/*
			 * intent.putExtra("USER_NAME", item.getSaleName());
							intent.putExtra("DAY", mDay);
							intent.putExtra("MONTH", mMonth);
							intent.putExtra("YEAR", mYear);
							intent.putExtra("AGENT", mAgent);
							intent.putExtra("AGENTNAME", mAgentName);
							intent.putExtra("REPORT_TYPE", subItem.getID());
			 * */
			/*UserName = intent.getStringExtra("USER_NAME");
			AgentName = intent.getStringExtra("AGENT_NAME");
			Day = intent.getIntExtra("DAY", 0);
			Month = intent.getIntExtra("MONTH", 0);
			Year = intent.getIntExtra("YEAR", 0);
			Agent = intent.getIntExtra("AGENT", 0);
			ReportType = intent.getIntExtra("REPORT_TYPE", 0);
			getData(1);*/

            Day = intent.getIntExtra("DAY", 0);
            Month = intent.getIntExtra("MONTH", 0);
            Year = intent.getIntExtra("YEAR", 0);
            UserName = intent.getStringExtra("SALE_NAME");
            getData(1);
        } catch (Exception e) {
            // TODO: handle exception

        }
    }


    //=========================================== Search Module =========================================


    //=============================================
    private void getData(int pageNumber) {
        new GetListReportSurveyManagerAction(this, UserName, Day, Month, Year, "", "", pageNumber);
    }


    public void LoadData(ArrayList<ReportSurveyManagerModel> list) {
		/*try {
			lstReport = list;
			if(lstReport != null && lstReport.size() > 0){
				ReportSurveyManegerAdapter abc = new ReportSurveyManegerAdapter(mContext, lstReport);
				lvObject.setAdapter(abc);
				ReportSurveyManagerModel item1 = lstReport.get(0);		
				
				mTotalPage = item1.getTotalPage();	
				int totalPage = mTotalPage;
				
				txt_row_total.setText(String.valueOf(item1.getTotalRow()));
				if(SpPage.getAdapter() == null || FLAG_FIRST_LOAD == 0){
					ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
					for(int i=1; i <= totalPage; i++){
						lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));				
					}										
					KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPage, Color.WHITE);
					
					SpPage.setAdapter(pageAdapter);
				}
			}else{
				txt_row_total.setText("0");
				lvObject.setAdapter(null);
				Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
			}			
		} catch (Exception e) {
			// TODO: handle exception
		}*/
        try {
            if (list == null || list.size() <= 0) {
				/*Common.alertDialog(getString(R.string.msg_no_data), this);	
				lvSBI.setAdapter(null);*/
                new AlertDialog.Builder(this).setTitle("Thông Báo").setMessage(getString(R.string.msg_no_data))
                        .setPositiveButton(R.string.lbl_ok, new OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                finish();
                            }
                        })
                        .setCancelable(false).create().show();
            } else {
                int mTotalPage = Integer.valueOf(list.get(0).getTotalPage());
                if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                    ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                    for (int i = 1; i <= mTotalPage; i++) {
                        lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                    }
                    KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
                    //adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
                    spPage.setAdapter(pageAdapter);
                }
                ReportSurveyManegerAdapter adapter = new ReportSurveyManegerAdapter(this, list);
                lvSBI.setAdapter(adapter);
            }
        } catch (Exception e) {
            // TODO: handle exception

        }
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

}
