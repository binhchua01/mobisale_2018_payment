package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListActivityCustomerActivity;

import isc.fpt.fsale.model.ActivityModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;

// API lấy hoạt động khách hàng
public class GetCustomerCareActivityList implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    public final String TAG_METHOD_NAME = "GetActivityList";
    private String[] paramNames, paramValues;

    public GetCustomerCareActivityList(Context context, String UserName, String Contract, int PageNumber) {
        mContext = context;
        this.paramNames = new String[]{"UserName", "Contract", "PageNumber"};
        this.paramValues = new String[]{UserName, Contract, String.valueOf(PageNumber)};
        execute();
    }

    public void execute() {
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues,
                Services.JSON_POST, message, GetCustomerCareActivityList.this);
        service.execute();
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onTaskComplete(String result) {
        try {
            boolean isError = false;
            List<ActivityModel> lst = null;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<ActivityModel> resultObject = new WSObjectsModel<>(jsObj, ActivityModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                } else {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError)
                    loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(List<ActivityModel> lst) {
        if (mContext.getClass().getSimpleName().equals(ListActivityCustomerActivity.class.getSimpleName())) {
            ListActivityCustomerActivity activity = (ListActivityCustomerActivity) mContext;
            activity.loadData(lst);
        }
    }
}
