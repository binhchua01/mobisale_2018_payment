package isc.fpt.fsale.action;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PotentialObjScheduleList;
import isc.fpt.fsale.ui.fragment.PotentialScheduleCreateDialog;
import isc.fpt.fsale.model.PotentialSchedule;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.AlarmReceiver;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 10/10/2017.
 */

public class UpdatePotentialObjSchedule implements AsyncTaskCompleteListener<String> {
    private final String UPDATE_POTENTIAL_OBJ_SCHEDULE = "UpdatePotentialObjSchedule";
    private Context mContext;
    private String[] arrParamName, arrParamValue;
    private AlarmManager alarmManager;
    private PotentialObjScheduleList potentialObjScheduleList;
    private int selectedPostion;
    private PotentialSchedule potentialScheduleUpdate;
    private int pageChanged;
    private PotentialScheduleCreateDialog potentialScheduleCreateDialog;

    public UpdatePotentialObjSchedule(Context mContext, int potentialObjID, String supporter, String potentialObjScheduleID, String scheduleDescription, String scheduleDate, int selectedPostion, PotentialSchedule potentialSchedule, PotentialScheduleCreateDialog potentialScheduleCreateDialog) {
        this.mContext = mContext;
        this.potentialObjScheduleList = (PotentialObjScheduleList) mContext;
        this.potentialScheduleCreateDialog = potentialScheduleCreateDialog;
        arrParamName = new String[]{"PotentialObjID", "Supporter", "PotentialObjScheduleID", "ScheduleDescription", "ScheduleDate"};
        this.arrParamValue = new String[]{String.valueOf(potentialObjID), supporter, potentialObjScheduleID, scheduleDescription, scheduleDate};
        this.selectedPostion = selectedPostion;
        this.pageChanged = 1;
        this.potentialScheduleUpdate = potentialSchedule;
        String message = mContext.getResources().getString(R.string.msg_pd_update_potential_schedule);
        CallServiceTask service = new CallServiceTask(mContext, UPDATE_POTENTIAL_OBJ_SCHEDULE, arrParamName, arrParamValue, Services.JSON_POST, message, UpdatePotentialObjSchedule.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        try {
            ArrayList<PotentialSchedule> lst = null;
            boolean isError = false;
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<PotentialSchedule> resultObject = new WSObjectsModel<PotentialSchedule>(jsObj, PotentialSchedule.class);
                    if (resultObject != null) {
                        if (resultObject.getErrorCode() >= 0) {
                            lst = resultObject.getArrayListObject();
                            SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                            alarmManager = ((MyApp) mContext.getApplicationContext()).getAlarmManager();
                            List<PotentialSchedule> listPotentialSchedule = potentialObjScheduleList.getListPotentialSchedule();
                            Intent myIntent = new Intent(mContext, AlarmReceiver.class);
                            try {
                                Calendar calendar = Calendar.getInstance();
                                for (PotentialSchedule e : lst) {
                                    Date sDate = simpleDate.parse(e.getScheduleDate());
                                    calendar.setTime(sDate);
                                    if (selectedPostion != -1 && potentialScheduleUpdate != null) {
                                        listPotentialSchedule.set(selectedPostion, e);
                                        // 2 is update potential schedule to calendar
//                                        updatePotentialSheduleInCalendar(calendar, e, 2);
                                    } else {
                                        // 1 is create potential schedule to calendar
//                                        updatePotentialSheduleInCalendar(calendar, e, 1);
                                        potentialObjScheduleList.getListPotentialSchedule().add(e);
                                        potentialObjScheduleList.getLblTotalSchedule().setText(String.valueOf(potentialObjScheduleList.getListPotentialSchedule().size()));
                                    }
                                    potentialObjScheduleList.getAdapterPotentialSchedule().notifyDataSetChanged();
                                    myIntent.putExtra("potentialFullName", potentialObjScheduleList.getmPotential().getFullName());
                                    myIntent.putExtra("scheduleDescription", e.getScheduleDescription());
                                    myIntent.putExtra("potentialObjScheduleID", e.getPotentialObjScheduleID());
                                    PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, e.getPotentialObjScheduleID(), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                                    } else {
                                        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                                    }
                                }
                                if (listPotentialSchedule != null && potentialObjScheduleList.getListPotentialSchedule().size() == 1) {
                                    potentialObjScheduleList.setNumberPage(potentialObjScheduleList.getListPotentialSchedule());
                                    potentialObjScheduleList.setVisibilityPotentialScheduleList(true);
                                }
                                if (listPotentialSchedule != null && potentialObjScheduleList.getListPotentialSchedule().size() == 0) {
                                    potentialObjScheduleList.setNumberPage(potentialObjScheduleList.getListPotentialSchedule());
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
                            isError = true;
                            Common.alertDialog(resultObject.getError(), mContext);
                        }
                        if (!isError) {
                            try {
                                AlertDialog.Builder builder = null;
                                Dialog dialog = null;
                                builder = new AlertDialog.Builder(mContext);
                                builder.setMessage(resultObject.getError())
                                        .setCancelable(false)
                                        .setPositiveButton(
                                                mContext.getResources().getString(R.string.lbl_yes),
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int id) {
//                                                        Calendar date = Calendar.getInstance();
//                                                        String s = Common.getSimpleDateFormat(date, Constants.DATE_FORMAT_VN);
//                                                        potentialObjScheduleList.getData(s, s, 1);
//                                                        potentialObjScheduleList.setHideFrameSearch();
                                                        potentialScheduleCreateDialog.dismiss();
                                                    }
                                                });

                                dialog = builder.create();
                                dialog.show();
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            Log.i("CreatePotentialSchedule_onTaskComplete:", e.getMessage());
            Common.alertDialog(
                    mContext.getResources().getString(R.string.msg_error_data),
                    mContext);
        }
    }
//    // tạo hoặc cập nhật 1 lịch hẹn vào calendar
//    private void updatePotentialSheduleInCalendar(Calendar calendar, PotentialSchedule potentialSchedule, int state) {
//        ContentResolver cr = mContext.getContentResolver();
//        ContentValues values = new ContentValues();
//        values.put(CalendarContract.Events.DTSTART, calendar.getTimeInMillis());
//        values.put(CalendarContract.Events.DTEND, calendar.getTimeInMillis());
//        values.put(CalendarContract.Events.TITLE, "Lịch hẹn khách hàng tiềm năng: " + potentialObjScheduleList.getmPotential().getFullName());
//        values.put(CalendarContract.Events.DESCRIPTION, potentialSchedule.getScheduleDescription());
//        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
//        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
//            return;
//        }
//        if (state == 1) {
//            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
//            long idCalendarPotentialSchedule = Long.parseLong(uri.getLastPathSegment());
//
//        } else {
//            Uri updateUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, potentialObjScheduleList.getListPotentialSchedule().get(selectedPostion).getIdPotentialScheduleCalendar());
//            cr.update(updateUri, values, null, null);
//        }
//    }
}