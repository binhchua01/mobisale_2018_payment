package isc.fpt.fsale.model.upsell.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import isc.fpt.fsale.adapter.PromotionIPTVAdapter;

public class ObjUpgradeForCare implements Parcelable {

    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("AdvisoryUpgradeID")
    @Expose
    private Integer advisoryUpgradeID;
    @SerializedName("AgeOfPrePaid")
    @Expose
    private Integer ageOfPrePaid;
    @SerializedName("Combo")
    @Expose
    private String combo;
    @SerializedName("Contract")
    @Expose
    private String contract;
    @SerializedName("DueDate")
    @Expose
    private String dueDate;
    @SerializedName("FeeFromLocalType")
    @Expose
    private Integer feeFromLocalType;
    @SerializedName("FromLocalType")
    @Expose
    private Integer fromLocalType;
    @SerializedName("FromLocalTypeName")
    @Expose
    private String fromLocalTypeName;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("MoneyUpgrade")
    @Expose
    private Integer moneyUpgrade;
    @SerializedName("Note")
    @Expose
    private String note;
    @SerializedName("NumTrans")
    @Expose
    private Integer numTrans;
    @SerializedName("ObjID")
    @Expose
    private Integer objID;
    @SerializedName("ObjStatus")
    @Expose
    private String objStatus;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("PotentialRating")
    @Expose
    private Integer potentialRating;
    @SerializedName("PrePaidType")
    @Expose
    private Integer prePaidType;
    @SerializedName("ProgramUpgradeID")
    @Expose
    private Integer programUpgradeID;
    @SerializedName("ProgramUpgradeName")
    @Expose
    private String programUpgradeName;
    @SerializedName("PromotionName")
    @Expose
    private String promotionName;
    @SerializedName("Result")
    @Expose
    private Boolean result;
    @SerializedName("ServiceType")
    @Expose
    private Integer serviceType;
    @SerializedName("ToLocalType")
    @Expose
    private Integer toLocalType;
    @SerializedName("ToLocalTypeName")
    @Expose
    private String toLocalTypeName;
    @SerializedName("UpgradeStatus")
    @Expose
    private Integer upgradeStatus;

    protected ObjUpgradeForCare(Parcel in) {
        address = in.readString();
        if (in.readByte() == 0) {
            advisoryUpgradeID = null;
        } else {
            advisoryUpgradeID = in.readInt();
        }
        if (in.readByte() == 0) {
            ageOfPrePaid = null;
        } else {
            ageOfPrePaid = in.readInt();
        }
        combo = in.readString();
        contract = in.readString();
        dueDate = in.readString();
        if (in.readByte() == 0) {
            feeFromLocalType = null;
        } else {
            feeFromLocalType = in.readInt();
        }
        if (in.readByte() == 0) {
            fromLocalType = null;
        } else {
            fromLocalType = in.readInt();
        }
        fromLocalTypeName = in.readString();
        fullName = in.readString();
        message = in.readString();
        if (in.readByte() == 0) {
            moneyUpgrade = null;
        } else {
            moneyUpgrade = in.readInt();
        }
        note = in.readString();
        if (in.readByte() == 0) {
            numTrans = null;
        } else {
            numTrans = in.readInt();
        }
        if (in.readByte() == 0) {
            objID = null;
        } else {
            objID = in.readInt();
        }
        objStatus = in.readString();
        phone = in.readString();
        if (in.readByte() == 0) {
            potentialRating = null;
        } else {
            potentialRating = in.readInt();
        }
        if (in.readByte() == 0) {
            prePaidType = null;
        } else {
            prePaidType = in.readInt();
        }
        if (in.readByte() == 0) {
            programUpgradeID = null;
        } else {
            programUpgradeID = in.readInt();
        }
        programUpgradeName = in.readString();
        promotionName = in.readString();
        byte tmpResult = in.readByte();
        result = tmpResult == 0 ? null : tmpResult == 1;
        if (in.readByte() == 0) {
            serviceType = null;
        } else {
            serviceType = in.readInt();
        }
        if (in.readByte() == 0) {
            toLocalType = null;
        } else {
            toLocalType = in.readInt();
        }
        toLocalTypeName = in.readString();
        if (in.readByte() == 0) {
            upgradeStatus = null;
        } else {
            upgradeStatus = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        if (advisoryUpgradeID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(advisoryUpgradeID);
        }
        if (ageOfPrePaid == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(ageOfPrePaid);
        }
        dest.writeString(combo);
        dest.writeString(contract);
        dest.writeString(dueDate);
        if (feeFromLocalType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(feeFromLocalType);
        }
        if (fromLocalType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(fromLocalType);
        }
        dest.writeString(fromLocalTypeName);
        dest.writeString(fullName);
        dest.writeString(message);
        if (moneyUpgrade == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(moneyUpgrade);
        }
        dest.writeString(note);
        if (numTrans == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(numTrans);
        }
        if (objID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(objID);
        }
        dest.writeString(objStatus);
        dest.writeString(phone);
        if (potentialRating == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(potentialRating);
        }
        if (prePaidType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(prePaidType);
        }
        if (programUpgradeID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(programUpgradeID);
        }
        dest.writeString(programUpgradeName);
        dest.writeString(promotionName);
        dest.writeByte((byte) (result == null ? 0 : result ? 1 : 2));
        if (serviceType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(serviceType);
        }
        if (toLocalType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(toLocalType);
        }
        dest.writeString(toLocalTypeName);
        if (upgradeStatus == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(upgradeStatus);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObjUpgradeForCare> CREATOR = new Creator<ObjUpgradeForCare>() {
        @Override
        public ObjUpgradeForCare createFromParcel(Parcel in) {
            return new ObjUpgradeForCare(in);
        }

        @Override
        public ObjUpgradeForCare[] newArray(int size) {
            return new ObjUpgradeForCare[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAdvisoryUpgradeID() {
        return advisoryUpgradeID;
    }

    public void setAdvisoryUpgradeID(Integer advisoryUpgradeID) {
        this.advisoryUpgradeID = advisoryUpgradeID;
    }

    public Integer getAgeOfPrePaid() {
        return ageOfPrePaid;
    }

    public void setAgeOfPrePaid(Integer ageOfPrePaid) {
        this.ageOfPrePaid = ageOfPrePaid;
    }

    public String getCombo() {
        return combo;
    }

    public void setCombo(String combo) {
        this.combo = combo;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getFeeFromLocalType() {
        return feeFromLocalType;
    }

    public void setFeeFromLocalType(Integer feeFromLocalType) {
        this.feeFromLocalType = feeFromLocalType;
    }

    public Integer getFromLocalType() {
        return fromLocalType;
    }

    public void setFromLocalType(Integer fromLocalType) {
        this.fromLocalType = fromLocalType;
    }

    public String getFromLocalTypeName() {
        return fromLocalTypeName;
    }

    public void setFromLocalTypeName(String fromLocalTypeName) {
        this.fromLocalTypeName = fromLocalTypeName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getMoneyUpgrade() {
        return moneyUpgrade;
    }

    public void setMoneyUpgrade(Integer moneyUpgrade) {
        this.moneyUpgrade = moneyUpgrade;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getNumTrans() {
        return numTrans;
    }

    public void setNumTrans(Integer numTrans) {
        this.numTrans = numTrans;
    }

    public Integer getObjID() {
        return objID;
    }

    public void setObjID(Integer objID) {
        this.objID = objID;
    }

    public String getObjStatus() {
        return objStatus;
    }

    public void setObjStatus(String objStatus) {
        this.objStatus = objStatus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getPotentialRating() {
        return potentialRating;
    }

    public void setPotentialRating(Integer potentialRating) {
        this.potentialRating = potentialRating;
    }

    public Integer getPrePaidType() {
        return prePaidType;
    }

    public void setPrePaidType(Integer prePaidType) {
        this.prePaidType = prePaidType;
    }

    public Integer getProgramUpgradeID() {
        return programUpgradeID;
    }

    public void setProgramUpgradeID(Integer programUpgradeID) {
        this.programUpgradeID = programUpgradeID;
    }

    public String getProgramUpgradeName() {
        return programUpgradeName;
    }

    public void setProgramUpgradeName(String programUpgradeName) {
        this.programUpgradeName = programUpgradeName;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public Integer getServiceType() {
        return serviceType;
    }

    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }

    public Integer getToLocalType() {
        return toLocalType;
    }

    public void setToLocalType(Integer toLocalType) {
        this.toLocalType = toLocalType;
    }

    public String getToLocalTypeName() {
        return toLocalTypeName;
    }

    public void setToLocalTypeName(String toLocalTypeName) {
        this.toLocalTypeName = toLocalTypeName;
    }

    public Integer getUpgradeStatus() {
        return upgradeStatus;
    }

    public void setUpgradeStatus(Integer upgradeStatus) {
        this.upgradeStatus = upgradeStatus;
    }
}
