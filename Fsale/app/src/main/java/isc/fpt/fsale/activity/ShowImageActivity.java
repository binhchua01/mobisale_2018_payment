package isc.fpt.fsale.activity;


import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetImage;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;


public class ShowImageActivity extends Activity {
    private ImageView imgData;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_single_image);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_notification));
        imgData = (ImageView) findViewById(R.id.img_image);
        imgData.setOnClickListener(new OnClickListener() {

            @SuppressLint("InlinedApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    imgData.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    imgData.setSystemUiVisibility(View.STATUS_BAR_HIDDEN);
                else {

                }

            }
        });
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            String imageObj = intent.getStringExtra("PATH");
            if (imageObj != null) {
                //imgData.setImageBitmap(Common.StringToBitMap(imageObj));
                new GetImage(this, imageObj);
            }
        }
    }

    public void loadData(String image) {

        if (Common.StringToBitMap(image) != null)
            imgData.setImageBitmap(Common.StringToBitMap(image));
        else {
            Common.alertDialog("Không có dữ liệu!", this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Common.reportActivityStop(this, this);
    }


}
