package isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_package;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CameraPackage;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;

public class CameraPackageAdapter extends RecyclerView.Adapter<CameraPackageAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<CameraPackage> mList;
    private CameraDetail mCameraDetail;
    private OnItemClickListener mListener;

    CameraPackageAdapter(Context mContext, List<CameraPackage> mList,
                         CameraDetail mCameraDetail, OnItemClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCameraDetail = mCameraDetail;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_camera_package, parent, false);
        return new SimpleViewHolder(view);
    }

    private void whenItemClicked(CameraPackage mCameraPackage) {
        mCameraDetail.setPackID(mCameraPackage.getPackID());
        mCameraDetail.setPackName(mCameraPackage.getPackName());
        mCameraDetail.setMinCam(mCameraPackage.getMinCam());
        mCameraDetail.setMaxCam(mCameraPackage.getMaxCam());
        mCameraDetail.setCost(mCameraPackage.getCost());
        mCameraDetail.setQty(mCameraPackage.getMinCam());
        mCameraDetail.setServiceType(mCameraPackage.getServiceType());

        mListener.onItemClick(mCameraDetail);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvCameraPackageName;
        LinearLayout layoutWrap;

        SimpleViewHolder(View itemView) {
            super(itemView);
            tvCameraPackageName = (TextView) itemView.findViewById(R.id.tv_item_name);
            layoutWrap = (LinearLayout) itemView.findViewById(R.id.layout_wrap);
        }

        void bindView(final CameraPackage mCameraPackage) {
            tvCameraPackageName.setText(mCameraPackage.getPackName());
            if (mCameraPackage.isSelected()) {
                layoutWrap.setBackgroundResource(R.drawable.background_radius_selected);
            } else {
                layoutWrap.setBackgroundResource(R.drawable.background_radius);
            }

            layoutWrap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    whenItemClicked(mCameraPackage);
                }
            });
        }
    }

    public void notifyData(List<CameraPackage> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }
}