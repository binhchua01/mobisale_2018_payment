package isc.fpt.fsale.ui.maxytv.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.maxy.GetMaxyDevice;
import isc.fpt.fsale.action.maxy.GetPromotionByMaxyDevice;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyPromotionDeviceAdapter;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyTypeAdapter;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.BOX_FIRST;
import static isc.fpt.fsale.utils.Constants.COUNT_DEVICE;
import static isc.fpt.fsale.utils.Constants.LOCAL_TYPE;
import static isc.fpt.fsale.utils.Constants.MAXY_PROMOTION_ID;
import static isc.fpt.fsale.utils.Constants.MAXY_PROMOTION_TYPE;
import static isc.fpt.fsale.utils.Constants.NUM_BOX;
import static isc.fpt.fsale.utils.Constants.POSITION;
import static isc.fpt.fsale.utils.Constants.REGTYPE;
import static isc.fpt.fsale.utils.Constants.TAG_CONTRACT;

public class MaxyPromotionDeviceActivity extends BaseActivitySecond implements OnItemClickListener<MaxyBox> {
    private View loBack;
    private MaxyBox mMaxyBox;
    private List<MaxyBox> mListBoxSelected;
    private MaxyPromotionDeviceAdapter mAdapter;
    private int position;
    private String mClassName;
    private int RegType, box, maxyPromotionID, maxyPromotionType, localtype, numBox, count;
    private String contract;

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_device_list;
    }

    @Override
    protected void initView() {
        getDataIntent();
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.lbl_screen_name_maxy_promotion_device_list));
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_device_list);
        mAdapter = new MaxyPromotionDeviceAdapter(this, mListBoxSelected != null ? mListBoxSelected : new ArrayList<MaxyBox>(), this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(Constants.LIST_MAXY_DETAIL_SELECTED) != null) {
            mMaxyBox = bundle.getParcelable(Constants.LIST_MAXY_DETAIL_SELECTED);
            position = bundle.getInt(POSITION);
            RegType = bundle.getInt(REGTYPE);
            localtype = bundle.getInt(LOCAL_TYPE);
            box = bundle.getInt(BOX_FIRST);
            maxyPromotionID = bundle.getInt(MAXY_PROMOTION_ID);
            maxyPromotionType = bundle.getInt(MAXY_PROMOTION_TYPE);
            contract = bundle.getString(TAG_CONTRACT);
            numBox = bundle.getInt(NUM_BOX);
            count = bundle.getInt(COUNT_DEVICE);
        }
        if (bundle != null && bundle.getString(Constants.CLASS_NAME) != null) {
            mClassName = bundle.getString(Constants.CLASS_NAME);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mMaxyBox != null) {
            new GetPromotionByMaxyDevice(this, mMaxyBox.getBoxID(), RegType, box, localtype, maxyPromotionID, maxyPromotionType, contract);
        }
    }

    public void mLoadPromotionDevice(List<MaxyBox> mList) {
        this.mListBoxSelected = mList;
        mAdapter.notifyData(this.mListBoxSelected);
    }

    private void sendData(MaxyBox mMaxy) {
        mMaxy.setBoxID(this.mMaxyBox.getBoxID());
        mMaxy.setBoxName(this.mMaxyBox.getBoxName());
        mMaxy.setBoxCount(1);
        mMaxy.setBoxFirst(box);
        mMaxy.setBoxType(this.mMaxyBox.getBoxType());
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.OBJECT_MAXY_PROMOTION_DEVICE, mMaxy);
        returnIntent.putExtra(POSITION, position);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void onItemClick(MaxyBox mMaxy) {
        if (mMaxyBox.getPromotionID() == 0 && count == numBox) {
            Dialog dialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Bạn đã chọn đủ số lượng thiết bị!")
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.title_agree),
                            (dialog1, id) -> onBackPressed());
            dialog = builder.create();
            dialog.show();
        } else {
            sendData(mMaxy);
        }
    }
}
