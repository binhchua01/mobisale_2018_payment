package isc.fpt.fsale.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PaidType;

/**
 * Created by HCM.TUANTT14 on 4/27/2018.
 */

public class PaidTypeAdapter extends ArrayAdapter<PaidType> {
    private ArrayList<PaidType> lstObj;
    private Context mContext;
    private Typeface tf;

    public PaidTypeAdapter(Context context, int textViewResourceId, ArrayList<PaidType> lstObj) {
        super(context, textViewResourceId);
        this.mContext = context;
        this.lstObj = lstObj;
        this.tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    public ArrayList<PaidType> getList() {
        return lstObj;
    }

    public int getCount() {
        if (lstObj != null)
            return lstObj.size();
        return 0;
    }

    public PaidType getItem(int position) {
        if (lstObj != null)
            return lstObj.get(position);
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setText(lstObj.get(position).getValue());
        label.setTypeface(tf);
        int padding = (int) mContext.getResources().getDimension(R.dimen.padding_medium);
        label.setPadding(padding, padding, padding, padding);
        return label;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setTypeface(tf);
        label.setText(lstObj.get(position).getValue());
        label.setTextColor(Color.BLACK);
        return label;
    }
}
