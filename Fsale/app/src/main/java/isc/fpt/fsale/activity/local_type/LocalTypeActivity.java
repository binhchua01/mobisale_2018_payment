package isc.fpt.fsale.activity.local_type;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListLocalType;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.LocalType;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.utils.Constants;

public class LocalTypeActivity extends BaseActivitySecond
        implements OnItemClickListener<LocalType> {

    private List<CategoryServiceList> mListService;
    private List<LocalType> mList;
    private RelativeLayout rltBack;
    private LocalTypeAdapter mAdapter;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_local_type;
    }

    @Override
    protected void initView() {
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_local_type);
        mListService = new ArrayList<>();
        mList = new ArrayList<>();
        mAdapter = new LocalTypeAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);
        getDataIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //get list local type
        new GetListLocalType(this, mListService);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelableArrayList(Constants.SERVICE_TYPE_LIST) != null) {
            mListService = bundle.getParcelableArrayList(Constants.SERVICE_TYPE_LIST);
        }
    }

    public void loadLocalType(List<LocalType> mList) {
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(LocalType object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.LOCAL_TYPE, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
