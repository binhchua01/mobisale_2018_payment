package isc.fpt.fsale.ui.maxytv.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import isc.fpt.fsale.model.ServiceCode;

public class MaxyTotal implements Parcelable {

    private List<ServiceCode> MaxyService;
    private int Total;

    public List<ServiceCode> getMaxyService() {
        return MaxyService;
    }

    public void setMaxyService(List<ServiceCode> maxyService) {
        MaxyService = maxyService;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

    public static Creator<MaxyTotal> getCREATOR() {
        return CREATOR;
    }

    public MaxyTotal() {
    }

    public MaxyTotal(Parcel in) {
        MaxyService = in.createTypedArrayList(ServiceCode.CREATOR);
        Total = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(MaxyService);
        dest.writeInt(Total);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MaxyTotal> CREATOR = new Creator<MaxyTotal>() {
        @Override
        public MaxyTotal createFromParcel(Parcel in) {
            return new MaxyTotal(in);
        }

        @Override
        public MaxyTotal[] newArray(int size) {
            return new MaxyTotal[size];
        }
    };
}
