package isc.fpt.fsale.model.Banking;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankingHeaders implements Parcelable {

    @SerializedName("Key")
    @Expose
    private String key;
    @SerializedName("Value")
    @Expose
    private String value;

    public BankingHeaders(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public BankingHeaders() {
    }

    protected BankingHeaders(Parcel in) {
        key = in.readString();
        value = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BankingHeaders> CREATOR = new Creator<BankingHeaders>() {
        @Override
        public BankingHeaders createFromParcel(Parcel in) {
            return new BankingHeaders(in);
        }

        @Override
        public BankingHeaders[] newArray(int size) {
            return new BankingHeaders[size];
        }
    };

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
