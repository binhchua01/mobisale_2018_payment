package isc.fpt.fsale.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.content.Context;
import android.media.Ringtone;
import android.support.multidex.MultiDexApplication;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.slidingmenu.lib.SlidingMenu;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.HeaderRequestModel;
import isc.fpt.fsale.model.NotificationIDUserModel;
import isc.fpt.fsale.model.UserModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

public class MyApp extends MultiDexApplication {
    private static Context context;
    private static Activity activity;
    private boolean isAdmin = false;
    private boolean isLogIn = false;
    private boolean isLogOut = false;
    private int notification_id = 0, locationID = 0;
    private String userName = "";
    private String cityName = "";
    private SlidingMenu leftMenu;
    private UserModel user;
    private AlarmManager alarmManager;
    private Map<Integer, Ringtone> listRingtone;
    public final boolean HCM_VERSION = true;
    private List<NotificationIDUserModel> lstNotification;
    private ArrayList<CallServiceTask> syncThreads;

    public void setHeaderRequest(WSObjectsModel<HeaderRequestModel> header) {
        String json = "";
        if (header != null) {
            try {
                json = header.getJsonObject().toString();
                if (header.getErrorCode() == 0) {
                    Common.savePreference(this, Constants.SHARE_PRE_HEADER,
                            Constants.SHARE_PRE_HEADER_KEY_REQUEST_HEADER, json);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Common.savePreference(this, Constants.SHARE_PRE_HEADER,
                    Constants.SHARE_PRE_HEADER_KEY_REQUEST_HEADER, json);
        }
    }

    public WSObjectsModel<HeaderRequestModel> getHeaderRequest() {
        WSObjectsModel<HeaderRequestModel> header = null;
        try {
            String json = Common.loadPreference(this, Constants.SHARE_PRE_HEADER,
                    Constants.SHARE_PRE_HEADER_KEY_REQUEST_HEADER);
            if (!json.equals("")) {
                JSONObject jObj = new JSONObject(json);
                header = new WSObjectsModel<>(jObj, HeaderRequestModel.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return header;
    }

    public void addSyncThreads(CallServiceTask thread) {
        if (syncThreads == null)
            syncThreads = new ArrayList<>();
        syncThreads.add(thread);
    }

    public ArrayList<CallServiceTask> getSyncThreads() {
        return this.syncThreads;
    }

    public boolean getIsAdmin() {
        return this.isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public int getNotificationID() {
        return this.notification_id;
    }

    public void setNotificationID(int id) {
        this.notification_id = id;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<NotificationIDUserModel> getNotificationUserList() {
        return this.lstNotification;
    }

    public void setNotificationUserList(List<NotificationIDUserModel> lstNotification) {
        this.lstNotification = lstNotification;
    }

    public void addNotificationUserList(NotificationIDUserModel notificationUser) {
        if (lstNotification == null)
            lstNotification = new ArrayList<>();
        lstNotification.add(notificationUser);
    }


    public boolean getIsLogIn() {
        return this.isLogIn;
    }

    public void setIsLogIn(boolean isLogIn) {
        this.isLogIn = isLogIn;
    }

    public boolean getIsLogOut() {
        return this.isLogOut;
    }

    public void setIsLogOut(boolean isLogOut) {
        this.isLogOut = isLogOut;
    }

    public String getCityName() {
        return this.cityName;
    }

    public void setCityName(String CityName) {
        this.cityName = CityName;
    }

    public int getLocationID() {
        return this.locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }

    public SlidingMenu LeftMenu() {
        return this.leftMenu;
    }

    public void setLocationID(SlidingMenu menu) {
        this.leftMenu = menu;
    }


    public UserModel getUser() {
        return this.user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    @SuppressLint("UseSparseArrays")
    public void onCreate() {
        super.onCreate();
        MyApp.context = getApplicationContext();
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        listRingtone = new HashMap<>();
    }

    public AlarmManager getAlarmManager() {
        return alarmManager;
    }

    public void setAlarmManager(AlarmManager alarmManager) {
        this.alarmManager = alarmManager;
    }

    public Map<Integer, Ringtone> getListRingtone() {
        return listRingtone;
    }

    public void setListRingtone(Map<Integer, Ringtone> listRingtone) {
        this.listRingtone = listRingtone;
    }

    public synchronized static Context getAppContext() {
        return MyApp.context;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        activity = currentActivity;
    }

    public static Activity currentActivity() {
        return activity;
    }

    private static final String PROPERTY_ID = "UA-62233259-3";
    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     * <p>
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

    public MyApp() {
        super();
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(
                    R.xml.global_tracker)
                    : analytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
    }
}
