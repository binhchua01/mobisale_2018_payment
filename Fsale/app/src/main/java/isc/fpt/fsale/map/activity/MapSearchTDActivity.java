package isc.fpt.fsale.map.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListBookPortAction;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.action.GetGeocoder;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.map.utils.MapConstants;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;

public class MapSearchTDActivity extends FragmentActivity implements OnMapReadyCallback {
    private Spinner spTDType;
    private EditText txtTDName;
    private Button btnMapMode;
    private List<Marker> markerList = new ArrayList<>();
    private GoogleMap mMap;
//    private Location mapLocation;
    private Marker customerMarker;
    private WeakHashMap<Marker, RowBookPortModel> hashTDByName;
//    private final int zoomTD = 18;
//    private final int zoomCustomer = 11;
    private Context mContext;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_map_bookport_activity));
        setContentView(R.layout.activity_bookport_manual);
        this.mContext = this;
        spTDType = (Spinner) findViewById(R.id.sp_port_type);
        txtTDName = (EditText) findViewById(R.id.txt_tapdiem);
        txtTDName.setVisibility(View.GONE);
        ImageView imgTDInfo = (ImageView) findViewById(R.id.btn_find_list_port);
        imgTDInfo.setVisibility(View.GONE);
        Button btnGetTDList = (Button) findViewById(R.id.btn_show_list_port);
        btnGetTDList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getTDList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Button btnRecoveryTD = (Button) findViewById(R.id.btn_recover_registration);
        btnRecoveryTD.setVisibility(View.GONE);
        btnMapMode = (Button) findViewById(R.id.btn_map_mode);
        btnMapMode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMap != null) {
                    if (mMap.getMapType() == MAP_TYPE_SATELLITE) {
                        btnMapMode.setText(getResources().getString(
                                R.string.lbl_satellite));
                        MapConstants.IS_SATELLITE = false;
                        mMap.setMapType(MAP_TYPE_NORMAL);
                    } else {
                        btnMapMode.setText(getResources().getString(
                                R.string.lbl_map));
                        MapConstants.IS_SATELLITE = true;
                        mMap.setMapType(MAP_TYPE_SATELLITE);
                    }
                }
            }
        });
        setUpMapIfNeeded();
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        initSpTDType();
        getTDList();
    }

    private void setUpMapIfNeeded() {
        if (mMap != null)
            setUpMap();
        if (mMap == null) {
            SupportMapFragment mapFrag = (SupportMapFragment) this
                    .getSupportFragmentManager().findFragmentById(R.id.map);
            mapFrag.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                LinearLayout info = new LinearLayout(mContext);
                info.setOrientation(LinearLayout.VERTICAL);
                TextView title = new TextView(mContext);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());
                TextView snippet = new TextView(mContext);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());
                info.addView(title);
                info.addView(snippet);
                return info;
            }
        });
        if (mMap != null)
            setUpMap();
        else {
            Toast.makeText(this, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpMap() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, 2);
            }
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        // An nut dinh vi
        mMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {

            @Override
            public boolean onMyLocationButtonClick() {
                try {
                    getCurrentLocation();
                } catch (Exception e) {
                    Toast.makeText(MapSearchTDActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
        mMap.setOnMapLongClickListener(new OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng latlng) {
                if (customerMarker != null)
                    customerMarker.remove();
                customerMarker = drawMarkerOnMap(latlng,
                        getString(R.string.title_cuslocation),
                        R.drawable.icon_home, true, null, true);
            }
        });
        // Di chuyên marker
        mMap.setOnMarkerDragListener(new OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                customerMarker = marker;
            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }
        });

        mMap.setOnMarkerClickListener(new OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                try {
                    RowBookPortModel td = hashTDByName.get(marker);
                    if (td != null) {
                        txtTDName.setText(td.getTDName());
                        if (td.getTDName().contains("/"))
                            spTDType.setSelection(spTDType.getCount() - 1, true);

                        try {
                            if (!td.getAddress().trim().equals("")) {
                                String s = "Địa chỉ: " + td.getAddress();
                                s += td.getDistance().trim().equals("") ? "" : "\n Cách: " + td.getDistance() + "m";
                                Toast.makeText(MapSearchTDActivity.this, s, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                return false;
            }
        });
        if (mMap != null) {
            getCurrentLocation();
        }
    }

    private void getCurrentLocation() {
        if (mMap != null) {
            GPSTracker gps = new GPSTracker(this);
            Location location = gps.getLocation(true);
//            if ((location == null && mapLocation != null))
//                location = mapLocation;
//            if (Common.distanceTwoLocation(location, mapLocation) > 10)
//                location = mapLocation;
            if (location != null) {
                LatLng curLocation = new LatLng(location.getLatitude(), location.getLongitude());
                if (customerMarker != null) {
                    customerMarker.remove();
                }
                customerMarker = drawMarkerOnMap(
                        curLocation,
                        getString(R.string.title_cuslocation),
                        R.drawable.icon_home,
                        true,
                        null,
                        true
                );
            } else {
                new GetGeocoder(this, Constants.LOCATION_NAME).execute();
            }
        }
    }

    private void initSpTDType() {
        KeyValuePairModel itemADSL = new KeyValuePairModel(0, "ADSL");
        KeyValuePairModel itemFTTH = new KeyValuePairModel(1, "FTTH");
        KeyValuePairModel itemFTTHNew = new KeyValuePairModel(2, "FTTHNew");
        ArrayList<KeyValuePairModel> lstBookPortType = new ArrayList<>();
        lstBookPortType.add(itemADSL);
        lstBookPortType.add(itemFTTH);
        lstBookPortType.add(itemFTTHNew);
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstBookPortType, Gravity.LEFT);
        spTDType.setAdapter(adapter);
        spTDType.setSelection(2, true);
    }

    private void getTDList() {
        if (customerMarker != null) {
            String lat = String.valueOf(customerMarker.getPosition().latitude);
            String longitude = String
                    .valueOf(customerMarker.getPosition().longitude);
            String strContractLatLng = "(" + lat + "," + longitude + ")";
            if (!strContractLatLng.equals("") && !strContractLatLng.replace(",", "").equals("")) {
                int tdType = ((KeyValuePairModel) spTDType.getSelectedItem()).getID();
                new GetListBookPortAction(this, "", tdType, strContractLatLng);
            } else {
                Common.alertDialog(getString(R.string.msg_unable_get_location), this);
            }
        }
    }

    public void loadTDList(List<RowBookPortModel> lst) {
        if (markerList == null)
            markerList = new ArrayList<>();
        if (hashTDByName == null)
            hashTDByName = new WeakHashMap<>();
        clearMarkerHashMap();
        drawMarker(lst);
    }

    private void clearMarkerHashMap() {
        for (Marker item : markerList) {
            item.remove();
        }
        markerList.clear();
        hashTDByName.clear();
    }

    private void drawMarker(List<RowBookPortModel> lst) {
        if (lst != null && lst.size() > 0) {
            for (RowBookPortModel item : lst) {
                String title = item.getTDName();
                LatLng latlng = MapCommon.ConvertStrToLatLng(item
                        .getLatlngPort());
                if (latlng != null) {
                    String snippet = " Port trống: " + item.getPortFree() + "\n Technology: " + item.getTechnology();
                    int drawableID;
                    if (item.getPortFree() == 1) {
                        drawableID = R.drawable.ic_marker_red;
                    } else {
                        if (item.getPortFreeRatio() <= 0) {
                            drawableID = R.drawable.ic_marker_orange;
                        } else if (item.getPortFreeRatio() <= 0.25) {
                            drawableID = R.drawable.ic_marker_yellow;
                        } else {
                            drawableID = R.drawable.ic_marker_blue;
                        }
                    }

                    Marker marker = drawMarkerOnMap(latlng, title, drawableID,
                            false, snippet, false);
                    if (marker != null)
                        addHashMaker(marker, item);
                }
            }
        }
    }

    private void addHashMaker(Marker marker, RowBookPortModel item) {
        markerList.add(marker);
        hashTDByName.put(marker, item);
    }

    private Marker drawMarkerOnMap(LatLng latlng, String title, int dr, boolean isDraggable,
                                   String snippet, boolean isShowInfoWindow) {
        Marker marker = null;
        try {
            marker = MapCommon.addMarkerOnMap(mMap, title, latlng, dr, isDraggable);
            marker.setSnippet(snippet);
            if (isShowInfoWindow)
                marker.showInfoWindow();
            MapCommon.animeToLocation(mMap, latlng);
            MapCommon.setMapZoom(mMap, 18);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return marker;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    private void callBackPress() {
        this.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                AlertDialog.Builder builder;
                Dialog dialog;
                builder = new AlertDialog.Builder(mContext);
                builder.setMessage(getResources().getString(R.string.error_permission_denied))
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        callBackPress();
                                    }
                                });
                dialog = builder.create();
                dialog.show();
                return;
            }
        }
        if (requestCode == 2) {
            setUpMap();
        }
    }
}