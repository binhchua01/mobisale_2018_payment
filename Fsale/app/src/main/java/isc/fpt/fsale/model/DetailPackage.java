package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by haulc3 on 19,July,2019
 */
public class DetailPackage implements Parcelable {
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("ServiceCode")
    @Expose
    private Integer serviceCode;
    @SerializedName("PlanID")
    @Expose
    private String planId;

    private boolean isSelected;

    public DetailPackage() {
    }

    protected DetailPackage(Parcel in) {
        name = in.readString();
        planId = in.readString();
        if (in.readByte() == 0) {
            iD = null;
        } else {
            iD = in.readInt();
        }
        if (in.readByte() == 0) {
            serviceCode = null;
        } else {
            serviceCode = in.readInt();
        }
        isSelected = in.readByte() != 0;
    }

    public static final Creator<DetailPackage> CREATOR = new Creator<DetailPackage>() {
        @Override
        public DetailPackage createFromParcel(Parcel in) {
            return new DetailPackage(in);
        }

        @Override
        public DetailPackage[] newArray(int size) {
            return new DetailPackage[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Integer getiD() {
        return iD;
    }

    public void setiD(Integer iD) {
        this.iD = iD;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(planId);
        if (iD == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(iD);
        }
        if (serviceCode == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(serviceCode);
        }
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ID", getiD());
            jsonObject.put("PlanID", getPlanId());
            jsonObject.put("Name", getName());
            jsonObject.put("ServiceCode", getServiceCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
