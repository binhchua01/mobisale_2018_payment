/**
 *
 */
package isc.fpt.fsale.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPromotionList;
import isc.fpt.fsale.adapter.PromotionAutoCompleteAdapter;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * @author ISC-HUNGLQ9
 */
// màn hình lọc clkm internet
public class PromotionFilterActivity extends BaseActivity implements OnItemClickListener {

    public static final String TAG_PROMOTION_LIST = "TAG_PROMOTION_LIST";
    public static final String TAG_PROMOTION_ITEM = "TAG_PROMOTION_ITEM";
    public static final String TAG_LOCAL_TYPE = "TAG_LOCAL_TYPE";
    public static final String TAG_SERVICE_TYPE = "TAG_SERVICE_TYPE";
    public static final String TAG_CONTRACT = "TAG_CONTRACT";
    private EditText txtFilter;
    //private Button btnFind;
    private ImageButton imgClear;
    private ListView lvPromotion;
    private ArrayList<PromotionModel> lst;
    private int mLocalType, mServiceType;
    private String mContract;
    private Context mContext;

    public PromotionFilterActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.lbl_screen_name_promotion_filter);
        MyApp.setCurrentActivity(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_promotion_filter));
        setContentView(R.layout.activity_promotion_filter);
        this.mContext = this;
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));

        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        txtFilter = (EditText) findViewById(R.id.txt_filter);
        txtFilter.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() > 0)
                    imgClear.setVisibility(View.VISIBLE);
                else
                    imgClear.setVisibility(View.GONE);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                try {
                    if (s.length() > 0) {
                        ((PromotionAutoCompleteAdapter) lvPromotion.getAdapter()).getFilter().filter(s);
                    } else {
                        setFilterAdapter();
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        });
        imgClear = (ImageButton) findViewById(R.id.btn_clear_text);
        imgClear.setVisibility(View.GONE);
        imgClear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                txtFilter.setText("");
            }
        });
        lvPromotion = (ListView) findViewById(R.id.lv_promotion);
        lvPromotion.setOnItemClickListener(this);
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(TAG_PROMOTION_LIST))
                this.lst = intent.getParcelableArrayListExtra(TAG_PROMOTION_LIST);
            if (intent.hasExtra(TAG_LOCAL_TYPE))
                this.mLocalType = intent.getIntExtra(TAG_LOCAL_TYPE, 0);
            if (intent.hasExtra(TAG_SERVICE_TYPE))
                this.mServiceType = intent.getIntExtra(TAG_SERVICE_TYPE, 0);
            if (intent.hasExtra(TAG_CONTRACT))
                this.mContract = intent.getStringExtra(TAG_CONTRACT);
        }
        if (this.lst != null) {
            setFilterAdapter();
        } else {
            getPromotion();
        }
    }

    private void getPromotion() {
        int locationID = ((MyApp) this.getApplication()).getLocationID();
        new GetPromotionList(this, String.valueOf(locationID), mLocalType, mServiceType, mContract);
    }

    public void updatePromotionList(ArrayList<PromotionModel> lst) {
        if (lst != null) {
            this.lst = lst;
            setFilterAdapter();
        }
    }

    private void setFilterAdapter() {
        PromotionAutoCompleteAdapter adapter = new PromotionAutoCompleteAdapter(this.mContext, R.layout.row_auto_complete, lst);
        lvPromotion.setAdapter(adapter);
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        getDataFromIntent();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub
        PromotionModel item = (PromotionModel) parent.getItemAtPosition(position);
        if (item != null) {
            Intent intent = new Intent();
            intent.putExtra(TAG_PROMOTION_ITEM, item);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
