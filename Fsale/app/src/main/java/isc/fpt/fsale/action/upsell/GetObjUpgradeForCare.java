package isc.fpt.fsale.action.upsell;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeForCare;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetObjUpgradeForCare implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private OnGetObjUpdgradeForCare mCallback;

    public GetObjUpgradeForCare(Context mContext, String[] arrParams, OnGetObjUpdgradeForCare mCallback) {
        this.mCallback = mCallback;
        this.mContext = mContext;
        String[] params = new String[]{"ObjID", "ProgramUpgradeID", "UserName","Contract"};
        String message = "Xin vui long cho giay lat...";
        String GET_OBJ_UPGRADE_FOR_CARE = "GetObjUpgradeForCare";
        CallServiceTask service = new CallServiceTask(mContext, GET_OBJ_UPGRADE_FOR_CARE, params, arrParams,
                Services.JSON_POST, message, GetObjUpgradeForCare.this);
        service.execute();
    }

    public interface OnGetObjUpdgradeForCare{
        void onGetListSuccess(List<ObjUpgradeForCare> mList);
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<ObjUpgradeForCare> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ObjUpgradeForCare.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialogUpgrade(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        mCallback.onGetListSuccess(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
