package isc.fpt.fsale.model;

import java.lang.reflect.Field;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;


public class ReportSurveyManagerModel implements Parcelable {
	
	/* 
	 	Du lieu WEB ServiceType
	 	 
		RowNumber (INT)	Số thứ tự
		RegCode (STRING)	Phiếu đăng ký
		Contract (STRING)	Số hợp đồng
		FullName (STRING)	Tên khách hàng
		FirstTotalCab (INT)	Chiều dài cab ban đầu
		Balance (INT)	Chiều dài cab lệch
		TotalPage (INT)	Tổng trang
		CurrentPage (INT)	Trang hiện tại
		TotalRow (INT)	Tổng dòng
		AfterTotalCab (INT) Chiều dài phiếu thi công 

	 */
	
	private Integer RowNumber;
	private String RegCode;
	private String Contract;
	private String FullName;
	private Integer FirstTotalCab;
	private Integer Balance;
	private Integer TotalPage;
	private Integer CurrentPage;
	private Integer TotalRow;
	private Integer AfterTotalCab;
	
	
	public ReportSurveyManagerModel(){
		
	}
	public Integer getAfterTotalCab()
	{
		return this.AfterTotalCab;
	}
	
	public void setAfterTotalCab(Integer AfterTotalCab)
	{
		this.AfterTotalCab = AfterTotalCab;
	}
	
	public Integer getRowNumber() {
		return RowNumber;
	}

	public void setRowNumber(Integer rowNumber) {
		RowNumber = rowNumber;
	}

	public String getRegCode() {
		return RegCode;
	}

	public void setRegCode(String regCode) {
		RegCode = regCode;
	}

	public String getContract() {
		return Contract;
	}

	public void setContract(String contract) {
		Contract = contract;
	}

	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullName) {
		FullName = fullName;
	}

	public Integer getFirstTotalCab() {
		return FirstTotalCab;
	}

	public void setFirstTotalCab(Integer firstTotalCab) {
		FirstTotalCab = firstTotalCab;
	}

	public Integer getBalance() {
		return Balance;
	}

	public void setBalance(Integer balance) {
		Balance = balance;
	}

	public int getTotalPage() {
		return TotalPage;
	}

	public void setTotalPage(int totalPage) {
		TotalPage = totalPage;
	}

	public int getCurrentPage() {
		return CurrentPage;
	}

	public void setCurrentPage(int currentPage) {
		CurrentPage = currentPage;
	}

	public int getTotalRow() {
		return TotalRow;
	}

	public void setTotalRow(int totalRow) {
		TotalRow = totalRow;
	}
	
	
	public ReportSurveyManagerModel(Integer _RowNumber,String _RegCode,String _Contract,String _FullName, Integer _FirstTotalCab,
			Integer _Balance, 	int _TotalPage,int _CurrentPage, int _TotalRow, Integer _AfterTotalCab)
	{
		this.RowNumber=_RowNumber;
		this.RegCode=_RegCode;
		this.Contract=_Contract;
		this.FullName=_FullName;
		this.FirstTotalCab=_FirstTotalCab;
		this.Balance=_Balance;
		this.TotalPage=_TotalPage;
		this.CurrentPage=_CurrentPage;
		this.TotalRow=_TotalRow;
		this.AfterTotalCab= _AfterTotalCab;
		
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel pc, int flags) {
		// TODO Auto-generated method stub
		pc.writeInt(RowNumber);
		pc.writeString(RegCode);
		pc.writeString(Contract);
		pc.writeString(FullName);
		pc.writeInt(FirstTotalCab);
		pc.writeInt(Balance);
		pc.writeInt(TotalPage);
		pc.writeInt(CurrentPage);
		pc.writeInt(TotalRow);
		pc.writeInt(AfterTotalCab);
	}
	
	public ReportSurveyManagerModel(Parcel source){
		
		RowNumber = source.readInt();
		RegCode = source.readString();
		Contract = source.readString();
		FullName = source.readString();
		FirstTotalCab = source.readInt();
		Balance = source.readInt();
		TotalPage = source.readInt();
		CurrentPage = source.readInt();
		TotalRow = source.readInt();
		AfterTotalCab = source.readInt();
	}
	
	 public static ReportSurveyManagerModel Parse(JSONObject json){  
		 ReportSurveyManagerModel object = new ReportSurveyManagerModel();
	    	try {
	    		Field[] fields = object.getClass().getDeclaredFields();
	    		 for (Field field : fields) {
	    			 String propertiesName = field.getName();
	    			 if(json.has(propertiesName)){
	    				 field.setAccessible(true);
	    				 if(field.getType().getSimpleName().equals("String"))    					
	    					 field.set(object, json.getString(propertiesName));    					 
	    				 else if(field.getType().getSimpleName().equals("Integer"))
	    					 field.set(object, json.getInt(propertiesName));
	    			 }
	    		 }
	    		 return object;
			} catch (Exception e) {
				//String message = e.getMessage();

				e.printStackTrace();
			}
	    	return null;
	    }
	
	/** Static field used to regenerate object, individually or as arrays */
	public static final Creator<ReportSurveyManagerModel> CREATOR = new Creator<ReportSurveyManagerModel>() {
		@Override     
		public ReportSurveyManagerModel createFromParcel(Parcel source) {
	             return new ReportSurveyManagerModel(source);
		}
		@Override
		public ReportSurveyManagerModel[] newArray(int size) {
             return new ReportSurveyManagerModel[size];
        }
	};
}
