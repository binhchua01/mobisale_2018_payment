package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListRegistration;
import isc.fpt.fsale.action.GetPotentialObjDetail;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.activity.ListObjectSearchActivity;
import isc.fpt.fsale.adapter.RegistrationListAdapter;
import isc.fpt.fsale.model.ListRegistrationModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.ui.choose_service.ChooseServiceActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// Màn hình hiển thị danh sách phiếu đăng ký
public class FragmentListRegistration extends Fragment implements
        OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_REG_TYPE = "ARG_REG_TYPE";
    private static final String ARG_TITLE = "ARG_TITLE";

    private int mPage = 1, mRegType = 0;
    public ListView lvReg;
    private Context mContext;
    private ImageView imgCreate;
    private SwipeRefreshLayout swipeContainer;
    private int mTotalPage;
    private boolean isLoading = false;
    private List<ListRegistrationModel> mList;
    private RegistrationListAdapter mAdapter;

    public static FragmentListRegistration newInstance(int agent, String title) {
        FragmentListRegistration fragment = new FragmentListRegistration();
        Bundle args = new Bundle();
        args.putInt(ARG_REG_TYPE, agent);
        args.putString(ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentListRegistration() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mRegType = getArguments().getInt(ARG_REG_TYPE, 0);
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_registration, container, false);
        mContext = getActivity();
        initView(rootView);
        initEventView();
        getDataDefault();
        return rootView;
    }

    private void getDataDefault() {
        if (mRegType == 0) {
            getData(mPage);
        }
    }

    private void initEventView() {
        lvReg.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListRegistrationModel selectedItem = (ListRegistrationModel) parent.getItemAtPosition(position);
                new GetRegistrationDetail(mContext, Constants.USERNAME, selectedItem.getID());
                Log.d("TAG", "onItemClick: " + selectedItem.getID());
            }
        });
        lvReg.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if (isLoading) {
                        if (mPage < mTotalPage) {
                            mPage++;
                            getData(mPage);
                            isLoading = false;
                        }
                    }
                }
            }
        });
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main),
                getResources().getColor(R.color.background_main));
        swipeContainer.setOnRefreshListener(this);
        // Neu la PDK chua duoc duyet thi an nut tao moi
        if (mRegType == 1) {
            imgCreate.setVisibility(View.GONE);
        }

        imgCreate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /* PĐK bán mới */
                if (mRegType == 0) {
                    Intent intent = new Intent(mContext, ChooseServiceActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(intent);
                } else if (mRegType == 2) { /* PĐK bán thêm */
                    Intent intent = new Intent(mContext, ListObjectSearchActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    private void initView(View rootView) {
        lvReg = (ListView) rootView.findViewById(R.id.lv_object);
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.container);
        imgCreate = (ImageView) rootView.findViewById(R.id.img_create);

        mList = new ArrayList<>();
        mAdapter = new RegistrationListAdapter(mContext, mList);
        lvReg.setAdapter(mAdapter);

    }

    // lấy danh sách theo trang
    private void getData(int page) {
        new GetListRegistration(mContext, Constants.USERNAME, page, mRegType, null, this);
    }

    //refresh list
    public void getData() {
        mPage = 1;
        mList.clear();
        new GetListRegistration(mContext, Constants.USERNAME, mPage, mRegType, null, this);
    }

    // xử lý dữ liệu trả về danh sách phiếu đăng ký và phân trang từ api
    public void loadData(ArrayList<ListRegistrationModel> lst) {
        if (lst != null && lst.size() > 0) {
            mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            this.mList.addAll(lst);
            mAdapter.notifyData(this.mList);
            lvReg.invalidateViews();
            isLoading = true;
        }
    }

    // sự kiện nhấn vào 1 pdk trong danh sách
    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        try {
            Common.hideSoftKeyboard(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PotentialObjModel selectedItem = (PotentialObjModel) parentView.getItemAtPosition(position);
        if (selectedItem != null) {
            new GetPotentialObjDetail(mContext, Constants.USERNAME, selectedItem.getID());
        }
    }

    // sự kiện kéo cuộn refresh danh sách pdk
    @Override
    public void onRefresh() {
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                getData();
                swipeContainer.setRefreshing(false);
            }
        });
    }
}
