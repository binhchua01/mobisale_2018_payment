package isc.fpt.fsale.ui.maxytv.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.maxy.GetMaxyDevice;
import isc.fpt.fsale.action.maxy.GetMaxyExtra;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyExtraAdapter;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyTypeAdapter;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.ui.maxytv.model.MaxyExtra;
import isc.fpt.fsale.ui.maxytv.model.MaxyGeneral;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.MAXY_GENERAL;
import static isc.fpt.fsale.utils.Constants.NUM_BOX_NET;
import static isc.fpt.fsale.utils.Constants.NUM_EXTRA;
import static isc.fpt.fsale.utils.Constants.REGTYPE;
import static isc.fpt.fsale.utils.Constants.TYPE_DEPLOY;

public class MaxyExtraActivity extends BaseActivitySecond implements OnItemClickListener<MaxyExtra> {
    private View loBack;
    private List<MaxyExtra> mList;
    private List<MaxyExtra> mListExtraSelected;
    private MaxyExtraAdapter mAdapter;
    private String mClassName;
    private int mDevice, RegType, countExtra, countBox;
    private String contract;
    private MaxyGeneral mMaxyGeneral;
    private boolean is_selected_extra;


    @Override
    protected void initEvent() {
        loBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_device_list;
    }

    @Override
    protected void initView() {
        getDataIntent();

        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText("Danh sách gói Extra");
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_device_list);
        mAdapter = new MaxyExtraAdapter(this, mList != null ? mList : new ArrayList<MaxyExtra>(), this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelableArrayList(Constants.LIST_MAXY_EXTRA_SELECTED) != null) {
            mListExtraSelected = bundle.getParcelableArrayList(Constants.LIST_MAXY_EXTRA_SELECTED);
            RegType = bundle.getInt(REGTYPE);
            mMaxyGeneral = bundle.getParcelable(MAXY_GENERAL);
        }
        mDevice = bundle.getInt(Constants.LIST_MAXY_DETAIL_SELECTED);
        contract = bundle.getString(Constants.CONTRACT_EXTRA);
        countExtra = bundle.getInt(NUM_EXTRA);
        countBox = bundle.getInt(NUM_BOX_NET);
        if (bundle != null && bundle.getString(Constants.CLASS_NAME) != null) {
            mClassName = bundle.getString(Constants.CLASS_NAME);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(mClassName)) {
            new GetMaxyExtra(this, RegType, contract, mMaxyGeneral);
        }
    }

    @Override
    public void onItemClick(MaxyExtra mMaxy) {
        if (mDevice == 0) {
            if (mMaxy.getExtraType() == 1) {
                Common.alertDialog("Vui lòng chọn thiết bị Truyền hình!", this);
            } else {
                mMaxy.setPrepaidMonth(0);
                mMaxy.setChargeTimes(1);
                Intent returnIntent = new Intent();
                returnIntent.putExtra(Constants.OBJECT_MAXY_EXTRA_DETAIL, mMaxy);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        } else {
                mMaxy.setPrepaidMonth(0);
                mMaxy.setChargeTimes(1);
                Intent returnIntent = new Intent();
                returnIntent.putExtra(Constants.OBJECT_MAXY_EXTRA_DETAIL, mMaxy);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
        }

    }
    public void setupDialog(){
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Bạn đã chọn đủ số lượng Extra theo box OnNet")
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.title_agree),
                        (dialog1, id) -> onBackPressed());
        dialog = builder.create();
        dialog.show();
    }

    public void loadExtraList(List<MaxyExtra> mList) {
        for (MaxyExtra item : mList) {
            for (MaxyExtra itemSelected : mListExtraSelected) {
                if (itemSelected.getID() == item.getID()) {
                    item.setSelected(true);
                }
            }
        }
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }


}
