package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.RPCodeInfo;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 01,November,2019
 */
public class GetDesReferralCode implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterStep4 fragment;

    public GetDesReferralCode(Context mContext, String rpCode, FragmentRegisterStep4 fragment) {
        this.mContext = mContext;
        this.fragment = fragment;
        String message = mContext.getResources().getString(R.string.msg_pd_rp_code_info);
        String GET_DES_REFERRAL_CODE = "GetDesReferralCode";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Code", rpCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CallServiceTask service = new CallServiceTask(mContext, GET_DES_REFERRAL_CODE, jsonObject,
                Services.JSON_POST_OBJECT, message, GetDesReferralCode.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<RPCodeInfo> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), RPCodeInfo.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
//                        fragment.loadRpCodeInfo(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
