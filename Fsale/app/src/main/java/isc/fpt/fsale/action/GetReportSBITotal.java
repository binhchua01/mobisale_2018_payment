package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Check application version
 * @author: 		DuHK
 * @create date: 	30/05/2015
 * */
import isc.fpt.fsale.activity.ListReportSBITotalActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportSBITotalModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetReportSBITotal implements AsyncTaskCompleteListener<String> {

	private final String METHOD_NAME = "ReportSBITotal";
	private Context mContext;	
	private final String TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error", TAG_LIST_OBJECT = "ListObject";
	private final String TAG_SALE_NAME = "SaleName", TAG_TITLE = "Title", TAG_TOTAL = "Total", /*TAG_ROW_NUMBER = "RowNumber",  */TAG_ROW_LIST = "Rows";
	private final String TAG_TYPE = "Type", TAG_CURRENT_PAGE = "CurrentPage", TAG_TOTAL_ROW = "TotalRow", TAG_TOTAL_PAGE = "TotalPage";
	//Add by: DuHK
	public GetReportSBITotal(Context mContext, String UserName, int Day, int Month, int Year, int Agent, String AgentName, int Status, int PageNumber){
		this.mContext = mContext;	
		
		String[] paramNames = {"UserName", "Day", "Month", "Year", "Agent", "AgentName", "Status", "PageNumber"};
		String[] paramValues = {UserName, String.valueOf(Day), String.valueOf(Month), String.valueOf(Year), String.valueOf(Agent), AgentName, String.valueOf(Status), String.valueOf(PageNumber)};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetReportSBITotal.this);
		service.execute();	
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			ArrayList<ReportSBITotalModel> lstReport = new ArrayList<ReportSBITotalModel>();
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
					 JSONArray jsArr = jsObj.getJSONArray(TAG_LIST_OBJECT);
					 if(jsArr != null & jsArr.length() > 0){		
						 jsArr = jsObj.getJSONArray(TAG_LIST_OBJECT);
						 if(jsArr != null & jsArr.length() > 0){		
							 for(int index=0 ; index<jsArr.length();index++){
								 JSONObject item = jsArr.getJSONObject(index);
								 ReportSBITotalModel newItem = new ReportSBITotalModel();
								 if(item.has(TAG_SALE_NAME))
									 newItem.setSaleName(item.getString(TAG_SALE_NAME));
								 if(item.has(TAG_CURRENT_PAGE))
									 newItem.setCurrentPage(item.getInt(TAG_CURRENT_PAGE));
								 if(item.has(TAG_TOTAL_PAGE))
									 newItem.setTotalPage(item.getInt(TAG_TOTAL_PAGE));
								 if(item.has(TAG_TOTAL_ROW))
									 newItem.setTotalRow(item.getInt(TAG_TOTAL_ROW));
								 newItem.setReportList(new ArrayList<KeyValuePairModel>());
								 if(item.has(TAG_ROW_LIST)){
									 JSONArray rowsArr = item.getJSONArray(TAG_ROW_LIST);
									 for(int i = 0;i<rowsArr.length();i++){
										 JSONObject subItem = rowsArr.getJSONObject(i);
										 KeyValuePairModel newSubItem = new KeyValuePairModel(0,"", "");
										 if(subItem.has(TAG_TYPE))
											 newSubItem.setID(subItem.getInt(TAG_TYPE));
										 if(subItem.has(TAG_TITLE))
											 newSubItem.setDescription(subItem.getString(TAG_TITLE));
										 if(subItem.has(TAG_TOTAL))
											 newSubItem.setHint(subItem.getString(TAG_TOTAL));
										 newItem.getReportList().add(newSubItem);
									 }
								 }								 
								 lstReport.add(newItem);
							 }
						 }
						/* JSONObject firstItem = jsArr.toJSONObject(0);
						 String groupBySaleName = firstItem.getString(TAG_SALE_NAME).trim();
						 int totalPage = 0, currentPage = 0, totalRow = 0;
						 if(firstItem.has(TAG_CURRENT_PAGE))
							 currentPage = firstItem.getInt(TAG_CURRENT_PAGE);
						 if(firstItem.has(TAG_TOTAL_PAGE))
							 totalPage = firstItem.getInt(TAG_TOTAL_PAGE);
						 if(firstItem.has(TAG_TOTAL_ROW))
							 totalRow = firstItem.getInt(TAG_TOTAL_ROW);
						 ReportSBITotalModel groupByItem = new ReportSBITotalModel(groupBySaleName,new ArrayList<KeyValuePairModel>(),
								 currentPage, totalRow, totalPage);
						 //GroupBy danh sach theo ten sale
						 JSONObject item = null;
						 for(int i=0;i<jsArr.length();i++){
							 item = jsArr.toJSONObject(i);
							 int type = 0;
							 String title = "", total = "";
							 if( !groupBySaleName.equals("") && groupBySaleName.equals(item.getString(TAG_SALE_NAME).trim())){								 
								 if(item.has(TAG_TYPE))
									 type = item.getInt(TAG_TYPE);
								 if(item.has(TAG_TITLE))
									 title = item.getString(TAG_TITLE);
								 if(item.has(TAG_TOTAL))
									 total = item.getString(TAG_TOTAL);
								 groupByItem.getReportList().add(new KeyValuePairModel(type, title, total));
							 }else{
								 lstReport.add(groupByItem);
								 groupBySaleName = item.getString(TAG_SALE_NAME).trim();
								 if(item.has(TAG_CURRENT_PAGE))
									 currentPage = item.getInt(TAG_CURRENT_PAGE);
								 if(item.has(TAG_TOTAL_PAGE))
									 totalPage = item.getInt(TAG_TOTAL_PAGE);
								 if(item.has(TAG_TOTAL_ROW))
									 totalRow = item.getInt(TAG_TOTAL_ROW);
								 
								 groupByItem = new ReportSBITotalModel(groupBySaleName,new ArrayList<KeyValuePairModel>(),
										 							currentPage, totalRow, totalPage);
								 if(item.has(TAG_TYPE))
									 type = item.getInt(TAG_TYPE);
								 if(item.has(TAG_TITLE))
									 title = item.getString(TAG_TITLE);
								 if(item.has(TAG_TOTAL))
									 total = item.getString(TAG_TOTAL);
								 groupByItem.getReportList().add(new KeyValuePairModel(type, title, total));
							 }
						 }
						 if(item != null){
							 lstReport.add(groupByItem);
							 //groupBySaleName = item.getString(TAG_SALE_NAME).trim();
							 //groupByItem = new ReportSBITotalModel(groupBySaleName,new ArrayList<KeyValuePairModel>());
						 }*/
					 }
				 }else{
					 String message = "Serrivce error";
					 if(jsObj.has(TAG_ERROR))
						 message = jsObj.getString(TAG_ERROR);
					 Common.alertDialog(message, mContext);
				 }
				 
			 }
			 loadData(lstReport);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(ArrayList<ReportSBITotalModel> lst){//ListReportSBITotalActivity
		try {
			if(mContext.getClass().getSimpleName().equals(ListReportSBITotalActivity.class.getSimpleName())){
				ListReportSBITotalActivity activity = (ListReportSBITotalActivity)mContext;
				activity.loadData(lst);
			}
		} catch (Exception e) {

			Log.i("ReportSBITotal.loadRpCodeInfo()", e.getMessage());
		}
	}

}
