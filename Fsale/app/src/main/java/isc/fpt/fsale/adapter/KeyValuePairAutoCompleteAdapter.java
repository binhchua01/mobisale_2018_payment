package isc.fpt.fsale.adapter;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;


public class KeyValuePairAutoCompleteAdapter extends ArrayAdapter<KeyValuePairModel>  implements Filterable{
	private ArrayList<KeyValuePairModel> lstObj, resultList;	
	//private Context mContext;
	/*private boolean hasInitText = false;
    private Typeface tf; */

	public KeyValuePairAutoCompleteAdapter(Context context, int textViewResourceId, ArrayList<KeyValuePairModel> lstObj) {
		super(context, textViewResourceId);
		this.resultList = this.lstObj = lstObj;
		this.lstObj = lstObj;
	}
	
	public KeyValuePairAutoCompleteAdapter(Context context, int layoutResourceId, int textViewResourceId, ArrayList<KeyValuePairModel> lstObj) {
		super(context, layoutResourceId, textViewResourceId);
		this.resultList = this.lstObj = lstObj;
		this.lstObj = lstObj;
	}
	
	public KeyValuePairAutoCompleteAdapter(Context context,	int textViewResourceId, ArrayList<KeyValuePairModel> lstObj, int color) {
		super(context, textViewResourceId);
		//this.mContext = context;
		this.resultList = this.lstObj = lstObj;
	}
	
	public KeyValuePairAutoCompleteAdapter(Context context,	int textViewResourceId, ArrayList<KeyValuePairModel> lstObj, boolean hasInitText) {
		super(context, textViewResourceId);
		//this.mContext = context;
		this.lstObj = lstObj;
	}
	

	public int getCount(){
	       return resultList.size();
	}
		
	public KeyValuePairModel getItem(int position){
		return resultList.get(position);
    }
	
	public long getItemId(int position){
       return position;
    }
	
	public ArrayList<KeyValuePairModel> getList(){
		return resultList;
	}
	
	/*@Override
    public View getDropDownView(int position, View convertView,
            ViewGroup parent) {
        TextView label = new TextView(getContext());
        if(hasInitText && position == 0){
        	label.setVisibility(View.GONE);  
        }
        else{
            label.setText(lstObj.get(position).getDescription());
            label.setTypeface(tf);
	        int padding = (int) getContext().getResources().getDimension(R.dimen.padding_medium);
	        label.setPadding(padding, padding, padding, padding);
        }
        return label;    	
    }	*/
	
	@SuppressLint("InflateParams")
	@Override
    public View getDropDownView(int position, View convertView,
            ViewGroup parent) {
    	if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_auto_complete, null);
		}
        TextView label = (TextView)convertView.findViewById(R.id.lbl_desc);
        if(lstObj.get(position) == null){
        	label.setVisibility(View.GONE);  
        }
        else{
            label.setText(lstObj.get(position).getDescription());
            //label.setTypeface(tf);
	        //int padding = (int) mContext.getResources().getDimension(R.dimen.padding_medium);
	        //label.setPadding(padding, padding, padding, padding);
        }
        int padding = (int) getContext().getResources().getDimension(R.dimen.padding_medium);
        convertView.setPadding(padding,padding,padding,padding);    	
        return convertView;    	
    }	
	
	
	@SuppressLint("InflateParams")
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_auto_complete, null);
		}
		KeyValuePairModel item = getItem(position);
        TextView label = (TextView)convertView.findViewById(R.id.lbl_desc);
        if(item != null)
        	label.setText(item.getDescription()); 
        
        return label;
    }
	
	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                	resultList = getPromotionFilterList(constraint.toString());
                	filterResults.values = resultList;
                	filterResults.count = resultList.size();
                } 
                return filterResults;
            }
 
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }
        };
 
        return filter;
    }
	
	@SuppressLint("DefaultLocale")
	private ArrayList<KeyValuePairModel> getPromotionFilterList(String constraint){
		String[] subStrs = constraint.split("\\*");
    	ArrayList<KeyValuePairModel> results = new ArrayList<KeyValuePairModel>();
    	//results = lstObj;
    	for(int i = 0;i<lstObj.size();i++){
    		KeyValuePairModel item = lstObj.get(i);
    		String parentStr = item.getDescription().toUpperCase();
    		boolean exist = true;
    		if(subStrs.length > 0){
	    		for(int j =0;j<subStrs.length;j++){
	    			String s = subStrs[j].toUpperCase();
	    			if(s != null && !s.trim().equals(""))
	    				if(!parentStr.contains(s))
	    					exist = false;                				
	    		}
	    		if(exist){
	    			results.add(item);
	    		}
    		}else
    			results = lstObj;
    			
    	}
    	return results;
	}
	
	
	
}
