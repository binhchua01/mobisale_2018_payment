package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/*
 * @Description: Kiểm tra số nhà
 * @author: DuHK
 * @create date:
 */

public class CheckHouseNumber implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    public final String TAG_METHOD_NAME = "CheckAddressNumber";
    private RegistrationDetailModel mRegister;

    public CheckHouseNumber(Context context, String LocationParent, String HouseNumber, RegistrationDetailModel mRegister) {
        mContext = context;
        this.mRegister = mRegister;
        String[] paramNames = new String[]{"UserName", "LocationParent", "HouseNumber"};
        String[] paramValues = new String[]{Constants.USERNAME, LocationParent, HouseNumber};
        String message = "Đang kiểm tra...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, CheckHouseNumber.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                List<UpdResultModel> lst;
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(
                        jsObj, UpdResultModel.class);
                lst = resultObject.getListObject();
                loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(List<UpdResultModel> lst) {
        if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
            RegisterActivityNew activity = (RegisterActivityNew) mContext;
            activity.checkRegister(lst, mRegister);
        }
    }
}