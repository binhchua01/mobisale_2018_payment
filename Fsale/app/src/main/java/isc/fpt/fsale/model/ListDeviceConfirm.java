package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by haulc3 on 18,April,2019
 */
public class ListDeviceConfirm {
    @SerializedName("ListDevice")
    @Expose
    private List<DeviceConfirm> listDevice = null;
    @SerializedName("SaleName")
    @Expose
    private String saleName;
    @SerializedName("Time")
    @Expose
    private String time;

    private boolean isChecked = false;

    public List<DeviceConfirm> getListDevice() {
        return listDevice;
    }

    public void setListDevice(List<DeviceConfirm> listDevice) {
        this.listDevice = listDevice;
    }

    public String getSaleName() {
        return saleName;
    }

    public void setSaleName(String saleName) {
        this.saleName = saleName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
