package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ListGiftBoxOTT;
import isc.fpt.fsale.model.ListGiftBoxOTTResult;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetListGiftBoxOTT implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterStep4 activity;

    public GetListGiftBoxOTT(Context mContext, FragmentRegisterStep4 activity, ListGiftBoxOTT object) {
        this.mContext = mContext;
        this.activity = activity;
        String message = mContext.getResources().getString(R.string.msg_pd_get_list_gift_box_ott);
        String GET_LIST_GIFT_BOX_OTT = "GetListGiftBoxOTT";
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_GIFT_BOX_OTT, object.toJsonObject(),
                Services.JSON_POST_OBJECT, message, GetListGiftBoxOTT.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<ListGiftBoxOTTResult> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ListGiftBoxOTTResult.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    activity.loadDataSpGiftBoxOTT(lst);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
