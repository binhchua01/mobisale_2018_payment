
package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.Gravity;
import android.widget.Spinner;
import android.widget.TextView;

public class GetPortFreeADSLAction implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private Spinner spPortFree;
    private ArrayList<KeyValuePairModel> LIST_PORT;
    private int TypeService;
    private String TDName;
    private TextView txtPortFreeAvailability;

    public GetPortFreeADSLAction(Context _mContext, String TDName, Spinner sp,
                                 int _TypeService, TextView txtPortFreeAvailability) {
        this.mContext = _mContext;
        this.spPortFree = sp;
        this.TypeService = _TypeService;
        this.txtPortFreeAvailability = txtPortFreeAvailability;
        this.TDName = TDName;
        String[] arrParams = new String[]{TDName};
        CallService(arrParams, this.TypeService);

    }

    private void CallService(String[] arrParams, int TypeService) {
        String message = "Xin cho trong giay lat";
        String[] params = new String[]{"TDName"};
        if (TypeService == 1) {

            String GET_PORT_FREE_ADSL = "GetPortFreeFirstTD";
            CallServiceTask service = new CallServiceTask(mContext, GET_PORT_FREE_ADSL, params, arrParams,
                    Services.JSON_POST, message, GetPortFreeADSLAction.this);
            service.execute();
        } else {
            String GET_PORT_FREE_VDSL = "GetPortFreeFirstTD";
            CallServiceTask service = new CallServiceTask(mContext, GET_PORT_FREE_VDSL, params, arrParams,
                    Services.JSON_POST, message, GetPortFreeADSLAction.this);
            service.execute();
        }
    }

    public void handleGetDistrictsResult(String json) {
        if (Common.jsonObjectValidate(json)) {
            LIST_PORT = new ArrayList<>();
            LIST_PORT.add(new KeyValuePairModel(-1, "[ Vui lòng chọn port ]"));
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if (Common.isEmptyJSONObject(jsObj)) {
                KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LIST_PORT, Gravity.LEFT);
                spPortFree.setAdapter(adapter);
                return;
            }
            bindData(jsObj);
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LIST_PORT, Gravity.LEFT);
            spPortFree.setAdapter(adapter);
        } else {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
        }
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {
            if (this.TypeService == 1) {
                String TAG_GET_PORT_FREE_RESULT_ADSL = "GetPortFreeFirstTDMethodPostResult";
                jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_RESULT_ADSL);
            } else {
                String TAG_GET_PORT_FREE_RESULT_VDSL = "GetPortFreeFirstTDMethodPostResult";
                jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_RESULT_VDSL);
            }
            String error = jsArr.getJSONObject(0).getString("ErrorService");
            if (error.equals("null")) {
                int l = jsArr.length();
                if (l > 0) {
                    LIST_PORT = new ArrayList<>();
                    for (int i = 0; i < l; i++) {
                        JSONObject iObj = jsArr.getJSONObject(i);
                        LIST_PORT.add(new KeyValuePairModel(iObj.getInt("ID"), iObj.getString("PortID")));
                    }
                    new GetPortFreeAvailabilityADSL(mContext, TDName, TypeService, l, txtPortFreeAvailability);
                }
            } else {
                Common.alertDialog("Lỗi WS: " + error, mContext);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleGetDistrictsResult(result);
    }
}