package isc.fpt.fsale.action;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by HCM.TUANTT14 on 9/13/2018.
 */
// API kết nối ShortLink
public class MobiSaleResetShortlink implements AsyncTaskCompleteListener<String> {
    private String[] arrParamName, arrParamValue;
    private Context mContext;
    private final String MOBISALE_RESET_SHORT_LINK = "MobiSale_ResetShortlink";
    private final String ResultID = "ResultID", Result = "Result", ListObject = "ListObject";

    //Constructor
    public MobiSaleResetShortlink(Context context, RegistrationDetailModel mRegister) {
        try {
            this.mContext = context;
            arrParamName = new String[]{"RegCode", "SaleName", "ObjID", "Contract"};
            this.arrParamValue = new String[]{mRegister.getRegCode(), mRegister.getUserName(), String.valueOf(mRegister.getObjID()), mRegister.getContract()};
            String message = mContext.getResources().getString(R.string.msg_pd_process_get_short_link);
            CallServiceTask service = new CallServiceTask(mContext, MOBISALE_RESET_SHORT_LINK, arrParamName, arrParamValue, Services.JSON_POST, message, MobiSaleResetShortlink.this);
            service.execute();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    if (jsObj.has(Result)) {
                        Common.alertDialog(jsObj.getString(Result), mContext);
                    } else {
                        Common.alertDialog(jsObj.toString(), mContext);
                    }
                } else {
                    Common.alertDialog(jsObj.toString(), mContext);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
