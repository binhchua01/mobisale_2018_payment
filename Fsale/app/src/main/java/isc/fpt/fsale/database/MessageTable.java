package isc.fpt.fsale.database;

import isc.fpt.fsale.database.DaoMaster.OpenHelper;
import isc.fpt.fsale.database.MessageModelDao.Properties;
import isc.fpt.fsale.model.MessageModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class MessageTable {
	private SQLiteDatabase db;    
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private MessageModelDao messageDao;
    private OpenHelper helper;
    private Context mContext;
    
    public MessageTable(Context context){
    	this.mContext = context;
    	helper = new OpenHelper(mContext, Constants.DATABASE_NAME, null) {
			
			@Override
			public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
				// TODO Auto-generated method stub
				
			}
		};    	
		/*db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        messageDao = daoSession.getMessageModelDao();*/
    }
    
    private void openDB(){
    	db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        messageDao = daoSession.getMessageModelDao();
    }
    
    private void closeDB(){
    	if(helper != null)
    		helper.close();    	
    }
    public Long add(MessageModel item){
    	openDB();
    	Long result =  messageDao.insert(item);
    	closeDB();
    	return result;
    }
    
    public int add(List<MessageModel> items){
    	openDB();
    	int count = 0;
    	for(MessageModel item : items )
    		if(messageDao.insert(item) >0)
    			count++;
    	closeDB();
    	return count;
    }
    
    public void update(MessageModel item){
    	 openDB();
    	 messageDao.update(item);
    	 closeDB();
    }
    
    public void update(List<MessageModel> items){
    	openDB();
    	for(MessageModel item : items )
    		messageDao.update(item);
    	closeDB();
   }
    
    public void remove(MessageModel item){
    	openDB();
    	messageDao.delete(item);
    	closeDB();
    }
    
    public void remove(List<MessageModel> items){
    	try {
    		for(MessageModel item: items)
    			remove(item);
		} catch (Exception e) {

			e.printStackTrace();
		}
    }
    
    public void removeAll(){
    	openDB();
    	messageDao.deleteAll();
    	closeDB();
    }
    
    public List<MessageModel> getAllMessage(){
    	openDB();
    	List<MessageModel> result = messageDao.loadAll();
    	closeDB();
    	return result;
    }
    
    /*
    public List<MessageModel> getMessageSendFrom(String sendFrom){    	
    	return messageDao.queryBuilder().where(Properties.Sendfrom.eq(sendFrom)).list();
    }*/
    
    /*
     * TODO: Lấy lịch sử chat giữa 2 người
     * */
    public List<MessageModel> getConversation(String sendFrom){ 
    	openDB();
    	List<MessageModel> result = messageDao.queryBuilder().whereOr(Properties.Sendfrom.eq(sendFrom), Properties.To.eq(sendFrom)).list();
    	closeDB();
    	return result;
    }
    
   /*
    * TODO: Lấy danh sách các tin nhắn mới nhất theo người gửi => DS những người đã chat (lấy ra tin mới nhất)
    * */
   @SuppressLint("DefaultLocale")
   public List<MessageModel> getSendFromList(){
    	List<MessageModel> result = new ArrayList<MessageModel>();
    	String userName = null;
    	try {
    		userName = ((MyApp)mContext.getApplicationContext()).getUserName();
    		if(userName == "")
    			userName = Common.loadPreference(mContext, Constants.SHARE_PRE_GROUP_USER, Constants.SHARE_PRE_GROUP_USER_NAME);
		} catch (Exception e) {

			e.printStackTrace();
			userName = Constants.USERNAME;
		}
    	openDB();
    	List<MessageModel> lst =  messageDao.queryBuilder().where(Properties.Sendfrom.notEq(userName)).orderDesc(Properties.Id).list();
    	closeDB();
    	MessageModel item = new MessageModel();
    	if(lst.size() > 0){
    		item = lst.get(0);
    		result.add(item);
    	}
    	for(int i= 1; i<lst.size(); i++){
    		if(lst.get(i) != null)//Neu tim thay nguoi gui khac nguoi gui hien tai thi kiem tra Nguoi gui nay da add vao hay chua
				if(!lst.get(i).getSendfrom().toUpperCase().equals(item.getSendfrom().toUpperCase())){
						if(!messageContainsItemFromList(lst.get(i), result)){
							item = lst.get(i);
			    			result.add(item);
						}
	    		}
    	}
    	return result;
    }
    
   @SuppressLint("DefaultLocale")
   public static boolean messageContainsItemFromList(MessageModel value, List<MessageModel> items)
   {
       for(MessageModel item: items)
       {
           if(item.getSendfrom().toUpperCase().equals(value.getSendfrom().toUpperCase()))
           {
               return true;
           }
       }
       return false;
   }
   
   /*
    * TODO: Lấy tin nhắn gần nhất theo người gửi
    * */
   
   public MessageModel getLastMessageSendBy(String sendFrom){ 
	    openDB();
   		List<MessageModel> result = messageDao.queryBuilder().where(Properties.Sendfrom.eq(sendFrom)).orderDesc(Properties.Id).limit(1).list();
   		closeDB();
   		if(result != null && result.size()>0)
   			return result.get(0);   		
   		return null;
   }
   
   public int getCountUnReadMessage(){ 
	   try {
		   openDB();
		   String userName = null;
	    	try {
	    		userName = ((MyApp)mContext.getApplicationContext()).getUserName();
	    		if(userName == "")
	    			userName = Common.loadPreference(mContext, Constants.SHARE_PRE_GROUP_USER, Constants.SHARE_PRE_GROUP_USER_NAME);
			} catch (Exception e) {

				e.printStackTrace();
				userName = Constants.USERNAME;
			}
		   List<MessageModel> result = messageDao.queryBuilder().where(Properties.Sendfrom.notEq(userName)).whereOr(Properties.IsRead.isNull(), Properties.IsRead.eq(false)).list();
		   closeDB();
		   return result.size();
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			return -1;
		}
  }
  
   /* public void updateAllIsDelete(boolean isDelete){
    	messageDao.updateAllIsDelete(isDelete);
    }
    */
    
}
