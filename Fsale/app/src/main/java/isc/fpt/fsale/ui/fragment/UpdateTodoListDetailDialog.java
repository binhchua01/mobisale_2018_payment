package isc.fpt.fsale.ui.fragment;

import java.util.ArrayList;

import isc.fpt.fsale.action.UpdateTodoListDetail;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ToDoListModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;


public class UpdateTodoListDetailDialog extends DialogFragment{
	
	private Context mContext;
	private ImageButton imgClose;
	private Button btnUpdate;
	
	private EditText txtDesc;
	private Spinner spCareType;
	
	private ToDoListModel todoListItem;
	private RadioButton radFinish;

	public UpdateTodoListDetailDialog(){ }

	@SuppressLint("ValidFragment")
	public UpdateTodoListDetailDialog(Context context, ToDoListModel todoList) {
		this.mContext= context;				
		this.todoListItem = todoList;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		// Inflate the layout to use as dialog or embedded fragment
		View view = inflater.inflate(R.layout.dialog_update_todolist_detail, container);
		Common.setupUI(view);
		txtDesc = (EditText)view.findViewById(R.id.txt_desc);
		spCareType = (Spinner)view.findViewById(R.id.sp_care_type);
		imgClose = (ImageButton)view.findViewById(R.id.img_close);
		imgClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getDialog().dismiss();
			}
		});
		
		radFinish = (RadioButton)view.findViewById(R.id.rad_is_finish);
		btnUpdate = (Button)view.findViewById(R.id.btn_update);
		btnUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				comfirmToUpdate();
			}
		});
		initCusType();
		return view;
	}
	
	
	/** The system calls this only when creating the layout in a dialog. */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {	   
	    Dialog dialog = super.onCreateDialog(savedInstanceState);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    return dialog;
	}
	@Override
	public void onStart() {
		
		super.onStart();
	}
	private void initCusType(){
    	ArrayList<KeyValuePairModel> lstObjType = new ArrayList<KeyValuePairModel>();
		lstObjType.add(new KeyValuePairModel("Call", "Gọi điện cho KH"));
		lstObjType.add(new KeyValuePairModel("SMS", "Gửi SMS"));
		lstObjType.add(new KeyValuePairModel("Email", "Gửi email"));		
		lstObjType.add(new KeyValuePairModel("", "Khác..."));
    	/*lstObjType.add(new KeyValuePairModel("CALL", "CALL"));
		lstObjType.add(new KeyValuePairModel("SMS", "SMS"));
		lstObjType.add(new KeyValuePairModel("EMAIL", "EMAIL"));		
		lstObjType.add(new KeyValuePairModel("", "Khác..."));*/
		KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstObjType, Gravity.LEFT);
		spCareType.setAdapter(adapter);
    }
	
	private void comfirmToUpdate(){
		 new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage("Bạn có muốn cập nhật?")
			.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {			 				      
					//new GetRegistrationDetail(mContext, Constants.USERNAME, String.valueOf(regID));
			       //MyApp.currentActivity().finish();			 				    	
			    	update();
			      
			    }
			})
			.setCancelable(false).create().show();	
	}
	
	private boolean checkForUpdate(String careType, String desc){
		if(careType.trim().equals("") && desc.trim().equals("")){
			Common.alertDialog("Vui lòng nhập nội dung ghi chú!", mContext);
			return false;
		}
		return true;
	}
	private void update(){
		int todoListID = 0, status = 0;
		String userName ="",  careType = "", desc = "";
		userName = ((MyApp)mContext.getApplicationContext()).getUserName();
		if(todoListItem != null){
			todoListID = todoListItem.getID();
		}		
		
		if(spCareType.getAdapter() != null)
			careType = ((KeyValuePairModel)spCareType.getSelectedItem()).getsID();
		desc = txtDesc.getText().toString();
		
		if(radFinish.isChecked())
			status = 1;
		else
			status = 0;
		
		if(checkForUpdate(careType, desc)){
			new UpdateTodoListDetail(mContext, this, userName, todoListID, careType, desc, status);
		}
	}
	
}
