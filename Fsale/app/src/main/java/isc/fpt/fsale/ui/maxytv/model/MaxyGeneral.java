package isc.fpt.fsale.ui.maxytv.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.model.Mac;
import isc.fpt.fsale.model.MacOTT;

public class MaxyGeneral implements Parcelable {

    private int PackageID;
    private String Package;
    private int NumBox;
    private int DeviceTotal;
    private int PrepaidTotal;
    private int Total;
    private int RequestDrillWall;
    private int UseCombo;
    private int Status;
    private int PromotionID;
    private String PromotionDesc;
    private int PromotionType;
    private int PrepaidMonth;
    private int BonusMonth;
    private int ServiceCode;
    private int Price;
    private int TypeDeployID;
    private String TypeDeployName;
    private int TypeSetupID;
    private String TypeSetupName;
    private List<MacOTT> listMACOTT;

    private boolean isSelected = false;


    public MaxyGeneral(int packageID, String aPackage, int numBox, int deviceTotal, int prepaidTotal, int total, int requestDrillWall, int useCombo, int status, int promotionID, String promotionDesc, int promotionType, int prepaidMonth, int bonusMonth, int serviceCode, int price, int typeDeployID, String typeDeployName,
                       int typeSetupID, String typeSetupName, List<MacOTT> listMACOTT, boolean isSelected) {
        PackageID = packageID;
        Package = aPackage;
        NumBox = numBox;
        DeviceTotal = deviceTotal;
        PrepaidTotal = prepaidTotal;
        Total = total;
        RequestDrillWall = requestDrillWall;
        UseCombo = useCombo;
        Status = status;
        PromotionID = promotionID;
        PromotionDesc = promotionDesc;
        PromotionType = promotionType;
        PrepaidMonth = prepaidMonth;
        BonusMonth = bonusMonth;
        ServiceCode = serviceCode;
        Price = price;
        TypeDeployID = typeDeployID;
        TypeDeployName = typeDeployName;
        TypeSetupID = typeSetupID;
        TypeSetupName = typeSetupName;
        this.listMACOTT = listMACOTT;
        this.isSelected = isSelected;
    }

    public MaxyGeneral() {

    }


    protected MaxyGeneral(Parcel in) {
        PackageID = in.readInt();
        Package = in.readString();
        NumBox = in.readInt();
        DeviceTotal = in.readInt();
        PrepaidTotal = in.readInt();
        Total = in.readInt();
        RequestDrillWall = in.readInt();
        UseCombo = in.readInt();
        Status = in.readInt();
        PromotionID = in.readInt();
        PromotionDesc = in.readString();
        PromotionType = in.readInt();
        PrepaidMonth = in.readInt();
        BonusMonth = in.readInt();
        ServiceCode = in.readInt();
        Price = in.readInt();
        TypeDeployID = in.readInt();
        TypeDeployName = in.readString();
        TypeSetupID = in.readInt();
        TypeSetupName = in.readString();
        listMACOTT = in.createTypedArrayList(MacOTT.CREATOR);
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(PackageID);
        dest.writeString(Package);
        dest.writeInt(NumBox);
        dest.writeInt(DeviceTotal);
        dest.writeInt(PrepaidTotal);
        dest.writeInt(Total);
        dest.writeInt(RequestDrillWall);
        dest.writeInt(UseCombo);
        dest.writeInt(Status);
        dest.writeInt(PromotionID);
        dest.writeString(PromotionDesc);
        dest.writeInt(PromotionType);
        dest.writeInt(PrepaidMonth);
        dest.writeInt(BonusMonth);
        dest.writeInt(ServiceCode);
        dest.writeInt(Price);
        dest.writeInt(TypeDeployID);
        dest.writeString(TypeDeployName);
        dest.writeInt(TypeSetupID);
        dest.writeString(TypeSetupName);
        dest.writeTypedList(listMACOTT);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MaxyGeneral> CREATOR = new Creator<MaxyGeneral>() {
        @Override
        public MaxyGeneral createFromParcel(Parcel in) {
            return new MaxyGeneral(in);
        }

        @Override
        public MaxyGeneral[] newArray(int size) {
            return new MaxyGeneral[size];
        }
    };

    public int getPackageID() {
        return PackageID;
    }

    public void setPackageID(int packageID) {
        PackageID = packageID;
    }

    public String getPackage() {
        return Package;
    }

    public void setPackage(String aPackage) {
        Package = aPackage;
    }

    public int getNumBox() {
        return NumBox;
    }

    public void setNumBox(int numBox) {
        NumBox = numBox;
    }

    public int getDeviceTotal() {
        return DeviceTotal;
    }

    public void setDeviceTotal(int deviceTotal) {
        DeviceTotal = deviceTotal;
    }

    public int getPrepaidTotal() {
        return PrepaidTotal;
    }

    public void setPrepaidTotal(int prepaidTotal) {
        PrepaidTotal = prepaidTotal;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

    public int getRequestDrillWall() {
        return RequestDrillWall;
    }

    public void setRequestDrillWall(int requestDrillWall) {
        RequestDrillWall = requestDrillWall;
    }

    public int getUseCombo() {
        return UseCombo;
    }

    public void setUseCombo(int useCombo) {
        UseCombo = useCombo;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int promotionID) {
        PromotionID = promotionID;
    }

    public String getPromotionDesc() {
        return PromotionDesc;
    }

    public void setPromotionDesc(String promotionDesc) {
        PromotionDesc = promotionDesc;
    }

    public int getPromotionType() {
        return PromotionType;
    }

    public void setPromotionType(int promotionType) {
        PromotionType = promotionType;
    }

    public int getPrepaidMonth() {
        return PrepaidMonth;
    }

    public void setPrepaidMonth(int prepaidMonth) {
        PrepaidMonth = prepaidMonth;
    }

    public int getBonusMonth() {
        return BonusMonth;
    }

    public void setBonusMonth(int bonusMonth) {
        BonusMonth = bonusMonth;
    }

    public int getServiceCode() {
        return ServiceCode;
    }

    public void setServiceCode(int serviceCode) {
        ServiceCode = serviceCode;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getTypeDeployID() {
        return TypeDeployID;
    }

    public void setTypeDeployID(int typeDeployID) {
        TypeDeployID = typeDeployID;
    }

    public String getTypeDeployName() {
        return TypeDeployName;
    }

    public void setTypeDeployName(String typeDeployName) {
        TypeDeployName = typeDeployName;
    }

    public int getTypeSetupID() {
        return TypeSetupID;
    }

    public void setTypeSetupID(int typeSetupID) {
        TypeSetupID = typeSetupID;
    }

    public String getTypeSetupName() {
        return TypeSetupName;
    }

    public void setTypeSetupName(String typeSetupName) {
        TypeSetupName = typeSetupName;
    }

    public List<MacOTT> getListMACOTT() {
        return listMACOTT;
    }

    public void setListMACOTT(List<MacOTT> listMACOTT) {
        this.listMACOTT = listMACOTT;
    }

    public static Creator<MaxyGeneral> getCREATOR() {
        return CREATOR;
    }

    public MaxyGeneral(int packageID, String aPackage, int numBox, int deviceTotal, int prepaidTotal, int total, int requestDrillWall, int useCombo, int status, int promotionID, String promotionDesc, int promotionType, int prepaidMonth, int bonusMonth, int serviceCode, int price, boolean isSelected) {
        PackageID = packageID;
        Package = aPackage;
        NumBox = numBox;
        DeviceTotal = deviceTotal;
        PrepaidTotal = prepaidTotal;
        Total = total;
        RequestDrillWall = requestDrillWall;
        UseCombo = useCombo;
        Status = status;
        PromotionID = promotionID;
        PromotionDesc = promotionDesc;
        PromotionType = promotionType;
        PrepaidMonth = prepaidMonth;
        BonusMonth = bonusMonth;
        ServiceCode = serviceCode;
        Price = price;
        this.isSelected = isSelected;
    }

    public JSONObject toJsonObjectMaxyGeneral() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArrayMAC = new JSONArray();
        if (getListMACOTT() != null) {
            for (MacOTT item : getListMACOTT()) {
                jsonArrayMAC.put(item.toJSONObject());
            }
        }
        try {
            jsonObject.put("PackageID", getPackageID());
            jsonObject.put("Package", getPackage());
            jsonObject.put("DeviceTotal", getDeviceTotal());
            jsonObject.put("PrepaidTotal", getPrepaidTotal());
            jsonObject.put("Total", getTotal());
            jsonObject.put("UseCombo", getUseCombo());
            jsonObject.put("PromotionID", getPromotionID());
            jsonObject.put("PromotionDesc", getPromotionDesc());
            jsonObject.put("PromotionType", getPromotionType());
            jsonObject.put("PrepaidMonth", getPrepaidMonth());
            jsonObject.put("BonusMonth", getBonusMonth());
            jsonObject.put("ServiceCode", getServiceCode());
            jsonObject.put("Price", getPrice());
            jsonObject.put("TypeDeployID", getTypeDeployID());
            jsonObject.put("TypeDeployName", getTypeDeployName());
            jsonObject.put("NumBox", getNumBox());
            jsonObject.put("TypeSetupID", getTypeSetupID());
            jsonObject.put("TypeSetupName", getTypeSetupName());
            jsonObject.put("listMACOTT", jsonArrayMAC);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
