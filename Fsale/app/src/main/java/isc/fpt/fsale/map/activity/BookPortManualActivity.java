package isc.fpt.fsale.map.activity;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import isc.fpt.fsale.action.GetIPV4Action;
import isc.fpt.fsale.action.GetListBookPortAction;
import isc.fpt.fsale.action.GetLocationBranchPOPAction;
import isc.fpt.fsale.action.RecoverRegistrationAction;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.action.GetGeocoder;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.map.utils.MapConstants;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import isc.fpt.fsale.utils.MyApp.TrackerName;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class BookPortManualActivity extends FragmentActivity implements OnMapReadyCallback {
    private Spinner spTDType;
    private EditText txtTDName;
    private Button btnRecoveryTD, btnGetTDList, btnMapMode;
    private ImageView imgTDInfo;
    private List<Marker> markerList = new ArrayList<>();
    private GoogleMap mMap;
    private Location mapLocation, locationDevice;
    private Marker customerMarker;
    private RegistrationDetailModel mRegister;
    private WeakHashMap<Marker, RowBookPortModel> hashTDByName;
    private final int zoomTD = 18;
    private RowBookPortModel currentTD;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_map_bookport_activity));
        setContentView(R.layout.activity_bookport_manual);
        initView();
        initViewEvent();
        setUpMapIfNeeded();
        getDataFromIntent();
    }

    private void initViewEvent() {
        imgTDInfo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getTDInfo(currentTD);
            }
        });
        btnGetTDList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getTDList();
            }
        });
        btnRecoveryTD.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmToReconvertPort();
            }
        });
        btnMapMode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // if current map mode is satellite: change to normal
                if (mMap.getMapType() == MAP_TYPE_SATELLITE) {
                    btnMapMode.setText(getResources().getString(
                            R.string.lbl_satellite));
                    MapConstants.IS_SATELLITE = false;
                    mMap.setMapType(MAP_TYPE_NORMAL);
                }
                // if current map mode is normal: change to satellite
                else {
                    btnMapMode.setText(getResources().getString(
                            R.string.lbl_map));
                    MapConstants.IS_SATELLITE = true;
                    mMap.setMapType(MAP_TYPE_SATELLITE);
                }
            }
        });
    }

    private void initView() {
        spTDType = (Spinner) findViewById(R.id.sp_port_type);
        txtTDName = (EditText) findViewById(R.id.txt_tapdiem);
        imgTDInfo = (ImageView) findViewById(R.id.btn_find_list_port);
        btnGetTDList = (Button) findViewById(R.id.btn_show_list_port);
        btnRecoveryTD = (Button) findViewById(R.id.btn_recover_registration);
        btnMapMode = (Button) findViewById(R.id.btn_map_mode);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    //lấy thông tin PĐK từ Intent
    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            mRegister = intent.getParcelableExtra(Constants.MODEL_REGISTER);
            if (mRegister != null) {
                txtTDName.setText(mRegister.getTDName());
                if (!mRegister.getTDName().trim().equals("")) {
                    btnRecoveryTD.setVisibility(View.VISIBLE);
                } else {
                    btnRecoveryTD.setVisibility(View.GONE);
                }
            }
        }
        initSpTDType();
        getTDList();
    }

    // CÀI ĐẶT MAP
    private void setUpMapIfNeeded() {
        if (mMap != null)
            setUpMap();
        if (mMap == null) {
            SupportMapFragment mapFrag = (SupportMapFragment) this
                    .getSupportFragmentManager().findFragmentById(R.id.map);
            mapFrag.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                LinearLayout info = new LinearLayout(BookPortManualActivity.this);
                info.setOrientation(LinearLayout.VERTICAL);
                TextView title = new TextView(BookPortManualActivity.this);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());
                TextView snippet = new TextView(BookPortManualActivity.this);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());
                info.addView(title);
                info.addView(snippet);
                return info;
            }
        });
        if (mMap != null)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                setUpMap();
            } else {
                Toast.makeText(this, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }
    }

    private void setUpMap() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, 2);
            }
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        // An nut dinh vi
        mMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                try {
                    getCurrentLocation();
                } catch (Exception e) {
                    Toast.makeText(BookPortManualActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
        mMap.setOnMapLongClickListener(new OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latlng) {
                if (customerMarker != null)
                    customerMarker.remove();
                customerMarker = drawMarkerOnMap(
                        latlng,
                        getString(R.string.title_cuslocation),
                        R.drawable.icon_home,
                        true,
                        null,
                        true,
                        zoomTD
                );
            }
        });
        // Di chuyên marker
        mMap.setOnMarkerDragListener(new OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                customerMarker = marker;
            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }
        });
        // Khi nhấn vào marker
        mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                try {
                    RowBookPortModel td = hashTDByName.get(marker);
                    if (td != null) {
                        currentTD = td;
                        txtTDName.setText(td.getTDName());
                        if (td.getTDName().contains("/"))
                            spTDType.setSelection(spTDType.getCount() - 1, true);
                        try {
                            if (!td.getAddress().trim().equals("")) {
                                String s = "Địa chỉ: " + td.getAddress();
                                s += td.getDistance().trim().equals("") ? ""
                                        : "\n Cách: " + td.getDistance() + "m";
                                Toast.makeText(BookPortManualActivity.this, s, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                return false;
            }
        });

        if (mMap != null) {
            getCurrentLocation();
        }
    }

    private void getCurrentLocation() {
        if (mMap != null) {
            try {
                GPSTracker gps = new GPSTracker(this);
                Location location = gps.getLocation(true);
                if (location == null)
                    location = gps.getLocation(false);
                if ((location == null && mapLocation != null))
                    location = mapLocation;
                if (Common.distanceTwoLocation(location, mapLocation) > 10)
                    location = mapLocation;
                locationDevice = location;
                if (location != null) {
                    LatLng curLocation = new LatLng(location.getLatitude(),
                            location.getLongitude());

                    if (customerMarker != null)
                        customerMarker.remove();
                    customerMarker = drawMarkerOnMap(
                            curLocation,
                            getString(R.string.title_cuslocation),
                            R.drawable.icon_home,
                            true,
                            null,
                            true,
                            zoomTD
                    );
                } else {
                    new GetGeocoder(this, Constants.LOCATION_NAME).execute();
                }
            } catch (Exception e) {
                MyApp myApp = ((MyApp) getApplicationContext());
                if (myApp != null) {
                    Crashlytics.logException(new Exception(Constants.USERNAME + "," + "không lấy được vị trí" + locationDevice.toString() + " thiết bị"));
                }
                e.printStackTrace();
            }
        }
    }

    //Khởi tạo loại tập điểm
    private void initSpTDType() {
        int TDType = -1;
        String groupPoint = "";
        if (mRegister != null) {
            if (mRegister.getBookPortType() >= 0)
                TDType = mRegister.getBookPortType();
            groupPoint = mRegister.getTDName();
        }
        KeyValuePairModel itemADSL = new KeyValuePairModel(0, "ADSL");
        KeyValuePairModel itemFTTH = new KeyValuePairModel(1, "FTTH");
        KeyValuePairModel itemFTTHNew = new KeyValuePairModel(2, "FTTHNew");
        ArrayList<KeyValuePairModel> lstBookPortType = new ArrayList<>();
        lstBookPortType.add(itemADSL);
        lstBookPortType.add(itemFTTH);
        lstBookPortType.add(itemFTTHNew);
        if (TDType == 0)
            lstBookPortType.remove(itemFTTH);
        if (TDType == 1)
            lstBookPortType.remove(itemADSL);
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this,
                R.layout.my_spinner_style, lstBookPortType, Gravity.LEFT);
        spTDType.setAdapter(adapter);
        try {
            if (groupPoint != null && groupPoint.contains("/")) {
                spTDType.setSelection(lstBookPortType.size() - 1, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //GET/LOAD DS TẬP ĐIỂM
    private void getTDList() {
        if (customerMarker != null) {
            String lat = String.valueOf(customerMarker.getPosition().latitude);
            String longitude = String
                    .valueOf(customerMarker.getPosition().longitude);
            String strContractLatLng = "(" + lat + "," + longitude + ")";
            if (!strContractLatLng.equals("") && !strContractLatLng.replace(",", "").equals("")) {
                int tdType = ((KeyValuePairModel) spTDType.getSelectedItem()).getID();
                new GetListBookPortAction(this, String.valueOf(mRegister.getID()), tdType, strContractLatLng);
            } else {
                Common.alertDialog(getString(R.string.msg_unable_get_location), this);
            }
        }
    }

    public void loadTDList(List<RowBookPortModel> lst) {
        if (markerList == null)
            markerList = new ArrayList<>();
        if (hashTDByName == null)
            hashTDByName = new WeakHashMap<>();
        clearMarkerHashMap();
        drawMarker(lst);
    }

    private void clearMarkerHashMap() {
        for (Marker item : markerList) {
            item.remove();
        }
        markerList.clear();
        hashTDByName.clear();
    }

    // VẼ/QUẢN LÝ MARKER
    private void drawMarker(List<RowBookPortModel> lst) {
        if (lst != null && lst.size() > 0) {
            for (RowBookPortModel item : lst) {
                String title = item.getTDName();
                LatLng latlng = MapCommon.ConvertStrToLatLng(item
                        .getLatlngPort());
                if (latlng != null) {
                    String snippet = " Port trống: " + item.getPortFree() + "\n Technology: " + item.getTechnology();
                    int drawableID;
                    if (item.getPortFree() == 1) {
                        drawableID = R.drawable.ic_marker_red;
                    } else {
                        if (item.getPortFreeRatio() <= 0) {
                            drawableID = R.drawable.ic_marker_orange;
                        } else if (item.getPortFreeRatio() <= 0.25) {
                            drawableID = R.drawable.ic_marker_yellow;
                        } else {
                            drawableID = R.drawable.ic_marker_blue;
                        }
                    }

                    Marker marker = drawMarkerOnMap(latlng, title, drawableID,
                            false, snippet, false, zoomTD);
                    if (marker != null)
                        addHashMaker(marker, item);
                }
            }
        }
    }

    private void addHashMaker(Marker marker, RowBookPortModel item) {
        markerList.add(marker);
        hashTDByName.put(marker, item);
    }

    private Marker drawMarkerOnMap(LatLng latlng, String title, int dr,
                                   boolean isDraggable, String snippet, boolean isShowInfoWindow,
                                   int zoomSize) {
        Marker marker = null;
        try {
            marker = MapCommon.addMarkerOnMap(mMap, title, latlng, dr,
                    isDraggable);
            marker.setSnippet(snippet);
            if (isShowInfoWindow)
                marker.showInfoWindow();
            MapCommon.animeToLocation(mMap, latlng);
            MapCommon.setMapZoom(mMap, zoomSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return marker;
    }

    //LẤY THÔNG TIN TẬP ĐIỂM
    @SuppressLint("DefaultLocale")
    private void getTDInfo(RowBookPortModel TDSelecedInfo) {
        String stdName = txtTDName.getText().toString().trim().toLowerCase();
        stdName = stdName.replace(" ", "");
        Location locationMarker = new Location("MarkerLocation");
        if (customerMarker != null) {
            locationMarker.setLatitude(customerMarker.getPosition().latitude);
            locationMarker.setLongitude(customerMarker.getPosition().longitude);
        }

        if (!stdName.trim().equals("")) {
            int TDKind = 0;
            try {
                TDKind = ((KeyValuePairModel) spTDType.getSelectedItem()).getID();
            } catch (Exception e) {
                e.printStackTrace();
            }
            RowBookPortModel selectedItem;
            if (TDSelecedInfo != null) {
                if (TDSelecedInfo.getTDName().trim().toLowerCase().equals(stdName))
                    selectedItem = TDSelecedInfo;
                else
                    selectedItem = new RowBookPortModel(
                            0,
                            0,
                            0,
                            0,
                            "",
                            0,
                            0,
                            0,
                            stdName,
                            String.valueOf(TDKind),
                            TDKind,
                            "",
                            "",
                            "",
                            0
                    );
            } else {
                selectedItem = new RowBookPortModel(
                        0,
                        0,
                        0,
                        0,
                        "",
                        0,
                        0,
                        0,
                        stdName,
                        String.valueOf(TDKind),
                        TDKind,
                        "",
                        "",
                        "",
                        0
                );
            }
            if (mRegister != null) {
                new GetLocationBranchPOPAction(
                        getSupportFragmentManager(),
                        this,
                        selectedItem,
                        mRegister.getRegCode(),
                        String.valueOf(mRegister.getID()),
                        mRegister,
                        locationDevice,
                        locationMarker
                );
            } else {
                Common.alertDialog("Thông tin PĐK không hợp lệ!", this);
            }
        } else {
            txtTDName.setError(getString(R.string.msg_td_empty));
        }
    }

    // THU HỒI PORT
    private void confirmToReconvertPort() {
        if (mRegister != null) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.title_notification))
                    .setMessage(
                            getString(R.string.msg_confirm_recover_registration))
                    .setPositiveButton(R.string.lbl_ok,
                            new Dialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    recoveryPort();
                                    dialog.cancel();
                                }
                            })
                    .setNeutralButton(R.string.lbl_cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    private void recoveryPort() {
        if (mRegister != null) {
            GetIPV4Action NewIP = new GetIPV4Action();
            String IP = NewIP.GetIP();
            int type = 0;
            if (mRegister.getTDName() != null) {
                if (mRegister.getTDName().contains("/"))
                    type = 3;
                else {
                    KeyValuePairModel item = (KeyValuePairModel) spTDType
                            .getItemAtPosition(0);
                    type = item.getID() + 1;
                }
            }
            String[] paramsValue = new String[]{String.valueOf(type),
                    mRegister.getRegCode(), Constants.USERNAME, IP};
            new RecoverRegistrationAction(this, paramsValue);
            switch (type) {
                case 1:
                    onClickTracker(
                            "Thu hồi port ADSL",
                            getString(R.string.lbl_screen_name_dialog_book_port_adsl));
                    break;
                case 2:
                    onClickTracker(
                            "Thu hồi port FTTH",
                            getString(R.string.lbl_screen_name_dialog_book_port_ftth));
                    break;
                case 3:
                    onClickTracker(
                            "Thu hồi port FTTH New",
                            getString(R.string.lbl_screen_name_dialog_book_port_ftth_new));
                    break;
                default:
                    break;
            }

        }

    }

    private void onClickTracker(String label, String screenName) {
        try {
            // Get tracker.
            Tracker t = ((MyApp) this.getApplication())
                    .getTracker(TrackerName.APP_TRACKER);
            // Set screen name.
            t.setScreenName(screenName + ".onClick()");
            // Send a screen view.
            t.send(new HitBuilders.ScreenViewBuilder().build());

            // This event will also be sent with the most recently set screen
            // name.
            // Build and send an Event.
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("BOOK_PORT_ONCLICK")
                    .setAction("onClick")
                    .setLabel(label)
                    .build());

            // Clear the screen name field when we're done.
            t.setScreenName(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    public void setCityLocation(LatLng cityLocation) {
        if (cityLocation != null) {
            String strContractLatLng = cityLocation.latitude + "," + cityLocation.longitude;
            if (customerMarker != null)
                customerMarker.remove();
            int zoomCustomer = 11;
            customerMarker = drawMarkerOnMap(
                    cityLocation,
                    getString(R.string.title_cuslocation),
                    R.drawable.icon_home,
                    true,
                    null,
                    true,
                    zoomCustomer
            );
            SharedPreferences sharedPref = this
                    .getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(this
                            .getString(R.string.lbl_Shared_Preferences_city_Lat_Lng),
                    strContractLatLng);
            editor.apply();
        }
    }

    private void callBackPress() {
        this.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                Dialog dialog = new Dialog(this);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getResources().getString(R.string.error_permission_denied))
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        callBackPress();
                                    }
                                })
                        .create();
                dialog.show();
                return;
            }
        }
        if (requestCode == 2) {
            setUpMap();
        }
    }
}
