package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.model.GetAllPromotionPackage;
import isc.fpt.fsale.ui.fpt_camera.model.PromotionDetail;
import isc.fpt.fsale.ui.fpt_camera.promotion_detail.PromotionDetailActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 10,September,2019
 */
public class GetPromotionFptCamera implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private List<PromotionDetail> mList;

    public GetPromotionFptCamera(Context mContext, GetAllPromotionPackage mObject) {
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.msg_pd_update);
        String GET_ALL_PROMOTION = "GetAllPromoPackage";
        CallServiceTask service = new CallServiceTask(mContext, GET_ALL_PROMOTION, mObject.toJsonObject(),
                Services.JSON_POST_OBJECT, message, GetPromotionFptCamera.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<PromotionDetail> resultObject = new WSObjectsModel<>(jsObj, PromotionDetail.class);
                mList = resultObject.getListObject();
                //callback to activity
                if (mList.size() == 0) return;
                if (mContext != null) {
                    ((PromotionDetailActivity) mContext).loadPromotion(mList);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
