package isc.fpt.fsale.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.BaseActivity;
import isc.fpt.fsale.utils.Common;

/**
 * Created by haulc3 on 19,July,2019
 */
public abstract class BaseFragment extends Fragment {
    @Nullable
    protected View layoutContainer;
    private BaseActivitySecond mActivitySecond;
    private BaseActivity mActivity;
    private View mView;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivitySecond) {
            this.mActivitySecond = (BaseActivitySecond) context;
        }else if(context instanceof BaseActivity){
            this.mActivity = (BaseActivity) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(getFragmentLayout(), container, false);
            layoutContainer = mView.findViewById(R.id.container);
            initView(mView);
        } else {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null) {
                parent.removeView(mView);
            }
        }
        return mView;
    }

    protected abstract void initView(View mView);

    protected abstract void initEvent();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(layoutContainer != null){
            hiddenKeySoft(layoutContainer);
        }
        initEvent();
        bindData();
    }

    protected abstract void bindData();

    protected abstract int getFragmentLayout();

    public void showError(String errorMessage) {
        if(mActivity != null){
            Common.alertDialog(errorMessage, mActivity);
        }else if(mActivitySecond != null){
            Common.alertDialog(errorMessage, mActivitySecond);
        }
    }

    public void hiddenKeySoft(View view) {
        if(mActivity != null){
            mActivity.hiddenKeySoft(view);
        }else if(mActivitySecond != null){
            mActivitySecond.hiddenKeySoft(view);
        }
    }
}
