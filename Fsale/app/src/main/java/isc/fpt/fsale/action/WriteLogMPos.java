package isc.fpt.fsale.action;

import isc.fpt.fsale.database.mPOSLogTable;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.model.mPOSLogModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

public class WriteLogMPos extends AsyncTask<Void, Void, String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "WriteLogMPos", TAG_PARAM_LOGS = "Logs";
	private List<mPOSLogModel> Logs = new ArrayList<mPOSLogModel>();
	
	public WriteLogMPos(Context context, List<mPOSLogModel> Logs) {	
		mContext = context;		
		this.Logs = Logs;
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
	}
	
	@Override
	protected String doInBackground(Void...voids) {
		// TODO Auto-generated method stub		
		try {
			JSONArray arr = new JSONArray();
			for(mPOSLogModel item: this.Logs){
				arr.put(item.toJSONObject());				
			}
			JSONObject json = new JSONObject();
			json.put(TAG_PARAM_LOGS, arr);
			return Services.postJsonObject(TAG_METHOD_NAME, json, mContext);
		} catch (JSONException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		return null;
		//return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);		
		try {
			if (result != null && Common.jsonObjectValidate(result)) {
			List<UpdResultModel> lst = null;
			boolean isError = false;
			JSONObject jsObj;
			jsObj = new JSONObject(result);
			if(jsObj != null){
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(jsObj, UpdResultModel.class);
				if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//ServiceType Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError){
				 updateClientDB(lst);
			 }
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
	}
	

	private void updateClientDB(List<UpdResultModel> lst ){
		try {
			if(lst != null && lst.size() >0){
				mPOSLogTable table = new mPOSLogTable(mContext);
				for(UpdResultModel item : lst){
					if(!Common.isNullOrEmpty(item.getClientID())){
						table.delete(item.getClientID());
					}
				}
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		
	}
		
	

}
