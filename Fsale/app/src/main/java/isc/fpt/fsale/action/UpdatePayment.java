package isc.fpt.fsale.action;

import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ConfirmPaymentActivity;
import isc.fpt.fsale.model.PaymentInformationPOST;
import isc.fpt.fsale.model.PaymentInformationResult;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

/**
 * Created by HCM.TUANTT14 on 5/14/2018.
 */
// API cập nhật khoản thu thanh toán online bán mới
public class UpdatePayment implements AsyncTaskCompleteListener<String> {
    //Declares variables
    private int regID = 0;
    private String Result;
    private PaymentInformationResult payInfoPublic;
    private Context mContext;
    private RegistrationDetailModel registrationDetailModel;

    public UpdatePayment(Context mContext, PaymentInformationPOST paymentInformationPOST,
                         PaymentInformationResult payInfoPublic, RegistrationDetailModel registrationDetailModel) {
        this.mContext = mContext;
        this.payInfoPublic = payInfoPublic;
        this.registrationDetailModel = registrationDetailModel;
        String[] paramNames = {"SaleName", "RegCode"};
        String[] paramValues = {paymentInformationPOST.getSaleName(), paymentInformationPOST.getRegCode()};
        String message = mContext.getResources().getString(
                R.string.msg_process_update_payment);
        try {
            String UPDATE_PAYMENT = "UpdatePayment";
            CallServiceTask service = new CallServiceTask(mContext, UPDATE_PAYMENT,
                    paramNames, paramValues, Services.JSON_POST, message, UpdatePayment.this);
            service.execute();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        if (result != null && Common.jsonObjectValidate(result)) {
            try {
                JSONObject jsObj = JSONParsing.getJsonObj(result);
                jsObj = jsObj.getJSONObject("UpdatePaymentResult");
                String TAG_ERROR_CODE = "ErrorCode";
                if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {//Check service Error
                    String TAG_RESULT_LIST = "ListObject";
                    JSONArray resultArray = jsObj.getJSONArray(TAG_RESULT_LIST);
                    for (int i = 0; i < resultArray.length(); i++) {
                        JSONObject item = resultArray.getJSONObject(i);
                        String TAG_RESULT_ID = "ResultID";
                        if (item.has(TAG_RESULT_ID))
                            regID = Integer.parseInt(item.getString(TAG_RESULT_ID));
                        String TAG_RESULT = "Result";
                        if (item.has(TAG_RESULT))
                            Result = item.getString(TAG_RESULT);
                    }
                    //regID = 0 là thành công
                    if (regID == 0) {
                        if (payInfoPublic != null) {
                            Intent confirmIntent = new Intent(mContext, ConfirmPaymentActivity.class);
                            confirmIntent.putExtra("REGISTER", registrationDetailModel);
                            confirmIntent.putExtra("PAID_TYPE_VALUE", payInfoPublic.getPaidTypeName());
                            confirmIntent.putExtra("CONFIRM_PAYMENT_INFO", payInfoPublic);
                            mContext.startActivity(confirmIntent);
                        } else {
                            Common.showToast(mContext, mContext.getResources().getString(R.string.info_detail_registration_not_available));
                        }
                    } else {
                        Common.alertDialog(Result, mContext);
                    }
                } else {
                    String TAG_ERROR = "Error";
                    Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
            }
        } else
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
    }
}
