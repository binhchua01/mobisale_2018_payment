package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ListObjectModel;
import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListObjectAdapter  extends BaseAdapter{
	private List<ListObjectModel> mList;	
	private Context mContext;
	
	public ListObjectAdapter(Context context, List<ListObjectModel> lst){
		this.mContext = context;
		this.mList = lst;	
	}

	@Override
	public int getCount() {
		return mList != null ? mList.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return mList != null ? mList.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListObjectModel item = mList.get(position);		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_list_object, null);
		}
		
		TextView lblRowNumber = (TextView) convertView.findViewById(R.id.lbl_row_number);
		lblRowNumber.setText(String.valueOf(position + 1));
		
		TextView lblFullName = (TextView) convertView.findViewById(R.id.lbl_full_name);
		if(item.getFullName() != null)
			lblFullName.setText(item.getFullName());		
		
		TextView lblContract = (TextView) convertView.findViewById(R.id.lbl_contract);
		if(item.getContract() != null)
			lblContract.setText(item.getContract());
		
		TextView lblLocalTypeName = (TextView) convertView.findViewById(R.id.lbl_local_type_name);
		if(item.getLocalTypeName() != null)
			lblLocalTypeName.setText(item.getLocalTypeName());
		
		TextView lblAddress = (TextView) convertView.findViewById(R.id.lbl_address);
		if(item.getAddress() != null)
			lblAddress.setText(item.getAddress());
		
		TextView lblServiceType = (TextView) convertView.findViewById(R.id.lbl_service_type_name);
		if(item.getServiceTypeName() != null)
			lblServiceType.setText(item.getServiceTypeName());
		
		TextView lblComboStatusName = (TextView) convertView.findViewById(R.id.lbl_combo_status_name);
		lblComboStatusName.setText(item.getComboStatusName());
		
		return convertView;
	}

}
