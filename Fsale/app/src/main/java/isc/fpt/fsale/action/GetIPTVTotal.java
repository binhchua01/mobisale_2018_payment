package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.IpTvTotal;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// API tính tổng tiền IPTV
public class GetIPTVTotal implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterStep4 fragment;
    private String GET_IP_TV_V2_TOTAL = "GetIPTVTotal_V2";

    // Ban moi
    public GetIPTVTotal(Context context, FragmentRegisterStep4 fragment, RegistrationDetailModel mRegister) {
        this.mContext = context;
        this.fragment = fragment;
        String message = mContext.getResources().getString(R.string.msg_pd_get_ip_tv_total);
        CallServiceTask service = new CallServiceTask(mContext, GET_IP_TV_V2_TOTAL, mRegister.toJSONObjectGetIpTvTotal(),
                Services.JSON_POST_OBJECT, message, GetIPTVTotal.this);
        service.execute();
    }

    // Ban them
    public GetIPTVTotal(Context context, RegistrationDetailModel mRegister, String phone1,
                        String contract, String regCode, int IPTVPromotionType, int IPTVPromotionTypeBoxOrder) {
        this.mContext = context;
        if (mRegister == null) {
            mRegister = new RegistrationDetailModel();
        }
        String message = mContext.getResources().getString(R.string.msg_pd_get_ip_tv_total);
        CallServiceTask service = new CallServiceTask(mContext, GET_IP_TV_V2_TOTAL,
                mRegister.toJSONObjectGetIpTvTotal(phone1, contract, regCode, IPTVPromotionType, IPTVPromotionTypeBoxOrder),
                Services.JSON_POST_OBJECT, message, GetIPTVTotal.this);
        service.execute();
    }

    public void handleUpdateRegistration(String json) {
        JSONObject jsObj = getJsonObject(json);
        if (Common.jsonObjectValidate(json)) {
            try {
                assert jsObj != null;
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                String TAG_ERROR_CODE = "ErrorCode";
                if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                    String TAG_OBJEC_LIST = "ListObject";
                    JSONArray array = jsObj.getJSONArray(TAG_OBJEC_LIST);
                    JSONObject item = array.getJSONObject(0);
                    int DeviceTotal = 0, PrepaidTotal = 0, Total = 0, monthlyTotal = 0;
                    String TAG_DEVICE_TOTAL = "DeviceTotal";
                    if (item.has(TAG_DEVICE_TOTAL))
                        DeviceTotal = item.getInt(TAG_DEVICE_TOTAL);
                    String TAG_PREPAID_TOTAL = "PrepaidTotal";
                    if (item.has(TAG_PREPAID_TOTAL))
                        PrepaidTotal = item.getInt(TAG_PREPAID_TOTAL);
                    String TAG_TOTAL = "Total";
                    if (item.has(TAG_TOTAL))
                        Total = item.getInt(TAG_TOTAL);
                    /**/
                    String TAG_MONTHLY_TOTAL = "MonthlyTotal";
                    if (item.has(TAG_MONTHLY_TOTAL))
                        monthlyTotal = item.getInt(TAG_MONTHLY_TOTAL);
                    try {
                        if (mContext != null)
                            ((RegisterContractActivity) mContext)
                                    .getTotalFragment().loadIpTvTotal(DeviceTotal, PrepaidTotal, Total);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    String TAG_ERROR = "Error";
                    Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            return JSONParsing.getJsonObj(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onTaskComplete(String result) {
        if (fragment != null) {//ban moi
            try {
                List<IpTvTotal> lst = new ArrayList<>();
                if (Common.jsonObjectValidate(result)) {
                    JSONObject jsObj = new JSONObject(result);
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        lst.add(new Gson().fromJson(jsonArray.get(i).toString(), IpTvTotal.class));
                    }
                    //callback to activity
                    String ERROR_CODE = "ErrorCode";
                    String ERROR = "Error";
                    if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                        Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                    } else {
                        if (fragment != null) {
                            fragment.loadIpTvPrice(lst.get(0));
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {//ban them
            handleUpdateRegistration(result);
        }
    }
}
