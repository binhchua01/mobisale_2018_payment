package isc.fpt.fsale.action;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.AutoCreatePreCheckListActivity;
import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.model.AutoPreCheckListInfo;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/*
 * Created by HCM.TUANTT14 on 7/4/2018.
 * API kiểm tra hợp đồng có tạo được checklist tự động
 */

public class AutoPreCheckListCheckCL implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private CreatePreChecklistActivity createPreChecklistActivity;
    private List<AutoPreCheckListInfo> lst;
    private ObjectModel objectModel;

    public AutoPreCheckListCheckCL(Context context, CreatePreChecklistActivity activity,
                                   ObjectModel objectModel, String[] paramsValue) {
        this.mContext = context;
        this.createPreChecklistActivity = activity;
        this.objectModel = objectModel;
        String[] arrParamName = new String[]{"ObjID", "Contract", "UserName"};
        String message = mContext.getResources().getString(R.string.msg_process_auto_create_pre_checklist);
        String AUTO_PRE_CHECK_LIST_CHECK_CL = "AutoPreCheckList_CheckCL";
        CallServiceTask service = new CallServiceTask(mContext, AUTO_PRE_CHECK_LIST_CHECK_CL,
                arrParamName, paramsValue, Services.JSON_POST, message, AutoPreCheckListCheckCL.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        String resultText;
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                String TAG_RESULT = "Result";
                resultText = jsObj.getString(TAG_RESULT);
                String TAG_RESULT_ID = "ResultID";
                if (jsObj.has(TAG_RESULT_ID) && jsObj.getInt(TAG_RESULT_ID) < 0) {
                    Common.alertDialog(resultText, mContext);
                } else {
                    int resultID;
                    if (jsObj.has(TAG_RESULT_ID) && (resultID = jsObj.getInt(TAG_RESULT_ID)) > 0) {
                        WSObjectsModel<AutoPreCheckListInfo> resultObject = new WSObjectsModel<>(jsObj, AutoPreCheckListInfo.class);
                        lst = resultObject.getArrayListObject();
                        switch (resultID) {
                            case 1:
                                this.createPreChecklistActivity.showCreatePrecheckList(objectModel);
                                break;
                            case 2: {
                                if (lst.size() > 0) {
                                    if (resultText.length() == 0) {
                                        Intent intent = new Intent(mContext, AutoCreatePreCheckListActivity.class);
                                        intent.putExtra("autoPreCheckListInfo", lst.get(0));
                                        intent.putExtra("objectModel", objectModel);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        mContext.startActivity(intent);
                                    } else {
                                        AlertDialog.Builder builder;
                                        Dialog dialog;
                                        builder = new AlertDialog.Builder(mContext);
                                        builder.setMessage(jsObj.getString(TAG_RESULT)).setCancelable(false).setPositiveButton("Có",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        Intent intent = new Intent(mContext, AutoCreatePreCheckListActivity.class);
                                                        intent.putExtra("autoPreCheckListInfo", lst.get(0));
                                                        intent.putExtra("objectModel", objectModel);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        mContext.startActivity(intent);
                                                    }
                                                });
                                        dialog = builder.create();
                                        dialog.show();
                                    }
                                } else {
                                    Common.alertDialog(mContext.getResources().getString(R.string.lbl_emty_data_auto_pre_check_list), mContext);
                                }
                                break;
                            }
                        }
                    } else {
                        Common.alertDialog(resultText, mContext);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}