package isc.fpt.fsale.services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import org.json.JSONException;

import java.io.IOException;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListObjectActivity;
import isc.fpt.fsale.activity.UploadRegistrationDocumentActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

/**
 * ASYNC TASK CLASS: PingHostTask
 *
 * @description: AsyncTask class to ping a remote host
 * @author: vandn
 * @created on:		 25/11/2013
 */
public class PingHostTask extends AsyncTask<Void, Void, Boolean> {
    private Context mContext;
    private ProgressDialog pd;
    private boolean isNetWorkAvailable = true;
    /*	private int iFailExeCount = 0;
    private final int loop = 5;	*/
    private AsyncTaskCompleteListener<Boolean> callback;

    public PingHostTask(Context mContext, AsyncTaskCompleteListener<Boolean> cb) {
        this.mContext = mContext;
        this.callback = cb;
    }

    @Override
    protected void onPreExecute() {
        if (Common.isNetworkAvailable(this.mContext)) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                public void run() {
                    pd = Common.showProgressBar(mContext, mContext.getResources().getString(R.string.msg_network_checking));
                }
            });
        } else isNetWorkAvailable = false;
    }

    /**
     * This method is what performs the tasks you wish to perform in the background.
     * In this case, it is the ping request.
     */
    @Override
    protected Boolean doInBackground(Void... params) {
        if (isNetWorkAvailable) {
            int pingCount = 5;
            int pingDelay = 3000;
            String host = "www.google.com";
            if (isPoorSignal(pingCount, pingDelay, host))
                return true;

			/*try{
                boolean ping  = pingHost();
				if(ping == false)
					iFailExeCount++;
				while(iExecuteCount <= loop)
			   { 
				    Thread.sleep(1000);
				    iExecuteCount++;    
				    ping = pingHost();
				    if(ping == false)
				    	iFailExeCount++;
			   }
				
			   if(iFailExeCount >= 3)
				   isPoorSignal = true;				
			}
			catch(Exception e){
				e.printStackTrace();
			}*/
        }
        return false;
    }

    /**
     * This method is called automatically after the 'doinBackground' tasks have finished
     */
    @Override
    protected void onPostExecute(Boolean isPoorSignal) {
        if (isNetWorkAvailable) {
            // kiểm tra processbar đã hủy
            try {
                if (this.pd != null && this.pd.isShowing()) {
                    this.pd.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            try {
                callback.onTaskComplete(isPoorSignal);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            // kiểm tra tình trạng upload số hình thành công và thất bại khi mạng mất kết nối
            if (mContext != null && mContext.getClass().getSimpleName().equals(UploadRegistrationDocumentActivity.class.getSimpleName())) {
                Intent intent = new Intent("UPLOAD_FILE_DISCONNECT_TO_INTERNET");
                mContext.sendBroadcast(intent);
            }
            Common.alertDialog(mContext.getResources().getString(R.string.msg_check_internet), mContext);
        }
    }

    public boolean isPoorSignal(int count, int delay, String host) {
        int r = 1;
        int isFailExeCount = 0;
        boolean isPoorSignal = false;
        for (int i = 0; i < count; i++) {

            try {
                Thread.sleep(delay);
            } catch (InterruptedException e1) {
                // TODO Auto-generated catch block

                e1.printStackTrace();
                return false;
            }
            try {
                r = pingHost(host, delay);
            } catch (IOException e) {
                // TODO Auto-generated catch block

                e.printStackTrace();
                return false;

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block

                e.printStackTrace();
                return false;
            }
            //r: int value of 0 or 1 or 2 0=success, 1=fail, 2=error
            if (r != 0) {
                isFailExeCount++;
            }
        }
        if (isFailExeCount >= 3)
            isPoorSignal = true;
        return isPoorSignal;
    }


    /**
     * Ping a host and return an int value of 0 or 1 or 2 0=success, 1=fail,
     * 2=error
     * <p>
     * Does not work in Android emulator and also delay by '1' second if host
     * not pingable In the Android emulator only ping to 127.0.0.1 works
     *
     * @param host in dotted IP address format
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public static int pingHost(String host, int timeout) throws IOException, InterruptedException {
        Runtime runtime = Runtime.getRuntime();
        timeout /= 1000;
        String cmd = "ping -c 1 -W " + timeout + " " + host;
        Process proc = runtime.exec(cmd);
        int exit = proc.waitFor();
        //int exit = proc.exitValue();
        return exit;
    }
}
