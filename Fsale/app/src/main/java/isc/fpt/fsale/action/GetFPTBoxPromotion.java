package isc.fpt.fsale.action;

import android.content.Context;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.model.PromotionFPTBoxModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by HCM.TUANTT14 on 1/19/2018.
 * update by: haulc3 on 11/02/2020
 */

public class GetFPTBoxPromotion implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private int idPromotionSelected;
    private Spinner spinnerPromotion;
    private int deviceID;

    public GetFPTBoxPromotion(Context mContext, Spinner spinnerPromotion, int deviceID,
                              String billToDistrict, int idPromotionSelected) {
        this.mContext = mContext;
        this.idPromotionSelected = idPromotionSelected;
        this.spinnerPromotion = spinnerPromotion;
        this.deviceID = deviceID;
        String[] arrParamName = new String[]{"UserName", "OTTID", "BillTo_District"};
        String[] arrParamValue = new String[]{Constants.USERNAME, String.valueOf(this.deviceID), billToDistrict};
        String message = mContext.getResources().getString(
                R.string.msg_pd_get_fpt_box_promotion_list);
        String GET_FPT_BOX_PROMOTION_LIST = "GetDeviceOTTPromotion";
        CallServiceTask service = new CallServiceTask(mContext, GET_FPT_BOX_PROMOTION_LIST, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetFPTBoxPromotion.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<PromotionFPTBoxModel> lst = null;
            boolean isError = false;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<PromotionFPTBoxModel> resultObject = new WSObjectsModel<>(jsObj, PromotionFPTBoxModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getArrayListObject();
                } else {
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }

                if (!isError) {
                    if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
                        RegisterActivityNew registerActivityNew = (RegisterActivityNew) mContext;
                        registerActivityNew.step2.loadPromotionFptBox(lst, spinnerPromotion, deviceID, idPromotionSelected);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}