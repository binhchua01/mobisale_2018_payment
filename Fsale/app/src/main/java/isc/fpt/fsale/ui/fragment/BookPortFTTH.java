package isc.fpt.fsale.ui.fragment;

import java.util.ArrayList;

import com.danh32.fontify.Button;
import com.danh32.fontify.EditText;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import isc.fpt.fsale.action.GetIPV4Action;
import isc.fpt.fsale.action.GetPercentTDFTTH;
import isc.fpt.fsale.action.GetPortFreeFTTHAction;
import isc.fpt.fsale.action.GetPortFreeSwitchCardFTTHAction;
import isc.fpt.fsale.action.InsertBookPortFTTH;
import isc.fpt.fsale.action.RecoverRegistrationAction;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import isc.fpt.fsale.utils.MyApp.TrackerName;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class BookPortFTTH extends DialogFragment {

    private Context mContext;
    private ImageButton btnClose;
    private Button btnBookPort, btnRecoverRegistration;
    private RowBookPortModel selectedItemParams;
    private Spinner SpRegions, SpBranch, SpTypeService, SpPop, SpLoai, SpPortFree, SpPortFreeSwitchCard;
    public EditText txt_CardNumber;
    private TextView txt_MinPort, lblTDName, lblRegCode;
    private KeyValuePairAdapter adapter;
    private String SOPHIEUDK, ID;
    private String TypeContract = "FD";
    private int TypeCore = 1;
    private int iLocationID = 0;
    private String IDCoreTD = "0";
    private String PortTextTD = "0";
    private String TypeService = "1";
    private ImageView imgReload, imgReloadPortSwitchCard;
    private int iTDID;
    private String sTDName = "";
    private String IDCoreSC = "0";
    private String[] paramsValue;
    private RegistrationDetailModel modelRegister;
    private Location locationCurrentDevice;
    private Location locationMarker;
    private String latlngDevice = "";
    private String latlngMarker = "";

    public BookPortFTTH() {
    }

    @SuppressLint("ValidFragment")
    public BookPortFTTH(Context _mContext, RowBookPortModel _selectedItem, String _SOPHIEUDK, String _ID, RegistrationDetailModel modelRegister) {
        // TODO Auto-generated constructor stub
        this.mContext = _mContext;
        this.selectedItemParams = _selectedItem;
        this.SOPHIEUDK = _SOPHIEUDK;
        this.ID = _ID;
        this.modelRegister = modelRegister;
    }

    @SuppressLint("ValidFragment")
    public BookPortFTTH(Context _mContext, RowBookPortModel _selectedItem, String _SOPHIEUDK, String _ID,
                        RegistrationDetailModel modelRegister, Location locationCurrentDevice, Location locationMarker) {
        // TODO Auto-generated constructor stub
        this.mContext = _mContext;
        this.selectedItemParams = _selectedItem;
        this.SOPHIEUDK = _SOPHIEUDK;
        this.ID = _ID;
        this.modelRegister = modelRegister;
        this.locationCurrentDevice = locationCurrentDevice;
        this.locationMarker = locationMarker;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View view = inflater.inflate(R.layout.book_port_ftth, container);
        btnClose = (ImageButton) view.findViewById(R.id.btn_close);

        //TXT
        lblRegCode = (TextView) view.findViewById(R.id.lbl_reg_code);
        txt_CardNumber = (EditText) view.findViewById(R.id.txt_bookport_ftth_number_card);
        txt_MinPort = (TextView) view.findViewById(R.id.txt_min_port);

        //Phieu dang ki
        lblRegCode.setText(this.SOPHIEUDK);

        //	Spinner
        SpRegions = (Spinner) view.findViewById(R.id.sp_bookport_ftth_regions);
        SpBranch = (Spinner) view.findViewById(R.id.sp_bookport_ftth_branch);
        SpTypeService = (Spinner) view.findViewById(R.id.sp_bookport_ftth_types_services);
        SpPop = (Spinner) view.findViewById(R.id.sp_bookport_ftth_pop);
        lblTDName = (TextView) view.findViewById(R.id.lbl_td_name);
        SpLoai = (Spinner) view.findViewById(R.id.sp_bookport_ftth_type);
        SpPortFree = (Spinner) view.findViewById(R.id.sp_bookport_ftth_port_free);
        SpPortFreeSwitchCard = (Spinner) view.findViewById(R.id.sp_bookport_ftth_port_switch_card_free);

        btnBookPort = (Button) view.findViewById(R.id.btn_book_port);

        // Thu hồi phiếu đăng ký
        btnRecoverRegistration = (Button) view.findViewById(R.id.btn_recover_registration);
        btnRecoverRegistration.setVisibility(View.GONE);
        // Neu tap diem da duoc book va ODCCable khac ten tap diem ==> hien nut thu hoi

        //if(this.modelRegister.getODCCableType().equals("") && !this.modelRegister.getTDName().equals("") )
        if (modelRegister != null) {
            if ((this.modelRegister.getTDName() != null && !this.modelRegister.getTDName().equals("")))
                btnRecoverRegistration.setVisibility(View.VISIBLE);
        }


        btnRecoverRegistration.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                AlertDialog.Builder builder = null;
                Dialog dialog = null;
                builder = new AlertDialog.Builder(mContext);
                builder.setMessage(mContext.getResources().getString(R.string.msg_confirm_recover_registration)).setCancelable(false).setPositiveButton("Có",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                recoveryPort();
                            }
                        }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.cancel();
                    }
                });
                dialog = builder.create();
                dialog.show();

            }
        });

        imgReload = (ImageView) view.findViewById(R.id.img_reload);
        imgReloadPortSwitchCard = (ImageView) view.findViewById(R.id.img_reload_switch_card);

        this.iTDID = selectedItemParams.getTDID();

        ShowData();

        SpLoai.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);

                //String TDID=String.valueOf(iTDID);
                //String params = Services.getParams(new String[]{TDID});

                if (selectedItem.getID() == 1) {
                    TypeCore = 1;
                    //new GetPortFreeFTTHAction(mContext,params,SpPortFree,1);
                } else {
                    TypeCore = 2;
                    //new GetPortFreeFTTHAction(mContext,params,SpPortFree,2);
                }
                LoadPort();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        SpTypeService.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() == 1) {
                    TypeContract = "FD";
                    TypeService = "1";
                } else {
                    TypeContract = "LL";
                    TypeService = "2";
                }
                LoadPortSwitchCard();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        SpPortFree.setEnabled(false);
        SpPortFree.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                PortTextTD = selectedItem.getDescription();
                IDCoreTD = String.valueOf(selectedItem.getID());
                LoadPortSwitchCard();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        SpPortFreeSwitchCard.setEnabled(false);
        SpPortFreeSwitchCard.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (SpPortFreeSwitchCard.getAdapter() != null) {
                    KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                    IDCoreSC = String.valueOf(selectedItem.getID());
                } else
                    IDCoreSC = null;

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        imgReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadPort();
            }
        });

        imgReloadPortSwitchCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadPortSwitchCard();
            }
        });

        btnBookPort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //RegCode : SOPHIEUDK
                String CardNumber = txt_CardNumber.getText().toString().trim();
                if (CardNumber == null || CardNumber.equals(""))
                    CardNumber = "1";
                String UserName = Constants.USERNAME;
                String TDID = String.valueOf(selectedItemParams.getTDID());
                String TDName = selectedItemParams.getTDName();


                //IDCoreTD : IDCoreTD
                //PortTextTD : PortTextTD
                String LocationID = String.valueOf(selectedItemParams.getLocationID());
                String BranchID = String.valueOf(selectedItemParams.getBranchID());
                GetIPV4Action NewIP = new GetIPV4Action();
                String IP = NewIP.GetIP();
                //TypeCore : TypeCore
                //TypeContract :TypeContract
                String Latlng = selectedItemParams.getLatlngPort();
                if (locationCurrentDevice != null)
                    latlngDevice = locationCurrentDevice.getLatitude() + "," + locationCurrentDevice.getLongitude();

                if (locationMarker != null)
                    latlngMarker = locationMarker.getLatitude() + "," + locationMarker.getLongitude();

                // Kiểm tra port tập điểm
                if (SpPortFree.getAdapter() != null) {
                    KeyValuePairModel selectedItem = (KeyValuePairModel) SpPortFree.getSelectedItem();
                    IDCoreTD = String.valueOf(selectedItem.getID());
                    PortTextTD = selectedItem.getDescription();
                } else {
                    IDCoreTD = PortTextTD = null;
                }

                if (IDCoreTD == null || IDCoreTD.equals("-1")) {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_port_empty), mContext);
                    return;
                }

                // Kiểm tra port tập điểm
                if (SpPortFreeSwitchCard.getAdapter() != null) {
                    KeyValuePairModel selectedItem = (KeyValuePairModel) SpPortFreeSwitchCard.getSelectedItem();
                    IDCoreSC = String.valueOf(selectedItem.getID());
                } else
                    IDCoreSC = null;

                if (IDCoreSC == null || IDCoreSC.equals("-1")) {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_port_empty_switch_card), mContext);
                    return;
                }

                paramsValue = new String[]{SOPHIEUDK, CardNumber, UserName, TDID, IDCoreTD, TDName,
                        PortTextTD, LocationID, BranchID, IP, String.valueOf(TypeCore), TypeContract,
                        Latlng, IDCoreSC, latlngDevice, latlngMarker};

                AlertDialog.Builder builder = null;
                Dialog dialog = null;
                builder = new AlertDialog.Builder(mContext);
                builder.setMessage(mContext.getResources().getString(R.string.msg_confirm_update)).setCancelable(false).setPositiveButton("Có",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                new InsertBookPortFTTH(mContext, paramsValue, ID);
                                onClickTracker("Book-Port FTTH");
                            }
                        }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.cancel();
                    }
                });
                dialog = builder.create();
                dialog.show();

            }
        });


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        return view;
    }

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Common.reportActivityCreate(getActivity().getApplication(), getString(R.string.lbl_screen_name_dialog_book_port_ftth));
        return dialog;
    }


    public void ShowData() {
        if (selectedItemParams != null) {
            //Vung mien SpRegions
            ArrayList<KeyValuePairModel> ListSpRegions = new ArrayList<KeyValuePairModel>();
            ListSpRegions.add(new KeyValuePairModel(1, selectedItemParams.GetLocationName()));
            adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpRegions, Gravity.LEFT);
            SpRegions.setAdapter(adapter);

            iLocationID = selectedItemParams.getLocationID();


            //Vung mien SpBranch
            ArrayList<KeyValuePairModel> ListSpBranch = new ArrayList<KeyValuePairModel>();
            ListSpBranch.add(new KeyValuePairModel(1, selectedItemParams.GetNameBranch()));
            adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpBranch, Gravity.LEFT);
            SpBranch.setAdapter(adapter);


            //Loai Dich Vu SpTypeService
            ArrayList<KeyValuePairModel> ListSpTypeService = new ArrayList<>();
            ListSpTypeService.add(new KeyValuePairModel(1, "FTTH"));
            ListSpTypeService.add(new KeyValuePairModel(2, "Lease-line"));
            adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpTypeService, Gravity.LEFT);
            SpTypeService.setAdapter(adapter);

            //Ten POP SpPop
            ArrayList<KeyValuePairModel> ListSpPop = new ArrayList<KeyValuePairModel>();
            ListSpPop.add(new KeyValuePairModel(1, selectedItemParams.GetPopName()));
            adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPop, Gravity.LEFT);
            SpPop.setAdapter(adapter);

            //Tap diem SpTapDiem
			/*ArrayList<KeyValuePairModel> ListSpTapDiem = new ArrayList<KeyValuePairModel>();
			ListSpTapDiem.add(new KeyValuePairModel(1, selectedItemParams.getTDName()));
			abc = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpTapDiem);
			SpTapDiem.setAdapter(abc);*/
            lblTDName.setText(selectedItemParams.getTDName());
            sTDName = selectedItemParams.getTDName();

            //Loai
            //ArrayList<KeyValuePairModel> ListSpLoai = new ArrayList<KeyValuePairModel>();
            //ListSpLoai.add(new KeyValuePairModel(2,"Một Core"));
            //ListSpLoai.add(new KeyValuePairModel(1,"Hai Core"));
            //abc = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpLoai);
            //SpLoai.setAdapter(abc);
            LoadCoreType();
        }
    }

    private void LoadPort() {
        //PortFree SpPortFree
        String[] params = new String[]{String.valueOf(iTDID)};
        new GetPortFreeFTTHAction(mContext, params, SpPortFree, TypeCore, txt_MinPort, SpPortFreeSwitchCard, txt_CardNumber);
    }

    private void LoadCoreType() {
        String[] params = new String[]{sTDName};
        new GetPercentTDFTTH(mContext, params, SpLoai, iLocationID);
    }

    private void LoadPortSwitchCard() {
        String[] arrParams = new String[]{selectedItemParams.GetPopName(), TypeService};
        new GetPortFreeSwitchCardFTTHAction(mContext, arrParams, SpPortFreeSwitchCard, txt_MinPort, SpPortFree);
    }

    //Thu hoi port
    private void recoveryPort() {
        GetIPV4Action NewIP = new GetIPV4Action();
        String IP = NewIP.GetIP();
        String UserName = Constants.USERNAME;
        paramsValue = new String[]{"2", SOPHIEUDK, UserName, IP};
        new RecoverRegistrationAction(mContext, paramsValue);
        onClickTracker("Thu hồi port FTTH");

    }

    private void onClickTracker(String lable) {
        try {
            // Get tracker.
            Tracker t = ((MyApp) getActivity().getApplication()).getTracker(
                    TrackerName.APP_TRACKER);
            // Set screen name.
            t.setScreenName(getString(R.string.lbl_screen_name_dialog_book_port_ftth) + ".onClick()");
            // Send a screen view.
            t.send(new HitBuilders.ScreenViewBuilder().build());

            // This event will also be sent with the most recently set screen name.
            // Build and send an Event.
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("BOOK_PORT_ONCLICK")
                    .setAction("onClick")
                    .setLabel(lable)
                    .build());

            // Clear the screen name field when we're done.
            t.setScreenName(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
