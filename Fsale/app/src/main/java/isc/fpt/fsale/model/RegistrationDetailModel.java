package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.model.camera319.Camera2Total;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCameraOfNet;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.ui.maxytv.model.MaxyTotal;
import isc.fpt.fsale.ui.maxytv.model.ObjectMaxy;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class RegistrationDetailModel implements Parcelable {
    private String Address;
    private String AddressOrigin;
    private String BillTo_City;
    private String BillTo_District;
    private String BillTo_Number;
    private String BillTo_Street;
    private String BillTo_Ward;
    private int BookPortType;
    private int BranchCode;
    private String CableStatus;
    private String Contact;
    private String Contact_1;
    private String Contact_2;
    private String Contract;
    private String CreateBy;
    private String CreateDate;
    private String CurrentHouse;
    private int CusType;
    private String CusTypeDetail;
    private int Deposit;
    private int DepositBlackPoint;
    private String Description;
    private String DescriptionIBB;
    private int DivisionID;
    private String Email;
    private int EoC;
    private String Floor;
    private String FullName;
    private int ID;
    private String Image;
    private int InDoor;
    private int InDType;
    private int INSCable;
    private int InternetTotal;
    private int IPTVBoxCount;
    private int IPTVChargeTimes;
    private int IPTVDeviceTotal;
    private int IPTVHBOChargeTimes;
    private int IPTVHBOPrepaidMonth;
    private int IPTVKPlusChargeTimes;
    private int IPTVKPlusPrepaidMonth;
    private String IPTVPackage;
    private int IPTVPLCCount;
    private int IPTVPrepaid;
    private int IPTVPrepaidTotal;
    private int IPTVPromotionID;
    private int IPTVRequestDrillWall;
    private int IPTVRequestSetUp;
    private int IPTVReturnSTBCount;
    private int IPTVStatus;
    private int IPTVTotal;
    private int IPTVUseCombo;
    private int IPTVVTCChargeTimes;
    private int IPTVVTCPrepaidMonth;
    private int IPTVVTVChargeTimes;
    private int IPTVVTVPrepaidMonth;
    private int ISPType;
    private String Latlng;
    private int LegalEntity;
    private int LocalType;
    private String LocalTypeName;
    private int LocationID;
    private String Lot;
    private String MapCode;
    private int Modem;
    private String NameVilla;
    private String NameVillaVN;
    private String Note;
    private int ObjectType;
    private int ObjID;
    private String ODCCableType;
    private int OutDoor;
    private int OutDType;
    private int PartnerID;
    private String Passport;
    private String PaymentAbility;
    private String Phone_1;
    private String Phone_2;
    private int Position;
    private String PromotionName;
    private int PromotionID;
    private String RegCode;
    private String Room;
    private int StatusDeposit;
    private String Supporter;
    private String TaxId;
    private int Total;
    private int Type_1;
    private int Type_2;
    private int TypeHouse;
    private String UserName;
    private String TDName;
    private int EquipmentDelivery;
    private int PotentialID;
    private String TDLatLng;
    private String TDAddress;
    private String Birthday;
    private int Payment;
    private String AddressPassport;
    private String PaymentName;
    private int IPTVFimPlusChargeTimes;
    private int IPTVFimPlusPrepaidMonth;
    private int IPTVFimHotChargeTimes;
    private int IPTVFimHotPrepaidMonth;
    private int IPTVFimPlusStdChargeTimes;
    private int IPTVFimPlusStdPrepaidMonth;
    private int IPTVPromotionIDBoxOrder;
    private int PromotionComboID;
    private String PromotionComboName;
    private int RegType;
    private int ContractServiceType;
    private int ServiceType;
    private int ContractLocalType;
    private int ContractPromotionID;
    private int ContractComboStatus;
    private int IsNewAddress;
    private int OTTTotal;
    private int OTTBoxCount;
    private String ContractPromotionName;
    private String ContractServiceTypeName;
    private List<PackageModel> ListPackage;
    private List<Device> ListDevice;
    private List<FPTBox> listDeviceOTT;
    private List<MacOTT> listMACOTT;
    private String EmailAdmin;
    private String DomainName;
    private String TechName;
    private String TechPhoneNumber;
    private String TechEmail;
    private String Office365Total;
    private String ImageInfo;
    private String ImageSignature;
    private int IPTVPromotionType, IPTVPromotionTypeBoxOrder;
    private String IPTVPromotionDesc;
    private CheckListRegistration checkListRegistration;
    private String IPTVPromotionBoxOrderDesc;
    private String cameraTotal;
    private ArrayList<CameraSelected> listCamera;
    private ArrayList<CloudPackageSelected> listCloud;
    private SetupDetail setupCamera;
    private CheckListService checkListService;
    private ListDeviceMACOTT ListDeviceMACOTT;
    private String GiftName;
    private int GiftID;
    private CartExtraOtt ObjectExtraOTT;
    private List<CategoryServiceList> CategoryServiceList;
    private int BaseObjID;
    private String BaseContract;
    private String HTMLDetail;
    private ObjectCamera ObjectCamera;
    private int Foxy2ChargeTimes;
    private int Foxy2PrepaidMonth;
    private int Foxy4ChargeTimes;
    private int Foxy4PrepaidMonth;
    private String BaseImageInfo;
    private int BaseRegIDImageInfo;
    private double DeviceTotal;
    private int DiscountRP_VoucherTotal;
    private String ReferralCode;
    private String ReferralCodeDesc;
    private String VoucherCode;
    private String VoucherCodeDesc;
    private String BillTo_CityVN;
    private String BillTo_DistrictVN;
    private String BillTo_StreetVN;
    private String BillTo_WardVN;
    private InternetTotal ServiceCodeInternetTotal;
    private IpTvTotal ServiceCodeIpTvTotal;
    private Camera2Total ServiceCodeCameraTotal;
    private MaxyTotal ServiceCodeMaxyTotal;
    private int CategoryServiceGroupID;
    private ObjectCameraOfNet ObjectCameraOfNet;
    private isc.fpt.fsale.ui.maxytv.model.ObjectMaxy ObjectMaxy;
    private int SourceType;

    public RegistrationDetailModel() {
    }

    protected RegistrationDetailModel(Parcel in) {
        Address = in.readString();
        AddressOrigin = in.readString();
        BillTo_City = in.readString();
        BillTo_District = in.readString();
        BillTo_Number = in.readString();
        BillTo_Street = in.readString();
        BillTo_Ward = in.readString();
        BookPortType = in.readInt();
        BranchCode = in.readInt();
        CableStatus = in.readString();
        Contact = in.readString();
        Contact_1 = in.readString();
        Contact_2 = in.readString();
        Contract = in.readString();
        CreateBy = in.readString();
        CreateDate = in.readString();
        CurrentHouse = in.readString();
        CusType = in.readInt();
        CusTypeDetail = in.readString();
        Deposit = in.readInt();
        DepositBlackPoint = in.readInt();
        Description = in.readString();
        DescriptionIBB = in.readString();
        DivisionID = in.readInt();
        Email = in.readString();
        EoC = in.readInt();
        Floor = in.readString();
        FullName = in.readString();
        ID = in.readInt();
        Image = in.readString();
        InDoor = in.readInt();
        InDType = in.readInt();
        INSCable = in.readInt();
        InternetTotal = in.readInt();
        IPTVBoxCount = in.readInt();
        IPTVChargeTimes = in.readInt();
        IPTVDeviceTotal = in.readInt();
        IPTVHBOChargeTimes = in.readInt();
        IPTVHBOPrepaidMonth = in.readInt();
        IPTVKPlusChargeTimes = in.readInt();
        IPTVKPlusPrepaidMonth = in.readInt();
        IPTVPackage = in.readString();
        IPTVPLCCount = in.readInt();
        IPTVPrepaid = in.readInt();
        IPTVPrepaidTotal = in.readInt();
        IPTVPromotionID = in.readInt();
        IPTVRequestDrillWall = in.readInt();
        IPTVRequestSetUp = in.readInt();
        IPTVReturnSTBCount = in.readInt();
        IPTVStatus = in.readInt();
        IPTVTotal = in.readInt();
        IPTVUseCombo = in.readInt();
        IPTVVTCChargeTimes = in.readInt();
        IPTVVTCPrepaidMonth = in.readInt();
        IPTVVTVChargeTimes = in.readInt();
        IPTVVTVPrepaidMonth = in.readInt();
        ISPType = in.readInt();
        Latlng = in.readString();
        LegalEntity = in.readInt();
        LocalType = in.readInt();
        LocalTypeName = in.readString();
        LocationID = in.readInt();
        Lot = in.readString();
        MapCode = in.readString();
        Modem = in.readInt();
        NameVilla = in.readString();
        NameVillaVN = in.readString();
        Note = in.readString();
        ObjectType = in.readInt();
        ObjID = in.readInt();
        ODCCableType = in.readString();
        OutDoor = in.readInt();
        OutDType = in.readInt();
        PartnerID = in.readInt();
        Passport = in.readString();
        PaymentAbility = in.readString();
        Phone_1 = in.readString();
        Phone_2 = in.readString();
        Position = in.readInt();
        PromotionName = in.readString();
        PromotionID = in.readInt();
        RegCode = in.readString();
        Room = in.readString();
        StatusDeposit = in.readInt();
        Supporter = in.readString();
        TaxId = in.readString();
        Total = in.readInt();
        Type_1 = in.readInt();
        Type_2 = in.readInt();
        TypeHouse = in.readInt();
        UserName = in.readString();
        TDName = in.readString();
        EquipmentDelivery = in.readInt();
        PotentialID = in.readInt();
        TDLatLng = in.readString();
        TDAddress = in.readString();
        Birthday = in.readString();
        Payment = in.readInt();
        AddressPassport = in.readString();
        PaymentName = in.readString();
        IPTVFimPlusChargeTimes = in.readInt();
        IPTVFimPlusPrepaidMonth = in.readInt();
        IPTVFimHotChargeTimes = in.readInt();
        IPTVFimHotPrepaidMonth = in.readInt();
        IPTVFimPlusStdChargeTimes = in.readInt();
        IPTVFimPlusStdPrepaidMonth = in.readInt();
        IPTVPromotionIDBoxOrder = in.readInt();
        PromotionComboID = in.readInt();
        PromotionComboName = in.readString();
        RegType = in.readInt();
        ContractServiceType = in.readInt();
        ContractLocalType = in.readInt();
        ContractPromotionID = in.readInt();
        ContractComboStatus = in.readInt();
        IsNewAddress = in.readInt();
        OTTTotal = in.readInt();
        OTTBoxCount = in.readInt();
        ContractPromotionName = in.readString();
        ContractServiceTypeName = in.readString();
        ListPackage = in.createTypedArrayList(PackageModel.CREATOR);
        ListDevice = in.createTypedArrayList(Device.CREATOR);
        listDeviceOTT = in.createTypedArrayList(FPTBox.CREATOR);
        listMACOTT = in.createTypedArrayList(MacOTT.CREATOR);
        EmailAdmin = in.readString();
        DomainName = in.readString();
        TechName = in.readString();
        TechPhoneNumber = in.readString();
        TechEmail = in.readString();
        Office365Total = in.readString();
        ImageInfo = in.readString();
        ImageSignature = in.readString();
        IPTVPromotionType = in.readInt();
        IPTVPromotionTypeBoxOrder = in.readInt();
        IPTVPromotionDesc = in.readString();
        checkListRegistration = in.readParcelable(CheckListRegistration.class.getClassLoader());
        IPTVPromotionBoxOrderDesc = in.readString();
        cameraTotal = in.readString();
        listCamera = in.createTypedArrayList(CameraSelected.CREATOR);
        listCloud = in.createTypedArrayList(CloudPackageSelected.CREATOR);
        setupCamera = in.readParcelable(SetupDetail.class.getClassLoader());
        checkListService = in.readParcelable(CheckListService.class.getClassLoader());
        ListDeviceMACOTT = in.readParcelable(isc.fpt.fsale.model.ListDeviceMACOTT.class.getClassLoader());
        GiftName = in.readString();
        GiftID = in.readInt();
        ObjectExtraOTT = in.readParcelable(CartExtraOtt.class.getClassLoader());
        CategoryServiceList = in.createTypedArrayList(isc.fpt.fsale.model.CategoryServiceList.CREATOR);
        BaseObjID = in.readInt();
        BaseContract = in.readString();
        HTMLDetail = in.readString();
        ObjectCamera = in.readParcelable(isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera.class.getClassLoader());
        ObjectCameraOfNet = in.readParcelable(ObjectCameraOfNet.class.getClassLoader());
        Foxy2ChargeTimes = in.readInt();
        Foxy2PrepaidMonth = in.readInt();
        Foxy4ChargeTimes = in.readInt();
        Foxy4PrepaidMonth = in.readInt();
        BaseImageInfo = in.readString();
        BaseRegIDImageInfo = in.readInt();
        DeviceTotal = in.readDouble();
        DiscountRP_VoucherTotal = in.readInt();
        ReferralCode = in.readString();
        ReferralCodeDesc = in.readString();
        VoucherCode = in.readString();
        VoucherCodeDesc = in.readString();
        BillTo_CityVN = in.readString();
        BillTo_DistrictVN = in.readString();
        BillTo_StreetVN = in.readString();
        BillTo_WardVN = in.readString();
        ServiceCodeInternetTotal = in.readParcelable(InternetTotal.class.getClassLoader());
        ServiceCodeCameraTotal = in.readParcelable(Camera2Total.class.getClassLoader());
        ServiceCodeIpTvTotal = in.readParcelable(IpTvTotal.class.getClassLoader());
        CategoryServiceGroupID = in.readInt();
        ObjectMaxy = in.readParcelable(ObjectMaxy.class.getClassLoader());
        ServiceCodeMaxyTotal = in.readParcelable(MaxyTotal.class.getClassLoader());
        ServiceType = in.readInt();
        SourceType = in.readInt();
    }

    public static final Creator<RegistrationDetailModel> CREATOR = new Creator<RegistrationDetailModel>() {
        @Override
        public RegistrationDetailModel createFromParcel(Parcel in) {
            return new RegistrationDetailModel(in);
        }

        @Override
        public RegistrationDetailModel[] newArray(int size) {
            return new RegistrationDetailModel[size];
        }
    };

    //ver 3.21
    // update ver 3.23 28/9/2021 --> SourceType
    public RegistrationDetailModel(String address, String addressOrigin, String billTo_City, String billTo_District, String billTo_Number, String billTo_Street, String billTo_Ward, int bookPortType, int branchCode, String cableStatus, String contact, String contact_1, String contact_2, String contract, String createBy, String createDate, String currentHouse, int cusType, String cusTypeDetail, int deposit, int depositBlackPoint, String description, String descriptionIBB, int divisionID, String email, int eoC, String floor, String fullName, int ID, String image, int inDoor, int inDType, int INSCable, int internetTotal, int IPTVBoxCount, int IPTVChargeTimes, int IPTVDeviceTotal, int IPTVHBOChargeTimes, int IPTVHBOPrepaidMonth, int IPTVKPlusChargeTimes, int IPTVKPlusPrepaidMonth, String IPTVPackage, int IPTVPLCCount, int IPTVPrepaid, int IPTVPrepaidTotal, int IPTVPromotionID, int IPTVRequestDrillWall, int IPTVRequestSetUp, int IPTVReturnSTBCount, int IPTVStatus, int IPTVTotal, int IPTVUseCombo, int IPTVVTCChargeTimes, int IPTVVTCPrepaidMonth, int IPTVVTVChargeTimes, int IPTVVTVPrepaidMonth, int ISPType, String latlng, int legalEntity, int localType, String localTypeName, int locationID, String lot, String mapCode, int modem, String nameVilla, String nameVillaVN, String note, int objectType, int objID, String ODCCableType, int outDoor, int outDType, int partnerID, String passport, String paymentAbility, String phone_1, String phone_2, int position, String promotionName, int promotionID, String regCode, String room, int statusDeposit, String supporter, String taxId, int total, int type_1, int type_2, int typeHouse, String userName, String TDName, int equipmentDelivery, int potentialID, String TDLatLng, String TDAddress, String birthday, int payment, String addressPassport, String paymentName, int IPTVFimPlusChargeTimes, int IPTVFimPlusPrepaidMonth, int IPTVFimHotChargeTimes, int IPTVFimHotPrepaidMonth, int IPTVFimPlusStdChargeTimes, int IPTVFimPlusStdPrepaidMonth, int IPTVPromotionIDBoxOrder, int promotionComboID, String promotionComboName, int regType, int contractServiceType, int contractLocalType, int contractPromotionID, int contractComboStatus, int isNewAddress, int OTTTotal, int OTTBoxCount, String contractPromotionName, String contractServiceTypeName, List<PackageModel> listPackage, List<Device> listDevice, List<FPTBox> listDeviceOTT, List<MacOTT> listMACOTT, String emailAdmin, String domainName, String techName, String techPhoneNumber, String techEmail, String office365Total, String imageInfo, String imageSignature, int IPTVPromotionType, int IPTVPromotionTypeBoxOrder, String IPTVPromotionDesc, CheckListRegistration checkListRegistration, String IPTVPromotionBoxOrderDesc, String cameraTotal, ArrayList<CameraSelected> listCamera, ArrayList<CloudPackageSelected> listCloud, SetupDetail setupCamera, CheckListService checkListService, isc.fpt.fsale.model.ListDeviceMACOTT listDeviceMACOTT, String giftName, int giftID, CartExtraOtt objectExtraOTT, List<isc.fpt.fsale.model.CategoryServiceList> categoryServiceList, int baseObjID, String baseContract, String HTMLDetail, isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera objectCamera, int foxy2ChargeTimes, int foxy2PrepaidMonth, int foxy4ChargeTimes, int foxy4PrepaidMonth, String baseImageInfo, int baseRegIDImageInfo, double deviceTotal, int discountRP_VoucherTotal, String referralCode, String referralCodeDesc, String voucherCode, String voucherCodeDesc, String billTo_CityVN, String billTo_DistrictVN, String billTo_StreetVN, String billTo_WardVN, isc.fpt.fsale.model.InternetTotal serviceCodeInternetTotal, IpTvTotal serviceCodeIpTvTotal, Camera2Total serviceCodeCameraTotal, int categoryServiceGroupID, isc.fpt.fsale.ui.fpt_camera.model.ObjectCameraOfNet objectCameraOfNet, isc.fpt.fsale.ui.maxytv.model.ObjectMaxy objectMaxy, MaxyTotal maxyTotal, int serviceType, int sourceType) {
        Address = address;
        AddressOrigin = addressOrigin;
        BillTo_City = billTo_City;
        BillTo_District = billTo_District;
        BillTo_Number = billTo_Number;
        BillTo_Street = billTo_Street;
        BillTo_Ward = billTo_Ward;
        BookPortType = bookPortType;
        BranchCode = branchCode;
        CableStatus = cableStatus;
        Contact = contact;
        Contact_1 = contact_1;
        Contact_2 = contact_2;
        Contract = contract;
        CreateBy = createBy;
        CreateDate = createDate;
        CurrentHouse = currentHouse;
        CusType = cusType;
        CusTypeDetail = cusTypeDetail;
        Deposit = deposit;
        DepositBlackPoint = depositBlackPoint;
        Description = description;
        DescriptionIBB = descriptionIBB;
        DivisionID = divisionID;
        Email = email;
        EoC = eoC;
        Floor = floor;
        FullName = fullName;
        this.ID = ID;
        Image = image;
        InDoor = inDoor;
        InDType = inDType;
        this.INSCable = INSCable;
        InternetTotal = internetTotal;
        this.IPTVBoxCount = IPTVBoxCount;
        this.IPTVChargeTimes = IPTVChargeTimes;
        this.IPTVDeviceTotal = IPTVDeviceTotal;
        this.IPTVHBOChargeTimes = IPTVHBOChargeTimes;
        this.IPTVHBOPrepaidMonth = IPTVHBOPrepaidMonth;
        this.IPTVKPlusChargeTimes = IPTVKPlusChargeTimes;
        this.IPTVKPlusPrepaidMonth = IPTVKPlusPrepaidMonth;
        this.IPTVPackage = IPTVPackage;
        this.IPTVPLCCount = IPTVPLCCount;
        this.IPTVPrepaid = IPTVPrepaid;
        this.IPTVPrepaidTotal = IPTVPrepaidTotal;
        this.IPTVPromotionID = IPTVPromotionID;
        this.IPTVRequestDrillWall = IPTVRequestDrillWall;
        this.IPTVRequestSetUp = IPTVRequestSetUp;
        this.IPTVReturnSTBCount = IPTVReturnSTBCount;
        this.IPTVStatus = IPTVStatus;
        this.IPTVTotal = IPTVTotal;
        this.IPTVUseCombo = IPTVUseCombo;
        this.IPTVVTCChargeTimes = IPTVVTCChargeTimes;
        this.IPTVVTCPrepaidMonth = IPTVVTCPrepaidMonth;
        this.IPTVVTVChargeTimes = IPTVVTVChargeTimes;
        this.IPTVVTVPrepaidMonth = IPTVVTVPrepaidMonth;
        this.ISPType = ISPType;
        Latlng = latlng;
        LegalEntity = legalEntity;
        LocalType = localType;
        LocalTypeName = localTypeName;
        LocationID = locationID;
        Lot = lot;
        MapCode = mapCode;
        Modem = modem;
        NameVilla = nameVilla;
        NameVillaVN = nameVillaVN;
        Note = note;
        ObjectType = objectType;
        ObjID = objID;
        this.ODCCableType = ODCCableType;
        OutDoor = outDoor;
        OutDType = outDType;
        PartnerID = partnerID;
        Passport = passport;
        PaymentAbility = paymentAbility;
        Phone_1 = phone_1;
        Phone_2 = phone_2;
        Position = position;
        PromotionName = promotionName;
        PromotionID = promotionID;
        RegCode = regCode;
        Room = room;
        StatusDeposit = statusDeposit;
        Supporter = supporter;
        TaxId = taxId;
        Total = total;
        Type_1 = type_1;
        Type_2 = type_2;
        TypeHouse = typeHouse;
        UserName = userName;
        this.TDName = TDName;
        EquipmentDelivery = equipmentDelivery;
        PotentialID = potentialID;
        this.TDLatLng = TDLatLng;
        this.TDAddress = TDAddress;
        Birthday = birthday;
        Payment = payment;
        AddressPassport = addressPassport;
        PaymentName = paymentName;
        this.IPTVFimPlusChargeTimes = IPTVFimPlusChargeTimes;
        this.IPTVFimPlusPrepaidMonth = IPTVFimPlusPrepaidMonth;
        this.IPTVFimHotChargeTimes = IPTVFimHotChargeTimes;
        this.IPTVFimHotPrepaidMonth = IPTVFimHotPrepaidMonth;
        this.IPTVFimPlusStdChargeTimes = IPTVFimPlusStdChargeTimes;
        this.IPTVFimPlusStdPrepaidMonth = IPTVFimPlusStdPrepaidMonth;
        this.IPTVPromotionIDBoxOrder = IPTVPromotionIDBoxOrder;
        PromotionComboID = promotionComboID;
        PromotionComboName = promotionComboName;
        RegType = regType;
        ContractServiceType = contractServiceType;
        ContractLocalType = contractLocalType;
        ContractPromotionID = contractPromotionID;
        ContractComboStatus = contractComboStatus;
        IsNewAddress = isNewAddress;
        this.OTTTotal = OTTTotal;
        this.OTTBoxCount = OTTBoxCount;
        ContractPromotionName = contractPromotionName;
        ContractServiceTypeName = contractServiceTypeName;
        ListPackage = listPackage;
        ListDevice = listDevice;
        this.listDeviceOTT = listDeviceOTT;
        this.listMACOTT = listMACOTT;
        EmailAdmin = emailAdmin;
        DomainName = domainName;
        TechName = techName;
        TechPhoneNumber = techPhoneNumber;
        TechEmail = techEmail;
        Office365Total = office365Total;
        ImageInfo = imageInfo;
        ImageSignature = imageSignature;
        this.IPTVPromotionType = IPTVPromotionType;
        this.IPTVPromotionTypeBoxOrder = IPTVPromotionTypeBoxOrder;
        this.IPTVPromotionDesc = IPTVPromotionDesc;
        this.checkListRegistration = checkListRegistration;
        this.IPTVPromotionBoxOrderDesc = IPTVPromotionBoxOrderDesc;
        this.cameraTotal = cameraTotal;
        this.listCamera = listCamera;
        this.listCloud = listCloud;
        this.setupCamera = setupCamera;
        this.checkListService = checkListService;
        ListDeviceMACOTT = listDeviceMACOTT;
        GiftName = giftName;
        GiftID = giftID;
        ObjectExtraOTT = objectExtraOTT;
        CategoryServiceList = categoryServiceList;
        BaseObjID = baseObjID;
        BaseContract = baseContract;
        this.HTMLDetail = HTMLDetail;
        ObjectCamera = objectCamera;
        Foxy2ChargeTimes = foxy2ChargeTimes;
        Foxy2PrepaidMonth = foxy2PrepaidMonth;
        Foxy4ChargeTimes = foxy4ChargeTimes;
        Foxy4PrepaidMonth = foxy4PrepaidMonth;
        BaseImageInfo = baseImageInfo;
        BaseRegIDImageInfo = baseRegIDImageInfo;
        DeviceTotal = deviceTotal;
        DiscountRP_VoucherTotal = discountRP_VoucherTotal;
        ReferralCode = referralCode;
        ReferralCodeDesc = referralCodeDesc;
        VoucherCode = voucherCode;
        VoucherCodeDesc = voucherCodeDesc;
        BillTo_CityVN = billTo_CityVN;
        BillTo_DistrictVN = billTo_DistrictVN;
        BillTo_StreetVN = billTo_StreetVN;
        BillTo_WardVN = billTo_WardVN;
        ServiceCodeInternetTotal = serviceCodeInternetTotal;
        ServiceCodeIpTvTotal = serviceCodeIpTvTotal;
        ServiceCodeCameraTotal = serviceCodeCameraTotal;
        CategoryServiceGroupID = categoryServiceGroupID;
        ObjectCameraOfNet = objectCameraOfNet;
        ObjectMaxy = objectMaxy;
        ServiceCodeMaxyTotal = maxyTotal;
        ServiceType = serviceType;
        SourceType = sourceType;
    }

    public int getSourceType() {
        return SourceType;
    }

    public void setSourceType(int sourceType) {
        SourceType = sourceType;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }

    public MaxyTotal getServiceCodeMaxyTotal() {
        return ServiceCodeMaxyTotal;
    }

    public void setServiceCodeMaxyTotal(MaxyTotal serviceCodeMaxyTotal) {
        ServiceCodeMaxyTotal = serviceCodeMaxyTotal;
    }

    public static Creator<RegistrationDetailModel> getCREATOR() {
        return CREATOR;
    }

    public isc.fpt.fsale.ui.maxytv.model.ObjectMaxy getObjectMaxy() {
        return ObjectMaxy;
    }

    public void setObjectMaxy(isc.fpt.fsale.ui.maxytv.model.ObjectMaxy objectMaxy) {
        ObjectMaxy = objectMaxy;
    }

    public ObjectCameraOfNet getObjectCameraOfNet() {
        return ObjectCameraOfNet;
    }

    public void setObjectCameraOfNet(ObjectCameraOfNet objectCameraOfNet) {
        this.ObjectCameraOfNet = objectCameraOfNet;
    }

    public String getContractServiceTypeName() {
        return ContractServiceTypeName;
    }

    public void setContractServiceTypeName(String contractServiceTypeName) {
        ContractServiceTypeName = contractServiceTypeName;
    }

    public String getContractPromotionName() {
        return ContractPromotionName;
    }

    public void setContractPromotionName(String contractPromotionName) {
        ContractPromotionName = contractPromotionName;
    }

    public int getIsNewAddress() {
        return IsNewAddress;
    }

    public int getOTTTotal() {
        return OTTTotal;
    }

    public void setOTTTotal(int oTTTotal) {
        OTTTotal = oTTTotal;
    }

    public void setIsNewAddress(int isNewAddress) {
        IsNewAddress = isNewAddress;
    }

    public int getOTTBoxCount() {
        return OTTBoxCount;
    }

    public void setOTTBoxCount(int oTTBoxCount) {
        OTTBoxCount = oTTBoxCount;
    }

    public int getContractComboStatus() {
        return ContractComboStatus;
    }

    public void setContractComboStatus(int contractComboStatus) {
        ContractComboStatus = contractComboStatus;
    }

    public int getContractLocalType() {
        return ContractLocalType;
    }

    public void setContractLocalType(int contractLocalType) {
        ContractLocalType = contractLocalType;
    }

    public int getContractPromotionID() {
        return ContractPromotionID;
    }

    public void setContractPromotionID(int contractPromotionID) {
        ContractPromotionID = contractPromotionID;
    }

    public int getContractServiceType() {
        return ContractServiceType;
    }

    public void setContractServiceType(int contractServiceType) {
        ContractServiceType = contractServiceType;
    }

    public int getRegType() {
        return RegType;
    }

    public void setRegType(int regType) {
        RegType = regType;
    }

    public int getPromotionComboID() {
        return PromotionComboID;
    }

    public void setPromotionComboID(int PromotionComboID) {
        this.PromotionComboID = PromotionComboID;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getPaymentName() {
        return PaymentName;
    }

    public void setPaymentName(String paymentName) {
        PaymentName = paymentName;
    }

    public String getPromotionComboName() {
        return PromotionComboName;
    }

    public void setPromotionComboName(String promotionComboName) {
        PromotionComboName = promotionComboName;
    }

    public void setIPTVPromotionIDBoxOrder(int iPTVPromotionIDBoxOrder) {
        IPTVPromotionIDBoxOrder = iPTVPromotionIDBoxOrder;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getAddressOrigin() {
        return AddressOrigin;
    }

    public void setAddressOrigin(String addressOrigin) {
        AddressOrigin = addressOrigin;
    }

    public String getBillTo_City() {
        return BillTo_City;
    }

    public void setBillTo_City(String BillTo_City) {
        this.BillTo_City = BillTo_City;
    }

    public String getBillTo_District() {
        return BillTo_District;
    }

    public void setBillTo_District(String BillTo_District) {
        this.BillTo_District = BillTo_District;
    }

    public String getBillTo_Number() {
        return BillTo_Number;
    }

    public void setBillTo_Number(String BillTo_Number) {
        this.BillTo_Number = BillTo_Number;
    }

    public String getBillTo_Street() {
        return BillTo_Street;
    }

    public void setBillTo_Street(String BillTo_Street) {
        this.BillTo_Street = BillTo_Street;
    }

    public String getBillTo_Ward() {
        return BillTo_Ward;
    }

    public void setBillTo_Ward(String BillTo_Ward) {
        this.BillTo_Ward = BillTo_Ward;
    }

    public int getBookPortType() {
        return BookPortType;
    }

    public void setBookPortType(int BookPortType) {
        this.BookPortType = BookPortType;
    }

    public int getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(int BranchCode) {
        this.BranchCode = BranchCode;
    }

    public String getCableStatus() {
        return CableStatus;
    }

    public void setCableStatus(String CableStatus) {
        this.CableStatus = CableStatus;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String Contact) {
        this.Contact = Contact;
    }

    public String getContact_1() {
        return Contact_1;
    }

    public void setContact_1(String Contact_1) {
        this.Contact_1 = Contact_1;
    }

    public String getContact_2() {
        return Contact_2;
    }

    public void setContact_2(String Contact_2) {
        this.Contact_2 = Contact_2;
    }

    public String getContract() {
        return Contract;
    }

    public void setContract(String Contract) {
        this.Contract = Contract;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String CreateBy) {
        this.CreateBy = CreateBy;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public String getCurrentHouse() {
        return CurrentHouse;
    }

    public void setCurrentHouse(String CurrentHouse) {
        this.CurrentHouse = CurrentHouse;
    }

    public int getCusType() {
        return CusType;
    }

    public void setCusType(int CusType) {
        this.CusType = CusType;
    }

    public String getCusTypeDetail() {
        return CusTypeDetail;
    }

    public void setCusTypeDetail(String CusTypeDetail) {
        this.CusTypeDetail = CusTypeDetail;
    }

    public int getDeposit() {
        return Deposit;
    }

    public void setDeposit(int Deposit) {
        this.Deposit = Deposit;
    }

    public int getDepositBlackPoint() {
        return DepositBlackPoint;
    }

    public void setDepositBlackPoint(int DepositBlackPoint) {
        this.DepositBlackPoint = DepositBlackPoint;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getDescriptionIBB() {
        return DescriptionIBB;
    }

    public void setDescriptionIBB(String DescriptionIBB) {
        this.DescriptionIBB = DescriptionIBB;
    }

    public int getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(int DivisionID) {
        this.DivisionID = DivisionID;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public int getEoC() {
        return EoC;
    }

    public void setEoC(int EoC) {
        this.EoC = EoC;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String Floor) {
        this.Floor = Floor;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public int getInDoor() {
        return InDoor;
    }

    public void setInDoor(int InDoor) {
        this.InDoor = InDoor;
    }

    public int getInDType() {
        return InDType;
    }

    public void setInDType(int InDType) {
        this.InDType = InDType;
    }

    public int getINSCable() {
        return INSCable;
    }

    public void setINSCable(int INSCable) {
        this.INSCable = INSCable;
    }

    public int getInternetTotal() {
        return InternetTotal;
    }

    public void setInternetTotal(int InternetTotal) {
        this.InternetTotal = InternetTotal;
    }

    public int getIPTVBoxCount() {
        return IPTVBoxCount;
    }

    public void setIPTVBoxCount(int IPTVBoxCount) {
        this.IPTVBoxCount = IPTVBoxCount;
    }

    public int getIPTVChargeTimes() {
        return IPTVChargeTimes;
    }

    public void setIPTVChargeTimes(int IPTVChargeTimes) {
        this.IPTVChargeTimes = IPTVChargeTimes;
    }

    public int getIPTVDeviceTotal() {
        return IPTVDeviceTotal;
    }

    public void setIPTVDeviceTotal(int IPTVDeviceTotal) {
        this.IPTVDeviceTotal = IPTVDeviceTotal;
    }

    public int getIPTVHBOChargeTimes() {
        return IPTVHBOChargeTimes;
    }

    public void setIPTVHBOChargeTimes(int IPTVHBOChargeTimes) {
        this.IPTVHBOChargeTimes = IPTVHBOChargeTimes;
    }

    public int getIPTVHBOPrepaidMonth() {
        return IPTVHBOPrepaidMonth;
    }

    public void setIPTVHBOPrepaidMonth(int IPTVHBOPrepaidMonth) {
        this.IPTVHBOPrepaidMonth = IPTVHBOPrepaidMonth;
    }

    public int getIPTVKPlusChargeTimes() {
        return IPTVKPlusChargeTimes;
    }

    public void setIPTVKPlusChargeTimes(int IPTVKPlusChargeTimes) {
        this.IPTVKPlusChargeTimes = IPTVKPlusChargeTimes;
    }

    public int getIPTVKPlusPrepaidMonth() {
        return IPTVKPlusPrepaidMonth;
    }

    public void setIPTVKPlusPrepaidMonth(int IPTVKPlusPrepaidMonth) {
        this.IPTVKPlusPrepaidMonth = IPTVKPlusPrepaidMonth;
    }

    public int getIPTVFimPlusStdChargeTimes() {
        return IPTVFimPlusStdChargeTimes;
    }

    public void setIPTVFimPlusStdChargeTimes(int IPTVFimPlusStdChargeTimes) {
        this.IPTVFimPlusStdChargeTimes = IPTVFimPlusStdChargeTimes;
    }

    public int getIPTVFimPlusStdPrepaidMonth() {
        return IPTVFimPlusStdPrepaidMonth;
    }

    public void setIPTVFimPlusStdPrepaidMonth(int IPTVFimPlusStdPrepaidMonth) {
        this.IPTVFimPlusStdPrepaidMonth = IPTVFimPlusStdPrepaidMonth;
    }

    public String getIPTVPackage() {
        return IPTVPackage;
    }

    public void setIPTVPackage(String IPTVPackage) {
        this.IPTVPackage = IPTVPackage;
    }

    public int getIPTVPLCCount() {
        return IPTVPLCCount;
    }

    public void setIPTVPLCCount(int IPTVPLCCount) {
        this.IPTVPLCCount = IPTVPLCCount;
    }

    public int getIPTVPrepaid() {
        return IPTVPrepaid;
    }

    public void setIPTVPrepaid(int IPTVPrepaid) {
        this.IPTVPrepaid = IPTVPrepaid;
    }

    public int getIPTVPrepaidTotal() {
        return IPTVPrepaidTotal;
    }

    public void setIPTVPrepaidTotal(int IPTVPrepaidTotal) {
        this.IPTVPrepaidTotal = IPTVPrepaidTotal;
    }

    public int getIPTVPromotionID() {
        return IPTVPromotionID;
    }

    public void setIPTVPromotionID(int IPTVPromotionID) {
        this.IPTVPromotionID = IPTVPromotionID;
    }

    public int getIPTVRequestDrillWall() {
        return IPTVRequestDrillWall;
    }

    public void setIPTVRequestDrillWall(int IPTVRequestDrillWall) {
        this.IPTVRequestDrillWall = IPTVRequestDrillWall;
    }

    public int getIPTVRequestSetUp() {
        return IPTVRequestSetUp;
    }

    public void setIPTVRequestSetUp(int IPTVRequestSetUp) {
        this.IPTVRequestSetUp = IPTVRequestSetUp;
    }

    public int getIPTVReturnSTBCount() {
        return IPTVReturnSTBCount;
    }

    public void setIPTVReturnSTBCount(int IPTVReturnSTBCount) {
        this.IPTVReturnSTBCount = IPTVReturnSTBCount;
    }

    public int getIPTVStatus() {
        return IPTVStatus;
    }

    public void setIPTVStatus(int IPTVStatus) {
        this.IPTVStatus = IPTVStatus;
    }

    public int getIPTVTotal() {
        return IPTVTotal;
    }

    public void setIPTVTotal(int IPTVTotal) {
        this.IPTVTotal = IPTVTotal;
    }

    public int getIPTVUseCombo() {
        return IPTVUseCombo;
    }

    public void setIPTVUseCombo(int IPTVUseCombo) {
        this.IPTVUseCombo = IPTVUseCombo;
    }

    public int getIPTVVTCChargeTimes() {
        return IPTVVTCChargeTimes;
    }

    public void setIPTVVTCChargeTimes(int IPTVVTCChargeTimes) {
        this.IPTVVTCChargeTimes = IPTVVTCChargeTimes;
    }

    public int getIPTVVTCPrepaidMonth() {
        return IPTVVTCPrepaidMonth;
    }

    public void setIPTVVTCPrepaidMonth(int IPTVVTCPrepaidMonth) {
        this.IPTVVTCPrepaidMonth = IPTVVTCPrepaidMonth;
    }

    public int getIPTVVTVChargeTimes() {
        return IPTVVTVChargeTimes;
    }

    public void setIPTVVTVChargeTimes(int IPTVVTVChargeTimes) {
        this.IPTVVTVChargeTimes = IPTVVTVChargeTimes;
    }

    public int getIPTVVTVPrepaidMonth() {
        return IPTVVTVPrepaidMonth;
    }

    public void setIPTVVTVPrepaidMonth(int IPTVVTVPrepaidMonth) {
        this.IPTVVTVPrepaidMonth = IPTVVTVPrepaidMonth;
    }

    public int getISPType() {
        return ISPType;
    }

    public void setISPType(int ISPType) {
        this.ISPType = ISPType;
    }

    public String getLatlng() {
        return Latlng;
    }

    public void setLatlng(String Latlng) {
        this.Latlng = Latlng;
    }

    public int getLegalEntity() {
        return LegalEntity;
    }

    public void setLegalEntity(int LegalEntity) {
        this.LegalEntity = LegalEntity;
    }

    public int getLocalType() {
        return LocalType;
    }

    public void setLocalType(int LocalType) {
        this.LocalType = LocalType;
    }

    public String getLocalTypeName() {
        return LocalTypeName;
    }

    public void setLocalTypeName(String LocalTypeName) {
        this.LocalTypeName = LocalTypeName;
    }

    public int getLocationID() {
        return LocationID;
    }

    public void setLocationID(int LocationID) {
        this.LocationID = LocationID;
    }

    public String getLot() {
        return Lot;
    }

    public void setLot(String Lot) {
        this.Lot = Lot;
    }

    public String getMapCode() {
        return MapCode;
    }

    public void setMapCode(String MapCode) {
        this.MapCode = MapCode;
    }

    public int getModem() {
        return Modem;
    }

    public void setModem(int Modem) {
        this.Modem = Modem;
    }

    public String getNameVilla() {
        return NameVilla;
    }

    public void setNameVilla(String NameVilla) {
        this.NameVilla = NameVilla;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String Note) {
        this.Note = Note;
    }

    public int getObjectType() {
        return ObjectType;
    }

    public void setObjectType(int ObjectType) {
        this.ObjectType = ObjectType;
    }

    public int getObjID() {
        return ObjID;
    }

    public void setObjID(int ObjID) {
        this.ObjID = ObjID;
    }

    public String getODCCableType() {
        return ODCCableType;
    }

    public void setODCCableType(String ODCCableType) {
        this.ODCCableType = ODCCableType;
    }

    public int getOutDoor() {
        return OutDoor;
    }

    public void setOutDoor(int OutDoor) {
        this.OutDoor = OutDoor;
    }

    public int getOutDType() {
        return OutDType;
    }

    public void setOutDType(int OutDType) {
        this.OutDType = OutDType;
    }

    public int getPartnerID() {
        return PartnerID;
    }

    public void setPartnerID(int PartnerID) {
        this.PartnerID = PartnerID;
    }

    public String getPassport() {
        return Passport;
    }

    public void setPassport(String Passport) {
        this.Passport = Passport;
    }

    public String getPaymentAbility() {
        return PaymentAbility;
    }

    public void setPaymentAbility(String PaymentAbility) {
        this.PaymentAbility = PaymentAbility;
    }

    public String getPhone_1() {
        return Phone_1;
    }

    public void setPhone_1(String Phone_1) {
        this.Phone_1 = Phone_1;
    }

    public String getPhone_2() {
        return Phone_2;
    }

    public void setPhone_2(String Phone_2) {
        this.Phone_2 = Phone_2;
    }

    public int getPosition() {
        return Position;
    }

    public void setPosition(int Position) {
        this.Position = Position;
    }

    public String getPromotionName() {
        return PromotionName;
    }

    public void setPromotionName(String PromotionName) {
        this.PromotionName = PromotionName;
    }

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int PromotionID) {
        this.PromotionID = PromotionID;
    }

    public String getRegCode() {
        return RegCode;
    }

    public void setRegCode(String RegCode) {
        this.RegCode = RegCode;
    }

    public String getRoom() {
        return Room;
    }

    public void setRoom(String Room) {
        this.Room = Room;
    }

    public int getStatusDeposit() {
        return StatusDeposit;
    }

    public void setStatusDeposit(int StatusDeposit) {
        this.StatusDeposit = StatusDeposit;
    }

    public String getSupporter() {
        return Supporter;
    }

    public void setSupporter(String Supporter) {
        this.Supporter = Supporter;
    }

    public String getTaxId() {
        return TaxId;
    }

    public void setTaxId(String TaxId) {
        this.TaxId = TaxId;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int Total) {
        this.Total = Total;
    }

    public int getType_1() {
        return Type_1;
    }

    public void setType_1(int Type_1) {
        this.Type_1 = Type_1;
    }

    public int getType_2() {
        return Type_2;
    }

    public void setType_2(int Type_2) {
        this.Type_2 = Type_2;
    }

    public int getTypeHouse() {
        return TypeHouse;
    }

    public void setTypeHouse(int TypeHouse) {
        this.TypeHouse = TypeHouse;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getTDName() {
        return TDName;
    }

    public void setTDName(String TDName) {
        this.TDName = TDName;
    }

    public void setEquipmentDelivery(int EquipmentDelivery) {
        this.EquipmentDelivery = EquipmentDelivery;
    }

    public int getEquipmentDelivery() {
        return this.EquipmentDelivery;
    }

    public void setPotentialID(int PotentialID) {
        this.PotentialID = PotentialID;
    }

    public int getPotentialID() {
        return this.PotentialID;
    }

    public String getTDLatLng() {
        return TDLatLng;
    }

    public void setTDLatLng(String TDLatLng) {
        this.TDLatLng = TDLatLng;
    }

    public String getTDAddress() {
        return TDAddress;
    }

    public void setTDAddress(String TDAddress) {
        this.TDAddress = TDAddress;
    }

    public String getBirthDay() {
        return Birthday;
    }

    public void setBirthDay(String BirthDay) {
        this.Birthday = BirthDay;
    }

    public String getAddressPassport() {
        return AddressPassport;
    }

    public void setAddressPassport(String AddressPassport) {
        this.AddressPassport = AddressPassport;
    }

    public void setPayment(int Payment) {
        this.Payment = Payment;
    }

    public int getPayment() {
        return this.Payment;
    }

    public void setPaymentTypeName(String PaymentTypeName) {
        this.PaymentName = PaymentTypeName;
    }

    public String getPaymentTypeName() {
        return this.PaymentName;
    }

    public int getIPTVFimPlusChargeTimes() {
        return IPTVFimPlusChargeTimes;
    }

    public void setIPTVFimPlusChargeTimes(int fimPlusChargeTimes) {
        this.IPTVFimPlusChargeTimes = fimPlusChargeTimes;
    }

    public int getIPTVFimPlusPrepaidMonth() {
        return IPTVFimPlusPrepaidMonth;
    }

    public void setIPTVFimPlusPrepaidMonth(int IPTVFimPlusPrepaidMonth) {
        this.IPTVFimPlusPrepaidMonth = IPTVFimPlusPrepaidMonth;
    }

    public int getIPTVFimHotChargeTimes() {
        return IPTVFimHotChargeTimes;
    }

    public void setIPTVFimHotChargeTimes(int IPTVFimHotChargeTimes) {
        this.IPTVFimHotChargeTimes = IPTVFimHotChargeTimes;
    }

    public int getIPTVFimHotPrepaidMonth() {
        return IPTVFimHotPrepaidMonth;
    }

    public void setIPTVFimHotPrepaidMonth(int IPTVFimHotPrepaidMonth) {
        this.IPTVFimHotPrepaidMonth = IPTVFimHotPrepaidMonth;
    }

    public int getIPTVPromotionIDBoxOrder() {
        return IPTVPromotionIDBoxOrder;
    }

    public void setIPTVPromotionIDBoxOther(int IPTVPromotionIDOrther) {
        this.IPTVPromotionIDBoxOrder = IPTVPromotionIDOrther;
    }

    public List<PackageModel> getListPackage() {
        return ListPackage;
    }

    public void setListPackage(List<PackageModel> listPackage) {
        ListPackage = listPackage;
    }

    public List<Device> getListDevice() {
        return ListDevice;
    }

    public void setListDevice(List<Device> listDevice) {
        ListDevice = listDevice;
    }

    public List<FPTBox> getListDeviceOTT() {
        return listDeviceOTT;
    }

    public void setListDeviceOTT(List<FPTBox> listDeviceOTT) {
        this.listDeviceOTT = listDeviceOTT;
    }

    public List<MacOTT> getListMACOTT() {
        return listMACOTT;
    }

    public void setListMACOTT(List<MacOTT> listMACOTT) {
        this.listMACOTT = listMACOTT;
    }

    public String getEmailAdmin() {
        return EmailAdmin;
    }

    public void setEmailAdmin(String emailAdmin) {
        EmailAdmin = emailAdmin;
    }

    public String getDomainName() {
        return DomainName;
    }

    public void setDomainName(String domainName) {
        DomainName = domainName;
    }

    public String getTechName() {
        return TechName;
    }

    public void setTechName(String techName) {
        TechName = techName;
    }

    public String getTechPhoneNumber() {
        return TechPhoneNumber;
    }

    public void setTechPhoneNumber(String techPhoneNumber) {
        TechPhoneNumber = techPhoneNumber;
    }

    public String getTechEmail() {
        return TechEmail;
    }

    public void setTechEmail(String techEmail) {
        TechEmail = techEmail;
    }


    public String getOffice365Total() {
        return Office365Total;
    }

    public void setOffice365Total(String office365Total) {
        Office365Total = office365Total;
    }

    public String getImageInfo() {
        return ImageInfo;
    }

    public void setImageInfo(String imageInfo) {
        ImageInfo = imageInfo;
    }

    public String getImageSignature() {
        return ImageSignature;
    }

    public void setImageSignature(String imageSignature) {
        ImageSignature = imageSignature;
    }

    public int getIPTVPromotionType() {
        return IPTVPromotionType;
    }

    public void setIPTVPromotionType(int IPTVPromotionType) {
        this.IPTVPromotionType = IPTVPromotionType;
    }

    public int getIPTVPromotionTypeBoxOrder() {
        return IPTVPromotionTypeBoxOrder;
    }

    public void setIPTVPromotionTypeBoxOrder(int IPTVPromotionTypeBoxOrder) {
        this.IPTVPromotionTypeBoxOrder = IPTVPromotionTypeBoxOrder;
    }

    public String getIPTVPromotionDesc() {
        return IPTVPromotionDesc;
    }

    public void setIPTVPromotionDesc(String IPTVPromotionDesc) {
        this.IPTVPromotionDesc = IPTVPromotionDesc;
    }

    public CheckListRegistration getCheckListRegistration() {
        return checkListRegistration;
    }

    public void setCheckListRegistration(CheckListRegistration checkListRegistration) {
        this.checkListRegistration = checkListRegistration;
    }

    public String getCameraTotal() {
        return cameraTotal;
    }

    public void setCameraTotal(String cameraTotal) {
        this.cameraTotal = cameraTotal;
    }

    public ArrayList<CameraSelected> getListCamera() {
        return listCamera;
    }

    public void setListCamera(ArrayList<CameraSelected> listCamera) {
        this.listCamera = listCamera;
    }

    public ArrayList<CloudPackageSelected> getListCloud() {
        return listCloud;
    }

    public void setListCloud(ArrayList<CloudPackageSelected> listCloud) {
        this.listCloud = listCloud;
    }

    public SetupDetail getSetupCamera() {
        return setupCamera;
    }

    public void setSetupCamera(SetupDetail setupCamera) {
        this.setupCamera = setupCamera;
    }

    public String getIPTVPromotionBoxOrderDesc() {
        return IPTVPromotionBoxOrderDesc;
    }

    public void setIPTVPromotionBoxOrderDesc(String IPTVPromotionBoxOrderDesc) {
        this.IPTVPromotionBoxOrderDesc = IPTVPromotionBoxOrderDesc;
    }


    public CheckListService getCheckListService() {
        return checkListService;
    }

    public void setCheckListService(CheckListService checkListService) {
        this.checkListService = checkListService;
    }

    public isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera getObjectCamera() {
        return ObjectCamera;
    }

    public void setObjectCamera(isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera objectCamera) {
        ObjectCamera = objectCamera;
    }

    public isc.fpt.fsale.model.ListDeviceMACOTT getListDeviceMACOTT() {
        return ListDeviceMACOTT;
    }

    public void setListDeviceMACOTT(isc.fpt.fsale.model.ListDeviceMACOTT listDeviceMACOTT) {
        ListDeviceMACOTT = listDeviceMACOTT;
    }

    public String getGiftName() {
        return GiftName;
    }

    public void setGiftName(String giftName) {
        this.GiftName = giftName;
    }

    public int getGiftID() {
        return GiftID;
    }

    public void setGiftID(int giftID) {
        this.GiftID = giftID;
    }

    public CartExtraOtt getObjectExtraOTT() {
        return ObjectExtraOTT;
    }

    public void setObjectExtraOTT(CartExtraOtt objectExtraOTT) {
        ObjectExtraOTT = objectExtraOTT;
    }

    public List<CategoryServiceList> getCategoryServiceList() {
        return CategoryServiceList;
    }

    public void setCategoryServiceList(List<CategoryServiceList> categoryServiceList) {
        CategoryServiceList = categoryServiceList;
    }

    public int getBaseObjID() {
        return BaseObjID;
    }

    public void setBaseObjID(int baseObjID) {
        BaseObjID = baseObjID;
    }

    public String getBaseContract() {
        return BaseContract;
    }

    public void setBaseContract(String baseContract) {
        BaseContract = baseContract;
    }

    public String getHTMLDetail() {
        return HTMLDetail;
    }

    public void setHTMLDetail(String HTMLDetail) {
        this.HTMLDetail = HTMLDetail;
    }

    public int getFoxy2ChargeTimes() {
        return Foxy2ChargeTimes;
    }

    public void setFoxy2ChargeTimes(int foxy2ChargeTimes) {
        Foxy2ChargeTimes = foxy2ChargeTimes;
    }

    public int getFoxy2PrepaidMonth() {
        return Foxy2PrepaidMonth;
    }

    public void setFoxy2PrepaidMonth(int foxy2PrepaidMonth) {
        Foxy2PrepaidMonth = foxy2PrepaidMonth;
    }

    public int getFoxy4ChargeTimes() {
        return Foxy4ChargeTimes;
    }

    public void setFoxy4ChargeTimes(int foxy4ChargeTimes) {
        Foxy4ChargeTimes = foxy4ChargeTimes;
    }

    public int getFoxy4PrepaidMonth() {
        return Foxy4PrepaidMonth;
    }

    public void setFoxy4PrepaidMonth(int foxy4PrepaidMonth) {
        Foxy4PrepaidMonth = foxy4PrepaidMonth;
    }

    public String getBaseImageInfo() {
        return BaseImageInfo;
    }

    public void setBaseImageInfo(String baseImageInfo) {
        BaseImageInfo = baseImageInfo;
    }

    public int getBaseRegIDImageInfo() {
        return BaseRegIDImageInfo;
    }

    public void setBaseRegIDImageInfo(int baseRegIDImageInfo) {
        BaseRegIDImageInfo = baseRegIDImageInfo;
    }

    public double getDeviceTotal() {
        return DeviceTotal;
    }

    public void setDeviceTotal(double deviceTotal) {
        DeviceTotal = deviceTotal;
    }

    public int getDiscountRP_VoucherTotal() {
        return DiscountRP_VoucherTotal;
    }

    public void setDiscountRP_VoucherTotal(int discountRP_VoucherTotal) {
        DiscountRP_VoucherTotal = discountRP_VoucherTotal;
    }

    public String getReferralCode() {
        return ReferralCode;
    }

    public void setReferralCode(String referralCode) {
        ReferralCode = referralCode;
    }

    public String getVoucherCode() {
        return VoucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        VoucherCode = voucherCode;
    }

    public String getBillTo_CityVN() {
        return BillTo_CityVN;
    }

    public void setBillTo_CityVN(String billTo_CityVN) {
        BillTo_CityVN = billTo_CityVN;
    }

    public String getBillTo_DistrictVN() {
        return BillTo_DistrictVN;
    }

    public void setBillTo_DistrictVN(String billTo_DistrictVN) {
        BillTo_DistrictVN = billTo_DistrictVN;
    }

    public String getBillTo_StreetVN() {
        return BillTo_StreetVN;
    }

    public void setBillTo_StreetVN(String billTo_StreetVN) {
        BillTo_StreetVN = billTo_StreetVN;
    }

    public String getBillTo_WardVN() {
        return BillTo_WardVN;
    }

    public void setBillTo_WardVN(String billTo_WardVN) {
        BillTo_WardVN = billTo_WardVN;
    }

    public String getNameVillaVN() {
        return NameVillaVN;
    }

    public void setNameVillaVN(String nameVillaVN) {
        NameVillaVN = nameVillaVN;
    }

    public isc.fpt.fsale.model.InternetTotal getServiceCodeInternetTotal() {
        return ServiceCodeInternetTotal;
    }

    public void setServiceCodeInternetTotal(isc.fpt.fsale.model.InternetTotal serviceCodeInternetTotal) {
        ServiceCodeInternetTotal = serviceCodeInternetTotal;
    }

    public IpTvTotal getServiceCodeIpTvTotal() {
        return ServiceCodeIpTvTotal;
    }

    public void setServiceCodeIpTvTotal(IpTvTotal serviceCodeIpTvTotal) {
        this.ServiceCodeIpTvTotal = serviceCodeIpTvTotal;
    }

    public String getReferralCodeDesc() {
        return ReferralCodeDesc;
    }

    public void setReferralCodeDesc(String referralCodeDesc) {
        ReferralCodeDesc = referralCodeDesc;
    }

    public String getVoucherCodeDesc() {
        return VoucherCodeDesc;
    }

    public void setVoucherCodeDesc(String voucherCodeDesc) {
        VoucherCodeDesc = voucherCodeDesc;
    }

    public Camera2Total getServiceCodeCameraTotal() {
        return ServiceCodeCameraTotal;
    }

    public void setServiceCodeCameraTotal(Camera2Total serviceCodeCameraTotal) {
        ServiceCodeCameraTotal = serviceCodeCameraTotal;
    }

    // Convert Object to Json bán mới
    public JSONObject toJSONObject() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("ID", this.getID());
            jsonObj.put("ObjID", this.getObjID());
            jsonObj.put("Contract", this.getContract());
            jsonObj.put("LocationID", this.getLocationID());
            jsonObj.put("LocalType", this.getLocalType());
            jsonObj.put("FullName", this.getFullName());
            jsonObj.put("Contact", this.getContact());
            jsonObj.put("BillTo_City", this.getBillTo_City());
            jsonObj.put("BillTo_District", this.getBillTo_District());
            jsonObj.put("BillTo_Ward", this.getBillTo_Ward());
            jsonObj.put("TypeHouse", this.getTypeHouse());
            jsonObj.put("BillTo_Street", this.getBillTo_Street());
            jsonObj.put("Lot", this.getLot());
            jsonObj.put("Floor", this.getFloor());
            jsonObj.put("Room", this.getRoom());
            jsonObj.put("NameVilla", this.getNameVilla());
            jsonObj.put("Position", this.getPosition());
            jsonObj.put("BillTo_Number", this.getBillTo_Number());
            jsonObj.put("Address", this.getAddress());
            jsonObj.put("Note", this.getNote());
            jsonObj.put("Passport", this.getPassport());
            jsonObj.put("TaxId", this.getTaxId());
            jsonObj.put("Phone_1", this.getPhone_1());
            jsonObj.put("Phone_2", this.getPhone_2());
            jsonObj.put("Type_1", this.getType_1());
            jsonObj.put("Type_2", this.getType_2());
            jsonObj.put("Contact_1", this.getContact_1());
            jsonObj.put("Contact_2", this.getContact_2());
            jsonObj.put("ISPType", this.getISPType());
            jsonObj.put("CusTypeDetail", this.getCusTypeDetail());
            jsonObj.put("CurrentHouse", this.getCurrentHouse());
            jsonObj.put("PaymentAbility", this.getPaymentAbility());
            jsonObj.put("PartnerID", this.getPartnerID());
            jsonObj.put("LegalEntity", this.getLegalEntity());
            jsonObj.put("Supporter", Constants.IBB_MEMBER_ID);
            jsonObj.put("PromotionID", this.getPromotionID());
            jsonObj.put("PromotionDesc", this.getPromotionName());
            jsonObj.put("Total", this.getTotal());
            jsonObj.put("UserName", this.getUserName());
            jsonObj.put("Deposit", this.getDeposit());
            jsonObj.put("DepositBlackPoint", this.getDepositBlackPoint());
            jsonObj.put("PromotionComboID", this.getPromotionComboID());
            jsonObj.put("ObjectType", this.getObjectType());
            jsonObj.put("CableStatus", this.getCableStatus());
            jsonObj.put("InsCable", this.getINSCable());
            jsonObj.put("CusType", this.getCusType());
            jsonObj.put("Email", this.getEmail());
            jsonObj.put("InternetTotal", this.getInternetTotal());
            jsonObj.put("DescriptionIBB", this.getDescriptionIBB());
            jsonObj.put("EquipmentDelivery", this.getEquipmentDelivery());
            jsonObj.put("PotentialID", this.getPotentialID());
            jsonObj.put("OutDoor", this.getOutDoor());
            jsonObj.put("InDoor", this.getInDoor());
            jsonObj.put("Birthday", getBirthday() == null ? "" :
                    Common.getSimpleDateFormat(getBirthday(), Constants.DATE_FORMAT));
            jsonObj.put("Payment", this.getPayment());
            jsonObj.put("AddressPassport", this.getAddressPassport());
            if (ListPackage != null && ListPackage.size() != 0) {
                JSONArray arr = new JSONArray();
                for (PackageModel item : this.ListPackage) {
                    arr.put(item.toJSONObject());
                }
                jsonObj.put("ListPackage", arr);
            }
            if (ListDevice != null && ListDevice.size() != 0) {
                // tuantt1420062017 danh sách thiết bị
                JSONArray arrDevices = new JSONArray();
                for (Device item : this.ListDevice) {
                    arrDevices.put(item.toJSONObjectRegisterV2());
                }
                jsonObj.put("ListDevice", arrDevices);
            }

            if (listDeviceOTT != null && listDeviceOTT.size() != 0 &&
                    listMACOTT != null && listMACOTT.size() != 0) {
                // tuantt14 22012018 danh sách fpt box
                JSONObject jsonLOtt = new JSONObject();
                JSONArray arrFPTBox = new JSONArray();
                for (FPTBox item : this.listDeviceOTT) {
                    arrFPTBox.put(item.toJSONObject());
                }
                jsonLOtt.put("listDeviceOTT", arrFPTBox);
                // tuantt14 23012018 danh sách Mac
                JSONArray arrMacList = new JSONArray();
                for (MacOTT item : this.listMACOTT) {
                    arrMacList.put(item.toJSONObject());
                }
                jsonLOtt.put("listMACOTT", arrMacList);
                jsonObj.put("lOtt", jsonLOtt);
            }
            jsonObj.put("EmailAdmin", this.getEmailAdmin());
            jsonObj.put("DomainName", this.getDomainName());
            jsonObj.put("TechName", this.getTechName());
            jsonObj.put("TechPhoneNumber", this.getTechPhoneNumber());
            jsonObj.put("TechEmail", this.getTechEmail());
            jsonObj.put("Office365Total", this.getOffice365Total());
            jsonObj.put("OTTBoxCount", this.getOTTBoxCount());
            jsonObj.put("OTTTotal", this.getOTTTotal());
            jsonObj.put("ImageInfo", this.getImageInfo());
            jsonObj.put("ImageSignature", this.getImageSignature());
            jsonObj.put("GiftName", this.getGiftName());
            jsonObj.put("GiftID", this.getGiftID());
            jsonObj.put("DeviceTotal", this.getDeviceTotal());
            jsonObj.put("TotalDiscountRP_Voucher", this.getDiscountRP_VoucherTotal());
            jsonObj.put("ReferralCode", this.getReferralCode() != null ? getReferralCode() : "");
            jsonObj.put("ReferralCodeDesc", this.getReferralCodeDesc() != null ? getReferralCodeDesc() : "");
            jsonObj.put("VoucherCode", this.getVoucherCode() != null ? getVoucherCode() : "");
            jsonObj.put("VoucherCodeDesc", this.getVoucherCodeDesc() != null ? getVoucherCodeDesc() : "");
            JSONArray jsonArray = new JSONArray();
            for (isc.fpt.fsale.model.CategoryServiceList item : CategoryServiceList) {
                jsonArray.put(item.getCategoryServiceID());
            }
            jsonObj.put("CategoryServices", jsonArray);

            JSONArray jsonArrayServiceCode = new JSONArray();
            for (ServiceCode item : ServiceCodeInternetTotal.getServiceCodes() != null ? ServiceCodeInternetTotal.getServiceCodes() : new ArrayList<ServiceCode>()) {
                jsonArrayServiceCode.put(item.toJSONObject());
            }

            for (ServiceCode item : ServiceCodeIpTvTotal.getServiceCodes() != null ? ServiceCodeIpTvTotal.getServiceCodes() : new ArrayList<ServiceCode>()) {
                jsonArrayServiceCode.put(item.toJSONObject());
            }

            for (ServiceCode item : ServiceCodeCameraTotal.getServiceCodes() != null ? ServiceCodeCameraTotal.getServiceCodes() : new ArrayList<ServiceCode>()) {
                jsonArrayServiceCode.put(item.toJSONObject());
            }

            for (ServiceCode item : ServiceCodeMaxyTotal.getMaxyService() != null ? ServiceCodeMaxyTotal.getMaxyService() : new ArrayList<ServiceCode>()){
                jsonArrayServiceCode.put(item.toJSONObject());
            }
            jsonObj.put("ServiceCodes", jsonArrayServiceCode);
            //camera 2 version 3.19;
            //-----------------------------------//
            jsonObj.put("IPTVUseCombo", this.getIPTVUseCombo());
            jsonObj.put("IPTVRequestSetUp", this.getIPTVRequestSetUp());
            jsonObj.put("IPTVRequestDrillWall", this.getIPTVRequestDrillWall());
            jsonObj.put("IPTVStatus", this.getIPTVStatus());
            jsonObj.put("IPTVPromotionID", this.getIPTVPromotionID());
            jsonObj.put("IPTVPackage", this.getIPTVPackage());
            jsonObj.put("IPTVBoxCount", this.getIPTVBoxCount());
            jsonObj.put("IPTVPLCCount", this.getIPTVPLCCount());
            jsonObj.put("IPTVReturnSTBCount", this.getIPTVReturnSTBCount());
            jsonObj.put("IPTVChargeTimes", this.getIPTVChargeTimes());
            jsonObj.put("IPTVPrepaid", this.getIPTVPrepaid());
            jsonObj.put("IPTVHBOPrepaidMonth", this.getIPTVHBOPrepaidMonth());
            jsonObj.put("IPTVHBOChargeTimes", this.getIPTVHBOChargeTimes());
            jsonObj.put("IPTVKPlusPrepaidMonth", this.getIPTVKPlusPrepaidMonth());
            jsonObj.put("IPTVKPlusChargeTimes", this.getIPTVKPlusChargeTimes());
            jsonObj.put("IPTVVTCPrepaidMonth", this.getIPTVVTCPrepaidMonth());
            jsonObj.put("IPTVVTCChargeTimes", this.getIPTVVTCChargeTimes());
            jsonObj.put("IPTVDeviceTotal", this.getIPTVDeviceTotal());
            jsonObj.put("IPTVPrepaidTotal", this.getIPTVPrepaidTotal());
            jsonObj.put("IPTVTotal", this.getIPTVTotal());
            jsonObj.put("IPTVFimPlusChargeTimes", this.getIPTVFimPlusChargeTimes());
            jsonObj.put("IPTVFimPlusPrepaidMonth", this.getIPTVFimPlusPrepaidMonth());
            jsonObj.put("IPTVFimHotChargeTimes", this.getIPTVFimHotChargeTimes());
            jsonObj.put("IPTVFimHotPrepaidMonth", this.getIPTVFimHotPrepaidMonth());
            jsonObj.put("IPTVPromotionIDBoxOrder", this.getIPTVPromotionIDBoxOrder());
            jsonObj.put("IPTVPromotionType", this.getIPTVPromotionType());
            jsonObj.put("IPTVPromotionDesc", this.getIPTVPromotionDesc());
            jsonObj.put("IPTVPromotionTypeBoxOrder", this.getIPTVPromotionTypeBoxOrder());
            jsonObj.put("IPTVPromotionBoxOrderDesc", this.getIPTVPromotionBoxOrderDesc());
            jsonObj.put("IPTVFimPlusStdChargeTimes", this.getIPTVFimPlusStdChargeTimes());
            jsonObj.put("IPTVFimPlusStdPrepaidMonth", this.getIPTVFimPlusStdPrepaidMonth());
            jsonObj.put("Foxy2ChargeTimes", this.getFoxy2ChargeTimes());
            jsonObj.put("Foxy2PrepaidMonth", this.getFoxy2PrepaidMonth());
            jsonObj.put("Foxy4ChargeTimes", this.getFoxy4ChargeTimes());
            jsonObj.put("Foxy4PrepaidMonth", this.getFoxy4PrepaidMonth());
            //----------------------------------//
            jsonObj.put("CameraServices", getObjectCameraOfNet().toJsonObjectUpdate());
            //ver 3.21
            jsonObj.put("MaxyService", getObjectMaxy().toJsonObjectUpdate());
            //ver 3.23
            jsonObj.put("SourceType", this.getSourceType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    //json tính tổng tiền e-voucher & RP
    public JSONObject toJSONObjectEVoucher() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("Passport", getPassport());
            jsonObject.put("PhoneNumber", getPhone_1());
            jsonObject.put("RegType", 0); // 0 : bán mới, 1 : bán thêm
            jsonObject.put("LocationID", Constants.LOCATION_ID);
            jsonObject.put("RegCode", getRegCode());
            jsonObject.put("Contract", getContract());
            jsonObject.put("LocalType", getLocalType());
            jsonObject.put("NetPromotionID", getPromotionID());
            jsonObject.put("ReferralCode", getReferralCode());
            jsonObject.put("VoucherCode", getVoucherCode());
//            JSONArray jsonArrayServiceCode = new JSONArray();
//            for (ServiceCode item : ServiceCodeInternetTotal.getServiceCodes() != null ?
//                    ServiceCodeInternetTotal.getServiceCodes() : new ArrayList<ServiceCode>()) {
//                jsonArrayServiceCode.put(item.toJSONObject());
//            }
//
//            for (ServiceCode item : ServiceCodeIpTvTotal.getServiceCodes() != null ?
//                    ServiceCodeIpTvTotal.getServiceCodes() : new ArrayList<ServiceCode>()) {
//                jsonArrayServiceCode.put(item.toJSONObject());
//            }
//
//            jsonObject.put("ServiceCodes", jsonArrayServiceCode);

            /*
                author: nhannh26
                day: 11/09/2021
                version: 3.22 ++
            */
            //service code intenet
            JSONArray jsonArrayServiceCodeIntenet = new JSONArray();
            for (ServiceCode item : ServiceCodeInternetTotal.getServiceCodes() != null
                    ? ServiceCodeInternetTotal.getServiceCodes() : new ArrayList<ServiceCode>()) {
                jsonArrayServiceCodeIntenet.put(item.toJSONObject());
            }
            jsonObject.put("ServiceCodesNet", jsonArrayServiceCodeIntenet);
            //service code maxy
            JSONArray jsonArrayServiceCodeMaxy = new JSONArray();
            for (ServiceCode item : ServiceCodeMaxyTotal.getMaxyService() != null
                    ? ServiceCodeMaxyTotal.getMaxyService() : new ArrayList<ServiceCode>()) {
                jsonArrayServiceCodeMaxy.put(item.toJSONObject());
            }
            jsonObject.put("ServiceCodesMaxy", jsonArrayServiceCodeMaxy);
            JSONArray jsonArray = new JSONArray();
            for (isc.fpt.fsale.model.CategoryServiceList item : getCategoryServiceList()) {
                jsonArray.put(item.getCategoryServiceID());
            }
            jsonObject.put("CategoryServices", jsonArray);
            jsonObject.put("IPTVPromotionID", getObjectMaxy().getMaxyGeneral().getPromotionID());
            jsonObject.put("IPTVPromotionType", getObjectMaxy().getMaxyGeneral().getPromotionType());

//            for (isc.fpt.fsale.model.CategoryServiceList item : getCategoryServiceList()) {
//                if (item.getCategoryServiceID() == 4) {//IPTV
//                    jsonObject.put("IPTVPromotionID", getIPTVPromotionID());
//                    jsonObject.put("IPTVPromotionType", getIPTVPromotionType());
//                    jsonObject.put("IPTVPromotionIDBoxOrder", getIPTVPromotionIDBoxOrder());
//                    jsonObject.put("IPTVPromotionTypeBoxOrder", getIPTVPromotionTypeBoxOrder());
//                } else {
//                    jsonObject.put("IPTVPromotionID", 0);
//                    jsonObject.put("IPTVPromotionType", 0);
//                    jsonObject.put("IPTVPromotionIDBoxOrder", 0);
//                    jsonObject.put("IPTVPromotionTypeBoxOrder", 0);
//                }
//            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    //json lấy danh sách e-voucher
    public JSONObject toJSONObjectGetListEVoucher() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("Passport", getPassport());
            jsonObject.put("PhoneNumber", getPhone_1());
            jsonObject.put("RegType", 0); // 0 : bán mới, 1 : bán thêm
            jsonObject.put("LocationID", Constants.LOCATION_ID);
            JSONArray jsonArray = new JSONArray();
            for (isc.fpt.fsale.model.CategoryServiceList item : CategoryServiceList) {
                jsonArray.put(item.getCategoryServiceID());
            }
            jsonObject.put("CategoryServices", jsonArray);
            jsonObject.put("LocalType", getLocalType());
            jsonObject.put("NetPromotionID", getPromotionID());
            boolean isIpTV = false;
            for (isc.fpt.fsale.model.CategoryServiceList item : CategoryServiceList) {
                if (item.getCategoryServiceID() == 4) {//IPTV
                    jsonObject.put("IPTVPromotionID", getIPTVPromotionID());
                    jsonObject.put("IPTVPromotionIDBoxOrder", getIPTVPromotionIDBoxOrder());
                    jsonObject.put("IPTVPromotionType", getIPTVPromotionType());
                    jsonObject.put("IPTVBoxCount", getIPTVBoxCount());
                    isIpTV = true;
                } else {
                    if (!isIpTV) {
                        jsonObject.put("IPTVPromotionID", 0);
                        jsonObject.put("IPTVPromotionIDBoxOrder", 0);
                        jsonObject.put("IPTVPromotionType", 0);
                        jsonObject.put("IPTVBoxCount", 0);
                        isIpTV = false;
                    }
                }
            }
//            JSONArray jsonArrayServiceCode = new JSONArray();
//            for (ServiceCode item : ServiceCodeInternetTotal.getServiceCodes() != null
//                    ? ServiceCodeInternetTotal.getServiceCodes() : new ArrayList<ServiceCode>()) {
//                jsonArrayServiceCode.put(item.toJSONObject());
//            }
//            for (ServiceCode item : ServiceCodeIpTvTotal.getServiceCodes() != null
//                    ? ServiceCodeIpTvTotal.getServiceCodes() : new ArrayList<ServiceCode>()) {
//                jsonArrayServiceCode.put(item.toJSONObject());
//            }
//            for (ServiceCode item : ServiceCodeCameraTotal.getServiceCodes() != null
//                    ? ServiceCodeCameraTotal.getServiceCodes() : new ArrayList<ServiceCode>()) {
//                jsonArrayServiceCode.put(item.toJSONObject());
//            }
//            jsonObject.put("ServiceCodes", jsonArrayServiceCode);
            /*
                author: nhannh26
                day: 11/09/2021
                version: 3.22 ++
            */
            //service code intenet
            JSONArray jsonArrayServiceCodeIntenet = new JSONArray();
            for (ServiceCode item : ServiceCodeInternetTotal.getServiceCodes() != null
                    ? ServiceCodeInternetTotal.getServiceCodes() : new ArrayList<ServiceCode>()) {
                jsonArrayServiceCodeIntenet.put(item.toJSONObject());
            }
            jsonObject.put("ServiceCodesNet", jsonArrayServiceCodeIntenet);
            //service code maxy
            JSONArray jsonArrayServiceCodeMaxy = new JSONArray();
            for (ServiceCode item : ServiceCodeMaxyTotal.getMaxyService() != null
                    ? ServiceCodeMaxyTotal.getMaxyService() : new ArrayList<ServiceCode>()) {
                jsonArrayServiceCodeMaxy.put(item.toJSONObject());
            }
            jsonObject.put("ServiceCodesMaxy", jsonArrayServiceCodeMaxy);
            jsonObject.put("IPTVPromotionID", getObjectMaxy().getMaxyGeneral().getPromotionID());
            jsonObject.put("IPTVPromotionType", getObjectMaxy().getMaxyGeneral().getPromotionType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public JSONObject toJSONObjectGetCameraTotal() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("objectCameraOfNet", getObjectCameraOfNet().toJsonObject());
            jsonObject.put("CameraTotal", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public JSONObject  toJSONObjectGetMaxyTotal() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("RegType", getRegType());
            jsonObject.put("LocationID", Constants.LST_REGION.get(0).getsID());
            jsonObject.put("BranchCode", Constants.BRANCH_CODE);
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("Contract", this.getContract());
            jsonObject.put("MaxyService", getObjectMaxy().toJsonObjectUpdate());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public JSONObject toJSONObjectGetInternetTotal() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("PromotionID", getPromotionID());
            jsonObject.put("LocalType", getLocalType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    //json tính tổng tiền iptv (bán mới)
    public JSONObject toJSONObjectGetIpTvTotal() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("PhoneNumber", getPhone_1());
            jsonObject.put("Passport", getPassport());
            jsonObject.put("RegType", getRegType());
            jsonObject.put("LocalType", getLocalType());
            jsonObject.put("RegCode", getRegCode());
            jsonObject.put("Contract", getContract());
            jsonObject.put("LocationID", Constants.LOCATION_ID);
            jsonObject.put("IPTVPromotionID", getIPTVPromotionID());
            jsonObject.put("IPTVPromotionType", getIPTVPromotionType());
            jsonObject.put("IPTVPromotionIDBoxOrder", getIPTVPromotionIDBoxOrder());
            jsonObject.put("IPTVPromotionTypeBoxOrder", getIPTVPromotionTypeBoxOrder());
            jsonObject.put("IPTVPackage", getIPTVPackage());
            jsonObject.put("IPTVBoxCount", getIPTVBoxCount());
            jsonObject.put("IPTVPLCCount", getIPTVPLCCount());
            jsonObject.put("IPTVReturnSTBCount", getIPTVReturnSTBCount());
            jsonObject.put("IPTVKPlusPrepaidMonth", getIPTVKPlusPrepaidMonth());
            jsonObject.put("IPTVKPlusChargeTimes", getIPTVKPlusChargeTimes());
            jsonObject.put("IPTVVTCPrepaidMonth", getIPTVVTCPrepaidMonth());
            jsonObject.put("IPTVVTCChargeTimes", getIPTVVTCChargeTimes());
            jsonObject.put("IPTVHBOPrepaidMonth", getIPTVHBOPrepaidMonth());
            jsonObject.put("IPTVHBOChargeTimes", getIPTVHBOChargeTimes());
            jsonObject.put("IPTVFimPlusPrepaidMonth", getIPTVFimPlusPrepaidMonth());
            jsonObject.put("IPTVFimPlusChargeTimes", getIPTVFimPlusChargeTimes());
            jsonObject.put("IPTVFimHotPrepaidMonth", getIPTVFimHotPrepaidMonth());
            jsonObject.put("IPTVFimHotChargeTimes", getIPTVFimHotChargeTimes());
            jsonObject.put("IPTVFimPlusStdPrepaidMonth", getIPTVFimPlusStdPrepaidMonth());
            jsonObject.put("IPTVFimPlusStdChargeTimes", getIPTVFimPlusStdChargeTimes());
            jsonObject.put("IPTVFoxy2PrepaidMonth", getFoxy2PrepaidMonth());
            jsonObject.put("IPTVFoxy2ChargeTimes", getFoxy2ChargeTimes());
            jsonObject.put("IPTVFoxy4PrepaidMonth", getFoxy4PrepaidMonth());
            jsonObject.put("IPTVFoxy4ChargeTimes", getFoxy4ChargeTimes());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    //json tính tổng tiền iptv (bán thêm)
    public JSONObject toJSONObjectGetIpTvTotal(String phone1, String contract, String regCode, int IPTVPromotionType, int IPTVPromotionTypeBoxOrder) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("PhoneNumber", phone1);
            jsonObject.put("Passport", getPassport());
            jsonObject.put("RegType", 1);
            jsonObject.put("LocalType", getLocalType());
            jsonObject.put("RegCode", regCode);
            jsonObject.put("Contract", contract);
            jsonObject.put("LocationID", Constants.LOCATION_ID);
            jsonObject.put("IPTVPromotionID", getIPTVPromotionID());
            jsonObject.put("IPTVPromotionType", IPTVPromotionType);
            jsonObject.put("IPTVPromotionIDBoxOrder", getIPTVPromotionIDBoxOrder());
            jsonObject.put("IPTVPromotionTypeBoxOrder", IPTVPromotionTypeBoxOrder);
            jsonObject.put("IPTVPackage", getIPTVPackage());
            jsonObject.put("IPTVBoxCount", getIPTVBoxCount());
            jsonObject.put("IPTVPLCCount", getIPTVPLCCount());
            jsonObject.put("IPTVReturnSTBCount", getIPTVReturnSTBCount());
            jsonObject.put("IPTVKPlusPrepaidMonth", getIPTVKPlusPrepaidMonth());
            jsonObject.put("IPTVKPlusChargeTimes", getIPTVKPlusChargeTimes());
            jsonObject.put("IPTVVTCPrepaidMonth", getIPTVVTCPrepaidMonth());
            jsonObject.put("IPTVVTCChargeTimes", getIPTVVTCChargeTimes());
            jsonObject.put("IPTVHBOPrepaidMonth", getIPTVHBOPrepaidMonth());
            jsonObject.put("IPTVHBOChargeTimes", getIPTVHBOChargeTimes());
            jsonObject.put("IPTVFimPlusPrepaidMonth", getIPTVFimPlusPrepaidMonth());
            jsonObject.put("IPTVFimPlusChargeTimes", getIPTVFimPlusChargeTimes());
            jsonObject.put("IPTVFimHotPrepaidMonth", getIPTVFimHotPrepaidMonth());
            jsonObject.put("IPTVFimHotChargeTimes", getIPTVFimHotChargeTimes());
            jsonObject.put("IPTVFimPlusStdPrepaidMonth", getIPTVFimPlusStdPrepaidMonth());
            jsonObject.put("IPTVFimPlusStdChargeTimes", getIPTVFimPlusStdChargeTimes());
            jsonObject.put("IPTVFoxy2PrepaidMonth", getFoxy2PrepaidMonth());
            jsonObject.put("IPTVFoxy2ChargeTimes", getFoxy2ChargeTimes());
            jsonObject.put("IPTVFoxy4PrepaidMonth", getFoxy4PrepaidMonth());
            jsonObject.put("IPTVFoxy4ChargeTimes", getFoxy4ChargeTimes());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Bán thêm", jsonObject.toString());
        return jsonObject;
    }

    //json object tính tổng tiền fpt play
    public JSONObject toJSONObjectGetFptPlayTotal() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("BillTo_District", getBillTo_District());
            JSONArray jsonArray = new JSONArray();
            for (FPTBox item : getListDeviceOTT()) {
                jsonArray.put(item.toJSONObject());
            }
            jsonObject.put("lOtt", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    //json lấy DS hình thức thanh toán
    public JSONObject toJSONObjectGetPaymentMethodList() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("BillTo_City", Constants.LOCATION_NAME);
            jsonObject.put("BillTo_District", getBillTo_District());
            jsonObject.put("BillTo_Ward", getBillTo_Ward());
            jsonObject.put("BillTo_Street", getBillTo_Street());
            jsonObject.put("BillTo_NameVilla", getNameVilla());
            jsonObject.put("BillTo_Number", getBillTo_Number());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static RegistrationDetailModel Parse(JSONObject json) {
        RegistrationDetailModel object = new RegistrationDetailModel();
        try {
            Field[] fields = object.getClass().getDeclaredFields();
            for (Field field : fields) {
                String propertiesName = field.getName();
                if (json.has(propertiesName)) {
                    field.setAccessible(true);
                    if (field.getType().getSimpleName().equals("String"))
                        field.set(object, json.getString(propertiesName));
                    else if (field.getType().getSimpleName().equals("Integer"))
                        field.set(object, json.getInt(propertiesName));
                }
            }
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Field[] fields = getClass().getDeclaredFields();
        for (Field f : fields) {
            sb.append(f.getName());
            sb.append(": ");
            try {
                sb.append("[");
                sb.append(f.get(this));
                sb.append("]");
            } catch (Exception e) {
                e.printStackTrace();
            }
            sb.append(", ");
            sb.append('\n');
        }
        return sb.toString();
    }

    // Convert Object to Json Bán thêm
    public JSONObject toJsonUpdateRegisterContract() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("UserName", this.getUserName());
            jsonObj.put("RegCode", this.getRegCode());
            jsonObj.put("Contract", this.getContract());
            jsonObj.put("Email", this.getEmail());
            jsonObj.put("Phone_1", this.getPhone_1());
            jsonObj.put("Type_1", this.getType_1());
            jsonObj.put("Contact_1", this.getContact_1());
            jsonObj.put("BillTo_City", this.getBillTo_City());
            jsonObj.put("BillTo_District", this.getBillTo_District());
            jsonObj.put("BillTo_Ward", this.getBillTo_Ward());
            jsonObj.put("BillTo_Street", this.getBillTo_Street());
            jsonObj.put("BillTo_Number", this.getBillTo_Number());
            jsonObj.put("BranchCode", Constants.BRANCH_CODE);
            jsonObj.put("TypeHouse", this.getTypeHouse());
            jsonObj.put("Lot", this.getLot());
            jsonObj.put("Floor", this.getFloor());
            jsonObj.put("Room", this.getRoom());
            jsonObj.put("NameVilla", this.getNameVilla());
            jsonObj.put("Position", this.getPosition());
            jsonObj.put("NoteAddress", this.getNote());
            jsonObj.put("DescriptionIBB", this.getDescriptionIBB());
            jsonObj.put("InDoor", this.getInDoor());
            jsonObj.put("OutDoor", this.getOutDoor());
            jsonObj.put("Payment", this.getPayment());
            jsonObj.put("LocalType", this.getLocalType());
            jsonObj.put("LocationID", this.getLocationID());
            jsonObj.put("PromotionID", this.getPromotionID());
            jsonObj.put("PromotionDesc", this.getPromotionName());
            jsonObj.put("PromotionComboID", this.getPromotionComboID());
            jsonObj.put("IPTVUseCombo", this.getIPTVUseCombo());
            jsonObj.put("IPTVRequestSetUp", this.getIPTVRequestSetUp());
            jsonObj.put("IPTVRequestDrillWall", this.getIPTVRequestDrillWall());
            jsonObj.put("IPTVStatus", this.getIPTVStatus());
            jsonObj.put("IPTVPromotionID", this.getIPTVPromotionID());
            jsonObj.put("IPTVPackage", this.getIPTVPackage());
            jsonObj.put("IPTVBoxCount", this.getIPTVBoxCount());
            jsonObj.put("IPTVPLCCount", this.getIPTVPLCCount());
            jsonObj.put("IPTVReturnSTBCount", this.getIPTVReturnSTBCount());
            jsonObj.put("IPTVChargeTimes", this.getIPTVChargeTimes());
            jsonObj.put("IPTVPrepaid", this.getIPTVPrepaid());
            jsonObj.put("IPTVKPlusPrepaidMonth", this.getIPTVKPlusPrepaidMonth());
            jsonObj.put("IPTVKPlusChargeTimes", this.getIPTVKPlusChargeTimes());
            jsonObj.put("IPTVVTCPrepaidMonth", this.getIPTVVTCPrepaidMonth());
            jsonObj.put("IPTVVTCChargeTimes", this.getIPTVVTCChargeTimes());
            jsonObj.put("IPTVDeviceTotal", this.getIPTVDeviceTotal());
            jsonObj.put("IPTVPrepaidTotal", this.getIPTVPrepaidTotal());
            jsonObj.put("iFimPlusPrepaidMonth", this.getIPTVFimPlusPrepaidMonth());
            jsonObj.put("iFimPlusChargeTimes", this.getIPTVFimPlusChargeTimes());
            jsonObj.put("iFimHotPrepaidMonth", this.getIPTVFimHotPrepaidMonth());
            jsonObj.put("iFimHotChargeTimes", this.getIPTVFimHotChargeTimes());
            jsonObj.put("iIPTVPromotionIDBoxOrder", this.getIPTVPromotionIDBoxOrder());
            jsonObj.put("IPTVPromotionBoxOrderDesc", this.getIPTVPromotionBoxOrderDesc());
            jsonObj.put("IPTVPromotionType", this.getIPTVPromotionType());
            jsonObj.put("IPTVPromotionDesc", this.getIPTVPromotionDesc());
            jsonObj.put("IPTVPromotionTypeBoxOrder", this.getIPTVPromotionTypeBoxOrder());
            jsonObj.put("IPTVTotal", this.getIPTVTotal());
            jsonObj.put("InternetTotal", this.getInternetTotal());
            jsonObj.put("Total", this.getTotal());
            jsonObj.put("IPTVFimPlusStdChargeTimes", this.getIPTVFimPlusStdChargeTimes());
            jsonObj.put("IPTVFimPlusStdPrepaidMonth", this.getIPTVFimPlusStdPrepaidMonth());
            JSONArray arr = new JSONArray();
            if (this.ListPackage != null) {
                for (PackageModel item : this.ListPackage) {
                    arr.put(item.toJSONObject());
                }
            }
            try {
                jsonObj.put("ListPackage", arr);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray arrDevices = new JSONArray();
            for (Device item : this.ListDevice) {
                arrDevices.put(item.toJSONObjectRegister());
            }
            jsonObj.put("ListDevice", arrDevices);
            jsonObj.put("EmailAdmin", this.getEmailAdmin());
            jsonObj.put("DomainName", this.getDomainName());
            jsonObj.put("TechName", this.getTechName());
            jsonObj.put("TechPhoneNumber", this.getTechPhoneNumber());
            jsonObj.put("TechEmail", this.getTechEmail());
            jsonObj.put("Office365Total", this.getOffice365Total());
            jsonObj.put("OTTBoxCount", this.getOTTBoxCount());
            jsonObj.put("OTTTotal", this.getOTTTotal());
            jsonObj.put("ImageInfo", this.getImageInfo());
            jsonObj.put("ImageSignature", this.getImageSignature());
            jsonObj.put("GiftName", this.getGiftName());
            jsonObj.put("GiftID", this.getGiftID());
            jsonObj.put("Foxy2ChargeTimes", this.getFoxy2ChargeTimes());
            jsonObj.put("Foxy2PrepaidMonth", this.getFoxy2PrepaidMonth());
            jsonObj.put("Foxy4ChargeTimes", this.getFoxy4ChargeTimes());
            jsonObj.put("Foxy4PrepaidMonth", this.getFoxy4PrepaidMonth());
            jsonObj.put("ObjID", this.getObjID());
            jsonObj.put("DeviceTotal", this.getDeviceTotal());
            jsonObj.put("Supporter", this.getSupporter() == null ? Constants.IBB_MEMBER_ID : getSupporter());
            jsonObj.put("MaxyService", getObjectMaxy().toJsonObjectUpdate() == null ?
                    new JSONObject() : getObjectMaxy().toJsonObjectUpdate());
            jsonObj.put("CameraOfNetServices", getObjectCameraOfNet().toJsonObjectUpdate() == null ?
                    new JSONObject() : getObjectCameraOfNet().toJsonObjectUpdate());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getCategoryServiceGroupID() {
        return CategoryServiceGroupID;
    }

    public void setCategoryServiceGroupID(int categoryServiceGroupID) {
        CategoryServiceGroupID = categoryServiceGroupID;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Address);
        parcel.writeString(AddressOrigin);
        parcel.writeString(BillTo_City);
        parcel.writeString(BillTo_District);
        parcel.writeString(BillTo_Number);
        parcel.writeString(BillTo_Street);
        parcel.writeString(BillTo_Ward);
        parcel.writeInt(BookPortType);
        parcel.writeInt(BranchCode);
        parcel.writeString(CableStatus);
        parcel.writeString(Contact);
        parcel.writeString(Contact_1);
        parcel.writeString(Contact_2);
        parcel.writeString(Contract);
        parcel.writeString(CreateBy);
        parcel.writeString(CreateDate);
        parcel.writeString(CurrentHouse);
        parcel.writeInt(CusType);
        parcel.writeString(CusTypeDetail);
        parcel.writeInt(Deposit);
        parcel.writeInt(DepositBlackPoint);
        parcel.writeString(Description);
        parcel.writeString(DescriptionIBB);
        parcel.writeInt(DivisionID);
        parcel.writeString(Email);
        parcel.writeInt(EoC);
        parcel.writeString(Floor);
        parcel.writeString(FullName);
        parcel.writeInt(ID);
        parcel.writeString(Image);
        parcel.writeInt(InDoor);
        parcel.writeInt(InDType);
        parcel.writeInt(INSCable);
        parcel.writeInt(InternetTotal);
        parcel.writeInt(IPTVBoxCount);
        parcel.writeInt(IPTVChargeTimes);
        parcel.writeInt(IPTVDeviceTotal);
        parcel.writeInt(IPTVHBOChargeTimes);
        parcel.writeInt(IPTVHBOPrepaidMonth);
        parcel.writeInt(IPTVKPlusChargeTimes);
        parcel.writeInt(IPTVKPlusPrepaidMonth);
        parcel.writeString(IPTVPackage);
        parcel.writeInt(IPTVPLCCount);
        parcel.writeInt(IPTVPrepaid);
        parcel.writeInt(IPTVPrepaidTotal);
        parcel.writeInt(IPTVPromotionID);
        parcel.writeInt(IPTVRequestDrillWall);
        parcel.writeInt(IPTVRequestSetUp);
        parcel.writeInt(IPTVReturnSTBCount);
        parcel.writeInt(IPTVStatus);
        parcel.writeInt(IPTVTotal);
        parcel.writeInt(IPTVUseCombo);
        parcel.writeInt(IPTVVTCChargeTimes);
        parcel.writeInt(IPTVVTCPrepaidMonth);
        parcel.writeInt(IPTVVTVChargeTimes);
        parcel.writeInt(IPTVVTVPrepaidMonth);
        parcel.writeInt(ISPType);
        parcel.writeString(Latlng);
        parcel.writeInt(LegalEntity);
        parcel.writeInt(LocalType);
        parcel.writeString(LocalTypeName);
        parcel.writeInt(LocationID);
        parcel.writeString(Lot);
        parcel.writeString(MapCode);
        parcel.writeInt(Modem);
        parcel.writeString(NameVilla);
        parcel.writeString(NameVillaVN);
        parcel.writeString(Note);
        parcel.writeInt(ObjectType);
        parcel.writeInt(ObjID);
        parcel.writeString(ODCCableType);
        parcel.writeInt(OutDoor);
        parcel.writeInt(OutDType);
        parcel.writeInt(PartnerID);
        parcel.writeString(Passport);
        parcel.writeString(PaymentAbility);
        parcel.writeString(Phone_1);
        parcel.writeString(Phone_2);
        parcel.writeInt(Position);
        parcel.writeString(PromotionName);
        parcel.writeInt(PromotionID);
        parcel.writeString(RegCode);
        parcel.writeString(Room);
        parcel.writeInt(StatusDeposit);
        parcel.writeString(Supporter);
        parcel.writeString(TaxId);
        parcel.writeInt(Total);
        parcel.writeInt(Type_1);
        parcel.writeInt(Type_2);
        parcel.writeInt(TypeHouse);
        parcel.writeString(UserName);
        parcel.writeString(TDName);
        parcel.writeInt(EquipmentDelivery);
        parcel.writeInt(PotentialID);
        parcel.writeString(TDLatLng);
        parcel.writeString(TDAddress);
        parcel.writeString(Birthday);
        parcel.writeInt(Payment);
        parcel.writeString(AddressPassport);
        parcel.writeString(PaymentName);
        parcel.writeInt(IPTVFimPlusChargeTimes);
        parcel.writeInt(IPTVFimPlusPrepaidMonth);
        parcel.writeInt(IPTVFimHotChargeTimes);
        parcel.writeInt(IPTVFimHotPrepaidMonth);
        parcel.writeInt(IPTVFimPlusStdChargeTimes);
        parcel.writeInt(IPTVFimPlusStdPrepaidMonth);
        parcel.writeInt(IPTVPromotionIDBoxOrder);
        parcel.writeInt(PromotionComboID);
        parcel.writeString(PromotionComboName);
        parcel.writeInt(RegType);
        parcel.writeInt(ContractServiceType);
        parcel.writeInt(ContractLocalType);
        parcel.writeInt(ContractPromotionID);
        parcel.writeInt(ContractComboStatus);
        parcel.writeInt(IsNewAddress);
        parcel.writeInt(OTTTotal);
        parcel.writeInt(OTTBoxCount);
        parcel.writeString(ContractPromotionName);
        parcel.writeString(ContractServiceTypeName);
        parcel.writeTypedList(ListPackage);
        parcel.writeTypedList(ListDevice);
        parcel.writeTypedList(listDeviceOTT);
        parcel.writeTypedList(listMACOTT);
        parcel.writeString(EmailAdmin);
        parcel.writeString(DomainName);
        parcel.writeString(TechName);
        parcel.writeString(TechPhoneNumber);
        parcel.writeString(TechEmail);
        parcel.writeString(Office365Total);
        parcel.writeString(ImageInfo);
        parcel.writeString(ImageSignature);
        parcel.writeInt(IPTVPromotionType);
        parcel.writeInt(IPTVPromotionTypeBoxOrder);
        parcel.writeString(IPTVPromotionDesc);
        parcel.writeParcelable(checkListRegistration, i);
        parcel.writeString(IPTVPromotionBoxOrderDesc);
        parcel.writeString(cameraTotal);
        parcel.writeTypedList(listCamera);
        parcel.writeTypedList(listCloud);
        parcel.writeParcelable(setupCamera, i);
        parcel.writeParcelable(checkListService, i);
        parcel.writeParcelable(ListDeviceMACOTT, i);
        parcel.writeString(GiftName);
        parcel.writeInt(GiftID);
        parcel.writeParcelable(ObjectExtraOTT, i);
        parcel.writeTypedList(CategoryServiceList);
        parcel.writeInt(BaseObjID);
        parcel.writeString(BaseContract);
        parcel.writeString(HTMLDetail);
        parcel.writeParcelable(ObjectCamera, i);
        parcel.writeParcelable(ObjectCameraOfNet, i);
        parcel.writeInt(Foxy2ChargeTimes);
        parcel.writeInt(Foxy2PrepaidMonth);
        parcel.writeInt(Foxy4ChargeTimes);
        parcel.writeInt(Foxy4PrepaidMonth);
        parcel.writeString(BaseImageInfo);
        parcel.writeInt(BaseRegIDImageInfo);
        parcel.writeDouble(DeviceTotal);
        parcel.writeInt(DiscountRP_VoucherTotal);
        parcel.writeString(ReferralCode);
        parcel.writeString(ReferralCodeDesc);
        parcel.writeString(VoucherCode);
        parcel.writeString(VoucherCodeDesc);
        parcel.writeString(BillTo_CityVN);
        parcel.writeString(BillTo_DistrictVN);
        parcel.writeString(BillTo_StreetVN);
        parcel.writeString(BillTo_WardVN);
        parcel.writeParcelable(ServiceCodeInternetTotal, i);
        parcel.writeParcelable(ServiceCodeCameraTotal, i);
        parcel.writeParcelable(ServiceCodeIpTvTotal, i);
        parcel.writeInt(CategoryServiceGroupID);
        parcel.writeParcelable(ObjectMaxy, i);
        parcel.writeParcelable(ServiceCodeMaxyTotal, i);
        parcel.writeInt(ServiceType);
        parcel.writeInt(SourceType);
    }

    public RegistrationDetailModel(String address, String addressOrigin, String billTo_City, String billTo_District, String billTo_Number, String billTo_Street, String billTo_Ward, int bookPortType, int branchCode, String cableStatus, String contact, String contact_1, String contact_2, String contract, String createBy, String createDate, String currentHouse, int cusType, String cusTypeDetail, int deposit, int depositBlackPoint, String description, String descriptionIBB, int divisionID, String email, int eoC, String floor, String fullName, int ID, String image, int inDoor, int inDType, int INSCable, int internetTotal, int IPTVBoxCount, int IPTVChargeTimes, int IPTVDeviceTotal, int IPTVHBOChargeTimes, int IPTVHBOPrepaidMonth, int IPTVKPlusChargeTimes, int IPTVKPlusPrepaidMonth, String IPTVPackage, int IPTVPLCCount, int IPTVPrepaid, int IPTVPrepaidTotal, int IPTVPromotionID, int IPTVRequestDrillWall, int IPTVRequestSetUp, int IPTVReturnSTBCount, int IPTVStatus, int IPTVTotal, int IPTVUseCombo, int IPTVVTCChargeTimes, int IPTVVTCPrepaidMonth, int IPTVVTVChargeTimes, int IPTVVTVPrepaidMonth, int ISPType, String latlng, int legalEntity, int localType, String localTypeName, int locationID, String lot, String mapCode, int modem, String nameVilla, String nameVillaVN, String note, int objectType, int objID, String ODCCableType, int outDoor, int outDType, int partnerID, String passport, String paymentAbility, String phone_1, String phone_2, int position, String promotionName, int promotionID, String regCode, String room, int statusDeposit, String supporter, String taxId, int total, int type_1, int type_2, int typeHouse, String userName, String TDName, int equipmentDelivery, int potentialID, String TDLatLng, String TDAddress, String birthday, int payment, String addressPassport, String paymentName, int IPTVFimPlusChargeTimes, int IPTVFimPlusPrepaidMonth, int IPTVFimHotChargeTimes, int IPTVFimHotPrepaidMonth, int IPTVFimPlusStdChargeTimes, int IPTVFimPlusStdPrepaidMonth, int IPTVPromotionIDBoxOrder, int promotionComboID, String promotionComboName, int regType, int contractServiceType, int serviceType, int contractLocalType, int contractPromotionID, int contractComboStatus, int isNewAddress, int OTTTotal, int OTTBoxCount, String contractPromotionName, String contractServiceTypeName, List<PackageModel> listPackage, List<Device> listDevice, List<FPTBox> listDeviceOTT, List<MacOTT> listMACOTT, String emailAdmin, String domainName, String techName, String techPhoneNumber, String techEmail, String office365Total, String imageInfo, String imageSignature, int IPTVPromotionType, int IPTVPromotionTypeBoxOrder, String IPTVPromotionDesc, CheckListRegistration checkListRegistration, String IPTVPromotionBoxOrderDesc, String cameraTotal, ArrayList<CameraSelected> listCamera, ArrayList<CloudPackageSelected> listCloud, SetupDetail setupCamera, CheckListService checkListService, isc.fpt.fsale.model.ListDeviceMACOTT listDeviceMACOTT, String giftName, int giftID, CartExtraOtt objectExtraOTT, List<isc.fpt.fsale.model.CategoryServiceList> categoryServiceList, int baseObjID, String baseContract, String HTMLDetail, isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera objectCamera, int foxy2ChargeTimes, int foxy2PrepaidMonth, int foxy4ChargeTimes, int foxy4PrepaidMonth, String baseImageInfo, int baseRegIDImageInfo, double deviceTotal, int discountRP_VoucherTotal, String referralCode, String referralCodeDesc, String voucherCode, String voucherCodeDesc, String billTo_CityVN, String billTo_DistrictVN, String billTo_StreetVN, String billTo_WardVN, int categoryServiceGroupID, isc.fpt.fsale.ui.fpt_camera.model.ObjectCameraOfNet objectCameraOfNet, isc.fpt.fsale.ui.maxytv.model.ObjectMaxy objectMaxy) {
        Address = address;
        AddressOrigin = addressOrigin;
        BillTo_City = billTo_City;
        BillTo_District = billTo_District;
        BillTo_Number = billTo_Number;
        BillTo_Street = billTo_Street;
        BillTo_Ward = billTo_Ward;
        BookPortType = bookPortType;
        BranchCode = branchCode;
        CableStatus = cableStatus;
        Contact = contact;
        Contact_1 = contact_1;
        Contact_2 = contact_2;
        Contract = contract;
        CreateBy = createBy;
        CreateDate = createDate;
        CurrentHouse = currentHouse;
        CusType = cusType;
        CusTypeDetail = cusTypeDetail;
        Deposit = deposit;
        DepositBlackPoint = depositBlackPoint;
        Description = description;
        DescriptionIBB = descriptionIBB;
        DivisionID = divisionID;
        Email = email;
        EoC = eoC;
        Floor = floor;
        FullName = fullName;
        this.ID = ID;
        Image = image;
        InDoor = inDoor;
        InDType = inDType;
        this.INSCable = INSCable;
        InternetTotal = internetTotal;
        this.IPTVBoxCount = IPTVBoxCount;
        this.IPTVChargeTimes = IPTVChargeTimes;
        this.IPTVDeviceTotal = IPTVDeviceTotal;
        this.IPTVHBOChargeTimes = IPTVHBOChargeTimes;
        this.IPTVHBOPrepaidMonth = IPTVHBOPrepaidMonth;
        this.IPTVKPlusChargeTimes = IPTVKPlusChargeTimes;
        this.IPTVKPlusPrepaidMonth = IPTVKPlusPrepaidMonth;
        this.IPTVPackage = IPTVPackage;
        this.IPTVPLCCount = IPTVPLCCount;
        this.IPTVPrepaid = IPTVPrepaid;
        this.IPTVPrepaidTotal = IPTVPrepaidTotal;
        this.IPTVPromotionID = IPTVPromotionID;
        this.IPTVRequestDrillWall = IPTVRequestDrillWall;
        this.IPTVRequestSetUp = IPTVRequestSetUp;
        this.IPTVReturnSTBCount = IPTVReturnSTBCount;
        this.IPTVStatus = IPTVStatus;
        this.IPTVTotal = IPTVTotal;
        this.IPTVUseCombo = IPTVUseCombo;
        this.IPTVVTCChargeTimes = IPTVVTCChargeTimes;
        this.IPTVVTCPrepaidMonth = IPTVVTCPrepaidMonth;
        this.IPTVVTVChargeTimes = IPTVVTVChargeTimes;
        this.IPTVVTVPrepaidMonth = IPTVVTVPrepaidMonth;
        this.ISPType = ISPType;
        Latlng = latlng;
        LegalEntity = legalEntity;
        LocalType = localType;
        LocalTypeName = localTypeName;
        LocationID = locationID;
        Lot = lot;
        MapCode = mapCode;
        Modem = modem;
        NameVilla = nameVilla;
        NameVillaVN = nameVillaVN;
        Note = note;
        ObjectType = objectType;
        ObjID = objID;
        this.ODCCableType = ODCCableType;
        OutDoor = outDoor;
        OutDType = outDType;
        PartnerID = partnerID;
        Passport = passport;
        PaymentAbility = paymentAbility;
        Phone_1 = phone_1;
        Phone_2 = phone_2;
        Position = position;
        PromotionName = promotionName;
        PromotionID = promotionID;
        RegCode = regCode;
        Room = room;
        StatusDeposit = statusDeposit;
        Supporter = supporter;
        TaxId = taxId;
        Total = total;
        Type_1 = type_1;
        Type_2 = type_2;
        TypeHouse = typeHouse;
        UserName = userName;
        this.TDName = TDName;
        EquipmentDelivery = equipmentDelivery;
        PotentialID = potentialID;
        this.TDLatLng = TDLatLng;
        this.TDAddress = TDAddress;
        Birthday = birthday;
        Payment = payment;
        AddressPassport = addressPassport;
        PaymentName = paymentName;
        this.IPTVFimPlusChargeTimes = IPTVFimPlusChargeTimes;
        this.IPTVFimPlusPrepaidMonth = IPTVFimPlusPrepaidMonth;
        this.IPTVFimHotChargeTimes = IPTVFimHotChargeTimes;
        this.IPTVFimHotPrepaidMonth = IPTVFimHotPrepaidMonth;
        this.IPTVFimPlusStdChargeTimes = IPTVFimPlusStdChargeTimes;
        this.IPTVFimPlusStdPrepaidMonth = IPTVFimPlusStdPrepaidMonth;
        this.IPTVPromotionIDBoxOrder = IPTVPromotionIDBoxOrder;
        PromotionComboID = promotionComboID;
        PromotionComboName = promotionComboName;
        RegType = regType;
        ContractServiceType = contractServiceType;
        ServiceType = serviceType;
        ContractLocalType = contractLocalType;
        ContractPromotionID = contractPromotionID;
        ContractComboStatus = contractComboStatus;
        IsNewAddress = isNewAddress;
        this.OTTTotal = OTTTotal;
        this.OTTBoxCount = OTTBoxCount;
        ContractPromotionName = contractPromotionName;
        ContractServiceTypeName = contractServiceTypeName;
        ListPackage = listPackage;
        ListDevice = listDevice;
        this.listDeviceOTT = listDeviceOTT;
        this.listMACOTT = listMACOTT;
        EmailAdmin = emailAdmin;
        DomainName = domainName;
        TechName = techName;
        TechPhoneNumber = techPhoneNumber;
        TechEmail = techEmail;
        Office365Total = office365Total;
        ImageInfo = imageInfo;
        ImageSignature = imageSignature;
        this.IPTVPromotionType = IPTVPromotionType;
        this.IPTVPromotionTypeBoxOrder = IPTVPromotionTypeBoxOrder;
        this.IPTVPromotionDesc = IPTVPromotionDesc;
        this.checkListRegistration = checkListRegistration;
        this.IPTVPromotionBoxOrderDesc = IPTVPromotionBoxOrderDesc;
        this.cameraTotal = cameraTotal;
        this.listCamera = listCamera;
        this.listCloud = listCloud;
        this.setupCamera = setupCamera;
        this.checkListService = checkListService;
        ListDeviceMACOTT = listDeviceMACOTT;
        GiftName = giftName;
        GiftID = giftID;
        ObjectExtraOTT = objectExtraOTT;
        CategoryServiceList = categoryServiceList;
        BaseObjID = baseObjID;
        BaseContract = baseContract;
        this.HTMLDetail = HTMLDetail;
        ObjectCamera = objectCamera;
        Foxy2ChargeTimes = foxy2ChargeTimes;
        Foxy2PrepaidMonth = foxy2PrepaidMonth;
        Foxy4ChargeTimes = foxy4ChargeTimes;
        Foxy4PrepaidMonth = foxy4PrepaidMonth;
        BaseImageInfo = baseImageInfo;
        BaseRegIDImageInfo = baseRegIDImageInfo;
        DeviceTotal = deviceTotal;
        DiscountRP_VoucherTotal = discountRP_VoucherTotal;
        ReferralCode = referralCode;
        ReferralCodeDesc = referralCodeDesc;
        VoucherCode = voucherCode;
        VoucherCodeDesc = voucherCodeDesc;
        BillTo_CityVN = billTo_CityVN;
        BillTo_DistrictVN = billTo_DistrictVN;
        BillTo_StreetVN = billTo_StreetVN;
        BillTo_WardVN = billTo_WardVN;
        CategoryServiceGroupID = categoryServiceGroupID;
        ObjectCameraOfNet = objectCameraOfNet;
        ObjectMaxy = objectMaxy;
    }

    public JSONObject toJsonObjectCostSetupCamera() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("RegType", getRegType());
            jsonObject.put("CusType", getCusType());
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
