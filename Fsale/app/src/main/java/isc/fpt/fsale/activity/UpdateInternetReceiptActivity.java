package isc.fpt.fsale.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetContractDepositList;
import isc.fpt.fsale.action.GetListGiftBox;
import isc.fpt.fsale.action.GetPromotionList;
import isc.fpt.fsale.action.UpdateReceiptInternet;
import isc.fpt.fsale.activity.local_type.LocalTypeActivity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.PromotionAdapter;
import isc.fpt.fsale.adapter.SpGiftBoxAdapter;
import isc.fpt.fsale.model.DepositValueModel;
import isc.fpt.fsale.model.GiftBox;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.LocalType;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

//màn hình cập nhật phiếu thu điện tử
public class UpdateInternetReceiptActivity extends BaseActivity {
    private LinearLayout containerAcceptExcessMoney;
    private Spinner spPromotion, spDepositContract, spGift;
    private TextView lblRegCode, lblNewTotal, lblSubTotal,
            lblCurrentTotal, lblPhoneNumber, lblIPTVTotal, lblInternetTotal,
            lblPromotionDesc, lblDeviceTotal, tvLocalType;
    private RadioGroup radGroupAcceptExcess;
    private RadioButton radAgree, radDisAgree;
    private RegistrationDetailModel mRegistration;
    private Context mContext;
    private Button btnUpdate;
    private List<GiftBox> mListGiftBox;
    private SpGiftBoxAdapter mAdapterGiftBox;
    private int isBind = 0;
    private boolean isFirst = true;
    private final int CODE_LOCAL_TYPE = 100;
    private LocalType mLocalType;

    public UpdateInternetReceiptActivity() {
        super(R.string.lbl_screen_name_update_internet_receipt);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_update_internet_receipt));
        setContentView(R.layout.activity_update_internet_receipt);
        this.mContext = this;
        initView();
        initEventView();
        getDataFromIntent();
        initDepositContract();
        initSpinnerGiftBox();
    }

    private void initView() {
        containerAcceptExcessMoney = (LinearLayout) findViewById(R.id.frm_accept_excess_money);
        lblRegCode = (TextView) findViewById(R.id.lbl_reg_code);
        lblNewTotal = (TextView) findViewById(R.id.lbl_new_total);
        lblSubTotal = (TextView) findViewById(R.id.lbl_sub_total);
        lblCurrentTotal = (TextView) findViewById(R.id.lbl_current_total);
        lblPhoneNumber = (TextView) findViewById(R.id.lbl_phone_number);
        lblIPTVTotal = (TextView) findViewById(R.id.lbl_ip_tv_total);
        lblInternetTotal = (TextView) findViewById(R.id.lbl_internet_total);
        lblDeviceTotal = (TextView) findViewById(R.id.lbl_device_total);
        lblPromotionDesc = (TextView) findViewById(R.id.lbl_promotion_desc);
        radGroupAcceptExcess = (RadioGroup) findViewById(R.id.rad_group_accept_excess_money);
        radAgree = (RadioButton) findViewById(R.id.rad_agree);
        radDisAgree = (RadioButton) findViewById(R.id.rad_dis_agree);
        radAgree.setEnabled(false);
        radDisAgree.setEnabled(false);
        spGift = (Spinner) findViewById(R.id.sp_gift);
        btnUpdate = (Button) findViewById(R.id.btn_update);
    }

    private void initEventView() {
        tvLocalType = (TextView) findViewById(R.id.tv_local_type);
        tvLocalType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                        (ArrayList<? extends Parcelable>) mRegistration.getCategoryServiceList());
                Intent intent = new Intent(UpdateInternetReceiptActivity.this, LocalTypeActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_LOCAL_TYPE);
            }
        });
        spPromotion = (Spinner) findViewById(R.id.sp_promotion);
        spPromotion.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                PromotionModel item = (PromotionModel) parentView.getItemAtPosition(position);
                lblPromotionDesc.setText(item.getPromotionName());
                if (!isFirst) {
                    isBind = 1;
                    if (mRegistration.getCheckListRegistration() != null
                            && mRegistration.getCheckListRegistration().getTrangThaiThanhToan() != 0) {
                        //goi api lay ds qua tang khi thay doi CLKM
                        new GetListGiftBox(mContext, UpdateInternetReceiptActivity.this, objGetListGiftBox());
                    }

                }
                isFirst = false;
                updateTotal();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        spDepositContract = (Spinner) findViewById(R.id.sp_deposit_contract);
        spDepositContract.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                updateTotal();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                confirmToUpdate();
            }
        });
    }

    private void initSpinnerGiftBox() {
        mListGiftBox = new ArrayList<>();
        //init data to spinner device type confirm
        mAdapterGiftBox = new SpGiftBoxAdapter(mContext, R.layout.item_spinner,
                R.id.text_view_0, (ArrayList<GiftBox>) this.mListGiftBox);
        spGift.setAdapter(mAdapterGiftBox);
        spGift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                GiftBox mGiftBox = mAdapterGiftBox.getItem(position);
                if (mGiftBox != null) {
                    mRegistration.setGiftID(mGiftBox.getGiftID());
                    mRegistration.setGiftName(mGiftBox.getGiftName());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            if (requestCode == CODE_LOCAL_TYPE) {
                mLocalType = data.getParcelableExtra(Constants.LOCAL_TYPE);
                tvLocalType.setText(mLocalType.getLocalTypeName());
                getPromotion(mLocalType.getLocalTypeID());
            }
        }
    }

    public void loadDataSpGiftBox(List<GiftBox> mList) {
        this.mListGiftBox = mList;
        mAdapterGiftBox.notifyData(this.mListGiftBox);
        //load old data
        if (mRegistration != null) {//cập nhật tong tien
            if (isBind == 0) {//load lại quà tặng của PDK
                spGift.setSelection(Common.getIndex(spGift, mRegistration.getGiftID()), true);
                isBind = 2;
            } else if (isBind == 1) {//user chọn lại CLKM
                spGift.setSelection(Common.getIndex(spGift, 0), true);
                isBind = 2;
            }
        }
    }

    private JSONObject objGetListGiftBox() {
        if (mRegistration == null) return null;
        JSONObject jsonObject = new JSONObject();
        //get service type
        JSONArray jsonArray = new JSONArray();
        if (mRegistration.getPromotionID() != 0) {
            jsonArray.put(1);//internet
        }

        if (mRegistration.getListDevice() != null && mRegistration.getListDevice().size() != 0) {
            jsonArray.put(2);//thiết bị lẻ
        }

        if (mRegistration.getListDeviceOTT() != null && mRegistration.getListDeviceOTT().size() != 0) {
            jsonArray.put(3);//fpt play box
        }

        if (mRegistration.getIPTVPromotionID() != 0) {
            jsonArray.put(4);//iptv
        }

        try {
            jsonObject.put("ServiceType", jsonArray);
            jsonObject.put("BranchCode", Constants.BRANCH_CODE);
            jsonObject.put("RegType", "0");//0 - bán mới, 1 - bán thêm
            if (spPromotion.getSelectedItem() != null) {
                jsonObject.put("NetPromotionID", spPromotion.getAdapter() != null &&
                        spPromotion.getAdapter().getCount() > 0 ? ((PromotionModel)
                        spPromotion.getSelectedItem()).getPromotionID() : 0);
            } else {
                jsonObject.put("NetPromotionID", mRegistration.getPromotionID());
            }
            jsonObject.put("IPTVPromotionID", mRegistration.getIPTVPromotionID());
            jsonObject.put("IPTVPromotionIDBoxOrder", mRegistration.getIPTVPromotionIDBoxOrder());
            jsonObject.put("IPTVPromotionType", mRegistration.getIPTVPromotionType());
            jsonObject.put("LocalType", mRegistration.getLocalType());
            jsonObject.put("LocationID", Constants.LOCATION_ID);
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("CusTypeDetail", mRegistration.getCusTypeDetail());
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
        if (mRegistration.getCheckListRegistration() != null
                && mRegistration.getCheckListRegistration().getTrangThaiThanhToan() == 0) {
            //goi api lay ds qua tang khi thay doi CLKM
            new GetListGiftBox(mContext, UpdateInternetReceiptActivity.this, objGetListGiftBox());
        }
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            mRegistration = intent.getParcelableExtra(Constants.MODEL_REGISTER);
        }
        if (mRegistration != null) {
            if (mRegistration.getCheckListRegistration() != null) {
                if (mRegistration.getCheckListRegistration().getTrangThaiThanhToan() != 0) {
                    containerAcceptExcessMoney.setVisibility(View.GONE);
                    //cho phép cập nhật quà tặng khi HĐ chưa TT
                    spGift.setEnabled(true);
                } else {
                    containerAcceptExcessMoney.setVisibility(View.VISIBLE);
                    //không cho phép cập nhật quà tặng khi HĐ đã TT
                    spGift.setEnabled(false);
                }
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_emty_data_container_status_for_register), mContext);
            }
            lblRegCode.setText(mRegistration.getRegCode());
            lblPhoneNumber.setText(mRegistration.getPhone_1());
            tvLocalType.setText(mRegistration.getLocalTypeName());
            lblCurrentTotal.setText(Common.formatNumber(mRegistration.getTotal()));
            lblIPTVTotal.setText(Common.formatNumber(mRegistration.getIPTVTotal()));
            lblDeviceTotal.setText(Common.formatNumber(mRegistration.getDeviceTotal()));
            getPromotion(mRegistration.getLocalType());
            mLocalType = new LocalType(mRegistration.getLocalType(), mRegistration.getLocalTypeName());
        }
    }

    //Goi API get Promotion List
    private void getPromotion(int localTypeID) {
        int locationId = ((MyApp) getApplication()).getLocationID();
        new GetPromotionList(this, String.valueOf(locationId), localTypeID, 0);
    }

    public void loadPromotion(ArrayList<PromotionModel> lst) {
        if (lst == null || lst.size() <= 0) {
            lblPromotionDesc.setText("");
        }
        PromotionAdapter adapter = new PromotionAdapter(this, R.layout.row_auto_complete, lst);
        spPromotion.setAdapter(adapter);
        if (mRegistration != null && lst != null)
            spPromotion.setSelection(Common.indexOf(lst, mRegistration.getPromotionID()), true);

        if (mRegistration != null && mRegistration.getCheckListRegistration().getTrangThaiThanhToan() != 0) {
            //load DS qua tang
            new GetListGiftBox(mContext, this, objGetListGiftBox());
        }
    }

    //Goi API lay Dat coc thue bao
    private void initDepositContract() {
        String RegCode = "";
        if (mRegistration != null)
            RegCode = mRegistration.getRegCode();
        new GetContractDepositList(this, Constants.USERNAME, RegCode);
    }

    public void loadDepositContract(List<DepositValueModel> lst) {
        ArrayList<KeyValuePairModel> lstDeposit = new ArrayList<>();
        for (int i = 0; i < lst.size(); i++) {
            lstDeposit.add(new KeyValuePairModel(lst.get(i).getTotal(), lst.get(i).getTitle()));
        }
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstDeposit, Gravity.CENTER);
        spDepositContract.setAdapter(adapter);
        if (mRegistration != null) {
            spDepositContract.setSelection(Common.getIndex(spDepositContract, mRegistration.getDeposit()), true);
        }
    }

    //Update tong tien
    private double updateTotal() {
        int currentTotal = mRegistration != null ? mRegistration.getTotal() : 0;
        int ipTvTotal = mRegistration != null ? mRegistration.getIPTVTotal() : 0;
        double deviceTotal = mRegistration != null ? mRegistration.getDeviceTotal() : 0;
        int officeTotal = mRegistration != null ? Integer.parseInt(mRegistration.getOffice365Total()) : 0;
        int promotionAmount = spPromotion.getAdapter() != null && spPromotion.getAdapter().getCount() > 0 ? ((PromotionModel) spPromotion.getSelectedItem()).getRealPrepaid() : 0;
        int depositContract = spDepositContract.getAdapter() != null && spDepositContract.getAdapter().getCount() > 0 ? ((KeyValuePairModel) spDepositContract.getSelectedItem()).getID() : 0;
        double newTotal = promotionAmount + depositContract + ipTvTotal + deviceTotal + officeTotal;
        lblInternetTotal.setText(Common.formatNumber(promotionAmount));
        lblNewTotal.setText(Common.formatNumber(newTotal));
        double subTotal = newTotal - currentTotal;
        if (subTotal < 0) {
            int strSubTotal = (int) subTotal;
            String str = String.valueOf(strSubTotal).replace("-", "");
            lblSubTotal.setText(Common.formatNumberNegative(Integer.parseInt(str)));
        } else {
            lblSubTotal.setText(Common.formatNumber((int) subTotal));
        }
        if (newTotal < currentTotal) {
            radAgree.setEnabled(true);
            radDisAgree.setEnabled(true);
        } else {
            radGroupAcceptExcess.clearCheck();
            radAgree.setEnabled(false);
            radDisAgree.setEnabled(false);
        }
        return newTotal;
    }

    private boolean checkForUpdate() {
        if (mLocalType == null) {
            Common.alertDialog("Chưa chọn gói Dịch vụ.", this);
            return false;
        }
        int promotionID = spPromotion.getAdapter() != null && spPromotion.getAdapter().getCount() > 0 ?
                ((PromotionModel) spPromotion.getSelectedItem()).getPromotionID() : 0;
        double newTotal = updateTotal();
        int currentTotal = mRegistration.getTotal();
        if (promotionID <= 0) {
            Common.alertDialog("Chưa chọn Câu lệnh khuyến mãi.", this);
            return false;
        }
        if (newTotal <= 0) {
            Common.alertDialog("Tổng tiền phải lơn hơn 0.", this);
            return false;
        }
        if (newTotal < currentTotal) {
            if (mRegistration.getCheckListRegistration() != null) {
                if (mRegistration.getCheckListRegistration().getTrangThaiThanhToan() == 0) {
                    if (!radAgree.isChecked() && !radDisAgree.isChecked()) {
                        Common.alertDialog(
                                "Chưa chọn Khách hàng đồng ý chuyển tiền thừa vào tài khoản trả trước hay không.",
                                this
                        );
                        return false;
                    }
                }
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_emty_data_container_status_for_register), mContext);
            }
            return true;
        }
        return true;
    }

    private void confirmToUpdate() {
        if (checkForUpdate()) {
            double newTotal = updateTotal();
            int currentTotal = mRegistration != null ? mRegistration.getTotal() : 0;
            String message = getString(R.string.msg_confirm_update);
            if (newTotal > currentTotal)
                message = getString(R.string.msg_confirm_send_sms_update_receipt);

            AlertDialog.Builder builder;
            Dialog dialog;
            builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.title_notification));
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.lbl_yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            update();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.lbl_no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            dialog = builder.create();
            dialog.show();
        }
    }

    private void update() {
        try {
            int localTypeID, promotionID, acceptExcessMoney = 0;
            localTypeID = mLocalType.getLocalTypeID();
            promotionID = spPromotion.getAdapter() != null && spPromotion.getAdapter().getCount() > 0 ?
                    ((PromotionModel) spPromotion.getSelectedItem()).getPromotionID() : 0;
            int depositContract = spDepositContract.getAdapter() != null && spDepositContract.getAdapter().getCount() > 0 ?
                    ((KeyValuePairModel) spDepositContract.getSelectedItem()).getID() : 0;
            double total = updateTotal();
            if (radAgree.isChecked())
                acceptExcessMoney = 1;
            new UpdateReceiptInternet(this, mRegistration, localTypeID,
                    promotionID, 0, depositContract, total, acceptExcessMoney);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.msg_confirm_to_back))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.lbl_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.lbl_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isBind = 0;
        isFirst = true;
    }
}