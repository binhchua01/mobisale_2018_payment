package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.ChatAdapter;
import isc.fpt.fsale.database.MessageTable;
import isc.fpt.fsale.model.GCMUserModel;
import isc.fpt.fsale.model.MessageModel;
import isc.fpt.fsale.model.NotificationIDUserModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class ChatActivity extends Activity {

    private ListView lvMessage;
    private TextView lblChatWith;
    private EditText txtMessage;
    private Button btnSend;
    private ProgressDialog pd;
    private List<MessageModel> lstMessage;
    private MessageTable tableMessage;
    private String chatWith = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Common.reportActivityCreate(getApplication(),
                getString(R.string.lbl_screen_name_chat));
        lvMessage = (ListView) findViewById(R.id.lv_chat);
        txtMessage = (EditText) findViewById(R.id.txt_message);
        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new OnClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View v) {
                try {
                    send();
                    Common.hideSoftKeyboard(ChatActivity.this);
                } catch (Exception e) {

                    Common.alertDialog(e.getMessage(), ChatActivity.this);
                }
            }
        });
        lblChatWith = (TextView) findViewById(R.id.lbl_chat_with);
        loadDataFromIntent(getIntent());
    }

    @SuppressLint("DefaultLocale")
    private void loadDataFromIntent(Intent intent) {
        try {
            chatWith = intent.getStringExtra("SEND_FROM");
            lstMessage = new ArrayList<>();
            tableMessage = new MessageTable(this);
            if (chatWith != null && !chatWith.equals("")) {
                lblChatWith.setText(chatWith);
                lstMessage = tableMessage.getConversation(chatWith);
            }
            if (lstMessage != null && lstMessage.size() > 0) {
                ChatAdapter adapter = new ChatAdapter(this, lstMessage);
                lvMessage.setAdapter(adapter);
            }
            // Xoa Notification neu dang chat
            try {
                List<NotificationIDUserModel> lst = ((MyApp) this
                        .getApplication()).getNotificationUserList();
                if (lst != null) {
                    if (lst.size() > 0) {
                        for (NotificationIDUserModel item : lst) {
                            if (item.getUserName().toUpperCase()
                                    .equals(chatWith.toUpperCase())) {
                                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                mNotificationManager.cancel(item
                                        .getNoticationID());
                                ((MyApp) this.getApplication())
                                        .getNotificationUserList().remove(item);
                            }
                        }
                    }
                }

            } catch (Exception e) {

                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat")
    private void send() {
        if (!txtMessage.getText().toString().trim().equals("")) {
            String sendFrom;
            try {
                sendFrom = ((MyApp) this.getApplication()).getUserName();
                if (sendFrom.equals(""))
                    sendFrom = Common.loadPreference(this,
                            Constants.SHARE_PRE_GROUP_USER,
                            Constants.SHARE_PRE_GROUP_USER_NAME);
            } catch (Exception e) {

                sendFrom = Constants.USERNAME;
                e.printStackTrace();
            }
            MessageModel message = new MessageModel();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat format1 = new SimpleDateFormat(
                    "dd-MM-yyyy HH:mm:ss");
            String formatted = format1.format(c.getTime());
            message.setMessage(txtMessage.getText().toString());
            message.setDatetime(formatted);
            message.setSendfrom(sendFrom);
            message.setSendFromRegID(Common.loadPreference(this,
                    Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_REG_ID));
            MessageModel lastMessage = tableMessage
                    .getLastMessageSendBy(chatWith);
            GCMUserModel sendToGCM = new GCMUserModel();
            if (lastMessage != null) {
                message.setCategory(lastMessage.getCategory());
                sendToGCM.setRegID(lastMessage.getSendFromRegID());
                sendToGCM.setUserName(lastMessage.getSendfrom());
                message.setTo(chatWith);
            }
            sendNotification(message, sendToGCM);
        }
    }

    @SuppressLint("SimpleDateFormat")
    private void sendNotification(final MessageModel msg,
                                  final GCMUserModel sendTo) {

        pd = Common.showProgressBar(this, "Đang gửi tin nhắn...");
        new AsyncTask<Object, String, String>() {
            MessageModel message = msg;

            @Override
            protected void onPostExecute(String result) {
                JSONObject js = null;
                int success = 0;
                try {
                    js = new JSONObject(result);
                    if (js.has("success"))
                        success = js.getInt("success");
                } catch (JSONException e) {
                    // TODO Auto-generated catch block

                    e.printStackTrace();
                }
                if (pd.isShowing())
                    pd.dismiss();
                if (success > 0) {
                    try {
                        tableMessage.add(message);
                        lstMessage.add(message);
                        txtMessage.setText("");
                        ((ChatAdapter) lvMessage.getAdapter())
                                .notifyDataSetChanged();

                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                } else
                    Common.alertDialog("Gửi thất bại!", ChatActivity.this);

            };

            @Override
            protected String doInBackground(Object... params) {
                String msg = "";
                try {
                    List<GCMUserModel> lst = new ArrayList<GCMUserModel>();
                    lst.add(sendTo);
                    msg = Services.sendGCMMessage(lst, message.toJSONObject());
                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }
        }.execute(null, null, null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MyApp) getApplication()).registerReceiver(mMessageReceiver,
                new IntentFilter("MOBISALE_CHAT_ACTIVITY"));
    }

    // Must unregister onPause()
    @Override
    protected void onPause() {
        super.onPause();
        ((MyApp) getApplication()).unregisterReceiver(mMessageReceiver);
    }

    // This is the handler that will manager to process the broadcast intent
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @SuppressLint("DefaultLocale")
        @Override
        public void onReceive(Context context, Intent intent) {

            String sendFrom = intent.getStringExtra("SEND_FROM");
            if (chatWith != null && sendFrom != null
                    && sendFrom.toLowerCase().equals(chatWith.toLowerCase()))
                loadDataFromIntent(intent);
        }
    };
}
