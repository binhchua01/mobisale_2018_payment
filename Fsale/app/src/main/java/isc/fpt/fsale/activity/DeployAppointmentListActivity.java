package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetDeployAppointmentList;
import isc.fpt.fsale.adapter.DeployAppointmentAdapter;
import isc.fpt.fsale.model.DeployAppointmentModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;

//màn hình danh sách hẹn triển khai
public class DeployAppointmentListActivity extends BaseActivity {
    private RegistrationDetailModel mRegister;
    private DeployAppointmentAdapter mAdapter;
    private List<DeployAppointmentModel> mList;

    public DeployAppointmentListActivity() {
        super(R.string.lbl_screen_name_deploy_appointment_list);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_deploy_appointment_list));
        setContentView(R.layout.activity_deploy_appointment_list);
        initViews();
        getDataFromIntent();
        getData();
    }

    private void initViews() {
        ListView lvDeploy = (ListView) findViewById(R.id.lv_deploy_appointment_list);
        mList = new ArrayList<>();
        mAdapter = new DeployAppointmentAdapter(this, mList);
        lvDeploy.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        if (getIntent() != null) {
            mRegister = getIntent().getParcelableExtra(Constants.MODEL_REGISTER);
        }
    }

    private void getData() {
        if (mRegister != null) {
            new GetDeployAppointmentList(this, Constants.USERNAME, mRegister.getRegCode());
        }
    }

    public void LoadData(List<DeployAppointmentModel> mList) {
        if (mList == null || mList.size() <= 0) {
            showPopupNoDataDisplay();
        } else {
            this.mList = mList;
            mAdapter.notifyData(this.mList);
        }
    }

    private void showPopupNoDataDisplay() {
        new AlertDialog.Builder(this)
                .setTitle("Thông Báo")
                .setMessage(getString(R.string.msg_no_data))
                .setPositiveButton(R.string.lbl_ok, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
