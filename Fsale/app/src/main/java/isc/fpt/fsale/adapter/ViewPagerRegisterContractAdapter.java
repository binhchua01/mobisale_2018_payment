package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentFptCamera;
import isc.fpt.fsale.ui.fragment.FragmentFptCameraTotal;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractCamera;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractCusInfo;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractDevice;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractIPTV;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractInternet;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractMaxy;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractTotal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.SparseArray;

public class ViewPagerRegisterContractAdapter extends FragmentPagerAdapter {
    public Context mContext;
    //intenet
    public int TAG_TAP_CUS_INFO = 0;
    public int TAG_TAP_INTERNET = 1;
    public int TAG_TAP_MAXY_TV = 2;
    public int TAG_TAP_INTERNET_CAMERA = 3;
    public int TAG_TAP_DEVICE = 4;
    public int TAG_TAP_TOTAL = 5;
    //camera
    public int TAG_TAP_FPT_CAMERA = 6;
    public int TAG_TAP_FPT_CAMERA_TOTAL = 7;
    private SparseArray<Fragment> hashFragment;
    public int mServiceType;
    private Drawable myDrawable;

    public ViewPagerRegisterContractAdapter(FragmentManager fm, Context context, int ServiceType) {
        super(fm);
        mContext = context;
        mServiceType = ServiceType;
        initListFragment();
    }

    private void initListFragment() {
        //check service type to management tab is show - service type is 3 - camera
        if (mServiceType == 5) {
            //fpt camera => 3 fragment is show
            hashFragment = new SparseArray<>();
            hashFragment.append(TAG_TAP_CUS_INFO,//0
                    FragmentRegisterContractCusInfo.newInstance());
            hashFragment.append(TAG_TAP_FPT_CAMERA,//6
                    FragmentFptCamera.newInstance());
            hashFragment.append(TAG_TAP_FPT_CAMERA_TOTAL,//7
                    FragmentFptCameraTotal.newInstance());
        } else {
            //default service type  => 5 fragment is show
            hashFragment = new SparseArray<>();
            hashFragment.append(TAG_TAP_CUS_INFO,
                    FragmentRegisterContractCusInfo.newInstance());
            hashFragment.append(TAG_TAP_INTERNET,
                    FragmentRegisterContractInternet.newInstance());
            hashFragment.append(TAG_TAP_MAXY_TV,
                    FragmentRegisterContractMaxy.newInstance());
            hashFragment.append(TAG_TAP_INTERNET_CAMERA,
                    FragmentRegisterContractCamera.newInstance());
            hashFragment.append(TAG_TAP_DEVICE,
                    FragmentRegisterContractDevice.newInstance());
            hashFragment.append(TAG_TAP_TOTAL,
                    FragmentRegisterContractTotal.newInstance());
        }
    }

    public int getTAG_TAP_CUS_INFO() {
        return TAG_TAP_CUS_INFO;
    }

    public int getTAG_TAP_INTERNET() {
        return TAG_TAP_INTERNET;
    }

    public int getTAG_TAP_TOTAL() {
        return TAG_TAP_TOTAL;
    }

    public int getTAG_TAP_DEVICE() {
        return TAG_TAP_DEVICE;
    }

    public int getTAG_TAP_FPT_CAMERA() {
        return TAG_TAP_FPT_CAMERA;
    }

    public int getTAG_TAP_FPT_CAMERA_TOTAL() {
        return TAG_TAP_FPT_CAMERA_TOTAL;
    }

    //ver 3.21 author nhannh26

    public int getTAG_TAP_MAXY_TV() {
        return TAG_TAP_MAXY_TV;
    }

    public int getTAG_TAP_INTERNET_CAMERA() { return TAG_TAP_INTERNET_CAMERA; }

    @Override
    public Fragment getItem(int position) {
        if (position <= hashFragment.size() - 1) {
            int key = hashFragment.keyAt(position);
            Fragment fragment = hashFragment.valueAt(position);

            if (key == getTAG_TAP_CUS_INFO()) {
                return fragment;
            }

            if (key == getTAG_TAP_INTERNET()) {
                return fragment;
            }

            if (key == getTAG_TAP_MAXY_TV()) {
                return fragment;
            }

            if (key == getTAG_TAP_INTERNET_CAMERA()) {
                return fragment;
            }

            if (key == getTAG_TAP_DEVICE()) {
                return fragment;
            }

            if (key == getTAG_TAP_TOTAL()) {
                return fragment;
            }

            if (key == getTAG_TAP_FPT_CAMERA()) {
                return fragment;
            }

            if (key == getTAG_TAP_FPT_CAMERA_TOTAL()) {
                return fragment;
            }
        }
        return new Fragment();
    }

    public Fragment getItemByTagId(int tagID) {
        if (hashFragment != null && hashFragment.get(tagID) != null)
            return hashFragment.get(tagID);
        return null;
    }

    public int getTagIdByPosition(int position) {
        return hashFragment.keyAt(position);
    }

    @Override
    public int getCount() {
        return hashFragment.size();
    }

    @SuppressLint("DefaultLocale")
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        try {
            int key = -1;
            if (position <= hashFragment.size() - 1) {
                key = hashFragment.keyAt(position);
            }

            if (key == getTAG_TAP_CUS_INFO()) {
                myDrawable = mContext.getResources().getDrawable(R.drawable.ic_customer_info);
                title = mContext.getString(R.string.lbl_title_group_info_object).toUpperCase();
            } else if (key == getTAG_TAP_INTERNET()) {
                myDrawable = mContext.getResources().getDrawable(R.drawable.ic_internet);
                title = mContext.getString(R.string.lbl_title_group_info_register).toUpperCase();
            } else if (key == getTAG_TAP_MAXY_TV()) {
                myDrawable = mContext.getResources().getDrawable(R.drawable.ic_iptv);
                title = mContext.getString(R.string.lbl_title_group_info_maxytv).toUpperCase();
            } else if (key == getTAG_TAP_INTERNET_CAMERA()) {
                myDrawable = mContext.getResources().getDrawable(R.drawable.ic_camera_v2);
                title = mContext.getString(R.string.lbl_title_group_fpt_camera).toUpperCase();
            } else if (key == getTAG_TAP_DEVICE()) {
                myDrawable = mContext.getResources().getDrawable(R.drawable.ic_internet);
                title = mContext.getString(R.string.lbl_title_group_info_device).toUpperCase();
            } else if (key == getTAG_TAP_TOTAL()) {
                myDrawable = mContext.getResources().getDrawable(R.drawable.ic_register_total);
                title = mContext.getString(R.string.lbl_total_amount).toUpperCase();
            } else if (key == getTAG_TAP_FPT_CAMERA()) {
                myDrawable = mContext.getResources().getDrawable(R.drawable.ic_internet);
                title = mContext.getString(R.string.lbl_title_group_fpt_camera).toUpperCase();
            } else if (key == getTAG_TAP_FPT_CAMERA_TOTAL()) {
                myDrawable = mContext.getResources().getDrawable(R.drawable.ic_register_total);
                title = mContext.getString(R.string.lbl_total_amount).toUpperCase();
            }
            title = "# " + title;
            SpannableStringBuilder sb = new SpannableStringBuilder(title);
            if (myDrawable != null) {
                myDrawable.setBounds(0, 0, myDrawable.getIntrinsicWidth(),
                        myDrawable.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(myDrawable,
                        ImageSpan.ALIGN_BOTTOM);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            return sb;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return title;
    }
}
