package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by haulc3 on 01,November,2019
 */
public class VoucherTotal implements Parcelable {
    private boolean IsValid;
    private String Message;
    private String ReferralCode;
    private String ReferralCodeDesc;
    private int TotalDiscount;

    private VoucherTotal(Parcel in) {
        IsValid = in.readByte() != 0;
        Message = in.readString();
        ReferralCode = in.readString();
        ReferralCodeDesc = in.readString();
        TotalDiscount = in.readInt();
    }

    public static final Creator<VoucherTotal> CREATOR = new Creator<VoucherTotal>() {
        @Override
        public VoucherTotal createFromParcel(Parcel in) {
            return new VoucherTotal(in);
        }

        @Override
        public VoucherTotal[] newArray(int size) {
            return new VoucherTotal[size];
        }
    };

    public boolean isValid() {
        return IsValid;
    }

    public void setValid(boolean valid) {
        IsValid = valid;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getReferralCode() {
        return ReferralCode;
    }

    public void setReferralCode(String referralCode) {
        ReferralCode = referralCode;
    }

    public String getReferralCodeDesc() {
        return ReferralCodeDesc;
    }

    public void setReferralCodeDesc(String referralCodeDesc) {
        ReferralCodeDesc = referralCodeDesc;
    }

    public int getTotalDiscount() {
        return TotalDiscount;
    }

    public void setTotalDiscount(int totalDiscount) {
        TotalDiscount = totalDiscount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (IsValid ? 1 : 0));
        parcel.writeString(Message);
        parcel.writeString(ReferralCode);
        parcel.writeString(ReferralCodeDesc);
        parcel.writeInt(TotalDiscount);
    }
}
