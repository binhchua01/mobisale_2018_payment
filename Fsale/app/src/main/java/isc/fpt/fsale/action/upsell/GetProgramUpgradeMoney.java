package isc.fpt.fsale.action.upsell;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.model.upsell.response.ProgramUpgradeMoney;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetProgramUpgradeMoney implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private OnGetUpgradeMoney mCallBack;
    public GetProgramUpgradeMoney(Context mContext, String[] ProgramID,OnGetUpgradeMoney mCallBack) {
        this.mContext = mContext;
        this.mCallBack = mCallBack;
        String[] params = new String[]{"ProgramUpgradeID"};
        String message = "Đang lấy danh sách tiền upgrade...";
        String GET_UPGRADE_MONEY = "GetProgramUpgradeMoney";
        CallServiceTask service = new CallServiceTask(mContext, GET_UPGRADE_MONEY, params, ProgramID,
                Services.JSON_POST, message, GetProgramUpgradeMoney.this);
        service.execute();
    }

    public interface OnGetUpgradeMoney{
        void onGetSuccess(List<ProgramUpgradeMoney> lst);
    }
    @Override
    public void onTaskComplete(String result) {
        try {
            List<ProgramUpgradeMoney> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ProgramUpgradeMoney.class));
                }String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        mCallBack.onGetSuccess(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
