package isc.fpt.fsale.activity.device_price_list;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by haulc3 on 18,October,2019
 */
public class DevicePrice implements Parcelable {
    private int DeviceID;
    private int CustomerStatus;
    private int EFStatus;
    private int NumMax;
    private int NumMin;
    private double Price;
    private int PriceID;
    private String PriceName;
    private String PriceText;
    private int ServiceCode;
    private int ReturnStatus;


    protected DevicePrice(Parcel in) {
        DeviceID = in.readInt();
        CustomerStatus = in.readInt();
        EFStatus = in.readInt();
        NumMax = in.readInt();
        NumMin = in.readInt();
        Price = in.readDouble();
        PriceID = in.readInt();
        PriceName = in.readString();
        PriceText = in.readString();
        ServiceCode = in.readInt();
        ReturnStatus = in.readInt();
    }

    public static final Creator<DevicePrice> CREATOR = new Creator<DevicePrice>() {
        @Override
        public DevicePrice createFromParcel(Parcel in) {
            return new DevicePrice(in);
        }

        @Override
        public DevicePrice[] newArray(int size) {
            return new DevicePrice[size];
        }
    };

    public int getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(int deviceID) {
        DeviceID = deviceID;
    }

    public int getCustomerStatus() {
        return CustomerStatus;
    }

    public void setCustomerStatus(int customerStatus) {
        CustomerStatus = customerStatus;
    }

    public int getEFStatus() {
        return EFStatus;
    }

    public void setEFStatus(int EFStatus) {
        this.EFStatus = EFStatus;
    }

    public int getNumMax() {
        return NumMax;
    }

    public void setNumMax(int numMax) {
        NumMax = numMax;
    }

    public int getNumMin() {
        return NumMin;
    }

    public void setNumMin(int numMin) {
        NumMin = numMin;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getPriceID() {
        return PriceID;
    }

    public void setPriceID(int priceID) {
        PriceID = priceID;
    }

    public String getPriceName() {
        return PriceName;
    }

    public void setPriceName(String priceName) {
        PriceName = priceName;
    }

    public String getPriceText() {
        return PriceText;
    }

    public void setPriceText(String priceText) {
        PriceText = priceText;
    }

    public int getServiceCode() {
        return ServiceCode;
    }

    public void setServiceCode(int serviceCode) {
        ServiceCode = serviceCode;
    }

    public int getReturnStatus() {
        return ReturnStatus;
    }

    public void setReturnStatus(int returnStatus) {
        ReturnStatus = returnStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(DeviceID);
        parcel.writeInt(CustomerStatus);
        parcel.writeInt(EFStatus);
        parcel.writeInt(NumMax);
        parcel.writeInt(NumMin);
        parcel.writeDouble(Price);
        parcel.writeInt(PriceID);
        parcel.writeString(PriceName);
        parcel.writeString(PriceText);
        parcel.writeInt(ServiceCode);
        parcel.writeInt(ReturnStatus);
    }
}
