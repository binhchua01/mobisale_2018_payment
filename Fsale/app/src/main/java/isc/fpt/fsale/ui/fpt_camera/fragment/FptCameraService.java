package isc.fpt.fsale.ui.fpt_camera.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetCostSetupCamera;
import isc.fpt.fsale.adapter.DeviceCameraAdapter;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV3;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV4;
import isc.fpt.fsale.ui.fpt_camera.CameraDetailAdapter;
import isc.fpt.fsale.ui.fpt_camera.CloudDetailAdapter;
import isc.fpt.fsale.ui.fpt_camera.FptCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_package.CameraPackageActivity;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_type.CameraTypeActivity;
import isc.fpt.fsale.ui.fpt_camera.camera_price_list.DevicePriceListCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.cloud_detail.cloud_type.CloudTypeActivity;
import isc.fpt.fsale.ui.fpt_camera.device.service_type.ServiceTypeCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.device_list.DeviceListCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCamera;
import isc.fpt.fsale.ui.fpt_camera.model.PromotionDetail;
import isc.fpt.fsale.ui.fpt_camera.model.RegisterFptCameraModel;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.CAMERA_DETAIL;
import static isc.fpt.fsale.utils.Constants.CLASS_NAME;
import static isc.fpt.fsale.utils.Constants.CLOUD_DETAIL;
import static isc.fpt.fsale.utils.Constants.COMBO;
import static isc.fpt.fsale.utils.Constants.LIST_CAMERA_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_CLOUD_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.OBJECT_CAMERA_DETAIL;
import static isc.fpt.fsale.utils.Constants.POSITION;

/**
 * Created by haulc3 on 28,August,2019
 */
public class FptCameraService extends BaseFragment implements
        OnItemClickListenerV3<Object, Integer, Integer>, OnItemClickListener<Boolean>, OnItemClickListenerV4<Integer, Device, Integer> {
    private List<CategoryServiceList> mListServiceSelected;
    private RegisterFptCameraModel mRegisterFptCameraModel = null;
    private TextView tvServiceType, tvLocalType;
    private View loQuantityDeploy, frmDevicesBanner, frmRegisterCamera;
    private CameraDetailAdapter mCameraAdapter;
    private CloudDetailAdapter mCloudAdapter;
    public List<CameraDetail> mListCamera;
    public List<CloudDetail> mListCloud;
    public RadioButton rdoDeploy, rdoNoDeploy;
    public SetupDetail mSetupDetail;
    private TextView tvSetupDetailPrice, tvSetupDetailQuantity, tvSetupDetailLess, tvSetupDetailPlus;
    private Button btnAddCamera, btnAddCloud, btnSelectDevice;
    public boolean is_have_device = false;
    private final int CODE_CAMERA_DETAIL = 1;
    private final int CODE_CLOUD_DETAIL = 2;
    private final int CODE_CAMERA_PACKAGE = 3;
    private FptCameraActivity activity;

    private final int CODE_LOCAL_TYPE = 100;
    private final int CODE_SERVICE_TYPE = 101;
    private final int CODE_DEVICE_LIST_SELECTED = 102;
    private final int CODE_DEVICE_PRICE_LIST_SELECTED = 103;


    public List<Device> mListDevice;
    private DeviceCameraAdapter mDeviceAdapter;
    public boolean is_clear_rp_and_voucher;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (FptCameraActivity) getActivity();
        mRegisterFptCameraModel = activity.getRegisterModel();
        // mListServiceSelected = activity.getListService();
        if (mRegisterFptCameraModel.getObjectCamera() == null) {
            mRegisterFptCameraModel.setObjectCamera(
                    new ObjectCamera(
                            new ArrayList<CameraDetail>(), new ArrayList<CloudDetail>(),
                            new SetupDetail(), new PromotionDetail()
                    )
            );
        }
    }

    @Override
    protected void initView(View mView) {
        tvServiceType = mView.findViewById(R.id.tv_service_type);
        tvLocalType = mView.findViewById(R.id.tv_local_type);
        btnAddCamera = mView.findViewById(R.id.btn_add_camera);
        btnAddCloud = mView.findViewById(R.id.btn_add_cloud_service);
        RecyclerView mRecyclerViewCamera = mView.findViewById(R.id.list_camera_device_final);
        RecyclerView mRecyclerViewCloud = mView.findViewById(R.id.list_cloud_service_final);
        rdoDeploy = mView.findViewById(R.id.rdo_deploy);
        rdoNoDeploy = mView.findViewById(R.id.rdo_receive_on_bar);
        loQuantityDeploy = mView.findViewById(R.id.layout_deploy);
        tvSetupDetailPrice = mView.findViewById(R.id.tv_camera_deploy_cost);
        tvSetupDetailQuantity = mView.findViewById(R.id.lbl_camera_deploy_quantity);
        tvSetupDetailLess = mView.findViewById(R.id.tv_camera_deploy_less);
        tvSetupDetailPlus = mView.findViewById(R.id.tv_camera_deploy_plus);
        //camera detail
        mListCamera = mRegisterFptCameraModel.getObjectCamera().getCameraDetail() != null ?
                mRegisterFptCameraModel.getObjectCamera().getCameraDetail() : new ArrayList<CameraDetail>();
        mCameraAdapter = new CameraDetailAdapter(getActivity(), mListCamera, this, this);
        mRecyclerViewCamera.setAdapter(mCameraAdapter);
        //cloud detail
        mListCloud = mRegisterFptCameraModel.getObjectCamera().getCloudDetail() != null ?
                mRegisterFptCameraModel.getObjectCamera().getCloudDetail() : new ArrayList<CloudDetail>();
        mCloudAdapter = new CloudDetailAdapter(getActivity(), mListCloud, this, this);
        mRecyclerViewCloud.setAdapter(mCloudAdapter);
        //setup detail
        mSetupDetail = mRegisterFptCameraModel.getObjectCamera().getSetupDetail();
        if (mSetupDetail.getType() == 1) {//có triển khai
            isDeploy();
        } else {
            isNoDeploy();
        }

        if (mRegisterFptCameraModel.getObjectCamera().getCameraDetail().size() != 0) {
            rdoDeploy.setEnabled(true);
        } else {
            rdoDeploy.setEnabled(false);
        }
        mListServiceSelected = new ArrayList<>();
        List<CategoryServiceList> lstSv = new ArrayList<>();
        lstSv.add(new CategoryServiceList(5, "FPT Camera"));
        mListServiceSelected = lstSv;


        //update devices
        btnSelectDevice = mView.findViewById(R.id.btn_select_device);
        RecyclerView mRecyclerViewDevice = mView.findViewById(R.id.lv_list_device);
        mListDevice = new ArrayList<>();
        mDeviceAdapter = new DeviceCameraAdapter(this.getActivity(), mListDevice, this);
        mRecyclerViewDevice.setAdapter(mDeviceAdapter);
        frmDevicesBanner = mView.findViewById(R.id.frm_devices);
        frmRegisterCamera = mView.findViewById(R.id.frm_camera);


    }

    public void isNoDeploy() {
        loQuantityDeploy.setVisibility(View.GONE);
        rdoNoDeploy.setChecked(true);
        rdoDeploy.setChecked(false);
        tvSetupDetailQuantity.setText(String.valueOf(0));
        tvSetupDetailPrice.setText(
                String.format(getString(R.string.txt_unit_price), Common.formatNumber(0)));
        mSetupDetail = null;
    }

    private void isDeploy() {
        loQuantityDeploy.setVisibility(View.VISIBLE);
        rdoDeploy.setChecked(true);
        rdoNoDeploy.setChecked(false);
        tvSetupDetailQuantity.setText(String.valueOf(mSetupDetail.getQty()));
        tvSetupDetailPrice.setText(
                String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(mSetupDetail.getQty() * mSetupDetail.getCost()))
        );
    }


    @Override
    protected void initEvent() {
        btnAddCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(LIST_CAMERA_DETAIL_SELECTED,
                        (ArrayList<? extends Parcelable>) mListCamera);
                if (mRegisterFptCameraModel.getBaseObjID() == 0 &&
                        TextUtils.isEmpty(mRegisterFptCameraModel.getBaseContract())) {
                    bundle.putInt(COMBO, Constants.COMBO_TYPE_NO);
                } else {
                    bundle.putInt(COMBO, Constants.COMBO_TYPE_YES);
                }
                bundle.putString(Constants.CLASS_NAME, FptCameraService.class.getSimpleName());
                Intent intent = new Intent(getContext(), CameraTypeActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_CAMERA_DETAIL);
            }
        });
        btnAddCloud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(LIST_CLOUD_DETAIL_SELECTED,
                        (ArrayList<? extends Parcelable>) mListCloud);
                if (mRegisterFptCameraModel.getBaseObjID() == 0 &&
                        TextUtils.isEmpty(mRegisterFptCameraModel.getBaseContract())) {
                    bundle.putInt(COMBO, Constants.COMBO_TYPE_NO);
                } else {
                    bundle.putInt(COMBO, Constants.COMBO_TYPE_YES);
                }
                bundle.putString(Constants.CLASS_NAME, FptCameraService.class.getSimpleName());
                Intent intent = new Intent(getContext(), CloudTypeActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_CLOUD_DETAIL);
            }
        });

        rdoDeploy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    new GetCostSetupCamera(getContext(), FptCameraService.this, mRegisterFptCameraModel);
                }
            }
        });

        rdoNoDeploy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    isNoDeploy();
                }
            }
        });

        tvSetupDetailLess.setOnClickListener(view -> {
            int quantity = mSetupDetail.getQty();
            if (quantity == 1) {
                return;
            }
            quantity--;
            mSetupDetail.setQty(quantity);
            tvSetupDetailQuantity.setText(String.valueOf(quantity));
            tvSetupDetailPrice.setText(
                    String.format(getString(R.string.txt_unit_price), Common.formatNumber(quantity * mSetupDetail.getCost()))
            );
            activity.setIsUpdatePromotion(true);
        });

        tvSetupDetailPlus.setOnClickListener(view -> {
            int quantity = mSetupDetail.getQty();
            quantity++;
            mSetupDetail.setQty(quantity);
            tvSetupDetailQuantity.setText(String.valueOf(quantity));
            tvSetupDetailPrice.setText(
                    String.format(getString(R.string.txt_unit_price), Common.formatNumber(quantity * mSetupDetail.getCost()))
            );
            activity.setIsUpdatePromotion(true);
        });


        //version 3.14

        btnSelectDevice.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(Constants.DEVICE_LIST_SELECTED,
                    (ArrayList<? extends Parcelable>) mListDevice);
            bundle.putString(Constants.CLASS_NAME, FptCameraService.class.getSimpleName());
            Intent intent = new Intent(getActivity(), DeviceListCameraActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_DEVICE_LIST_SELECTED);

        });

        tvServiceType.setOnClickListener(v -> {
            if (mRegisterFptCameraModel != null) {
                Bundle bundle = new Bundle();
                List<CategoryServiceList> lstSv = new ArrayList<>();
                lstSv.add(new CategoryServiceList(5, "FPT Camera"));
                lstSv.add(new CategoryServiceList(2, "Thiết bị"));
                bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                        (ArrayList<? extends Parcelable>) lstSv);
                bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST_SELECTED,
                        (ArrayList<? extends Parcelable>) mListServiceSelected);
                Intent intent = new Intent(getActivity(), ServiceTypeCameraActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_SERVICE_TYPE);
            } else {
                if (((FptCameraActivity) getActivity()).getListService().size() > 1) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                            (ArrayList<? extends Parcelable>) ((FptCameraActivity) getActivity()).getListService());
                    bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST_SELECTED,
                            (ArrayList<? extends Parcelable>) mListServiceSelected);
                    Intent intent = new Intent(getActivity(), ServiceTypeCameraActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_SERVICE_TYPE);
                }
            }
        });
    }

    private int getCameraCount() {
        if (mListCamera == null || mListCamera.size() == 0) {
            return 0;
        }
        int cameraCount = 0;
        for (CameraDetail item : mListCamera) {
            cameraCount += item.getQty();
        }
        return cameraCount;
    }

    public void loadInfoSetupDetail(List<SetupDetail> mList) {
        mSetupDetail = mList.get(0);

        mSetupDetail.setType(1);//1 - triển khai , 0 - giao tại quầy
        //set min set up detail
        mSetupDetail.setQty(getCameraCount());

        isDeploy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                switch (requestCode) {
                    case CODE_CAMERA_DETAIL:
                        List<CameraDetail> mList = data.getParcelableArrayListExtra(OBJECT_CAMERA_DETAIL);
                        mListCamera.clear();
                        if (mList != null && mList.size() != 0) {
                            mListCamera.addAll(mList);
                        } else {
                            rdoDeploy.setChecked(false);
                            rdoDeploy.setEnabled(false);
                            rdoNoDeploy.setChecked(true);
                            loQuantityDeploy.setVisibility(View.GONE);
                        }
                        mCameraAdapter.notifyDataChanged(mListCamera);
                        break;
                    case CODE_CLOUD_DETAIL:
                        CloudDetail mCloudDetail = data.getParcelableExtra(CLOUD_DETAIL);
                        if (this.mListCloud != null) {
                            this.mListCloud.add(mCloudDetail);
                        }
                        mCloudAdapter.notifyDataChanged(this.mListCloud);
                        break;
                    case CODE_CAMERA_PACKAGE:
                        CameraDetail mCameraDetail = data.getParcelableExtra(OBJECT_CAMERA_DETAIL);
                        int position = data.getIntExtra(POSITION, -1);
                        mListCamera.set(position, mCameraDetail);
                        mCameraAdapter.notifyDataChanged(mListCamera);
                        if (mListCamera.size() != 0) {
                            //hiển thị layout triển khai camera
                            rdoDeploy.setEnabled(true);
                        } else {
                            rdoDeploy.setEnabled(false);
                        }
                        break;
                    case CODE_SERVICE_TYPE:
                        List<CategoryServiceList> mListServices = data.getParcelableArrayListExtra(Constants.SERVICE_TYPE_LIST);
                        mListServiceSelected.clear();
                        mListServiceSelected = mListServices;
                        StringBuilder service = new StringBuilder();
                        for (int i = 0; i < mListServiceSelected.size(); i++) {
                            if ((mListServiceSelected.size() - 1) == i) {
                                service.append(mListServiceSelected.get(i).getCategoryServiceName());
                            } else {
                                service.append(mListServiceSelected.get(i).getCategoryServiceName()).append(", ");
                            }
                        }
                        //set service type name to view
                        tvServiceType.setText(service.toString());
                        initViewByServiceType(mListServiceSelected);

                        //clear thông tin RP & E-Voucher
                        //is_clear_rp_and_voucher = true;
                        break;
                    case CODE_DEVICE_LIST_SELECTED:
                        Device mDevice = data.getParcelableExtra(Constants.DEVICE);
                        mListDevice.add(mDevice);
                        mDeviceAdapter.notifyData(mListDevice);
                        break;
                    case CODE_DEVICE_PRICE_LIST_SELECTED:
                        Device mDeviceSelected = data.getParcelableExtra(Constants.DEVICE);
                        if (mDeviceSelected == null) return;
                        int positions = data.getIntExtra(Constants.POSITION, 0);

                        if (mListDevice.size() > 1) {
                            for (Device item : mListDevice) {
                                if (item.getDeviceID() == mDeviceSelected.getDeviceID() &&
                                        item.getPriceID() == mDeviceSelected.getPriceID()) {
                                    Common.alertDialog(getString(R.string.message_double_price), this.getActivity());
                                    return;
                                }
                            }
                        }

                        mListDevice.set(positions, mDeviceSelected);
                        mDeviceAdapter.notifyData(mListDevice);
                        break;
                }
            }
        }
    }

    private boolean is_service_type = true;

    @SuppressLint("SetTextI18n")
    @Override
    protected void bindData() {
        List<CategoryServiceList> mListService = new ArrayList<>();
        if (mRegisterFptCameraModel != null && mRegisterFptCameraModel.getCategoryServiceList() != null &&
                mRegisterFptCameraModel.getCategoryServiceList().size() != 0) {
            mListService = mRegisterFptCameraModel.getCategoryServiceList();
        }
        if (((FptCameraActivity) getActivity()).getListService() != null &&
                ((FptCameraActivity) getActivity()).getListService().size() != 0) {
            mListService = ((FptCameraActivity) getActivity()).getListService();
        }

        StringBuilder service = new StringBuilder();

        if (mRegisterFptCameraModel.getCategoryServiceList() != null &&
                mRegisterFptCameraModel.getCategoryServiceList().size() != 0) {
            mListServiceSelected = mRegisterFptCameraModel.getCategoryServiceList();
        }

        for (int i = 0; i < mListServiceSelected.size(); i++) {
            if ((mListServiceSelected.size() - 1) == i) {
                service.append(mListServiceSelected.get(i).getCategoryServiceName());
            } else {
                service.append(mListServiceSelected.get(i).getCategoryServiceName()).append(", ");
            }
        }
        if (frmDevicesBanner.getVisibility() == View.VISIBLE) {
            tvServiceType.setText(service.toString());
        }
        else {
            tvServiceType.setText("FPT Camera");
        }
        tvLocalType.setText("FPT Camera");
        tvLocalType.setEnabled(false);
        mRegisterFptCameraModel.setLocalType(219);

        visibleDevicesControl(false);
        initDataDevice();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_fpt_camera_service;
    }

    @Override
    public void onItemClick(Object mObject, Integer position, Integer type) {
        //type == 1 is camera package, type == 3 remove cloud object
        Bundle bundle = new Bundle();
        Intent intent;
        switch (type) {
            case 1:
                CameraDetail mCameraDetail = (CameraDetail) mObject;
                bundle.putParcelable(CAMERA_DETAIL, mCameraDetail);
                bundle.putInt(POSITION, position);
                if (mRegisterFptCameraModel.getBaseObjID() == 0 &&
                        TextUtils.isEmpty(mRegisterFptCameraModel.getBaseContract())) {
                    bundle.putInt(COMBO, Constants.COMBO_TYPE_NO);
                } else {
                    bundle.putInt(COMBO, Constants.COMBO_TYPE_YES);
                }
                bundle.putString(CLASS_NAME, FptCameraService.class.getSimpleName());
                intent = new Intent(getContext(), CameraPackageActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_CAMERA_PACKAGE);
                break;
            case 3:
                CloudDetail mCloudDetailRemove = (CloudDetail) mObject;
                mListCloud.remove(mCloudDetailRemove);
                mCloudAdapter.notifyDataChanged(this.mListCloud);
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(Boolean isUpdatePromotion) {
        activity.setIsUpdatePromotion(isUpdatePromotion);
    }

    //update 3.14

    private void initViewByServiceType(List<CategoryServiceList> lstServiceType) {
        if (lstServiceType.size() == 1) {
            if (lstServiceType.get(0).getCategoryServiceID() == 5) {
                visibleDevicesControl(false);
            } else {
                visibleDevicesControl(true);
            }
        }

        for (CategoryServiceList item : lstServiceType) {
            if (item.getCategoryServiceID() == 5) {
                visibleCameraControl(true);
                visibleDevicesControl(false);
            }

            if (item.getCategoryServiceID() == 2) {
                visibleDevicesControl(true);
            }
        }
    }

    // ẩn hiện tab thiết bị
    private void visibleDevicesControl(boolean flag) {
        if (flag) {
            is_have_device = true;
            frmDevicesBanner.setVisibility(View.VISIBLE);
        } else {
            is_have_device = false;
            frmDevicesBanner.setVisibility(View.GONE);
            if (mListDevice.size() > 0) {
                mListDevice.clear();
            }
        }
        mDeviceAdapter.notifyData(mListDevice);
    }

    private void visibleCameraControl(boolean flag) {
        if (flag) {
            frmRegisterCamera.setVisibility(View.VISIBLE);
        } else {
            frmRegisterCamera.setVisibility(View.GONE);
        }
    }

    public List<Device> getListDeviceSelect() {
        return mListDevice;
    }

    // init data device
    public void initDataDevice() {
        if (mRegisterFptCameraModel != null && mRegisterFptCameraModel.getListDevice() != null) {
            mListDevice = mRegisterFptCameraModel.getListDevice();
            mDeviceAdapter.notifyData(mListDevice);
        }
        if (mListDevice.size() > 0) {
            visibleDevicesControl(true);
        }
    }

    @Override
    public void onItemClickV4(Integer type, Device mDevice, Integer position) {
        switch (type) {
            case 0:
                mListDevice.remove(mDevice);
                mDeviceAdapter.notifyData(mListDevice);
                break;
            case 1:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.DEVICE, mDevice);
                bundle.putInt(Constants.POSITION, position);
                bundle.putString(Constants.CLASS_NAME, FptCameraService.class.getSimpleName());
                Intent intent = new Intent(getActivity(), DevicePriceListCameraActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_DEVICE_PRICE_LIST_SELECTED);
                break;
        }
    }
}
