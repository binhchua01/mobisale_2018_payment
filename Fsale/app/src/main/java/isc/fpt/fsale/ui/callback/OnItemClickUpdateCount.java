package isc.fpt.fsale.ui.callback;

public interface OnItemClickUpdateCount<T> {
    void onItemClickUpdateCount(T object);
}
