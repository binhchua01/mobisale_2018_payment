package isc.fpt.fsale.activity.service_type;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class ServiceTypeActivity extends BaseActivitySecond
        implements OnItemClickListener<List<CategoryServiceList>> {
    private RelativeLayout rltBack;
    private Button btnUpdate, btnCancel;
    private List<CategoryServiceList> mList, mListSelected;

    @Override
    protected void initEvent() {
        rltBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListSelected != null && mListSelected.size() != 0) {
                    if (checkInternetOrNot()) {
                        Intent returnIntent = new Intent();
                        returnIntent.putParcelableArrayListExtra(Constants.SERVICE_TYPE_LIST,
                                (ArrayList<? extends Parcelable>) mListSelected);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        Common.alertDialog("Vui lòng chọn kèm Dịch Vụ Internet", ServiceTypeActivity.this);
                    }
                } else {
                    Common.alertDialog("Vui lòng chọn kèm Dịch Vụ Internet", ServiceTypeActivity.this);
                }
            }
        });
    }

    private boolean checkInternetOrNot() {
        for (CategoryServiceList item : mListSelected) {
            if (item.getCategoryServiceID() == 1) {//có chọn internet
                return true;
            }
        }
        return false;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_service_type;
    }

    @Override
    protected void initView() {
        getDataIntent();
        rltBack = (RelativeLayout) findViewById(R.id.btn_back);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_service_type);
        btnUpdate = (Button) findViewById(R.id.btn_update);
        btnCancel = (Button) findViewById(R.id.btn_cancel);
        for (CategoryServiceList item : mList) {
            for (CategoryServiceList itemSelected : mListSelected) {
                if (item.getCategoryServiceID().equals(itemSelected.getCategoryServiceID())) {
                    item.setSelected(true);
                }
            }
        }
        ServiceTypeAdapter mAdapter = new ServiceTypeAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelableArrayList(Constants.SERVICE_TYPE_LIST) != null) {
            mList = bundle.getParcelableArrayList(Constants.SERVICE_TYPE_LIST);
        }
        if (bundle != null && bundle.getParcelableArrayList(Constants.SERVICE_TYPE_LIST_SELECTED) != null) {
            mListSelected = bundle.getParcelableArrayList(Constants.SERVICE_TYPE_LIST_SELECTED);
        }
    }

    @Override
    public void onItemClick(List<CategoryServiceList> mList) {
        mListSelected.clear();
        for (CategoryServiceList item : mList) {
            if (item.isSelected()) {
                mListSelected.add(item);
            }
        }
    }
}
