package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.FragmentCustomerCareToDoList;
import isc.fpt.fsale.ui.fragment.FragmentListCustomerCare;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.util.Log;

public class UpdateConfirmNotification implements AsyncTaskCompleteListener<String>{ 

	private final String TAG_METHOD_NAME = "ConfirmNotification";
	private static String[] paramNames = new String[]{"Code", "Name", "Note"};
	private String[] paramValues;
	private Context mContext;
	private Fragment mFragment;
	
	public UpdateConfirmNotification(Context mContext, String Code, String Note, Fragment fragment) {
		this.mContext = mContext;		
		this.mFragment = fragment;
		String message = mContext.getResources().getString(R.string.msg_pd_update);
		String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
		this.paramValues = new String[]{Code, userName, Note };		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, UpdateConfirmNotification.this);
		service.execute();
	}
	
	
	public void handleUpdateRegistration(String result){	
		try {
			boolean hasError = false;
			List<UpdResultModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(jsObj, UpdResultModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//ServiceType Error
						 hasError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!hasError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("UpdateConfirmNotification:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
							
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);
	}

	private void loadData(List<UpdResultModel> lst){
		if(lst != null && lst.size() > 0){
			final UpdResultModel item = lst.get(0);
			if(item.getResultID() >0){	
				try {
					if(mFragment != null){
						if(mFragment.getClass().getSimpleName().equals(FragmentCustomerCareToDoList.class.getSimpleName())){
							final FragmentCustomerCareToDoList fragment = (FragmentCustomerCareToDoList)mFragment;
							new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(item.getResult())
			 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
			 				    @Override
			 				    public void onClick(DialogInterface dialog, int which) {			 				      
			 				    	fragment.getData();
			 				       dialog.cancel();
			 				    }
			 				})
							.setCancelable(false).create().show();;
						}else if(mFragment.getClass().getSimpleName().equals(FragmentListCustomerCare.class.getSimpleName())){
							final FragmentListCustomerCare fragment = (FragmentListCustomerCare)mFragment;
							new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(item.getResult())
			 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
			 				    @Override
			 				    public void onClick(DialogInterface dialog, int which) {			 				      
			 				    	fragment.getData();
			 				       dialog.cancel();
			 				    }
			 				})
							.setCancelable(false).create().show();;
						}
					}else
						Common.alertDialog(lst.get(0).getResult(), mContext);
				} catch (Exception e) {
					// TODO: handle exception

					Common.alertDialog(lst.get(0).getResult(), mContext);
				}
			}else
				Common.alertDialog(lst.get(0).getResult(), mContext);
		}
		
		
	}
}
