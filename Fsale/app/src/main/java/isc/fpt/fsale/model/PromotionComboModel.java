package isc.fpt.fsale.model;

public class PromotionComboModel {

	private int ComboID;
	private int PromotionID;
	private String Description;
	private String PromotionPayTVID;
	
	public PromotionComboModel(){
		
	}
	
	public PromotionComboModel(int comboID, int promotionID, String description, String promotionPayTVID) {
		ComboID = comboID;
		PromotionID = promotionID;
		Description = description;
		PromotionPayTVID = promotionPayTVID;
	}
	public int getComboID() {
		return ComboID;
	}
	public void setComboID(int comboID) {
		ComboID = comboID;
	}
	public int getPromotionID() {
		return PromotionID;
	}
	public void setPromotionID(int promotionID) {
		PromotionID = promotionID;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getPromotionPayTVID() {
		return PromotionPayTVID;
	}
	public void setPromotionPayTVID(String promotionPayTVID) {
		PromotionPayTVID = promotionPayTVID;
	}
}
