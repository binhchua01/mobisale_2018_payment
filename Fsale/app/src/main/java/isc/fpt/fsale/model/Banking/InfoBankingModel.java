package isc.fpt.fsale.model.Banking;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InfoBankingModel implements Parcelable {

    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("ResultID")
    @Expose
    private Integer resultID;

    protected InfoBankingModel(Parcel in) {
        result = in.readString();
        if (in.readByte() == 0) {
            resultID = null;
        } else {
            resultID = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(result);
        if (resultID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(resultID);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InfoBankingModel> CREATOR = new Creator<InfoBankingModel>() {
        @Override
        public InfoBankingModel createFromParcel(Parcel in) {
            return new InfoBankingModel(in);
        }

        @Override
        public InfoBankingModel[] newArray(int size) {
            return new InfoBankingModel[size];
        }
    };

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getResultID() {
        return resultID;
    }

    public void setResultID(Integer resultID) {
        this.resultID = resultID;
    }
}
