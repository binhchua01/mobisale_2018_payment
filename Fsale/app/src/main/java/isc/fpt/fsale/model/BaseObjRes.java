package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by haulc3 on 14,November,2019
 */
public class BaseObjRes<T> {
    @SerializedName("Error")
    @Expose
    private String error;
    @SerializedName("ErrorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("ListObject")
    @Expose
    private List<T> listObject;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public List<T> getListObject() {
        return listObject;
    }

    public void setListObject(List<T> listObject) {
        this.listObject = listObject;
    }
}
