package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetReportPrecheckListAction;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ReportPrecheckListAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportPrecheckListModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

public class ListReportPrecheckListActivity extends BaseActivity implements OnItemClickListener {

    private Spinner spDay, spMonth, spYear, spAgent;
    private ListView lvResult;
    private LinearLayout frmFind;
    private ImageView imgNavigation;
    private Spinner spPage;
    private EditText txtAgentName;

    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
    private int Day = 0, Month = 0, Year = 0, Agent = 0;
    private String AgentName;

    public ListReportPrecheckListActivity() {
        super(R.string.title_ReportPrecheckList);
    }

    @Override
    public void startActivity(Intent intent) {
        // TODO Auto-generated method stub
        //Finish this activity after start another activity
        finish();
        super.startActivity(intent);
    }

    @SuppressLint("InflateParams")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_report_precheck_list_activity));


        setContentView(R.layout.activity_report_prechecklist_total);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }

        spDay = (Spinner) findViewById(R.id.sp_day);
        spMonth = (Spinner) findViewById(R.id.sp_month);
        spYear = (Spinner) findViewById(R.id.sp_year);

        lvResult = (ListView) findViewById(R.id.lv_report);
        txtAgentName = (EditText) findViewById(R.id.txt_agent_name);

        final Button btnFind = (Button) findViewById(R.id.btn_find);
        imgNavigation = (ImageView) findViewById(R.id.img_navigation_drop_down);
        frmFind = (LinearLayout) findViewById(R.id.frm_find);
        btnFind.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (FLAG_FIRST_LOAD > 0)
                    FLAG_FIRST_LOAD = 0;
                getReport(1);
            }
        });

        imgNavigation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dropDownNavigation();
            }
        });
        spYear.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                int month = 0, year = 0;
                try {
                    year = item.getID();
                    month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
                } catch (Exception e) {

                    Log.i("ContractFilterActivity_spYear.setOnItemSelectedListener:", e.getMessage());
                }
                //common.
                Common.initDaySpinner(ListReportPrecheckListActivity.this, spDay, month, year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spMonth.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                int month = 0, year = 0;
                try {
                    month = item.getID();
                    year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();
                } catch (Exception e) {

                    Log.i("ContractFilterActivity_spMonth.setOnItemSelectedListener:", e.getMessage());
                }
                Common.initDaySpinner(ListReportPrecheckListActivity.this, spDay, month, year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        lvResult.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            }
        });

        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getReport(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spAgent = (Spinner) findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() > 0) {
                    txtAgentName.setEnabled(true);
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        Common.initYearSpinner(this, spYear);
        Common.initMonthSpinner(this, spMonth);
        //initSpinnerStatus();
        initSpinnerAgent();

        // super.addRight(new MenuRightReport());


    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    /*@Override
      public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
          int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else if (itemId == R.id.action_right) {
            toggle(SlidingMenu.RIGHT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
      }
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //MenuInflater inflater = getSupportMenuInflater();
        //inflater.inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View convertView, int position, long id) {

    }

    //=============================================

    private void initSpinnerAgent() {
        //Loai Tim kiem
        ArrayList<KeyValuePairModel> ListSpAgent = new ArrayList<KeyValuePairModel>();
        ListSpAgent.add(new KeyValuePairModel(0, "Tất cả"));
        ListSpAgent.add(new KeyValuePairModel(1, "Nhân viên"));
        KeyValuePairAdapter adapterAgent = new KeyValuePairAdapter(this, R.layout.my_spinner_style, ListSpAgent, Gravity.LEFT);
        spAgent.setAdapter(adapterAgent);

    }

    private void getReport(int pageNumber) {

        AgentName = txtAgentName.getText().toString();
        if (AgentName.equals(""))
            AgentName = "0";
        Agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
        Day = ((KeyValuePairModel) spDay.getSelectedItem()).getID();
        Month = ((KeyValuePairModel) spMonth.getSelectedItem()).getID();
        Year = ((KeyValuePairModel) spYear.getSelectedItem()).getID();

        //String params = Services.getParams(new String[]{Constants.USERNAME, String.valueOf(month), String.valueOf(year),
        //String.valueOf(agent), agentName, String.valueOf(curPage)});
        //GetReportPrecheckListAction(Context context, String SaleAccount, int Month, int Year, String Agent, String AgentName, int PageNumber){
        if (Agent > 0 && AgentName.equals("")) {
            Common.alertDialog(getString(R.string.msg_search_value_empty), this);
        } else {
            new GetReportPrecheckListAction(this, Constants.USERNAME, Day, Month, Year, Agent, AgentName, pageNumber);
        }

    }
	
	/*public void LoadData(ArrayList<ReportPrecheckListModel> list){
		try {
			lstReport = list;
			if(lstReport != null && lstReport.size() > 0){					
				ReportPrecheckListAdapter abc = new ReportPrecheckListAdapter(mContext, lstReport);
				lvObject.setAdapter(abc);
				dropDownNavigation();
				ReportPrecheckListModel item1 = lstReport.get(0);
				mTotalPage = item1.getTotalPage();	
				int totalPage = mTotalPage;		
				txt_row_total.setText(String.valueOf(item1.getTotalRow()));
				if(SpPage.getAdapter() == null || FLAG_FIRST_LOAD == 0){
					ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
					for(int i=1; i <= totalPage; i++){
						lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));				
					}										
					KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPage, Color.WHITE);
					
					SpPage.setAdapter(pageAdapter);
				}
			}else{
				txt_row_total.setText("0");
				lvObject.setAdapter(null);
				Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
			}
			else
			{
				lstReport = new ArrayList<ReportPrecheckListModel>();
				txt_row_total.setText("0");
				ReportPrecheckListAdapter abc = new ReportPrecheckListAdapter(mContext, lstReport, month, year);
				lvObject.setAdapter(abc);
				
				ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
				lstPage.add(new KeyValuePairModel(0, String.valueOf(0)));
				KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPage, Color.WHITE);
				
				SpPage.setAdapter(pageAdapter);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}*/

    public void loadData(ArrayList<ReportPrecheckListModel> lst) {
        if (lst != null && lst.size() > 0) {
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                dropDownNavigation();
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
                //adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            ReportPrecheckListAdapter adapter = new ReportPrecheckListAdapter(this, lst);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
            lvResult.setAdapter(new ReportPrecheckListAdapter(this, new ArrayList<ReportPrecheckListModel>()));
        }

    }

    private void dropDownNavigation() {
        if (frmFind.getVisibility() == View.VISIBLE) {
            frmFind.setVisibility(View.GONE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
        } else {
            frmFind.setVisibility(View.VISIBLE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
        }
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }


}
