package isc.fpt.fsale.ui.fragment;

import isc.fpt.fsale.R;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageButton;

public class HouseNumFormatRuleDialog extends DialogFragment{

	private ImageButton imgClose;
	private WebView webView;

	public HouseNumFormatRuleDialog(){}


	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    // Inflate the layout to use as dialog or embedded fragment
		View view = inflater.inflate(R.layout.dialog_house_number_format_rule, container);
		imgClose = (ImageButton) view.findViewById(R.id.btn_close);
		imgClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getDialog().dismiss();
			}
		});
		webView = (WebView) view.findViewById(R.id.web_view);
		setText();
		return view;
	}
	
	private void setText(){		
		webView.loadUrl("file:///android_asset/html/house_num_format.htm");
	}
	/** The system calls this only when creating the layout in a dialog. */
	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {	   
	    Dialog dialog = super.onCreateDialog(savedInstanceState);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    return dialog;
	}
}
