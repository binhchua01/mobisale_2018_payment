package isc.fpt.fsale.model.Banking;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListBankingModel implements Parcelable {

    @SerializedName("ActiveDay")
    @Expose
    private String activeDay;
    @SerializedName("CardNumber")
    @Expose
    private String cardNumber;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ImgUrl")
    @Expose
    private String imgUrl;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("StatusId")
    @Expose
    private Integer statusId;
    @SerializedName("StatusName")
    @Expose
    private String statusName;

    protected ListBankingModel(Parcel in) {
        activeDay = in.readString();
        cardNumber = in.readString();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        imgUrl = in.readString();
        name = in.readString();
        if (in.readByte() == 0) {
            statusId = null;
        } else {
            statusId = in.readInt();
        }
        statusName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(activeDay);
        dest.writeString(cardNumber);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(imgUrl);
        dest.writeString(name);
        if (statusId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(statusId);
        }
        dest.writeString(statusName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ListBankingModel> CREATOR = new Creator<ListBankingModel>() {
        @Override
        public ListBankingModel createFromParcel(Parcel in) {
            return new ListBankingModel(in);
        }

        @Override
        public ListBankingModel[] newArray(int size) {
            return new ListBankingModel[size];
        }
    };

    public String getActiveDay() {
        return activeDay;
    }

    public void setActiveDay(String activeDay) {
        this.activeDay = activeDay;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

}
