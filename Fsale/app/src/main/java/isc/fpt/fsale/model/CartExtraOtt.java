package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by haulc3 on 20,July,2019
 */
public class CartExtraOtt implements Parcelable {
    @SerializedName("IsAutoActive")
    @Expose
    private Integer isAutoActive;
    @SerializedName("SaleType")
    @Expose
    private Integer saleType;
    @SerializedName("ServiceList")
    @Expose
    private List<ServiceListExtraOtt> serviceList;

    public CartExtraOtt() { }

    public CartExtraOtt(Integer isAutoActive, Integer saleType, List<ServiceListExtraOtt> serviceList) {
        this.isAutoActive = isAutoActive;
        this.saleType = saleType;
        this.serviceList = serviceList;
    }

    protected CartExtraOtt(Parcel in) {
        isAutoActive = in.readInt();
        saleType = in.readInt();
        serviceList = in.createTypedArrayList(ServiceListExtraOtt.CREATOR);
    }

    public static final Creator<CartExtraOtt> CREATOR = new Creator<CartExtraOtt>() {
        @Override
        public CartExtraOtt createFromParcel(Parcel in) {
            return new CartExtraOtt(in);
        }

        @Override
        public CartExtraOtt[] newArray(int size) {
            return new CartExtraOtt[size];
        }
    };

    public Integer getIsAutoActive() {
        return isAutoActive;
    }

    public void setIsAutoActive(Integer isAutoActive) {
        this.isAutoActive = isAutoActive;
    }

    public Integer getSaleType() {
        return saleType;
    }

    public void setSaleType(Integer saleType) {
        this.saleType = saleType;
    }

    public List<ServiceListExtraOtt> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<ServiceListExtraOtt> serviceList) {
        this.serviceList = serviceList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(isAutoActive);
        dest.writeInt(saleType);
        dest.writeTypedList(serviceList);
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("IsAutoActive", getIsAutoActive());
            jsonObject.put("SaleType", getSaleType());
            JSONArray jsonArray = new JSONArray();
            for(ServiceListExtraOtt item : getServiceList()){
                jsonArray.put(item.toJsonObject());
            }
            jsonObject.put("ServiceList", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
