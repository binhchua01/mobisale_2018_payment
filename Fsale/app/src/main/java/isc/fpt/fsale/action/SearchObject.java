package isc.fpt.fsale.action;

import android.content.Context;
import android.view.View;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.adapter.ObjectListAdapter;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

public class SearchObject implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private TextView txtObjectId;
    private TextView txtCustomer;
    private ListView lvObjectList;
    private ScrollView frmPreCheckListInfo;
    private ArrayList<ObjectModel> lstObject;
    private int l;
    private JSONArray jsArr;

    public SearchObject(Context mContext, String Contract, String Username,
                        String searchType, TextView txtObjectId, TextView txtCustomer) {
        this.mContext = mContext;
        this.txtObjectId = txtObjectId;
        this.txtCustomer = txtCustomer;
        String[] params = new String[]{"Agent", "AgentName", "UserName", "PageNumber"};
        String[] arrParams = new String[]{searchType, Contract, Username, "1"};
        if (mContext != null) {
            frmPreCheckListInfo = (ScrollView) ((CreatePreChecklistActivity) (mContext)).findViewById(R.id.frm_prechecklist_info);
            lvObjectList = (ListView) ((CreatePreChecklistActivity) (mContext)).findViewById(R.id.lv_object_list);
        }
        String message = mContext.getResources().getString(R.string.msg_pd_get_search_contact);
        String SEARCH_OBJECT = "SearchObject";
        CallServiceTask service = new CallServiceTask(mContext, SEARCH_OBJECT, params, arrParams,
                Services.JSON_POST, message, SearchObject.this);
        service.execute();
    }

    private void HandleResultSearch(String json) {
        if (Common.jsonObjectValidate(json)) {
            bindData(json);
        } else {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
        }
    }

    public void bindData(String result) {
        JSONObject jsObj;
        lstObject = new ArrayList<>();
        try {
            jsObj = JSONParsing.getJsonObj(result);
            String TAG_SEARCH_OBJECT_RESULT = "SearchObjectMethodPostResult";
            jsArr = jsObj.getJSONArray(TAG_SEARCH_OBJECT_RESULT);
            l = jsArr.length();
            showCreatePreCheckListNew();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showCreatePreCheckListNew() {
        try {
            if (l > 0) {
                String TAG_ERROR = "ErrorService";
                String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                if (error.equals("null")) {
                    for (int i = 0; i < l; i++) {
                        JSONObject item = jsArr.getJSONObject(i);
                        lstObject.add(ObjectModel.Parse(item));
                    }
                    lvObjectList.setAdapter(new ObjectListAdapter(mContext, lstObject));
                    lvObjectList.setVisibility(View.VISIBLE);
                    frmPreCheckListInfo.setVisibility(View.GONE);

                } else {
                    Common.alertDialog("Lỗi WS:" + error, mContext);
                    txtObjectId.setText("");
                    txtCustomer.setText("");
                    frmPreCheckListInfo.setVisibility(View.GONE);
                    lvObjectList.setVisibility(View.GONE);
                }
            } else {
                Common.alertDialog("Không tìm thấy dữ liệu", mContext);
                txtObjectId.setText("");
                txtCustomer.setText("");
                frmPreCheckListInfo.setVisibility(View.GONE);
                lvObjectList.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        HandleResultSearch(result);
    }
}
