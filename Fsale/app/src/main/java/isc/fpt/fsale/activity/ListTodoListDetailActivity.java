package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetTodoListDetail;
import isc.fpt.fsale.adapter.ToDoListDetailAdapter;
import isc.fpt.fsale.ui.fragment.UpdateTodoListDetailDialog;
import isc.fpt.fsale.model.ToDoListDetailModel;
import isc.fpt.fsale.model.ToDoListModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.List;

import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewOutlineProvider;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;


public class ListTodoListDetailActivity extends BaseActivity implements OnScrollListener {


    private ListView mListView;
    private ImageView imgCreate;

    private ToDoListModel todoListItem;

    private int mCurrentPage = 1, mTotalPage = 1;

    UpdateTodoListDetailDialog aboutDialog;

    public ListTodoListDetailActivity() {
        super(R.string.lbl_screen_name_todolist_detail);
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_todolist_detail));
        setContentView(R.layout.activity_todolist_detail);
        mListView = (ListView) findViewById(R.id.lv_object);
        imgCreate = (ImageButton) findViewById(R.id.img_create_potential_obj);
        if (Build.VERSION.SDK_INT >= 21) {//Lollipop

            imgCreate.setOutlineProvider(new ViewOutlineProvider() {

                @Override
                public void getOutline(View view, Outline outline) {
                    // TODO Auto-generated method stub
                    int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
                    outline.setOval(0, 0, diameter, diameter);
                }
            });
            imgCreate.setClipToOutline(true);
            StateListAnimator sla = AnimatorInflater.loadStateListAnimator(this, R.drawable.selector_button_add_material_design);

            imgCreate.setStateListAnimator(sla);//getDrawable(R.drawable.selector_button_add_material_design));
            //android:stateListAnimator="@drawable/selector_button_add_material_design"
        }
        imgCreate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                UpdateTodoListDetailDialog aboutDialog = new UpdateTodoListDetailDialog(ListTodoListDetailActivity.this, todoListItem);
                Common.showFragmentDialog(ListTodoListDetailActivity.this.getSupportFragmentManager(), aboutDialog, "fragment_update_todolist_detail_dialog");
            }
        });
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            todoListItem = intent.getExtras().getParcelable("TODOLIST_ITEM");
            getData(1);
        }
    }

    public void getData(int page) {
        String UserName = "", Contract = "", AgentName = "";
        int Agent = 0;
        UserName = ((MyApp) this.getApplication()).getUserName();
        Contract = todoListItem.getContract();
        Agent = 1;
        AgentName = String.valueOf(todoListItem.getID());
        new GetTodoListDetail(this, UserName, Contract, Agent, AgentName);
    }

    @SuppressLint("LongLogTag")
    public void loadData(List<ToDoListDetailModel> lst) {
        if (lst != null && lst.size() > 0) {
            mTotalPage = lst.get(lst.size() - 1).getTotalPage();
            mCurrentPage = lst.get(lst.size() - 1).getCurrentPage();
            if (mListView.getAdapter() == null || mListView.getAdapter().getCount() <= 0) {
                ToDoListDetailAdapter mAdapter = new ToDoListDetailAdapter(this, lst);
                mListView.setAdapter(mAdapter);
            } else {
                try {
                    if (mCurrentPage > 1)
                        ((ToDoListDetailAdapter) mListView.getAdapter()).addAll(lst);
                    else
                        ((ToDoListDetailAdapter) mListView.getAdapter()).setListData(lst);
                } catch (Exception e) {

                    Log.i("FragmentCustomerCareToDoList:", e.getMessage());
                }
            }
        } else {
            Common.alertDialog("Không có dữ liệu!", this);
        }
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    private int currentVisibleItemCount;
    private int currentScrollState;
    private int currentFirstVisibleItem;
    private int totalItem;

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // TODO Auto-generated method stub
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItem = totalItemCount;

    }

    private void isScrollCompleted() {
        if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                && this.currentScrollState == SCROLL_STATE_IDLE && currentVisibleItemCount > 0) {
            if (mCurrentPage < mTotalPage) {
                mCurrentPage++;
                getData(mCurrentPage);
            }
        }

    }
}
