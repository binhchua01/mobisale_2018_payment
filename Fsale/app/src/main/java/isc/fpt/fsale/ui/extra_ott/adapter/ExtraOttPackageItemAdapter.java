package isc.fpt.fsale.ui.extra_ott.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.DetailPackage;
import isc.fpt.fsale.model.ServiceListExtraOtt;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV2;
import isc.fpt.fsale.utils.Common;

/**
 * Created by haulc3 on 19,July,2019
 */
public class ExtraOttPackageItemAdapter extends RecyclerView.Adapter<ExtraOttPackageItemAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<ServiceListExtraOtt> mList;
    private OnItemClickListenerV2 mListener;

    public ExtraOttPackageItemAdapter(Context mContext, List<ServiceListExtraOtt> mList,
                                      OnItemClickListenerV2 mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_extra_ott_package, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindData(mList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvPackageName, tvPromotion, tvQuantity;
        TextView tvQuantityLess, tvQuantityPlus, tvPrice;
        ImageView imgDelete;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvPackageName = (TextView) itemView.findViewById(R.id.tv_extra_ott_package_name);
            tvPromotion = (TextView) itemView.findViewById(R.id.tv_promotion);
            tvQuantity = (TextView) itemView.findViewById(R.id.tv_quantity);
            tvQuantityLess = (TextView) itemView.findViewById(R.id.tv_quantity_less);
            tvQuantityPlus = (TextView) itemView.findViewById(R.id.tv_quantity_plus);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
            imgDelete = (ImageView) itemView.findViewById(R.id.img_delete);
        }

        @SuppressLint("SetTextI18n")
        public void bindData(final ServiceListExtraOtt mServiceListExtraOtt, final int position) {
            if(mServiceListExtraOtt.getPromoCommand().getDetailPackage() != null ||
                    mServiceListExtraOtt.getPromoCommand().getDetailPackage().size() != 0){
                StringBuilder packageName = new StringBuilder();
                final List<DetailPackage> mListPackageOtt = mServiceListExtraOtt.getPromoCommand().getDetailPackage();
                for (int i = 0; i < mListPackageOtt.size(); i++) {
                    if((mListPackageOtt.size() - 1) == i){
                        packageName.append(mListPackageOtt.get(i).getName());
                    }else{
                        packageName.append(mListPackageOtt.get(i).getName()).append(" & ");
                    }
                }
                tvPackageName.setText(packageName);
            }
            tvPromotion.setText((mServiceListExtraOtt.getPromoCommand().getCommandText() == null ?
                    "" : mServiceListExtraOtt.getPromoCommand().getCommandText()));
            tvQuantity.setText(String.valueOf(mServiceListExtraOtt.getQuantity() == null ?
                    0 : mServiceListExtraOtt.getQuantity()));
            tvPrice.setText(calculatorPrice(mServiceListExtraOtt));

            tvPromotion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //chưa chọn package
                    if(mServiceListExtraOtt.getPromoCommand().getDetailPackage() == null ||
                            mServiceListExtraOtt.getPromoCommand().getDetailPackage().size() == 0){
                        return;
                    }
                    mListener.onItemClick(position, 1);//get promotion
                }
            });

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(position, 2);//remove this item
                }
            });

            tvQuantityLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //chưa chọn CLKM
                    if(mServiceListExtraOtt.getPromoCommand().getMinCount() == null){
                        return;
                    }
                    int quantity = mServiceListExtraOtt.getQuantity();
                    if(quantity == mServiceListExtraOtt.getPromoCommand().getMinCount()){
                        return;
                    }
                    quantity--;
                    mList.get(position).setQuantity(quantity);
                    tvQuantity.setText(String.valueOf(mServiceListExtraOtt.getQuantity()));
                    tvPrice.setText(calculatorPrice(mServiceListExtraOtt));

                }
            });

            tvQuantityPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mServiceListExtraOtt.getPromoCommand().getMaxCount() == null){
                        return;
                    }
                    int quantity = mServiceListExtraOtt.getQuantity();
                    if(quantity >= mServiceListExtraOtt.getPromoCommand().getMaxCount()){
                        return;
                    }
                    quantity++;
                    mList.get(position).setQuantity(quantity);
                    tvQuantity.setText(String.valueOf(mServiceListExtraOtt.getQuantity()));
                    tvPrice.setText(calculatorPrice(mServiceListExtraOtt));
                }
            });
        }
    }

    public void notifyData(List<ServiceListExtraOtt> mList){
        this.mList = mList;
        notifyDataSetChanged();
    }

    private String calculatorPrice(ServiceListExtraOtt mServiceListExtraOtt){
        int total = mServiceListExtraOtt.getPromoCommand().getTotalAmountVAT() == null ?
                0 : mServiceListExtraOtt.getPromoCommand().getTotalAmountVAT() *
                (mServiceListExtraOtt.getQuantity() == null ? 0 : mServiceListExtraOtt.getQuantity());
        return String.format(mContext.getString(R.string.txt_unit_price), Common.formatNumber(total));
    }
}
