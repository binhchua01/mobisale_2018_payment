package isc.fpt.fsale.ui.maxytv.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV4;
import isc.fpt.fsale.ui.callback.OnItemClickUpdateCount;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.utils.Common;

/**
 * Created by nhannh26 on 2021-05-28.
 */
public class MaxyDeviceContractsAdapter extends RecyclerView.Adapter<MaxyDeviceContractsAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<MaxyBox> mList;
    private OnItemClickListenerV4<Object, Integer, Integer> mListener;
    private OnItemClickUpdateCount<Boolean> mUpdateCount;
    private int numBox;

    public MaxyDeviceContractsAdapter(Context mContext, List<MaxyBox> mList,
                                      OnItemClickListenerV4<Object, Integer, Integer> mListener, OnItemClickUpdateCount<Boolean> mUpdatecount) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
        this.mUpdateCount = mUpdatecount;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_list_maxytv_device, parent, false);
        return new SimpleViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.bindView(mList.get(position), position, mContext);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvMaxyTVDeviceName, tvMaxyTVPackageQuantity, tvPricePerUnit,
                tvMaxyTVPackagePlus, tvMaxyTVPackageLess, tvMaxyTVPromotion;
        ImageView imgDelete;

        SimpleViewHolder(View itemView) {
            super(itemView);
            tvMaxyTVDeviceName = (TextView) itemView.findViewById(R.id.tv_device_maxytv);
            tvMaxyTVPromotion = (TextView) itemView.findViewById(R.id.tv_selected_maxytv_promotion);
            tvMaxyTVPackagePlus = (TextView) itemView.findViewById(R.id.tv_maxytv_device_plus);
            tvMaxyTVPackageLess = (TextView) itemView.findViewById(R.id.tv_maxytv_device_less);
            tvMaxyTVPackageQuantity = (TextView) itemView.findViewById(R.id.lbl_maxytv_device_quantity);
            tvPricePerUnit = (TextView) itemView.findViewById(R.id.tv_maxytv_device_total);
            imgDelete = itemView.findViewById(R.id.img_delete);
        }

        void bindView(final MaxyBox mDetail, final int position, Context context) {

            tvMaxyTVDeviceName.setText(mDetail.getBoxName());
            if (mDetail.getPromotionDesc() == null) {
                tvMaxyTVPromotion.setText(mContext.getResources().getString(R.string.lbl_message_choose_promotion_maxy_device));
            } else {
                tvMaxyTVPromotion.setText(mDetail.getPromotionDesc());
            }
            tvMaxyTVPackageQuantity.setText(String.valueOf(mDetail.getBoxCount()));
            tvPricePerUnit.setText(
                    calculatorTotalCameraPackage(mDetail.getPrice(), mDetail.getBoxCount())
            );
            //set event when click to position
            tvMaxyTVPromotion.setOnClickListener(view -> {
                mListener.onItemClickV4(mDetail, position, 7);//promotion
            });
            if (mDetail.getPromotionID() != 0) {
                //set event press less or plus quantity
                tvMaxyTVPackageLess.setOnClickListener(view -> {
                    if (mList.get(position).getPromotionID() != 0) {
                        int min = 1;
                        int quantity = mList.get(position).getBoxCount();
                        if (quantity == min) {
                            return;
                        }
                        quantity--;
                        mList.get(position).setBoxCount(quantity);
                        tvPricePerUnit.setText(
                                calculatorTotalCameraPackage(mList.get(position).getPrice(), mList.get(position).getBoxCount())
                        );
                        notifyDataChanged(mList);
                        mUpdateCount.onItemClickUpdateCount(true);
                    }
                });
                tvMaxyTVPackagePlus.setOnClickListener(view -> {
                    if (mList.get(position).getPromotionID() != 0) {
                        if (((RegisterContractActivity) mContext).getMaxyFragment().numBox > ((RegisterContractActivity) mContext).getMaxyFragment().count) {
                            int max;
                            if (((RegisterContractActivity) mContext).getMaxyFragment().serviceType == 0) {
                                max = ((RegisterContractActivity) mContext).getMaxyFragment().numBox - 1;
                            } else {
                                max = ((RegisterContractActivity) mContext).getMaxyFragment().numBox;
                            }
                            int quantity = mList.get(position).getBoxCount();
                            if (quantity == max) {
                                return;
                            }
                            quantity++;
                            mList.get(position).setBoxCount(quantity);
                            tvPricePerUnit.setText(
                                    calculatorTotalCameraPackage(mList.get(position).getPrice(), mList.get(position).getBoxCount())
                            );
                            notifyDataChanged(mList);
                            mUpdateCount.onItemClickUpdateCount(true);
                        } else {
                            Toast.makeText(mContext, mContext.getString(R.string.msg_select_num_box), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            imgDelete.setOnClickListener(view -> {
                mListener.onItemClickV4(mDetail, position, 6);//delete
            });
        }

    }

    public void notifyDataChanged(List<MaxyBox> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    private String calculatorTotalCameraPackage(int cost, int quantity) {
        return String.format(mContext.getString(R.string.txt_unit_price), Common.formatNumber((cost * quantity)));
    }
}
