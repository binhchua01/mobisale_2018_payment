package isc.fpt.fsale.adapter.upsell;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danh32.fontify.Button;
import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.upsell.ResendShortlink;
import isc.fpt.fsale.activity.upsell.history.HistoryUpsellActivity;
import isc.fpt.fsale.callback.upsell.OnItemListenerUpsell;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeTransHistoryModel;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeTransListModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class ObjProgramHistoryAdapter extends RecyclerView.Adapter<ObjProgramHistoryAdapter.SimpleViewHolder>{

    private Context mContext;
    private List<ObjUpgradeTransHistoryModel> list;

    public ObjProgramHistoryAdapter(Context mContext, List<ObjUpgradeTransHistoryModel> mList) {
        this.mContext = mContext;
        this.list = mList;
    }
    @Override
    public ObjProgramHistoryAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_history_upsell, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ObjProgramHistoryAdapter.SimpleViewHolder holder, int position) {
        holder.bindView(list.get(position), position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder{
        TextView mDayCare, mCustomer, mProgram, mMoneyUpgrade, mCharge, mChargeUpgrade, mSurvey, mNote, mReason, mKMNet, mKMPaytv, mMoneyTT;
        public SimpleViewHolder(View itemView) {
            super(itemView);
            mDayCare = itemView.findViewById(R.id.txt_day_care);
            mCustomer = itemView.findViewById(R.id.txt_person_care);
            mProgram = itemView.findViewById(R.id.txt_program);
            mMoneyUpgrade = itemView.findViewById(R.id.txt_money_upgrade);
            mCharge = itemView.findViewById(R.id.txt_money_present);
            mChargeUpgrade = itemView.findViewById(R.id.txt_charge_upgrade);
            mSurvey = itemView.findViewById(R.id.txt_survey_results);
            mNote = itemView.findViewById(R.id.txt_note);
            //update ver
            mReason = itemView.findViewById(R.id.txt_reason);
            mKMNet = itemView.findViewById(R.id.txt_km_net);
            mKMPaytv = itemView.findViewById(R.id.txt_km_paytv);
            mMoneyTT = itemView.findViewById(R.id.txt_money_tt);
        }

        @SuppressLint("SetTextI18n")
        public void bindView(final ObjUpgradeTransHistoryModel model, final int position){
            mDayCare.setText(model.getCreatedDate());
            mCustomer.setText(model.getSaleAdvisor());
            mProgram.setText(model.getProgramUpgradeName());
            mMoneyUpgrade.setText(Common.formatMoney(model.getMoneyUpgrade().toString()));
            mCharge.setText(Common.formatMoney(model.getFeeFromLocalType().toString()));
            double currentPay = model.getMoneyUpgrade() + model.getFeeFromLocalType();
            mChargeUpgrade.setText(Common.formatMoney(Double.toString(currentPay)));
            mSurvey.setText(model.getUpgradeStatusName());
            mReason.setText(model.getAdvisoryUpgradeName());
            mNote.setText(model.getNote());
            mKMNet.setText(model.getPromotionNetName());
            mKMPaytv.setText(model.getPromotionIPTVName());
            mMoneyTT.setText(model.getTotalMoney().toString());
        }
    }

    public void notifyData(List<ObjUpgradeTransHistoryModel> mList) {
        this.list = mList;
        notifyDataSetChanged();
    }
}
