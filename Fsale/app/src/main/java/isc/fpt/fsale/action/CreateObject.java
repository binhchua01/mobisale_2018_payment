package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class CreateObject implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    public final String TAG_METHOD_NAME = "CreateObject";
    private int RegID;

    public CreateObject(Context context, String UserName, int regID, String RegCode) {
        mContext = context;
        this.RegID = regID;
        String[] paramNames = new String[]{"UserName", "RegCode"};
        String[] paramValues = new String[]{UserName, RegCode};
        String message = "Đang cập nhật...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME,
                paramNames, paramValues, Services.JSON_POST, message, CreateObject.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(jsObj, UpdResultModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    if (resultObject.getListObject().size() > 0) {
                        UpdResultModel item = resultObject.getListObject().get(0);
                        if (item.getResultID() > 0) {
                            new AlertDialog.Builder(mContext)
                                    .setTitle(mContext.getResources()
                                            .getString(R.string.title_notification))
                                    .setMessage(item.getResult())
                                    .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            new GetRegistrationDetail(mContext, Constants.USERNAME, RegID);
                                            dialog.cancel();
                                        }
                                    })
                                    .setCancelable(false)
                                    .create()
                                    .show();
                        } else {
                            Common.alertDialog(item.getResult(), mContext);
                        }
                    }
                } else {
                    Common.alertDialog(resultObject.getError(), mContext);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
