package isc.fpt.fsale.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.danh32.fontify.Button;
import com.danh32.fontify.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListPaidType;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.action.PaymentInformation;
import isc.fpt.fsale.action.StartPayment;
import isc.fpt.fsale.action.UpdatePayment;
import isc.fpt.fsale.adapter.PaidTypeAdapter;
import isc.fpt.fsale.model.CreateObject_2018POST;
import isc.fpt.fsale.model.PaidType;
import isc.fpt.fsale.model.PaymentInformationPOST;
import isc.fpt.fsale.model.PaymentInformationResult;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.StartPaymentPOST;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Pay Contract Activity
 * Author: TruongPV5
 * Date: 20/04/2018
 * update by : haulc3
 */
//màn hình thanh toán online bán mới
public class PayContractActivity extends BaseActivity {
    private int typePaidSelected;
    private Context mContext;
    private PaymentInformationPOST paymentInformationPOST;
    private RegistrationDetailModel mRegister;
    private StartPaymentPOST startPaymentPOST;
    private PaymentInformationResult payInfoPublic = null;
    private TextView tvRegCodePayContract, tvCode, tvCusName, tvPhone, tvTotal, tvStatus;
    private Spinner spPayType;
    private Button btnPay, btnGoToAppointmentScreen;
    private LinearLayout loSignature;

    public PayContractActivity() {
        super(R.string.pay_contract_header_activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.pay_contract_header_activity));
        setContentView(R.layout.activity_pay_contract);
        this.mContext = this;
        createWidgets();
        getPaymentInfo();
        createEvents();
    }

    private void createWidgets() {
        tvRegCodePayContract = (TextView) findViewById(R.id.tv_reg_code_pay_contract);
        tvCode = (TextView) findViewById(R.id.tv_code_pay_contract);
        tvCusName = (TextView) findViewById(R.id.tv_cus_name_pay_contract);
        tvPhone = (TextView) findViewById(R.id.tv_phone_pay_contract);
        tvTotal = (TextView) findViewById(R.id.tv_total_pay_contract);
        tvStatus = (TextView) findViewById(R.id.tv_status_pay_contract);
        btnPay = (Button) findViewById(R.id.btn_pay_contract);
        spPayType = (Spinner) findViewById(R.id.sp_pay_contract);
        btnGoToAppointmentScreen = (Button) findViewById(R.id.btn_tick_deploy);
        loSignature = (LinearLayout) findViewById(R.id.frm_container_customer_signature);
        loSignature.setVisibility(View.GONE);
    }

    private void createEvents() {
        //sự kiện nhấn nút thanh toán,tạo khoản thu, cập nhật khoản thu
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnPay.getText().equals(getResources().getString(R.string.title_payment))) {
                    //kết nối api thanh toán
                    typePaidSelected = spPayType.getAdapter() != null ? ((PaidType) spPayType.getSelectedItem()).getKey() : 0;
                    String paidTypeValue = spPayType.getAdapter() != null ? ((PaidType) spPayType.getSelectedItem()).getValue() : "";
                    startPaymentPOST.setPaidType(typePaidSelected);
                    new StartPayment(mContext, startPaymentPOST, paidTypeValue, payInfoPublic, mRegister);
                } else if (btnPay.getText().equals(getResources().getString(R.string.title_update_payment))) {
                    if (paymentInformationPOST != null) {
                        //kết nối api cập nhật khoản thu
                        if (payInfoPublic != null) {
                            new UpdatePayment(mContext, paymentInformationPOST, payInfoPublic, mRegister);
                        } else {
                            Common.showToast(mContext, mContext.getResources().getString(R.string.info_detail_registration_not_available));
                        }
                    }
                }
            }
        });

        btnGoToAppointmentScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DeployAppointmentActivity.class);
                intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                //Finish payment contract activity
                PayContractActivity.this.finish();
            }
        });
    }

    /**
     * khởi tạo quá trình lấy thông tin ban đầu
     */
    private void getPaymentInfo() {
        //POST object
        //Declares variables
        CreateObject_2018POST createObject_2018POST = getIntent().getParcelableExtra("CREATE_OBJECT_2018_POST");
        mRegister = getIntent().getParcelableExtra(Constants.MODEL_REGISTER);
        paymentInformationPOST = new PaymentInformationPOST();
        startPaymentPOST = new StartPaymentPOST();
        if (createObject_2018POST != null || mRegister != null) {
            if (createObject_2018POST != null) {
                paymentInformationPOST.setSaleName(createObject_2018POST.getUserName());
                paymentInformationPOST.setRegCode(createObject_2018POST.getRegCode());
                startPaymentPOST.setSaleMan(createObject_2018POST.getUserName());
                if (!TextUtils.isEmpty(createObject_2018POST.getImageSignature())) {
                    loSignature.setVisibility(View.GONE);
                }
            }
            if (mRegister != null) {
                paymentInformationPOST.setSaleName(mRegister.getUserName());
                paymentInformationPOST.setRegCode(mRegister.getRegCode());
                startPaymentPOST.setSaleMan(mRegister.getUserName());
                if (!TextUtils.isEmpty(mRegister.getImageSignature())) {
                    loSignature.setVisibility(View.GONE);
                }
                if(mRegister.getCheckListService().isOTT() || mRegister.getCheckListService().isExtraOTT()){
                    btnGoToAppointmentScreen.setVisibility(View.GONE);
                }
            }
            //kết nối api danh sách thanh toán
            new GetListPaidType(this.mContext, this, paymentInformationPOST);
        } else {
            Common.showToast(mContext, "Thông tin chi tiết phiếu đăng ký truyền đi thất bại!");
        }
    }

    // load danh sách thanh toán từ api
    public void loadPaidTypeList(ArrayList<PaidType> paidTypeList) {
        if (paidTypeList.size() > 0) {
            PaidTypeAdapter paidTypeAdapter = new PaidTypeAdapter(mContext, android.R.layout.simple_spinner_dropdown_item, paidTypeList);
            paidTypeAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
            spPayType.setAdapter(paidTypeAdapter);
        } else {
            Common.alertDialog("Không có dữ liệu danh sách thanh toán", mContext);
        }
    }

    /**
     * hiển thị thông tin thanh toán của khách hàng
     */
    public void showPaymentInformation(PaymentInformationResult objResult) {
        if(objResult == null){
            return;
        }
        payInfoPublic = objResult;
        tvRegCodePayContract.setText(objResult.getRegCode());
        tvCode.setText(objResult.getContract());
        tvCusName.setText(objResult.getFullName());
        tvPhone.setText(objResult.getPhoneNumber());
        tvTotal.setText(Common.formatNumber(objResult.getTotalUnpaidReceipts()));
        tvStatus.setText(objResult.getPaymentMessage());
        startPaymentPOST.setRegCode(objResult.getRegCode());
        startPaymentPOST.setContract(objResult.getContract());
        startPaymentPOST.setTotal(objResult.getTotalUnpaidReceipts());

        if (objResult.getPaymentStatus() == 1 || objResult.getPaymentStatus() == 0) {
            tvTotal.setText(Common.formatNumber(objResult.getTotalReceipts()));
            btnPay.setVisibility(View.GONE);
            spPayType.setSelection(Common.getIndex(spPayType, objResult.getPaidType()), true);
        } else if (objResult.getPaymentStatus() == 2) {
            btnPay.setText(getResources().getString(R.string.title_payment));
            spPayType.setSelection(0, true);
        } else if (objResult.getPaymentStatus() == 3) {
            btnPay.setText(getResources().getString(R.string.title_update_payment));
            spPayType.setSelection(Common.getIndex(spPayType, objResult.getPaidType()), true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
        //kết nối api lấy thông tin thanh toán 2018
        new PaymentInformation(this.mContext, paymentInformationPOST);
    }

    @Override
    public void onBackPressed() {
        if (payInfoPublic != null) {
            new GetRegistrationDetail(mContext, payInfoPublic.getCreateBy(), payInfoPublic.getRegCode(), true);
        } else {
            Common.showToast(mContext, "Thông tin khách hàng không tồn tại!");
            finish();
        }
    }
}
