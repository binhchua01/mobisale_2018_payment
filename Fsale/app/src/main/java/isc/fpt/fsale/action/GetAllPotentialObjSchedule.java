package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PotentialObjScheduleList;
import isc.fpt.fsale.model.PotentialSchedule;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.AlarmReceiver;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 10/11/2017.
 */

public class GetAllPotentialObjSchedule implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private boolean createLocalSchedule;
    private ArrayList<PotentialSchedule> lst;

    public  GetAllPotentialObjSchedule(Context mContext, String supporter, String fromDate,
                                       String toDate, String potentialObjID, String pageNumber, boolean createLocalSchedule) {
        this.mContext = mContext;
        this.createLocalSchedule = createLocalSchedule;
        String[] arrParamName = new String[]{"Supporter", "FromDate", "ToDate", "PotentialObjID", "PageNumber"};
        if(fromDate != null  && toDate != null){
            if(fromDate.length()>0 && toDate.length()>0){
                String timeDefaultStart = "00:00:00";
                fromDate+=" "+ timeDefaultStart;
                String timeDefaultEnd = "23:59:59";
                toDate+= " "+ timeDefaultEnd;
            }
        }
        String[] arrParamValue = new String[]{supporter, fromDate, toDate, potentialObjID, pageNumber};
        String message = mContext.getResources().getString(R.string.msg_pd_get_list_potential_schedule);
        String UPDATE_POTENTIAL_OBJ_SCHEDULE = "GetAllPotentialObjSchedule";
        CallServiceTask service = new CallServiceTask(mContext, UPDATE_POTENTIAL_OBJ_SCHEDULE,
                arrParamName, arrParamValue, Services.JSON_POST, message, GetAllPotentialObjSchedule.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        boolean isError = false;
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<PotentialSchedule> resultObject = new WSObjectsModel<>(jsObj, PotentialSchedule.class);
                if (resultObject.getErrorCode() == 0) {
                    lst = resultObject.getArrayListObject();
                } else {
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (createLocalSchedule) {
            if(lst != null) {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                AlarmManager alarmManager = ((MyApp) mContext.getApplicationContext()).getAlarmManager();
                try {
                    Calendar calendar = Calendar.getInstance();
                    for (PotentialSchedule e : lst) {
                        Date sDate = simpleDate.parse(e.getScheduleDate());
                        calendar.setTime(sDate);
                        Intent myIntent = new Intent(mContext, AlarmReceiver.class);
                        myIntent.putExtra("potentialFullName", e.getFullName());
                        myIntent.putExtra("scheduleDescription", e.getScheduleDescription());
                        myIntent.putExtra("potentialObjScheduleID", e.getPotentialObjScheduleID());
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, e.getPotentialObjScheduleID(), myIntent, 0);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                        } else {
                            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }else{
            if (!isError && lst != null) {
                if(mContext != null && mContext.getClass().getSimpleName().equals(PotentialObjScheduleList.class.getSimpleName())){
                    PotentialObjScheduleList potentialObjScheduleList = (PotentialObjScheduleList) mContext;
                    potentialObjScheduleList.loadPotentialScheduleList(lst);
                }
            }
        }
    }
}