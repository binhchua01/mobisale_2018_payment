package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionBrochureModel;
import isc.fpt.fsale.utils.Constants;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;


public class PromotionAdVideoViewActivity extends Activity {
    public static final String ARG_BUNDLE = "ARG_BUNDLE";
    public static final String ARG_TOP = "TAG_TOP";
    public static final String ARG_LEFT = "TAG_LEFT";
    public static final String ARG_WIDTH = "TAG_WIDTH";
    public static final String ARG_HEIGHT = "TAG_HEIGHT";
    public static final String ARG_PROMOTION_BROCHURE_OBJECT = "TAG_PROMOTION_OBJECT";
    public static final String ARG_SELECTED_POSITION = "ARG_SELECTED_POSITION";
    // Declare variables
    private ProgressDialog pDialog;
    private VideoView videoview;
    private PromotionBrochureModel mPromotionObj;
    // Insert your Video URL
    //String VideoURL = "https://lh3.googleusercontent.com/W7ZxvuFGwf6G3o8a3MnuSw7R8wIk6aLTjFlD4h6yM0d6=m22";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_promotion_video);
        videoview = (VideoView) findViewById(R.id.video_View);
		/*frmMain = (RelativeLayout)findViewById(R.id.frm_main);
		frmMain.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(videoview != null){
					videoview.showContextMenu();
				}
			}
		});*/
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Video khuyến mãi");
        pDialog.setMessage("Buffering...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            mPromotionObj = intent.getBundleExtra(ARG_BUNDLE).getParcelable(ARG_PROMOTION_BROCHURE_OBJECT);
            if (mPromotionObj != null) {
                pDialog.show();
                play(mPromotionObj.getMediaUrl());
            }
        }
    }

    private void play(String url) {
        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(this);
            mediacontroller.setAnchorView(videoview);
            // Get the URL from String VideoURL
            Uri video = Uri.parse(mPromotionObj.getMediaUrl());
            videoview.setMediaController(mediacontroller);
            videoview.setVideoURI(video);
        } catch (Exception e) {

            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoview.requestFocus();
        videoview.setOnPreparedListener(new OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                pDialog.dismiss();
                videoview.seekTo(1000);
                videoview.start();

            }
        });

    }

}
