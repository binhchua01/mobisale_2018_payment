package isc.fpt.fsale.ui.fpt_camera.device_list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListEquipment;
import isc.fpt.fsale.action.GetListEquipmentCamera;
import isc.fpt.fsale.activity.device_list.DeviceListAdapter;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.utils.Constants;

public class DeviceListCameraActivity extends BaseActivitySecond implements OnItemClickListener<Device> {
    private View loBack;
    private List<Device> mList;
    private List<Device> mListDeviceSelected;
    private DeviceListAdapter mAdapter;
    private String mClassName;

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_device_list;
    }

    @Override
    protected void initView() {
        getDataIntent();

        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText(getResources().getString(R.string.lbl_screen_name_device_list));
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_device_list);
        mAdapter = new DeviceListAdapter(this, mList != null ? mList : new ArrayList<Device>(), this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelableArrayList(Constants.DEVICE_LIST_SELECTED) != null) {
            mListDeviceSelected = bundle.getParcelableArrayList(Constants.DEVICE_LIST_SELECTED);
        }
        if (bundle != null && bundle.getString(Constants.CLASS_NAME) != null) {
            mClassName = bundle.getString(Constants.CLASS_NAME);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!TextUtils.isEmpty(mClassName)){
            new GetListEquipmentCamera(this, mClassName);
        }
    }

    public void loadDeviceList(List<Device> mList){
        for(Device item : mList){
            for(Device itemSelected : mListDeviceSelected){
                if(itemSelected.getDeviceID() == item.getDeviceID()){
                    item.setSelected(true);
                }
            }
        }

        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(Device mDevice) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.DEVICE, mDevice);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
