package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.MapBookPortAuto;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.Util;

// API Bookport Auto
public class AutoBookPort implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private int errorCode;
    private String ODCCable;
    private String latLng;
    private boolean isManual;
    private MapBookPortAuto mMapBookPortAuto;

    public AutoBookPort(Context mContext, String UserName, String RegCode,
                        int Type, String LatLng, String LatLngDevice) {
        this.mContext = mContext;
        if (mContext.getClass().getSimpleName().equals(MapBookPortAuto.class.getSimpleName())) {
            mMapBookPortAuto = (MapBookPortAuto) mContext;
        }
        String[] arrParamName = new String[]{"UserName", "RegCode", "Type", "Latlng", "LatlngDevice"};
        String[] arrParamValue = new String[]{UserName, RegCode, String.valueOf(Type), LatLng, LatLngDevice};
        String message = mContext.getResources().getString(R.string.msg_process_book_port_auto);
        String GET_AUTO_BOOK_PORT = "AutoBookPort";
        CallServiceTask service = new CallServiceTask(mContext,
                GET_AUTO_BOOK_PORT, arrParamName, arrParamValue,
                Services.JSON_POST, message, AutoBookPort.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        JSONObject jsObj = JSONParsing.getJsonObj(result);
        if (jsObj != null) {
            try {
                String RESPONSE_RESULT = "AutoBookPortResult";
                jsObj = jsObj.getJSONObject(RESPONSE_RESULT);
                String TAG_ERROR_CODE = "ErrorCode";
                if (jsObj.has(TAG_ERROR_CODE)) {
                    errorCode = jsObj.getInt(TAG_ERROR_CODE);
                }
                String TAG_IS_MANUAL = "isManual";
                if (jsObj.has(TAG_IS_MANUAL)) {
                    isManual = jsObj.getBoolean(TAG_IS_MANUAL);
                }
                if (errorCode == 0 || errorCode == 1) {
                    String TAG_RESULT = "Result";
                    if (jsObj.has(TAG_RESULT)) {
                        JSONObject jsObjResult = jsObj.getString(TAG_RESULT) != null ? jsObj.getJSONObject(TAG_RESULT) : null;
                        if (jsObjResult != null) {
                            mMapBookPortAuto.setNumberRetryConnect(0);
                            String TAG_ODC_CABLE = "ODCCable";
                            if (jsObjResult.has(TAG_ODC_CABLE)) {
                                ODCCable = jsObjResult.getString(TAG_ODC_CABLE);
                            }
                            String TAG_LAT_LNG = "latLng";
                            if (jsObjResult.has(TAG_LAT_LNG)) {
                                latLng = jsObjResult.getString(TAG_LAT_LNG);
                            }
                        }
                    }
                    if (latLng != null && !latLng.equals("") && ODCCable != null && !ODCCable.equals("")) {
                        String message = errorCode == 0 ?
                                mContext.getResources().getString(R.string.msg_message_bookport_success) :
                                mContext.getResources().getString(R.string.msg_message_recoverty_port);
                        latLng = Util.replaceCharacter(latLng);
                        mMapBookPortAuto.drawMakerBookPortSuccess(latLng, message, ODCCable);
                    } else {
                        Common.alertDialog(mContext.getResources().getString(R.string.title_message_box_book_port_time_out), mContext);
                    }
                } else {
                    if (mMapBookPortAuto != null) {
                        if (isManual) {
                            mMapBookPortAuto.showMessageAutoBookPortManual(mContext,
                                    mContext.getResources().getString(R.string.title_message_box_book_port_time_out_last));
                        } else {
                            if (mMapBookPortAuto.getNumberRetryConnect() == Constants.AutoBookPort_RetryConnect) {
                                mMapBookPortAuto.showMessageAutoBookPortManual(mContext,
                                        mContext.getResources().getString(R.string.title_message_box_book_port_time_out_last));
                            } else {
                                Common.alertDialog(mContext.getResources().getString(R.string.title_message_box_book_port_time_out), mContext);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                //  e.getStackTrace();
                if (mMapBookPortAuto.getNumberRetryConnect() == Constants.AutoBookPort_RetryConnect) {
                    mMapBookPortAuto.showMessageAutoBookPortManual(mContext,
                            mContext.getResources().getString(R.string.title_message_box_book_port_time_out_last));
                } else {
                    Common.alertDialog(mContext.getResources().getString(R.string.title_message_box_book_port_time_out), mContext);
                }

            }
        } else {
            if (mMapBookPortAuto.getNumberRetryConnect() == Constants.AutoBookPort_RetryConnect) {
                mMapBookPortAuto.showMessageAutoBookPortManual(mContext,
                        mContext.getResources().getString(R.string.title_message_box_book_port_time_out_last));
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.title_message_box_book_port_time_out), mContext);
            }
        }
    }
}