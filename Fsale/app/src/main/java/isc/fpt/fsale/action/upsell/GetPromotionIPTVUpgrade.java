package isc.fpt.fsale.action.upsell;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.callback.upsell.PromotionIPTVUpgradeCallBack;
import isc.fpt.fsale.callback.upsell.PromotionNetUpgradeCallBack;
import isc.fpt.fsale.model.upsell.response.GetPromotionIPTVUpgradeModel;
import isc.fpt.fsale.model.upsell.response.GetPromotionNetUpgradeModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetPromotionIPTVUpgrade implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private PromotionIPTVUpgradeCallBack upgradeCallBack;

    public GetPromotionIPTVUpgrade(Context mContext, String[] enable, PromotionIPTVUpgradeCallBack callBack) {
        this.mContext = mContext;
        this.upgradeCallBack = callBack;
        String[] params = new String[]{"ProgramUpgradeID", "ObjID"};
        String message = "Vui lòng chờ...";
        String GET_PROMOTION_IPTV = "GetPromotionIPTV_Upgrade";
        CallServiceTask service = new CallServiceTask(mContext, GET_PROMOTION_IPTV, params, enable,
                Services.JSON_POST, message, GetPromotionIPTVUpgrade.this);
        service.execute();
    }


    @Override
    public void onTaskComplete(String result) {
        try {
            List<GetPromotionIPTVUpgradeModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), GetPromotionIPTVUpgradeModel.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        upgradeCallBack.onGetSuccess(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
