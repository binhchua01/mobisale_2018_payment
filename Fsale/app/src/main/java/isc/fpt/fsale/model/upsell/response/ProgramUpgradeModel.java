package isc.fpt.fsale.model.upsell.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProgramUpgradeModel implements Parcelable {

    @SerializedName("ID")
    @Expose
    private Integer ID;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("FromDate")
    @Expose
    private String FromDate;
    @SerializedName("ErrorService")
    @Expose
    private String ErrorService;
    @SerializedName("ToDate")
    @Expose
    private String ToDate;
    @SerializedName("Enable")
    @Expose
    private Integer Enable;

    public ProgramUpgradeModel(Integer ID, String name, String description, String fromDate, String errorService, String toDate, Integer enable) {
        this.ID = ID;
        Name = name;
        Description = description;
        FromDate = fromDate;
        ErrorService = errorService;
        ToDate = toDate;
        Enable = enable;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String fromDate) {
        FromDate = fromDate;
    }

    public String getErrorService() {
        return ErrorService;
    }

    public void setErrorService(String errorService) {
        ErrorService = errorService;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String toDate) {
        ToDate = toDate;
    }

    public Integer getEnable() {
        return Enable;
    }

    public void setEnable(Integer enable) {
        Enable = enable;
    }

    protected ProgramUpgradeModel(Parcel in) {
        if (in.readByte() == 0) {
            ID = null;
        } else {
            ID = in.readInt();
        }
        Name = in.readString();
        Description = in.readString();
        FromDate = in.readString();
        ErrorService = in.readString();
        ToDate = in.readString();
        if (in.readByte() == 0) {
            Enable = null;
        } else {
            Enable = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (ID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(ID);
        }
        dest.writeString(Name);
        dest.writeString(Description);
        dest.writeString(FromDate);
        dest.writeString(ErrorService);
        dest.writeString(ToDate);
        if (Enable == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Enable);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProgramUpgradeModel> CREATOR = new Creator<ProgramUpgradeModel>() {
        @Override
        public ProgramUpgradeModel createFromParcel(Parcel in) {
            return new ProgramUpgradeModel(in);
        }

        @Override
        public ProgramUpgradeModel[] newArray(int size) {
            return new ProgramUpgradeModel[size];
        }
    };
}
