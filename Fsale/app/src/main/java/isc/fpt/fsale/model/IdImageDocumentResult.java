package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 10/23/2018.
 */

public class IdImageDocumentResult implements Parcelable {
    private int Id, LinkId, Source, Type;
    private String Path, Date;

    public IdImageDocumentResult() {

    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getLinkId() {
        return LinkId;
    }

    public void setLinkId(int linkId) {
        LinkId = linkId;
    }

    public int getSource() {
        return Source;
    }

    public void setSource(int source) {
        Source = source;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    protected IdImageDocumentResult(Parcel in) {
        Id = in.readInt();
        LinkId = in.readInt();
        Source = in.readInt();
        Type = in.readInt();
        Path = in.readString();
        Date = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeInt(LinkId);
        dest.writeInt(Source);
        dest.writeInt(Type);
        dest.writeString(Path);
        dest.writeString(Date);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IdImageDocumentResult> CREATOR = new Creator<IdImageDocumentResult>() {
        @Override
        public IdImageDocumentResult createFromParcel(Parcel in) {
            return new IdImageDocumentResult(in);
        }

        @Override
        public IdImageDocumentResult[] newArray(int size) {
            return new IdImageDocumentResult[size];
        }
    };
}
