package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;

public class UpdateIPTVAction implements AsyncTaskCompleteListener<String>{ 

	private final String TAG_UPDATEUSEDHDBOX = "UpdateUsedHDBox";
	private final String TAG_UPDATEUSEDHDBOX_RESULT = "UpdateUsedHDBoxResult";
	private final String TAG_ERRORSERVICE = "ErrorService";
	private final String TAG_RESULT = "Result";
	private final String TAG_RESULTID = "ResultID";
	private DialogFragment dialogfram;
	private String[] arrParamName, arrParamValue;
	
	private Context mContext;
	
	public UpdateIPTVAction(Context mContext,String[] paramsValue, DialogFragment dialog) 
	{
		this.mContext = mContext;
		this.dialogfram = dialog;
		Insert(paramsValue);
	}
	
	public void Insert(String[] paramsValue)
	{		
		/*
		 "iObjID","iSupportID","iPortalID","iOneTVPackageExtraList","sLogOnUser", "iStatus", "iNotTrialReason", "sComment", "sNotTrialNote"
		*/
		arrParamName = new String[]{"iObjID", "iSupportID", "iPortalID", "iOneTVPackageExtraList", "sLogOnUser", "iStatus", "iNotTrialReason","sComment","sNotTrialNote"};
		this.arrParamValue = paramsValue;
		String message = "Vui lòng đợi giây lát";
		CallServiceTask service = new CallServiceTask(mContext,TAG_UPDATEUSEDHDBOX,arrParamName,arrParamValue, Services.JSON_POST, message, UpdateIPTVAction.this);
		service.execute();
	}
	
	public void handleUpdateRegistration(String json){		
		if(json != null && Common.jsonObjectValidate(json)){			
			 try {	
				   JSONArray jsArr;
				    JSONObject jsObj = JSONParsing.getJsonObj(json);
				    jsArr = jsObj.getJSONArray(TAG_UPDATEUSEDHDBOX_RESULT);
				    int l = jsArr.length();
				    if (l > 0) 
				    {
					     String error = jsArr.getJSONObject(0).getString(TAG_ERRORSERVICE);
					     if (error.equals("null")) 
					     {
					    	 String result = jsArr.getJSONObject(0).getString(TAG_RESULT);
						      int ResultID=jsArr.getJSONObject(0).getInt(TAG_RESULTID);
						      if(ResultID==1)
						      {
						    	   new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(result)
					 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
					 				    @Override
					 				    public void onClick(DialogInterface dialog, int which) {
					 				        dialog.cancel();
					 				        dialogfram.dismiss();
					 				       new GetListObjectHDBoxAction(mContext, "", "1","1");
					 				    }
					 				})
				 					.setCancelable(false).create().show();
						    	 
						      }
						      else
						      {
						    	 Common.alertDialog(result, mContext);
						      }
					     }
					     else  Common.alertDialog(error, mContext);
					    	 
				    }
			 }
			 catch (Exception e)
			 {

				e.printStackTrace();
				Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
			 }
		}		
		else Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);		
	}
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);
	}

}
