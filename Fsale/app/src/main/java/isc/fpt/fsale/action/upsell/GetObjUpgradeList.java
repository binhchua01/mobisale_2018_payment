package isc.fpt.fsale.action.upsell;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.callback.upsell.OnGetObjUpdgrade;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeListModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetObjUpgradeList implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private OnGetObjUpdgrade mCallback;

    public GetObjUpgradeList(Context mContext, String[] arrParams, OnGetObjUpdgrade mCallback) {
        this.mCallback = mCallback;
        this.mContext = mContext;
        String[] params = new String[]{"UserName", "ProgramUpgradeID", "Agent", "AgentName", "PageSize", "PageNum"};
        String message = "Xin vui long cho giay lat...";
        String GET_OBJ_UPGRADE_LIST = "GetObjUpgradeList";
        CallServiceTask service = new CallServiceTask(mContext, GET_OBJ_UPGRADE_LIST, params, arrParams,
                Services.JSON_POST, message, GetObjUpgradeList.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<ObjUpgradeListModel> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ObjUpgradeListModel.class));
                }
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (mContext != null) {
                        mCallback.onGetListSuccess(lst);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
