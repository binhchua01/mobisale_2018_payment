package isc.fpt.fsale.adapter;

import isc.fpt.fsale.model.ReportPayTVDetailModel;
import java.util.ArrayList;
import isc.fpt.fsale.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReportPayTVDetailAdapter extends BaseAdapter {

	private ArrayList<ReportPayTVDetailModel> mList;	
	private Context mContext;
	
	public ReportPayTVDetailAdapter(Context context, ArrayList<ReportPayTVDetailModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportPayTVDetailModel item = mList.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_pay_tv_detail, null);
		}
		if(item != null){
			TextView lblContract = (TextView)convertView.findViewById(R.id.lbl_contract);
			TextView lblPackage = (TextView)convertView.findViewById(R.id.lbl_package);
			TextView lblSaleBoxDate = (TextView)convertView.findViewById(R.id.lbl_sale_box_date);
			TextView lblActiveBoxDate = (TextView)convertView.findViewById(R.id.lbl_active_box_date);
			TextView lblType = (TextView)convertView.findViewById(R.id.lbl_type);
			TextView lblRowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			TextView lblPhoneNumber = (TextView)convertView.findViewById(R.id.lbl_customize_phone);
			if(item != null){
				lblContract.setText(item.getContract());
				lblPackage.setText(item.getPackage());
				lblSaleBoxDate.setText(item.getNgayBanBox());
				lblActiveBoxDate.setText(item.getNgayActiveBox());
				lblType.setText(String.valueOf(item.getLoai()));
				lblRowNumber.setText(String.valueOf(item.getRowNumber()));
				lblPhoneNumber.setText(item.getPhoneNumber());
			}
		}
		
		return convertView;
	}


}
