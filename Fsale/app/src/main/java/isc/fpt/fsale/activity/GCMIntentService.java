package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CustomerCareListModel;
import isc.fpt.fsale.model.NotificationIDUserModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;


public class GCMIntentService extends GCMBaseIntentService {
    private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super(Constants.GOOGLE_SENDER_ID);
    }

    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
    }

    /**
     * Method called on device unregistred
     */
    @Override
    protected void onUnregistered(Context context, String registrationId) {

    }

    /**
     * Method called on Receiving a new message from GCM server
     */
    @Override
    protected void onMessage(Context context, Intent intent) {
        try {
            //Tuấn cập nhật code 16032018
            String title = "Title", message = "Message";
            if (intent.getExtras().getString("title") != null &&
                    !intent.getExtras().getString("title").trim().equals("")) {
                // xử lý lấy tiêu đề notification
                title = intent.getExtras().getString("title");
            }
            if (intent.getExtras().getString("message") != null &&
                    !intent.getExtras().getString("message").trim().equals("")) {
                // xử lý lấy nội dung notification
                message = intent.getExtras().getString("message");
            }
            Intent notificationIntent = null;
            if (intent.getExtras().getString("modelCategory").equals("PTC_Return")) {
                //xử lý hiển thị notification phiếu thi công trả về
                notificationIntent = new Intent(context, ListPTCReturnActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                showNotification(context, title, message, pendingIntent);
            } else if (intent.getExtras().getString("modelCategory").equals("KHTN")) {
                //xử lý hiển thị notification mã code khách hàng tiềm năng
                if (intent.getExtras().getSerializable("model") != null) {
                    JSONObject object = new JSONObject(intent.getExtras().getSerializable("model").toString());
                    if (object.has("PotentialObjID") && object.getInt("PotentialObjID") > 0 || object.has("Code") && !(object.getString("Code").equals(""))) {
                        PotentialObjModel potential = getPotential(object);
                        String code = object.getString("Code");
                        generateNotification(context, potential, code, title, message);
                    }
                }
            } else if (intent.getExtras().getString("modelCategory").equals("Payment")) {
                //xử lý hiển thị notification sau khi thanh toán thành công TPBANK và QRCode
                if (intent.getExtras().getSerializable("model") != null) {
                    JSONObject object = new JSONObject(intent.getExtras().getSerializable("model").toString());
                    notificationIntent = new Intent(context, ListNotificationActivity.class);
                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    showNotification(context, title, message, pendingIntent);
                    String regCode = object.has("RegCode") ? object.getString("RegCode") : "";
                    navigateToConfirmPaymentActivity(context, regCode);
                }
            } else if (intent.getExtras().getString("modelCategory").equals("PayTVSurvey")) {
                //xử lý hiển thị notification phần chăm sóc khách hàng PAYTV
                generateNotificationCareCustomerFptPlay(context, title, message);
            } else {
                //xử lý hiển thị notification các trường hợp khác
                notificationIntent = new Intent(context, ListNotificationActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(context,
                        0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                showNotification(context, title, message, pendingIntent);
            }
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // tạo notification chung cho cả ứng dụng
    private void showNotification(Context context, String title, String message, PendingIntent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setAutoCancel(true);
        builder.setOngoing(true);
        builder.setContentTitle(title);
        builder.setContentText(message);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentIntent(intent);
        builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        builder.setNumber(100);
        builder.build();
        int id = (int) System.currentTimeMillis();
        notificationManager.notify(id, builder.getNotification());
    }

    private static PotentialObjModel getPotential(JSONObject object) {
        PotentialObjModel potential = new PotentialObjModel();
        try {
            potential.setID(Integer.valueOf(object.getInt("PotentialObjID")));
            potential.setFullName(object.getString("FullName"));
            potential.setPhone1(object.getString("PhoneNumber"));
            potential.setEmail(object.getString("Email"));
            potential.setAddress(object.getString("Address"));
            potential.setCaseID(object.getString("CaseID"));
            potential.setNote(object.getString("Description"));
            potential.setSource(1);
            potential.setContract("Contract");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return potential;
    }

    /**
     * Method called on receiving a deleted message
     */
    @Override
    protected void onDeletedMessages(Context context, int total) {
    }

    /**
     * Method called on Error
     */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        return super.onRecoverableError(context, errorId);
    }

    static void navigateToConfirmPaymentActivity(Context context, String regCode) {
        Intent intent = new Intent("" + "NAVIGATE_CONFIRM_PAYMENT");
        intent.putExtra("RegCode", regCode);
        context.sendBroadcast(intent);
    }

    @SuppressLint("DefaultLocale")
    private static int getNotificationID(Context context, String sendFrom) {
        NotificationIDUserModel newNtf = new NotificationIDUserModel(0, sendFrom);
        try {
            boolean hasUser = false;
            List<NotificationIDUserModel> lst = ((MyApp) context.getApplicationContext()).getNotificationUserList();
            if (lst != null && lst.size() > 0) {
                NotificationIDUserModel item = null;
                for (int i = 0; i < lst.size(); i++) {
                    item = lst.get(i);
                    if (item.getUserName().toUpperCase().equals(newNtf.getUserName().toUpperCase()) == true) {
                        newNtf = item;
                        hasUser = true;
                        break;
                    }
                }
                if (!hasUser) {
                    newNtf.setNoticationID(lst.size());
                    ((MyApp) context.getApplicationContext()).getNotificationUserList().add(newNtf);
                }
            } else {
                ((MyApp) context.getApplicationContext()).addNotificationUserList(newNtf);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return newNtf.getNoticationID();
    }

    @SuppressLint("DefaultLocale")
    private static int getNotificationID(Context context, CustomerCareListModel customer) {
        NotificationIDUserModel newNtf = new NotificationIDUserModel(0, "", customer.getContract());
        try {
            boolean hasUser = false;
            List<NotificationIDUserModel> lst = ((MyApp) context.getApplicationContext()).getNotificationUserList();
            if (lst != null && lst.size() > 0) {
                NotificationIDUserModel item = null;
                for (int i = 0; i < lst.size(); i++) {
                    item = lst.get(i);
                    if (item.getContract().toUpperCase().equals(newNtf.getContract().toUpperCase()) == true) {
                        newNtf = item;
                        hasUser = true;
                        break;
                    }
                }
                if (!hasUser) {
                    newNtf.setNoticationID(lst.size());
                    ((MyApp) context.getApplicationContext()).getNotificationUserList().add(newNtf);
                }
            } else {
                ((MyApp) context.getApplicationContext()).addNotificationUserList(newNtf);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return newNtf.getNoticationID();
    }

    //notification khách hàng tiềm năng
    @SuppressWarnings("deprecation")
    private static void generateNotification(Context context, PotentialObjModel potential, String code, String title, String message) {
        try {
            int notificationID = potential.getID();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent = new Intent(context, GetByCodeActivity.class);
            notificationIntent.putExtra("PotentialObjID", String.valueOf(potential.getID()));
            notificationIntent.putExtra("Code", code);
            PendingIntent intent = PendingIntent.getActivity(context, notificationID, notificationIntent, PendingIntent.FLAG_ONE_SHOT);
            if (Build.VERSION.SDK_INT < 16) {
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher_notification)
                        .setContentTitle(title)
                        .setContentText(message);
                mBuilder.setContentIntent(intent);
                mBuilder.setAutoCancel(true);
                mBuilder.setOngoing(true);
                mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
                notificationManager.notify(notificationID, mBuilder.build());
            } else {
                Notification.Builder builder = new Notification.Builder(context);
                builder.setAutoCancel(true);
                builder.setContentTitle(title);
                builder.setContentText(message);
                builder.setSmallIcon(R.drawable.ic_launcher);
                builder.setContentIntent(intent);
                builder.setOngoing(true);
                builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                builder.setNumber(100);
                builder.build();
                notificationManager.notify(notificationID, builder.getNotification());
            }
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //notification chăm sóc khách hàng paytv
    @SuppressWarnings("deprecation")
    private static void generateNotificationCareCustomerFptPlay(Context context, String title, String message) {
        try {
            int notificationID = (int) System.currentTimeMillis();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent = new Intent(context, CareCustomerFPTPlayActivity.class);
            notificationIntent.putExtra("LINK_CS_KH_PAY_TV", Constants.Link_CSKH_PayTv);
            PendingIntent intent = PendingIntent.getActivity(context, notificationID, notificationIntent, PendingIntent.FLAG_ONE_SHOT);
            if (Build.VERSION.SDK_INT < 16) {
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher_notification)
                        .setContentTitle(title)
                        .setContentText(message);
                mBuilder.setContentIntent(intent);
                mBuilder.setAutoCancel(true);
                mBuilder.setOngoing(true);
                mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
                notificationManager.notify(notificationID, mBuilder.build());
            } else {
                Notification.Builder builder = new Notification.Builder(context);
                builder.setAutoCancel(true);
                builder.setContentTitle(title);
                builder.setContentText(message);
                builder.setSmallIcon(R.drawable.ic_launcher);
                builder.setContentIntent(intent);
                builder.setOngoing(true);
                builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                builder.setNumber(100);
                builder.build();
                notificationManager.notify(notificationID, builder.getNotification());
            }
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}

