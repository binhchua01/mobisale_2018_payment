package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class GetPortFreeFTTHAction implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private Spinner spPortFree;
    private ArrayList<KeyValuePairModel> LIST_PORT;
    private int TypeService;
    private TextView txtMinPort;
    private Spinner spPortSwitchCard;
    private EditText txtCardNumber;

    public GetPortFreeFTTHAction(Context _mContext, String[] arrParams, Spinner sp, int _TypeService,
                                 TextView minPort, Spinner portSwitchCard, EditText txtCardNumber) {
        this.mContext = _mContext;
        this.spPortFree = sp;
        this.TypeService = _TypeService;
        this.txtMinPort = minPort;
        this.spPortSwitchCard = portSwitchCard;
        this.txtCardNumber = txtCardNumber;
        CallService(arrParams, this.TypeService);

    }

    private void CallService(String[] arrParams, int TypeService) {
        String message = "Xin cho trong giay lat";
        String[] params = new String[]{"TDID"};
        if (TypeService == 2) {
            String GET_PORT_FREE_FTTH_1Core = "GetPortFreeSingleCoreFTTH";
            CallServiceTask service = new CallServiceTask(mContext, GET_PORT_FREE_FTTH_1Core, params,
                    arrParams, Services.JSON_POST, message, GetPortFreeFTTHAction.this);
            service.execute();
        } else {
            String GET_PORT_FREE_FTTH_2Core = "GetPortFreeFTTH";
            CallServiceTask service = new CallServiceTask(mContext, GET_PORT_FREE_FTTH_2Core, params,
                    arrParams, Services.JSON_POST, message, GetPortFreeFTTHAction.this);
            service.execute();
        }
    }

    public void handleGetDistrictsResult(String json) {
        if (Common.jsonObjectValidate(json)) {
            LIST_PORT = new ArrayList<>();
            LIST_PORT.add(new KeyValuePairModel(-1, "[ Vui lòng chọn port ]"));
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if (Common.isEmptyJSONObject(jsObj)) {
                KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LIST_PORT, Gravity.LEFT);
                spPortFree.setAdapter(adapter);
                return;
            }
            bindData(jsObj);
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LIST_PORT, Gravity.LEFT);
            spPortFree.setAdapter(adapter);
            try {
                if (txtCardNumber != null && LIST_PORT != null && LIST_PORT.size() > 0) {
                    String[] splip = LIST_PORT.get(0).getDescription().split("-");
                    txtCardNumber.setText(splip[0]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {
            if (this.TypeService == 2) {
                String TAG_GET_PORT_FREE_RESULT_FTTH_1Core = "GetPortFreeSingleCoreFTTHMethodPostResult";
                jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_RESULT_FTTH_1Core);
            } else {
                String TAG_GET_PORT_FREE_RESULT_FTTH_2Core = "GetPortFreeFTTHMethodPostResult";
                jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_RESULT_FTTH_2Core);
            }
            String error = jsArr.getJSONObject(0).getString("ErrorService");
            if (error.equals("null")) {
                int l = jsArr.length();
                int iMinPort;
                try {
                    iMinPort = spPortSwitchCard.getCount();
                } catch (Exception ex) {
                    iMinPort = 0;
                }

                if (l < iMinPort || iMinPort == 0)
                    iMinPort = l;

                txtMinPort.setText(String.valueOf(iMinPort));

                if (l > 0) {
                    LIST_PORT = new ArrayList<>();
                    for (int i = 0; i < l; i++) {
                        JSONObject iObj = jsArr.getJSONObject(i);
                        LIST_PORT.add(new KeyValuePairModel(iObj.getInt("ID"), iObj.getString("PortID")));
                    }
                }
            } else {
                Common.alertDialog("Lỗi WS: " + error, mContext);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleGetDistrictsResult(result);
    }
}