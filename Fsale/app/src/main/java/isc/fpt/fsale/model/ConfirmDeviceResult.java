package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haulc3 on 22,April,2019
 */
public class ConfirmDeviceResult {
    @SerializedName("Result")
    @Expose
    private String Result;
    @SerializedName("ResultID")
    @Expose
    private Integer ResultID;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        this.Result = result;
    }

    public Integer getResultID() {
        return ResultID;
    }

    public void setResultID(Integer resultID) {
        this.ResultID = resultID;
    }
}
