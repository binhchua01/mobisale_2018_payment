package isc.fpt.fsale.model;

/**
 * Created by Hau Le on 2019-01-02.
 */
public class AllCloudPackage {
    private int Cost;
    private int PackID;
    private String PackName;
    private int ServiceType;

    public AllCloudPackage() {
    }

    public AllCloudPackage(int cost, int packID, String packName, int serviceType) {
        Cost = cost;
        PackID = packID;
        PackName = packName;
        ServiceType = serviceType;
    }

    public int getCost() {
        return Cost;
    }

    public void setCost(int cost) {
        Cost = cost;
    }

    public int getPackID() {
        return PackID;
    }

    public void setPackID(int packID) {
        PackID = packID;
    }

    public String getPackName() {
        return PackName;
    }

    public void setPackName(String packName) {
        PackName = packName;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }
}
