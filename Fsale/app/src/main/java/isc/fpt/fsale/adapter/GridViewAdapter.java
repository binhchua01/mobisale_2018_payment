package isc.fpt.fsale.adapter;

import isc.fpt.fsale.action.GetImage;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ImageObjectModel;
import isc.fpt.fsale.model.PromotionBrochureModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class GridViewAdapter extends ArrayAdapter<PromotionBrochureModel> {

    //private final ColorMatrixColorFilter grayscaleFilter;
    private Context mContext;
    private int layoutResourceId;
    private ArrayList<PromotionBrochureModel> mGridData = new ArrayList<PromotionBrochureModel>();

    public GridViewAdapter(Context mContext, int layoutResourceId, ArrayList<PromotionBrochureModel> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     *
     * @param mGridData
     */
    public void setGridData(ArrayList<PromotionBrochureModel> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       // View row = convertView;
        ViewHolder holder;       
        
        if (convertView == null) {
            /*LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);*/
        	convertView = LayoutInflater.from(mContext).inflate(layoutResourceId, null);
            holder = new ViewHolder();
            holder.titleTextView = (TextView) convertView.findViewById(R.id.grid_item_title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.grid_item_image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        PromotionBrochureModel item = mGridData.get(position);
        if(item != null){
	        holder.titleTextView.setText(Html.fromHtml(item.getTitle()));
	        if(holder.imageView != null && !holder.isLoaded){	        	
		        //Picasso.with(mContext).load(item.getImageUrl()).into(holder.imageView);
	        	if(item.getType() == PromotionBrochureModel.MEDIA_TYPE_IMAGE)
	        		new GetImageThumbnail(holder, item).execute();
	        	else if(item.getType() == PromotionBrochureModel.MEDIA_TYPE_VIDEO)
	        		Picasso.get().load(item.getThumbnail()).into(holder.imageView);
	        }
        }
        return convertView;
    }

    static class ViewHolder {
        TextView titleTextView;
        ImageView imageView;
        Boolean isLoaded = false;
    }
    
    public class GetImageThumbnail extends AsyncTask<Void, Void, Bitmap> {

    	private ViewHolder mHolder;
    	private PromotionBrochureModel mBrochure;    	

    	public GetImageThumbnail(ViewHolder holder, PromotionBrochureModel brochure) {
    	    this.mHolder= holder;
    	    this.mBrochure = brochure;
    	}

    	@Override
    	protected Bitmap doInBackground(Void... params) {
    	   String result = null;
    		try {
    			if(mBrochure != null){
    				String[] paramsValue = new String[]{mBrochure.getThumbnail()};
    			/*	if(mBrochure.getType() == PromotionBrochureModel.MEDIA_TYPE_IMAGE)
    					paramsValue = new String[]{mBrochure.getThumbnail()};
    				else if(mBrochure.getType() == PromotionBrochureModel.MEDIA_TYPE_VIDEO)
    					paramsValue = new String[]{mBrochure.getThumbnail()};*/
	    			result = Services.postJsonUpload(GetImage.GET_IMAGE_METHOD, GetImage.paramNames, paramsValue, mContext);
	    			ArrayList<ImageObjectModel> lst = null;
	    			boolean isError = false;
	    			 JSONObject jsObj = new JSONObject(result);
	    			 if(jsObj != null){
	    				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
	    				 WSObjectsModel<ImageObjectModel> resultObject = new WSObjectsModel<ImageObjectModel>(jsObj, ImageObjectModel.class);
	    				 if(resultObject != null){
	    					 if(resultObject.getErrorCode() == 0){//OK not Error
	    						lst = resultObject.getArrayListObject();								
	    					 }else{//ServiceType Error
	    						 isError = true;
	    						 Common.alertDialog( resultObject.getError(),mContext);
	    					 }
	    				 }
	    			 }
	    			 if(!isError && lst != null && lst.size() > 0){
	    				 return Common.StringToBitMap(lst.get(0).getImage());
	    			 }
    			}
    		} catch (JSONException e) {

    			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
    			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
    		} catch (Exception e) {
				// TODO Auto-generated catch block

				e.printStackTrace();
			}
    	    return null;
    	}

    	@Override
    	protected void onPostExecute(Bitmap bmp) {
    	    super.onPostExecute(bmp);
    	    if(bmp != null && mHolder != null){
    	    	if(mHolder.imageView != null)
    	    		mHolder.imageView.setImageBitmap(bmp);
    	    }
		}

		
	}
}