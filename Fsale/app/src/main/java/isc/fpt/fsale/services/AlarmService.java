package isc.fpt.fsale.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.CancelPotentialScheduleActivity;

import static android.app.PendingIntent.FLAG_ONE_SHOT;

/**
 * Created by HCM.TUANTT14 on 9/7/2017.
 */

public class AlarmService extends IntentService {
    private NotificationManager alarmNotificationManager;

    public AlarmService() {
        super("AlarmService");
    }

    @Override
    public void onHandleIntent(Intent intent) {
        String potentialFullName = intent.getStringExtra("potentialFullName");
        String scheduleDescription = intent.getStringExtra("scheduleDescription");
        int potentialObjScheduleID = intent.getIntExtra("potentialObjScheduleID", 0);
        sendNotification(potentialFullName, scheduleDescription, potentialObjScheduleID);
    }

    private void sendNotification(String potentialFullName, String scheduleDescription, final int potentialObjScheduleID) {
        alarmNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = new Intent(this, CancelPotentialScheduleActivity.class);
        intent.setAction(Long.toString(System.currentTimeMillis()));
        Bundle bundle = new Bundle();
        bundle.putInt("potentialObjScheduleID", potentialObjScheduleID);
        intent.putExtras(bundle);
        PendingIntent contentIntent = PendingIntent.getActivity(this, potentialObjScheduleID, intent, FLAG_ONE_SHOT);
        final NotificationCompat.Builder alarmNotificationBuilder = new NotificationCompat.Builder(
                this).setContentTitle(potentialFullName)
                .setSmallIcon(R.drawable.ic_launcher).setStyle(new NotificationCompat.BigTextStyle().bigText(scheduleDescription))
                .setContentText(scheduleDescription);
        alarmNotificationBuilder.setContentIntent(contentIntent);
        alarmNotificationBuilder.setDeleteIntent(contentIntent);
//        Intent hideReceive = new Intent();
//        hideReceive.setAction("YES_ACTION");
//        Bundle bundleHide = new Bundle();
//        bundleHide.putInt("potentialObjScheduleID", potentialObjScheduleID);
//        hideReceive.putExtras(bundleHide);
//        PendingIntent pendingIntentYes = PendingIntent.getBroadcast(this, potentialObjScheduleID, hideReceive, 0);
//        alarmNotificationBuilder.addAction(R.drawable.ic_launcher, "Tắt thông báo", pendingIntentYes);
        Notification notification = alarmNotificationBuilder.build();
        alarmNotificationManager.notify(potentialObjScheduleID, notification);
    }
}
