package isc.fpt.fsale.action;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.extra_ott.RegisterExtraOttModel;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 22,July,2019
 */
public class UpdateRegistrationExtraOtt implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public UpdateRegistrationExtraOtt(Context mContext, RegisterExtraOttModel object) {
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.msg_pd_update);
        String UPDATE_REGISTRATION = "UpdateRegistration_ExOTT";
        CallServiceTask service = new CallServiceTask(mContext, UPDATE_REGISTRATION, object.toJsonObject(),
                Services.JSON_POST_OBJECT, message, UpdateRegistrationExtraOtt.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                boolean hasError = false;
                List<UpdResultModel> lst = null;
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(jsObj, UpdResultModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                } else {//ServiceType Error
                    hasError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!hasError)
                    loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(List<UpdResultModel> lst) {
        if (lst.size() > 0) {
            final UpdResultModel item = lst.get(0);
            if (item.getResultID() > 0) {
                new AlertDialog.Builder(mContext)
                        .setTitle(mContext.getResources()
                                .getString(R.string.title_notification))
                        .setMessage(item.getResult())
                        .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                new GetRegistrationDetail(mContext, Constants.USERNAME, item.getResultID());
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            } else {
                Common.alertDialog(lst.get(0).getResult(), mContext);
            }
        }
    }
}
