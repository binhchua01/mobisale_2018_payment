package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ListDeploymentProgressModel implements Parcelable{
	private String RegCode;	
	private String Contract;
	private String ObjDate;
	private String DeploymentDate;
	private String WaitTime;
	private String FinishDate;
	
	// add by GiauTQ 01-05-2014
	private String RowNumber;
	private String TotalPage;
	private String CurrentPage;
	private String TotalRow;
	
	// add by GiauTQ 16/07/2014
	private String ObjectPhone;
	private String Allot;
	
	private int SendMail;
	//Add by DuHK
	private String FullName;
	private String DeployAppointmentDate;
	
	
	public ListDeploymentProgressModel(String RegCode, String Contract, String ObjDate, String DeploymentDate, String WaitTime,
									   String finishDate, String rowNumber, String totalPage, String currentPage,String totalRow,
									   String phone, String allot, String fullName) {
		// TODO Auto-generated constructor stub\
		this.setRegCode(RegCode);
		this.setContract(Contract);
		this.setObjDate(ObjDate);
		this.setDeploymentDate(DeploymentDate);
		this.setWaitTime(WaitTime);
		this.setFinishDate(finishDate);
		// add by GiauTQ 01-05-2014
		this.setRowNumber(rowNumber);
		this.setTotalPage(totalPage);
		this.setCurrentPage(currentPage);
		this.setTotalRow(totalRow);
		// add by GiauTQ 16/07/2014
		this.setObjectPhone(phone);
		this.setAllot(allot);
		//
		this.FullName = fullName;
	}
	
	public ListDeploymentProgressModel(String RegCode, String Contract, String ObjDate, String DeploymentDate, String WaitTime,
			   String finishDate, String rowNumber, String totalPage, String currentPage,String totalRow,
			   String phone, String allot, int sendMail, String fullName, String DeployAppointmentDate) {
		// TODO Auto-generated constructor stub\
		this.setRegCode(RegCode);
		this.setContract(Contract);
		this.setObjDate(ObjDate);
		this.setDeploymentDate(DeploymentDate);
		this.setWaitTime(WaitTime);
		this.setFinishDate(finishDate);
		// add by GiauTQ 01-05-2014
		this.setRowNumber(rowNumber);
		this.setTotalPage(totalPage);
		this.setCurrentPage(currentPage);
		this.setTotalRow(totalRow);
		// add by GiauTQ 16/07/2014
		this.setObjectPhone(phone);
		this.setAllot(allot);
		this.setSendMail(sendMail);
		//
		this.FullName = fullName;
		//
		this.DeployAppointmentDate = DeployAppointmentDate;
	}

	public String getObjectPhone()
	{
		return this.ObjectPhone;
	}
	
	public void setObjectPhone(String phone)
	{
		this.ObjectPhone = phone;
	}
	
	public String getAllot()
	{
		return this.Allot;
	}
	
	public void setAllot(String allot)
	{
		this.Allot = allot;
	}
	
	public String getRegCode() {
		return RegCode;
	}

	public void setRegCode(String regCode) {
		RegCode = regCode;
	}

	public String getContract() {
		return Contract;
	}

	public void setContract(String contract) {
		Contract = contract;
	}

	public String getObjDate() {
		return ObjDate;
	}

	public void setObjDate(String objDate) {
		ObjDate = objDate;
	}

	public String getDeploymentDate() {
		return DeploymentDate;
	}

	public void setDeploymentDate(String deploymentDate) {
		DeploymentDate = deploymentDate;
	}

	public String getWaitTime() {
		return WaitTime;
	}

	public void setWaitTime(String waitTime) {
		WaitTime = waitTime;
	}
	
	public void setSendMail(int sendMail) {
		SendMail = sendMail;
	}

	public int getSendMail() {
		return SendMail;
	}

	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	//
	public String getFullName() {
		return FullName;
	}

	public void setFullName(String FullName) {
		this.FullName = FullName;
	}
	
	//
	public String getDeployAppointmentDate() {
		return DeployAppointmentDate;
	}

	public void setDeployAppointmentDate(String DeployAppointmentDate) {
		this.DeployAppointmentDate = DeployAppointmentDate;
	}
	
	@Override
	public void writeToParcel(Parcel pc, int flags) {
		// TODO Auto-generated method stub		
		pc.writeString(RegCode);
		pc.writeString(Contract);
		pc.writeString(ObjDate);		
		pc.writeString(DeploymentDate);
		pc.writeString(WaitTime);	
		pc.writeString(FinishDate);
		// add by GiauTQ 01-05-2014
		pc.writeString(RowNumber);
		pc.writeString(TotalPage);
		pc.writeString(CurrentPage);
		pc.writeString(TotalRow);
		pc.writeString(ObjectPhone);
		pc.writeString(Allot);
		pc.writeInt(SendMail);
		//
		pc.writeString(FullName);
		//
		pc.writeString(DeployAppointmentDate);
	}
	
	public ListDeploymentProgressModel(Parcel source) {
		
		RegCode = source.readString();
		Contract = source.readString();		
		ObjDate = source.readString();
		DeploymentDate = source.readString();
		WaitTime = source.readString();
		FinishDate = source.readString();
		// add by GiauTQ 01-05-2014
		RowNumber = source.readString();
		TotalPage = source.readString();
		CurrentPage = source.readString();
		TotalRow = source.readString();
		ObjectPhone = source.readString();
		Allot = source.readString();
		SendMail = source.readInt();
		//
		FullName = source.readString();
		//
		DeployAppointmentDate = source.readString();
	}
	
	public String getFinishDate() {
		return FinishDate;
	}

	public void setFinishDate(String finishDate) {
		FinishDate = finishDate;
	}
	
	// add by GiauTQ 01-05-2014
	public String getRowNumber() {
		return RowNumber;
	}

	public void setRowNumber(String rowNumber) {
		RowNumber = rowNumber;
	}
	
	public String getTotalPage() {
		return TotalPage;
	}

	public void setTotalPage(String totalPage) {
		TotalPage = totalPage;
	}
	
	public void setCurrentPage(String currentPage) {
		CurrentPage = currentPage;
	}
	
	public String getCurrentPage() {
		return CurrentPage;
	}
	
	public void setTotalRow(String totalRow) {
		TotalRow = totalRow;
	}
	
	public String getTotalRow() {
		return TotalRow;
	}

	/** Static field used to regenerate object, individually or as arrays */
	public static final Creator<ListDeploymentProgressModel> CREATOR = new Creator<ListDeploymentProgressModel>() {
		@Override
		public ListDeploymentProgressModel createFromParcel(Parcel source) {
			return new ListDeploymentProgressModel(source);
		}

		@Override
		public ListDeploymentProgressModel[] newArray(int size) {
			return new ListDeploymentProgressModel[size];
		}
	};

	
}
