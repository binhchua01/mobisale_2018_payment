package isc.fpt.fsale.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetFPTBoxPromotion;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PromotionFPTBoxModel;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3v2;
import isc.fpt.fsale.utils.Common;

public class FPTBoxAdapter extends RecyclerView.Adapter<FPTBoxAdapter.SimpleViewHolder> {
    private List<FPTBox> mList;
    private Context mContext;
    private HashMap<Integer, ArrayList<PromotionFPTBoxModel>> lstPromotionFPTBox;

    public FPTBoxAdapter(Context mContext, List<FPTBox> mList, HashMap<Integer,
            ArrayList<PromotionFPTBoxModel>> lstPromotionFPTBox) {
        this.mContext = mContext;
        this.mList = mList;
        this.lstPromotionFPTBox = lstPromotionFPTBox;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_row_device, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position), position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgDelete;
        private TextView txtDeviceName, txtTotalFPTBox, tvFPTBoxLess, tvFPTBoxPlus;
        private Spinner spPromotion;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            imgDelete = (ImageView) itemView.findViewById(R.id.img_delete);
            txtDeviceName = (TextView) itemView.findViewById(R.id.txt_device_name);
            txtTotalFPTBox = (TextView) itemView.findViewById(R.id.txt_total_count_device);
            tvFPTBoxLess = (TextView) itemView.findViewById(R.id.tv_device_charge_time_less);
            tvFPTBoxPlus = (TextView) itemView.findViewById(R.id.tv_device_charge_time_plus);
            spPromotion = (Spinner) itemView.findViewById(R.id.sp_promotion_device_detail);
        }

        public void bindView(final FPTBox mFptBox, final int pos) {
            imgDelete.setVisibility(View.GONE);
            txtDeviceName.setText(mFptBox.getOTTName());
            txtTotalFPTBox.setText(String.valueOf(mFptBox.getOTTCount()));

            if (lstPromotionFPTBox.get(mFptBox.getOTTID()) == null) {
                ArrayList<PromotionFPTBoxModel> arrayList = new ArrayList<>();
                arrayList.add(new PromotionFPTBoxModel(-1, "Chọn CLKM FPT Box"));
                lstPromotionFPTBox.put(mFptBox.getOTTID(), arrayList);
            }

            tvFPTBoxLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int tempTotalDevice = Integer.valueOf(txtTotalFPTBox.getText().toString());
                    if (tempTotalDevice > 1) {
                        txtTotalFPTBox.setText(String.valueOf(tempTotalDevice - 1));
                        mFptBox.setOTTCount(tempTotalDevice - 1);
                    }

                    FragmentRegisterStep3v2.isBind = 1;
                }

            });

            tvFPTBoxPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int tempTotalDevice = Integer.valueOf(txtTotalFPTBox.getText().toString());
                    txtTotalFPTBox.setText(String.valueOf(tempTotalDevice + 1));
                    mFptBox.setOTTCount(tempTotalDevice + 1);

                    FragmentRegisterStep3v2.isBind = 1;
                }
            });

//            spPromotion.setOnTouchListener(new View.OnTouchListener() {
//                @SuppressLint("ClickableViewAccessibility")
//                public boolean onTouch(View v, MotionEvent event) {
//                    int fptBoxID = mFptBox.getOTTID();
//                    if (event.getAction() == MotionEvent.ACTION_UP) {
//                        String billToDistrict = "";
//                        if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
//                            billToDistrict = (((RegisterActivityNew) mContext).step1.spDistricts.getAdapter() != null &&
//                                    ((RegisterActivityNew) mContext).step1.spDistricts.getSelectedItem() != null) ?
//                                    ((KeyValuePairModel) ((RegisterActivityNew) mContext).step1.spDistricts.getSelectedItem()).getsID() : "";
//                        }
//                        if (lstPromotionFPTBox.get(fptBoxID) != null) {
//                            new GetFPTBoxPromotion(mContext, spPromotion, fptBoxID,
//                                    billToDistrict, mFptBox.getPromotionID());
//                        }
//                    }
//                    return false;
//                }
//            });

            PromotionFPTBoxModelAdapter adapter = new PromotionFPTBoxModelAdapter(
                    mContext, lstPromotionFPTBox.get(mFptBox.getOTTID()));
            spPromotion.setAdapter(adapter);

            if(mFptBox.getPromotionID() != 0){
                spPromotion.setSelection(Common.getIndex(spPromotion, mFptBox.getPromotionID()), true);
                setupSpinner(spPromotion, mFptBox);
            }else{
                setupSpinner(spPromotion, mFptBox);
                spPromotion.setSelection(0, true);
            }
        }
    }

    private void setupSpinner(final Spinner spinner, final FPTBox mFptBox){
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                PromotionFPTBoxModel promotionFPTBoxModel = (PromotionFPTBoxModel) spinner.getSelectedItem();

                //chưa gọi API lấy CLKM
                int fptBoxID = mFptBox.getOTTID();
                if (lstPromotionFPTBox.get(fptBoxID) != null && promotionFPTBoxModel.getPromotionID() == -1) {
                    String billToDistrict = "";
                    if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
                        billToDistrict = (((RegisterActivityNew) mContext).step1.spDistricts.getAdapter() != null &&
                                ((RegisterActivityNew) mContext).step1.spDistricts.getSelectedItem() != null) ?
                                ((KeyValuePairModel) ((RegisterActivityNew) mContext).step1.spDistricts.getSelectedItem()).getsID() : "";
                    }
                    new GetFPTBoxPromotion(mContext, spinner, fptBoxID, billToDistrict, mFptBox.getPromotionID());
                }

                mFptBox.setPromotionID(promotionFPTBoxModel.getPromotionID());
                mFptBox.setPromotionText(promotionFPTBoxModel.getPromotionName());

                FragmentRegisterStep3v2.isBind = 1;
            }

            public void onNothingSelected(AdapterView<?> parent) {
                spinner.setSelection(0, true);
            }
        });
    }
}
