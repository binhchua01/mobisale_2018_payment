package isc.fpt.fsale.ui.maxytv.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.maxy.GetMaxyPackage;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyExtraAdapter;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyPackageAdapter;
import isc.fpt.fsale.ui.maxytv.model.MaxyExtra;
import isc.fpt.fsale.ui.maxytv.model.MaxyGeneral;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.LOCAL_TYPE;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_PACKAGE_DETAIL;
import static isc.fpt.fsale.utils.Constants.REGTYPE;
import static isc.fpt.fsale.utils.Constants.TAG_CONTRACT;
import static isc.fpt.fsale.utils.Constants.USE_COMBO;

public class MaxyPackageActivity extends BaseActivitySecond implements OnItemClickListener<MaxyGeneral> {
    private View loBack;
    private MaxyPackageAdapter mAdapter;
    private List<MaxyGeneral> mList;
    private MaxyGeneral mListSelected;
    private int RegType, localType, useCombo;
    private String contract;
    @Override
    protected void onResume() {
        super.onResume();
        new GetMaxyPackage(this, RegType, localType, contract);
    }

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_maxy_package;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
        tvTitleToolbar.setText("Chọn danh sách truyền hình");
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_maxy_package);
        mList = new ArrayList<>();
        mAdapter = new MaxyPackageAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);
        getDataIntent();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(Constants.LIST_MAXY_PACKAGE_SELECTED) != null) {
            mListSelected = bundle.getParcelable(Constants.LIST_MAXY_PACKAGE_SELECTED);
            RegType = bundle.getInt(REGTYPE);
            localType = bundle.getInt(LOCAL_TYPE);
            contract = bundle.getString(TAG_CONTRACT);
            useCombo = bundle.getInt(USE_COMBO);
        }
    }

    @Override
    public void onItemClick(MaxyGeneral object) {
        object.setUseCombo(useCombo);
        object.setTypeSetupID(mListSelected.getTypeSetupID());
        object.setTypeSetupName(mListSelected.getTypeSetupName());
        object.setTypeDeployID(mListSelected.getTypeDeployID());
        object.setTypeDeployName(mListSelected.getTypeDeployName());
        Intent returnIntent = new Intent();
        returnIntent.putExtra(OBJECT_MAXY_PACKAGE_DETAIL, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    public void mLoadPackage(List<MaxyGeneral> lst) {
        this.mList = lst;
        mAdapter.notifyData(this.mList);
    }
}
