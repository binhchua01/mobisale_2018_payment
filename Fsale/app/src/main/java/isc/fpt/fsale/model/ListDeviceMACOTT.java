package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by haulc3 on 16,January,2019
 */
public class ListDeviceMACOTT implements Parcelable {
    private List<FPTBox> listDeviceOTT;
    private List<MacOTT> listMACOTT;

    protected ListDeviceMACOTT(Parcel in) {
        listDeviceOTT = in.createTypedArrayList(FPTBox.CREATOR);
        listMACOTT = in.createTypedArrayList(MacOTT.CREATOR);
    }

    public static final Creator<ListDeviceMACOTT> CREATOR = new Creator<ListDeviceMACOTT>() {
        @Override
        public ListDeviceMACOTT createFromParcel(Parcel in) {
            return new ListDeviceMACOTT(in);
        }

        @Override
        public ListDeviceMACOTT[] newArray(int size) {
            return new ListDeviceMACOTT[size];
        }
    };

    public List<FPTBox> getListDeviceOTT() {
        return listDeviceOTT;
    }

    public void setListDeviceOTT(List<FPTBox> listDeviceOTT) {
        this.listDeviceOTT = listDeviceOTT;
    }

    public List<MacOTT> getListMACOTT() {
        return listMACOTT;
    }

    public void setListMACOTT(List<MacOTT> listMACOTT) {
        this.listMACOTT = listMACOTT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(listDeviceOTT);
        parcel.writeTypedList(listMACOTT);
    }
}
