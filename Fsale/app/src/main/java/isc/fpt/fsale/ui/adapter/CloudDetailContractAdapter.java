package isc.fpt.fsale.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV4;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.utils.Common;

/**
 * Created by Hau Le on 2018-12-26.
 */
public class CloudDetailContractAdapter extends RecyclerView.Adapter<CloudDetailContractAdapter.SimpleViewHolder> {
    private Context mContext;
    private List<CloudDetail> mList;
    private OnItemClickListenerV4 mListener;
    private OnItemClickListener mUpdatePromotion;

    public CloudDetailContractAdapter(Context mContext, List<CloudDetail> mList,
                                      OnItemClickListenerV4 mListener, OnItemClickListener mUpdatePromotion) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
        this.mUpdatePromotion = mUpdatePromotion;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_cloud_service_selected, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(mList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvCloudPackage, tvCloudType, tvCloudServiceLess, tvCloudServiceMore;
        ImageView imgDelete;
        TextView tvCloudServiceQuantity, tvCloudServiceTotal;

        SimpleViewHolder(View itemView) {
            super(itemView);
            tvCloudPackage = (TextView) itemView.findViewById(R.id.tv_cloud_service_name);
            tvCloudType = (TextView) itemView.findViewById(R.id.tv_cloud_type);
            tvCloudServiceLess = (TextView) itemView.findViewById(R.id.tv_cloud_service_less);
            tvCloudServiceMore = (TextView) itemView.findViewById(R.id.tv_cloud_service_plus);
            tvCloudServiceQuantity = (TextView) itemView.findViewById(R.id.lbl_cloud_service_quantity);
            tvCloudServiceTotal = (TextView) itemView.findViewById(R.id.tv_cloud_service_total);
            imgDelete = (ImageView) itemView.findViewById(R.id.img_delete);
        }

        void bindView(final CloudDetail mCloudDetail, final int position) {
            tvCloudType.setText(mCloudDetail.getTypeName());
            tvCloudPackage.setText(mCloudDetail.getPackName());
            tvCloudServiceQuantity.setText(String.valueOf(mCloudDetail.getQty()));
            tvCloudServiceTotal.setText(calculatorCloudServiceTotal(mCloudDetail.getCost(), mCloudDetail.getQty()));

            tvCloudServiceLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (tvCloudPackage.getText().toString()
                            .equals(mContext.getResources().getString(R.string.lbl_choose_cloud_package_expired))) {
                        return;
                    }
                    int quantity = mCloudDetail.getQty();
                    if (quantity == 1) {
                        return;
                    }
                    quantity--;
                    mCloudDetail.setQty(quantity);
                    tvCloudServiceQuantity.setText(String.valueOf(mCloudDetail.getQty()));
                    tvCloudServiceTotal.setText(calculatorCloudServiceTotal(mCloudDetail.getCost(), mCloudDetail.getQty()));
                    mListener.onItemClickV4(mCloudDetail, position, 5);
                    mUpdatePromotion.onItemClick(true);
                }
            });

            tvCloudServiceMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (tvCloudPackage.getText().toString()
                            .equals(mContext.getResources().getString(R.string.lbl_choose_cloud_package_expired))) {
                        return;
                    }
                    int quantity = mCloudDetail.getQty();
                    quantity++;
                    mCloudDetail.setQty(quantity);
                    tvCloudServiceQuantity.setText(String.valueOf(mCloudDetail.getQty()));
                    tvCloudServiceTotal.setText(calculatorCloudServiceTotal(mCloudDetail.getCost(), mCloudDetail.getQty()));
                    mListener.onItemClickV4(mCloudDetail, position, 4);
                    mUpdatePromotion.onItemClick(true);
                }
            });

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClickV4(mCloudDetail, position, 3);
                }
            });
        }
    }

    public void notifyDataChanged(List<CloudDetail> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    //calculator total of cloud service
    private String calculatorCloudServiceTotal(int cost, int quantity) {
        return String.format(mContext.getString(R.string.txt_unit_price), Common.formatNumber((cost * quantity)));
    }
}
