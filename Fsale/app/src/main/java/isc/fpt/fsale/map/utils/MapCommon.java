package isc.fpt.fsale.map.utils;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.location.Criteria;
import android.util.Log;

/**
 * CLASS: 			MapCommon
 * @description:	common map methods container
 * @author: 		vandn
 * @created on: 	09/08/2013 *
 * */
public class MapCommon {
	public static Circle radiusCircle;

	//--------------------------#region: SETTINGS---------------------------------------------
	/**
	 * set GPS best provider
	 * */
	public static final Criteria initGPS_Settings(){
		Criteria criteria = new Criteria();
		try{
			criteria.setPowerRequirement(Criteria.POWER_HIGH);
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
	    	criteria.setAltitudeRequired(false);
	    	criteria.setBearingRequired(false);
	    	criteria.setSpeedRequired(false);
	    	criteria.setCostAllowed(true);
		}
		catch (Exception e){

			e.printStackTrace();
		}
		return criteria;
	}

	//-------------------------#region: CONTROL MAP--------------------------------------------
	/**
	 * set point to map view center
	 * */
	public static void animeToLocation(LatLng point){
		if(point != null){
			MapConstants.MAP.moveCamera(CameraUpdateFactory.newLatLng(point));
		}
	}

	public static void animeToLocation(GoogleMap map, LatLng point){
		if(point != null){
			if(map != null)
				map.moveCamera(CameraUpdateFactory.newLatLng(point));
		}
	}

	/**
	 * zoom map to specific size
	 */
	public static void setMapZoom (int zoomSize){
		if(MapConstants.MAP != null)
			MapConstants.MAP.animateCamera(CameraUpdateFactory.zoomTo(zoomSize));
	}

	public static void setMapZoom (GoogleMap map, int zoomSize){
		if(map != null)
			map.animateCamera(CameraUpdateFactory.zoomTo(zoomSize));
	}

	/**
	 * add marker to map
	 */
	public static Marker addMarkerOnMap(String title, LatLng coordinate, int drawableID, boolean isDraggable){
		MarkerOptions markerOption = new MarkerOptions();
		markerOption.position(coordinate);
        markerOption.title(title);
        markerOption.icon(BitmapDescriptorFactory.fromResource(drawableID));
		if(isDraggable){
	        markerOption.draggable(true);
		}
		Marker startPerc = MapConstants.MAP.addMarker(markerOption);
		return startPerc;
	}

	public static Marker addMarkerOnMap(GoogleMap map, String title, LatLng coordinate, int drawableID, boolean isDraggable){
		MarkerOptions markerOption = new MarkerOptions();
		markerOption.position(coordinate);
        markerOption.title(title);
        markerOption.icon(BitmapDescriptorFactory.fromResource(drawableID));
		if(isDraggable){
	        markerOption.draggable(true);
		}

		Marker startPerc = map.addMarker(markerOption);
		return startPerc;
	}

	//------------------------#region: DRAW---------------------------------------------------
	/**
	 * Add marker my location on map
	 * */
	/*public static void addMylocationIconOnMap(LatLng p, String title) {
		try {					
			String scoordinates = MapCommon.getGPSCoordinates(p);
			Marker startPerc = MapConstants.MAP.addMarker(new MarkerOptions()
	        .position(p)
	        .title(title)
	       // .snippet(snippet)
	        .icon(BitmapDescriptorFactory.fromResource(R.drawable.cuslocation_red))	       
	        .draggable(true));			
			// radiusCircle = CommonDraw.drawCirclebyRadius(fRadius, p);
			animeToLocation(p);
		} 
		catch (Exception e) {
			Log.e("LOG_ADD_MYLOCATE_ON_MAP", e.getMessage());
		}
	}*/
	
/*
	*//**
	 * Add location marker to map
	 *//*
	public void addMyLocationOnMap() {
		try {		
			
			MapConstants.MAP.clear();	
			String title = mContext.getString(R.string.title_yourlocation);
			String address = MapCommon.getAddressByLatlng(getGPSCoordinates());
			String snippet = "MyLocation" +"@Dia chi: " + address + "@Sai so: " + getAccuarcy() + "m" + "@Toa do: " + getGPSCoordinates(); 
			MapCommon.addMylocationIconOnMap(coordinate, title, snippet, getAccuarcy());	
			MapCommon.setMapZoom(16);
		}
		catch (Exception e) {
			Log.e("LOG_ADD_MYLOCATE_ON_MAP", e.getMessage());
		}
	}*/
	//-----------------------#end region: DRAW------------------------------------------------

	//-----------------------#region: CONVERT-------------------------------------------------

	/**
	 * convert lat, lng to string coordinates (added by vandn, 05/12/2012)
	 */
	public static String getGPSCoordinates(double lat, double lng){
		return "(" + String.valueOf(lat) + "," + String.valueOf(lng) + ")";
	}

	/**
	 * convert GeoPoint to string coordinates (added by vandn, 05/12/2012)
	 */
	public static String getGPSCoordinates(LatLng p){
		return "(" + p.latitude + "," + p.longitude + ")";
	}

	/**
	 * convert string LatLng to GeoPoint(added by vandn, 04/12/2012)
	 */
	public static LatLng ConvertStrToLatLng(String strLatLng)
	{
		LatLng point = null;
		if(strLatLng != null && !strLatLng.trim().equals("") &&!strLatLng.equals("null")){
			String[] coordinates = strLatLng.replace("(", "").replace(")", "").split(",");
			if(coordinates.length >1){
				Double lat = Double.parseDouble(coordinates[0].trim());
				Double lng = Double.parseDouble(coordinates[1].trim());
				point = new LatLng(lat, lng);
			}
		}
		return point;
	}

	/**
	 * get address for given latlng
	 * */
	public static String getAddressByLatlng(String sCoordinates){
		String sLatLng = sCoordinates.replace("(", "").replace(")", "");
		String sUrl;
		String address = "";
		try {
			sUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + URLEncoder.encode(sLatLng, "utf8")+ "&sensor=false";
			JSONObject jsonObj = connectGooglePlace(sUrl);
			if(jsonObj != null){
				address = jsonObjToAddress(jsonObj);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		return address;
	}

	/**
	 * connect google api convert latlng to address/address to latlng
	 * */
	public static JSONObject connectGooglePlace(String sUrl) {
		JSONObject jsonObject = null;
        StringBuilder stringBuilder = new StringBuilder();
        try{
	        URL url = new URL(sUrl);
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		       conn.setReadTimeout(100000 /* milliseconds */);
		       conn.setConnectTimeout(150000 /* milliseconds */);
		       conn.setRequestMethod("GET");
		       conn.setDoInput(true);
		   conn.connect();
		   InputStreamReader in = new InputStreamReader(conn.getInputStream());
	       int read;
	       char[] buff = new char[1024];
	       while ((read = in.read(buff)) != -1) {
	    	   stringBuilder.append(buff, 0, read);
           }
	       in.close();
	       conn.disconnect();
	       jsonObject = new JSONObject(stringBuilder.toString());
	   }catch (Exception e){

		   Log.e("LOG_CONNECT_GOOGLE_PLACE", e.getMessage());
		   jsonObject = null;
	   }
	   return jsonObject;
	}
	/**
	 * convert json object to address
	 * */
	public static String jsonObjToAddress(JSONObject jsonObject){
	    String address = "";
	    try{
		    address = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
		    .getString("formatted_address");
	    }
	    catch (JSONException ex){

	    	Log.e("LOG_CONVERT_JSON_TO_ADDRESS", ex.getMessage());
	    }
	    return address;

	}
	//-----------------------#end region: CONVERT----------------------------------------------

}
