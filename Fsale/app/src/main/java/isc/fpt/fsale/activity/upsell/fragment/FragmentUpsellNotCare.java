package isc.fpt.fsale.activity.upsell.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.danh32.fontify.EditText;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.upsell.GetObjUpgradeList;
import isc.fpt.fsale.action.upsell.GetProgramUpgrade;
import isc.fpt.fsale.activity.SwipeRefreshLayout;
import isc.fpt.fsale.activity.upsell.create_upgrade.CreateInfoUpgradeActicity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.upsell.KeyValuePairProgramUpgradeAdapter;
import isc.fpt.fsale.adapter.upsell.UpsellNotcareAdapter;
import isc.fpt.fsale.callback.upsell.OnItemListenerUpsell;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeListModel;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeTransListModel;
import isc.fpt.fsale.model.upsell.response.ProgramUpgradeModel;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class FragmentUpsellNotCare extends BaseFragment implements OnItemListenerUpsell {

    EditText mEdtSearch;
    Spinner mSpnAgent, mSpnProgram;
    RecyclerView mRecyclerDetail;
    private UpsellNotcareAdapter adapter;
    private List<ObjUpgradeListModel> nodelList = new ArrayList<>();
    private List<ProgramUpgradeModel> mProgram = new ArrayList<>();
    ProgramUpgradeModel keyvalue;
    SwipeRefreshLayout refreshLayout;
    ImageView mImgFind;
    boolean isLoading = false;
    private int pageNum = 1, agent;

    public static FragmentUpsellNotCare newInstance() {
        return new FragmentUpsellNotCare();
    }

    @Override
    protected void initView(View mView) {
        mSpnAgent = mView.findViewById(R.id.sp_agent);
        mRecyclerDetail = mView.findViewById(R.id.recycler_detail);
        mSpnProgram = mView.findViewById(R.id.spn_program);
        mEdtSearch = mView.findViewById(R.id.edt_search_value);
        mImgFind = mView.findViewById(R.id.img_find);
        refreshLayout = mView.findViewById(R.id.swiperefresh);
    }

    @Override
    public void onResume() {
        super.onResume();
        String[] enable = {"1"};
        new GetProgramUpgrade(getContext(), enable, lst -> {
            if (lst != null) {
                this.mProgram = lst;
                loadProgramType();
            }
        });
    }

    @Override
    protected void initEvent() {
        mImgFind.setOnClickListener(v -> {
            Common.hideSoftKeyboard(getContext());
            agent = ((KeyValuePairModel) mSpnAgent.getSelectedItem()).getID();
            pageNum = 1;
            String[] arrValue = {Constants.USERNAME, keyvalue.getID().toString(), String.valueOf(agent),
                    mEdtSearch.getText().toString(), "10", String.valueOf(pageNum)};
            new GetObjUpgradeList(getContext(), arrValue, lst -> {
                if (lst != null) {
                    if (lst.size() < 10) {
                        isLoading = true;
                    } else {
                        pageNum = pageNum + 1;
                        isLoading = false;
                    }
                    nodelList = lst;
                    initRecyclerView();
                }
            });

        });
        refreshLayout.setOnRefreshListener(() -> {
            loadProgramType();
            mEdtSearch.setText("");
            refreshLayout.setRefreshing(false);
        });
        loadSearchType();
        initScrollListener();
    }

    @Override
    protected void bindData() {

    }

    private void loadProgramType() {
        @SuppressLint("RtlHardcoded") KeyValuePairProgramUpgradeAdapter adapter = new KeyValuePairProgramUpgradeAdapter(getContext(),
                R.layout.my_spinner_style, mProgram, Gravity.LEFT);
        mSpnProgram.setAdapter(adapter);
        mSpnProgram.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                keyvalue = mProgram.get(position);
                agent = ((KeyValuePairModel) mSpnAgent.getSelectedItem()).getID();
                pageNum = 1;
                String[] value = {Constants.USERNAME, keyvalue.getID().toString(),
                        String.valueOf(agent), "", "10", String.valueOf(pageNum)};
                new GetObjUpgradeList(getContext(), value, mList -> {
                    if (mList != null) {
                        if (mList.size() < 10) {
                            isLoading = true;
                        } else {
                            pageNum = pageNum + 1;
                            isLoading = false;
                        }
                        nodelList = mList;
                        initRecyclerView();
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initRecyclerView() {
        adapter = new UpsellNotcareAdapter(getContext(), nodelList, this);
        mRecyclerDetail.setAdapter(adapter);
        mRecyclerDetail.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_upsell_main;
    }

    private void loadSearchType() {
        ArrayList<KeyValuePairModel> lstSearchType = new ArrayList<>();
        lstSearchType.add(new KeyValuePairModel(1, "Số HD"));
        lstSearchType.add(new KeyValuePairModel(2, "Tên KH"));
        lstSearchType.add(new KeyValuePairModel(3, "Phone"));
        lstSearchType.add(new KeyValuePairModel(4, "Địa chỉ"));
        @SuppressLint("RtlHardcoded") KeyValuePairAdapter adapter = new KeyValuePairAdapter(getContext(),
                R.layout.my_spinner_style, lstSearchType, Gravity.LEFT);
        mSpnAgent.setAdapter(adapter);
    }

    @Override
    public void CareOnItemListener(ObjUpgradeTransListModel careModel) {

    }

    private void initScrollListener() {
        mRecyclerDetail.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null &&
                            linearLayoutManager.findLastCompletelyVisibleItemPosition() == nodelList.size() - 1) {
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        isLoading = false;
    }

    private void loadMore() {
        new Handler().postDelayed(() -> {
            agent = ((KeyValuePairModel) mSpnAgent.getSelectedItem()).getID();
            String[] arrValue = {Constants.USERNAME.trim(), keyvalue.getID().toString(),
                    String.valueOf(agent), "", "10", String.valueOf(pageNum)};
            new GetObjUpgradeList(getContext(), arrValue, lst -> {
                if (lst != null) {
                    if (lst.size() < 10) {
                        isLoading = true;
                    } else {
                        pageNum = pageNum + 1;
                        isLoading = false;
                    }
                    nodelList.addAll(lst);
                    adapter.notifyDataSetChanged();
                }
            });
        }, 200);
    }

    @Override
    public void NotCareOnItemListener(ObjUpgradeListModel careModel) {
        Intent intent = new Intent(getContext(), CreateInfoUpgradeActicity.class);
        intent.putExtra("OBJ_ID", careModel.getObjID().toString());
        intent.putExtra("CONTRACT", careModel.getContract());
        intent.putExtra("PROGRAM", keyvalue.getID().toString());
        startActivity(intent);
    }
}
