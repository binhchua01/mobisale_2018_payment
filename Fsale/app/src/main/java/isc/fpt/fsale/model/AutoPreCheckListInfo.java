package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 7/9/2018.
 */
// model chứa thông tin checklist tự động
public class AutoPreCheckListInfo implements Parcelable {
    private int Init_Status;
    private String Init_Status_Text;
    private int StatusCL;
    private String Supporter;
    private String SubSupporter;
    private int DeptID;
    private int OwnerType;

    public AutoPreCheckListInfo() {

    }

    public AutoPreCheckListInfo(int init_Status, int statusCL, String supporter, String subSupporter, int deptID, int ownerType) {
        Init_Status = init_Status;
        StatusCL = statusCL;
        Supporter = supporter;
        SubSupporter = subSupporter;
        DeptID = deptID;
        OwnerType = ownerType;
    }

    public int getInit_Status() {
        return Init_Status;
    }

    public void setInit_Status(int init_Status) {
        Init_Status = init_Status;
    }

    public String getInit_Status_Text() {
        return Init_Status_Text;
    }

    public void setInit_Status_Text(String init_Status_Text) {
        Init_Status_Text = init_Status_Text;
    }

    public int getStatusCL() {
        return StatusCL;
    }

    public void setStatusCL(int statusCL) {
        StatusCL = statusCL;
    }

    public String getSupporter() {
        return Supporter;
    }

    public void setSupporter(String supporter) {
        Supporter = supporter;
    }

    public String getSubSupporter() {
        return SubSupporter;
    }

    public void setSubSupporter(String subSupporter) {
        SubSupporter = subSupporter;
    }

    public int getDeptID() {
        return DeptID;
    }

    public void setDeptID(int deptID) {
        DeptID = deptID;
    }

    public int getOwnerType() {
        return OwnerType;
    }

    public void setOwnerType(int ownerType) {
        OwnerType = ownerType;
    }

    protected AutoPreCheckListInfo(Parcel in) {
        Init_Status = in.readInt();
        Init_Status_Text = in.readString();
        StatusCL = in.readInt();
        Supporter = in.readString();
        SubSupporter = in.readString();
        DeptID = in.readInt();
        OwnerType = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Init_Status);
        dest.writeString(Init_Status_Text);
        dest.writeInt(StatusCL);
        dest.writeString(Supporter);
        dest.writeString(SubSupporter);
        dest.writeInt(DeptID);
        dest.writeInt(OwnerType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AutoPreCheckListInfo> CREATOR = new Creator<AutoPreCheckListInfo>() {
        @Override
        public AutoPreCheckListInfo createFromParcel(Parcel in) {
            return new AutoPreCheckListInfo(in);
        }

        @Override
        public AutoPreCheckListInfo[] newArray(int size) {
            return new AutoPreCheckListInfo[size];
        }
    };


}
