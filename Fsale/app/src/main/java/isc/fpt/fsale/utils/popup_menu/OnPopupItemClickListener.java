package isc.fpt.fsale.utils.popup_menu;
/**
 * INTERFACE: 	OnPopupItemClickListener
 * @author: 	vandn
 * @created on: 12/11/2013
 * */
public interface OnPopupItemClickListener {
	public abstract void onItemClick(int itemId);
}
