package isc.fpt.fsale.database;

import isc.fpt.fsale.database.DaoMaster.OpenHelper;
import isc.fpt.fsale.database.mPOSLogModelDao.Properties;
import isc.fpt.fsale.model.mPOSLogModel;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class mPOSLogTable {
	private SQLiteDatabase db;    
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private mPOSLogModelDao mPOSLogDao;
    private OpenHelper helper;
    private Context mContext;
    
    public mPOSLogTable(Context context){
    	this.mContext = context;
    	helper = new OpenHelper(mContext, Constants.DATABASE_NAME, null) {
			
			@Override
			public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
				// TODO Auto-generated method stub
				
			}
		};    	
		/*db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        messageDao = daoSession.getMessageModelDao();*/
    }
    
    private void openDB(){
    	db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        mPOSLogDao = daoSession.getMPOSLogModelDao();
    }
    
    private void closeDB(){
    	if(helper != null)
    		helper.close();    	
    }
    public Long add(mPOSLogModel item){
    	openDB();
    	Long result =  mPOSLogDao.insert(item);
    	closeDB();
    	return result;
    }
    
    public int add(List<mPOSLogModel> items){
    	int count = 0;
    	try {
			openDB();    	
	    	for(mPOSLogModel item : items )
	    		if(mPOSLogDao.insert(item) >0)
	    			count++;
	    	closeDB();
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}    
    	return count;
    }
    
    public void update(mPOSLogModel item){
    	 openDB();
    	 mPOSLogDao.update(item);
    	 closeDB();
    }
    
    public void update(List<mPOSLogModel> items){
    	openDB();
    	for(mPOSLogModel item : items )
    		mPOSLogDao.update(item);
    	closeDB();
   }
    
    /**
     * TODO: Xoa item theo ClientID
     * */
    public int delete(String ClienID){
    	int count = 0;
    	try {    		
    		List<mPOSLogModel> result = get(ClienID);
        	if(result != null && result.size() > 0){        		
        		for(mPOSLogModel item: result){
        			delete(item);
        			count++;
        		}        		
        	} 
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}    	
    	return count;
    }
    
    public List<mPOSLogModel> get(String ClientID){    	
    	openDB();
    	List<mPOSLogModel> result = mPOSLogDao.queryBuilder().where(Properties.ClientID.eq(ClientID)).list();
    	closeDB();
    	return result;
    }
    
    public void delete(mPOSLogModel item){
    	openDB();
    	mPOSLogDao.delete(item);
    	closeDB();
    }
    
    public void delete(List<mPOSLogModel> items){
    	try {
    		for(mPOSLogModel item: items)
    			delete(item);
		} catch (Exception e) {

			e.printStackTrace();
		}
    }
    
    public void removeAll(){
    	openDB();
    	mPOSLogDao.deleteAll();
    	closeDB();
    }
    
    public List<mPOSLogModel> getAll(){
    	openDB();
    	List<mPOSLogModel> result = mPOSLogDao.queryBuilder().orderDesc(Properties.ClientDate).list();
    	closeDB();
    	return result;
    }
    
    public List<mPOSLogModel> getAllOrderByDate(){
    	openDB();
    	List<mPOSLogModel> result = mPOSLogDao.loadAll();
    	closeDB();
    	return result;
    }
    
   
    
}
