package isc.fpt.fsale.utils;

import isc.fpt.fsale.model.CloudPackageSelected;

/**
 * Created by Hau Le on 2019-01-02.
 */
public interface CloudServiceCallback {
    void CallbackCloudServiceListSelected(CloudPackageSelected obj);
}
