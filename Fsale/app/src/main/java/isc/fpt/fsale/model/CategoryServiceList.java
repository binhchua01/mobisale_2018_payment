package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haulc3 on 17,July,2019
 */
public class CategoryServiceList implements Parcelable {
    @SerializedName("CategoryServiceID")
    @Expose
    private Integer categoryServiceID;
    @SerializedName("CategoryServiceName")
    @Expose
    private String categoryServiceName;

    private boolean isSelected;

    public CategoryServiceList() {
    }

    public CategoryServiceList(Integer categoryServiceID, String categoryServiceName) {
        this.categoryServiceID = categoryServiceID;
        this.categoryServiceName = categoryServiceName;
    }

    protected CategoryServiceList(Parcel in) {
        if (in.readByte() == 0) {
            categoryServiceID = null;
        } else {
            categoryServiceID = in.readInt();
        }
        categoryServiceName = in.readString();
    }

    public static final Creator<CategoryServiceList> CREATOR = new Creator<CategoryServiceList>() {
        @Override
        public CategoryServiceList createFromParcel(Parcel in) {
            return new CategoryServiceList(in);
        }

        @Override
        public CategoryServiceList[] newArray(int size) {
            return new CategoryServiceList[size];
        }
    };

    public Integer getCategoryServiceID() {
        return categoryServiceID;
    }

    public void setCategoryServiceID(Integer categoryServiceID) {
        this.categoryServiceID = categoryServiceID;
    }

    public String getCategoryServiceName() {
        return categoryServiceName;
    }

    public void setCategoryServiceName(String categoryServiceName) {
        this.categoryServiceName = categoryServiceName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (categoryServiceID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(categoryServiceID);
        }
        dest.writeString(categoryServiceName);
    }
}
