package isc.fpt.fsale.utils;

import org.json.JSONException;

/**
 * @interface: 		AsyncTaskCompleteListener
 * @Description: 	asyncTask complete listerner
 * @author: 		vandn
 * @create date: 	13/08/2013
 * */
public interface AsyncTaskCompleteListener<T> {
	 void onTaskComplete(T result) throws JSONException;
}
