package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.CreatePotentialCEMObjActivity;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.activity.ListPotentialObjSurveyListActivity;
import isc.fpt.fsale.activity.PotentialObjDetailActivity;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;

// lấy chi tiết khách hàng tiềm năng
public class GetPotentialObjDetail implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    public final String TAG_METHOD_NAME = "GetPotentialObjDetail";
    private String[] paramNames, paramValues;
    private boolean isCreate = false;
    private String supporter;

    public GetPotentialObjDetail(Context context, String UserName, int ID) {
        this.mContext = context;
        this.paramNames = new String[]{"UserName", "ID"};
        this.paramValues = new String[]{UserName, String.valueOf(ID)};
        String message = "Đang lấy thông tin khách hàng...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME,
                paramNames, paramValues, Services.JSON_POST, message, GetPotentialObjDetail.this);
        service.execute();
    }

    public GetPotentialObjDetail(Context context, String UserName, int ID, boolean isCreate) {
        this.mContext = context;
        this.paramNames = new String[]{"UserName", "ID"};
        this.paramValues = new String[]{UserName, String.valueOf(ID)};
        this.isCreate = isCreate;
        String message = "Đang lấy thông tin khách hàng...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME,
                paramNames, paramValues, Services.JSON_POST, message, GetPotentialObjDetail.this);
        service.execute();
    }

    public GetPotentialObjDetail(Context context, String UserName, int ID, boolean isCreate, String supporter) {
        this.mContext = context;
        this.paramNames = new String[]{"UserName", "ID"};
        this.paramValues = new String[]{UserName, String.valueOf(ID)};
        this.isCreate = isCreate;
        this.supporter = supporter;
        String message = "Đang lấy thông tin khách hàng...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME,
                paramNames, paramValues, Services.JSON_POST, message, GetPotentialObjDetail.this);
        service.execute();
    }

    public GetPotentialObjDetail(Context context, String UserName, int ID, String supporter) {
        this.mContext = context;
        this.supporter = supporter;
        this.paramNames = new String[]{"UserName", "ID"};
        this.paramValues = new String[]{UserName, String.valueOf(ID)};
        String message = "Đang lấy thông tin khách hàng...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME,
                paramNames, paramValues, Services.JSON_POST, message, GetPotentialObjDetail.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<PotentialObjModel> lst = null;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<PotentialObjModel> resultObject = new WSObjectsModel<>(jsObj, PotentialObjModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                } else {//ServiceType Error
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(List<PotentialObjModel> lst) {
        try {
            if (mContext != null && lst != null && lst.size() > 0) {
                if (!isCreate) {
                    Intent intent = new Intent(mContext, PotentialObjDetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("POTENTIAL_OBJECT", lst.get(0));
                    intent.putExtra(PotentialObjDetailActivity.TAG_SUPPORTER, this.supporter);
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, ListPotentialObjSurveyListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("POTENTIAL_OBJECT", lst.get(0));
                    mContext.startActivity(intent);
                }
                if (mContext.getClass().getSimpleName().equals(CreatePotentialObjActivity.class.getSimpleName())) {
                    ((CreatePotentialObjActivity) mContext).finish();
                } else if (mContext.getClass().getSimpleName().equals(ListPotentialObjSurveyListActivity.class.getSimpleName())) {
                    ((ListPotentialObjSurveyListActivity) mContext).finish();
                } else if (mContext.getClass().getSimpleName().equals(CreatePotentialCEMObjActivity.class.getSimpleName())) {
                    ((CreatePotentialCEMObjActivity) mContext).finish();
                }
            } else {
                Common.alertDialog("Không có dữ liệu.", mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}