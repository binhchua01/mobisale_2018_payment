package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.UpdatePotentialAdvisoryResultActivity;
import isc.fpt.fsale.model.PotentialAdvisoryResultDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class GetPotentialAdvisoryResultDetail implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    public final String TAG_METHOD_NAME = "GetPotentialAdvisoryResultDetail";
    private String[] paramValues;
    public static final String[] paramNames = new String[]{"UserName", "PotentialID"};

    public GetPotentialAdvisoryResultDetail(Context context, int PotentialID) {
        this.mContext = context;
        this.paramValues = new String[]{Constants.USERNAME, String.valueOf(PotentialID)};
        execute();
    }

    public void execute() {
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues,
                Services.JSON_POST, message, GetPotentialAdvisoryResultDetail.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            boolean isError = false;
            ArrayList<PotentialAdvisoryResultDetailModel> lst = null;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<PotentialAdvisoryResultDetailModel> resultObject = new WSObjectsModel<>(jsObj, PotentialAdvisoryResultDetailModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getArrayListObject();
                } else {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError)
                    loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void loadData(ArrayList<PotentialAdvisoryResultDetailModel> lst) {
        if (mContext.getClass().getSimpleName().equals(UpdatePotentialAdvisoryResultActivity.class.getSimpleName())) {
            UpdatePotentialAdvisoryResultActivity activity = (UpdatePotentialAdvisoryResultActivity) mContext;
            activity.loadResultList(lst);
        }
    }
}