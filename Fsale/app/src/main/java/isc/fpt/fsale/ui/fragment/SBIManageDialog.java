package isc.fpt.fsale.ui.fragment;

import java.util.ArrayList;

import com.danh32.fontify.TextView;

import isc.fpt.fsale.action.GetSBIList;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.SBIAdapter;
import isc.fpt.fsale.model.SBIModel;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

// màn hình danh sách SBI
public class SBIManageDialog extends DialogFragment{
	private Context mContext;
	private ImageButton imgClose;
	private TextView txtSBITotal;
	private ListView lvSBIInTime, lvSBIOutOfTime, lvSBIUsed, lvSBICancel;
	private LinearLayout frmSBIInTime, frmSBIOutOfTime, frmSBIUsed, frmSBICancel;
	private TextView lblInTimeTotal, lblOutOfTimeTotal, lblUsedTotal, lblCancelTotal;
	private ProgressBar procBar;
	public SBIManageDialog(){ }
	@SuppressLint("ValidFragment")
	public SBIManageDialog(Context context) {		
		this.mContext= context;				
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		// Inflate the layout to use as dialog or embedded fragment
		View view = inflater.inflate(R.layout.dialog_sbi_manager, container);
		onClickTracker("SBIManageDialog");
		procBar = (ProgressBar)view.findViewById(R.id.progress_bar);
		imgClose = (ImageButton)view.findViewById(R.id.btn_close_dialog_sbi_manager);
		txtSBITotal = (TextView)view.findViewById(R.id.txt_sbi_total);
		lvSBIInTime = (ListView)view.findViewById(R.id.lv_sbi_in_time);
		lvSBIOutOfTime = (ListView)view.findViewById(R.id.lv_sbi_out_of_time);
		lvSBIUsed = (ListView)view.findViewById(R.id.lv_sbi_used);
		lvSBICancel = (ListView)view.findViewById(R.id.lv_sbi_cancel);
		imgClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dismiss();
				
			}
		});
		lblInTimeTotal = (TextView)view.findViewById(R.id.lbl_sbi_in_time_total);
		lblOutOfTimeTotal = (TextView)view.findViewById(R.id.lbl_sbi_out_of_time_total);
		lblUsedTotal = (TextView)view.findViewById(R.id.lbl_sbi_used);
		lblCancelTotal = (TextView)view.findViewById(R.id.lbl_sbi_cancel);
		
		frmSBIInTime= (LinearLayout)view.findViewById(R.id.frm_sbi_in_time);
		frmSBIOutOfTime= (LinearLayout)view.findViewById(R.id.frm_sbi_out_of_time);
		frmSBIUsed= (LinearLayout)view.findViewById(R.id.frm_sbi_used);
		frmSBICancel= (LinearLayout)view.findViewById(R.id.frm_sbi_cancel);
		
		frmSBIInTime.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(lvSBIInTime.getVisibility() == View.GONE)
					lvSBIInTime.setVisibility(View.VISIBLE);
				else
					lvSBIInTime.setVisibility(View.GONE);
				
			}
		});
		
		frmSBIOutOfTime.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(lvSBIOutOfTime.getVisibility() == View.GONE)
					lvSBIOutOfTime.setVisibility(View.VISIBLE);
				else
					lvSBIOutOfTime.setVisibility(View.GONE);
				
			}
		});
		
		frmSBIUsed.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(lvSBIUsed.getVisibility() == View.GONE)
					lvSBIUsed.setVisibility(View.VISIBLE);
				else
					lvSBIUsed.setVisibility(View.GONE);
				
			}
		}); 
		
		frmSBICancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(lvSBICancel.getVisibility() == View.GONE)
					lvSBICancel.setVisibility(View.VISIBLE);
				else
					lvSBICancel.setVisibility(View.GONE);
				
			}
		});
		
		getData();
		return view;
	}
	
	
	/** The system calls this only when creating the layout in a dialog. */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {	   
	    Dialog dialog = super.onCreateDialog(savedInstanceState);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    return dialog;
	}
	@Override
	public void onStart() {

		super.onStart();
	}
	 /*
		 Status:
		 0: Hết hạn sử dụng
		 1: Còn hạn sử dụng
		 2: Đã sử dụng
		 3: Hủy
    */
	int i=0;
	private void getData(){
		procBar.setVisibility(View.VISIBLE);
		//kết nối API lấy danh sách SBI
		new GetSBIList(mContext, SBIManageDialog.this, null, -2);
	}
	//call back cập nhật danh sách SBI vào listview
	public void loadData(ArrayList<SBIModel> list)
	{	
		procBar.setVisibility(View.GONE);
		if(list != null && list.size() > 0){
			txtSBITotal.setText(""+list.size());
			ArrayList<SBIModel> lstInTime = new ArrayList<SBIModel>();
			ArrayList<SBIModel> lstOutOfTime = new ArrayList<SBIModel>();
			ArrayList<SBIModel> lstUsed = new ArrayList<SBIModel>();
			ArrayList<SBIModel> lstCancel = new ArrayList<SBIModel>();
			for(int i=0; i< list.size(); i++){
				SBIModel item = list.get(i);
				if(item.getStatus() == 0)//SBI qua han
					lstOutOfTime.add(item);
				if(item.getStatus() == 1)//SBI con han
					lstInTime.add(item);
				if(item.getStatus() == 2)//SBI da su dung
					lstUsed.add(item);
				if(item.getStatus() == 3)//SBi da huy
					lstCancel.add(item);
			}
			lblInTimeTotal.setText(""+ lstInTime.size());
			lblOutOfTimeTotal.setText("" + lstOutOfTime.size());
			lblUsedTotal.setText("" + lstUsed.size());
			lblCancelTotal.setText("" + lstCancel.size());
			
			lvSBIInTime.setAdapter(new SBIAdapter(mContext, lstInTime, SBIManageDialog.this));
			lvSBIOutOfTime.setAdapter(new SBIAdapter(mContext, lstOutOfTime));
			lvSBIUsed.setAdapter(new SBIAdapter(mContext, lstUsed));
			lvSBICancel.setAdapter(new SBIAdapter(mContext, lstCancel));
		}
		
	}

	 private void onClickTracker(String lable){
		 try {
		} catch (Exception e) {

			Log.i("Book-Port FTTH New Tracker: ",e.getMessage());
		}

	 }
	
}
