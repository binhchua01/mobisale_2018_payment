package isc.fpt.fsale.model.upsell.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPromotionIPTVUpgradeModel implements Parcelable {

    @SerializedName("Money")
    @Expose
    private Integer money;
    @SerializedName("PrepaidMonth")
    @Expose
    private Integer prepaidMonth;
    @SerializedName("PromotionID")
    @Expose
    private Integer promotionID;
    @SerializedName("PromotionName")
    @Expose
    private String promotionName;
    @SerializedName("PromotionType")
    @Expose
    private Integer promotionType;

    protected GetPromotionIPTVUpgradeModel(Parcel in) {
        if (in.readByte() == 0) {
            money = null;
        } else {
            money = in.readInt();
        }
        if (in.readByte() == 0) {
            prepaidMonth = null;
        } else {
            prepaidMonth = in.readInt();
        }
        if (in.readByte() == 0) {
            promotionID = null;
        } else {
            promotionID = in.readInt();
        }
        promotionName = in.readString();
        if (in.readByte() == 0) {
            promotionType = null;
        } else {
            promotionType = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (money == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(money);
        }
        if (prepaidMonth == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(prepaidMonth);
        }
        if (promotionID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(promotionID);
        }
        dest.writeString(promotionName);
        if (promotionType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(promotionType);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetPromotionIPTVUpgradeModel> CREATOR = new Creator<GetPromotionIPTVUpgradeModel>() {
        @Override
        public GetPromotionIPTVUpgradeModel createFromParcel(Parcel in) {
            return new GetPromotionIPTVUpgradeModel(in);
        }

        @Override
        public GetPromotionIPTVUpgradeModel[] newArray(int size) {
            return new GetPromotionIPTVUpgradeModel[size];
        }
    };

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Integer getPrepaidMonth() {
        return prepaidMonth;
    }

    public void setPrepaidMonth(Integer prepaidMonth) {
        this.prepaidMonth = prepaidMonth;
    }

    public Integer getPromotionID() {
        return promotionID;
    }

    public void setPromotionID(Integer promotionID) {
        this.promotionID = promotionID;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public Integer getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Integer promotionType) {
        this.promotionType = promotionType;
    }

}
