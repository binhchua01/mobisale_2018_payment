package isc.fpt.fsale.services;

import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.Toast;

public class UpdateService extends Service {
    private static Timer timer = null;

    private Context mContext;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        mContext = this;
        startService();
    }

    private void startService() {
        if (timer == null) {
            timer = new Timer();
            timer.schedule(new MainTask(), Constants.TIMER_DEPLAY, Constants.TIMER_PERIOD);
        }
    }

    private class MainTask extends TimerTask {
        public void run() {
            toastHandler.sendEmptyMessage(0);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "ServiceType Stopped ...", Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("HandlerLeak")
    private final Handler toastHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (mContext != null) {
                Common.RegGCM(mContext, false, 0);
            }
        }
    };
}