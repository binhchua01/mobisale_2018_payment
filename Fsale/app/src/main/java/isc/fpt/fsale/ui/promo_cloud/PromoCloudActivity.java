package isc.fpt.fsale.ui.promo_cloud;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.camera319.GetPromoCloudOfNet;
import isc.fpt.fsale.model.camera319.PromoCloud;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.CLOUD_DETAIL;
import static isc.fpt.fsale.utils.Constants.CUSTYPE;
import static isc.fpt.fsale.utils.Constants.OBJECT_PROMOTION_CLOUD_DETAIL;
import static isc.fpt.fsale.utils.Constants.POSITION;
import static isc.fpt.fsale.utils.Constants.REGTYPE;

public class PromoCloudActivity extends BaseActivitySecond implements OnItemClickListener<PromoCloud> {
    private View loBack;
    private PromoCloudAdapter mAdapter;
    private List<PromoCloud> mList;
    private List<CloudDetail> mListCloudDetail;
    private int cusType, regType;
    private int qty = 0;
    int position = 0;

    @Override
    protected void onResume() {
        super.onResume();
        if (mListCloudDetail != null)
            new GetPromoCloudOfNet(this, regType, cusType, qty);
    }

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_camera_package;
    }

    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mListCloudDetail = bundle.getParcelableArrayList(CLOUD_DETAIL);
            for (CloudDetail item : mListCloudDetail) {
                qty += item.getQty();
                cusType = bundle.getInt(CUSTYPE);
                regType = bundle.getInt(REGTYPE);
            }
            TextView tvTitleToolbar = (TextView) findViewById(R.id.tv_title_toolbar);
            tvTitleToolbar.setText("Chọn câu lệnh khuyến mãi Cloud");
            RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_camera_package);
            mList = new ArrayList<>();
            mAdapter = new PromoCloudAdapter(this, mList, this);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    public void loadCloudPromotion(List<PromoCloud> mList) {
        this.mList.clear();
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(PromoCloud object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(OBJECT_PROMOTION_CLOUD_DETAIL, object);
        returnIntent.putExtra(POSITION, position);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
