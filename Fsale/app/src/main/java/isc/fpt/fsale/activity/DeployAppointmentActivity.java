package isc.fpt.fsale.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetAbility4Days;
import isc.fpt.fsale.action.GetAbility4Deployment;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.action.GetSubTeamID1;
import isc.fpt.fsale.action.UpdateDeployAppointment;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.ui.fragment.DatePickerDialog;
import isc.fpt.fsale.model.Ability4Deployment;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.SubTeamID1Model;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//màn hình hẹn triển khai
public class DeployAppointmentActivity extends BaseActivity {
    private Spinner spPartner, spSubTeam, spDate, spTimeZone;
    private WebView wvShowDeployAppointment;
    private RegistrationDetailModel mRegister;
    private String tagDeployAppointment;
    private LinearLayout frmDeployAppointmentTimeZone;
    private Button btnShowDeployAppointment, btnUpdate, btnShowList;

    public DeployAppointmentActivity() {
        super(R.string.lbl_screen_name_deploy_appointment);
    }

    public void startActivity(Intent intent) {
        if (!intent.getComponent().getClassName().equals(DeployAppointmentListActivity.class.getName()))
            finish();
        super.startActivity(intent);
    }

    public WebView getWvShowDeployAppointment() {
        return wvShowDeployAppointment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_deploy_appointment));
        setContentView(R.layout.activity_deploy_appointment);
        initView();
        initEventView();
        initSpinnerDate(null);
    }

    private void getDataIntent() {
        if (getIntent().getParcelableExtra(Constants.MODEL_REGISTER) != null) {
            mRegister = getIntent().getParcelableExtra(Constants.MODEL_REGISTER);
            tagDeployAppointment = getIntent().getStringExtra(Constants.TAG_DEPLOY_CAPACITY);
            if (tagDeployAppointment != null) {
                setTitle(R.string.title_activity_deploy_capacity_by_day);
                frmDeployAppointmentTimeZone.setVisibility(View.GONE);
                btnShowList.setVisibility(View.GONE);
                btnUpdate.setVisibility(View.GONE);
            }
        }
    }

    private void initView() {
        frmDeployAppointmentTimeZone = (LinearLayout)
                findViewById(R.id.frm_deploy_appointment_time_zone);
        spPartner = (Spinner) this.findViewById(R.id.sp_deploy_appointment_partner);
        spSubTeam = (Spinner) this.findViewById(R.id.sp_deploy_appointment_sub_team);
        spDate = (Spinner) this.findViewById(R.id.sp_deploy_appointment_date);
        spTimeZone = (Spinner) this.findViewById(R.id.sp_deploy_appointment_time_zone);
        btnShowDeployAppointment = (Button) findViewById(R.id.btn_show_deploy_appointment);
        wvShowDeployAppointment = (WebView) findViewById(R.id.wv_show_deploy_appointment);
        btnUpdate = (Button) this.findViewById(R.id.btn_update);
        btnShowList = (Button) findViewById(R.id.btn_show_list);
    }

    private void initEventView() {
        btnShowDeployAppointment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int partnerID = 0;
                int subTeam = 0;
                String appointmentDate = "";
                if (spPartner.getAdapter() != null && spPartner.getAdapter().getCount() > 0)
                    partnerID = ((KeyValuePairModel) spPartner.getSelectedItem()).getID();
                if (spSubTeam.getAdapter() != null && spSubTeam.getAdapter().getCount() > 0)
                    subTeam = ((KeyValuePairModel) spSubTeam.getSelectedItem()).getID();
                if (spDate.getAdapter() != null && spDate.getAdapter().getCount() > 0)
                    appointmentDate = ((KeyValuePairModel) spDate.getSelectedItem()).getsID();
                //kết nối api lấy Xem NLTK
                if (partnerID != 0 && subTeam != 0 && mRegister != null && !appointmentDate.equals("")) {
                    new GetAbility4Days(DeployAppointmentActivity.this, String.valueOf(partnerID),
                            String.valueOf(subTeam), appointmentDate, mRegister.getRegCode());
                }
            }
        });
        spDate.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                if (item.getsID().equals("")) {
                    showTimePickerDialog();
                } else {
                    getTimeZone();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });
        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                if (checkForUpdate())
                    update();
                else
                    Common.alertDialog(getString(R.string.msg_not_fount_partner_and_subteam),
                            DeployAppointmentActivity.this);
            }
        });
        btnShowList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                showDeployAppointmentList();
            }
        });

        spTimeZone.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                KeyValuePairModel item = (KeyValuePairModel) spTimeZone.getSelectedItem();
                if (item.getAbility() == 0) {
                    Common.alertDialog(
                            "Múi giờ dự kiến full năng lực, vui lòng tư vấn KH sang múi giờ màu xanh",
                            DeployAppointmentActivity.this
                    );
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
        getDataIntent();
        getData();
    }

    public void setDataDeployAppointment(String data) {
        wvShowDeployAppointment.setVisibility(View.VISIBLE);
        wvShowDeployAppointment.loadDataWithBaseURL(null, data,
                "text/html", "utf-8", null);
    }

    private boolean checkForUpdate() {
        int partnerID = 0;
        int subTeam = 0;
        if (spPartner.getAdapter() != null && spPartner.getAdapter().getCount() > 0)
            partnerID = ((KeyValuePairModel) spPartner.getSelectedItem()).getID();
        if (spSubTeam.getAdapter() != null && spSubTeam.getAdapter().getCount() > 0)
            subTeam = ((KeyValuePairModel) spSubTeam.getSelectedItem()).getID();
        return partnerID > 0 && subTeam > 0;
    }

    private void initSpinnerDate(KeyValuePairModel topItem) {
        final Calendar calCurrentDay = Calendar.getInstance(), calTomorrow = Calendar.getInstance();
        String today, tomorrow;
        today = Common.getSimpleDateFormat(calCurrentDay, Constants.DATE_FORMAT);
        calTomorrow.add(Calendar.DAY_OF_MONTH, 1);
        tomorrow = Common.getSimpleDateFormat(calTomorrow, Constants.DATE_FORMAT);
        ArrayList<KeyValuePairModel> lst = new ArrayList<>();
        if (topItem != null)
            lst.add(topItem);
        lst.add(new KeyValuePairModel(today, "Hôm nay"));
        lst.add(new KeyValuePairModel(tomorrow, "Ngày mai"));
        lst.add(new KeyValuePairModel("", "Khác..."));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lst, Gravity.CENTER);
        spDate.setAdapter(adapter);
    }

    public void initSpinnerTimeZone(ArrayList<Ability4Deployment> list) {
        ArrayList<KeyValuePairModel> lst = new ArrayList<>();
        //add item default
        lst.add(new KeyValuePairModel("-1", "Chọn múi giờ", -1, -1));
        for (Ability4Deployment item : list) {
            lst.add(new KeyValuePairModel(item.getTimeZone(), item.getDesc(),
                    Color.parseColor(item.getColorCode()), item.getAbilityDesc()));
        }
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lst, Gravity.CENTER);
        spTimeZone.setAdapter(adapter);
    }

    private void showTimePickerDialog() {
        DialogFragment newFragment;
        if (tagDeployAppointment != null) {
            newFragment = new DatePickerDialog(true);
        } else {
            newFragment = new DatePickerDialog();
        }
        newFragment.setCancelable(false);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void showDeployAppointmentList() {
        Intent intent = new Intent(this, DeployAppointmentListActivity.class);
        intent.putExtra(Constants.MODEL_REGISTER, mRegister);
        startActivity(intent);
    }

    public void updateDateSpinner(int newYear, int newMonth, int newDay) {
        if (spDate != null) {
            if (newYear > 0) {
                String customDateDesc = "Ngày " + newDay + " tháng " + (newMonth + 1) + " năm " + (newYear);
                Calendar date = Calendar.getInstance();
                date.set(newYear, newMonth, newDay);
                String customDate = Common.getSimpleDateFormat(date, Constants.DATE_FORMAT);
                initSpinnerDate(new KeyValuePairModel(customDate, customDateDesc));
            } else {
                spDate.setSelection(0, true);
            }
        }
    }

    private void getData() {
        if (mRegister != null) {
            //kết nối api lấy danh sách Đối tác và Tổ con
            new GetSubTeamID1(this, Constants.USERNAME, mRegister.getRegCode());
        }
    }

    //tải dữ liệu danh sách Đối tác và Tổ con.
    public void loadData(SubTeamID1Model subTeamID1) {
        if (subTeamID1 != null && (subTeamID1.getPartnerList().size() > 0 ||
                subTeamID1.getSupTeamList().size() > 0)) {
            if (subTeamID1.getPartnerList().size() > 0) {
                spPartner.setAdapter(new KeyValuePairAdapter(this,
                        R.layout.my_spinner_style, subTeamID1.getPartnerList(), Gravity.CENTER));
            }
            if (subTeamID1.getSupTeamList().size() > 0) {
                spSubTeam.setAdapter(new KeyValuePairAdapter(this,
                        R.layout.my_spinner_style, subTeamID1.getSupTeamList(), Gravity.CENTER));
            }
            //load time zone when have list partner and sub team
            getTimeZone();
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Thông Báo")
                    .setMessage(getString(R.string.msg_not_fount_partner_and_subteam))
                    .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    private void update() {
        int partnerID = 0;
        int subTeam = 0;
        int abilityDesc = -1;
        String appointmentDate = "", timeZone = "";
        if (spPartner.getAdapter() != null && spPartner.getAdapter().getCount() > 0)
            partnerID = ((KeyValuePairModel) spPartner.getSelectedItem()).getID();
        if (spSubTeam.getAdapter() != null && spSubTeam.getAdapter().getCount() > 0)
            subTeam = ((KeyValuePairModel) spSubTeam.getSelectedItem()).getID();
        if (spTimeZone.getAdapter() != null && spTimeZone.getAdapter().getCount() > 0)
            timeZone = ((KeyValuePairModel) spTimeZone.getSelectedItem()).getsID();
        if (spDate.getAdapter() != null && spDate.getAdapter().getCount() > 0)
            appointmentDate = ((KeyValuePairModel) spDate.getSelectedItem()).getsID();
        if (spTimeZone.getAdapter() != null && spTimeZone.getAdapter().getCount() > 0)
            abilityDesc = ((KeyValuePairModel) spTimeZone.getSelectedItem()).getAbility();


        //cập nhật Hẹn triển khai
        if (mRegister != null) {
            if (abilityDesc < 0) {
                //không chọn múi giờ
                Common.alertDialog(
                        "Vui lòng chọn múi giờ hẹn triển khai",
                        DeployAppointmentActivity.this
                );
            } else if (abilityDesc == 0) {
                //chọn múi giờ có ability = 0
                final String finalAppointmentDate = appointmentDate;
                final int finalPartnerID = partnerID;
                final String finalTimeZone = timeZone;
                final int finalSubTeam = subTeam;
                final int finalAbilityDesc = abilityDesc;
                new AlertDialog.Builder(this).setTitle("Thông Báo")
                        .setMessage("Sẽ hẹn lại nếu không đáp ứng múi giờ này")
                        .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new UpdateDeployAppointment(DeployAppointmentActivity.this, Constants.USERNAME, finalPartnerID,
                                        finalSubTeam, finalTimeZone, finalAppointmentDate, mRegister.getRegCode(), finalAbilityDesc);
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            } else {
                new UpdateDeployAppointment(this, Constants.USERNAME, partnerID,
                        subTeam, timeZone, appointmentDate, mRegister.getRegCode(), abilityDesc);
            }
        }
    }

    public void loadResult(final List<UpdResultModel> lst) {
        if (lst.size() > 0) {
            new AlertDialog.Builder(this).setTitle("Thông Báo").setMessage(lst.get(0).getResult())
                    .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (lst.get(0).getResultID() > 0) {
                                // kết nối API lấy thông tin chi tiết thông tin khách hàng
                                new GetRegistrationDetail(DeployAppointmentActivity.this,
                                        Constants.USERNAME, mRegister.getID());
                            }
                        }
                    }).setCancelable(false).create().show();
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public void onBackPressed() {
        // kết nối API lấy thông tin chi tiết thông tin khách hàng
        new GetRegistrationDetail(this, Constants.USERNAME, mRegister.getID());
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    private void getTimeZone() {
        int partnerID = 0;
        int subTeam = 0;
        String appointmentDate = "";
        if (spPartner.getAdapter() != null && spPartner.getAdapter().getCount() > 0)
            partnerID = ((KeyValuePairModel) spPartner.getSelectedItem()).getID();
        if (spSubTeam.getAdapter() != null && spSubTeam.getAdapter().getCount() > 0)
            subTeam = ((KeyValuePairModel) spSubTeam.getSelectedItem()).getID();
        if (spDate.getAdapter() != null && spDate.getAdapter().getCount() > 0)
            appointmentDate = ((KeyValuePairModel) spDate.getSelectedItem()).getsID();
        //call api get time zone
        if (partnerID != 0 && subTeam != 0) {
            new GetAbility4Deployment(this, this,
                    partnerID, subTeam, appointmentDate, mRegister.getRegCode());
        }
    }
}