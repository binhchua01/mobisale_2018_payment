package isc.fpt.fsale.activity.upsell.history;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.upsell.GetObjUpgradeTransHistory;
import isc.fpt.fsale.adapter.upsell.ObjProgramHistoryAdapter;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeTransHistoryModel;
import isc.fpt.fsale.ui.base.BaseActivitySecond;

public class HistoryUpsellActivity extends BaseActivitySecond {
    TextView mTittle;
    RecyclerView mRecyclerHistory;
    ObjProgramHistoryAdapter adapter;
    private View loBack;
    private List<ObjUpgradeTransHistoryModel> mHistory;
    String mProgramID, mObjID;

    boolean isLoading = false;
    int pageNum = 1;

    @Override
    protected void initEvent() {
        loBack.setOnClickListener(view -> onBackPressed());
        initScrollListener();
    }



    @Override
    protected void onResume() {
        super.onResume();
        pageNum = 1;
        String[] arrValue2 = {mProgramID, mObjID, "10", String.valueOf(pageNum)};
        new GetObjUpgradeTransHistory(this, arrValue2, mList -> {
            if (mList != null && mList.size() > 0) {
                if (mList.size() < 10) {
                    isLoading = true;
                } else {
                    pageNum = pageNum + 1;
                    isLoading = false;
                }
                mHistory = mList;
                initRecyclerView();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_history_upsell;
    }

    private void initRecyclerView() {
        adapter = new ObjProgramHistoryAdapter(this, mHistory);
        mRecyclerHistory.setAdapter(adapter);
        mRecyclerHistory.setLayoutManager(new LinearLayoutManager(this));
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initView() {
        loBack = findViewById(R.id.btn_back);
        mTittle = (TextView) findViewById(R.id.tv_title_toolbar);
        mRecyclerHistory = (RecyclerView) findViewById(R.id.recycler_history);
        mTittle.setText("HISTORY UPGRADE");
        Intent iin = getIntent();
        Bundle b = iin.getExtras();

        if (b != null) {
            mProgramID = (String) b.get("PROGRAM_UPGRADE_ID");
            mObjID = (String) b.get("OBJ_ID");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isLoading = false;
    }

    private void initScrollListener() {
        mRecyclerHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null
                            && linearLayoutManager.findLastCompletelyVisibleItemPosition() == mHistory.size() - 1) {
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore() {
        new Handler().postDelayed(() -> {
            String[] arrValue2 = {mProgramID, mObjID, "10", String.valueOf(pageNum)};
            new GetObjUpgradeTransHistory(this, arrValue2, mList -> {
                if (mList != null && mList.size() > 0) {
                    if (mList.size() < 10) {
                        isLoading = true;
                    } else {
                        pageNum = pageNum + 1;
                        isLoading = false;
                    }
                    mHistory.addAll(mList);
                    adapter.notifyDataSetChanged();
                }
            });
        }, 200);
    }
}
