package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haulc3 on 18,April,2019
 */
public class DeviceConfirm {
    @SerializedName("DeviceName")
    @Expose
    private String deviceName;
    @SerializedName("MAC")
    @Expose
    private String mAC;
    @SerializedName("MACID")
    @Expose
    private Integer mACID;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMAC() {
        return mAC;
    }

    public void setMAC(String mAC) {
        this.mAC = mAC;
    }

    public Integer getMACID() {
        return mACID;
    }

    public void setMACID(Integer mACID) {
        this.mACID = mACID;
    }
}
