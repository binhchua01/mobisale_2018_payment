package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.callback.refreshListSelected;
import isc.fpt.fsale.model.DeviceConfirm;
import isc.fpt.fsale.model.ListDeviceConfirm;

/**
 * Created by haulc3 on 16,April,2019
 */
public class AdapterConfirmDevice extends RecyclerView.Adapter<AdapterConfirmDevice.SimpleViewHolder>
        implements refreshListSelected {
    private Context context;
    private ArrayList<ListDeviceConfirm> list;
    private CallbackDeviceConfirm callback;
    private ArrayList<DeviceConfirm> mListSelected;
    private int type;

    public AdapterConfirmDevice(Context context, ArrayList<ListDeviceConfirm> list,
                                CallbackDeviceConfirm callback, int type) {
        this.context = context;
        this.list = list;
        this.callback = callback;
        this.type = type;
        mListSelected = new ArrayList<>();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_confirm_device, parent, false);
        return new SimpleViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        int id = position + 1;
        holder.tvId.setText(String.valueOf(id));
        holder.tvReceiveTime.setText(list.get(position).getTime());
        holder.tvDeviceName.setText(list.get(position).getListDevice().get(0).getDeviceName());
        holder.tvMac.setText(list.get(position).getListDevice().get(0).getMAC());
        if (type == 0) {
            holder.cbConfirmDevice.setVisibility(View.GONE);
            holder.tvTimeConfirm.setText(context.getResources().getString(R.string.label_time_receive));
        } else {
            holder.tvTimeConfirm.setText(context.getResources().getString(R.string.label_time_transfer));
            holder.cbConfirmDevice.setVisibility(View.VISIBLE);
            if (list.get(position).isChecked()) {
                holder.cbConfirmDevice.setChecked(true);
            } else {
                holder.cbConfirmDevice.setChecked(false);
            }

            holder.loParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!list.get(position).isChecked()) {
                        //thêm item vào ds lựa chọn
                        mListSelected.add(list.get(position).getListDevice().get(0));
                    } else {
                        //xóa item khỏi danh sách lựa chọn
                        mListSelected.remove(list.get(position).getListDevice().get(0));
                    }
                    //set trạng thái check box
                    list.get(position).setChecked(!list.get(position).isChecked());

                    //callback về activity
                    callback.deviceConfirm(mListSelected);
                    //làm mới lại danh sách
                    notifyDataChanged(list);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void refresh() {
        mListSelected = new ArrayList<>();
    }

    public refreshListSelected getRefresh() {
        return AdapterConfirmDevice.this;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvId, tvDeviceName, tvMac, tvReceiveTime, tvTimeConfirm;
        CheckBox cbConfirmDevice;
        LinearLayout loParent;

        SimpleViewHolder(View itemView) {
            super(itemView);
            tvId = (TextView) itemView.findViewById(R.id.text_view_identity);
            tvDeviceName = (TextView) itemView.findViewById(R.id.text_view_device_name);
            tvMac = (TextView) itemView.findViewById(R.id.text_view_mac);
            tvReceiveTime = (TextView) itemView.findViewById(R.id.text_view_receive_time);
            cbConfirmDevice = (CheckBox) itemView.findViewById(R.id.button_check);
            loParent = (LinearLayout) itemView.findViewById(R.id.layout_parent);
            tvTimeConfirm = (TextView) itemView.findViewById(R.id.tv_time_confirm);
        }
    }

    public interface CallbackDeviceConfirm {
        void deviceConfirm(ArrayList<DeviceConfirm> listDevice);
    }

    public void selectAllItem(boolean isSelectAll) {//isSelectAll = true select all, isSelectAll = false, un select all=
        mListSelected.clear();
        if (isSelectAll) {
            //set checked all checkbox
            for (ListDeviceConfirm item : list) {
                item.setChecked(true);
                //thêm item vào ds lựa chọn
                mListSelected.add(item.getListDevice().get(0));
            }
        } else {
            //set checked all checkbox
            for (ListDeviceConfirm item : list) {
                item.setChecked(false);
            }
        }
        notifyDataChanged(list);
        //callback về activity
        callback.deviceConfirm(mListSelected);
    }

    public void notifyDataChanged(ArrayList<ListDeviceConfirm> mList) {
        this.list = mList;
        notifyDataSetChanged();
    }
}

