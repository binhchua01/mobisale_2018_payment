package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ListPrechecklistModel;
import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PrechecklistAdapter  extends BaseAdapter {

	private ArrayList<ListPrechecklistModel> mList;	
	private Context mContext;
	
	public PrechecklistAdapter(Context context, ArrayList<ListPrechecklistModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListPrechecklistModel precheckList = mList.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.list_row_prechecklist, null);
		}
		
		// add by GiauTQ 01-05-2014
		TextView txtRowNumber = (TextView) convertView.findViewById(R.id.txt_row_number);
		txtRowNumber.setText(precheckList.getRowNumber());
		
		TextView txtContract = (TextView) convertView.findViewById(R.id.txt_contract);
		txtContract.setText(precheckList.getContract());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(precheckList.getContract().trim()))
		{
			txtContract.setVisibility(View.GONE);
		}
		
		TextView txtFullName = (TextView) convertView.findViewById(R.id.tv_full_name);
		txtFullName.setText(precheckList.getFullName());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(precheckList.getFullName().trim()))
		{
			txtFullName.setVisibility(View.GONE);
		}
		
		TextView txtAddress = (TextView) convertView.findViewById(R.id.txt_address);
		txtAddress.setText(precheckList.getAddress());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(precheckList.getAddress().trim()))
		{
			txtAddress.setVisibility(View.GONE);
		}		
		
		TextView txtDescription = (TextView) convertView.findViewById(R.id.txt_description);
		TextView txtTitleDescription = (TextView) convertView.findViewById(R.id.txt_title_description);
		txtDescription.setText(precheckList.getDescription());
		// Ẩn đi khi không có giá trị
		if(precheckList.getDescription() == null || precheckList.getDescription() == "")
		{
			txtDescription.setVisibility(View.GONE);
			txtTitleDescription.setVisibility(View.GONE);
		}
		
		TextView txtSubDescription = (TextView) convertView.findViewById(R.id.txt_sub_description);
		TextView txtTitleSubDescription = (TextView) convertView.findViewById(R.id.txt_title_sub_description);
		txtSubDescription.setText(precheckList.getSupDescription());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(precheckList.getSupDescription().trim()))
		{
			txtSubDescription.setVisibility(View.GONE);
			txtTitleSubDescription.setVisibility(View.GONE);
		}

		
		TextView txtCreateDate = (TextView) convertView.findViewById(R.id.txt_create_date);
		TextView txtTitleCreateDate = (TextView) convertView.findViewById(R.id.txt_title_create_date);
		txtCreateDate.setText(precheckList.getCreateDate());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(precheckList.getCreateDate().trim()))
		{
			txtCreateDate.setVisibility(View.GONE);
			txtTitleCreateDate.setVisibility(View.GONE);
		}

		
		TextView txtUpdateBy = (TextView) convertView.findViewById(R.id.txt_update_by);
		TextView txtTitleUpdateBy = (TextView) convertView.findViewById(R.id.txt_title_update_by);
		txtUpdateBy.setText(precheckList.getUpdateBy());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(precheckList.getUpdateBy().trim()))
		{
			txtUpdateBy.setVisibility(View.GONE);
			txtTitleUpdateBy.setVisibility(View.GONE);
		}

		
		TextView txtUpdateDate = (TextView) convertView.findViewById(R.id.txt_update_date);
		TextView txtTitleUpdateDate = (TextView) convertView.findViewById(R.id.txt_title_update_date);
		txtUpdateDate.setText(precheckList.getUpdateDate());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(precheckList.getUpdateDate().trim()))
		{
			txtUpdateDate.setVisibility(View.GONE);
			txtTitleUpdateDate.setVisibility(View.GONE);
		}

				
		//
		return convertView;
	}

}
