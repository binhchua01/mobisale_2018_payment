package isc.fpt.fsale.ui.fragment;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionComboModel;
import isc.fpt.fsale.utils.Common;
import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentRegisterContractCombo extends Fragment {

	//private Context mContext;
	
	
	public static FragmentRegisterContractCombo newInstance() {
		FragmentRegisterContractCombo fragment = new FragmentRegisterContractCombo();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentRegisterContractCombo() {
		
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_register_contract_combo, container, false);
		try {
			Common.setupUI(getActivity(), rootView);
		} catch (Exception e) {

			e.printStackTrace();
			Common.alertDialog("onCreateView: " + e.getMessage(), getActivity());
		}
		return rootView;
	}
	
    
    public void updatePromotionCombo(ArrayList<PromotionComboModel> lst){
    	
    }

	@Override
	public void onStart() {
		super.onStart();
	}
	
    @Override
    public void onDestroyView() {
        super.onDestroyView();        
    }
  
}
