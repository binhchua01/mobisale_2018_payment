package isc.fpt.fsale.adapter.upsell;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.danh32.fontify.Button;
import com.danh32.fontify.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.upsell.ResendShortlink;
import isc.fpt.fsale.activity.upsell.create_upgrade.CreateInfoUpgradeActicity;
import isc.fpt.fsale.callback.upsell.OnItemListenerUpsell;
import isc.fpt.fsale.model.MacOTT;
import isc.fpt.fsale.model.upsell.response.ObjUpgradeTransListModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class UpsellcareAdapter extends RecyclerView.Adapter<UpsellcareAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<ObjUpgradeTransListModel> list;
    private OnItemListenerUpsell mListener;




    public UpsellcareAdapter(Context mContext, List<ObjUpgradeTransListModel> mList, OnItemListenerUpsell mListener) {
        this.mContext = mContext;
        this.list = mList;
        this.mListener = mListener;
    }

    @Override
    public UpsellcareAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_recycler_upsell_care, parent, false);
            final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
            return mViewHolder;

    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bindView(list.get(position), position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class SimpleViewHolder  extends RecyclerView.ViewHolder {
        TextView mContracts, mName, mphone, mAddress, mProgram, mStatus, mMoneyUpgrade, mDayCare, mNumberPage, mKMNet, mKMPaytv, mMoneyTT;
        Button mBtnResend, mRecare;

        public SimpleViewHolder(View itemView) {
            super(itemView);

            mNumberPage = itemView.findViewById(R.id.txt_number_item);
            mContracts = itemView.findViewById(R.id.txt_number_contract);
            mName = itemView.findViewById(R.id.txt_name);
            mphone = itemView.findViewById(R.id.txt_phone);
            mAddress = itemView.findViewById(R.id.txt_address);
            mProgram = itemView.findViewById(R.id.txt_program);
            mStatus = itemView.findViewById(R.id.txt_status);
            mMoneyUpgrade = itemView.findViewById(R.id.txt_money_upgrade);
            mDayCare = itemView.findViewById(R.id.txt_day_care);
            mBtnResend = itemView.findViewById(R.id.btn_resend_shortlink);
            mRecare = itemView.findViewById(R.id.btn_recare);

            //update ver
            mKMNet = itemView.findViewById(R.id.txt_km_net);
            mKMPaytv = itemView.findViewById(R.id.txt_km_paytv);
            mMoneyTT = itemView.findViewById(R.id.txt_money_tt);
        }

        @SuppressLint("SetTextI18n")
        public void bindView(final ObjUpgradeTransListModel model, final int position) {
            mNumberPage.setText(position + 1 + "");
            mContracts.setText(model.getContract());
            mName.setText(model.getFullName());
            mphone.setText(model.getPhone());
            mAddress.setText(model.getAddress());
            mProgram.setText(model.getProgramUpgradeName());
            mStatus.setText(model.getUpgradeStatusName());
            mMoneyUpgrade.setText(Common.formatMoney(model.getMoneyUpgrade().toString()));
            mDayCare.setText(model.getCreatedDate());
            mKMNet.setText(model.getPromotionNetName());
            mKMPaytv.setText(model.getPromotionIPTVName());
            mMoneyTT.setText(Common.formatMoney(model.getTotalMoney().toString()));
            if (model.getIsResendShortLink() != 0) {
                mBtnResend.setVisibility(View.VISIBLE);
                mBtnResend.setOnClickListener(v -> {
                    String[] arrValue = {model.getObjID().toString(), model.getProgramUpgradeID().toString(), Constants.USERNAME, model.getPhone()};
                    new ResendShortlink(mContext, arrValue);
                });
            } else {
                mBtnResend.setVisibility(View.GONE);
            }

            if(model.getIsRecare() != 0){
                mRecare.setVisibility(View.VISIBLE);
                mRecare.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, CreateInfoUpgradeActicity.class);
                    intent.putExtra("OBJ_ID", model.getObjID().toString());
                    intent.putExtra("CONTRACT", model.getContract());
                    intent.putExtra("PROGRAM", model.getProgramUpgradeID().toString());
                    mContext.startActivity(intent);
                });
            }
            else {
                mRecare.setVisibility(View.GONE);
            }
        }
    }

    public void notifyData(List<ObjUpgradeTransListModel> mList) {
        this.list = mList;
        notifyDataSetChanged();
    }
}
