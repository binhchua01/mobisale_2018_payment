package isc.fpt.fsale.ui.fragment;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.action.GetListGiftBox;
import isc.fpt.fsale.action.UpdateRegistrationContract;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.adapter.SpGiftBoxAdapter;
import isc.fpt.fsale.model.GiftBox;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.camera319.Camera2Total;
import isc.fpt.fsale.ui.callback.OnLoadTotalIntenet;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.ui.maxytv.model.MaxyTotal;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class FragmentRegisterContractTotal extends Fragment {
    private Context mContext;
    private TextView lblIPTVTotal, lblInternetTotal, lblDeviceTotal, lblTotal, lblMaxyTotal, lblCameraTotal;
    private RegistrationDetailModel mRegister;
    private ObjectDetailModel mObject;
    private Spinner spGift;
    private Button btnUpdate;
    private List<GiftBox> mListGiftBox;
    private SpGiftBoxAdapter mAdapterGiftBox;
    private MaxyTotal mMaxyTotal;
    private Camera2Total mCameraTotal;


    public static FragmentRegisterContractTotal newInstance() {
        FragmentRegisterContractTotal fragment = new FragmentRegisterContractTotal();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentRegisterContractTotal() {
    }

    public TextView getLblTotal() {
        return lblTotal;
    }

    public void setLblTotal(TextView lblTotal) {
        this.lblTotal = lblTotal;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_contract_total, container, false);
        try {
            Common.setupUI(getActivity(), rootView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initView(rootView);
        initEventView();
        getRegisterFromActivity();
        setSpGiftBox();
        return rootView;
    }

    private void setSpGiftBox() {
        mListGiftBox = new ArrayList<>();
        //init data to spinner device type confirm
        mAdapterGiftBox = new SpGiftBoxAdapter(mContext, R.layout.item_spinner,
                R.id.text_view_0, (ArrayList<GiftBox>) this.mListGiftBox);
        spGift.setAdapter(mAdapterGiftBox);
        spGift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                GiftBox mGiftBox = mAdapterGiftBox.getItem(position);
                if (mGiftBox != null) {
                    mRegister.setGiftID(mGiftBox.getGiftID());
                    mRegister.setGiftName(mGiftBox.getGiftName());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void loadDataSpGiftBox(List<GiftBox> mListGiftBox) {
        this.mListGiftBox = mListGiftBox;
        mAdapterGiftBox.notifyData(this.mListGiftBox);
        //load old data
        if (mRegister != null) {//cập nhật PDK ban them
            if (FragmentRegisterContractIPTV.isBind == 0) {//load lại quà tặng của PDK
                spGift.setSelection(Common.getIndex(spGift, mRegister.getGiftID()), true);
                FragmentRegisterContractIPTV.isBind = 2;
            } else if (FragmentRegisterContractIPTV.isBind == 1) {//user chọn lại CLKM
                spGift.setSelection(Common.getIndex(spGift, 0), true);
                FragmentRegisterContractIPTV.isBind = 2;
            }
        } else {//Tạo PDK
            if (FragmentRegisterContractIPTV.isBind == 1) {//user chọn lại CLKM
                spGift.setSelection(Common.getIndex(spGift, 0), true);
                FragmentRegisterContractIPTV.isBind = 2;
            }
        }
    }

    private JSONObject objGetListGiftBox() {
        JSONObject jsonObject = new JSONObject();
        //get service type
        JSONArray jsonArray = new JSONArray();
        if (mRegister.getPromotionID() != 0) {
            jsonArray.put(1);//internet
        }

        if (mRegister.getListDevice() != null && mRegister.getListDevice().size() != 0) {
            jsonArray.put(2);//thiết bị lẻ
        }

        if (mRegister.getListDeviceOTT() != null && mRegister.getListDeviceOTT().size() != 0) {
            jsonArray.put(3);//fpt play box
        }

        if (mRegister.getIPTVPromotionID() != 0) {
            jsonArray.put(4);//iptv
        }

//        if(mRegister.getObjectMaxy() != null){
//            jsonArray.put(5);//maxy
//        }

        try {
            jsonObject.put("ServiceType", jsonArray);
            jsonObject.put("BranchCode", Constants.BRANCH_CODE);
            jsonObject.put("RegType", "1");//0 - bán mới, 1 - bán thêm
            jsonObject.put("NetPromotionID", mRegister.getPromotionID());
            jsonObject.put("IPTVPromotionID", mRegister.getIPTVPromotionID());
            jsonObject.put("IPTVPromotionIDBoxOrder", mRegister.getIPTVPromotionIDBoxOrder());
            jsonObject.put("IPTVPromotionType", mRegister.getIPTVPromotionType());
            if (mObject != null) {
                jsonObject.put("LocalType", mObject.getLocalType());
            } else {
                jsonObject.put("LocalType", mRegister.getLocalType());
            }
            jsonObject.put("LocationID", Constants.LOCATION_ID);
            jsonObject.put("UserName", Constants.USERNAME);
            if (mObject != null) {
                jsonObject.put("CusTypeDetail", mObject.getCusTypeDetail());
            } else {
                jsonObject.put("CusTypeDetail", mRegister.getCusTypeDetail());
            }
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initEventView() {
        btnUpdate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmUpdate();
            }
        });
    }

    private void initView(View rootView) {
        mContext = getActivity();
        lblMaxyTotal = rootView.findViewById(R.id.lbl_maxy_tv_total);
//        lblIPTVTotal = (TextView) rootView.findViewById(R.id.lbl_ip_tv_total);
        lblInternetTotal = (TextView) rootView.findViewById(R.id.lbl_internet_total);
        lblDeviceTotal = (TextView) rootView.findViewById(R.id.lbl_device_total);
        lblCameraTotal = (TextView) rootView.findViewById(R.id.lbl_camera_total);
        lblTotal = (TextView) rootView.findViewById(R.id.lbl_total);
        spGift = (Spinner) rootView.findViewById(R.id.sp_gift);
        btnUpdate = (Button) rootView.findViewById(R.id.btn_update);
    }

    public void getRegisterFromActivity() {
        if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
            RegisterContractActivity activity = (RegisterContractActivity) mContext;
            mObject = activity.getObject();
            mRegister = activity.getRegister();
        }
        loadData();
    }

    @SuppressLint("SetTextI18n")
    private void loadData() {
        if (mRegister != null) {
//            lblIPTVTotal.setText(Common.formatNumber(mRegister.getIPTVTotal()));
            lblInternetTotal.setText(Common.formatNumber(mRegister.getInternetTotal()));
            lblDeviceTotal.setText(Common.formatNumber(mRegister.getDeviceTotal()));
            lblMaxyTotal.setText(Common.formatNumber(mRegister.getObjectMaxy().getMaxyGeneral().getTotal()));
            lblTotal.setText(Common.formatNumber(getTotal()));
            new GetListGiftBox(mContext, FragmentRegisterContractTotal.this, objGetListGiftBox());
        }
    }

    private boolean checkForUpdate() {
        if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
            RegisterContractActivity activity = (RegisterContractActivity) mContext;
            return activity.checkForUpdate();
        }
        return false;
    }

    private void showConfirmRegisterExtraInternet() {
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Thông báo");
        builder.setMessage("Bạn đang đăng ký thêm gói dịch vụ Internet cho gói dịch vụ IPTV Only.")
                .setCancelable(false)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        update();
                    }
                }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void showConfirmUpdate() {
        if (checkForUpdate()) {
            AlertDialog.Builder builder;
            Dialog dialog;
            builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Cập nhật TTKH");
            builder.setMessage(mContext.getResources().getString(R.string.msg_confirm_update)).setCancelable(false)
                    .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (mRegister != null && mRegister.getContractServiceType() == 1
                                    && mRegister.getPromotionID() != 0
                                    || mObject != null && mObject.getServiceType() == 1
                                    && mRegister.getPromotionID() != 0) {
                                showConfirmRegisterExtraInternet();
                            } else {
                                update();
                            }
                        }
                    }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            dialog = builder.create();
            dialog.show();
        }
    }

    // gọi api cập nhật thông tin màn hình bán thêm
    private void update() {
        try {
            mRegister.setLocationID(((MyApp) mContext.getApplicationContext()).getLocationID());
            mRegister.setUserName(((MyApp) mContext.getApplicationContext()).getUserName());
            String strTotal = lblTotal.getText().toString();
            String str2 = strTotal.replace(".", "").trim();
            mRegister.setTotal(Integer.parseInt(str2));
            if (mObject != null) {
                mRegister.setLocalType(mObject.getLocalType());
                mRegister.setLocalTypeName(mObject.getLocalTypeName());
                mRegister.setObjID(mObject.getObjID());
            }
            new UpdateRegistrationContract(mContext, mRegister.toJsonUpdateRegisterContract());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadDevicePrice(int deviceTotal) {
        if (mRegister != null) {
            mRegister.setDeviceTotal(deviceTotal);
        }
        lblDeviceTotal.setText(Common.formatNumber(deviceTotal));
        lblTotal.setText(Common.formatNumber(getTotal()));
    }

    public void loadIpTvTotal(int ipTvDeviceTotal, int ipTvPrepaidTotal, int ipTvTotal) {
//        if (mRegister != null) {
//            mRegister.setIPTVPrepaid(ipTvPrepaidTotal);
//            mRegister.setIPTVDeviceTotal(ipTvDeviceTotal);
//            mRegister.setIPTVTotal(ipTvTotal);
//        }
//        lblIPTVTotal.setText(Common.formatNumber(ipTvTotal));
//        lblTotal.setText(Common.formatNumber(getTotal()));
    }

    private int getTotal() {
        String internetTotal = lblInternetTotal.getText().toString().replace(".", "").trim();
        String deviceTotal = lblDeviceTotal.getText().toString().replace(".", "").trim();
        String ipMaxyTotal = lblMaxyTotal.getText().toString().replace(".", "").trim();
        String cameraTotal = lblCameraTotal.getText().toString().replace(".", "").trim();
        return Integer.parseInt(internetTotal) + Integer.parseInt(deviceTotal) + Integer.parseInt(ipMaxyTotal) + Integer.parseInt(cameraTotal);
    }

    public void loadMaxyPrice(MaxyTotal maxyTotal) {
        if (mRegister != null) {
            this.mMaxyTotal = maxyTotal;
            mRegister.getObjectMaxy().getMaxyGeneral().setTotal(maxyTotal.getTotal());
            lblMaxyTotal.setText(Common.formatNumber(maxyTotal.getTotal()));
            lblTotal.setText(Common.formatNumber(getTotal()));
        }
    }

    public void loadCameraPrice(Camera2Total cameraTotal) {
        if (mRegister != null) {
            this.mCameraTotal = cameraTotal;
            mRegister.getObjectCameraOfNet().setCameraTotal(mCameraTotal.getTotal());
            lblCameraTotal.setText(Common.formatNumber(cameraTotal.getTotal()));
            lblTotal.setText(Common.formatNumber(getTotal()));
        }
    }
}
