package isc.fpt.fsale.callback.upsell;

import java.util.List;

import isc.fpt.fsale.model.upsell.response.GetPromotionNetUpgradeModel;

public interface PromotionNetUpgradeCallBack {
    void onGetSuccess(List<GetPromotionNetUpgradeModel> lst);
}
