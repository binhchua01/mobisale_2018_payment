package isc.fpt.fsale.ui.callback;

/**
 * Created by haulc3 on 20,July,2019
 */
public interface OnItemClickListenerV2<A, B> {
    void onItemClick(A object1, B object2);
}
