package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PaymentInformationResult implements Parcelable {
    //Declares variables
    private String Contract;
    private String CreateBy;
    private String FullName;
    private int PaidType;
    private String PaidTypeName;
    private String PaymentMessage;
    private int PaymentStatus;
    private String PhoneNumber;
    private String RegCode;
    private int TotalReceipts;
    private int TotalUnpaidReceipts;
    private CheckListRegistration checkListRegistration;

    private PaymentInformationResult(Parcel in) {
        Contract = in.readString();
        CreateBy = in.readString();
        FullName = in.readString();
        PaidType = in.readInt();
        PaidTypeName = in.readString();
        PaymentMessage = in.readString();
        PaymentStatus = in.readInt();
        PhoneNumber = in.readString();
        RegCode = in.readString();
        TotalReceipts = in.readInt();
        TotalUnpaidReceipts = in.readInt();
        checkListRegistration = in.readParcelable(CheckListRegistration.class.getClassLoader());
    }

    public static final Creator<PaymentInformationResult> CREATOR = new Creator<PaymentInformationResult>() {
        @Override
        public PaymentInformationResult createFromParcel(Parcel in) {
            return new PaymentInformationResult(in);
        }

        @Override
        public PaymentInformationResult[] newArray(int size) {
            return new PaymentInformationResult[size];
        }
    };

    public String getContract() {
        return Contract;
    }

    public void setContract(String contract) {
        Contract = contract;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String createBy) {
        CreateBy = createBy;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public int getPaidType() {
        return PaidType;
    }

    public void setPaidType(int paidType) {
        PaidType = paidType;
    }

    public String getPaidTypeName() {
        return PaidTypeName;
    }

    public void setPaidTypeName(String paidTypeName) {
        PaidTypeName = paidTypeName;
    }

    public String getPaymentMessage() {
        return PaymentMessage;
    }

    public void setPaymentMessage(String paymentMessage) {
        PaymentMessage = paymentMessage;
    }

    public int getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(int paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getRegCode() {
        return RegCode;
    }

    public void setRegCode(String regCode) {
        RegCode = regCode;
    }

    public int getTotalReceipts() {
        return TotalReceipts;
    }

    public void setTotalReceipts(int totalReceipts) {
        TotalReceipts = totalReceipts;
    }

    public int getTotalUnpaidReceipts() {
        return TotalUnpaidReceipts;
    }

    public void setTotalUnpaidReceipts(int totalUnpaidReceipts) {
        TotalUnpaidReceipts = totalUnpaidReceipts;
    }

    public CheckListRegistration getCheckListRegistration() {
        return checkListRegistration;
    }

    public void setCheckListRegistration(CheckListRegistration checkListRegistration) {
        this.checkListRegistration = checkListRegistration;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Contract);
        parcel.writeString(CreateBy);
        parcel.writeString(FullName);
        parcel.writeInt(PaidType);
        parcel.writeString(PaidTypeName);
        parcel.writeString(PaymentMessage);
        parcel.writeInt(PaymentStatus);
        parcel.writeString(PhoneNumber);
        parcel.writeString(RegCode);
        parcel.writeInt(TotalReceipts);
        parcel.writeInt(TotalUnpaidReceipts);
        parcel.writeParcelable(checkListRegistration, i);
    }
}
