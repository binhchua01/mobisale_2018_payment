package isc.fpt.fsale.action;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.VoucherTotal;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 28,October,2019
 */
public class GetDisCountRpVoucherTotal implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterStep4 fragment;

    public GetDisCountRpVoucherTotal(Context mContext, RegistrationDetailModel
            mRegistrationDetailModel, FragmentRegisterStep4 fragment) {
        this.mContext = mContext;
        this.fragment = fragment;
        String message = mContext.getResources().getString(R.string.msg_pd_apply_e_voucher);
        String GET_DISCOUNT_RP_VOUCHER_TOTAL = "GetDiscountRP_VoucherTotal";
        CallServiceTask service = new CallServiceTask(mContext, GET_DISCOUNT_RP_VOUCHER_TOTAL,
                mRegistrationDetailModel.toJSONObjectEVoucher(), Services.JSON_POST_OBJECT,
                message, GetDisCountRpVoucherTotal.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<VoucherTotal> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), VoucherTotal.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    if (!lst.get(0).isValid()) {
                        Common.alertDialog(lst.get(0).getMessage(), mContext);
                    }
                    fragment.loadVoucherDiscount(lst.get(0));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
