package isc.fpt.fsale.adapter;

import isc.fpt.fsale.activity.ListReportCreateObjectDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ReportCreateObjectTotalModel;
import isc.fpt.fsale.utils.Common;

import java.util.List;

import com.danh32.fontify.TextView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;


public class ReportCreateObjectTotalAdapter  extends BaseAdapter{
	private List<ReportCreateObjectTotalModel> mList;	
	private Context mContext;
	private String fromDate = null, toDate =  null;
	
	public ReportCreateObjectTotalAdapter(Context context, List<ReportCreateObjectTotalModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	public ReportCreateObjectTotalAdapter(Context context, List<ReportCreateObjectTotalModel> lst, String fromDate, String toDate){
		this.mContext = context;
		this.mList = lst;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ReportCreateObjectTotalModel item = mList.get(position);		

		if (convertView == null) {			
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_report_sbi_total, null);
		}
		try {
			TextView rowNumber = (TextView)convertView.findViewById(R.id.lbl_row_number);
			TextView saleName = (TextView)convertView.findViewById(R.id.lbl_report_sale_name);
			LinearLayout frmData = (LinearLayout)convertView.findViewById(R.id.frm_report_data);
			if(frmData.getChildCount()>0){
				frmData.removeAllViews();
			}			
			if(item != null){					
				rowNumber.setText(String.valueOf(item.getRowNumber()));
				saleName.setText(item.getName());				
				frmData.addView(initRowItem(item.getStatusName(), String.valueOf(item.getTotal()), item.getName(), item.getStatus()));				
			}				
		} catch (Exception e) {

			Common.alertDialog("ReportPotentialObjTotalAdapter.getView():" + e.getMessage(), mContext);
		}
		
		return convertView;
	}
	
	@SuppressLint("InflateParams")
	private LinearLayout initRowItem(String label, final String value, final String saleName, final int status){
		try {
			View viewChild = LayoutInflater.from(mContext).inflate( R.layout.sub_row_report_total, null );
			LinearLayout frmDataRow = (LinearLayout)viewChild.findViewById(R.id.frm_report_data_row);
			frmDataRow.setVisibility(View.VISIBLE);		
			TextView lblTitle = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_title);
			TextView lblTotal = (TextView)viewChild.findViewById(R.id.lbl_report_sbi_total_total);
			lblTitle.setText(label);
			lblTotal.setText(value);
			lblTotal.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub					
					try {
						int v1 = Integer.valueOf(value);
						if(v1 > 0)
							startAcitivity(saleName, status);	
					} catch (Exception e) {

						e.printStackTrace();
					}
									
				}
			});
			return frmDataRow;
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			return null;
		}		
	}
	
	private void startAcitivity(String saleName, int status){
		Intent intent = new Intent(mContext, ListReportCreateObjectDetailActivity.class);
		if(saleName != null && !saleName.equals(""))
			intent.putExtra("SALE_NAME", saleName);
		if(fromDate != null && !fromDate.equals(""))
			intent.putExtra("FROM_DATE", fromDate);
		if(toDate != null && !toDate.equals(""))
			intent.putExtra("TO_DATE", toDate);
		intent.putExtra("STATUS", status);
		mContext.startActivity(intent);
	}
}
