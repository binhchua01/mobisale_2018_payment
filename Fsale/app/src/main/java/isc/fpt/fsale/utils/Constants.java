package isc.fpt.fsale.utils;

import android.content.Context;

import com.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;

import isc.fpt.fsale.model.KeyValuePairModel;

public class Constants {
    public static final String REGISTRATION_EXTRA_OTT = "registration_extra_ott";
    public static final String REGISTRATION_INTERNET = "registration_internet";
    public static final String KEY_PAIR_OBJ = "KEY_PAIR";
    public static final String DISTRICT = "district";
    public static final String WARD = "ward";
    public static final String LOCAL_TYPE = "local_type";
    public static final String STREET_OR_CONDO = "street_or_condo";
    public static final String LIST_CAT_SERVICE = "list_cat_service";
    public static final String DISTRICT_ID = "district_id";
    public static final String WARD_ID = "ward_id";
    public static final String TYPE = "type";
    public static final String SERVICE_TYPE_LIST = "service_type_list";
    public static final String EXTRA_OTT_PACKAGE = "extra_ott_package";
    public static final String POSITION = "position";
    public static final String TAG_LOCAL_PATH_IDENTITY_CARD = "TAG_LOCAL_PATH_IDENTITY_CARD";
    public static final String TAG_INFO_IDENTITY_CARD = "TAG_INFO_IDENTITY_CARD";
    public static final String MODEL_REGISTER = "modelRegister";
    public static final String TAG_DEPLOY_CAPACITY = "TAG_DEPLOY_CAPACITY";
    public static final String POTENTIAL_OBJECT = "POTENTIAL_OBJECT";
    public static final String TAG_CONTRACT = "TAG_CONTRACT";
    public static final String SERVICE_TYPE_LIST_SELECTED = "service_type_list_selected";
    public static final String DEVICE_LIST_SELECTED = "device_list_selected";
    public static final String TAG_IMAGE_DOCUMENT_IDENTITY_CARD_SELECTED = "TAG_IMAGE_DOCUMENT_IDENTITY_CARD_SELECTED";
    public static final String TAG_UPDATE_REGISTRATION = "TAG_UPDATE_REGISTRATION";
    public static final String TAG_REGISTRATION_CREATE_FORM = "TAG_REGISTRATION_CREATE_FORM";
    public static final String TAG_VIEW_SIGNATURE = "TAG_VIEW_SIGNATURE";
    public static final String TAG_UPLOAD_IMAGE_TYPE = "TAG_UPLOAD_IMAGE_TYPE";
    public static final String TAG_LIST_IMAGE_DOCUMENT_SELECTED = "TAG_LIST_IMAGE_DOCUMENT_SELECTED";
    public static final String TAG_LIST_IMAGE_DOCUMENT = "TAG_LIST_IMAGE_DOCUMENT";
    public static final String OBJECT_CAMERA_DETAIL = "OBJECT_CAMERA_DETAIL";
    public static final String OBJECT_PROMOTION_CLOUD_DETAIL = "OBJECT_PROMOTION_CLOUD_DETAIL";
    public static final String CAMERA_DETAIL = "CAMERA_DETAIL";
    public static final String CLOUD_DETAIL = "CLOUD_DETAIL";
    public static final String LIST_CAMERA_DETAIL_SELECTED = "LIST_CAMERA_DETAIL_SELECTED";
    public static final String LIST_CLOUD_DETAIL_SELECTED = "LIST_CLOUD_DETAIL_SELECTED";
    public static final String LIST_PROMOTION_CLOUD_DETAIL_SELECTED = "LIST_PROMOTION_CLOUD_DETAIL_SELECTED";
    public static final String CAMERA_PROMOTION = "CAMERA_PROMOTION";
    public static final String PROMOTION_CLOUD_DETAIL = "PROMOTION_CLOUD_DETAIL";
    public static final String CAMERA_PROMOTION_SELECTED = "CAMERA_PROMOTION_SELECTED";
    public static final String COMBO = "COMBO";
    public static final String CUSTYPE = "CUSTYPE";
    public static final int COMBO_TYPE_YES = 1;
    public static final int COMBO_TYPE_NO = 0;
    public static final String CLASS_NAME = "CLASS_NAME";
    public static final String DEVICE = "device";
    public static final String VOUCHER = "VOUCHER";
    public static final String LIST_MAC_SELECTED = "LIST_MAC_SELECTED";
    public static final String OBJECT_MAXY_TYPE_DEPLOY = "OBJECT_MAXY_TYPE_DEPLOY";
    public static final String OBJECT_MAXY_SETUP_TYPE = "OBJECT_MAXY_SETUP_TYPE";
    public static String IBB_MEMBER_ID = "";
    public static String SECRET_KEY = "tamtm11";
    public static SlidingMenu SLIDING_MENU;
    public static String USERNAME = "";
    public static String DEBUG_USERNAME = "Oanh254";
    public static String BRANCH_CODE = "";
    public static String LOCATION_ID = "";
    public static String PORT_LOCATION_ID = "";
    public static Boolean IS_MANAGER = false;
    public static String MANAGER_NAME = "longnv";
    public static String LOCATION_NAME = "HO CHI MINH";
    public static ArrayList<KeyValuePairModel> LST_REGION = new ArrayList<>();
    public static int LocationID = 0;
    public static int CURRENT_MENU = -1;
    public static int IS_BUY_CAMERA = 0;
    public static String LIST_FPT_BOX_SELECTED = "LIST_FPT_BOX_SELECTED";
    // kiểm tra account debug
    public static boolean isDebug() {
        if (USERNAME != null && USERNAME.toLowerCase().equals(DEBUG_USERNAME.toLowerCase()))
            return true;
        return false;
    }

    public static String RESPONSE_RESULT = "ResponseResult";
    public static String LIST_OBJECT = "ListObject";
    public static String ERROR_CODE = "ErrorCode";
    public static String ERROR = "Error";

    public static String WS_URL_PRODUCTION = "https://wsmobisale.fpt.vn/MobiSaleService.svc/";
    public static String WS_URL_STAGING = "https://betawsmobisale.fpt.vn/MobiSaleService.svc/";
    public static String WS_URL_DEV = "http://210.245.105.13:7006/MobiSaleService.svc/";

    public static String WS_UPLOAD_IMAGE_STAGING = "https://sapi.fpt.vn/systemapitest/api/ImageManagerment/";
    public static String WS_UPLOAD_IMAGE_PRODUCTION = "https://sapi.fpt.vn/systemapi/api/ImageManagerment/";

    public static String WS_URL_BETA = "http://beta.wsmobisale.fpt.net/MobiSaleService.svc/";// 27.7
    public static String WS_URL_ORACLE_SIGNATURE_PRODUCT = "https://wsmobisaleora.fpt.vn/MobiSaleService.svc/";
    public static String WS_URL_BETA_ORACLE_SIGNATURE_STAGING = "https://betawsmobisaleora.fpt.vn/MobiSaleService.svc/";
    public static String DEVICE_IMEI = "";
    //============================= GCM ==================================
    public static final String GOOGLE_SENDER_ID = "41454830249";
    public static int TIMER_DEPLAY = (1000 * 60) * 3;//Thoi gian chay lan dau tien của GCM
    public static int TIMER_PERIOD = (1000 * 60 * 60) * 3;//Khoang cách thời gian giữa mỗi lần chạy GCM
    public static final String SHARE_PRE_GROUP_USER = "USER";
    public static final String SHARE_PRE_GROUP_USER_NAME = "USER_NAME";
    public static final String SHARE_PRE_GCM = "GCM";
    public static final String SHARE_PRE_GCM_REG_ID = "REG_ID";
    public static final String SHARE_PRE_GCM_SHOW_TOAST = "SHOW_TOAST";
    public static final String GCM_SENDER_APP_ID = "key=AIzaSyAe_fdTfmZW1J05crNBAC5doseTBYJHVcc";
    public static final String DATABASE_NAME = "mobiSale-db";
    public static final String DIRECTORY_ROOT = "MobiSale";
    public static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";//dd/MM/yyyy HH:mm:ss
    public static String DATE_TIME_FORMAT_VN = "dd/MM/yyyy HH:mm:ss";//dd/MM/yyyy HH:mm:ss
    public static String DATE_FORMAT_VN = "dd/MM/yyyy";//dd/MM/yyyy HH:mm:ss
    public static String DATE_FORMAT = "yyyy/MM/dd";//dd/MM/yyyy HH:mm:ss
    public static final String SHARE_PRE_HEADER = "HEADER";
    public static final String SHARE_PRE_HEADER_KEY_REQUEST_HEADER = "REQUEST_HEADER";
    //PHÒNG IBB
    public static final String SHARE_PRE_OBJECT = "OBJECT";
    public static final String SHARE_PRE_OBJECT_DEPT_LIST = "DEPT_LIST";
    public static final String SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST = "PAYMENT_TYPE_LIST";
    //ABOUNT VERSION DIALOG
    public static final String SHARE_PRE_ABOUT_VERSION = "SHARE_PRE_ABOUT_VERSION";
    //MPOS DEFAULT PAID TYPE
    public static final String SHARE_PRE_PAID_TYPE = "SHARE_PRE_PAID_TYPE";
    public static final String SHARE_PRE_PAID_MPOS = "SHARE_PRE_PAID_MPOS";
    //KEY & PASS APP MPOS
    public static final String SHARE_PRE_MPOS_CREDENTIAL_KEY = "dbaae094f049c7eeb1494c8e442264b6";
    public static final String SHARE_PRE_MPOS_CREDENTIAL_PWD = "97ec7ae1398fa1a90d2b0c1b0c012506";
    //CACHE DISTRICT, WARD_ID, STREET...
    public static final String SHARE_PRE_CACHE_REGISTER = "SHARE_PRE_CACHE_REGISTER";
    public static final String SHARE_PRE_CACHE_REGISTER_DISTRICT = "SHARE_PRE_CACHE_REGISTER_DISTRICT";
    public static final String SHARE_PRE_CACHE_REGISTER_WARD = "SHARE_PRE_CACHE_REGISTER_WARD";
    // biến lưu trữ thông tin bookport tự động
    public static String autoBookPort = "AutoBookPort";
    public static int AutoBookPort;
    public static int AutoBookPort_iR;
    public static int AutoBookPort_RetryConnect;
    public static ArrayList<KeyValuePairModel> TypePortOfSale;
    public static int AutoBookPort_Timeout;
    //biến lưu trữ đường dẫn chăm sóc khách hàng paytv
    public static String Link_CSKH_PayTv = "";
    public static final String keyGoogleMap = "AIzaSyCKhKEWJwb4vS5eK0FcskPs7od98lGdBWE";
    // Biến chứa đối tượng context nhận Code KHTN, xử lý process dialog
    public static Context contextLocal;
    // các biến mặc định cho phần upload ảnh chữ ký và nhiều ảnh hồ sơ khách hàng
    public static final int IDENTITY_MBS_VN = 1;
    public static final String TOKEN = "93982FB25E1DEF0BD4A9DA16F95CCD01";
    public static final String AUTHORIZATION = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ0aGFuaHRtNEBmcHQuY29tLnZuIn0.9q8QCf49LS_DuP0pRtJJI7xpDI52tTYS18Jija4iBnU";
    public static String KEY_SCAN_IDENTITY_CARD;
    public final static int DEPOSIT_STATUS_PAYED = 0;

    public static int IS_ENABLE_REFERRAL;

    public int PAGE_NUMBER = 10;

    public static final String TAG_RESULT_COLLECTION_BANKING = "TAG_RESULT_COLLECTION_BANKING";
    public static final String TAG_RESULT_COLLECTION_BANKING_ID = "TAG_RESULT_COLLECTION_BANKING_ID";

    //    ver 3.21 author nhannh26
    public static final String LIST_MAXY_DETAIL_SELECTED = "LIST_MAXY_DETAIL_SELECTED";
    public static final String LIST_MAXY_DEPLOY_CLEAR= "LIST_MAXY_DEPLOY_CLEAR";
    public static final String LIST_MAXY_PROMOTION_BOX_1= "LIST_MAXY_PROMOTION_BOX_1";
    public static final String OBJECT_MAXY_DEVICE_DETAIL = "OBJECT_MAXY_DEVICE_DETAIL";
    public static final String OBJECT_MAXY_DEVICE_BOX_1_DETAIL = "OBJECT_MAXY_DEVICE_BOX_1_DETAIL";
    public static final String OBJECT_MAXY_PACKAGE_DETAIL = "OBJECT_MAXY_PACKAGE_DETAIL";
    public static final String LIST_MAXY_PACKAGE_SELECTED = "LIST_MAXY_PACKAGE_SELECTED";
    public static final String LIST_MAXY_PROMOTION_PACKAGE = "LIST_MAXY_PROMOTION_PACKAGE";
    public static final String OBJECT_MAXY_PROMOTION_DEVICE = "OBJECT_MAXY_PROMOTION_DEVICE";
    public static final String OBJECT_MAXY_PROMOTION_BOX_1 = "OBJECT_MAXY_PROMOTION_BOX_1";
    public static final String LIST_MAXY_EXTRA_SELECTED = "LIST_MAXY_EXTRA_SELECTED";
    public static final String OBJECT_MAXY_EXTRA_DETAIL = "OBJECT_MAXY_EXTRA_DETAIL";
    public static final String REGTYPE = "REGTYPE";
    public static final String NUM_BOX = "NUM_BOX";
    public static final String NUM_EXTRA = "NUM_EXTRA";
    public static final String NUM_BOX_NET = "NUM_BOX_NET";
    public static final String COUNT_DEVICE = "COUNT_DEVICE";
    public static final String USE_COMBO = "USE_COMBO";
    public static final String CONTRACT_EXTRA = "CONTRACT_EXTRA";
    public static int SERVICE_TYPE = 0;
    public static boolean IS_SALE_COD = true;
    public static final String MAXY_TYPE_DEPLOY = "MAXY_TYPE_DEPLOY";
    public static final String TYPE_DEPLOY = "TYPE_DEPLOY";
    public static final String MAXY_GENERAL = "MAXY_GENERAL";
    public static final String BOX_FIRST = "BOX_FIRST";
    public static final String MAXY_PROMOTION_ID = "MAXY_PROMOTION_ID";
    public static final String MAXY_PROMOTION_TYPE = "MAXY_PROMOTION_TYPE";
    //version 3.23 nhannh26
    public static final String NUM_CLOUD = "NUM_CLOUD";
}
