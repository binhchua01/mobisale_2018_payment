package isc.fpt.fsale.action;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.choose_mac_fpt_box.ChooseMacActivity;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.MacOTT;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 11,February,2020
 */
public class GetListMacOTT implements AsyncTaskCompleteListener<String> {
    private ChooseMacActivity activity;

    public GetListMacOTT(ChooseMacActivity activity, List<MaxyBox> mList) {
        this.activity = activity;
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        for (MaxyBox item : mList) {
            jsonArray.put(item.getBoxID());
        }
        try {
            //""Ftq.thuynmt""
            jsonObject.put("SaleName", Constants.USERNAME);
            jsonObject.put("ListOTTID", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = activity.getResources().getString(R.string.txt_message_get_list_mac);
        String GET_LIST_MAC_BY_SALE_NAME = "GetListMACBySaleName";
        CallServiceTask service = new CallServiceTask(activity, GET_LIST_MAC_BY_SALE_NAME, jsonObject,
                Services.JSON_POST_OBJECT, message, GetListMacOTT.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<MacOTT> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), MacOTT.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), activity);
                } else {
                    activity.loadMacList(lst);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
