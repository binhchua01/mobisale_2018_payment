package isc.fpt.fsale.action;
/*
 *
 * @Description: 	Check application version
 * @author: 		DuHK
 * @create date: 	30/05/2015
 * */

import isc.fpt.fsale.activity.DeployAppointmentActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class UpdateDeployAppointment implements AsyncTaskCompleteListener<String> {
	private Context mContext;

	//Add by: DuHK
	public UpdateDeployAppointment(Context mContext, String UserName, int DeptID,
								   int SubID, String Timezone, String AppointmentDate,
								   String RegCode, int AbilityDesc) {
		this.mContext = mContext;
		String[] paramNames = {"UserName", "DeptID", "SubID", "TimeZone",
				"AppointmentDate", "RegCode", "AbilityDesc"};
		String[] paramValues = {UserName, String.valueOf(DeptID), String.valueOf(SubID),
				Timezone, AppointmentDate, RegCode, String.valueOf(AbilityDesc)};
		String message = "Đang cập nhật...";
		String METHOD_NAME = "UpdateDeployAppointmentV2";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames,
				paramValues, Services.JSON_POST, message, UpdateDeployAppointment.this);
		service.execute();
	}


	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<UpdResultModel> lst = null;
			if (Common.jsonObjectValidate(result)) {
				JSONObject jsObj = new JSONObject(result);
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
				WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(jsObj, UpdResultModel.class);
				if (resultObject.getErrorCode() == 0) {//OK not Error
					lst = resultObject.getListObject();
				} else {//ServiceType Error
					Common.alertDialog(resultObject.getError(), mContext);
				}
				loadData(lst);
			}
		} catch (JSONException e) {
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}

	private void loadData(List<UpdResultModel> lst) {
		try {
			if (mContext.getClass().getSimpleName().equals(DeployAppointmentActivity.class.getSimpleName())) {
				DeployAppointmentActivity activity = (DeployAppointmentActivity) mContext;
				activity.loadResult(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}