package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.AcceptPotentialObjCEM;
import isc.fpt.fsale.action.GetStreetOrCondo;
import isc.fpt.fsale.action.GetWardList;
import isc.fpt.fsale.action.GetPotentialByCode;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

//màn hình nhập mã code KHTN
public class CreatePotentialCEMObjActivity extends BaseActivity {
    private EditText txtFullName, txtEmail, txtPhone1, txtCodeCem;
    private Spinner spDistrict, spWard, spStreet, spHouseType, spNameVilla, spISPType;
    private LinearLayout frmNameVilla, frmPosition, frmHomeNumber;
    private Button btnUpdate;
    private ImageView btnInputCode;
    private TextView lblISPStartDate, lblISPEndDate;
    private final static int TAG_HOME_STREET = 1;
    private final static int TAG_HOME_CONDO = 2;
    private final static int TAG_HOME_NO_ADDRESS = 3;
    public PotentialObjModel mCurrentPotential;

    public CreatePotentialCEMObjActivity() {
        super(R.string.lbl_screen_name_create_potential_cem_obj);
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_create_potential_cem_obj));
        setContentView(R.layout.activity_create_potential_cem_obj_detail);
        try {
            Common.setupUI(this, this.findViewById(android.R.id.content));
        } catch (Exception e) {
            e.printStackTrace();
        }
        initViews();
        addEvents();
        bindData();
    }

    private void bindData() {
        Intent myIntent = getIntent();
        String Code = myIntent.getStringExtra("Code");
        if (Code != null) {
            txtCodeCem.setText(Code);
        }
    }

    private void checkVisibleViews(int id) {
        switch (id) {
            case TAG_HOME_STREET:
                frmHomeNumber.setVisibility(View.VISIBLE);
                frmNameVilla.setVisibility(View.GONE);
                frmPosition.setVisibility(View.GONE);
                break;
            case TAG_HOME_NO_ADDRESS:
                frmHomeNumber.setVisibility(View.VISIBLE);
                frmNameVilla.setVisibility(View.GONE);
                frmPosition.setVisibility(View.VISIBLE);
                break;
            case TAG_HOME_CONDO:
                frmHomeNumber.setVisibility(View.GONE);
                frmNameVilla.setVisibility(View.VISIBLE);
                frmPosition.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    private void showDialogUpdate() {
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(CreatePotentialCEMObjActivity.this);
        builder.setMessage(getResources().getString(R.string.msg_confirm_update))
                .setCancelable(false)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        accept();
                    }
                })
                .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.cancel();
                    }
                });
        dialog = builder.create();
        dialog.show();
    }

    private void addEvents() {
        btnInputCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = txtCodeCem.getText().toString();
                if (code.equals("")) {
                    Common.alertDialog("Chưa nhập code.", CreatePotentialCEMObjActivity.this);
                } else {
                    String userName = ((MyApp) getApplication()).getUserName();
                    new GetPotentialByCode(CreatePotentialCEMObjActivity.this, userName, code.toUpperCase(), null);
                }
            }
        });

        spISPType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyValuePairModel item = (KeyValuePairModel) spISPType.getItemAtPosition(position);
                if (item != null) {
                    if (item.getID() == 0) {
                        lblISPEndDate.setText("");
                        lblISPStartDate.setText("");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnUpdate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAccept()) {
                    showDialogUpdate();
                } else {
                    Common.alertDialog("Chưa có thông tin khách hàng.", CreatePotentialCEMObjActivity.this);
                }
            }
        });

        spHouseType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyValuePairModel item = (KeyValuePairModel) spHouseType.getItemAtPosition(position);
                if (item != null) {
                    checkVisibleViews(item.getID());
                    if (spDistrict.getAdapter() != null && spWard.getAdapter() != null) {
                        KeyValuePairModel district = (KeyValuePairModel) spDistrict.getSelectedItem();
                        KeyValuePairModel ward = (KeyValuePairModel) spWard.getSelectedItem();
                        if (item.getID() == TAG_HOME_CONDO) {
                            initNameVilla(district.getsID(), ward.getsID());
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spWard.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyValuePairModel district = (KeyValuePairModel) spDistrict.getSelectedItem();
                KeyValuePairModel ward = (KeyValuePairModel) spWard.getItemAtPosition(position);
                KeyValuePairModel houseType = (KeyValuePairModel) spHouseType.getSelectedItem();
                if (ward != null) {
                    initStreet(district.getsID(), ward.getsID());
                    if (houseType != null && houseType.getID() == TAG_HOME_CONDO){
                        initNameVilla(district.getsID(), ward.getsID());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spDistrict.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyValuePairModel item = (KeyValuePairModel) spDistrict.getItemAtPosition(position);
                if (spDistrict.getSelectedItemPosition() > 0) {
                    initWard(item.getsID());
                } else {
                    if (spWard.getAdapter() != null && spWard.getAdapter().getCount() > 0){
                        spWard.setSelection(0, true);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txtFullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtFullName.setError(null);
            }
        });

        txtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtEmail.setError(null);
            }
        });

        txtPhone1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtPhone1.setError(null);
            }
        });
    }

    private void initViews() {
        lblISPStartDate = (TextView) findViewById(R.id.lbl_isp_start_date);
        lblISPEndDate = (TextView) findViewById(R.id.lbl_isp_end_date);
        btnInputCode = (ImageView) findViewById(R.id.btn_code);
        spISPType = (Spinner) findViewById(R.id.sp_isp_type);
        btnUpdate = (Button) findViewById(R.id.btn_update);
        spStreet = (Spinner) findViewById(R.id.sp_street);
        spNameVilla = (Spinner) findViewById(R.id.sp_name_villa);
        spHouseType = (Spinner) findViewById(R.id.sp_house_type);
        Spinner spCusType = (Spinner) findViewById(R.id.sp_cus_type);
        spCusType.setVisibility(View.GONE);
        spDistrict = (Spinner) findViewById(R.id.sp_district);
        spWard = (Spinner) findViewById(R.id.sp_ward);
        frmNameVilla = (LinearLayout) findViewById(R.id.frm_name_villa);
        frmPosition = (LinearLayout) findViewById(R.id.frm_position);
        frmHomeNumber = (LinearLayout) findViewById(R.id.frm_home_number);
        txtFullName = (EditText) findViewById(R.id.txt_full_name);
        txtEmail = (EditText) findViewById(R.id.txt_email);
        txtPhone1 = (EditText) findViewById(R.id.txt_phone_1);
        txtCodeCem = (EditText) findViewById(R.id.txt_code_cem);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void initWard(String district) {
        String id = "";
        new GetWardList(this, district, spWard, id);
    }

    private void initStreet(String district, String ward) {
        String id = "";
        new GetStreetOrCondo(this, district, ward, GetStreetOrCondo.street, spStreet, id);
    }

    private void initNameVilla(String district, String ward) {
        String id = "";
        new GetStreetOrCondo(this, district, ward, GetStreetOrCondo.condo, spNameVilla, id);
    }

    private void accept() {
        String UserName = ((MyApp) getApplication()).getUserName();
        new AcceptPotentialObjCEM(CreatePotentialCEMObjActivity.this, UserName,
                mCurrentPotential.getID(), mCurrentPotential.getCaseID());
    }

    private boolean checkAccept() {
        return mCurrentPotential != null && mCurrentPotential.getID() > 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}