package isc.fpt.fsale.services;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * CLASS: 		JSONParsing
 * @author: 	vandn
 * @created on: 13/08/2013
 * */
public class JSONParsing{
	public static final String TAG_TABLE = "Table";
	public static final String TAG_START_OBJ = "{";
	public static final String TAG_EMPTY_OBJ = "{}";
	public JSONParsing(){}
	/*
	*//**
	 * check if json string is obj or array
	 * *//*
	public static boolean isObj(String json){
		if(json.startsWith(TAG_START_OBJ))
			return true;
		else return false;
	}*/
	
	/**
	 * get json object from json array
	 * */
	public static JSONObject getJsonObj(JSONArray jArray, int index){
		JSONObject jsonObj = null;
		try {
			jsonObj = jArray.getJSONObject(index);
		} catch (JSONException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		if(jsonObj.equals(TAG_EMPTY_OBJ))
			jsonObj = null;	
		return jsonObj;
	}
	
	/**
	 * convert json string to json obj
	 * */
	public static JSONObject getJsonObj(String json){       
		JSONObject jsonObj = null;
		try {
			jsonObj = new JSONObject(json);
			if(jsonObj.equals(TAG_EMPTY_OBJ))
				jsonObj = null;		
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			//
			e.printStackTrace();
		}
		return jsonObj;
	}
	
	/**
	 * convert json string to json array
	 * *//*
	public static JSONArray getJsonArray(String json){       
		JSONArray jsonArr = null;
		try {
			jsonArr = new JSONArray(json);
			if(jsonArr.length()< 0)
				jsonArr = null;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonArr;
	}*/
	
}