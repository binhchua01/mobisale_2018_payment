package isc.fpt.fsale.action.camera319;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.ui.fragment.FragmentRegisterContractCamera;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3v2;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetCostSetupCamerav2 implements AsyncTaskCompleteListener<String> {
    private FragmentRegisterStep3v2 mFptCameraService;
    private FragmentRegisterContractCamera mFptCameraServiceContract;
    private Context mContext;

    //bán mới
    public GetCostSetupCamerav2(Context mContext, FragmentRegisterStep3v2 mFptCameraService, RegistrationDetailModel object) {
        this.mFptCameraService = mFptCameraService;
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.msg_progress_get_price_set_up_camera);
        String GET_COST_SETUP_CAMERA = "GetCostSetupCameraOfNet";
        CallServiceTask service = new CallServiceTask(mContext, GET_COST_SETUP_CAMERA,
                object.toJsonObjectCostSetupCamera(), Services.JSON_POST_OBJECT, message, GetCostSetupCamerav2.this);
        service.execute();
    }

    //bán thêm
    public GetCostSetupCamerav2(Context mContext, FragmentRegisterContractCamera mFptCameraServiceContract, RegistrationDetailModel object) {
        this.mFptCameraServiceContract = mFptCameraServiceContract;
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.msg_progress_get_price_set_up_camera);
        String GET_COST_SETUP_CAMERA = "GetCostSetupCameraOfNet";
        CallServiceTask service = new CallServiceTask(mContext, GET_COST_SETUP_CAMERA,
                object.toJsonObjectCostSetupCamera(), Services.JSON_POST_OBJECT, message, GetCostSetupCamerav2.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<SetupDetail> resultObject = new WSObjectsModel<>(jsObj, SetupDetail.class);
                List<SetupDetail> lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    //callback to activity
                    if (lst.size() == 0) return;
                    if (mFptCameraService != null) {
                        mFptCameraService.loadInfoSetupDetail(lst);
                    } else {
                        mFptCameraServiceContract.loadInfoSetupDetail(lst);
                    }
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
