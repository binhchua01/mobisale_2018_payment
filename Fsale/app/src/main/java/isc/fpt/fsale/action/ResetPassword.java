package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.EnCrypt;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;

public class ResetPassword  implements AsyncTaskCompleteListener<String> {
	private final String RESET_PASSWORD ="ResetPassword";
	private final String TAG_RESET_PASSWORD = "ResetPasswordResult";
	private final String TAG_RESULT = "Result";
	private final String TAG_RESULT_ID = "ResultID";
	private final String TAG_ERROR_SERVICE = "ErrorService";
	
	private DialogFragment frmResetPassword;
	private int ResultID = 0;
	private Context mContext;
	
	public ResetPassword(Context _mContext,String PasswordOld, String PasswordNew, DialogFragment frmResetPassword)
	{
		this.mContext=_mContext;
		this.frmResetPassword = frmResetPassword;
		//call service
		String message = "Xin cho trong giay lat";
		
		String[] params = new String[]{Constants.USERNAME, EnCrypt.SHA512Encrypt(PasswordOld), EnCrypt.SHA512Encrypt(PasswordNew)};
		String[] paramNames = new String[]{"UserName", "PasswordOld", "PasswordNew"};
		CallServiceTask service = new CallServiceTask(mContext,RESET_PASSWORD, paramNames, params, Services.JSON_POST, message, ResetPassword.this);
		service.execute();	
	}
	
	public void handleGetDistrictsResult(String json){
		if(json != null && Common.jsonObjectValidate(json)){	
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(Common.isEmptyJSONObject(jsObj))
			{
				return;
			}
			bindData(jsObj);
		}
		else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	 } 
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {	
				jsArr = jsObj.getJSONArray(TAG_RESET_PASSWORD);
				String error = jsArr.getJSONObject(0).getString(TAG_ERROR_SERVICE);
				if(error == "null")
				{
					int l = jsArr.length();
					if(l>0)
					{
						for(int i=0;i<l;i++)
						{
							JSONObject iObj = jsArr.getJSONObject(i);							
							try
							{
								if(iObj.has(TAG_RESULT_ID))
								{
									ResultID = iObj.getInt(TAG_RESULT_ID);
								}
								
								new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(iObj.getString(TAG_RESULT))
				 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
				 				    @Override
				 				    public void onClick(DialogInterface dialog, int which) {
				 				    	if(ResultID == 1)
				 				    		frmResetPassword.dismiss();
				 				        dialog.cancel();
				 				    }
				 				})
			 					.setCancelable(false).create().show();
							}
							catch(Exception ex)
							{

							}
						}
					}
				}
				else Common.alertDialog("Lỗi WS: " +error, mContext);
		
		} catch (JSONException e) {

		}
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetDistrictsResult(result);
	}
	
}
