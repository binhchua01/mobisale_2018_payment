package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListTodoListDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.UpdateTodoListDetailDialog;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class UpdateTodoListDetail implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    public final String TAG_METHOD_NAME = "UpdateToDoListDetail";
    private UpdateTodoListDetailDialog updateDialog;

    public UpdateTodoListDetail(Context context, UpdateTodoListDetailDialog updateDialog,
                                String UserName, int ToDoListID, String CareType, String Desc, int Status) {
        this.mContext = context;
        this.updateDialog = updateDialog;
        String message = "Đang cập nhật...";
        String[] paramNames = new String[]{"UserName", "ToDoListID", "CareType", "Desc", "Status"};
        String[] paramValues = new String[]{UserName, String.valueOf(ToDoListID), CareType, Desc, String.valueOf(Status)};
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, UpdateTodoListDetail.this);
        service.execute();
    }


    @Override
    public void onTaskComplete(String result) {
        try {
            List<UpdResultModel> lst = null;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<>(jsObj, UpdResultModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                } else {//ServiceType Error
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(List<UpdResultModel> lst) {
        try {
            if (lst.size() > 0) {
                UpdResultModel item = lst.get(0);
                if (item.getResultID() > 0) {
                    if (mContext.getClass().getSimpleName().equals(ListTodoListDetailActivity.class.getSimpleName())) {
                        new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(item.getResult())
                                .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            if (updateDialog != null) {
                                                updateDialog.dismiss();
                                            }
                                            dialog.cancel();
                                            ((ListTodoListDetailActivity) mContext).getData(1);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                })
                                .setCancelable(false)
                                .create()
                                .show();
                    } else {
                        Common.alertDialog(item.getMessage(), mContext);
                    }
                } else {
                    Common.alertDialog(lst.get(0).getResult(), mContext);
                }
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}