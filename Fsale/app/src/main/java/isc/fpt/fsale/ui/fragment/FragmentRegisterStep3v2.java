package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetFPTBoxList;
import isc.fpt.fsale.action.GetIPTVPrice;
import isc.fpt.fsale.action.GetPromotionIPTVList;
import isc.fpt.fsale.action.GetPromotionList;
import isc.fpt.fsale.action.camera319.GetCostSetupCamerav2;
import isc.fpt.fsale.activity.BarcodeScanActivity;
import isc.fpt.fsale.activity.PromotionFilterActivity;
import isc.fpt.fsale.activity.PromotionIPTVFilterActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.ShowImageActivity;
import isc.fpt.fsale.activity.choose_mac_fpt_box.ChooseMacActivity;
import isc.fpt.fsale.activity.device_list.DeviceListActivity;
import isc.fpt.fsale.activity.device_price_list.DevicePriceListActivity;
import isc.fpt.fsale.activity.local_type.LocalTypeActivity;
import isc.fpt.fsale.activity.service_type.ServiceTypeActivity;
import isc.fpt.fsale.adapter.DeviceAdapter;
import isc.fpt.fsale.adapter.FPTBoxAdapter;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.MacAdapter;
import isc.fpt.fsale.adapter.MacMaxyAdapter;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.IPTVPriceModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.LocalType;
import isc.fpt.fsale.model.MacOTT;
import isc.fpt.fsale.model.PackageModel;
import isc.fpt.fsale.model.PromotionFPTBoxModel;
import isc.fpt.fsale.model.PromotionIPTVModel;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.camera319.PromoCloud;
import isc.fpt.fsale.ui.adapter.CameraDetailV2Adapter;
import isc.fpt.fsale.ui.adapter.CloudDetailV2Adapter;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV3;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV4;
import isc.fpt.fsale.ui.callback.OnItemClickUpdateCount;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_package.CameraOrNetPackageActivity;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_type.CameraOrNetTypeActivity;
import isc.fpt.fsale.ui.fpt_camera.cloud_detail.netorcamera.cloudtype.CloudTypeV2Activity;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCameraOfNet;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyTVDeviceAdapter;
import isc.fpt.fsale.ui.maxytv.adapter.MaxyTVExtraAdapter;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.ui.maxytv.model.MaxyExtra;
import isc.fpt.fsale.ui.maxytv.model.MaxyGeneral;
import isc.fpt.fsale.ui.maxytv.model.MaxySetupType;
import isc.fpt.fsale.ui.maxytv.model.MaxyTypeDeploy;
import isc.fpt.fsale.ui.maxytv.model.ObjectMaxy;
import isc.fpt.fsale.ui.maxytv.ui.MaxyBox1Activity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyDeviceActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyExtraActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPackageActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPromotionBox1Activity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPromotionDeviceActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPromotionPackageActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxySetupTypeActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyTypeDeployActivity;
import isc.fpt.fsale.ui.promo_cloud.PromoCloudActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.BOX_FIRST;
import static isc.fpt.fsale.utils.Constants.CAMERA_DETAIL;
import static isc.fpt.fsale.utils.Constants.CLASS_NAME;
import static isc.fpt.fsale.utils.Constants.CLOUD_DETAIL;
import static isc.fpt.fsale.utils.Constants.CONTRACT_EXTRA;
import static isc.fpt.fsale.utils.Constants.COUNT_DEVICE;
import static isc.fpt.fsale.utils.Constants.CUSTYPE;
import static isc.fpt.fsale.utils.Constants.LIST_CAMERA_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_CLOUD_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_MAC_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_EXTRA_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_PACKAGE_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_PROMOTION_BOX_1;
import static isc.fpt.fsale.utils.Constants.LIST_MAXY_PROMOTION_PACKAGE;
import static isc.fpt.fsale.utils.Constants.LOCAL_TYPE;
import static isc.fpt.fsale.utils.Constants.MAXY_GENERAL;
import static isc.fpt.fsale.utils.Constants.MAXY_PROMOTION_ID;
import static isc.fpt.fsale.utils.Constants.MAXY_PROMOTION_TYPE;
import static isc.fpt.fsale.utils.Constants.MAXY_TYPE_DEPLOY;
import static isc.fpt.fsale.utils.Constants.NUM_BOX;
import static isc.fpt.fsale.utils.Constants.NUM_BOX_NET;
import static isc.fpt.fsale.utils.Constants.NUM_EXTRA;
import static isc.fpt.fsale.utils.Constants.OBJECT_CAMERA_DETAIL;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_DEVICE_BOX_1_DETAIL;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_DEVICE_DETAIL;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_EXTRA_DETAIL;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_PACKAGE_DETAIL;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_PROMOTION_BOX_1;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_PROMOTION_DEVICE;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_SETUP_TYPE;
import static isc.fpt.fsale.utils.Constants.OBJECT_MAXY_TYPE_DEPLOY;
import static isc.fpt.fsale.utils.Constants.OBJECT_PROMOTION_CLOUD_DETAIL;
import static isc.fpt.fsale.utils.Constants.POSITION;
import static isc.fpt.fsale.utils.Constants.REGTYPE;
import static isc.fpt.fsale.utils.Constants.TAG_CONTRACT;
import static isc.fpt.fsale.utils.Constants.TYPE_DEPLOY;
import static isc.fpt.fsale.utils.Constants.USE_COMBO;
import static java.lang.String.valueOf;

public class FragmentRegisterStep3v2 extends BaseFragment implements OnCheckedChangeListener, OnItemClickListenerV3<Integer, Device, Integer>, OnItemClickListenerV4<Object, Integer, Integer>, OnItemClickListener<Boolean>, OnItemClickUpdateCount<Boolean> {

    public RegistrationDetailModel modelDetail = null;
    public PromotionModel selectedInternetPromotion;
    public RegistrationDetailModel mRegister;
    public PromotionIPTVModel promotionIPTVModelBox1, promotionIPTVModelBox2;

    private MacAdapter macAdapter;
    private MacMaxyAdapter macMaxyAdapter;

    private FPTBoxAdapter fptBoxAdapter;
    public List<FPTBox> listFptBox, listFptBoxSelect;

    public List<Device> mListDeviceSelected;
    private DeviceAdapter mDeviceAdapter;

    private List<CharSequence> listNameFptBox;
    private List<MacOTT> macList, macListMaxy;
    public ArrayList<PromotionModel> mListPromotion;
    public ArrayList<PromotionIPTVModel> mListPromotionIPTVBox1;
    public ArrayList<PromotionIPTVModel> mListPromotionIPTVBox2;
    public List<CategoryServiceList> mListServiceSelected;

    private HashMap<Integer, ArrayList<PromotionFPTBoxModel>> listPromotionFptBox;
    @SuppressLint("UseSparseArrays")
    public Map<Integer, PackageModel> mDicPackage = new HashMap<>();
    public boolean imgIPTVTotal = false, spIPTVEquipmentDelivery = false, frmIPTVMonthlyTotal,
            is_have_fpt_box, is_have_device, is_have_ip_tv, is_clear_rp_and_voucher, is_have_camera, is_have_maxy;
    public int serviceType;
    public int localTypeId;
    public int customerType = 1;
    public static int isBind = 0;
    public String lblInternetTotal = "0";
    public String lblIPTVTotal = "0";
    public String lblTotal = "0";
    public String errorStr;
    public String contractParam;
    public String regCodeParam;

    private final int CODE_LOCAL_TYPE = 100;
    private final int CODE_SERVICE_TYPE = 101;
    private final int CODE_DEVICE_LIST_SELECTED = 102;
    private final int CODE_DEVICE_PRICE_LIST_SELECTED = 103;
    private final int CODE_CHOOSE_MAC = 104;

    public RadioButton radIpTvSetUpYes, radIpTvSetUpNo, radIPTVDrillWallYes, radIPTVDrillWallNo, rdoDeploy, rdoNoDeploy;
    public EditText txtPromotion;
    public EditText txtPromotionBox1;
    public EditText txtPromotionBox2;
    public EditText txtIPTVPrepaid;
    public EditText txtPromotionCloud;

    public ImageView imgIPTVBoxPlus, imgIPTVPLCLess, imgIPTVPLCPlus, imgIPTVReturnSTBLess,
            imgIPTVReturnSTBPlus, imgIPTVChargeTimesPlus, imgIPTVExtraNavigator, imgIPTVHBOChargeTimeLess,
            imgIPTVHBOChargeTimePlus, imgIPTVHBOPrepaidMonthLess, imgIPTVHBOPrepaidMonthPlus, imgIPTVBoxLess,
            imgPromHasImg, imgPromHasVideo, imgIPTVFimPlus_ChargeTimeLess, imgIPTVFimPlus_ChargeTimePlus,
            imgIPTVFimPlus_PrepaidMonthLess, imgIPTVFimPlus_PrepaidMonthPlus, imgIPTVFimHot_ChargeTimeLess,
            imgIPTVFimHot_ChargeTimePlus, imgIPTVFimHot_PrepaidMonthLess, imgIPTVKPlusChargeTimeLess,
            imgIPTVKPlusChargeTimePlus, imgIPTVKPlusPrepaidMonthLess, imgIPTVKPlusPrepaidMonthPlus,
            imgIPTVFimHot_PrepaidMonthPlus, imgIPTVVTVChargeTimeLess, imgIPTVVTVChargeTimePlus,
            imgIPTVVTVPrepaidMonthLess, imgIPTVVTVPrepaidMonthPlus,
            imgIPTVVTCChargeTimeLess, imgIPTVVTCChargeTimePlus, imgIPTVVTCPrepaidMonthLess,
            imgIPTVVTCPrepaidMonthPlus, imgIPTVFimStandard_ChargeTimeLess, imgIPTVFimStandard_ChargeTimePlus,
            imgIPTVFimStandard_MonthCountLess, imgIPTVFimStandard_MonthCountPlus,
            imgFoxy2ChargeTimeLess, imgFoxy2ChargeTimePlus, imgFoxy2PrepaidMonthLess, imgFoxy2PrepaidMonthPlus,
            imgFoxy4ChargeTimeLess, imgFoxy4ChargeTimePlus, imgFoxy4PrepaidMonthLess, imgFoxy4PrepaidMonthPlus;
    public Spinner spIPTVPackage, spIPTVCombo, spIPTVFormDeployment;
    public TextView lblIPTVPromotionBoxFirstAmount, lblIPTVPromotionBoxOrderAmount, lblIPTVReturnSTBCount,
            lblIPTVChargeTimesCount, txtIPTVBoxCount, lblIPTVPLCCount, lblPromotionInternetDetail,
            lblIPTVHBOChargeTimesCount, lblIPTVHBOPrepaidMonthCount, lblIPTVFimPlusAmount,
            lblIPTVFimPlus_ChargeTime, lblIPTVFimPlus_MonthCount, lblIPTVFimHotAmount, lblIPTVFimHot_ChargeTime,
            lblIPTVKPlusAmount, lblIPTVKPlusChargeTimesCount, lblIPTVKPlusPrepaidMonthCount, lblIPTVVTVAmount,
            lblIPTVVTVChargeTimesCount, lblIPTVVTVPrepaidMonthCount, lblIPTVFimStandard_ChargeTimes,
            lblIPTVFimStandard_Amount, lblIPTVFimStandard_MonthCount, lblIPTVVTCChargeTimesCount,
            lblIPTVVTCPrepaidMonthCount, lblIPTVVTCAmount, lblIPTVFimHot_MonthCount, tvLocalType, tvServiceType,
            tvFoxy2ChargeTime, tvFoxy2PrepaidMonth, tvFoxy4ChargeTime, tvFoxy4PrepaidMonth, tvFoxy2Amount, tvFoxy4Amount, lbliptvPromotionCamera;
    public CheckBox chbIpTvHbo, chbIPTVFimPlus, chbIpTvFimHot, chbIPTVKPlus,
            chbIpTvVtc, chbIpTvVtv, chbIPTVFimStandard, cbFoxy2, cbFoxy4;
    public LinearLayout frmIPTVFimPlus, frmIpTvFimHot, frmInternet, frmIpTv, frmIPTVExtraNavigation,
            frmIPTVExtra, frmIPTVKPlus, frmIpTvVtc, frmIPTVFimStandard, frmIpTvVtv, frmListFptBox, frmDacSacHD, frmCamera;
    public ScrollView frmMain;
    public Button btnShowKeyWord, btnChooseMac, btnSelectDevice, btnSelectFptBox, btnAddCamera, btnAddCloud;
    public View frmDevicesBanner, frmContentFptBox, loQuantityDeploy;
    private TextView tvSetupDetailPrice, tvSetupDetailQuantity, tvSetupDetailLess, tvSetupDetailPlus;
    public List<CameraDetail> mListCamera;
    private final int CODE_CAMERA_DETAIL = 1;
    private final int CODE_CAMERA_PACKAGE = 3;
    private final int CODE_CLOUD_DETAIL = 2;
    private final int CODE_CLOUD_PROMOTION = 4;
    private CameraDetailV2Adapter mCameraAdapter;
    public List<CloudDetail> mListCloud;
    public PromoCloud mProCloud;
    public SetupDetail mSetupDetail;
    private CloudDetailV2Adapter mCloudAdapter;
    private RegisterActivityNew activity;

    //Khai báo ver 3.22 nhannh26
    public LinearLayout frmMaxyTV, frmDeviceMaxy, frmMaxyContainer, frmMaxyMac;
    public TextView textPromotion, textMaxyGeneral, textPriceBox1;
    public RadioButton rdoRegisterComboYes, rdoRegisterComboNo;
    public EditText selPackage, selGetCLKM, mTypeDeploy, mSetupType, mBox1, mPromotionBox1;
    public Button btnSelectDeviceMaxyTV, btnSelectExtraMaxyTV, btnSelectMacMaxy;
    public RecyclerView recDeviceMaxyTV, recPackageMaxyTV, recMacMaxy;
    private MaxyTVDeviceAdapter mMaxyDeviceAdapter;
    private MaxyTVExtraAdapter mMaxyExtraAdapter;
    private final int CODE_MAXY_DEVICE_DETAIL = 6;
    private final int CODE_MAXY_PACKAGE_DETAIL = 5;
    private final int CODE_MAXY_PROMOTION_PACKAGE_DETAIL = 7;
    private final int CODE_MAXY_PROMOTION_DEVICE = 8;
    private final int CODE_MAXY_EXTRA_DETAIL = 9;
    public List<MaxyBox> mListMaxyDevice;
    public List<MaxyBox> listDevice;
    public List<MaxyExtra> mListMaxyExtra;
    public MaxyTypeDeploy mMaxyDeploy;
    public MaxySetupType mMaxySetup;
    public MaxyGeneral mMaxyGeneral = new MaxyGeneral();
    public MaxyBox maxyBox1;
    private final int CODE_MAXY_TYPE_DEPLOY = 10;
    private final int CODE_MAXY_SETUP_TYPE = 11;
    private final int CODE_MAXY_BOX_1 = 12;
    private final int CODE_MAXY_PROMOTION_BOX_1 = 13;
    private final int CODE_CHOOSE_MAC_MAXY = 14;
    public int count = 0, numBox = 0;


    public static FragmentRegisterStep3v2 newInstance(RegistrationDetailModel modelDetail) {
        return new FragmentRegisterStep3v2(modelDetail);
    }

    @SuppressLint("ValidFragment")
    public FragmentRegisterStep3v2(RegistrationDetailModel modelDetail) {
        super();
        this.modelDetail = modelDetail;
    }

    public FragmentRegisterStep3v2() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (RegisterActivityNew) getActivity();
        modelDetail = activity.getRegistrationDetail();
        if (modelDetail.getObjectCameraOfNet() == null) {
            modelDetail.setObjectCameraOfNet(
                    new ObjectCameraOfNet(new ArrayList<CameraDetail>(), new ArrayList<CloudDetail>(), new SetupDetail(), new PromoCloud(), 0));
        }
        if (modelDetail.getObjectMaxy() == null) {
            modelDetail.setObjectMaxy(
                    new ObjectMaxy(new MaxyGeneral(), new ArrayList<MaxyBox>(), new ArrayList<MaxyExtra>()));
        }
    }

    @Override
    protected void bindData() {
        //bind old data
        List<CategoryServiceList> mListService = new ArrayList<>();
        if (modelDetail != null && modelDetail.getCategoryServiceList() != null &&
                modelDetail.getCategoryServiceList().size() != 0) {
            mListService = modelDetail.getCategoryServiceList();
        }
        if (((RegisterActivityNew) getActivity()).getListService() != null &&
                ((RegisterActivityNew) getActivity()).getListService().size() != 0) {
            mListService = ((RegisterActivityNew) getActivity()).getListService();
        }
        if (modelDetail != null) {
            if (mListService.size() != 0) {//chỉ gọi khi nào bán Internet
                if (modelDetail.getLocalType() != 0) {//cập nhật PDK
                    StringBuilder service = new StringBuilder();
                    for (int i = 0; i < mListService.size(); i++) {
                        if ((mListService.size() - 1) == i) {
                            service.append(mListService.get(i).getCategoryServiceName());
                        } else {
                            service.append(mListService.get(i).getCategoryServiceName()).append(", ");
                        }
                    }
                    //set service type name to view
                    tvServiceType.setText(service.toString());
                    txtPromotion.setText(modelDetail.getPromotionName());
                    if (selectedInternetPromotion == null) {
                        selectedInternetPromotion = new PromotionModel();
                    }
                    selectedInternetPromotion.setPromotionID(modelDetail.getPromotionID());
                    selectedInternetPromotion.setPromotionName(modelDetail.getPromotionName());
                    mListServiceSelected = mListService;
                    serviceType = 1;
                    tvLocalType.setText(modelDetail.getLocalTypeName());
                    localTypeId = modelDetail.getLocalType();
                    new GetPromotionList(getActivity(), this, Constants.LOCATION_ID, localTypeId, serviceType);
                } else {
                    if (mListService.size() == 1) {
                        tvServiceType.setText(mListService.get(0).getCategoryServiceName());
                        serviceType = mListService.get(0).getCategoryServiceID();
                    }
                }
            }
        } else {
            if (mListService.size() == 1) {
                tvServiceType.setText(mListService.get(0).getCategoryServiceName());
                serviceType = mListService.get(0).getCategoryServiceID();
                mListServiceSelected = mListService;
            }
        }

        initViewByServiceType(mListService);
        initDataDevice();
        initDataFptBox();
        initIPTVCombo();
        initIPTVFormDeployment();
        initIPTVAllPackages();
        initContractRegCode();
        initControlElse();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_register_step3_v2;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case CODE_SERVICE_TYPE:
                    List<CategoryServiceList> mListService = data.getParcelableArrayListExtra(Constants.SERVICE_TYPE_LIST);
                    mListServiceSelected.clear();
                    mListServiceSelected = mListService;
                    StringBuilder service = new StringBuilder();
                    for (int i = 0; i < mListServiceSelected.size(); i++) {
                        if ((mListServiceSelected.size() - 1) == i) {
                            service.append(mListServiceSelected.get(i).getCategoryServiceName());
                        } else {
                            service.append(mListServiceSelected.get(i).getCategoryServiceName()).append(", ");
                        }
                    }
                    //set service type name to view
                    tvServiceType.setText(service.toString());
                    initViewByServiceType(mListServiceSelected);
                    //clear thông tin RP & E-Voucher
                    is_clear_rp_and_voucher = true;
                    break;
                case CODE_LOCAL_TYPE:
                    LocalType modelLocalType = data.getParcelableExtra(Constants.LOCAL_TYPE);
                    if (modelLocalType == null) return;
                    tvLocalType.setText(modelLocalType.getLocalTypeName());
                    localTypeId = modelLocalType.getLocalTypeID();
                    if (this.mListServiceSelected.size() != 0) {//chỉ gọi khi nào bán Internet
                        new GetPromotionList(getActivity(), this,
                                Constants.LOCATION_ID, localTypeId, 1/*Id của laoji dịch vụ internet*/);
                        serviceType = 1;
                    }
                    clearMaxyContainer(true);
                    //clear thông tin RP & E-Voucher
                    is_clear_rp_and_voucher = true;
                    break;
                case CODE_DEVICE_LIST_SELECTED:
                    Device mDevice = data.getParcelableExtra(Constants.DEVICE);
                    mListDeviceSelected.add(mDevice);
                    mDeviceAdapter.notifyData(mListDeviceSelected);
                    break;
                case CODE_DEVICE_PRICE_LIST_SELECTED:
                    Device mDeviceSelected = data.getParcelableExtra(Constants.DEVICE);
                    if (mDeviceSelected == null) return;
                    int position = data.getIntExtra(Constants.POSITION, 0);

                    if (mListDeviceSelected.size() > 1) {
                        for (Device item : mListDeviceSelected) {
                            if (item.getDeviceID() == mDeviceSelected.getDeviceID() &&
                                    item.getPriceID() == mDeviceSelected.getPriceID()) {
                                Common.alertDialog(getString(R.string.message_double_price), this.getActivity());
                                return;
                            }
                        }
                    }
                    mListDeviceSelected.set(position, mDeviceSelected);
                    mDeviceAdapter.notifyData(mListDeviceSelected);
                    break;
                case CODE_CHOOSE_MAC:
                    macList = data.getParcelableArrayListExtra(Constants.LIST_MAC_SELECTED);
                    macAdapter.notifyData(macList);
                    break;
                case CODE_CAMERA_DETAIL:
                    List<CameraDetail> mList = data.getParcelableArrayListExtra(OBJECT_CAMERA_DETAIL);
                    mListCamera.clear();
                    if (mList != null && mList.size() != 0) {
                        mListCamera.addAll(mList);
                        modelDetail.getObjectCameraOfNet().setCameraDetail(mListCamera);
                    } else {
                        rdoDeploy.setChecked(false);
                        rdoDeploy.setEnabled(false);
                        rdoNoDeploy.setChecked(true);
                        loQuantityDeploy.setVisibility(View.GONE);
                    }
                    mCameraAdapter.notifyDataChanged(mListCamera);
                    break;
                case CODE_CAMERA_PACKAGE:
                    CameraDetail mCameraDetail = data.getParcelableExtra(OBJECT_CAMERA_DETAIL);
                    int pos = data.getIntExtra(POSITION, -1);
                    mListCamera.set(pos, mCameraDetail);
                    mCameraAdapter.notifyDataChanged(mListCamera);
                    if (mListCamera.size() != 0) {
                        //hiển thị layout triển khai camera
                        rdoDeploy.setEnabled(true);
                    } else {
                        rdoDeploy.setEnabled(false);
                    }
                    break;
                case CODE_CLOUD_DETAIL:
                    CloudDetail mCloudDetail = data.getParcelableExtra(CLOUD_DETAIL);
                    if (mCloudDetail != null) {
                        for (CloudDetail item : mListCloud) {
                            if (item.getPackID() == mCloudDetail.getPackID()) {
                                Common.alertDialog("Trùng CLKM với gói Cloud khác.", this.getActivity());
                                return;
                            }
                        }
                        modelDetail.getObjectCameraOfNet().getCloudDetail().add(mCloudDetail);
                        mListCloud = modelDetail.getObjectCameraOfNet().getCloudDetail();
                        mCloudAdapter.notifyDataChanged(mListCloud);

                    }
                    mProCloud = new PromoCloud();
                    txtPromotionCloud.getText().clear();
                    if (mListCloud.size() != 0) {
                        setupPromotionCloud(true);
                    }
                    break;
                case CODE_CLOUD_PROMOTION:
                    PromoCloud mDetail = data.getParcelableExtra(OBJECT_PROMOTION_CLOUD_DETAIL);
                    if (mDetail != null) {
                        modelDetail.getObjectCameraOfNet().setPromotionDetail(mDetail);
                        mProCloud = mDetail;
                    }
                    assert mDetail != null;
                    txtPromotionCloud.setText(mDetail.getPromoName());
                    break;

                case CODE_MAXY_PACKAGE_DETAIL:
                    MaxyGeneral mGeneralData = data.getParcelableExtra(OBJECT_MAXY_PACKAGE_DETAIL);
                    if (mMaxyGeneral == null) return;
                    mMaxyGeneral = mGeneralData;
                    selPackage.setText(mMaxyGeneral.getPackage());
                    modelDetail.getObjectMaxy().setMaxyGeneral(mMaxyGeneral);
                    selGetCLKM.setText("");
                    textMaxyGeneral.setText(Common.formatMoney(String.valueOf(0)));
                    mMaxyGeneral.setTypeDeployID(mMaxyDeploy.getTypeDeployID());
                    mMaxyGeneral.setTypeDeployName(mMaxyDeploy.getTypeDeployName());
                    clearBox();
                    clearExtra();
                    break;
                case CODE_MAXY_PROMOTION_PACKAGE_DETAIL:
                    MaxyGeneral mMaxyGe = data.getParcelableExtra(OBJECT_MAXY_PACKAGE_DETAIL);
                    if (mMaxyGe == null) return;
                    selGetCLKM.setText(mMaxyGe.getPromotionDesc());
                    modelDetail.getObjectMaxy().setMaxyGeneral(mMaxyGe);
                    mMaxyGeneral = mMaxyGe;
                    textMaxyGeneral.setText(Common.formatMoney(String.valueOf(mMaxyGe.getPrice())));
                    numBox = mMaxyGeneral.getNumBox();
                    clearBox();
                    clearExtra();
                    break;

                case CODE_MAXY_DEVICE_DETAIL:
                    MaxyBox mMaxyBox = data.getParcelableExtra(OBJECT_MAXY_DEVICE_DETAIL);
                    mListMaxyDevice.add(mMaxyBox);
                    mMaxyDeviceAdapter.notifyDataChanged(mListMaxyDevice);
                    break;

                case CODE_MAXY_PROMOTION_DEVICE:
                    MaxyBox mBox = data.getParcelableExtra(OBJECT_MAXY_PROMOTION_DEVICE);
                    int pos1 = data.getIntExtra(POSITION, -1);
                    if (mBox != null) {
                        if (mListMaxyDevice.size() > 1) {
                            for (MaxyBox item : mListMaxyDevice) {
                                if (item.getBoxID() == mBox.getBoxID()) {
                                    if (item.getPromotionID() == mBox.getPromotionID()) {
                                        Common.alertDialog("Trùng CLKM với thiết bị khác.", this.getActivity());
                                        return;
                                    }
                                }
                            }
                        }
                        mListMaxyDevice.set(pos1, mBox);
                        mMaxyDeviceAdapter.notifyDataChanged(mListMaxyDevice);
                        removeBox2();
                        listDevice.addAll(mListMaxyDevice);
                        updateExtraOnBox();
                        updateCountDevice();
                    }
                    break;
                case CODE_MAXY_EXTRA_DETAIL:
                    MaxyExtra mMaxyExtra = data.getParcelableExtra(OBJECT_MAXY_EXTRA_DETAIL);
                    if (mMaxyExtra != null) {
                        if (this.mListMaxyExtra.size() >= 1) {
                            for (MaxyExtra item : mListMaxyExtra) {
                                if (item.getName().equals(mMaxyExtra.getName())) {
                                    mListMaxyExtra.remove(item);
                                    mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
                                    return;
                                }
                            }
                        }
                        mListMaxyExtra.add(mMaxyExtra);
                        mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
                    }
                    break;
                //3.22

                case CODE_MAXY_TYPE_DEPLOY:
                    MaxyTypeDeploy mMaxyTypeDeploy = data.getParcelableExtra(OBJECT_MAXY_TYPE_DEPLOY);
                    if (mMaxyTypeDeploy != null) {
                        clearMaxyContainer(false);
                        mMaxyDeploy = mMaxyTypeDeploy;
                        mTypeDeploy.setText(mMaxyTypeDeploy.getTypeDeployName());
                        mMaxyGeneral.setTypeDeployID(mMaxyTypeDeploy.getTypeDeployID());
                        mMaxyGeneral.setTypeDeployName(mMaxyTypeDeploy.getTypeDeployName());
                        frmMaxyContainer.setVisibility(View.VISIBLE);
                    }
                    if (mMaxyDeploy.getTypeDeployID() == 4 || mMaxyDeploy.getTypeDeployID() == 2) {
                        frmDeviceMaxy.setVisibility(View.GONE);
                        clearBox();
                    } else {
                        frmDeviceMaxy.setVisibility(View.VISIBLE);
                    }
                    break;

                case CODE_MAXY_SETUP_TYPE:
                    MaxySetupType mSetup = data.getParcelableExtra(OBJECT_MAXY_SETUP_TYPE);
                    if (mSetup != null) {
                        mMaxySetup = mSetup;
                        mSetupType.setText(mSetup.getTypeSetupName());
                        mMaxyGeneral.setTypeSetupID(mSetup.getTypeSetupID());
                        mMaxyGeneral.setTypeSetupName(mSetup.getTypeSetupName());
                        if (mMaxySetup.getTypeSetupID() == 3) {
                            frmMaxyMac.setVisibility(View.VISIBLE);
                            btnSelectMacMaxy.setVisibility(View.VISIBLE);
                        } else {
                            frmMaxyMac.setVisibility(View.GONE);
                            btnSelectMacMaxy.setVisibility(View.GONE);
                        }
                    }
                    break;

                case CODE_MAXY_BOX_1:
                    MaxyBox mBoxFirst = data.getParcelableExtra(OBJECT_MAXY_DEVICE_BOX_1_DETAIL);
                    assert mBoxFirst != null;
                    removeBox1();
                    maxyBox1 = new MaxyBox();
                    maxyBox1 = mBoxFirst;
                    mPromotionBox1.getText().clear();
                    textPriceBox1.setText(Common.formatMoney(String.valueOf(0)));
                    mBox1.setText(mBoxFirst.getBoxName());
                    break;

                case CODE_MAXY_PROMOTION_BOX_1:
                    MaxyBox mProBoxFirst = data.getParcelableExtra(OBJECT_MAXY_PROMOTION_BOX_1);
                    if (mProBoxFirst != null) {
                        maxyBox1 = mProBoxFirst;
                        removeBox1();
                        listDevice.add(mProBoxFirst);
                        mPromotionBox1.setText(mProBoxFirst.getPromotionDesc());
                        textPriceBox1.setText(Common.formatMoney(String.valueOf(mProBoxFirst.getPrice())));
                    }
                    assert mProBoxFirst != null;
                    if (mProBoxFirst.getBoxType() == 1) {
                        updateExtraOnBox();
                    }
                    updateExtraOnBox();
                    updateCountDevice();
                    break;

                case CODE_CHOOSE_MAC_MAXY:
                    if (macListMaxy.size() > 0) {
                        macListMaxy.clear();
                    }
                    macListMaxy = data.getParcelableArrayListExtra(Constants.LIST_MAC_SELECTED);
                    mMaxyGeneral.setListMACOTT(macListMaxy);
                    macMaxyAdapter.notifyData(macListMaxy);
                    break;
            }
        }
    }

    private void removeBox1() {
        List<MaxyBox> mBox = new ArrayList<>();
        for (MaxyBox item : listDevice) {
            if (item.getBoxFirst() == 1) {
                mBox.add(item);
                Log.e("listDeviceBox1", mBox.size() + "");
            }
        }
        listDevice.removeAll(mBox);
    }

    private void removeBox2() {
        List<MaxyBox> mBox = new ArrayList<>();
        for (MaxyBox item : listDevice) {
            if (item.getBoxFirst() == 2) {
                mBox.add(item);
                Log.e("listDeviceBox2", mBox.size() + "");
            }
        }
        listDevice.removeAll(mBox);
        updateCountDevice();
    }

    private void updateExtraOnBox() {
        for (MaxyExtra item : mListMaxyExtra) {
            item.setChargeTimes(1);
        }
        mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
    }

    public int countBoxOnNet() {
        updateCountDevice();
        int count = 0;
        for (MaxyBox item : listDevice) {
            if (item.getBoxType() == 1) {
                count += item.getBoxCount();
            }
        }
        return count;
    }

    public int countExtraBox() {
        int count = 0;
        for (MaxyExtra item : mListMaxyExtra) {
            if (item.getExtraType() == 1) {
                count += 1;
            }
        }
        return count;
    }

    @SuppressLint({"UseSparseArrays", "CutPasteId"})
    @Override
    protected void initView(View view) {
        frmMain = view.findViewById(R.id.frm_main);
        tvLocalType = view.findViewById(R.id.tv_local_type);
        tvServiceType = view.findViewById(R.id.tv_service_type);
        lblPromotionInternetDetail = view.findViewById(R.id.lbl_promotion_internet_detail);
        txtPromotion = view.findViewById(R.id.txt_promotion);
        txtPromotion.setFocusable(false);
        txtPromotion.setInputType(InputType.TYPE_NULL);
        txtPromotionBox1 = view.findViewById(R.id.txt_promotion_box1);
        txtPromotionBox2 = view.findViewById(R.id.txt_promotion_box2);
        frmInternet = view.findViewById(R.id.frm_internet);
        frmIpTv = view.findViewById(R.id.frm_ip_tv);
        frmIpTv.setVisibility(View.GONE);
        radIpTvSetUpYes = view.findViewById(R.id.rad_iptv_request_setup_yes);
        radIpTvSetUpNo = view.findViewById(R.id.rad_ip_tv_request_setup_no);
        radIPTVDrillWallYes = view.findViewById(R.id.rad_ip_tv_request_drill_the_wall_yes);
        radIPTVDrillWallNo = view.findViewById(R.id.rad_ip_tv_request_drill_the_wall_no);
        spIPTVCombo = view.findViewById(R.id.sp_iptv_combo);
        spIPTVFormDeployment = view.findViewById(R.id.sp_iptv_form_deployment);
        imgIPTVBoxLess = view.findViewById(R.id.img_btn_iptv_box_less);
        txtIPTVBoxCount = view.findViewById(R.id.txt_iptv_box_count);
        imgIPTVBoxPlus = view.findViewById(R.id.img_btn_iptv_box_plus);
        imgIPTVPLCLess = view.findViewById(R.id.img_btn_iptv_plc_less);
        lblIPTVPLCCount = view.findViewById(R.id.txt_iptv_plc_count);
        imgIPTVPLCPlus = view.findViewById(R.id.img_btn_iptv_plc_plus);
        imgIPTVReturnSTBLess = view.findViewById(R.id.img_btn_iptv_return_stb_less);
        lblIPTVReturnSTBCount = view.findViewById(R.id.txt_iptv_return_stb_count);
        imgIPTVReturnSTBPlus = view.findViewById(R.id.img_btn_iptv_return_stb_plus);
        spIPTVPackage = view.findViewById(R.id.sp_ip_tv_package);
        lblIPTVPromotionBoxFirstAmount = view.findViewById(R.id.lbl_iptv_promotion_box_first_amount);
        lblIPTVPromotionBoxOrderAmount = view.findViewById(R.id.lbl_iptv_promotion_box_order_amount);
        lblIPTVChargeTimesCount = view.findViewById(R.id.txt_iptv_charge_times);
        imgIPTVChargeTimesPlus = view.findViewById(R.id.img_btn_iptv_charge_times_plus);
        txtIPTVPrepaid = view.findViewById(R.id.txt_iptv_prepaid);
        frmIPTVExtra = view.findViewById(R.id.frm_iptv_extra);
        frmIPTVExtraNavigation = view.findViewById(R.id.frm_iptv_extra_navigation);
        imgIPTVExtraNavigator = view.findViewById(R.id.img_iptv_extra_navigation_drop_down);
        chbIpTvHbo = view.findViewById(R.id.chb_iptv_hbo);
        chbIPTVFimPlus = view.findViewById(R.id.chb_iptv_fim_plus);
        chbIpTvFimHot = view.findViewById(R.id.chb_iptv_fim_hot);
        lblIPTVFimPlus_ChargeTime = view.findViewById(R.id.lbl_iptv_fim_plus_charge_times);
        lblIPTVFimHot_ChargeTime = view.findViewById(R.id.lbl_iptv_fim_hot_charge_times);
        chbIPTVKPlus = view.findViewById(R.id.chb_iptv_k_plus);
        lblIPTVKPlusChargeTimesCount = view.findViewById(R.id.txt_iptv_k_plus_charge_times);
        chbIpTvVtc = view.findViewById(R.id.chb_iptv_vtc);
        lblIPTVVTCChargeTimesCount = view.findViewById(R.id.txt_iptv_vtc_charge_times);
        lblIPTVVTCPrepaidMonthCount = view.findViewById(R.id.txt_iptv_vtc_month_count);
        chbIpTvVtv = view.findViewById(R.id.chb_iptv_vtv_cab);
        lblIPTVVTVChargeTimesCount = view.findViewById(R.id.txt_iptv_vtv_cab_charge_times);
        btnShowKeyWord = view.findViewById(R.id.btn_show_key_word);
        imgPromHasImg = view.findViewById(R.id.img_prom_has_image);
        imgPromHasVideo = view.findViewById(R.id.img_prom_has_video);
        imgIPTVHBOChargeTimeLess = view.findViewById(R.id.img_btn_iptv_hbo_charge_times_less);
        lblIPTVHBOChargeTimesCount = view.findViewById(R.id.txt_ip_tv_hbo_charge_times);
        imgIPTVHBOChargeTimePlus = view.findViewById(R.id.img_btn_iptv_hbo_charge_times_plus);
        imgIPTVHBOPrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_hbo_month_count_less);
        imgIPTVHBOPrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_hbo_month_plus);
        lblIPTVHBOPrepaidMonthCount = view.findViewById(R.id.txt_iptv_hbo_month_count);
        frmIPTVFimPlus = view.findViewById(R.id.frm_iptv_fim_plus);
        frmIpTvFimHot = view.findViewById(R.id.frm_iptv_fim_hot);
        lblIPTVFimPlusAmount = view.findViewById(R.id.lbl_iptv_fim_plus_amount);
        imgIPTVFimPlus_ChargeTimeLess = view.findViewById(R.id.img_iptv_fim_plus_charge_time_less);
        imgIPTVFimPlus_ChargeTimePlus = view.findViewById(R.id.img_iptv_fim_plus_charge_time_plus);
        imgIPTVFimPlus_PrepaidMonthLess = view.findViewById(R.id.img_iptv_fim_plus_month_count_less);
        imgIPTVFimPlus_PrepaidMonthPlus = view.findViewById(R.id.img_iptv_fim_plus_month_count_plus);
        lblIPTVFimPlus_MonthCount = view.findViewById(R.id.lbl_iptv_fim_plus_month_count);
        lblIPTVFimHotAmount = view.findViewById(R.id.lbl_iptv_fim_hot_amount);
        imgIPTVFimHot_ChargeTimeLess = view.findViewById(R.id.img_iptv_fim_hot_charge_time_less);
        imgIPTVFimHot_ChargeTimePlus = view.findViewById(R.id.img_iptv_fim_hot_charge_time_plus);
        imgIPTVFimHot_PrepaidMonthLess = view.findViewById(R.id.img_iptv_fim_hot_month_count_less);
        lblIPTVFimHot_MonthCount = view.findViewById(R.id.lbl_iptv_fim_hot_month_count);
        imgIPTVFimHot_PrepaidMonthPlus = view.findViewById(R.id.img_iptv_fim_hot_month_count_plus);
        frmIPTVKPlus = view.findViewById(R.id.frm_iptv_k_plus);
        lblIPTVKPlusAmount = view.findViewById(R.id.lbl_iptv_k_plus_amount);
        lblIPTVKPlusPrepaidMonthCount = view.findViewById(R.id.txt_iptv_k_plus_month_count);
        imgIPTVKPlusChargeTimeLess = view.findViewById(R.id.img_btn_iptv_k_plus_charge_times_less);
        imgIPTVKPlusChargeTimePlus = view.findViewById(R.id.img_btn_iptv_k_plus_charge_times_plus);
        imgIPTVKPlusPrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_k_plus_month_count_pless);
        imgIPTVKPlusPrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_k_plus_month_plus);
        frmIpTvVtc = view.findViewById(R.id.frm_iptv_vtc_hd);
        lblIPTVVTCAmount = view.findViewById(R.id.lbl_iptv_vtc_amount);
        imgIPTVVTCChargeTimeLess = view.findViewById(R.id.img_btn_iptv_vtc_charge_times_less);
        imgIPTVVTCChargeTimePlus = view.findViewById(R.id.img_btn_iptv_vtc_charge_times_plus);
        imgIPTVVTCPrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_vtc_month_count_pless);
        imgIPTVVTCPrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_vtc_month_plus);
        frmIpTvVtv = view.findViewById(R.id.frm_iptv_vtv_cap);
        lblIPTVVTVAmount = view.findViewById(R.id.lbl_iptv_vtv_cab_amount);
        lblIPTVVTVPrepaidMonthCount = view.findViewById(R.id.txt_iptv_vtv_cab_month_count);
        imgIPTVVTVChargeTimeLess = view.findViewById(R.id.img_btn_iptv_vtv_cab_charge_times_less);
        imgIPTVVTVChargeTimePlus = view.findViewById(R.id.img_btn_iptv_vtv_cab_charge_times_plus);
        imgIPTVVTVPrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_vtv_cab_month_count_pless);
        imgIPTVVTVPrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_vtv_cab_month_plus);
        frmIPTVFimStandard = view.findViewById(R.id.frm_ip_tv_fim_standard);
        chbIPTVFimStandard = view.findViewById(R.id.chb_ip_tv_fim_standard);
        lblIPTVFimStandard_ChargeTimes = view.findViewById(R.id.lbl_ip_tv_fim_standard_charge_times);
        lblIPTVFimStandard_Amount = view.findViewById(R.id.lbl_ip_tv_fim_standard_amount);
        lblIPTVFimStandard_MonthCount = view.findViewById(R.id.lbl_ip_tv_fim_standard_month_count);
        imgIPTVFimStandard_ChargeTimeLess = view.findViewById(R.id.img_ip_tv_fim_standard_charge_time_less);
        imgIPTVFimStandard_ChargeTimePlus = view.findViewById(R.id.img_ip_tv_fim_standard_charge_time_plus);
        imgIPTVFimStandard_MonthCountLess = view.findViewById(R.id.img_ip_tv_fim_standard_month_count_less);
        imgIPTVFimStandard_MonthCountPlus = view.findViewById(R.id.img_ip_tv_fim_standard_month_count_plus);
        frmDevicesBanner = view.findViewById(R.id.frm_devices);
        btnSelectDevice = view.findViewById(R.id.btn_select_device);
        RecyclerView mRecyclerViewDevice = view.findViewById(R.id.lv_list_device);
        frmContentFptBox = view.findViewById(R.id.frm_content_fpt_box);
        frmContentFptBox.setVisibility(View.GONE);
        frmListFptBox = view.findViewById(R.id.frm_list_fpt_box);
        RecyclerView listViewFptBox = view.findViewById(R.id.lv_list_fpt_box);
        RecyclerView listViewMac = view.findViewById(R.id.lv_list_mac);
        btnChooseMac = view.findViewById(R.id.btn_scan_mac);
        btnSelectFptBox = view.findViewById(R.id.btn_select_fpt_box);

        mListServiceSelected = new ArrayList<>();

        mListDeviceSelected = new ArrayList<>();
        mDeviceAdapter = new DeviceAdapter(this.getActivity(), mListDeviceSelected, this);
        mRecyclerViewDevice.setAdapter(mDeviceAdapter);

        listPromotionFptBox = new HashMap<>();
        listFptBoxSelect = new ArrayList<>();
        macList = new ArrayList<>();
        listNameFptBox = new ArrayList<>();
        fptBoxAdapter = new FPTBoxAdapter(this.getActivity(), listFptBoxSelect, listPromotionFptBox);
        listViewFptBox.setAdapter(fptBoxAdapter);
        macAdapter = new MacAdapter(getActivity(), macList);
        listViewMac.setAdapter(macAdapter);

        cbFoxy2 = view.findViewById(R.id.chb_iptv_foxy_2);
        imgFoxy2ChargeTimeLess = view.findViewById(R.id.img_btn_iptv_foxy_2_charge_times_less);
        imgFoxy2ChargeTimePlus = view.findViewById(R.id.img_btn_iptv_foxy_2_charge_times_plus);
        imgFoxy2PrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_foxy_2_month_count_pless);
        imgFoxy2PrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_foxy_2_month_count_plus);
        tvFoxy2ChargeTime = view.findViewById(R.id.txt_iptv_foxy_2_charge_times);
        tvFoxy2PrepaidMonth = view.findViewById(R.id.txt_iptv_foxy_2_month_count);
        tvFoxy2Amount = view.findViewById(R.id.lbl_iptv_foxy_2_amount);

        cbFoxy4 = view.findViewById(R.id.chb_iptv_foxy_4);
        imgFoxy4ChargeTimeLess = view.findViewById(R.id.img_btn_iptv_foxy_4_charge_times_less);
        imgFoxy4ChargeTimePlus = view.findViewById(R.id.img_btn_iptv_foxy_4_charge_times_plus);
        imgFoxy4PrepaidMonthLess = view.findViewById(R.id.img_btn_iptv_foxy_4_month_count_pless);
        imgFoxy4PrepaidMonthPlus = view.findViewById(R.id.img_btn_iptv_foxy_4_month_count_plus);
        tvFoxy4ChargeTime = view.findViewById(R.id.txt_iptv_foxy_4_charge_times);
        tvFoxy4PrepaidMonth = view.findViewById(R.id.txt_iptv_foxy_4_month_count);
        tvFoxy4Amount = view.findViewById(R.id.lbl_iptv_foxy_4_amount);

        frmDacSacHD = view.findViewById(R.id.dac_sac_hd);
        frmDacSacHD.setVisibility(View.GONE);

        //ver 3.19
        frmCamera = view.findViewById(R.id.frm_camera);
        frmCamera.setVisibility(View.GONE);
        btnAddCamera = view.findViewById(R.id.btn_add_camera);
        btnAddCloud = view.findViewById(R.id.btn_add_cloud_service);
        RecyclerView mRecyclerViewCamera = view.findViewById(R.id.list_camera_device_final);
        RecyclerView mRecyclerViewCloud = view.findViewById(R.id.list_cloud_service_final);
        rdoDeploy = view.findViewById(R.id.rdo_deploy);
        rdoNoDeploy = view.findViewById(R.id.rdo_receive_on_bar);
        loQuantityDeploy = view.findViewById(R.id.layout_deploy);
        tvSetupDetailPrice = view.findViewById(R.id.tv_camera_deploy_cost);
        tvSetupDetailQuantity = view.findViewById(R.id.lbl_camera_deploy_quantity);
        tvSetupDetailLess = view.findViewById(R.id.tv_camera_deploy_less);
        tvSetupDetailPlus = view.findViewById(R.id.tv_camera_deploy_plus);
        txtPromotionCloud = view.findViewById(R.id.txt_promotion_cloud);
        lbliptvPromotionCamera = view.findViewById(R.id.lbl_iptv_promotion_camera_first_amount);

        mListCamera = new ArrayList<>();
        mListCamera = modelDetail.getObjectCameraOfNet().getCameraDetail() != null ?
                modelDetail.getObjectCameraOfNet().getCameraDetail() : new ArrayList<CameraDetail>();
        mCameraAdapter = new CameraDetailV2Adapter(getActivity(), mListCamera, this, this);
        mRecyclerViewCamera.setAdapter(mCameraAdapter);

        mListCloud = new ArrayList<>();
        mListCloud = modelDetail.getObjectCameraOfNet().getCloudDetail() != null ?
                modelDetail.getObjectCameraOfNet().getCloudDetail() : new ArrayList<CloudDetail>();
        mCloudAdapter = new CloudDetailV2Adapter(getActivity(), mListCloud, this, this);
        mRecyclerViewCloud.setAdapter(mCloudAdapter);

        mSetupDetail = modelDetail.getObjectCameraOfNet().getSetupDetail();

        if (modelDetail.getObjectCameraOfNet().getSetupDetail().getType() == 1) {//có triển khai
            isDeploy();
        } else {
            isNoDeploy();
        }

        rdoDeploy.setEnabled(modelDetail.getObjectCameraOfNet().getCameraDetail().size() != 0);
        if (modelDetail.getObjectCameraOfNet().getPromotionDetail() != null) {
            setupPromotionCloud(true);
            txtPromotionCloud.setText(modelDetail.getObjectCameraOfNet().getPromotionDetail().getPromoName());
            this.mProCloud = modelDetail.getObjectCameraOfNet().getPromotionDetail();
        } else {
            modelDetail.getObjectCameraOfNet().setPromotionDetail(new PromoCloud());
            this.mProCloud = new PromoCloud();
            setupPromotionCloud(true);
        }

        //Version 3.21 nhannh26
        //container
        frmMaxyTV = view.findViewById(R.id.frm_maxy_tv);
        frmMaxyContainer = view.findViewById(R.id.view_maxy_container);
        frmMaxyContainer.setVisibility(View.GONE);
        mTypeDeploy = view.findViewById(R.id.txt_type_deploy);
        //Combo radius
        rdoRegisterComboYes = view.findViewById(R.id.rdo_combo_yes);
        rdoRegisterComboYes.setChecked(true);
        rdoRegisterComboNo = view.findViewById(R.id.rdo_combo_no);
        //----------------------------------------------------------------------------------------//
        //package
        selPackage = view.findViewById(R.id.txt_trienkhai);
        selGetCLKM = view.findViewById(R.id.txt_clkm);
        textMaxyGeneral = view.findViewById(R.id.text_total_price_maxy_genegal);
        recPackageMaxyTV = view.findViewById(R.id.list_package_extra);
        //----------------------------------------------------------------------------------------//
        //mac offnet
        frmMaxyMac = view.findViewById(R.id.view_list_mac_maxy);
        recMacMaxy = view.findViewById(R.id.lv_list_mac_maxy);
        macListMaxy = new ArrayList<>();
        macListMaxy = modelDetail.getObjectMaxy().getMaxyGeneral().getListMACOTT() != null ?
                modelDetail.getObjectMaxy().getMaxyGeneral().getListMACOTT() : new ArrayList<>();
        macMaxyAdapter = new MacMaxyAdapter(getActivity(), macListMaxy);
        recMacMaxy.setAdapter(macMaxyAdapter);
        //----------------------------------------------------------------------------------------//
        //device box
        frmDeviceMaxy = view.findViewById(R.id.view_devices);
        mBox1 = view.findViewById(R.id.txt_device_box1);
        mPromotionBox1 = view.findViewById(R.id.txt_maxy_promotion_box1);
        textPriceBox1 = view.findViewById(R.id.txt_total_price_box1);
        recDeviceMaxyTV = view.findViewById(R.id.list_device_maxy_tv);
        btnSelectDeviceMaxyTV = view.findViewById(R.id.btn_add_device_maxy_tv);
        listDevice = new ArrayList<>();
        listDevice = modelDetail.getObjectMaxy().getMaxyBox() != null ?
                modelDetail.getObjectMaxy().getMaxyBox() : new ArrayList<>();
        mListMaxyDevice = new ArrayList<>();
        mMaxyDeviceAdapter = new MaxyTVDeviceAdapter(getActivity(),
                mListMaxyDevice, this, this);
        recDeviceMaxyTV.setAdapter(mMaxyDeviceAdapter);
        maxyBox1 = new MaxyBox();
        mMaxyDeploy = new MaxyTypeDeploy();
        for (MaxyBox item : listDevice) {
            if (item.getBoxFirst() != 1) {
                mListMaxyDevice.add(item);
            } else {
                maxyBox1 = item;
                mBox1.setText(item.getBoxName());
                mPromotionBox1.setText(item.getPromotionDesc());
                textPriceBox1.setText(Common.formatMoney(String.valueOf(item.getPrice())));
            }
        }
        updateCountDevice();
        //----------------------------------------------------------------------------------------//
        //extra
        btnSelectExtraMaxyTV = view.findViewById(R.id.btn_add_package_extra);
        mListMaxyExtra = new ArrayList<>();
        mListMaxyExtra = modelDetail.getObjectMaxy().getMaxyExtra() != null ?
                modelDetail.getObjectMaxy().getMaxyExtra() : new ArrayList<>();
        mMaxyExtraAdapter = new MaxyTVExtraAdapter(getActivity(),
                mListMaxyExtra, this, this);
        recPackageMaxyTV.setAdapter(mMaxyExtraAdapter);
        //----------------------------------------------------------------------------------------//
        //mac
        mSetupType = view.findViewById(R.id.txt_type_deployment);
        btnSelectMacMaxy = view.findViewById(R.id.btn_scan_mac_maxy);
        //check cập nhật bán mới
        if (modelDetail.getObjectMaxy().getMaxyGeneral() != null
                && modelDetail.getObjectMaxy().getMaxyGeneral().getPackageID() != 0) {
            this.mMaxyGeneral = modelDetail.getObjectMaxy().getMaxyGeneral();
            //deploy
            numBox = mMaxyGeneral.getNumBox();
            mMaxyDeploy.setTypeDeployID(mMaxyGeneral.getTypeDeployID());
            mMaxyDeploy.setTypeDeployName(mMaxyGeneral.getTypeDeployName());
            mTypeDeploy.setText(mMaxyDeploy.getTypeDeployName());
            frmMaxyContainer.setVisibility(View.VISIBLE);
            if (mMaxyGeneral.getTypeDeployID() == 4 || mMaxyGeneral.getTypeDeployID() == 2) {
                frmDeviceMaxy.setVisibility(View.GONE);
            } else {
                frmDeviceMaxy.setVisibility(View.VISIBLE);
            }
            //setup
            mSetupType.setText(mMaxyGeneral.getTypeSetupName());
            if (mMaxyGeneral.getTypeSetupID() == 3) {
                frmMaxyMac.setVisibility(View.VISIBLE);
                macMaxyAdapter.notifyData(macListMaxy);
            } else {
                frmMaxyMac.setVisibility(View.GONE);
            }
            selPackage.setText(mMaxyGeneral.getPackage());
            selGetCLKM.setText(mMaxyGeneral.getPromotionDesc());
            textMaxyGeneral.setText(Common.formatMoney(String.valueOf(mMaxyGeneral.getPrice())));
            //combo
            if (modelDetail.getObjectMaxy().getMaxyGeneral().getUseCombo() > 0) {
                rdoRegisterComboYes.setChecked(true);
            } else {
                rdoRegisterComboNo.setChecked(true);
            }
        } else {
            modelDetail.getObjectMaxy().setMaxyGeneral(new MaxyGeneral());
            this.mMaxyGeneral = new MaxyGeneral();
            mMaxyGeneral.setUseCombo(3);
        }
    }

    @Override
    protected void initEvent() {
        txtPromotion.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterActivity();
            }
        });
        txtPromotion.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    lblPromotionInternetDetail.setText(txtPromotion.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        txtPromotionBox1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideSoftKeyboard(txtPromotionBox1);
                showFilterActivity(true, 1);
            }
        });
        txtPromotionBox1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    // lấy giá gói IPTV Extra box 1
                    getIPTVPrice();
                    lblIPTVPromotionBoxFirstAmount.setVisibility(View.VISIBLE);
                }
            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        txtPromotionBox2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideSoftKeyboard(txtPromotionBox2);
                showFilterActivity(true, 2);
            }
        });
        txtPromotionBox2.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showFilterActivity(true, 2);
                    Common.hideSoftKeyboard(txtPromotionBox2);
                }
            }
        });
        txtPromotionBox2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    lblIPTVPromotionBoxOrderAmount.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        txtIPTVBoxCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    int boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    if (boxCount >= 2) {
                        txtPromotionBox2.setEnabled(true);
                        getIPTVPromotionBoxOrder();
                    } else if (boxCount == 1) {
                        try {
                            txtPromotionBox2.setText(null);
                            txtPromotionBox2.setEnabled(false);
                            promotionIPTVModelBox2 = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        lblIPTVPromotionBoxOrderAmount.setText(null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnShowKeyWord.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PromotionKeyWordSearchDialog dialog = new PromotionKeyWordSearchDialog(getActivity());
                Common.showFragmentDialog(getFragmentManager(), dialog,
                        "fragment_promotion_keyWord_search_dialog");
            }
        });
        imgPromHasImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedInternetPromotion != null) {
                    String imagePath = selectedInternetPromotion.getImageUrl();
                    if (imagePath != null && !imagePath.equals("")) {
                        Intent intent = new Intent(getActivity(), ShowImageActivity.class);
                        intent.putExtra("PATH", imagePath);
                        startActivity(intent);
                    }
                }
            }
        });
        tvServiceType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelDetail != null) {//cập nhật
                    Bundle bundle = new Bundle();
                    List<CategoryServiceList> lstSv = new ArrayList<>();
                    lstSv.add(new CategoryServiceList(1, "Internet"));
                    lstSv.add(new CategoryServiceList(2, "Thiết bị"));
                    lstSv.add(new CategoryServiceList(7, "FPT Camera"));
                    lstSv.add(new CategoryServiceList(8, "FPT Play"));
                    bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                            (ArrayList<? extends Parcelable>) lstSv);
                    bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST_SELECTED,
                            (ArrayList<? extends Parcelable>) mListServiceSelected);
                    Intent intent = new Intent(getActivity(), ServiceTypeActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_SERVICE_TYPE);
                } else {//tạo PDK mới
                    if (((RegisterActivityNew) getActivity()).getListService().size() > 1) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                                (ArrayList<? extends Parcelable>) ((RegisterActivityNew) getActivity()).getListService());
                        bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST_SELECTED,
                                (ArrayList<? extends Parcelable>) mListServiceSelected);
                        Intent intent = new Intent(getActivity(), ServiceTypeActivity.class);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, CODE_SERVICE_TYPE);
                    }
                }
            }
        });
        tvLocalType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((RegisterActivityNew) getActivity()).getListService().size() > 1) {
                    if (TextUtils.isEmpty(tvServiceType.getText().toString())) {
                        Toast.makeText(getActivity(), "Chưa chọn loại dịch vụ", Toast.LENGTH_SHORT).show();
                    } else {
                        getLocalType();
                    }
                } else {
                    getLocalType();
                }
            }
        });
        tvLocalType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtPromotion.setText(null);
                localTypeId = -1;
            }
        });

        btnSelectDevice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Constants.DEVICE_LIST_SELECTED,
                        (ArrayList<? extends Parcelable>) mListDeviceSelected);
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                Intent intent = new Intent(getActivity(), DeviceListActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_DEVICE_LIST_SELECTED);

            }
        });

        btnSelectFptBox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                isBind = 1;
                new GetFPTBoxList(getActivity(), FragmentRegisterStep3v2.this);
            }
        });

        btnChooseMac.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), ChooseMacActivity.class);
            intent.putParcelableArrayListExtra(Constants.LIST_FPT_BOX_SELECTED,
                    (ArrayList<? extends Parcelable>) listFptBoxSelect);
            intent.putParcelableArrayListExtra(Constants.LIST_MAC_SELECTED,
                    (ArrayList<? extends Parcelable>) macList);
            startActivityForResult(intent, CODE_CHOOSE_MAC);
        });

        radIpTvSetUpNo.setChecked(true);
        radIPTVDrillWallNo.setChecked(true);
        chbIpTvHbo.setOnCheckedChangeListener(this);
        chbIpTvFimHot.setOnCheckedChangeListener(this);
        chbIPTVFimPlus.setOnCheckedChangeListener(this);
        chbIPTVKPlus.setOnCheckedChangeListener(this);
        chbIpTvVtc.setOnCheckedChangeListener(this);
        chbIpTvVtv.setOnCheckedChangeListener(this);
        chbIPTVFimStandard.setOnCheckedChangeListener(this);
        spIPTVFormDeployment.setSelection(0, true);
        imgIPTVVTVPrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtv.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVVTVPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVVTVPrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtv.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVVTVPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVVTVChargeTimePlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtv.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVVTVChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVVTVChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtv.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVVTVChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        frmIpTvVtv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIpTvVtv.isChecked();
                chbIpTvVtv.setChecked(!checked);
            }
        });

        imgIPTVVTCPrepaidMonthPlus.setOnClickListener(new OnClickListener() {

            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtc.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVVTCPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVVTCPrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtc.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVVTCPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVVTCChargeTimePlus.setOnClickListener(new OnClickListener() {

            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtc.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVVTCChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVVTCChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvVtc.isChecked()) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVVTCChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        frmIpTvVtc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIpTvVtc.isChecked();
                chbIpTvVtc.setChecked(!checked);
            }
        });
        imgIPTVKPlusPrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVKPlusPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVKPlusPrepaidMonthLess.setOnClickListener(new OnClickListener() {

            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVKPlusPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVKPlusChargeTimePlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVKPlusChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVKPlusChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVKPlusChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        frmIPTVKPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIPTVKPlus.isChecked();
                chbIPTVKPlus.setChecked(!checked);
            }
        });
        // Số tháng trả trước FimHot
        imgIPTVFimHot_PrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvFimHot.isChecked()) {
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVFimHot_MonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgIPTVFimHot_PrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvFimHot.isChecked()) {
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVFimHot_MonthCount.setText(String.valueOf(count));
                }
            }
        });
        // Số lần tính cước FimHot
        imgIPTVFimHot_ChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvFimHot.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_ChargeTime.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVFimHot_ChargeTime.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgIPTVFimHot_ChargeTimePlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvFimHot.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_ChargeTime.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVFimHot_ChargeTime.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVFimPlus_PrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVFimPlus_MonthCount.setText(String.valueOf(count));
                }
            }
        });
        // Số tháng trả trước Fim+
        imgIPTVFimPlus_PrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVFimPlus_MonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgIPTVFimPlus_ChargeTimePlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_ChargeTime.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVFimPlus_ChargeTime.setText(String.valueOf(count));
                }
            }
        });
        // Số lần tính cước Fim+
        imgIPTVFimPlus_ChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_ChargeTime.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVFimPlus_ChargeTime.setText(String.valueOf(count));
                    }
                }
            }
        });

        frmIpTvFimHot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIpTvFimHot.isChecked();
                chbIpTvFimHot.setChecked(!checked);
            }
        });

        frmIPTVFimPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIPTVFimPlus.isChecked();
                chbIPTVFimPlus.setChecked(!checked);
            }
        });

        //Cac nút tăng giảm trả trước các gói
        imgIPTVHBOPrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvHbo.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVHBOPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVHBOPrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvHbo.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        // lại tiền IPTV)
                        count--;
                        lblIPTVHBOPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVHBOChargeTimePlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvHbo.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVHBOChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVHBOChargeTimeLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                if (chbIpTvHbo.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOChargeTimesCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVHBOChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVChargeTimesPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVChargeTimesCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int boxCount;
                try {
                    boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    if (count < boxCount)
                        count++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                lblIPTVChargeTimesCount.setText(String.valueOf(count));
            }
        });

        spIPTVPackage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                int packageID = selectedItem.getID();
                if (packageID != 0) {
                    new GetPromotionIPTVList(getActivity(), FragmentRegisterStep3v2.this, selectedItem.getHint(),
                            1, customerType, contractParam, regCodeParam);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        imgIPTVReturnSTBPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVReturnSTBCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                count++;
                lblIPTVReturnSTBCount.setText(String.valueOf(count));
            }
        });

        imgIPTVReturnSTBLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVReturnSTBCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (count > 0) {
                    count--;
                    lblIPTVReturnSTBCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVPLCPlus.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVPLCCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                count++;
                lblIPTVPLCCount.setText(String.valueOf(count));
            }
        });

        imgIPTVPLCLess.setOnClickListener(new OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVPLCCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (count > 0) {
                    count--;
                    lblIPTVPLCCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVBoxPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // IPTV)
                if (spIPTVPackage.getSelectedItemPosition() == 0)
                    spIPTVPackage.setSelection(1, true);
                int boxCount = 0;
                try {
                    boxCount = Integer.parseInt(txtIPTVBoxCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                boxCount++;
                txtIPTVBoxCount.setText(String.valueOf(boxCount));
            }
        });
        imgIPTVBoxLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // IPTV)
                int boxCount = 0;
                try {
                    boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (boxCount > 1) {
                    boxCount--;
                    txtIPTVBoxCount.setText(String.valueOf(boxCount));
                }
            }
        });
        imgIPTVFimStandard_MonthCountPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chbIPTVFimStandard.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimStandard_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblIPTVFimStandard_MonthCount.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVFimStandard_MonthCountLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chbIPTVFimStandard.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimStandard_MonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVFimStandard_MonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVFimStandard_ChargeTimePlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chbIPTVFimStandard.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    int boxCount = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimStandard_ChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVFimStandard_ChargeTimes.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVFimStandard_ChargeTimeLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chbIPTVFimStandard.isChecked()) {
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimStandard_ChargeTimes.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVFimStandard_ChargeTimes.setText(String.valueOf(count));
                    }
                }
            }
        });
        frmIPTVFimStandard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = chbIPTVFimStandard.isChecked();
                chbIPTVFimStandard.setChecked(!checked);
            }
        });

        //foxy 2
        imgFoxy2ChargeTimeLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy2.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy2ChargeTime.getText().toString());
                if (quantity == 1) {
                    return;
                }
                quantity--;
                tvFoxy2ChargeTime.setText(String.valueOf(quantity));
            }
        });

        imgFoxy2ChargeTimePlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy2.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy2ChargeTime.getText().toString());
                int maxQuantity = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                if (quantity < maxQuantity) {
                    quantity++;
                }
                tvFoxy2ChargeTime.setText(String.valueOf(quantity));
            }
        });

        imgFoxy2PrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy2.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy2PrepaidMonth.getText().toString());
                if (quantity == 0) {
                    return;
                }
                quantity--;
                tvFoxy2PrepaidMonth.setText(String.valueOf(quantity));
            }
        });

        imgFoxy2PrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy2.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy2PrepaidMonth.getText().toString());
                quantity++;
                tvFoxy2PrepaidMonth.setText(String.valueOf(quantity));
            }
        });

        //foxy4
        imgFoxy4ChargeTimeLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy4.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy4ChargeTime.getText().toString());
                if (quantity == 1) {
                    return;
                }
                quantity--;
                tvFoxy4ChargeTime.setText(String.valueOf(quantity));
            }
        });

        imgFoxy4ChargeTimePlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy4.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy4ChargeTime.getText().toString());
                int maxQuantity = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                if (quantity < maxQuantity) {
                    quantity++;
                }
                tvFoxy4ChargeTime.setText(String.valueOf(quantity));
            }
        });

        imgFoxy4PrepaidMonthLess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy4.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy4PrepaidMonth.getText().toString());
                if (quantity == 0) {
                    return;
                }
                quantity--;
                tvFoxy4PrepaidMonth.setText(String.valueOf(quantity));
            }
        });

        imgFoxy4PrepaidMonthPlus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cbFoxy4.isChecked()) {
                    return;
                }
                int quantity = Integer.parseInt(tvFoxy4PrepaidMonth.getText().toString());
                quantity++;
                tvFoxy4PrepaidMonth.setText(String.valueOf(quantity));
            }
        });

        cbFoxy2.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                if (modelDetail == null) {
                    tvFoxy2ChargeTime.setText(String.valueOf(1));
                    tvFoxy2PrepaidMonth.setText(String.valueOf(0));
                    return;
                }
                tvFoxy2ChargeTime.setText(modelDetail.getFoxy2ChargeTimes() != 0 ?
                        String.valueOf(modelDetail.getFoxy2ChargeTimes()) : String.valueOf(1));
                tvFoxy2PrepaidMonth.setText(modelDetail.getFoxy2PrepaidMonth() != 0 ?
                        String.valueOf(modelDetail.getFoxy2PrepaidMonth()) : String.valueOf(0));
            } else {
                tvFoxy2ChargeTime.setText(String.valueOf(0));
                tvFoxy2PrepaidMonth.setText(String.valueOf(0));
            }
        });

        cbFoxy4.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    if (modelDetail == null) {
                        tvFoxy4ChargeTime.setText(String.valueOf(1));
                        tvFoxy4PrepaidMonth.setText(String.valueOf(0));
                        return;
                    }
                    tvFoxy4ChargeTime.setText(modelDetail.getFoxy4ChargeTimes() != 0 ?
                            String.valueOf(modelDetail.getFoxy4ChargeTimes()) : String.valueOf(1));
                    tvFoxy4PrepaidMonth.setText(modelDetail.getFoxy4PrepaidMonth() != 0 ?
                            String.valueOf(modelDetail.getFoxy4PrepaidMonth()) : String.valueOf(0));
                } else {
                    tvFoxy4ChargeTime.setText(String.valueOf(0));
                    tvFoxy4PrepaidMonth.setText(String.valueOf(0));
                }
            }
        });

        //----------------------------v3.19-----------------------------------//
        rdoDeploy.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                new GetCostSetupCamerav2(getContext(), FragmentRegisterStep3v2.this, modelDetail);
            }
        });

        rdoNoDeploy.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                isNoDeploy();
            }
        });

        tvSetupDetailLess.setOnClickListener(view -> {
            int quantity = mSetupDetail.getQty();
            if (quantity == 1) {
                return;
            }
            quantity--;
            mSetupDetail.setQty(quantity);
            tvSetupDetailQuantity.setText(String.valueOf(quantity));
            tvSetupDetailPrice.setText(
                    String.format(getString(R.string.txt_unit_price), Common.formatNumber(quantity * mSetupDetail.getCost()))
            );
            activity.setIsUpdatePromotion(true);
        });

        tvSetupDetailPlus.setOnClickListener(view -> {
            int quantity = mSetupDetail.getQty();
            quantity++;
            mSetupDetail.setQty(quantity);
            tvSetupDetailQuantity.setText(String.valueOf(quantity));
            tvSetupDetailPrice.setText(
                    String.format(getString(R.string.txt_unit_price), Common.formatNumber(quantity * mSetupDetail.getCost()))
            );
            activity.setIsUpdatePromotion(true);
        });


        btnAddCamera.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(LIST_CAMERA_DETAIL_SELECTED,
                    (ArrayList<? extends Parcelable>) mListCamera);
            bundle.putInt(CUSTYPE, activity.getIsCusType());
            bundle.putInt(REGTYPE, 0);
            bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
            Intent intent = new Intent(getContext(), CameraOrNetTypeActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_CAMERA_DETAIL);
        });

        btnAddCloud.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(LIST_CLOUD_DETAIL_SELECTED,
                    (ArrayList<? extends Parcelable>) mListCloud);
            bundle.putInt(CUSTYPE, activity.getIsCusType());
            bundle.putInt(REGTYPE, 0);
            bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
            Intent intent = new Intent(getContext(), CloudTypeV2Activity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_CLOUD_DETAIL);
        });

        if (mListCloud.size() == 0)
            setupPromotionCloud(false);

//        ver 3.22 author nhannh26
        mTypeDeploy.setOnClickListener(v -> {
            if (localTypeId != 0) {
                Bundle bundle = new Bundle();
                bundle.putInt(REGTYPE, 0);
                bundle.putInt(LOCAL_TYPE, localTypeId);
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                Intent intent = new Intent(getContext(), MaxyTypeDeployActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_MAXY_TYPE_DEPLOY);
            } else {
                Toast.makeText(getContext(), getString(R.string.msg_select_service_intenet), Toast.LENGTH_SHORT).show();
            }
        });

        btnSelectDeviceMaxyTV.setOnClickListener(v -> {
            if (mMaxyGeneral.getPackageID() != 0) {
                if (getBox1().getBoxID() != 0 && getBox1().getPromotionID() != 0) {
                    if (count < mMaxyGeneral.getNumBox()) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList(LIST_MAXY_DETAIL_SELECTED,
                                (ArrayList<? extends Parcelable>) mListMaxyDevice);
                        bundle.putInt(REGTYPE, 0);
                        bundle.putInt(BOX_FIRST, 2);
                        bundle.putInt(LOCAL_TYPE, localTypeId);
                        bundle.putInt(MAXY_PROMOTION_ID, mMaxyGeneral.getPromotionID());
                        bundle.putInt(MAXY_PROMOTION_TYPE, mMaxyGeneral.getPromotionType());
                        bundle.putString(TAG_CONTRACT, "");
                        bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                        Intent intent = new Intent(getActivity(), MaxyDeviceActivity.class);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, CODE_MAXY_DEVICE_DETAIL);
                    } else {
                        Toast.makeText(getContext(), getString(R.string.msg_select_num_box), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.msg_select_box1_before_box_2), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.text_select_service_tv), Toast.LENGTH_SHORT).show();
            }
        });

        btnSelectExtraMaxyTV.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(LIST_MAXY_EXTRA_SELECTED,
                    (ArrayList<? extends Parcelable>) mListMaxyExtra);
            bundle.putInt(REGTYPE, 0);
            bundle.putInt(LIST_MAXY_DETAIL_SELECTED, listDevice.size());
            bundle.putString(CONTRACT_EXTRA, "");
            bundle.putInt(NUM_EXTRA, countExtraBox());
            bundle.putInt(NUM_BOX_NET, countBoxOnNet());
            bundle.putParcelable(MAXY_GENERAL, mMaxyGeneral);
            bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
            Intent intent = new Intent(getActivity(), MaxyExtraActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_MAXY_EXTRA_DETAIL);
        });


        selPackage.setOnClickListener(v ->
        {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
            bundle.putParcelable(LIST_MAXY_PACKAGE_SELECTED, mMaxyGeneral);
            bundle.putInt(REGTYPE, 0);
            bundle.putInt(USE_COMBO, mMaxyGeneral.getUseCombo());
            bundle.putInt(LOCAL_TYPE, localTypeId);
            bundle.putString(TAG_CONTRACT, "");
            Intent intent = new Intent(getActivity(), MaxyPackageActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_MAXY_PACKAGE_DETAIL);
        });

        selGetCLKM.setOnClickListener(v ->
        {
            Bundle bundle = new Bundle();
            if (mMaxyGeneral.getPackageID() != 0) {
                bundle.putParcelable(LIST_MAXY_PROMOTION_PACKAGE, mMaxyGeneral);
                bundle.putInt(REGTYPE, 0);
                bundle.putInt(LOCAL_TYPE, localTypeId);
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                Intent intent = new Intent(getActivity(), MaxyPromotionPackageActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_MAXY_PROMOTION_PACKAGE_DETAIL);
            } else {
                Common.alertDialog("Vui lòng chọn gói dịch vụ trước khi chọn CLKM ", getContext());
            }
        });

        rdoRegisterComboYes.setOnCheckedChangeListener((buttonView, isChecked) ->
        {
            if (isChecked) {
                mMaxyGeneral.setUseCombo(3);
            }
        });

        rdoRegisterComboNo.setOnCheckedChangeListener((buttonView, isChecked) ->
        {
            if (isChecked) {
                mMaxyGeneral.setUseCombo(0);
            }
        });

        mSetupType.setOnClickListener(v ->
        {
            Bundle bundle = new Bundle();
            bundle.putInt(REGTYPE, 0);
            bundle.putInt(LOCAL_TYPE, localTypeId);
            bundle.putInt(TYPE_DEPLOY, mMaxyGeneral.getTypeDeployID());
            bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
            Intent intent = new Intent(getContext(), MaxySetupTypeActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_MAXY_SETUP_TYPE);
        });
        btnSelectMacMaxy.setOnClickListener(v ->
        {
            Intent intent = new Intent(getActivity(), ChooseMacActivity.class);
            intent.putParcelableArrayListExtra(Constants.LIST_FPT_BOX_SELECTED,
                    (ArrayList<? extends Parcelable>) listDevice);
            intent.putParcelableArrayListExtra(Constants.LIST_MAC_SELECTED,
                    (ArrayList<? extends Parcelable>) macListMaxy);
            startActivityForResult(intent, CODE_CHOOSE_MAC_MAXY);
        });
        //box thứ nhất
        mBox1.setOnClickListener(v ->
        {
            if (mMaxyGeneral.getPackageID() != 0) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                bundle.putParcelable(LIST_MAXY_DETAIL_SELECTED, maxyBox1);
                bundle.putInt(REGTYPE, 0);
                bundle.putInt(BOX_FIRST, 1);
                bundle.putInt(LOCAL_TYPE, localTypeId);
                bundle.putInt(MAXY_PROMOTION_ID, mMaxyGeneral.getPromotionID());
                bundle.putInt(MAXY_PROMOTION_TYPE, mMaxyGeneral.getPromotionType());
                bundle.putString(TAG_CONTRACT, "");
                Intent intent = new Intent(getActivity(), MaxyBox1Activity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_MAXY_BOX_1);
            } else {
                Toast.makeText(getContext(), getString(R.string.text_select_service_tv), Toast.LENGTH_SHORT).show();
            }
        });

        mPromotionBox1.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            assert maxyBox1 != null;
            if (maxyBox1.getBoxID() != 0) {
                bundle.putParcelable(LIST_MAXY_PROMOTION_BOX_1, maxyBox1);
                bundle.putInt(REGTYPE, 0);
                bundle.putInt(BOX_FIRST, 1);
                bundle.putInt(LOCAL_TYPE, localTypeId);
                bundle.putInt(MAXY_PROMOTION_ID, mMaxyGeneral.getPromotionID());
                bundle.putInt(MAXY_PROMOTION_TYPE, mMaxyGeneral.getPromotionType());
                bundle.putString(TAG_CONTRACT, "");
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                Intent intent = new Intent(getActivity(), MaxyPromotionBox1Activity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_MAXY_PROMOTION_BOX_1);
            } else {
                Toast.makeText(getContext(), "Vui lòng chọn box trước khi chọn CLKM", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //clear data nhannh26//
    private void clearMaxyContainer(boolean flag) {
        if (flag) {
            frmMaxyContainer.setVisibility(View.GONE);
            mMaxyDeploy = new MaxyTypeDeploy();
            mTypeDeploy.setText("");
        }
        clearBox();
        clearExtra();
        mMaxyGeneral = new MaxyGeneral();
        selPackage.setText("");
        selGetCLKM.setText("");
        textMaxyGeneral.setText(Common.formatMoney(valueOf(0)));
        mMaxyGeneral.setUseCombo(3);
        rdoRegisterComboYes.setChecked(true);
        mMaxySetup = new MaxySetupType();
        mSetupType.setText("");
        macListMaxy.clear();
        macMaxyAdapter.notifyData(macListMaxy);
        btnSelectMacMaxy.setVisibility(View.GONE);
    }

    private void clearExtra() {
        mListMaxyExtra.clear();
        mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
    }

    private void clearBox() {
        updateCountDevice();
        listDevice.clear();
        clearBox1();
        clearBox2();
    }

    private void clearBox1() {
        maxyBox1 = new MaxyBox();
        mBox1.getText().clear();
        mPromotionBox1.getText().clear();
        textPriceBox1.setText(Common.formatMoney(String.valueOf(0)));
    }

    private void clearBox2() {
        mListMaxyDevice.clear();
        mMaxyDeviceAdapter.notifyDataSetChanged();
        updateCountDevice();
    }

    private void clearMACMaxy() {
        if (mMaxyGeneral.getListMACOTT().size() > 0 && mMaxyGeneral.getListMACOTT() != null) {
            mMaxyGeneral.getListMACOTT().clear();
        }
        if (macListMaxy.size() > 0) {
            macListMaxy.clear();
            macMaxyAdapter.notifyData(macListMaxy);
        }
    }

    private void updateCountDevice() {
        count = 0;
        for (MaxyBox item : listDevice) {
            count += item.getBoxCount();
        }
        Log.e("Count", "Count => " + count + " " + "NumBox => " + numBox);
    }

    private boolean checkMaxyOnDevice() {
        int countMaxy = 0;
        for (MaxyExtra item : mListMaxyExtra) {
            //1:Theo box  6:hợp đồng
            if (item.getExtraType() == 1) {
                countMaxy += 1;
            }
        }
        if (mMaxyGeneral.getTypeDeployID() == 2 || mMaxyGeneral.getTypeDeployID() == 4) {
            return true;
        } else {
            if (countMaxy < count || count == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    private MaxyBox getBox1() {
        MaxyBox box = new MaxyBox();
        for (MaxyBox item : listDevice) {
            if (item.getBoxFirst() == 1) {
                box = item;
            }
        }
        return box;
    }
    //end//

    public void isNoDeploy() {
        loQuantityDeploy.setVisibility(View.GONE);
        rdoNoDeploy.setChecked(true);
        rdoDeploy.setChecked(false);
        tvSetupDetailQuantity.setText(String.valueOf(0));
        tvSetupDetailPrice.setText(String.format(getString(R.string.txt_unit_price), Common.formatNumber(0)));
        if (modelDetail.getObjectCameraOfNet().getSetupDetail() != null) {
            mSetupDetail = null;
            modelDetail.getObjectCameraOfNet().setSetupDetail(null);
        }
    }

    private void isDeploy() {
        loQuantityDeploy.setVisibility(View.VISIBLE);
        rdoDeploy.setChecked(true);
        rdoNoDeploy.setChecked(false);
        tvSetupDetailQuantity.setText(String.valueOf(mSetupDetail.getQty()));
        tvSetupDetailPrice.setText(
                String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(mSetupDetail.getQty() * mSetupDetail.getCost()))
        );
    }

    public void loadInfoSetupDetail(List<SetupDetail> mList) {
        modelDetail.getObjectCameraOfNet().setSetupDetail(mList.get(0));

        mSetupDetail = modelDetail.getObjectCameraOfNet().getSetupDetail();

        mSetupDetail.setType(1);//1 - triển khai , 0 - giao tại quầy
        //set min set up detail
        mSetupDetail.setQty(getCameraCount());
        isDeploy();
    }

    private int getCameraCount() {
        if (mListCamera == null || mListCamera.size() == 0) {
            return 0;
        }
        int cameraCount = 0;
        for (CameraDetail item : mListCamera) {
            cameraCount += item.getQty();
        }
        return cameraCount;
    }

    public List<Device> getListDeviceSelect() {
        return mListDeviceSelected;
    }

    public List<FPTBox> getListFptBoxSelect() {
        return listFptBoxSelect;
    }

    public List<MacOTT> getMacList() {
        return macListMaxy;
    }

    public List<CameraDetail> getListCameraDetail() {
        return mListCamera;
    }

    public List<CloudDetail> getListCloudDetail() {
        return mListCloud;
    }

    public List<MaxyBox> getListMaxyDevice() {
        return listDevice;
    }

    public List<MaxyExtra> getListMaxyExtra() {
        return mListMaxyExtra;
    }

    private void getLocalType() {
        if (mListServiceSelected.size() == 0) {
            List<CategoryServiceList> mListService = new ArrayList<>();
            if (modelDetail != null && modelDetail.getCategoryServiceList() != null &&
                    modelDetail.getCategoryServiceList().size() != 0) {
                mListService = modelDetail.getCategoryServiceList();
            }
            if (((RegisterActivityNew) getActivity()).getListService() != null &&
                    ((RegisterActivityNew) getActivity()).getListService().size() != 0) {
                mListService = ((RegisterActivityNew) getActivity()).getListService();
            }
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                    (ArrayList<? extends Parcelable>) mListService);
            Intent intent = new Intent(getActivity(), LocalTypeActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_LOCAL_TYPE);
        } else {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                    (ArrayList<? extends Parcelable>) mListServiceSelected);
            Intent intent = new Intent(getActivity(), LocalTypeActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_LOCAL_TYPE);
        }
    }

    @SuppressLint("SetTextI18n")
    private void initViewByServiceType(List<CategoryServiceList> lstServiceType) {
        if (lstServiceType.size() == 1) {
            if (lstServiceType.get(0).getCategoryServiceID() == 1) {//internet
                visibleFPTBoxControl(false);
                visibleDevicesControl(false);
                visibleIPTVControl(false);
                visibleMaxyControl(false);
                visibleCameraControl(false);
            } else if (lstServiceType.get(0).getCategoryServiceID() == 3) {//fpt box
                visibleInternetControl(false);
                visibleDevicesControl(false);
                visibleIPTVControl(false);
                visibleMaxyControl(false);
                visibleCameraControl(false);
            } else {//camera
                visibleInternetControl(false);
                visibleFPTBoxControl(false);
                visibleDevicesControl(false);
                visibleMaxyControl(false);
                visibleIPTVControl(false);
            }
        } else if (lstServiceType.size() == 2) {
            if (lstServiceType.get(0).getCategoryServiceID() == 1 && lstServiceType.get(1).getCategoryServiceID() == 2) {//internet + thiet bi
                visibleFPTBoxControl(false);
                visibleIPTVControl(false);
                visibleCameraControl(false);
                visibleMaxyControl(false);
            } else if (lstServiceType.get(0).getCategoryServiceID() == 1 && lstServiceType.get(1).getCategoryServiceID() == 8) {//internet + iptv
                visibleFPTBoxControl(false);
                visibleDevicesControl(false);
                visibleIPTVControl(false);
                visibleCameraControl(false);
            } else if (lstServiceType.get(0).getCategoryServiceID() == 1 && lstServiceType.get(1).getCategoryServiceID() == 7) //internet + camera
            {
                visibleFPTBoxControl(false);
                visibleIPTVControl(false);
                visibleDevicesControl(false);
                visibleMaxyControl(false);
            }
        } else if (lstServiceType.size() == 3) {
            if (lstServiceType.get(0).getCategoryServiceID() == 1 && lstServiceType.get(1).getCategoryServiceID() == 2 && lstServiceType.get(2).getCategoryServiceID() == 8) //intenet + thiet bị + maxy
            {
                visibleFPTBoxControl(false);
                visibleCameraControl(false);
                visibleIPTVControl(false);
            } else if (lstServiceType.get(0).getCategoryServiceID() == 1 && lstServiceType.get(1).getCategoryServiceID() == 2 && lstServiceType.get(2).getCategoryServiceID() == 7) //intenet + thiet bị + camera
            {
                visibleFPTBoxControl(false);
                visibleIPTVControl(false);
                visibleMaxyControl(false);
            } else if (lstServiceType.get(0).getCategoryServiceID() == 1 && lstServiceType.get(1).getCategoryServiceID() == 7 && lstServiceType.get(2).getCategoryServiceID() == 8) {
                visibleFPTBoxControl(false);
                visibleDevicesControl(false);
                visibleIPTVControl(false);
            }

        } else if (lstServiceType.size() == 4) {
            if (!TextUtils.isEmpty(tvServiceType.getText().toString())) {
                visibleFPTBoxControl(false);
            } else {
                visibleInternetControl(false);
                visibleFPTBoxControl(false);
                visibleDevicesControl(false);
                visibleIPTVControl(false);
                visibleCameraControl(false);
                visibleMaxyControl(false);
                return;
            }
        }

        for (CategoryServiceList item : lstServiceType) {
            //internet
            if (item.getCategoryServiceID() == 1) {
                visibleInternetControl(true);
            }
            //thiết bị
            if (item.getCategoryServiceID() == 2) {
                visibleDevicesControl(true);
            }
            //camera
            if (item.getCategoryServiceID() == 7) {
                visibleCameraControl(true);
            }
            //maxy
            if (item.getCategoryServiceID() == 8) {
                visibleMaxyControl(true);
            }
            //fpt play box
            if (item.getCategoryServiceID() == 3) {
                visibleFPTBoxControl(true);
                tvLocalType.setText("FPT Play");
                localTypeId = 210;
                tvLocalType.setEnabled(false);
                tvServiceType.setEnabled(false);
            }
        }
    }

    private void showFilterActivity(boolean promotionIPTV, int box) {
        try {
            if (promotionIPTV) {
                if (mListPromotionIPTVBox1 != null && box == 1) {
                    Intent intent = new Intent(getActivity(), PromotionIPTVFilterActivity.class);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_TYPE_BOX, box);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_LIST, mListPromotionIPTVBox1);
                    getActivity().startActivityForResult(intent, 1);
                }
                if (mListPromotionIPTVBox2 != null && box == 2) {
                    Intent intent = new Intent(getActivity(), PromotionIPTVFilterActivity.class);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_TYPE_BOX, box);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_LIST, mListPromotionIPTVBox2);
                    getActivity().startActivityForResult(intent, 1);
                }
            } else {
                if (mListPromotion != null) {
                    Intent intent = new Intent(getActivity(), PromotionFilterActivity.class);
                    intent.putExtra(PromotionFilterActivity.TAG_PROMOTION_LIST, mListPromotion);
                    startActivityForResult(intent, 1);
                } else {
                    Toast.makeText(getActivity(), "Chưa chọn gói dịch vụ", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // init data device
    public void initDataDevice() {
        if (modelDetail != null && modelDetail.getListDevice() != null) {
            mListDeviceSelected = modelDetail.getListDevice();
            mDeviceAdapter.notifyData(mListDeviceSelected);
        }
        if (mListDeviceSelected.size() > 0) {
            visibleDevicesControl(true);
        }
    }

    // Tuấn cập nhật code 16012018
    // init data fpt box
    public void initDataFptBox() {
        if (modelDetail != null) {
            if (modelDetail.getListDeviceMACOTT() != null && modelDetail.getListDeviceMACOTT().getListDeviceOTT() != null) {
                listFptBoxSelect.addAll(modelDetail.getListDeviceMACOTT().getListDeviceOTT());
                for (FPTBox fptBox : listFptBoxSelect) {
                    if (listPromotionFptBox.get(fptBox.getOTTID()) == null) {
                        ArrayList<PromotionFPTBoxModel> arrayList = new ArrayList<>();
                        arrayList.add(new PromotionFPTBoxModel(-1, "Chọn CLKM FPT Box"));
                        arrayList.add(new PromotionFPTBoxModel(fptBox.getPromotionID(), fptBox.getPromotionText()));
                        listPromotionFptBox.put(fptBox.getOTTID(), arrayList);
                    }
                }
            }
            if (modelDetail.getListDeviceMACOTT() != null && modelDetail.getListDeviceMACOTT().getListMACOTT() != null) {
                macList.addAll(modelDetail.getListDeviceMACOTT().getListMACOTT());
            }
        }

        if (listFptBoxSelect.size() > 0) {
            frmListFptBox.setVisibility(View.VISIBLE);
            btnChooseMac.setVisibility(View.VISIBLE);
        }
    }

    // thêm 1 địa chỉ mac vào danh sách.
    public void addMacAddressToList(String id) {
        if (checkMacAddress(id)) {
            Common.alertDialog("Địa chỉ Mac này đã được quét trước đó.", getActivity());
        } else {
            macList.add(new MacOTT(id));
            macAdapter.notifyDataSetChanged();
            Common.hideSoftKeyboard(getActivity());
        }
    }

    // kiểm tra sự tồn tại của địa chỉ Mac trong danh sách Mac
    public boolean checkMacAddress(String value) {
        boolean checkExist = false;
        for (MacOTT mac : macList) {
            if (mac.getMAC().equals(value)) {
                checkExist = true;
                break;
            }
        }
        return checkExist;
    }

    private void startScan() {
        Intent intent = new Intent(getActivity(), BarcodeScanActivity.class);
        startActivityForResult(intent, 3);
    }

    // tai danh sach fpt box tu server
    public void loadFptBox(ArrayList<FPTBox> fptBos) {
        macAdapter.notifyDataSetChanged();
        listNameFptBox.clear();
        this.listFptBox = fptBos;
        boolean[] is_checked_fpt_box = new boolean[listFptBox.size()];
        for (int i = 0; i < listFptBox.size(); i++) {
            listNameFptBox.add(listFptBox.get(i).getOTTName());
        }
        if (listFptBoxSelect.size() > 0) {
            Common.getFPTBoxSelected(listFptBoxSelect, listFptBox, is_checked_fpt_box);
            listFptBoxSelect.clear();
        }
        listFptBoxSelect.addAll(listFptBox);
        CharSequence[] dialogListFptBox = listNameFptBox.toArray(new CharSequence[listNameFptBox.size()]);
        AlertDialog alert = Common.getAlertDialogShowFPTBox(
                getActivity(),
                "FPT BOX",
                is_checked_fpt_box,
                dialogListFptBox,
                listFptBoxSelect,
                listFptBox,
                frmListFptBox,
                fptBoxAdapter,
                true,
                btnChooseMac
        );
        alert.setCancelable(false);
        alert.show();
    }

    // tai cau lenh khuyen mai fpt box tu server
    public void loadPromotionFptBox(ArrayList<PromotionFPTBoxModel> lst, Spinner
            spinnerPromotion,
                                    int fptBoxID, int idPromotionSelected) {
        listPromotionFptBox.get(fptBoxID).clear();
        listPromotionFptBox.get(fptBoxID).add(new PromotionFPTBoxModel(-1, "Chọn CLKM FPT Box"));
        listPromotionFptBox.get(fptBoxID).addAll(lst);
        spinnerPromotion.setSelection(Common.getIndex(spinnerPromotion, idPromotionSelected), true);
    }

    private void getIPTVPromotionBoxOrder() {
        String packageName = ((KeyValuePairModel) spIPTVPackage.getSelectedItem()).getHint();
        new GetPromotionIPTVList(getActivity(), this, packageName, 2,
                customerType, contractParam, regCodeParam);
    }

    // khởi tạo màn hình LỌC CÂU LỆNH KHUYẾN MÃI INTERNET
    private void showFilterActivity() {
        try {
            if (mListPromotion != null) {
                Intent intent = new Intent(getActivity(), PromotionFilterActivity.class);
                intent.putExtra(PromotionFilterActivity.TAG_PROMOTION_LIST, mListPromotion);
                getActivity().startActivityForResult(intent, 1);
            } else {
                Toast.makeText(getActivity(), "Chưa chọn gói dịch vụ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIPTVPrice() {
        int IPTVPromotionType = (promotionIPTVModelBox1 != null ? promotionIPTVModelBox1.getType() : -1);
        int IPTVPromotionID = (promotionIPTVModelBox1 != null ? promotionIPTVModelBox1.getPromotionID() : 0);
        new GetIPTVPrice(getActivity(), this, contractParam, regCodeParam, IPTVPromotionType, IPTVPromotionID);
    }

    public void loadIPTVPrice(List<IPTVPriceModel> lst) {
        if (lst != null && lst.size() > 0) {
            try {
                IPTVPriceModel item = lst.get(0);
                lblIPTVFimHotAmount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFimHot())));
                lblIPTVFimPlusAmount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFimPlus())));
                lblIPTVKPlusAmount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getKPlus())));
                lblIPTVVTVAmount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getVTVCap())));
                lblIPTVVTCAmount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getDacSacHD())));
                lblIPTVFimStandard_Amount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFimPlusStd())));
                tvFoxy2Amount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFoxy2())));
                tvFoxy4Amount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFoxy4())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initControlElse() {
        if (modelDetail != null) {
            if (modelDetail.getIPTVRequestSetUp() > 0) {
                radIpTvSetUpYes.setChecked(true);
            } else {
                radIpTvSetUpNo.setChecked(true);
            }

            if (modelDetail.getIPTVRequestDrillWall() > 0) {
                radIPTVDrillWallYes.setChecked(true);
            } else {
                radIPTVDrillWallNo.setChecked(true);
            }
            txtIPTVBoxCount.setText(String.valueOf(modelDetail.getIPTVBoxCount()));
            lblIPTVPLCCount.setText(String.valueOf(modelDetail.getIPTVPLCCount()));
            lblIPTVReturnSTBCount.setText(String.valueOf(modelDetail.getIPTVReturnSTBCount()));
            lblIPTVChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVChargeTimes()));
            txtIPTVPrepaid.setText(String.valueOf(modelDetail.getIPTVPrepaid()));

            // load HBO Package Extra
            if (modelDetail.getIPTVHBOChargeTimes() > 0 || modelDetail.getIPTVHBOPrepaidMonth() > 0) {
                chbIpTvHbo.setChecked(true);
            }
            //hiển thị thông tin K+
            if (modelDetail.getIPTVKPlusChargeTimes() > 0 || modelDetail.getIPTVKPlusPrepaidMonth() > 0) {
                chbIPTVKPlus.setChecked(true);
            }
            //hiển thị thông tin Đặc sắc HD
            if (modelDetail.getIPTVVTCChargeTimes() > 0 || modelDetail.getIPTVVTCPrepaidMonth() > 0) {
                chbIpTvVtc.setChecked(true);
            }
            //hiển thị thông tin VTV Cab
            if (modelDetail.getIPTVVTVChargeTimes() > 0 || modelDetail.getIPTVVTVPrepaidMonth() > 0) {
                chbIpTvVtv.setChecked(true);
            }
            //hiển thị thông tin Fim+ -> Fim Cao Cap
            if (modelDetail.getIPTVFimPlusChargeTimes() > 0 || modelDetail.getIPTVFimPlusPrepaidMonth() > 0) {
                chbIPTVFimPlus.setChecked(true);
            }
            //hiển thị thông tin Fim Hot
            if (modelDetail.getIPTVFimHotChargeTimes() > 0 || modelDetail.getIPTVFimHotPrepaidMonth() > 0) {
                chbIpTvFimHot.setChecked(true);
            }
            //hiển thị thông tin Fim cơ bản
            if (modelDetail.getIPTVFimPlusStdChargeTimes() > 0 || modelDetail.getIPTVFimPlusStdPrepaidMonth() > 0) {
                chbIPTVFimStandard.setChecked(true);
            }

            if (modelDetail.getFoxy2ChargeTimes() > 0 || modelDetail.getFoxy2PrepaidMonth() > 0) {
                cbFoxy2.setChecked(true);
            }

            if (modelDetail.getFoxy4ChargeTimes() > 0 || modelDetail.getFoxy4PrepaidMonth() > 0) {
                cbFoxy4.setChecked(true);
            }
        }
    }

    // khởi tạo giá trị contract và regcode từ pdk
    private void initContractRegCode() {
        contractParam = modelDetail != null ? modelDetail.getContract() : "";
        regCodeParam = modelDetail != null ? modelDetail.getRegCode() : "";
    }

    // khởi tạo danh sách clkm iptv box 1
    @SuppressLint("SetTextI18n")
    public void initIPTVPromotionIPTVBox1(ArrayList<PromotionIPTVModel> lst) {
        this.mListPromotionIPTVBox1 = lst;
        if (this.mListPromotionIPTVBox1.size() > 0) {
            try {
                if (modelDetail != null) {
                    PromotionIPTVModel promotionIPTVModel = Common.getItemPromotionIPTVBox(lst, 1, modelDetail);
                    promotionIPTVModelBox1 = promotionIPTVModel;
                    if (promotionIPTVModel != null) {
                        txtPromotionBox1.setText(promotionIPTVModel.getPromotionName());
                        lblIPTVPromotionBoxFirstAmount.setText(
                                String.format(getString(R.string.txt_unit_price), Common.formatNumber(promotionIPTVModel.getAmount())));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // khởi tạo danh sách clkm iptv box 2
    public void initIPTVPromotionIPTVBox2(ArrayList<PromotionIPTVModel> lst) {
        this.mListPromotionIPTVBox2 = lst;
        if (this.mListPromotionIPTVBox2.size() > 0) {
            try {
                if (modelDetail != null) {
                    PromotionIPTVModel promotionIPTVModel = Common.getItemPromotionIPTVBox(lst, 2, modelDetail);
                    if (promotionIPTVModel != null) {
                        promotionIPTVModelBox2 = promotionIPTVModel;
                        txtPromotionBox2.setText(promotionIPTVModel.getPromotionName());
                        lblIPTVPromotionBoxOrderAmount.setText(
                                String.format(getString(R.string.txt_unit_price),
                                        Common.formatNumber(promotionIPTVModel.getAmount()))
                        );
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Combo IPTV
    private void initIPTVCombo() {
        ArrayList<KeyValuePairModel> lstCombo = new ArrayList<>();
        lstCombo.add(new KeyValuePairModel(0, "[Chọn combo]"));
        lstCombo.add(new KeyValuePairModel(2, "Không combo"));
        lstCombo.add(new KeyValuePairModel(3, "Combo"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(), R.layout.my_spinner_style, lstCombo, Gravity.LEFT);
        spIPTVCombo.setAdapter(adapter);
        if (modelDetail != null) {
            try {
                spIPTVCombo.setSelection(Common.getIndex(spIPTVCombo, modelDetail.getIPTVUseCombo()), true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initIPTVFormDeployment() {
        ArrayList<KeyValuePairModel> lstCusSolvency = new ArrayList<>();
        lstCusSolvency.add(new KeyValuePairModel(5, "Triển khai mới một line"));
        lstCusSolvency.add(new KeyValuePairModel(1, "Triển khai mới nhiều line"));
        spIPTVFormDeployment.setAdapter(new KeyValuePairAdapter(getActivity(), R.layout.my_spinner_style, lstCusSolvency, Gravity.LEFT));
        if (modelDetail != null && modelDetail.getIPTVPackage() != null) {
            if (!modelDetail.getIPTVPackage().trim().equals("")) {
                spIPTVFormDeployment.setSelection(Common.getIndex(spIPTVFormDeployment, modelDetail.getIPTVStatus()), true);
            }
        }
    }

    /* ======================= Init Package ============================= */
    private void initIPTVAllPackages() {
        ArrayList<KeyValuePairModel> lst = new ArrayList<>();
        lst.add(new KeyValuePairModel(0, "--Chọn gói dịch vụ--", ""));
        lst.add(new KeyValuePairModel(76, "Truyền Hình FPT", "VOD HD"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(), R.layout.my_spinner_style, lst, Gravity.RIGHT);
        spIPTVPackage.setAdapter(adapter);
        if (modelDetail != null) {
            try {
                for (int i = 0; i < lst.size(); i++) {
                    KeyValuePairModel selectedItem = lst.get(i);
                    if (selectedItem.getHint().equalsIgnoreCase(modelDetail.getIPTVPackage())) {
                        spIPTVPackage.setSelection(i, true);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //ver 3.19 camera
    private void visibleCameraControl(boolean flag) {
        if (flag) {
            is_have_camera = flag;
            frmCamera.setVisibility(View.VISIBLE);
        } else {
            is_have_camera = flag;
            frmCamera.setVisibility(View.GONE);
            clearDataCamera();
        }
    }

    //ver 3.21 maxy
    private void visibleMaxyControl(boolean flag) {
        if (flag) {
            is_have_maxy = flag;
            frmMaxyTV.setVisibility(View.VISIBLE);
            mMaxyGeneral.setUseCombo(3);
        } else {
            clearMaxy();
            is_have_maxy = flag;
            frmMaxyTV.setVisibility(View.GONE);
        }
    }

    private void clearDataCamera() {
        modelDetail.getObjectCameraOfNet().setCameraDetail(new ArrayList<>());
        modelDetail.getObjectCameraOfNet().setCloudDetail(new ArrayList<>());
        modelDetail.getObjectCameraOfNet().setSetupDetail(new SetupDetail());
        modelDetail.getObjectCameraOfNet().setPromotionDetail(new PromoCloud());
        modelDetail.getObjectCameraOfNet().setCameraTotal(0);
        mListCamera.clear();
        mCameraAdapter.notifyDataChanged(mListCamera);
        mListCloud.clear();
        mCloudAdapter.notifyDataChanged(mListCloud);
        isNoDeploy();
        frmCamera.setVisibility(View.GONE);
        txtPromotionCloud.setText("");
    }

    @SuppressLint("SetTextI18n")
    private void clearMaxy() {
        modelDetail.getObjectMaxy().setMaxyGeneral(new MaxyGeneral());
        mMaxyGeneral = new MaxyGeneral();
        mMaxyGeneral.setUseCombo(3);
        rdoRegisterComboYes.setChecked(true);
        mMaxyDeploy = new MaxyTypeDeploy();
        mMaxySetup = new MaxySetupType();
        clearBox();
        mListMaxyDevice.clear();
        mListMaxyExtra.clear();
        mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
        mMaxyDeviceAdapter.notifyDataChanged(mListMaxyDevice);
        selPackage.setText("");
        selGetCLKM.setText("");
        mTypeDeploy.setText("");
        mSetupType.setText("");
        textMaxyGeneral.setText(Common.formatMoney(String.valueOf(0)));
        macListMaxy.clear();
        macMaxyAdapter.notifyData(macListMaxy);
        btnSelectMacMaxy.setVisibility(View.GONE);
        frmMaxyContainer.setVisibility(View.GONE);
    }

    private void visibleInternetControl(boolean flag) {
        if (flag) {
            frmInternet.setVisibility(View.VISIBLE);
        } else {
            frmInternet.setVisibility(View.GONE);
        }
    }


    private void visibleIPTVControl(boolean flag) {
        if (flag) {
            is_have_ip_tv = true;
            frmIpTv.setVisibility(View.VISIBLE);
            disableIPTVControls(false);
        } else {
            is_have_ip_tv = false;
            frmIpTv.setVisibility(View.GONE);
            disableIPTVControls(true);
        }
    }

    private void visibleFPTBoxControl(boolean flag) {
        if (flag) {
            is_have_fpt_box = true;
            frmContentFptBox.setVisibility(View.VISIBLE);
            if (listFptBoxSelect.size() > 0) {
                frmListFptBox.setVisibility(View.VISIBLE);
                btnChooseMac.setVisibility(View.VISIBLE);
            }
        } else {
            is_have_fpt_box = false;
            frmContentFptBox.setVisibility(View.GONE);
            frmListFptBox.setVisibility(View.GONE);
            btnChooseMac.setVisibility(View.GONE);
            if (listFptBoxSelect.size() > 0) {
                listFptBoxSelect.clear();
            }
            if (macList.size() > 0) {
                macList.clear();
            }
        }
        fptBoxAdapter.notifyDataSetChanged();
        macAdapter.notifyDataSetChanged();
    }

    // ẩn hiện tab thiết bị
    private void visibleDevicesControl(boolean flag) {
        if (flag) {
            is_have_device = true;
            frmDevicesBanner.setVisibility(View.VISIBLE);
        } else {
            is_have_device = false;
            frmDevicesBanner.setVisibility(View.GONE);
            if (mListDeviceSelected.size() > 0) {
                mListDeviceSelected.clear();
            }
        }
        mDeviceAdapter.notifyData(mListDeviceSelected);
    }

    @SuppressLint("StringFormatMatches")
    private void disableIPTVControls(boolean status) {
        boolean enabled = !status;
        radIpTvSetUpNo.setEnabled(enabled);
        radIpTvSetUpYes.setEnabled(enabled);
        radIPTVDrillWallYes.setEnabled(enabled);
        radIPTVDrillWallNo.setEnabled(enabled);
        spIPTVFormDeployment.setEnabled(enabled);
        txtIPTVBoxCount.setEnabled(enabled);
        spIPTVPackage.setEnabled(enabled);
        txtPromotionBox1.setEnabled(enabled);
        txtIPTVPrepaid.setEnabled(false);
        imgIPTVBoxLess.setEnabled(enabled);
        imgIPTVBoxPlus.setEnabled(enabled);
        imgIPTVPLCLess.setEnabled(enabled);
        imgIPTVPLCPlus.setEnabled(enabled);
        imgIPTVReturnSTBLess.setEnabled(enabled);
        imgIPTVReturnSTBPlus.setEnabled(enabled);
        imgIPTVChargeTimesPlus.setEnabled(enabled);
        imgIPTVTotal = enabled;
        spIPTVEquipmentDelivery = enabled;
        spIPTVCombo.setEnabled(enabled);
        frmIPTVMonthlyTotal = false;
        if (!enabled) {
            radIPTVDrillWallNo.setChecked(true);
            radIpTvSetUpNo.setChecked(true);
            spIPTVPackage.setSelection(0, true);
            spIPTVFormDeployment.setSelection(0, true);
            lblIPTVTotal = "0";
            lblTotal = "0";
            txtIPTVBoxCount.setText("0");
            lblIPTVPLCCount.setText("0");
            txtIPTVPrepaid.setText("0");
            lblIPTVReturnSTBCount.setText("0");
            lblIPTVChargeTimesCount.setText("0");
            spIPTVCombo.setSelection(0, true);
            lblIPTVPromotionBoxFirstAmount.setText(null);
            lblIPTVPromotionBoxOrderAmount.setText(null);

            txtPromotionBox1.setText("");
            txtPromotionBox2.setText("");
            cbFoxy2.setChecked(false);
            cbFoxy4.setChecked(false);
            chbIpTvFimHot.setChecked(false);
            chbIPTVFimPlus.setChecked(false);
            chbIPTVFimStandard.setChecked(false);
            chbIpTvHbo.setChecked(false);
            chbIPTVKPlus.setChecked(false);
            chbIpTvVtc.setChecked(false);
            chbIpTvVtv.setChecked(false);

            lblIPTVFimStandard_Amount.setText(String.format(getString(R.string.txt_unit_price), 0));
            lblIPTVFimPlusAmount.setText(String.format(getString(R.string.txt_unit_price), 0));
            lblIPTVFimHotAmount.setText(String.format(getString(R.string.txt_unit_price), 0));
            lblIPTVKPlusAmount.setText(String.format(getString(R.string.txt_unit_price), 0));
            lblIPTVVTCAmount.setText(String.format(getString(R.string.txt_unit_price), 0));
            lblIPTVVTVAmount.setText(String.format(getString(R.string.txt_unit_price), 0));
            tvFoxy2Amount.setText(String.format(getString(R.string.txt_unit_price), 0));
            tvFoxy4Amount.setText(String.format(getString(R.string.txt_unit_price), 0));
        } else {
            if (lblIPTVChargeTimesCount.getText().toString().equals("0")
                    || lblIPTVChargeTimesCount.getText().toString().equals(""))
                lblIPTVChargeTimesCount.setText("1");
            if (txtIPTVBoxCount.getText().toString().equals("0") || txtIPTVBoxCount.getText().toString().equals(""))
                txtIPTVBoxCount.setText("1");
        }
    }

    public static int indexOf(ArrayList<PromotionModel> lst, int value) {
        for (int i = 0; i < lst.size(); i++) {
            PromotionModel item = lst.get(i);
            if (item.getPromotionID() == value)
                return i;
        }
        return -1;
    }

    // tải danh sách câu lệnh khuyến mãi về
    public void setAutoCompletePromotionAdapter(ArrayList<PromotionModel> lst) {
        this.mListPromotion = lst;
        if (txtPromotion != null) {
            txtPromotion.setText(null);
            try {
                if (lst != null && lst.size() > 0) {
                    selectedInternetPromotion = lst.get(0);
                    if (modelDetail != null) {
                        int position = indexOf(lst, modelDetail.getPromotionID());
                        if (position >= 0) {
                            selectedInternetPromotion = lst.get(position);
                            txtPromotion.setText(selectedInternetPromotion.getPromotionName());
                            int total = selectedInternetPromotion.getRealPrepaid();
                            lblInternetTotal = Common.formatNumber(total);
                        } else {
                            selectedInternetPromotion = null;
                            txtPromotion.setText("");
                            lblInternetTotal = "0";
                        }
                    } else {
                        selectedInternetPromotion = null;
                        txtPromotion.setText("");
                        lblInternetTotal = "0";
                    }
                } else {
                    selectedInternetPromotion = null;
                    txtPromotion.setText("");
                    lblInternetTotal = "0";
                }
            } catch (Exception e) {
                e.printStackTrace();
                e.printStackTrace();
            }
        }
    }

    public void getIPTVTotalNew() {
        if (spIPTVPackage.getSelectedItemPosition() <= 0) {
            errorStr = "Chưa chọn gói dịch vụ IPTV!";
        } else {
            int IPTVPLCCount = Integer.parseInt(lblIPTVPLCCount.getText().toString()),
                    IPTVReturnSTBCount = Integer.parseInt(lblIPTVReturnSTBCount.getText().toString()),
                    IPTVPromotionID = promotionIPTVModelBox1 != null ? promotionIPTVModelBox1.getPromotionID() : 0,
                    IPTVChargeTimes = 1,
                    IPTVPrepaid = (!txtIPTVPrepaid.getText().toString().equals("")
                            ? Integer.parseInt(txtIPTVPrepaid.getText().toString()) : 0),
                    IPTVBoxCount = Integer.parseInt(txtIPTVBoxCount.getText().toString()), IPTVHBOPrepaidMonth = Integer.parseInt(lblIPTVHBOPrepaidMonthCount.getText().toString()),
                    IPTVHBOChargeTimes = Integer.parseInt(lblIPTVHBOChargeTimesCount.getText().toString()), IPTVVTVPrepaidMonth = Integer.parseInt(lblIPTVVTVPrepaidMonthCount.getText().toString()),
                    IPTVVTVChargeTimes = Integer.parseInt(lblIPTVVTVChargeTimesCount.getText().toString()), IPTVKPlusPrepaidMonth = Integer.parseInt(lblIPTVKPlusPrepaidMonthCount.getText().toString()),
                    IPTVKPlusChargeTimes = Integer.parseInt(lblIPTVKPlusChargeTimesCount.getText().toString()), IPTVVTCPrepaidMonth = Integer.parseInt(lblIPTVVTCPrepaidMonthCount.getText().toString()),
                    IPTVVTCChargeTimes = Integer.parseInt(lblIPTVVTCChargeTimesCount.getText().toString()),
                    // Fim Chuan
                    IPTVFimStandardChargeTimes = Integer.parseInt(lblIPTVFimStandard_ChargeTimes.getText().toString()),
                    IPTVFimStandardPrepaidMonth = Integer.parseInt(lblIPTVFimStandard_MonthCount.getText().toString()),

                    IPTVFimPlusPrepaidMonth = Integer.parseInt(lblIPTVFimPlus_MonthCount.getText().toString()),
                    IPTVFimPlusChargeTimes = Integer.parseInt(lblIPTVFimPlus_ChargeTime.getText().toString()),
                    IPTVFimHotPrepaidMonth = Integer.parseInt(lblIPTVFimHot_MonthCount.getText().toString()),
                    IPTVFimHotChargeTimes = Integer.parseInt(lblIPTVFimHot_ChargeTime.getText().toString()),
                    IPTVPromotionIDBoxOrder = promotionIPTVModelBox2 != null ? promotionIPTVModelBox2.getPromotionID() : 0;
            RegistrationDetailModel mRegister = new RegistrationDetailModel();
            mRegister.setIPTVPackage(((KeyValuePairModel) spIPTVPackage.getSelectedItem()).getHint());
            mRegister.setIPTVPromotionID(IPTVPromotionID);
            mRegister.setIPTVChargeTimes(IPTVChargeTimes);
            mRegister.setIPTVPrepaid(IPTVPrepaid);
            mRegister.setIPTVBoxCount(IPTVBoxCount);
            mRegister.setIPTVPLCCount(IPTVPLCCount);
            mRegister.setIPTVReturnSTBCount(IPTVReturnSTBCount);
            mRegister.setIPTVVTVPrepaidMonth(IPTVVTVPrepaidMonth);
            mRegister.setIPTVVTVChargeTimes(IPTVVTVChargeTimes);
            mRegister.setIPTVKPlusPrepaidMonth(IPTVKPlusPrepaidMonth);
            mRegister.setIPTVKPlusChargeTimes(IPTVKPlusChargeTimes);
            mRegister.setIPTVVTCPrepaidMonth(IPTVVTCPrepaidMonth);
            mRegister.setIPTVVTCChargeTimes(IPTVVTCChargeTimes);
            mRegister.setIPTVHBOPrepaidMonth(IPTVHBOPrepaidMonth);
            mRegister.setIPTVHBOChargeTimes(IPTVHBOChargeTimes);
            mRegister.setIPTVFimPlusPrepaidMonth(IPTVFimPlusPrepaidMonth);
            mRegister.setIPTVFimPlusChargeTimes(IPTVFimPlusChargeTimes);
            mRegister.setIPTVFimHotPrepaidMonth(IPTVFimHotPrepaidMonth);
            mRegister.setIPTVFimHotChargeTimes(IPTVFimHotChargeTimes);
            mRegister.setIPTVPromotionIDBoxOther(IPTVPromotionIDBoxOrder);
            mRegister.setIPTVFimPlusStdChargeTimes(IPTVFimStandardChargeTimes);
            mRegister.setIPTVFimPlusStdPrepaidMonth(IPTVFimStandardPrepaidMonth);
            mRegister.setFoxy2ChargeTimes(Integer.parseInt(tvFoxy2ChargeTime.getText().toString()));
            mRegister.setFoxy2PrepaidMonth(Integer.parseInt(tvFoxy2PrepaidMonth.getText().toString()));
            mRegister.setFoxy4ChargeTimes(Integer.parseInt(tvFoxy4ChargeTime.getText().toString()));
            mRegister.setFoxy4PrepaidMonth(Integer.parseInt(tvFoxy4PrepaidMonth.getText().toString()));

            int serviceType = this.serviceType;
            if (IPTVPromotionID == -100) {
                errorStr = "Chưa chọn CLKM IPTV!";
            } else {
                this.mRegister = mRegister;
                this.serviceType = serviceType;
                errorStr = "ok";
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.chb_iptv_hbo) {
            imgIPTVHBOChargeTimeLess.setEnabled(isChecked);
            imgIPTVHBOChargeTimePlus.setEnabled(isChecked);
            imgIPTVHBOPrepaidMonthLess.setEnabled(isChecked);
            imgIPTVHBOPrepaidMonthPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVHBOChargeTimesCount.setText("0");
                lblIPTVHBOPrepaidMonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVHBOChargeTimes() > 0 || modelDetail.getIPTVHBOPrepaidMonth() > 0) {
                        lblIPTVHBOChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVHBOChargeTimes()));
                        lblIPTVHBOPrepaidMonthCount.setText(String.valueOf(modelDetail.getIPTVHBOPrepaidMonth()));
                    } else {
                        lblIPTVHBOChargeTimesCount.setText("1");
                        lblIPTVHBOPrepaidMonthCount.setText("0");
                    }
                } else {
                    lblIPTVHBOChargeTimesCount.setText("1");
                    lblIPTVHBOPrepaidMonthCount.setText("0");
                }
            }

        } else if (buttonView.getId() == R.id.chb_iptv_k_plus) {
            imgIPTVKPlusChargeTimeLess.setEnabled(isChecked);
            imgIPTVKPlusChargeTimePlus.setEnabled(isChecked);
            imgIPTVKPlusPrepaidMonthLess.setEnabled(isChecked);
            imgIPTVKPlusPrepaidMonthPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVKPlusChargeTimesCount.setText("0");
                lblIPTVKPlusPrepaidMonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVKPlusChargeTimes() > 0 || modelDetail.getIPTVKPlusPrepaidMonth() > 0) {
                        lblIPTVKPlusChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVKPlusChargeTimes()));
                        lblIPTVKPlusPrepaidMonthCount.setText(String.valueOf(modelDetail.getIPTVKPlusPrepaidMonth()));
                    } else {
                        lblIPTVKPlusChargeTimesCount.setText("1");
                        lblIPTVKPlusPrepaidMonthCount.setText("0");
                    }
                } else {
                    lblIPTVKPlusChargeTimesCount.setText("1");
                    lblIPTVKPlusPrepaidMonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_iptv_vtv_cab) {
            imgIPTVVTVChargeTimeLess.setEnabled(isChecked);
            imgIPTVVTVChargeTimePlus.setEnabled(isChecked);
            imgIPTVVTVPrepaidMonthLess.setEnabled(isChecked);
            imgIPTVVTVPrepaidMonthPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVVTVChargeTimesCount.setText("0");
                lblIPTVVTVPrepaidMonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVVTVChargeTimes() > 0 || modelDetail.getIPTVVTVPrepaidMonth() > 0) {
                        lblIPTVVTVChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVVTVChargeTimes()));
                        lblIPTVVTVPrepaidMonthCount.setText(String.valueOf(modelDetail.getIPTVVTVPrepaidMonth()));
                    } else {
                        lblIPTVVTVChargeTimesCount.setText("1");
                        lblIPTVVTVPrepaidMonthCount.setText("0");
                    }
                } else {
                    lblIPTVVTVChargeTimesCount.setText("1");
                    lblIPTVVTVPrepaidMonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_iptv_fim_plus) {
            imgIPTVFimPlus_ChargeTimeLess.setEnabled(isChecked);
            imgIPTVFimPlus_ChargeTimePlus.setEnabled(isChecked);
            imgIPTVFimPlus_PrepaidMonthLess.setEnabled(isChecked);
            imgIPTVFimPlus_PrepaidMonthPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVFimPlus_ChargeTime.setText("0");
                lblIPTVFimPlus_MonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVFimPlusChargeTimes() > 0 || modelDetail.getIPTVFimPlusPrepaidMonth() > 0) {
                        lblIPTVFimPlus_ChargeTime.setText(String.valueOf(modelDetail.getIPTVFimPlusChargeTimes()));
                        lblIPTVFimPlus_MonthCount.setText(String.valueOf(modelDetail.getIPTVFimPlusPrepaidMonth()));
                    } else {
                        lblIPTVFimPlus_ChargeTime.setText("1");
                        lblIPTVFimPlus_MonthCount.setText("0");
                    }
                } else {
                    lblIPTVFimPlus_ChargeTime.setText("1");
                    lblIPTVFimPlus_MonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_iptv_fim_hot) {
            imgIPTVFimHot_ChargeTimeLess.setEnabled(isChecked);
            imgIPTVFimHot_ChargeTimePlus.setEnabled(isChecked);
            imgIPTVFimHot_PrepaidMonthLess.setEnabled(isChecked);
            imgIPTVFimHot_PrepaidMonthPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVFimHot_ChargeTime.setText("0");
                lblIPTVFimHot_MonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVFimHotChargeTimes() > 0 || modelDetail.getIPTVFimHotPrepaidMonth() > 0) {
                        lblIPTVFimHot_ChargeTime.setText(String.valueOf(modelDetail.getIPTVFimHotChargeTimes()));
                        lblIPTVFimHot_MonthCount.setText(String.valueOf(modelDetail.getIPTVFimHotPrepaidMonth()));
                    } else {
                        lblIPTVFimHot_ChargeTime.setText("1");
                        lblIPTVFimHot_MonthCount.setText("0");
                    }
                } else {
                    lblIPTVFimHot_ChargeTime.setText("1");
                    lblIPTVFimHot_MonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_ip_tv_fim_standard) {
            imgIPTVFimStandard_ChargeTimeLess.setEnabled(isChecked);
            imgIPTVFimStandard_ChargeTimePlus.setEnabled(isChecked);
            imgIPTVFimStandard_MonthCountLess.setEnabled(isChecked);
            imgIPTVFimStandard_MonthCountPlus.setEnabled(isChecked);
            if (!isChecked) {
                lblIPTVFimStandard_ChargeTimes.setText("0");
                lblIPTVFimStandard_MonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVFimPlusStdChargeTimes() > 0 || modelDetail.getIPTVFimPlusStdPrepaidMonth() > 0) {
                        lblIPTVFimStandard_ChargeTimes.setText(String.valueOf(modelDetail.getIPTVFimPlusStdChargeTimes()));
                        lblIPTVFimStandard_MonthCount.setText(String.valueOf(modelDetail.getIPTVFimPlusStdPrepaidMonth()));
                    } else {
                        lblIPTVFimStandard_ChargeTimes.setText("1");
                        lblIPTVFimStandard_MonthCount.setText("0");
                    }
                } else {
                    lblIPTVFimStandard_ChargeTimes.setText("1");
                    lblIPTVFimStandard_MonthCount.setText("0");
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                String message = getActivity().getString(R.string.error_permission_denied);
                Common.getInstance().showPopup(getActivity(), message);
                return;
            }
        }
        if (requestCode == 2) {
            startScan();
        }
    }

    @Override
    public void onItemClick(Integer type, Device mDevice, Integer position) {
        switch (type) {
            case 0:
                mListDeviceSelected.remove(mDevice);
                mDeviceAdapter.notifyData(mListDeviceSelected);
                break;
            case 1:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.DEVICE, mDevice);
                bundle.putInt(Constants.POSITION, position);
                bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                Intent intent = new Intent(getActivity(), DevicePriceListActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_DEVICE_PRICE_LIST_SELECTED);
                break;
        }
    }

    @Override
    public void onItemClickV4(Object mObject, Integer position, Integer type) {
        Bundle bundle = new Bundle();
        Intent intent;
        switch (type) {
            case 1:
                CameraDetail mCameraDetail = (CameraDetail) mObject;
                bundle.putParcelable(CAMERA_DETAIL, mCameraDetail);
                bundle.putInt(POSITION, position);
                bundle.putInt(CUSTYPE, activity.isCusType);
                bundle.putInt(REGTYPE, 0);
                bundle.putString(CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                intent = new Intent(getContext(), CameraOrNetPackageActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_CAMERA_PACKAGE);
                break;
            case 3:
                CloudDetail mCloudDetailRemove = (CloudDetail) mObject;
                mListCloud.remove(mCloudDetailRemove);
                modelDetail.getObjectCameraOfNet().getCloudDetail().remove(mCloudDetailRemove);
                mCloudAdapter.notifyDataChanged(this.mListCloud);
                if (mListCloud.size() == 0) {
                    setupPromotionCloud(false);
                }
                mProCloud = new PromoCloud();
                txtPromotionCloud.getText().clear();
                break;
            case 4:
            case 5:
                mProCloud = new PromoCloud();
                txtPromotionCloud.getText().clear();
                break;
            case 6:
                MaxyBox maxyBox = (MaxyBox) mObject;
                mListMaxyDevice.remove(maxyBox);
                listDevice.remove(maxyBox);
                mMaxyDeviceAdapter.notifyDataChanged(this.mListMaxyDevice);
                if (maxyBox.getBoxType() == 1) {
                    updateExtraOnBox();
                }
                updateCountDevice();
                break;
            case 7:
                MaxyBox mBox = (MaxyBox) mObject;
                bundle.putParcelable(LIST_MAXY_DETAIL_SELECTED, mBox);
                bundle.putInt(POSITION, position);
                bundle.putInt(REGTYPE, 0);
                bundle.putInt(LOCAL_TYPE, localTypeId);
                bundle.putInt(BOX_FIRST, 2);
                bundle.putInt(MAXY_PROMOTION_ID, mMaxyGeneral.getPromotionID());
                bundle.putInt(MAXY_PROMOTION_TYPE, mMaxyGeneral.getPromotionType());
                bundle.putString(TAG_CONTRACT, "");
                bundle.putInt(NUM_BOX, numBox);
                bundle.putInt(COUNT_DEVICE, count);
                bundle.putString(CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                intent = new Intent(getContext(), MaxyPromotionDeviceActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_MAXY_PROMOTION_DEVICE);
                break;
            case 8:
                MaxyExtra maxyExtra = (MaxyExtra) mObject;
                mListMaxyExtra.remove(maxyExtra);
                mMaxyExtraAdapter.notifyDataChanged(this.mListMaxyExtra);
                break;
            default:
                break;
        }
    }

    private void setupPromotionCloud(boolean b) {
        txtPromotionCloud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (b) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(CUSTYPE, activity.getIsCusType());
                    bundle.putInt(REGTYPE, 0);
                    bundle.putParcelableArrayList(CLOUD_DETAIL, (ArrayList<? extends Parcelable>) mListCloud);
                    bundle.putString(Constants.CLASS_NAME, FragmentRegisterStep3v2.class.getSimpleName());
                    Intent intent = new Intent(getContext(), PromoCloudActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CODE_CLOUD_PROMOTION);
                } else {
                    Common.alertDialog(getString(R.string.msg_select_cloud_promotion), getContext());
                }
            }
        });
    }


    @Override
    public void onItemClick(Boolean isUpdatePromotion) {
        activity.setIsUpdatePromotion(isUpdatePromotion);
    }

    @Override
    public void onItemClickUpdateCount(Boolean flag) {
        if (flag) {
            for (MaxyExtra item : mListMaxyExtra) {
                item.setChargeTimes(1);
            }
            updateCountDevice();
            mMaxyExtraAdapter.notifyDataChanged(mListMaxyExtra);
        }
    }
}
