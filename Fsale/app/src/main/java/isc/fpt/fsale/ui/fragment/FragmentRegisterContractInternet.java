package isc.fpt.fsale.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPromotionCombo;
import isc.fpt.fsale.action.GetPromotionList;
import isc.fpt.fsale.activity.PromotionFilterActivity;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.activity.local_type.LocalTypeActivity;
import isc.fpt.fsale.adapter.PromotionComboAdapter;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.CheckListService;
import isc.fpt.fsale.model.LocalType;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.PromotionComboModel;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.ui.callback.OnLoadTotalIntenet;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;


public class FragmentRegisterContractInternet extends Fragment {
    private Context mContext;
    private Switch schInternet, schCombo;
    private TextView lblPromotion;
    private Spinner spPromotionCombo;
    private TextView lblPromotionCombo, tvLocalType;
    private RegistrationDetailModel mRegister;
    private ObjectDetailModel mObject;
    public PromotionModel mPromotion;
    public TextView txtPromotion;
    private RegisterContractActivity activity;
    private final int CODE_LOCAL_TYPE = 100;
    private LocalType mLocalType;
    public int localTypeID;
    ;

    public static FragmentRegisterContractInternet newInstance() {
        FragmentRegisterContractInternet fragment = new FragmentRegisterContractInternet();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentRegisterContractInternet() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_contract_internet,
                container, false);
        try {
            Common.setupUI(getActivity(), rootView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mContext = getActivity();
        // mở tắt internet
        schInternet = (Switch) rootView.findViewById(R.id.sch_internet);
        schInternet.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                enableInternet(isChecked);
            }
        });
        // gói dịch vụ
        tvLocalType = (TextView) rootView.findViewById(R.id.tv_local_type);
        tvLocalType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CategoryServiceList> mCatListService = new ArrayList<>();
                if (mRegister != null) {
                    mCatListService = mRegister.getCategoryServiceList();
                    if (schInternet.isChecked()) {
                        mCatListService.add(new CategoryServiceList(1, "Internet"));
                    } else {
                        for (CategoryServiceList item : mCatListService) {
                            if (item.getCategoryServiceID() == 1) {
                                mCatListService.remove(item);
                            }
                        }
                    }
                } else {
                    CheckListService object = activity.getObject().getCheckListService();
                    if (object.isIPTV()) {//TH PDK la IPTV Only
                        mCatListService.add(new CategoryServiceList(1, "Internet"));
                        mCatListService.add(new CategoryServiceList(4, "Pay TV"));
                    }
                }
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Constants.SERVICE_TYPE_LIST,
                        (ArrayList<? extends Parcelable>) mCatListService);
                Intent intent = new Intent(getActivity(), LocalTypeActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_LOCAL_TYPE);
            }
        });
        // khuyến mãi
        txtPromotion = (TextView) rootView.findViewById(R.id.txt_promotion);
        lblPromotion = (TextView) rootView.findViewById(R.id.lbl_promotion_desc);
        txtPromotion.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (txtPromotion.isEnabled())
                    showFilterActivity();
                else {
                    Common.alertDialog("Hơp đồng đã đăng ký Internet", getActivity());
                }
            }
        });
        txtPromotion.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                loadComboPromotion();
                lblPromotion.setText(txtPromotion.getText());
            }
        });
        // khuyến mãi combo
        schCombo = (Switch) rootView.findViewById(R.id.sch_combo);
        schCombo.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                enableCombo(isChecked);
                if (isChecked) {
                    loadComboPromotion();
                } else {
                    try {
                        spPromotionCombo.setAdapter(new PromotionComboAdapter(mContext, R.layout.row_auto_complete, null));
                        lblPromotionCombo.setText(null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        spPromotionCombo = (Spinner) rootView.findViewById(R.id.sp_combo_promotion);
        spPromotionCombo.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PromotionComboModel item = (PromotionComboModel) parent.getItemAtPosition(position);
                lblPromotionCombo.setText(item.getDescription());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        lblPromotionCombo = (TextView) rootView.findViewById(R.id.lbl_combo_promotion_desc);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDataFromActivity();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case CODE_LOCAL_TYPE:
                    mLocalType = data.getParcelableExtra(Constants.LOCAL_TYPE);
                    tvLocalType.setText(mLocalType.getLocalTypeName());
                    if (mObject != null) { mObject.setLocalType(mLocalType.getLocalTypeID());
                    } else { mRegister.setLocalType(mLocalType.getLocalTypeID()); }
                    loadPromotion();
                    break;
                case 1:
                    if (data.hasExtra(PromotionFilterActivity.TAG_PROMOTION_ITEM)) {
                        PromotionModel item = data.getParcelableExtra(PromotionFilterActivity.TAG_PROMOTION_ITEM);
                        if (item != null) {
                            FragmentRegisterContractIPTV.isBind = 1;
                            mPromotion = item;
                            txtPromotion.setText(item.getPromotionName());
                        }
                        txtPromotion.setError(null);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /*
     * ==========================
     * Lấy thông tin đối tượng PĐK từ Activity tạo PĐK
     * ============================
     */
    private void getDataFromActivity() {
        if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
            activity = (RegisterContractActivity) mContext;
            mRegister = activity.getRegister();
            ObjectDetailModel mObject = ((RegisterContractActivity) mContext).getObject();
            if (mObject != null) {
                this.mObject = mObject;
            }
        }
        enableInternetAndCombo();
    }

    private void loadPromotion() {
        String contract;
//        if (mRegister != null) {
        String locationID = String.valueOf(((MyApp) mContext.getApplicationContext()).getLocationID());
        localTypeID = mLocalType.getLocalTypeID();
        if (activity != null && activity.getObject() != null) {
            contract = activity.getObject().getContract();
        } else {
            contract = mRegister != null ? mRegister.getContract() : "";
        }
        new GetPromotionList(mContext, this, locationID, localTypeID, 2, contract);
//        }
    }

    /*
     * ==========================
     * lấy đối tượng CLKM từ DS CLKM dựa vào ID
     * Nếu tồn tại CLKM trong DS thì gán, nếu không thì sẽ lấy CLKM của HĐ để load CLMK Combo
     * ============================
     */
    public void updatePromotion(ArrayList<PromotionModel> lst) {
        txtPromotion.setText(null);
        if (lst != null && lst.size() > 0) {
            if (mRegister != null) {
                int id = mRegister.getPromotionID();
                int position = Common.indexOf(lst, id);
                if (position >= 0) {
                    mPromotion = lst.get(position);
//                    txtPromotion.setText(mPromotion.getPromotionName());
                } else {
//                    txtPromotion.setText(mRegister.getContractPromotionName());
                    mPromotion = null;
                }
            }
        } else {
            Common.alertDialog("Không có câu lệnh khuyến mãi", getActivity());
        }
    }

    /*
     * ==========================
     * Start Activity tìm CLKM khi click vào EditText CLMK Internet.
     * ============================
     */
    private void showFilterActivity() {
        try {
            int localType, serviceType = 2;
            String contract = null;
            if (activity != null && activity.getObject() != null) {
                contract = activity.getObject().getContract();
                serviceType = activity.getObject().getServiceType();
            } else if (mRegister != null) {
                contract = mRegister.getContact();
                serviceType = mRegister.getContractServiceType();
            }
            localType = mLocalType.getLocalTypeID();
            Intent intent = new Intent(mContext, PromotionFilterActivity.class);
            intent.putExtra(PromotionFilterActivity.TAG_LOCAL_TYPE, localType);
            intent.putExtra(PromotionFilterActivity.TAG_SERVICE_TYPE, serviceType);
            intent.putExtra(PromotionFilterActivity.TAG_CONTRACT, contract);
            startActivityForResult(intent, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * ==========================
     * Load DS CLKM Combo dựa vào ID CLMK Intenret
     * Nếu tạo mới PĐK: lấy ID CLKM Internet  của HĐ
     * Nếu Update PĐK: lấy theo ID CLKM Internet PĐK, không có thì lấy ID CLKM Internet  HĐ của PĐK
     * ============================
     */
    private void loadComboPromotion() {
        try {
            int PromotionID;
            if (schCombo.isChecked()) {
                if (mPromotion != null) {
                    PromotionID = mPromotion.getPromotionID();
                } else {
                    if (mRegister != null) {
                        PromotionID = mRegister.getContractPromotionID();
                    } else {
                        ObjectDetailModel md = activity.getObject();
                        PromotionID = md.getPromotionID();
                    }
                }
                int LocalType;
                String Contract = "";
                LocalType = mLocalType.getLocalTypeID();
                if (activity != null && activity.getObject() != null)
                    Contract = activity.getObject().getContract();
                else if (mRegister != null)
                    Contract = mRegister.getContract();
                new GetPromotionCombo(mContext, this, LocalType, PromotionID, Contract);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * udpate lại DS CLKM Combo.
     *
     * @param lst
     * @author ISC_DuHK
     */
    public void updatePromotionCombo(ArrayList<PromotionComboModel> lst) {
        try {
            if (lst != null && lst.size() != 0) {
                PromotionComboAdapter adapter = new PromotionComboAdapter(mContext, R.layout.row_auto_complete, lst);
                spPromotionCombo.setAdapter(adapter);
                int id = 0;
                if (mRegister != null && mRegister.getPromotionComboID() > 0) {
                    id = mRegister.getPromotionComboID();
                }
                spPromotionCombo.setSelection(getPromotionComboIndex(lst, id), true);
            } else {
                spPromotionCombo.setAdapter(new PromotionComboAdapter(mContext, R.layout.row_auto_complete, null));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getPromotionComboIndex(ArrayList<PromotionComboModel> lst, int promotionID) {
        if (lst != null) {
            for (int i = 0; i < lst.size(); i++) {
                if (lst.get(i).getComboID() == promotionID)
                    return i;
            }
        }
        return -1;
    }

    /*
     * Kiem tra dieu kien truoc khi cap nhat PDK
     *
     * @return: true - OK
     * @author ISC_DuHK
     */
    // kiểm tra khi kéo sang bên trái hoặc bên phải màn hình tạo pdk bán thêm
    public boolean checkForUpdate() {
        if (schInternet.isChecked()) {
            if (TextUtils.isEmpty(txtPromotion.getText().toString())) {
                Common.alertDialog("Chưa chọn Câu lệnh khuyến mãi", getActivity());
                return false;
            }
        }
        return true;
    }

    /*
     * Cap nhat thong tin PDK (bien dung chung nam o Activity)
     *
     * @author ISC_DuHK
     */
    // cập nhật thông tin pdk sau khi chuyển trang qua lại
    public void updateRegister() {
        try {
            if (activity != null) {
                RegistrationDetailModel register = activity.getRegister();
                /*neu co dang ky Internet*/
                int localType, promotionID = 0, internetTotal = 0, comboID;
                String promotionDesc = "";
                localType = mLocalType.getLocalTypeID();
                if (schInternet.isChecked()) {
                    if (register == null) {
                        register = new RegistrationDetailModel();
                    }
                    if (mPromotion != null) {
                        promotionID = mPromotion.getPromotionID();
                        promotionDesc = mPromotion.getPromotionName();
                        internetTotal = mPromotion.getRealPrepaid();
                    } else {
                        promotionID = register.getPromotionID();
                        promotionDesc = register.getPromotionName();
                        internetTotal = register.getInternetTotal();
                    }
                }
                if (schCombo.isChecked()) {
                    if (spPromotionCombo.getAdapter() != null && spPromotionCombo.getAdapter().getCount() > 0) {
                        comboID = ((PromotionComboModel) spPromotionCombo.getSelectedItem()).getComboID();
                        register.setPromotionComboID(comboID);
                    }
                } else {
                    register.setPromotionComboID(0);
                }
                // trạng thái check switch chọn internet
                if (schInternet.isChecked()) {
                    register.setLocalType(localType);
                    register.setPromotionID(promotionID);
                    register.setPromotionName(promotionDesc);
                    register.setInternetTotal(internetTotal);
                } else {
                    register.setLocalType(localType);
                    register.setPromotionID(0);
                    register.setPromotionName("");
                    register.setInternetTotal(0);
                }
//                register.setLocalType(localType);
//                register.setPromotionID(promotionID);
//                register.setPromotionName(promotionDesc);
//                register.setInternetTotal(internetTotal);
                activity.setRegister(register);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /*
     * ==========================
     * bật/tắt chức năng đăng ký NET và Combo dựa vào thông tin HĐ
     * ============================
     */
    private void enableInternetAndCombo() {
        try {
            int serviceType = -1, comBoStatus = 0, promotionID = 0, regComboID = 0;
            String promotionName = "";
            if (activity != null && activity.getObject() != null) {
                serviceType = activity.getObject().getServiceType();
                comBoStatus = activity.getObject().getComboStatus();
                promotionName = activity.getObject().getPromotionName();
                promotionID = activity.getObject().getPromotionID();
                mLocalType = new LocalType(activity.getObject().getLocalType(), activity.getObject().getLocalTypeName());
            } else if (mRegister != null) {
                serviceType = mRegister.getContractServiceType();
                comBoStatus = mRegister.getContractComboStatus();
                promotionID = mRegister.getPromotionID();
                promotionName = mRegister.getContractPromotionName();
                regComboID = mRegister.getPromotionComboID();
                mLocalType = new LocalType(mRegister.getLocalType(), mRegister.getLocalTypeName());
            }
            tvLocalType.setText(mLocalType.getLocalTypeName());

            /*Khong cho dang ky them internet neu HD da DK internet*/
            if (serviceType == 0 || serviceType == 2) {
                schInternet.setChecked(false);
                schInternet.setEnabled(false);
                schInternet.setVisibility(View.GONE);
                txtPromotion.setText(promotionName);
            } else {
                schInternet.setChecked(false);
                tvLocalType.setEnabled(false);
                schInternet.setEnabled(true);
                schInternet.setVisibility(View.VISIBLE);
            }
            if (comBoStatus > 0) { //Da dang ky combo
                schCombo.setChecked(false);
                schCombo.setEnabled(false);
            } else {
                /*Neu PDK da dang ky combo truoc do thi load lại/khong thi dong di*/
                if (regComboID > 0) {
                    schCombo.setChecked(true);
                } else {
                    schCombo.setChecked(false);
                }
                schCombo.setEnabled(true);
            }
            /*Nếu được đăng ký Internet mà CLKM = 0 => không đăng ký Intenret*/
            if (schInternet.isEnabled()) {
                if (promotionID == 0) {
                    schInternet.setChecked(false);
                }
            }

            if (mRegister != null && promotionID > 0) {
                schInternet.setChecked(true);
                schCombo.setChecked(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * ==========================
     * bật/tắt chức năng đăng ký NET
     * ============================
     */
    public void enableInternet(boolean enabled) {
        try {
            tvLocalType.setEnabled(enabled);
            txtPromotion.setEnabled(enabled);
            lblPromotion.setEnabled(enabled);
            txtPromotion.setClickable(enabled);
            if (enabled) {
                if (mPromotion != null) {
                    txtPromotion.setText(mPromotion.getPromotionName());
                    tvLocalType.setText(mLocalType.getLocalTypeName());
                } else if (mRegister != null) {
                    txtPromotion.setText(mRegister.getPromotionName());
                    tvLocalType.setText(mLocalType.getLocalTypeName());
                } else if (activity != null) {
                    txtPromotion.setText(null);
                    tvLocalType.setText(null);
                }
            } else {
                if (mRegister != null) {
                    txtPromotion.setText(mRegister.getPromotionName());
                    tvLocalType.setText(mLocalType.getLocalTypeName());
                } else if (activity != null) {
                    txtPromotion.setText(null);
                    tvLocalType.setText(mLocalType.getLocalTypeName());
                } else {
                    txtPromotion.setText(null);
                }
            }
            schCombo.setChecked(enabled);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * ==========================
     * bật/tắt chức năng đăng ký Combo dựa vào thông tin HĐ
     * ============================
     */
    public void enableCombo(boolean enabled) {
        spPromotionCombo.setEnabled(enabled);
        lblPromotionCombo.setEnabled(enabled);
    }
}