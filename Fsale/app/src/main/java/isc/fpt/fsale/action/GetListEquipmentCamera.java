package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.device_list.DeviceListActivity;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.device_list.DeviceListCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.fragment.FptCameraService;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetListEquipmentCamera implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public GetListEquipmentCamera(Context mContext, String mClassName) {
        this.mContext = mContext;
        String[] arrParamName = new String[]{"UserName", "Type"};
        String[] arrParamValue = new String[]{
                Constants.USERNAME,
                mClassName.equals(FptCameraService.class.getSimpleName()) ? "0" : "1" // 0 - bán mới , 1  - bán thêm
        };
        String message = mContext.getResources().getString(
                R.string.msg_pd_get_device_list);
        String GET_DEVICE_LIST = "GetListEquipmentCamera";
        CallServiceTask service = new CallServiceTask(mContext, GET_DEVICE_LIST, arrParamName,
                arrParamValue, Services.JSON_POST, message, GetListEquipmentCamera.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            List<Device> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                String LIST_OBJECT = "ListObject";
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                if (jsObj.getInt("ErrorCode") == 0) {// OK not Error
                    JSONArray jsonArray = jsObj.getJSONArray(LIST_OBJECT);
                    for(int i = 0; i < jsonArray.length(); i++){
                        lst.add(new Gson().fromJson(jsonArray.get(i).toString(), Device.class));
                    }
                } else {
                    Common.alertDialog(jsObj.getString("Error"), mContext);
                }

                if (lst.size() > 0) {
                    if (mContext != null) {
                        ((DeviceListCameraActivity) mContext).loadDeviceList(lst);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}