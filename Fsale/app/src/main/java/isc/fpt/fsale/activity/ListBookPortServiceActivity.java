package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetLocationBranchPOPAction;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ListBookPortAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

public class ListBookPortServiceActivity extends BaseActivity implements OnItemClickListener {

    private ArrayList<RowBookPortModel> lstbookport = new ArrayList<RowBookPortModel>();
    private ListView lvBilling;
    private ListBookPortAdapter adapter;
    private FragmentManager fm;
    private Button bttclose;
    private Context mContext;
    private String SOPHIEUDK, ID;
    private ImageView imgSearchTapDiem;
    private Spinner spTapDiemType;
    private int iTapDiemType;
    private EditText txtTapDiem, txtUserName, txtSoPhieu, txtSoPhieuID;
    private RegistrationDetailModel modelRegister;

    public ListBookPortServiceActivity() {
        super(R.string.menu_book_port);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_port);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_bookport_service_activity));
        mContext = ListBookPortServiceActivity.this;

        fm = this.getSupportFragmentManager();

        lvBilling = (ListView) findViewById(R.id.lv_billing_detail);
        lvBilling.setItemsCanFocus(true);
        lvBilling.setOnItemClickListener(this);
        lvBilling.setTextFilterEnabled(true);
        lvBilling.setSelection(0);

        imgSearchTapDiem = (ImageView) this.findViewById(R.id.btn_find_list_port);
        txtTapDiem = (EditText) this.findViewById(R.id.txt_tapdiem);
        spTapDiemType = (Spinner) this.findViewById(R.id.sp_port_type);

        // Test
        txtUserName = (EditText) this.findViewById(R.id.txtUserName);
        txtSoPhieu = (EditText) this.findViewById(R.id.txt_SoPhieu);
        txtSoPhieuID = (EditText) this.findViewById(R.id.txt_SoPhieuID);
        if (Constants.isDebug()) {
            txtUserName.setVisibility(View.VISIBLE);
            txtSoPhieu.setVisibility(View.VISIBLE);
            txtSoPhieuID.setVisibility(View.VISIBLE);
        }

        bttclose = (Button) this.findViewById(R.id.btn_close);

        bttclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        spTapDiemType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                iTapDiemType = selectedItem.getID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        imgSearchTapDiem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                RowBookPortModel selectedItem = new RowBookPortModel(0, 0, 0, 0, "", 0, 0, 0, txtTapDiem.getText().toString().trim(), String.valueOf(iTapDiemType), iTapDiemType, "", "", "", 0);
                if (Constants.isDebug()) {
                    new GetLocationBranchPOPAction(fm, mContext, selectedItem, txtSoPhieu.getText().toString(), txtSoPhieuID.getText().toString(), modelRegister);
                } else
                    new GetLocationBranchPOPAction(fm, mContext, selectedItem, SOPHIEUDK, ID, modelRegister);
            }
        });

        try {
            Intent myIntent = getIntent();
            int TapDiemType = -1;
            if (myIntent != null && myIntent.getExtras() != null) {
                lstbookport = myIntent.getParcelableArrayListExtra("list_model_send");
                this.SOPHIEUDK = myIntent.getStringExtra("Number_Register");
                this.ID = myIntent.getStringExtra("ID_Register");
                TapDiemType = myIntent.getIntExtra("BookPort_Type", -1);
            }

            setListTapDiemType(TapDiemType);
            adapter = new ListBookPortAdapter(mContext, lstbookport);
            lvBilling.setAdapter(adapter);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        RowBookPortModel selectedItem = (RowBookPortModel) parentView.getItemAtPosition(position);
        new GetLocationBranchPOPAction(fm, mContext, selectedItem, this.SOPHIEUDK, this.ID, modelRegister);
    }

    // Gắn danh sách loại bookport
    public void setListTapDiemType(int type) {
        ArrayList<KeyValuePairModel> lstBookPortType = new ArrayList<KeyValuePairModel>();
        if (type == 0)
            lstBookPortType.add(new KeyValuePairModel(0, "ADSL"));
        if (type == 1)
            lstBookPortType.add(new KeyValuePairModel(1, "FTTH"));

        lstBookPortType.add(new KeyValuePairModel(2, "FTTHNew"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstBookPortType, Gravity.LEFT);
        spTapDiemType.setAdapter(adapter);
    }

    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }
}
