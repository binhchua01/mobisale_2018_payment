package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.crashlytics.android.Crashlytics;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingFragmentActivity;

import io.fabric.sdk.android.Fabric;
import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.MenuListFragment_LeftSide;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.MyApp;

import static isc.fpt.fsale.utils.Common.showScreenActionBar;

@SuppressLint("Registered")
public class BaseActivity extends SlidingFragmentActivity {
    private int mTitleRes;
    private boolean IS_SHOW_LEFT_MENU = false;
    @Nullable
    View layoutContainer;

    public BaseActivity(int titleRes) {
        mTitleRes = titleRes;
    }

    public BaseActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(mTitleRes);
        Fabric.with(this, new Crashlytics());
        logUser();
        addLeft();
        showScreenActionBar(this);
        layoutContainer = findViewById(R.id.container);
        if(layoutContainer != null){
            hiddenKeySoft(layoutContainer);
        }
    }

    private void logUser() {
        MyApp myApp = ((MyApp) getApplicationContext());
        DeviceInfo info = new DeviceInfo(getApplicationContext());
        Crashlytics.setUserIdentifier(info.GetDeviceIMEI(this));
        if (myApp != null) {
            Crashlytics.setUserName(myApp.getUserName());
        }
    }

    //left side sliding menu
    private void addLeft() {
        FrameLayout left = new FrameLayout(BaseActivity.this);
        left.setId("LEFT".hashCode());
        setBehindLeftContentView(left);
        getSupportFragmentManager()
                .beginTransaction()
                .replace("LEFT".hashCode(), new MenuListFragment_LeftSide())
                .commit();
        // customize the left side SlidingMenu
        Constants.SLIDING_MENU = getSlidingMenu();
        Constants.SLIDING_MENU.setShadowDrawable(R.drawable.shadow, SlidingMenu.LEFT);
        Constants.SLIDING_MENU.setBehindOffsetRes(R.dimen.slidingmenu_offset, SlidingMenu.LEFT);
        Constants.SLIDING_MENU.setFadeDegree(0.35f);
        Constants.SLIDING_MENU.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.main_color_blue_new));
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_list);
        setSlidingActionBarEnabled(false);
    }

    protected void addRight(ListFragment secondMenu) {
        FrameLayout right = new FrameLayout(this);
        right.setId("RIGHT".hashCode());
        this.setBehindRightContentView(right);
        getSupportFragmentManager()
                .beginTransaction()
                .replace("RIGHT".hashCode(), secondMenu)
                .commit();
        Constants.SLIDING_MENU.setShadowDrawable(R.drawable.shadow_right, SlidingMenu.RIGHT);
        Constants.SLIDING_MENU.setBehindOffsetRes(R.dimen.slidingmenu_offset, SlidingMenu.RIGHT);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            toggle(SlidingMenu.LEFT);
            IS_SHOW_LEFT_MENU = !IS_SHOW_LEFT_MENU;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isShowLeftMenu() {
        return IS_SHOW_LEFT_MENU;
    }

    public void toggle(int side) {
        Common.hideSoftKeyboard(this);
        super.toggle(side);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @SuppressLint("ClickableViewAccessibility")
    public void hiddenKeySoft(View view) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    hideKeyboardOutside(view.getContext(), view);
                    view.clearFocus();
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hiddenKeySoft(innerView);
            }
        }
    }

    /**
     * hidden soft keybroad when un-focus edit text
     */
    protected void hideKeyboardOutside(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
