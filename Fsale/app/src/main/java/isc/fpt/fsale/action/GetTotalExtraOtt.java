package isc.fpt.fsale.action;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CalculatorExtraOttTotalRequest;
import isc.fpt.fsale.model.CartExtraOttTotal;
import isc.fpt.fsale.model.ExtraOttTotalResponse;
import isc.fpt.fsale.model.ServiceListExtraOtt;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.extra_ott.ExtraOttActivity;
import isc.fpt.fsale.ui.extra_ott.RegisterExtraOttModel;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 21,July,2019
 */
public class GetTotalExtraOtt implements AsyncTaskCompleteListener<String> {
    private static final String GET_TOTAL_EXTRA_OTT = "GetTotalAmount_ExOTT";
    private ExtraOttActivity activity;

    public GetTotalExtraOtt(ExtraOttActivity activity, RegisterExtraOttModel object) {
        this.activity = activity;
        List<ServiceListExtraOtt> list = object.getCartExtraOtt().getServiceList();
        List<CartExtraOttTotal> listCart = new ArrayList<>();
        for (ServiceListExtraOtt item : list) {
            listCart.add(new CartExtraOttTotal(item.getPromoCommand().getID(), item.getQuantity()));
        }
        CalculatorExtraOttTotalRequest request = new CalculatorExtraOttTotalRequest(Constants.USERNAME, listCart);
        String message = activity.getResources().getString(R.string.txt_message_calculator_extra_ott_total);
        CallServiceTask service = new CallServiceTask(activity, GET_TOTAL_EXTRA_OTT, request.toJsonObject(),
                Services.JSON_POST_OBJECT, message, GetTotalExtraOtt.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<ExtraOttTotalResponse> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), ExtraOttTotalResponse.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), activity);
                } else {
                    activity.resultExtraOttTotal(lst);
                }
            }
        } catch (JSONException e) {
            Common.alertDialog(activity.getResources().getString(R.string.msg_error_data), activity);
        }
    }
}
