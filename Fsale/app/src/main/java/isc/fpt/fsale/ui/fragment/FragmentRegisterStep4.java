package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetContractDepositList;
import isc.fpt.fsale.action.GetDisCountRpVoucherTotal;
import isc.fpt.fsale.action.GetIPTVTotal;
import isc.fpt.fsale.action.GetInternetTotal;
import isc.fpt.fsale.action.GetListGiftBox;
import isc.fpt.fsale.action.GetListGiftBoxOTT;
import isc.fpt.fsale.action.GetPaymentType;
import isc.fpt.fsale.action.GetTotalFPTBoxDevice;
import isc.fpt.fsale.action.TotalPriceEquipment;
import isc.fpt.fsale.action.camera319.GetCamera2Total;
import isc.fpt.fsale.action.maxy.GetMaxyTotal;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.voucher_list.VoucherListActivity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.SpGiftBoxAdapter;
import isc.fpt.fsale.adapter.SpGiftBoxOTTAdapter;
import isc.fpt.fsale.model.DepositValueModel;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.GiftBox;
import isc.fpt.fsale.model.InternetTotal;
import isc.fpt.fsale.model.IpTvTotal;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ListGiftBoxOTTResult;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.Voucher;
import isc.fpt.fsale.model.VoucherTotal;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.model.camera319.Camera2Total;
import isc.fpt.fsale.model.camera319.PromoCloud;
import isc.fpt.fsale.ui.base.BaseFragment;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCameraOfNet;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.ui.maxytv.model.MaxyTotal;
import isc.fpt.fsale.ui.maxytv.model.ObjectMaxy;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class FragmentRegisterStep4 extends BaseFragment {
    private RegistrationDetailModel modelDetail = null;

    public ScrollView scrollMain;
    public View totalPriceLayout, layoutTotalFptPlay, layoutTotalMaxyTv,
            layoutTotalDevice, layoutTotalInternet, layoutDepositPayment,
            layoutSurvey, layoutGift, layoutVoucherTotal, layoutVoucher, layoutChooseVoucher, layoutGiftFptPlay, layoutCamera;
    public int giftId;
    public Spinner spDepositBlackPoint, SpDepositNewObject, spPaymentType, spGift, spGiftFptPlay;
    public TextView lblMaxyTVTotal, lblInternetTotal, lblTotal, txtOutdoor,
            txtIndoor, lblOttTotal, lblDeviceTotal, tvVoucherTotal, tvVoucherName, tvRpDescription, tvCamera;
    public ImageView imgReloadPaymentType;
    public Button btnCreate, btnApplyVoucher, btnUnApplyVoucher;
    public boolean is_have_device, is_have_fpt_box, is_have_ip_tv, is_clear_rp_and_voucher, is_have_camera, is_have_maxy;
    public String giftName;
    private List<GiftBox> mListGiftBox;
    private List<ListGiftBoxOTTResult> mListGiftBoxOTT;
    private SpGiftBoxAdapter mAdapterGiftBox;
    private SpGiftBoxOTTAdapter mAdapterGiftBoxOTT;
    public EditText edtReferralCode;
    private final int CODE_VOUCHER = 0;
    public Voucher mVoucher;
    public InternetTotal mInternetTotal;
    public IpTvTotal mIpTvTotal;
    public Camera2Total mCamera2Total;
    public MaxyTotal mMaxyTotal;
    public LinearLayout layoutDisable, layoutReferralCode;

    public static FragmentRegisterStep4 newInstance(RegistrationDetailModel modelDetail) {
        return new FragmentRegisterStep4(modelDetail);
    }

    @SuppressLint("ValidFragment")
    public FragmentRegisterStep4(RegistrationDetailModel modelDetail) {
        super();
        this.modelDetail = modelDetail;
    }

    public FragmentRegisterStep4() {
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSpinners();
        initSpinnerPaymentType();
    }

    @Override
    protected void initView(View view) {
        totalPriceLayout = view.findViewById(R.id.frm_total_price);
        layoutTotalFptPlay = view.findViewById(R.id.layout_total_fpt_play);
        layoutTotalMaxyTv = view.findViewById(R.id.layout_total_maxy_tv);
        layoutTotalDevice = view.findViewById(R.id.layout_total_device);
        layoutTotalInternet = view.findViewById(R.id.layout_total_internet);
        layoutDepositPayment = view.findViewById(R.id.layout_deposit_payment);
        layoutSurvey = view.findViewById(R.id.layout_survey);
        layoutGift = view.findViewById(R.id.layout_gift);
        layoutVoucherTotal = view.findViewById(R.id.layout_total_voucher);
        layoutVoucher = view.findViewById(R.id.layout_voucher);
        layoutGiftFptPlay = view.findViewById(R.id.layout_gift_fpt_play);
        scrollMain = (ScrollView) view.findViewById(R.id.frm_main);
        spDepositBlackPoint = (Spinner) view.findViewById(R.id.sp_deposit_black_point);
        SpDepositNewObject = (Spinner) view.findViewById(R.id.sp_deposit_new_object);
        lblMaxyTVTotal = (TextView) view.findViewById(R.id.txt_total_maxy);
        lblInternetTotal = (TextView) view.findViewById(R.id.txt_total_internet);
        lblTotal = (TextView) view.findViewById(R.id.txt_register_total);
        spPaymentType = (Spinner) view.findViewById(R.id.sp_payment_type);
        imgReloadPaymentType = (ImageView) view.findViewById(R.id.img_reload_payment_type);
        txtOutdoor = (TextView) view.findViewById(R.id.txt_outdoor);
        tvVoucherName = (TextView) view.findViewById(R.id.tv_voucher_name);
        layoutDisable = (LinearLayout) view.findViewById(R.id.layout_on_off_disable);
        txtIndoor = (TextView) view.findViewById(R.id.txt_indoor);
        btnCreate = (Button) view.findViewById(R.id.btn_create_registration);
        lblOttTotal = (TextView) view.findViewById(R.id.txt_ott_total);
        lblDeviceTotal = (TextView) view.findViewById(R.id.txt_device_total);
        spGift = (Spinner) view.findViewById(R.id.sp_gift);
        spGiftFptPlay = (Spinner) view.findViewById(R.id.sp_gift_fpt_play);
        tvVoucherTotal = (TextView) view.findViewById(R.id.txt_total_voucher);
        edtReferralCode = (EditText) view.findViewById(R.id.edt_referral_code);
        layoutChooseVoucher = view.findViewById(R.id.layout_choose_e_voucher);
        btnApplyVoucher = (Button) view.findViewById(R.id.btn_apply_e_voucher);
        btnUnApplyVoucher = (Button) view.findViewById(R.id.btn_un_apply_e_voucher);
        layoutReferralCode = view.findViewById(R.id.layout_referral_code);
        layoutCamera = view.findViewById(R.id.layout_total_camera);
        if (((RegisterActivityNew) getActivity()).getListService() != null &&
                ((RegisterActivityNew) getActivity()).getListService().get(0).getCategoryServiceID() == 3) {
            layoutTotalFptPlay.setVisibility(View.VISIBLE);
            layoutGiftFptPlay.setVisibility(View.VISIBLE);
            layoutTotalMaxyTv.setVisibility(View.GONE);
            layoutTotalDevice.setVisibility(View.GONE);
            layoutTotalInternet.setVisibility(View.GONE);
            layoutDepositPayment.setVisibility(View.GONE);
            layoutSurvey.setVisibility(View.GONE);
            layoutGift.setVisibility(View.GONE);
            layoutVoucherTotal.setVisibility(View.GONE);
            layoutVoucher.setVisibility(View.GONE);
            layoutCamera.setVisibility(View.GONE);
        }
        tvRpDescription = (TextView) view.findViewById(R.id.tv_rp_description);
        tvCamera = view.findViewById(R.id.txt_camera_total);

    }

    @Override
    protected void initEvent() {
        txtOutdoor.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtOutdoor.setError(null);
            }
        });

        txtIndoor.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtIndoor.setError(null);
            }
        });

        imgReloadPaymentType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //kết nối api lấy danh sách hình thức thanh toán cước hàng tháng
                ((RegisterActivityNew) getActivity()).Update(false);
                new GetPaymentType(
                        getActivity(),
                        FragmentRegisterStep4.this,
                        ((RegisterActivityNew) getActivity()).getRegistrationDetail()
                );
            }
        });

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RegisterActivityNew) getActivity()).updateOnclick();
            }
        });

        spDepositBlackPoint.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                try {
                    KeyValuePairModel selectedItem = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                    if (selectedItem.getID() != 0) SpDepositNewObject.setSelection(0, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                updateTotal();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        SpDepositNewObject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                try {
                    KeyValuePairModel selectedItem = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                    if (selectedItem.getID() != 0) spDepositBlackPoint.setSelection(0, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                updateTotal();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        layoutChooseVoucher.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ((RegisterActivityNew) getActivity()).Update(false);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.REGISTRATION_INTERNET,
                        ((RegisterActivityNew) getActivity()).getRegistrationDetail());
                Intent intent = new Intent(getActivity(), VoucherListActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_VOUCHER);
            }
        });

        btnApplyVoucher.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getVoucherTotal();
            }
        });

        btnUnApplyVoucher.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                disableEnableControlsEVoucher(layoutDisable, true);
                clearDataRpVoucher(false);
            }
        });

        if (Constants.IS_ENABLE_REFERRAL == 1) {
            layoutReferralCode.setVisibility(View.VISIBLE);
            edtReferralCode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    tvRpDescription.setText(null);
                    tvRpDescription.setVisibility(View.GONE);
                }
            });
        } else {
            layoutReferralCode.setVisibility(View.GONE);
        }
    }

    @Override
    protected void bindData() {
        if (modelDetail != null) {
            txtOutdoor.setText(String.valueOf(modelDetail.getOutDoor()));
            txtIndoor.setText(String.valueOf(modelDetail.getInDoor()));
            edtReferralCode.setText(modelDetail.getReferralCode());
            if (!TextUtils.isEmpty(modelDetail.getVoucherCode()) && !TextUtils.isEmpty(modelDetail.getVoucherCodeDesc())) {
                mVoucher = new Voucher(modelDetail.getVoucherCodeDesc(), modelDetail.getVoucherCode());
                tvVoucherName.setText(mVoucher.getDescription());
            }
            if (!TextUtils.isEmpty(modelDetail.getReferralCodeDesc()) && !TextUtils.isEmpty(modelDetail.getReferralCode())) {
                tvRpDescription.setVisibility(View.VISIBLE);
                tvRpDescription.setText(modelDetail.getReferralCodeDesc());
            }
            tvVoucherTotal.setText(Common.formatNumberNegative(modelDetail.getDiscountRP_VoucherTotal()));
            if (!TextUtils.isEmpty(edtReferralCode.getText().toString()) || mVoucher != null) {
                disableEnableControlsEVoucher(layoutDisable, false);
            }
        }
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_register_step4;
    }

    private void initSpinnerPaymentType() {
        if (Common.hasPreference(getActivity(), Constants.SHARE_PRE_OBJECT, Constants.SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST)) {
            loadSpinnerPaymentType();
        } else {
            ((RegisterActivityNew) getActivity()).Update(false);
            new GetPaymentType(getActivity(), this, ((RegisterActivityNew) getActivity()).getRegistrationDetail());
        }
    }

    public void loadSpinnerPaymentType() {
        String deptStr = Common.loadPreference(getActivity(), Constants.SHARE_PRE_OBJECT,
                Constants.SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST);
        JSONObject jObj;
        WSObjectsModel<KeyValuePairModel> wsObject = null;
        ArrayList<KeyValuePairModel> listAgent;
        try {
            jObj = new JSONObject(deptStr);
            jObj = jObj.getJSONObject(Constants.RESPONSE_RESULT);
            wsObject = new WSObjectsModel<>(jObj, KeyValuePairModel.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (wsObject != null) {
            listAgent = wsObject.getArrayListObject();
        } else {
            listAgent = new ArrayList<>();
            listAgent.add(new KeyValuePairModel(0, "[ Không có dữ liệu ]"));
        }

        @SuppressLint("RtlHardcoded")
        KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(getActivity(),
                R.layout.my_spinner_style, listAgent, Gravity.LEFT);
        spPaymentType.setAdapter(adapterStatus);
        if (modelDetail != null) {
            spPaymentType.setSelection(Common.getIndex(spPaymentType, modelDetail.getPayment()), true);
        }
    }


    public void clearDataRpVoucher(boolean isClearAll) {
        if (isClearAll) {
            edtReferralCode.setText(null);
            tvRpDescription.setVisibility(View.GONE);
            tvRpDescription.setText(null);
        }
        tvVoucherName.setText(null);
        mVoucher = null;
        tvVoucherTotal.setText(Common.formatNumber(0));
        updateTotal();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            if (requestCode == CODE_VOUCHER) {
                mVoucher = data.getParcelableExtra(Constants.VOUCHER);
                //set service type name to view
                tvVoucherName.setText(mVoucher.getDescription());
            }
        }
    }

    // Đặt cọc thuê bao
    private void initSpinners() {
        setSpGiftBoxOTT();
        setSpNewObjectDeposit();
        setSpGiftBox();
    }

    private void setSpGiftBoxOTT() {
        mListGiftBoxOTT = new ArrayList<>();
        mAdapterGiftBoxOTT = new SpGiftBoxOTTAdapter(getActivity(), R.layout.item_spinner,
                R.id.text_view_0, (ArrayList<ListGiftBoxOTTResult>) this.mListGiftBoxOTT);
        spGiftFptPlay.setAdapter(mAdapterGiftBoxOTT);
        spGiftFptPlay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                ListGiftBoxOTTResult mGiftBoxOTT = mAdapterGiftBoxOTT.getItem(position);
                if (mGiftBoxOTT != null) {
                    giftId = mGiftBoxOTT.getGiftID();
                    giftName = mGiftBoxOTT.getGiftName();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void loadDataSpGiftBoxOTT(List<ListGiftBoxOTTResult> mListGiftBoxOTT) {
        this.mListGiftBoxOTT = mListGiftBoxOTT;
        mAdapterGiftBoxOTT.notifyData(this.mListGiftBoxOTT);
        //load old data
        if (modelDetail != null) {//cập nhật PDK bán mới
            if (FragmentRegisterStep3v2.isBind == 0) {//load lại quà tặng của PDK
                spGiftFptPlay.setSelection(Common.getIndex(spGiftFptPlay, modelDetail.getGiftID()), true);
                FragmentRegisterStep3v2.isBind = 2;
            } else if (FragmentRegisterStep3v2.isBind == 1) {//user thay đổi FPT Box
                spGiftFptPlay.setSelection(Common.getIndex(spGiftFptPlay, 0), true);
                FragmentRegisterStep3v2.isBind = 2;
            }
        } else {//Tạo PDK
            if (FragmentRegisterStep3v2.isBind == 1) {//user thay đổi FPT Box
                spGiftFptPlay.setSelection(Common.getIndex(spGiftFptPlay, 0), true);
                FragmentRegisterStep3v2.isBind = 2;
            }
        }
    }

    public void getGiftBoxListOTT() {
        new GetListGiftBoxOTT(getActivity(), FragmentRegisterStep4.this, ((RegisterActivityNew) getActivity()).getListGiftBoxOTT());
    }

    private void setSpGiftBox() {
        mListGiftBox = new ArrayList<>();
        mAdapterGiftBox = new SpGiftBoxAdapter(getActivity(), R.layout.item_spinner,
                R.id.text_view_0, (ArrayList<GiftBox>) this.mListGiftBox);
        spGift.setAdapter(mAdapterGiftBox);
        spGift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                GiftBox mGiftBox = mAdapterGiftBox.getItem(position);
                if (mGiftBox != null) {
                    giftId = mGiftBox.getGiftID();
                    giftName = mGiftBox.getGiftName();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void loadDataSpGiftBox(List<GiftBox> mListGiftBox) {
        this.mListGiftBox = mListGiftBox;
        mAdapterGiftBox.notifyData(this.mListGiftBox);
        //load old data
        if (modelDetail != null) {//cập nhật PDK bán mới
            if (FragmentRegisterStep3v2.isBind == 0) {//load lại quà tặng của PDK
                spGift.setSelection(Common.getIndex(spGift, modelDetail.getGiftID()), true);
                FragmentRegisterStep3v2.isBind = 2;
            } else if (FragmentRegisterStep3v2.isBind == 1) {//user chọn lại CLKM
                spGift.setSelection(Common.getIndex(spGift, 0), true);
                FragmentRegisterStep3v2.isBind = 2;
            }
        } else {//Tạo PDK
            if (FragmentRegisterStep3v2.isBind == 1) {//user chọn lại CLKM
                spGift.setSelection(Common.getIndex(spGift, 0), true);
                FragmentRegisterStep3v2.isBind = 2;
            }
        }
    }

    public void getGiftBoxList() {
        new GetListGiftBox(getActivity(), FragmentRegisterStep4.this, ((RegisterActivityNew) getActivity()).getListGiftBox());
    }

    //gọi API cập nhật danh sách đặt cọc thuê bao
    public void setSpNewObjectDeposit() {
        String RegCode = "";
        if (modelDetail != null)
            RegCode = modelDetail.getRegCode();
        //kết nối api lấy danh sách đặt cọc thuê bao
        new GetContractDepositList(getActivity(), this, Constants.USERNAME, RegCode);
    }

    // tải danh sách đặt cọc thuê bao
    public void updateSpContractDeposit(List<DepositValueModel> lst) {
        ArrayList<KeyValuePairModel> lstDeposit = new ArrayList<>();
        for (int i = 0; i < lst.size(); i++) {
            lstDeposit.add(new KeyValuePairModel(lst.get(i).getTotal(), lst.get(i).getTitle()));
        }
        @SuppressLint("RtlHardcoded")
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(), R.layout.my_spinner_style, lstDeposit, Gravity.LEFT);
        SpDepositNewObject.setAdapter(adapter);
        //set tiền đặt cọc thuê bao lên giao diện
        if (modelDetail != null) {
            SpDepositNewObject.setSelection(Common.getIndex(SpDepositNewObject, modelDetail.getDeposit()), true);
        }
    }

    // Cập nhật tổng tiền PĐK
    private void updateTotal() {
        int ipMaxyTotal, internetTotal, depositNewObject = 0, depositBlackPoint = 0, officeTotal = 0, ottTotal, deviceTotal, voucherDiscount, cameraTotal;
        internetTotal = Integer.valueOf(lblInternetTotal.getText().toString().replace(".", ""));
        ipMaxyTotal = Integer.valueOf(lblMaxyTVTotal.getText().toString().replace(".", ""));
        ottTotal = Integer.valueOf(lblOttTotal.getText().toString().replace(".", ""));
        deviceTotal = Integer.valueOf(lblDeviceTotal.getText().toString().replace(".", ""));
        voucherDiscount = Integer.valueOf(tvVoucherTotal.getText().toString().replace(".", ""));
        cameraTotal = Integer.valueOf(tvCamera.getText().toString().replace(".", ""));
        try {
            KeyValuePairModel tempSpDepositNewObject = ((KeyValuePairModel) SpDepositNewObject.getSelectedItem());
            if (tempSpDepositNewObject != null)
                depositNewObject = tempSpDepositNewObject.getID();
        } catch (Exception e) {
            e.printStackTrace();
        }
        int total = (ipMaxyTotal + internetTotal + deviceTotal + officeTotal + ottTotal + depositNewObject + depositBlackPoint + cameraTotal) + voucherDiscount;
        lblTotal.setText(Common.formatNumber(total));
    }

    private void getVoucherTotal() {
        if (TextUtils.isEmpty(edtReferralCode.getText().toString().trim()) && mVoucher == null) {
            Common.alertDialog(getString(R.string.message_get_total_rp_e_voucher), getActivity());
            return;
        }
        ((RegisterActivityNew) getActivity()).Update(false);
        new GetDisCountRpVoucherTotal(getActivity(), ((RegisterActivityNew) getActivity()).getRegistrationDetail(), this);
    }

    private void getInternetTotal() {
        if (!((RegisterActivityNew) getActivity()).step2.is_have_fpt_box) {
            if (((RegisterActivityNew) getActivity()).step2.mListServiceSelected.size() == 0) {
                Common.alertDialog("Chưa chọn loại dịch vụ", getActivity());
                ((RegisterActivityNew) getActivity()).setPageViewpager(1);
                return;
            } else if (((RegisterActivityNew) getActivity()).step2.localTypeId == 0) {
                Common.alertDialog("Vui lòng chọn gói dịch vụ", getActivity());
                ((RegisterActivityNew) getActivity()).setPageViewpager(1);
                return;
            }
        }
        ((RegisterActivityNew) getActivity()).Update(false);
        new GetInternetTotal(getActivity(), ((RegisterActivityNew) getActivity()).getRegistrationDetail(), this);
    }

    // tính tổng tiền thiết bị
    public void getDeviceTotalPrice(List<Device> mListDevice) {
        if (is_have_device) {
            if (mListDevice == null || mListDevice.size() == 0) {
                lblDeviceTotal.setText(Common.formatNumber(0));
                updateTotal();
            } else {
                //kết nối api lấy tổng tiền thiết bị
                new TotalPriceEquipment(this.getActivity(), mListDevice, 0, false);
            }
        } else {
            lblDeviceTotal.setText(Common.formatNumber(0));
            updateTotal();
        }
    }

    private void GetMaxyTotal() {
        if (((RegisterActivityNew) getActivity()).step2.is_have_maxy
                && ((RegisterActivityNew) getActivity()).step2.localTypeId != 0) {
            if (((RegisterActivityNew) getActivity()).step2.mMaxyGeneral.getTypeDeployID() == 0) {
                Common.alertDialog("Vui lòng chọn hình thức bán", getActivity());
                ((RegisterActivityNew) getActivity()).setPageViewpager(1);
                return;
            }
            if (((RegisterActivityNew) getActivity()).step2.mMaxyGeneral.getPackageID() == 0) {
                Common.alertDialog(getString(R.string.text_select_service_tv), getActivity());
                ((RegisterActivityNew) getActivity()).setPageViewpager(1);
                return;
            } else {
                if (((RegisterActivityNew) getActivity()).step2.mMaxyGeneral.getPromotionID() == 0) {
                    Common.alertDialog(getString(R.string.text_select_promotion_service_maxy), getActivity());
                    ((RegisterActivityNew) getActivity()).setPageViewpager(1);
                    return;
                }
            }
            if (((RegisterActivityNew) getActivity()).step2.mMaxyGeneral.getTypeSetupID() == 0) {
                Common.alertDialog(getString(R.string.text_select_setup_maxy), getActivity());
                ((RegisterActivityNew) getActivity()).setPageViewpager(1);
                return;
            }
            if (((RegisterActivityNew) getActivity()).step2.mListMaxyDevice.size() > 0) {
                List<MaxyBox> maxyBox = ((RegisterActivityNew) getActivity()).step2.mListMaxyDevice;
                for (MaxyBox item : maxyBox) {
                    if (item.getPromotionID() == 0) {
                        Common.alertDialog(getString(R.string.text_select_promotion_service_maxy_device), getActivity());
                        ((RegisterActivityNew) getActivity()).setPageViewpager(1);
                        return;
                    }
                }
            }
            if(((RegisterActivityNew) getActivity()).step2.maxyBox1 != null && ((RegisterActivityNew) getActivity()).step2.maxyBox1.getBoxID() != 0) {
                if (((RegisterActivityNew) getActivity()).step2.maxyBox1.getPromotionID() == 0) {
                    Common.alertDialog("Vui lòng chọn CLKM Box 1", getActivity());
                    ((RegisterActivityNew) getActivity()).setPageViewpager(1);
                    return;
                }
            }
            ((RegisterActivityNew) getActivity()).Update(false);
            new GetMaxyTotal(getActivity(), ((RegisterActivityNew) getActivity()).getRegistrationDetail().toJSONObjectGetMaxyTotal(), this);
        } else {
            lblMaxyTVTotal.setText(Common.formatNumber(0));
            updateTotal();
        }
    }

    private void getCameraTotal() {
        if (((RegisterActivityNew) getActivity()).step2.is_have_camera == true) {
            ((RegisterActivityNew) getActivity()).Update(false);
            new GetCamera2Total(getActivity(), ((RegisterActivityNew) getActivity()).getRegistrationDetail(), this);
        } else {
            tvCamera.setText(Common.formatNumber(0));
            updateTotal();
        }
    }

    // tính tổng tiền IPTV
//    public void getIPTVTotalPrice() {
//        if (is_have_ip_tv) {
//            ((RegisterActivityNew) getActivity()).Update(false);
//            new GetIPTVTotal(getActivity(), this, ((RegisterActivityNew) getActivity()).getRegistrationDetail());
//        } else {
//            lblIPTVTotal.setText(Common.formatNumber(0));
//        }
//    }

    // tính tổng tiền fpt box
    private void getFptBoxTotalPrice() {
        ((RegisterActivityNew) getActivity()).Update(false);
        new GetTotalFPTBoxDevice(getActivity(), ((RegisterActivityNew) getActivity()).getRegistrationDetail(), this);
    }


    public void getAllPrice(List<Device> mListDevice, ObjectMaxy maxy) {
        if (is_have_fpt_box) {
            //disable spinner SpDepositNewObject
            SpDepositNewObject.setEnabled(false);
            SpDepositNewObject.setSelection(0, true);
            //tính tổng tiền fpt play
            getFptBoxTotalPrice();
            //lấy ds quà tặng
            getGiftBoxListOTT();
        } else {
            SpDepositNewObject.setEnabled(true);
            //tính tổng tiền internet, iptv, thiết bị, discount voucher
            getInternetTotal();
            //getIPTVTotalPrice();
            getDeviceTotalPrice(mListDevice);
            getCameraTotal();
            GetMaxyTotal();
            //lấy ds quà tặng
            getGiftBoxList();
        }
    }

    public void loadVoucherDiscount(VoucherTotal mVoucherTotal) {
        tvVoucherTotal.setText(Common.formatNumberNegative(mVoucherTotal.getTotalDiscount()));
        updateTotal();
        if (mVoucherTotal.isValid()) {
            disableEnableControlsEVoucher(layoutDisable, false);
        }
        if (!TextUtils.isEmpty(mVoucherTotal.getReferralCodeDesc())) {
            tvRpDescription.setVisibility(View.VISIBLE);
            tvRpDescription.setText(mVoucherTotal.getReferralCodeDesc());
        }
    }

    public void disableEnableControlsEVoucher(ViewGroup vg, boolean isEnable) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(isEnable);
            if (isEnable) {
                child.setAlpha(1F);
            } else {
                child.setAlpha(0.5F);
            }
            if (child instanceof ViewGroup) {
                disableEnableControlsEVoucher((ViewGroup) child, isEnable);
            }
        }
        //disable button áp dụng voucher
        btnApplyVoucher.setEnabled(isEnable);
        if (isEnable) {
            btnApplyVoucher.setAlpha(1F);
        } else {
            btnApplyVoucher.setAlpha(0.5F);
        }
    }

    // set tổng tiền fpt box
    public void loadFptBoxPrice(int priceOtt) {
        lblOttTotal.setText(Common.formatNumber(priceOtt));
        updateTotal();
    }

    public void loadDevicePrice(double priceDevice) {
        lblDeviceTotal.setText(Common.formatNumber(priceDevice));
        updateTotal();
    }

    public void loadInternetPrice(InternetTotal mInternetTotal) {
        lblInternetTotal.setText(Common.formatNumber(mInternetTotal.getTotal()));
        this.mInternetTotal = mInternetTotal;
        updateTotal();
    }

    public void loadIpTvPrice(IpTvTotal mIpTvTotal) {
//        lblIPTVTotal.setText(Common.formatNumber(mIpTvTotal.getTotal()));
        this.mIpTvTotal = mIpTvTotal;
        updateTotal();
    }

    public void loadCameraPrice(Camera2Total camera2Total) {
        tvCamera.setText(Common.formatNumber(camera2Total.getTotal()));
        ((RegisterActivityNew) getActivity()).step2.modelDetail.getObjectCameraOfNet().setCameraTotal(camera2Total.getTotal());
        this.mCamera2Total = camera2Total;
        updateTotal();
    }

    public void loadMaxyPrice(MaxyTotal maxyTotal) {
        lblMaxyTVTotal.setText(Common.formatNumber(maxyTotal.getTotal()));
        ((RegisterActivityNew) getActivity()).step2.modelDetail.getObjectMaxy().getMaxyGeneral().setTotal(maxyTotal.getTotal());
        this.mMaxyTotal = maxyTotal;
        updateTotal();
    }
}