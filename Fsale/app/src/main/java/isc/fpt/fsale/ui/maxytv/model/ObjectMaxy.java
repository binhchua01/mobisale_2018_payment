package isc.fpt.fsale.ui.maxytv.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ObjectMaxy implements Parcelable {
    private MaxyGeneral MaxyGeneral;
    private List<MaxyBox> MaxyBox;
    private List<MaxyExtra> MaxyExtra;


    public ObjectMaxy(isc.fpt.fsale.ui.maxytv.model.MaxyGeneral maxyGeneral, List<isc.fpt.fsale.ui.maxytv.model.MaxyBox> maxyBox, List<isc.fpt.fsale.ui.maxytv.model.MaxyExtra> maxyExtra) {
        MaxyGeneral = maxyGeneral;
        MaxyBox = maxyBox;
        MaxyExtra = maxyExtra;
    }

    public ObjectMaxy() {
    }

    public isc.fpt.fsale.ui.maxytv.model.MaxyGeneral getMaxyGeneral() {
        return MaxyGeneral;
    }

    public void setMaxyGeneral(isc.fpt.fsale.ui.maxytv.model.MaxyGeneral maxyGeneral) {
        MaxyGeneral = maxyGeneral;
    }

    public List<isc.fpt.fsale.ui.maxytv.model.MaxyBox> getMaxyBox() {
        return MaxyBox;
    }

    public void setMaxyBox(List<isc.fpt.fsale.ui.maxytv.model.MaxyBox> maxyBox) {
        MaxyBox = maxyBox;
    }

    public List<isc.fpt.fsale.ui.maxytv.model.MaxyExtra> getMaxyExtra() {
        return MaxyExtra;
    }

    public void setMaxyExtra(List<isc.fpt.fsale.ui.maxytv.model.MaxyExtra> maxyExtra) {
        MaxyExtra = maxyExtra;
    }

    public static Creator<ObjectMaxy> getCREATOR() {
        return CREATOR;
    }

    protected ObjectMaxy(Parcel in) {
        MaxyGeneral = in.readParcelable(isc.fpt.fsale.ui.maxytv.model.MaxyGeneral.class.getClassLoader());
        MaxyBox = in.createTypedArrayList(isc.fpt.fsale.ui.maxytv.model.MaxyBox.CREATOR);
        MaxyExtra = in.createTypedArrayList(isc.fpt.fsale.ui.maxytv.model.MaxyExtra.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(MaxyGeneral, flags);
        dest.writeTypedList(MaxyBox);
        dest.writeTypedList(MaxyExtra);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObjectMaxy> CREATOR = new Creator<ObjectMaxy>() {
        @Override
        public ObjectMaxy createFromParcel(Parcel in) {
            return new ObjectMaxy(in);
        }

        @Override
        public ObjectMaxy[] newArray(int size) {
            return new ObjectMaxy[size];
        }
    };

    public JSONObject toJsonObjectUpdate() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArrayDevice = new JSONArray();
        for (MaxyBox item : getMaxyBox()) {
            jsonArrayDevice.put(item.toJsonObjectMaxyBox());
        }
        JSONArray jsonArrayExtra = new JSONArray();
        for (MaxyExtra item : getMaxyExtra()) {
            jsonArrayExtra.put(item.toJsonObjectMaxyExtra());
        }
        try {
            jsonObject.put("MaxyGeneral", getMaxyGeneral() == null ?
                    new JSONObject() : getMaxyGeneral().toJsonObjectMaxyGeneral());
            jsonObject.put("MaxyBox", jsonArrayDevice);
            jsonObject.put("MaxyExtra", jsonArrayExtra);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
