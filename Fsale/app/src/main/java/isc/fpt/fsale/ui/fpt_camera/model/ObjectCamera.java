package isc.fpt.fsale.ui.fpt_camera.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ObjectCamera implements Parcelable {
    private List<CameraDetail> CameraDetail;
    private List<CloudDetail> CloudDetail;
    private SetupDetail SetupDetail;
    private PromotionDetail PromotionDetail;
    private String UserName;
    private List<CameraServiceItem> mListService;

    public ObjectCamera() {
    }

    public ObjectCamera(List<CameraDetail> CameraDetail, List<CloudDetail> mListCloud,
                        SetupDetail mSetupDetail, PromotionDetail mPromoDetail) {
        this.CameraDetail = CameraDetail;
        this.CloudDetail = mListCloud;
        this.SetupDetail = mSetupDetail;
        this.PromotionDetail = mPromoDetail;
    }

    public ObjectCamera(String UserName, List<CameraServiceItem> mListService){
        this.UserName = UserName;
        this.mListService = mListService;
    }

    protected ObjectCamera(Parcel in) {
        CameraDetail = in.createTypedArrayList(isc.fpt.fsale.ui.fpt_camera.model.CameraDetail.CREATOR);
        CloudDetail = in.createTypedArrayList(isc.fpt.fsale.ui.fpt_camera.model.CloudDetail.CREATOR);
        SetupDetail = in.readParcelable(SetupDetail.class.getClassLoader());
        PromotionDetail = in.readParcelable(PromotionDetail.class.getClassLoader());
        UserName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(CameraDetail);
        dest.writeTypedList(CloudDetail);
        dest.writeParcelable(SetupDetail, flags);
        dest.writeParcelable(PromotionDetail, flags);
        dest.writeString(UserName);
    }

    public static final Creator<ObjectCamera> CREATOR = new Creator<ObjectCamera>() {
        @Override
        public ObjectCamera createFromParcel(Parcel in) {
            return new ObjectCamera(in);
        }

        @Override
        public ObjectCamera[] newArray(int size) {
            return new ObjectCamera[size];
        }
    };

    public List<CameraDetail> getCameraDetail() {
        return CameraDetail;
    }

    public void setCameraDetail(List<CameraDetail> cameraDetail) {
        CameraDetail = cameraDetail;
    }

    public List<CloudDetail> getCloudDetail() {
        return CloudDetail;
    }

    public void setCloudDetail(List<CloudDetail> cloudDetail) {
        CloudDetail = cloudDetail;
    }

    public SetupDetail getSetupDetail() {
        return SetupDetail;
    }

    public void setSetupDetail(SetupDetail setupDetail) {
        SetupDetail = setupDetail;
    }

    public PromotionDetail getPromotionDetail() {
        return PromotionDetail;
    }

    public void setPromotionDetail(PromotionDetail promotionDetail) {
        PromotionDetail = promotionDetail;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public List<CameraServiceItem> getmListService() {
        return mListService;
    }

    public void setmListService(List<CameraServiceItem> mListService) {
        this.mListService = mListService;
    }

    @Override
    public int describeContents() {
        return 0;
    }



    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArrayCamera = new JSONArray();
        for (CameraDetail item : getCameraDetail()) {
            jsonArrayCamera.put(item.toJsonObjectCamera());
        }
        JSONArray jsonArrayCloud = new JSONArray();
        for (CloudDetail item : getCloudDetail()) {
            jsonArrayCloud.put(item.toJsonObjectCloud());
        }
        try {
            jsonObject.put("CameraDetail", jsonArrayCamera);
            jsonObject.put("CloudDetail", jsonArrayCloud);
            jsonObject.put("SetupDetail", getSetupDetail() == null ?
                    new JSONObject() : getSetupDetail().toJsonObject());
            jsonObject.put("PromotionDetail", getPromotionDetail().toJsonObject());
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject toJSONObjectTotalPrice(){
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("UserName", getUserName());
            jsonObj.put("cameraServices", toJsonArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    private JSONArray toJsonArray(){
        JSONArray jsonArray = new JSONArray();
        for(CameraServiceItem item : mListService){
            jsonArray.put(item.toJSONObject());
        }
        return jsonArray;
    }
}