package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import static io.fabric.sdk.android.services.network.HttpRequest.trace;

public class CloudPackageSelected implements Parcelable {
    private int TypeID;
    private String TypeName;
    private int Cost;
    private int PackID;
    private String PackName;
    private int Qty;
    private int ServiceType;

    public CloudPackageSelected() {
    }

    public int getTypeID() {
        return TypeID;
    }

    public void setTypeID(int typeID) {
        TypeID = typeID;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }

    public int getCost() {
        return Cost;
    }

    public void setCost(int cost) {
        Cost = cost;
    }

    public int getPackID() {
        return PackID;
    }

    public void setPackID(int packID) {
        PackID = packID;
    }

    public String getPackName() {
        return PackName;
    }

    public void setPackName(String packName) {
        PackName = packName;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int serviceType) {
        ServiceType = serviceType;
    }

    protected CloudPackageSelected(Parcel in) {
        TypeID = in.readInt();
        TypeName = in.readString();
        Cost = in.readInt();
        PackID = in.readInt();
        PackName = in.readString();
        Qty = in.readInt();
        ServiceType = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(TypeID);
        dest.writeString(TypeName);
        dest.writeInt(Cost);
        dest.writeInt(PackID);
        dest.writeString(PackName);
        dest.writeInt(Qty);
        dest.writeInt(ServiceType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CloudPackageSelected> CREATOR = new Creator<CloudPackageSelected>() {
        @Override
        public CloudPackageSelected createFromParcel(Parcel in) {
            return new CloudPackageSelected(in);
        }

        @Override
        public CloudPackageSelected[] newArray(int size) {
            return new CloudPackageSelected[size];
        }
    };

    public JSONObject getJsonObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("TypeID", getTypeID());
            obj.put("TypeName", getTypeName());
            obj.put("PackID", getPackID());
            obj.put("PackName", getPackName());
            obj.put("Cost", getCost());
            obj.put("Qty", getQty());
            obj.put("ServiceType", getServiceType());
        } catch (JSONException e) {
            trace("DefaultListItem.toString JSONException: " + e.getMessage());
        }
        return obj;
    }
}
