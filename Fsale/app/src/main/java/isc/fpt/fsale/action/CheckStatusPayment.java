package isc.fpt.fsale.action;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ConfirmPaymentActivity;
import isc.fpt.fsale.activity.ConfirmPaymentRetailAdditionalActivity;
import isc.fpt.fsale.model.CheckStatusPaymentPost;
import isc.fpt.fsale.model.CheckStatusPaymentResult;
import isc.fpt.fsale.model.PaymentInformationResult;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by HCM.TUANTT14 on 5/2/2018.
 * API kiểm tra trạng thái thanh toán pdk
 */

public class CheckStatusPayment implements AsyncTaskCompleteListener<String> {
    private String[] mArrParamName, mArrParamValue;
    private Context mContext;
    private final String CHECK_STATUS_PAYMENT = "CheckStatusPayment";
    private String mAutoUpdatePayment = "1";
    private PaymentInformationResult mPayInfoPublic = null;
    private String mPaidTypeValue;
    private RegistrationDetailModel mRegistrationDetail;

    public CheckStatusPayment(Context mContext, CheckStatusPaymentPost mCheckStatusPaymentPost,
                              PaymentInformationResult mPayObject, String mPaymentType) {
        this.mContext = mContext;
        this.mArrParamName = new String[]{"SaleName", "RegCode", "Contract", "AutoUpdatePayment"};
        this.mArrParamValue = new String[]{mCheckStatusPaymentPost.getSaleMan(),
                mCheckStatusPaymentPost.getRegCode(), mCheckStatusPaymentPost.getContract(), mAutoUpdatePayment};
        this.mPayInfoPublic = mPayObject;
        this.mPaidTypeValue = mPaymentType;
        String message = this.mContext.getResources().getString(R.string.msg_pd_check_status_payment);
        CallServiceTask service = new CallServiceTask(this.mContext, CHECK_STATUS_PAYMENT, mArrParamName,
                mArrParamValue, Services.JSON_POST, message, CheckStatusPayment.this);
        service.execute();
    }

    public CheckStatusPayment(Context context, RegistrationDetailModel mRegistrationDetail, String mPaymentType) {
        this.mContext = context;
        this.mRegistrationDetail = mRegistrationDetail;
        this.mArrParamName = new String[]{"SaleName", "RegCode", "Contract", "AutoUpdatePayment"};
        this.mArrParamValue = new String[]{mRegistrationDetail.getUserName(), mRegistrationDetail.getRegCode(),
                mRegistrationDetail.getContract(), mAutoUpdatePayment};
        this.mPaidTypeValue = mPaymentType;
        String message = mContext.getResources().getString(R.string.msg_pd_check_status_payment);
        CallServiceTask service = new CallServiceTask(mContext, CHECK_STATUS_PAYMENT, mArrParamName,
                mArrParamValue, Services.JSON_POST, message, CheckStatusPayment.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<CheckStatusPaymentResult> objList;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject("CheckStatusPaymentResult");
                WSObjectsModel<CheckStatusPaymentResult> resultObject = new WSObjectsModel<>(jsObj, CheckStatusPaymentResult.class);
                if (resultObject.getErrorCode() == 0) {
                    objList = resultObject.getArrayListObject();
                    if (objList.size() > 0) {
                        if (objList.get(0).getPaymentStatus() == 0) { //==0: Successful payment
                            if (Common.checkLifeActivity(mContext)) {
                                new AlertDialog.Builder(mContext)
                                        .setTitle("Thông Báo")
                                        .setMessage(objList.get(0).getPaymentMessage())
                                        .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (mPayInfoPublic != null) {
                                                    Intent confirmIntent = new Intent(mContext, ConfirmPaymentActivity.class);
                                                    confirmIntent.putExtra("PAID_TYPE_VALUE", mPaidTypeValue);
                                                    confirmIntent.putExtra("CONFIRM_PAYMENT_INFO", mPayInfoPublic);
                                                    mContext.startActivity(confirmIntent);
                                                } else if (mRegistrationDetail != null) {
                                                    Intent confirmIntent = new Intent(mContext, ConfirmPaymentRetailAdditionalActivity.class);
                                                    confirmIntent.putExtra("PAID_TYPE_VALUE", mPaidTypeValue);
                                                    confirmIntent.putExtra(Constants.MODEL_REGISTER, mRegistrationDetail);
                                                }
                                                dialog.cancel();
                                                ((Activity) mContext).finish();
                                            }
                                        })
                                        .create()
                                        .show();
                            }
                        } else {
                            Common.alertDialog(objList.get(0).getPaymentMessage(), mContext);
                        }
                    }
                } else { //!=0: not successful payment
                    Common.alertDialog(resultObject.getError(), mContext);
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

}