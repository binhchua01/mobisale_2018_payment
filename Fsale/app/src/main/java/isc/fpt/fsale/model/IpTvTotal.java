package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by haulc3 on 05,November,2019
 */
public class IpTvTotal implements Parcelable {
    private String Message;
    private int ResultID;
    private List<ServiceCode> ServiceCodes;
    private int Total;
    private int PrepaidTotal;
    private int DeviceTotal;

    public IpTvTotal() {
    }

    private IpTvTotal(Parcel in) {
        Message = in.readString();
        ResultID = in.readInt();
        ServiceCodes = in.createTypedArrayList(ServiceCode.CREATOR);
        Total = in.readInt();
        PrepaidTotal = in.readInt();
        DeviceTotal = in.readInt();
    }

    public static final Creator<IpTvTotal> CREATOR = new Creator<IpTvTotal>() {
        @Override
        public IpTvTotal createFromParcel(Parcel in) {
            return new IpTvTotal(in);
        }

        @Override
        public IpTvTotal[] newArray(int size) {
            return new IpTvTotal[size];
        }
    };

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getResultID() {
        return ResultID;
    }

    public void setResultID(int resultID) {
        ResultID = resultID;
    }

    public List<ServiceCode> getServiceCodes() {
        return ServiceCodes;
    }

    public void setServiceCodes(List<ServiceCode> serviceCodes) {
        ServiceCodes = serviceCodes;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

    public int getPrepaidTotal() {
        return PrepaidTotal;
    }

    public void setPrepaidTotal(int prepaidTotal) {
        PrepaidTotal = prepaidTotal;
    }

    public int getDeviceTotal() {
        return DeviceTotal;
    }

    public void setDeviceTotal(int deviceTotal) {
        DeviceTotal = deviceTotal;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Message);
        parcel.writeInt(ResultID);
        parcel.writeTypedList(ServiceCodes);
        parcel.writeInt(Total);
        parcel.writeInt(PrepaidTotal);
        parcel.writeInt(DeviceTotal);
    }
}
