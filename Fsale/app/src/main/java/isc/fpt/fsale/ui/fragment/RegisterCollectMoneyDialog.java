package isc.fpt.fsale.ui.fragment;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import com.danh32.fontify.Button;
import com.danh32.fontify.TextView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import isc.fpt.fsale.action.UpdateDeposit;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.SBIModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;
import isc.fpt.fsale.utils.MyApp.TrackerName;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;


public class RegisterCollectMoneyDialog extends DialogFragment{
	
	private Context mContext;
	private ImageButton imgClose;
	private Button btnUpdate; 
	private Spinner spSBIAdsl, spSBIFTTH, spSBIiptv;
	private TextView txtTotal, txtRegCode, txtFullName, txtPhoneNumber;
	private RegistrationDetailModel mRegisterModel;	
	private ArrayList<SBIModel> mlstSBI;
	private LinearLayout frmInternet, frmIPTV;

	public RegisterCollectMoneyDialog(){ }

	@SuppressLint("ValidFragment")
	public RegisterCollectMoneyDialog(Context context, RegistrationDetailModel model, ArrayList<SBIModel> lstSBI) {
		mRegisterModel = model;		
		this.mContext= context;		
		mlstSBI = lstSBI;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		// Inflate the layout to use as dialog or embedded fragment
		View view = inflater.inflate(R.layout.dialog_register_collect_money, container);
		//Dong SBI IPTV khi khong co dk IPTV
		frmIPTV = (LinearLayout)view.findViewById(R.id.frm_collect_monney_iptv);
		frmInternet =(LinearLayout)view.findViewById(R.id.frm_internet);
		
		imgClose = (ImageButton)view.findViewById(R.id.btn_close);
		spSBIAdsl = (Spinner)view.findViewById(R.id.sp_register_sbi_adsl);
		spSBIFTTH = (Spinner)view.findViewById(R.id.sp_register_sbi_ftth);
		//Dong FTTH chi su dung ADSL
		spSBIFTTH.setVisibility(View.GONE);
		spSBIiptv = (Spinner)view.findViewById(R.id.sp_register_sbi_iptv);
		txtTotal = (TextView)view.findViewById(R.id.txt_register_total);
		txtRegCode = (TextView)view.findViewById(R.id.txt_reg_code);
		
		txtFullName = (TextView)view.findViewById(R.id.txt_full_name);
		txtPhoneNumber = (TextView)view.findViewById(R.id.txt_phone_number);
		btnUpdate = (Button)view.findViewById(R.id.btn_register_upate);
		btnUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(checkForUpdate()){
					String Message = "Hệ thống sẽ gửi tin nhắn đến số "+ txtPhoneNumber.getText().toString() +". Bạn có muốn cập nhật?";			
					AlertDialog.Builder builder = null;
					Dialog dialog = null;
					builder = new AlertDialog.Builder(mContext);
					builder.setTitle("Xác nhận cập nhật").setMessage(Message).setCancelable(false).setPositiveButton("Có",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											updateData();
										}
									}).setNegativeButton("Không",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,
												int id) {
											dialog.cancel();												
										}
									});
					dialog = builder.create();
					dialog.show();
				}
			}
		});
		imgClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dismiss();				
			}
		});
		//Load thong tin phiếu đăng ký
		if(mRegisterModel!= null){
			txtRegCode.setText(mRegisterModel.getRegCode());
			txtFullName.setText(mRegisterModel.getFullName());
			int total = mRegisterModel.getTotal();
			txtTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(total));
			txtPhoneNumber.setText(mRegisterModel.getPhone_1());
			if(mRegisterModel.getIPTVPackage().trim().equals(""))
				frmIPTV.setVisibility(View.GONE);
			if(mRegisterModel.getPromotionID() <= 0)
				frmInternet.setVisibility(View.GONE);
				
		}
		
		loadData(mlstSBI);
		
		return view;
	}
	
	/** The system calls this only when creating the layout in a dialog. */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {	   
	    Dialog dialog = super.onCreateDialog(savedInstanceState);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    Common.reportActivityCreate(getActivity().getApplication(), getString(R.string.lbl_screen_name_dialog_collect_money));
	    return dialog;
	}
	@Override
	public void onStart() {
		
		super.onStart();
	}
	
		
	@SuppressWarnings("null")
	public void loadData(ArrayList<SBIModel> list)
	{		
		if(mRegisterModel != null){
			ArrayList<KeyValuePairModel> lstSBIADSL,lstSBIIPTV;
			
			lstSBIIPTV = new ArrayList<KeyValuePairModel>();
			lstSBIADSL= new ArrayList<KeyValuePairModel>();	
			
			lstSBIADSL.add(new KeyValuePairModel("0", "--Chọn SBI--"));
			lstSBIIPTV.add(new KeyValuePairModel("0", "--Chọn SBI--"));
			
			if(list != null || list.size() > 0){
				for(int i=0;i<list.size();i++){
					SBIModel item = list.get(i);	
					//Co dk internet moi them sbi
					if(mRegisterModel.getPromotionID() != 0)					
						if(item.getType() == 1 || item.getType() == 2)//SBI ADSL hoac FTTH
							lstSBIADSL.add(new KeyValuePairModel(list.get(i).getSBI(), list.get(i).getSBI()));		
					//Khong check SBI IPTV
					if(!mRegisterModel.getIPTVPackage().trim().equals(""))
						if(item.getType() == 8)//SBI IPTV
							lstSBIIPTV.add(new KeyValuePairModel(list.get(i).getSBI(), list.get(i).getSBI()));	
				
				}
				/*if(lstSBIADSL.size() == 0)
					lstSBIADSL.add(new KeyValuePairModel("0", "[Không có số SBI]"));
				
				if(lstSBIIPTV.size() == 0)
					lstSBIIPTV.add(new KeyValuePairModel("0", "[Không có số SBI]"));
				*/
				spSBIAdsl.setAdapter(new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstSBIADSL, Gravity.LEFT));
				spSBIiptv.setAdapter(new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstSBIIPTV, Gravity.LEFT));
			}
		}
	}
	 
	private boolean checkForUpdate(){		
		String sbiInternet = ((KeyValuePairModel)spSBIAdsl.getSelectedItem()).getsID();
		//String sbiIPTV = ((KeyValuePairModel)spSBIiptv.getSelectedItem()).getsID();
		/**/
		//Kiem tra SBI Internet(neu co dk Internet)
		/*if(mRegisterModel.getPromotionID() != 0){
			if(sbiADSL.equals("0")){
				Common.alertDialog("Chưa chọn số SBI cho Internet", mContext);
				return false;
			}			
		}*/
		//Chưa chon SBi nào
		if(mRegisterModel.getPromotionID() > 0){
			if(sbiInternet.equals("0") || sbiInternet.equals("")){
				Common.alertDialog("Chưa chọn số SBI Internet", mContext);
				return false;
				//Dang ky Internet Only nhung ko co SBi
			}
		}else{			
			if(mRegisterModel.getTotal() <= 0){
				Common.alertDialog("Tổng tiền bằng 0! Vui lòng cập nhật lại TTKH!", mContext);
				return false;
				//Dang ky Internet Only nhung ko co SBi
			}
			
			//Co dang ky IPTV nhung khong co SBI
			/*if(!mRegisterModel.getIPTVPackage().trim().equals("")){
				
				if(sbiIPTV.equals("0")){
					Common.alertDialog("Chưa chọn số SBI cho IPTV", mContext);
					return false;
				}				
			}*/
			//Co dang ky Internet nhung ko co SBI
			/*if(mRegisterModel.getPromotionID() > 0){
				if(sbiInternet.equals("0")){
					Common.alertDialog("Chưa chọn số SBI cho Internet", mContext);
					return false;
					//Dang ky Internet Only nhung ko co SBi
				}
			}*/
		}
		//Kiem tra tinh trang PDK
		/*if(mRegisterModel.getStatusDeposit() > 0){
			Common.alertDialog("Phiếu đăng ký đã xuất phiếu thu!", mContext);
			return false;
		}*/
		
		
		return true;
	}
	
	private void updateData(){
		String sbiADSL = ((KeyValuePairModel)spSBIAdsl.getSelectedItem()).getsID();
		String sbiIPTV = ((KeyValuePairModel)spSBIiptv.getSelectedItem()).getsID();
		String sbiInternet = sbiADSL;	
		onClickTracker("Thu tiền PĐK");
		new UpdateDeposit(mContext, RegisterCollectMoneyDialog.this, mRegisterModel.getRegCode(), sbiInternet, sbiIPTV, mRegisterModel.getTotal(), mRegisterModel.getID());
				
	}
	
	 private void onClickTracker(String lable){
		 try {
			// Get tracker.
			 Tracker t = ((MyApp) getActivity().getApplication()).getTracker(
			     TrackerName.APP_TRACKER);
			 // Set screen name.
			 t.setScreenName(getString(R.string.lbl_screen_name_dialog_collect_money)+".onClick()");
			 // Send a screen view.
			 t.send(new HitBuilders.ScreenViewBuilder().build());

			 // This event will also be sent with the most recently set screen name.
			 // Build and send an Event.
			 t.send(new HitBuilders.EventBuilder()
			     .setCategory("DAT_COC_PHIEU_DANG_KY")
			     .setAction("onClick")
			     .setLabel(lable)
			     .build());

			 // Clear the screen name field when we're done.
			 t.setScreenName(null);
		} catch (Exception e) {
		 	e.printStackTrace();
		}
		
	 }
}
