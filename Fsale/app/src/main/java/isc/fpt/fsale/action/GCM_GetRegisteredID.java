package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.SupportDialog;
import isc.fpt.fsale.model.GCMUserModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;

/*
 * ACTION: 	 	 CheckVersion
 * @description: - call service and check if have new version
 * 				 - handle response result: if having new version, show new version download link
 * @author:	 	vandn, on 13/08/2013
 * */

public class GCM_GetRegisteredID implements AsyncTaskCompleteListener<String> {
    private SupportDialog mDialogSupport;

    public GCM_GetRegisteredID(Context mContext, SupportDialog dialog, int Agent, String AgentName) {
        mDialogSupport = dialog;
        String[] paramName = new String[]{"UserName", "Agent", "AgentName"};
        String[] paramValue = new String[]{Constants.USERNAME, String.valueOf(Agent), AgentName};
        String message = mContext.getResources().getString(R.string.msg_get_reg_id);
        String URL = "GCM_GetRegisteredID";
        CallServiceTask service = new CallServiceTask(mContext, URL, paramName, paramValue,
                Services.JSON_POST, message, GCM_GetRegisteredID.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            List<GCMUserModel> lst;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<GCMUserModel> resultObject = new WSObjectsModel<>(jsObj, GCMUserModel.class);
                lst = resultObject.getListObject();
                loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(List<GCMUserModel> lst) {
        if (mDialogSupport != null) {
            try {
                mDialogSupport.loadData(lst);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
