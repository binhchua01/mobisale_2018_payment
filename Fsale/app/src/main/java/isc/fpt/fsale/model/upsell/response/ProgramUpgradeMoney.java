package isc.fpt.fsale.model.upsell.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProgramUpgradeMoney implements Parcelable {

    @SerializedName("Enable")
    @Expose
    private Integer enable;
    @SerializedName("ErrorService")
    @Expose
    private Object errorService;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("MoneyUpgrade")
    @Expose
    private String moneyUpgrade;
    @SerializedName("ProgramUpgradeID")
    @Expose
    private Integer programUpgradeID;

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Object getErrorService() {
        return errorService;
    }

    public void setErrorService(Object errorService) {
        this.errorService = errorService;
    }

    public Integer getiD() {
        return iD;
    }

    public void setiD(Integer iD) {
        this.iD = iD;
    }

    public String getMoneyUpgrade() {
        return moneyUpgrade;
    }

    public void setMoneyUpgrade(String moneyUpgrade) {
        this.moneyUpgrade = moneyUpgrade;
    }

    public Integer getProgramUpgradeID() {
        return programUpgradeID;
    }

    public void setProgramUpgradeID(Integer programUpgradeID) {
        this.programUpgradeID = programUpgradeID;
    }

    protected ProgramUpgradeMoney(Parcel in) {
        if (in.readByte() == 0) {
            enable = null;
        } else {
            enable = in.readInt();
        }
        if (in.readByte() == 0) {
            iD = null;
        } else {
            iD = in.readInt();
        }
        moneyUpgrade = in.readString();
        if (in.readByte() == 0) {
            programUpgradeID = null;
        } else {
            programUpgradeID = in.readInt();
        }
    }

    public static final Creator<ProgramUpgradeMoney> CREATOR = new Creator<ProgramUpgradeMoney>() {
        @Override
        public ProgramUpgradeMoney createFromParcel(Parcel in) {
            return new ProgramUpgradeMoney(in);
        }

        @Override
        public ProgramUpgradeMoney[] newArray(int size) {
            return new ProgramUpgradeMoney[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (enable == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(enable);
        }
        if (iD == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(iD);
        }
        dest.writeString(moneyUpgrade);
        if (programUpgradeID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(programUpgradeID);
        }
    }
}
