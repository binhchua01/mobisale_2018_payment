package isc.fpt.fsale.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.Notification;

/**
 * Created by HCM.TUANTT14 on 3/15/2018.
 * <p>
 * update by haulc3
 */

public class NotificationAdapter extends BaseAdapter {
    private List<Notification> mList;
    private Context mContext;

    public NotificationAdapter(Context context, List<Notification> lst) {
        this.mContext = context;
        this.mList = lst;
    }

    @Override
    public int getCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        if (mList != null)
            return mList.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Notification notification = mList.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_row_notification, null);
        }
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView1);
        if (notification.getTypeName().equals("KHTN")) {
            imageView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_customer_support));
        } else if (notification.getTypeName().equals("PTC_Return")) {
            imageView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_support_develop));
        } else {
            imageView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_info_welcome_dark));
        }
        TextView tvTitleNotification = (TextView) convertView.findViewById(R.id.tv_title_notification);
        tvTitleNotification.setText(notification.getTitle());
        TextView tvContentNotification = (TextView) convertView.findViewById(R.id.tv_content_notification);
        tvContentNotification.setText(notification.getContent());
        TextView tvSendDateNotification = (TextView) convertView.findViewById(R.id.tv_send_date_notification);
        tvSendDateNotification.setText(notification.getSendDate());
        return convertView;
    }
}
