package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListPrechecklistActivity;

import isc.fpt.fsale.model.ListPrechecklistModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;

public class GetListPrechecklist implements AsyncTaskCompleteListener<String> {
    private ArrayList<ListPrechecklistModel> lstPrechecklist;
    private Context mContext;

    public GetListPrechecklist(Context mContext, String[] arrParams) {
        this.mContext = mContext;
        String[] params = new String[]{"UserName", "PageNumber"};
        String message = mContext.getResources().getString(
                R.string.msg_pd_get_list_prechecklist);
        String GET_LIST_PRECHECKLIST = "GetPrecheckList";
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_PRECHECKLIST, params, arrParams,
                Services.JSON_POST, message, GetListPrechecklist.this);
        service.execute();
    }

    private void handleGetListPrechecklist(String json) {
        lstPrechecklist = new ArrayList<>();
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if (!Common.isEmptyJSONObject(jsObj))
                bindData(jsObj);
        } else {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
        }
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {
            String TAG_LIST_PRECHECKLIST_RESULT = "GetPrecheckListMethodPostResult";
            jsArr = jsObj.getJSONArray(TAG_LIST_PRECHECKLIST_RESULT);
            int l = jsArr.length();
            if (l > 0) {
                String TAG_ERROR = "ErrorService";
                String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                if (error.equals("null")) {
                    for (int i = 0; i < l; i++) {
                        JSONObject iObj = jsArr.getJSONObject(i);

                        String _strAddress = "", _strFullName = "", _strRowNumber = "", _strTotalPage = "",
                                _strCurrentPage = "", _strTotalRow = "";

                        // Add by GiauTQ 30-04-2014
                        String TAG_FULL_NAME = "FullName";
                        if (iObj.has(TAG_FULL_NAME))
                            _strFullName = iObj.getString(TAG_FULL_NAME);

                        String TAG_ADDRESS = "Address";
                        if (iObj.has(TAG_ADDRESS))
                            _strAddress = iObj.getString(TAG_ADDRESS);

                        // add by GiauTQ 01-05-2014
                        String TAG_ROW_NUMBER = "RowNumber";
                        if (iObj.has(TAG_ROW_NUMBER))
                            _strRowNumber = iObj.getString(TAG_ROW_NUMBER);

                        String TAG_TOTAL_PAGE = "TotalPage";
                        if (iObj.has(TAG_TOTAL_PAGE))
                            _strTotalPage = iObj.getString(TAG_TOTAL_PAGE);

                        String TAG_CURRENT_PAGE = "CurrentPage";
                        if (iObj.has(TAG_CURRENT_PAGE))
                            _strCurrentPage = iObj.getString(TAG_CURRENT_PAGE);

                        String TAG_TOTAL_ROW = "TotalRow";
                        if (iObj.has(TAG_TOTAL_ROW))
                            _strTotalRow = iObj.getString(TAG_TOTAL_ROW);

                        String TAG_CONTRACT = "Contract";
                        String TAG_CREATE_DATE = "CreateDate";
                        String TAG_DESCRIPTION = "Description";
                        String TAG_SUB_DESCRIPTION = "SupDescription";
                        String TAG_UPDATE_BY = "UpdateBy";
                        String TAG_UPDATE_DATE = "UpdateDate";
                        lstPrechecklist.add(new ListPrechecklistModel(iObj.getString(TAG_CONTRACT), iObj.getString(TAG_CREATE_DATE),
                                iObj.getString(TAG_DESCRIPTION), iObj.getString(TAG_SUB_DESCRIPTION), iObj.getString(TAG_UPDATE_BY),
                                iObj.getString(TAG_UPDATE_DATE), _strFullName, _strAddress, _strRowNumber, _strTotalPage, _strCurrentPage, _strTotalRow));
                    }
                    showPreCheckList(lstPrechecklist);
                } else {
                    Common.alertDialog("Lỗi WS:" + error, mContext);
                }
            } else {
                Common.alertDialog("Không tìm thấy dữ liệu", mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showPreCheckList(ArrayList<ListPrechecklistModel> list) {
        Intent intent = new Intent(mContext, ListPrechecklistActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("list_prechecklist", list);
        mContext.startActivity(intent);
    }

    @Override
    public void onTaskComplete(String result) {
        handleGetListPrechecklist(result);
    }
}
