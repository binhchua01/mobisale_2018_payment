package isc.fpt.fsale.action;

import isc.fpt.fsale.ui.fragment.FragmentRegisterContractInternet;
import isc.fpt.fsale.model.PromotionComboModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;

public class GetPromotionCombo implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private FragmentRegisterContractInternet mFragment;


    public GetPromotionCombo(Context context, FragmentRegisterContractInternet fragment,
                             int LocalType, int PromotionID, String Contract) {
       this. mContext = context;
        this.mFragment = fragment;
        String[] paramValues = new String[]{
                Constants.USERNAME,
                String.valueOf(LocalType),
                String.valueOf(PromotionID),
                Contract
        };
        String message = "Đang lấy dữ liệu...";
        String TAG_METHOD_NAME = "GetPromotionCombo";
        String[] paramNames = new String[]{"UserName", "LocalType", "PromotionID", "Contract"};
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, GetPromotionCombo.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            boolean isError = false;
            ArrayList<PromotionComboModel> lst = null;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<PromotionComboModel> resultObject = new WSObjectsModel<>(jsObj, PromotionComboModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getArrayListObject();
                } else {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError)
                    loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("LongLogTag")
    private void loadData(ArrayList<PromotionComboModel> lst) {
        try {
            if (mFragment != null)
                mFragment.updatePromotionCombo(lst);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
