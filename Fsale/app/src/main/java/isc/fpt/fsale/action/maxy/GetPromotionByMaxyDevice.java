package isc.fpt.fsale.action.maxy;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.ui.maxytv.ui.MaxyBox1Activity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyDeviceActivity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPromotionBox1Activity;
import isc.fpt.fsale.ui.maxytv.ui.MaxyPromotionDeviceActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//API lấy danh sách CLKM
public class GetPromotionByMaxyDevice implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private int regType; // 0-bán mới: 1-bán thêm
    private int BoxID;

    public GetPromotionByMaxyDevice(Context mContext, int boxid, int RegType, int box, int localtype, int maxyProID, int maxyProType, String contract) {
        this.mContext = mContext;
        this.regType = RegType;
        this.BoxID = boxid;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("BoxID", BoxID);
            jsonObject.put("RegType", regType);
            jsonObject.put("LocationID", Constants.LST_REGION.get(0).getsID());
            jsonObject.put("BranchCode", Constants.BRANCH_CODE);
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("BoxFirst", box);
            jsonObject.put("LocalType", localtype);
            jsonObject.put("MaxyPromotionID", maxyProID);
            jsonObject.put("MaxyPromotionType", maxyProType);
            jsonObject.put("Contract", contract);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = "Đang lấy dánh sách khuyến mãi...";
        String GET_MAXY_PROMOTION_DEVICE = "GetPromotionByMaxyDevice";
        CallServiceTask service = new CallServiceTask(mContext, GET_MAXY_PROMOTION_DEVICE, jsonObject,
                Services.JSON_POST_OBJECT, message, GetPromotionByMaxyDevice.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<MaxyBox> resultObject = new WSObjectsModel<>(jsObj, MaxyBox.class);
                List<MaxyBox> lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (mContext != null) {
                    if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                        Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                    } else {
                        if (mContext.getClass().getSimpleName().equals(MaxyPromotionDeviceActivity.class.getSimpleName())) {
                            ((MaxyPromotionDeviceActivity) mContext).mLoadPromotionDevice(lst);
                        } else {
                            ((MaxyPromotionBox1Activity) mContext).mLoadPromotionDevice(lst);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
