package isc.fpt.fsale.adapter;

import isc.fpt.fsale.activity.NotificationActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.database.MessageTable;
import isc.fpt.fsale.model.MessageModel;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MessageAdapter extends BaseAdapter {
	public List<MessageModel> mList;
	private Context mContext;
	//private int[] colors = new int[] { 0xFFCC99, 0x30f0efda };
	
	public MessageAdapter(Context context, List<MessageModel> lst){
		this.mContext = context;
		this.mList = lst;
		}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		try {
			final MessageModel mess = mList.get(position);
			//final int fposition = position;
			
			final NotificationActivity notification = (NotificationActivity) parent.getContext();
			
			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(R.layout.row_notification, null);
			}				
			
			TextView txtUser = (TextView) convertView.findViewById(R.id.txt_user);
			if(mess.getSendfrom() != null)
				txtUser.setText(mess.getSendfrom());
			
			TextView txtMessage = (TextView) convertView.findViewById(R.id.txt_notification);
			if(mess.getMessage() != null)
				txtMessage.setText(mess.getMessage());
			
			TextView txtDateTime = (TextView) convertView.findViewById(R.id.txt_notification_date_time);
			if(mess.getDatetime() != null)
				txtDateTime.setText(mess.getDatetime());	
			
			TextView txtTitle = (TextView) convertView.findViewById(R.id.txt_notification_title);
			if(mess.getTitle() != null)
				txtTitle.setText(mess.getTitle());
			
			ImageView imgBookmark = (ImageView) convertView.findViewById(R.id.img_bookmark);
			LinearLayout frmBookmark = (LinearLayout) convertView.findViewById(R.id.frm_bookmark);
			
			final CheckBox chkDelete = (CheckBox) convertView.findViewById(R.id.chk_delete);
			LinearLayout frmDelete = (LinearLayout) convertView.findViewById(R.id.frm_delete);
			
			if(notification.isDelete)
			{
				frmBookmark.setVisibility(View.GONE);
				frmDelete.setVisibility(View.VISIBLE);
			}
			else
			{
				frmBookmark.setVisibility(View.VISIBLE);
				frmDelete.setVisibility(View.GONE);
			}
			
			if(mess.getIsBookmark())
				imgBookmark.setImageResource(R.drawable.ic_bookmark);
			else
				imgBookmark.setImageResource(R.drawable.ic_unbookmark);
			
			if(mess.getIsDelete())
				chkDelete.setChecked(true);
			else
				chkDelete.setChecked(false);
			
			chkDelete.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						NotificationActivity activity = (NotificationActivity) v.getContext();
						MessageAdapter adapter = (MessageAdapter) activity.lvMessageList.getAdapter();
						List<MessageModel> messageList = adapter.mList;
						if(!messageList.get(position).getIsDelete())
						{
							messageList.get(position).setIsDelete(true);
						}
						else
						{
							messageList.get(position).setIsDelete(false);
							mess.setIsDelete(false);
						}
						
						if(activity.isCheckAll)
							activity.isCheckAll = false;
						activity.showbadge();
					} catch (Exception e) {

						e.printStackTrace();
					}
					
				}
			});
			
			
			
			frmBookmark.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						ImageView imgBookmark = (ImageView) v.findViewById(R.id.img_bookmark);
						NotificationActivity activity = (NotificationActivity) v.getContext();
						MessageAdapter adapter = (MessageAdapter) activity.lvMessageList.getAdapter();
						List<MessageModel> messageList = adapter.mList;
						//MessageModel selectedItem = messageList.get(position);
						if(!messageList.get(position).getIsBookmark())
						{
							imgBookmark.setImageResource(R.drawable.ic_bookmark);
							messageList.get(position).setIsBookmark(true);
						}
						else
						{
							imgBookmark.setImageResource(R.drawable.ic_unbookmark);
							messageList.get(position).setIsBookmark(false);
						}
						updateTable(messageList.get(position));
					} catch (Exception e) {
						// TODO: handle exception

						e.printStackTrace();
					}
					
				}
			});
			
			if(!mess.getIsRead()){
				txtMessage.setTypeface(null,Typeface.BOLD);
				txtDateTime.setTypeface(null,Typeface.BOLD);
			}
			else
			{
				txtMessage.setTypeface(null,Typeface.NORMAL);
				txtDateTime.setTypeface(null,Typeface.NORMAL);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		
		return convertView;
	}
	
	private void updateTable(MessageModel item){
		try {
			MessageTable table = new MessageTable(mContext);
			table.update(item);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
}
