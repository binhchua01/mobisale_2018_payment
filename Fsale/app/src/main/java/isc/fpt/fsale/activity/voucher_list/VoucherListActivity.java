package isc.fpt.fsale.activity.voucher_list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListEVoucher;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.Voucher;
import isc.fpt.fsale.ui.base.BaseActivitySecond;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.utils.Constants;

public class VoucherListActivity extends BaseActivitySecond implements OnItemClickListener<Voucher> {
    private View layoutBack;
    private RegistrationDetailModel mRegistrationDetailModel;
    private VoucherAdapter mAdapter;
    private List<Voucher> mList;

    @Override
    protected void onResume() {
        super.onResume();
        if(mRegistrationDetailModel != null){
            new GetListEVoucher(this, mRegistrationDetailModel);
        }
    }

    @Override
    protected void initEvent() {
        layoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_voucher_list;
    }

    @Override
    protected void initView() {
        layoutBack = findViewById(R.id.btn_back);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.voucher_list);
        mList = new ArrayList<>();
        mAdapter = new VoucherAdapter(this, mList, this);
        mRecyclerView.setAdapter(mAdapter);

        getDataIntent();
    }

    private void getDataIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(Constants.REGISTRATION_INTERNET) != null) {
            mRegistrationDetailModel = bundle.getParcelable(Constants.REGISTRATION_INTERNET);
        }
    }

    public void loadData(List<Voucher> mList){
        this.mList = mList;
        mAdapter.notifyData(this.mList);
    }

    @Override
    public void onItemClick(Voucher object) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.VOUCHER, object);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
