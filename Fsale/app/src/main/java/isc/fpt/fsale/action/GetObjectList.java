package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListObjectActivity;
import isc.fpt.fsale.activity.ListObjectSearchActivity;

import isc.fpt.fsale.model.ListObjectModel;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.FptCameraSelectComboActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

// API BÁN THÊM DỊCH VỤ
public class GetObjectList implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private final String TAG_METHOD_NAME_2 = "GetObjectList";
    private String API_NAME;

    public GetObjectList(Context mContext, String agent, String agentName, int PageNumber) {
        this.mContext = mContext;
        if (agentName == null || agentName.equals(""))
            agentName = " ";
        String params = Services.getParams(new String[]{agent, agentName, Constants.USERNAME, String.valueOf(PageNumber)});
        String message = mContext.getResources().getString(R.string.msg_pd_get_search_contact);
        String TAG_METHOD_NAME = "SearchObject";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, params,
                Services.GET, message, GetObjectList.this);
        service.execute();
    }

    public GetObjectList(Context mContext, int agent, String agentName) {
        this.mContext = mContext;
        String[] paramName = new String[]{"UserName", "Agent", "AgentName"};
        API_NAME = TAG_METHOD_NAME_2;
        String[] paramValues = new String[]{Constants.USERNAME, String.valueOf(agent), agentName};
        String message = mContext.getResources().getString(R.string.msg_pd_get_search_contact);
        CallServiceTask service = new CallServiceTask(mContext, API_NAME, paramName,
                paramValues, Services.JSON_POST, message, GetObjectList.this);
        service.execute();
    }

    //tìm HĐ bán combo fpt camera
    public GetObjectList(Context mContext, int agent, String agentName, int categoryServiceId) {
        this.mContext = mContext;
        String[] paramName = new String[]{"UserName", "Agent", "AgentName", "CategoryServiceID"};
        API_NAME = TAG_METHOD_NAME_2;
        String[] paramValues = new String[]{Constants.USERNAME, String.valueOf(agent),
                agentName, String.valueOf(categoryServiceId)};
        String message = mContext.getResources().getString(R.string.msg_pd_get_search_contact);
        CallServiceTask service = new CallServiceTask(mContext, API_NAME, paramName,
                paramValues, Services.JSON_POST, message, GetObjectList.this);
        service.execute();
    }


    @Override
    public void onTaskComplete(String result) {
        HandleResultSearch(result);
    }

    private void HandleResultSearch(String result) {
        if (API_NAME != null && API_NAME.equals(TAG_METHOD_NAME_2)) {
            try {
                List<ListObjectModel> lst = null;
                boolean isError = false;
                if (Common.jsonObjectValidate(result)) {
                    JSONObject jsObj = new JSONObject(result);
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<ListObjectModel> resultObject = new WSObjectsModel<>(jsObj, ListObjectModel.class);
                    if (resultObject.getErrorCode() == 0) {//OK not Error
                        lst = resultObject.getListObject();
                    } else {//ServiceType Error
                        isError = true;
                        Common.alertDialog(resultObject.getError(), mContext);
                    }
                    if (!isError)
                        loadDataSearchObject(lst);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            JSONObject jsObj = getJsonObject(result);
            String TAG_RESULT_LIST = "SearchObjectResult";
            if (jsObj != null && jsObj.has(TAG_RESULT_LIST)) {
                try {
                    JSONArray jsArr = jsObj.getJSONArray(TAG_RESULT_LIST);
                    ArrayList<ObjectModel> lstObject = new ArrayList<>();
                    String TAG_ERROR = "ErrorService";
                    if (jsArr.getJSONObject(0).isNull(TAG_ERROR) ||
                            jsArr.getJSONObject(0).getString(TAG_ERROR).equals("null")) {
                        for (int i = 0; i < jsArr.length(); i++) {
                            JSONObject item = jsArr.getJSONObject(i);
                            lstObject.add(ObjectModel.Parse(item));
                        }
                        loadData(lstObject);
                    } else
                        Common.alertDialog("Lỗi WS:" + jsArr.getJSONObject(0).getString(TAG_ERROR), mContext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            return JSONParsing.getJsonObj(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void loadData(ArrayList<ObjectModel> lst) {
        if (mContext != null && mContext.getClass().getSimpleName().equals(ListObjectActivity.class.getSimpleName())) {
            ListObjectActivity activity = (ListObjectActivity) mContext;
            activity.loadData(lst);
        }
    }

    private void loadDataSearchObject(List<ListObjectModel> lst) {
        if (mContext != null && mContext.getClass().getSimpleName().equals(ListObjectSearchActivity.class.getSimpleName())) {
            ListObjectSearchActivity activity = (ListObjectSearchActivity) mContext;
            activity.loadData(lst);
        }

        if (mContext != null && mContext.getClass().getSimpleName().equals(FptCameraSelectComboActivity.class.getSimpleName())) {
            FptCameraSelectComboActivity activity = (FptCameraSelectComboActivity) mContext;
            activity.loadData(lst);
        }
    }
}
