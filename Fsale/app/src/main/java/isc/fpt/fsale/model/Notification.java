package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 3/15/2018.
 *
 * update by haulc3
 */

public class Notification implements Parcelable {
    private String Title;
    private String SourceName;
    private String Content;
    private String CreateDate;
    private String SendDate;
    private String SaleName;
    private String TypeName;
    private String JSONExtra;
    private String KeysJSON;
    private int Type;

    public Notification() {

    }

    protected Notification(Parcel in) {
        Title = in.readString();
        SourceName = in.readString();
        Content = in.readString();
        CreateDate = in.readString();
        SendDate = in.readString();
        SaleName = in.readString();
        TypeName = in.readString();
        JSONExtra = in.readString();
        KeysJSON = in.readString();
        Type = in.readInt();
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSourceName() {
        return SourceName;
    }

    public void setSourceName(String sourceName) {
        SourceName = sourceName;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public String getSendDate() {
        return SendDate;
    }

    public void setSendDate(String sendDate) {
        SendDate = sendDate;
    }

    public String getSaleName() {
        return SaleName;
    }

    public void setSaleName(String saleName) {
        SaleName = saleName;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }

    public String getJSONExtra() {
        return JSONExtra;
    }

    public void setJSONExtra(String JSONExtra) {
        this.JSONExtra = JSONExtra;
    }

    public String getKeysJSON() {
        return KeysJSON;
    }

    public void setKeysJSON(String keysJSON) {
        KeysJSON = keysJSON;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Title);
        parcel.writeString(SourceName);
        parcel.writeString(Content);
        parcel.writeString(CreateDate);
        parcel.writeString(SendDate);
        parcel.writeString(SaleName);
        parcel.writeString(TypeName);
        parcel.writeString(JSONExtra);
        parcel.writeString(KeysJSON);
        parcel.writeInt(Type);
    }
}
