/**
 *
 */
package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mposlib.caller.CallMposController;
import com.mposlib.caller.ErrorHandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckForDeposit;
import isc.fpt.fsale.action.GetSBIList;
import isc.fpt.fsale.action.UpdateDeposit;
import isc.fpt.fsale.action.UpdateDepositmPOS;
import isc.fpt.fsale.action.WriteLogMPos;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.database.mPOSLogTable;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.SBIModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.mPOSLogModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class DepositActivity extends BaseActivity implements
        com.mposlib.receiver.MposListener {

    private static final String TAG_STEP_0 = "0. Gui thong tin thanh toan qua mPOS.";
    private static final String TAG_STEP_1 = "1. mPOS Thanh toán thành công.";
    private static final String TAG_STEP_1_1 = "1. mPOS Thanh toán thành công(Thanh toán lại).";

    private TextView lblPhoneNumber, lblRegCode, lblFullName, lblTotal;
    private Spinner spSBIInternet, spSBIIPTV, spPaidType;
    private Button btnDeposit, btnShowLog;
    private CheckBox chbRemember;
    private ImageView imgSignatureCustomer;
    private RegistrationDetailModel mRegister;
    private CallMposController myController;
    private List<mPOSLogModel> logs = new ArrayList<mPOSLogModel>();
    private Toast toast;
    private String linkImage;

    public DepositActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.lbl_deposite_registration);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(),
                getString(R.string.lbl_deposite_registration));
        setContentView(R.layout.activity_deposit);
        myController = new CallMposController(this);
        lblPhoneNumber = (TextView) findViewById(R.id.lbl_phone_number);
        lblRegCode = (TextView) findViewById(R.id.lbl_reg_code);
        lblFullName = (TextView) findViewById(R.id.lbl_full_name);
        lblTotal = (TextView) findViewById(R.id.lbl_total);
        spSBIInternet = (Spinner) findViewById(R.id.sp_sbi_internet);
        spSBIIPTV = (Spinner) findViewById(R.id.sp_sbi_iptv);
        spPaidType = (Spinner) findViewById(R.id.sp_payment_type);
        imgSignatureCustomer = (ImageView) findViewById(R.id.iv_customer_signature_deposit);
        imgSignatureCustomer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(DepositActivity.this,
                        SignatureActivity.class);
                i.putExtra("RegCode", lblRegCode.getText().toString());
                startActivityForResult(i, 1);
            }
        });
        spPaidType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                if (chbRemember.isChecked()) {
                    if (selectedItem.getID() == 1)
                        saveRememberPaidType(false);
                    else if (selectedItem.getID() == 2)
                        saveRememberPaidType(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        btnDeposit = (Button) findViewById(R.id.btn_deposit);
        btnDeposit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (checkForUpdate()) {
                    int payType = spPaidType.getAdapter() != null ? ((KeyValuePairModel) spPaidType
                            .getSelectedItem()).getID() : 0;
                    if (payType != 2) {
                        // thanh toán tiền mặt, thanh toán online HiFPT/Members
                        comfirmToUpdate();
                    } else {
                        // Thanh toan mPOS
                        checkForDeposit();
                    }
                }
            }
        });
        chbRemember = (CheckBox) findViewById(R.id.chb_remember);
        chbRemember.setVisibility(View.GONE);
        chbRemember.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                // TODO Auto-generated method stub
                saveRememberPaidType(isChecked);
            }
        });

        //
        btnShowLog = (Button) findViewById(R.id.btn_show_log);
        try {
            String userName = ((MyApp) this.getApplication()).getUserName();
            if (userName.toUpperCase().equals("HCM.GIAUTQ"))
                btnShowLog.setVisibility(View.VISIBLE);
            else
                btnShowLog.setVisibility(View.GONE);
            btnShowLog.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(DepositActivity.this,
                            ListmPOSLogActivity.class);
                    DepositActivity.this.startActivity(intent);
                }
            });
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }

        getDataFromIntent();
        initPaidType();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("image");
                linkImage = data.getStringExtra("linkImage");
                Bitmap decodedByte = Common.StringToBitMap(result);
                BitmapDrawable ob = new BitmapDrawable(getResources(),
                        decodedByte);
                imgSignatureCustomer.setBackground(ob);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // Write your code if there's no result
            }
        }
    }

    /* TODO: Sale checkBox */
    private void saveRememberPaidType(boolean isChecked) {
        if (isChecked)
            Common.savePreference(DepositActivity.this,
                    Constants.SHARE_PRE_PAID_TYPE,
                    Constants.SHARE_PRE_PAID_MPOS, "1");
        else
            Common.savePreference(DepositActivity.this,
                    Constants.SHARE_PRE_PAID_TYPE,
                    Constants.SHARE_PRE_PAID_MPOS, "0");
    }

    /* TODO: lấy dữ liệu từ Activty Register Detail */
    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(RegisterDetailActivity.TAG_REGISTER)) {
                mRegister = intent
                        .getParcelableExtra(RegisterDetailActivity.TAG_REGISTER);
                if (mRegister != null) {
                    lblPhoneNumber.setText(mRegister.getPhone_1());
                    lblRegCode.setText(mRegister.getRegCode());
                    lblFullName.setText(mRegister.getFullName());
                    lblTotal.setText(String.valueOf(mRegister.getTotal()));
                    getSBI();

                }
            }
        }
    }

    /* TODO: Lấy các SBI chưa sử dụng */
    private void getSBI() {
        int Status = 1;// lấy SBI còn hạn sd
        new GetSBIList(this, mRegister, Status);
    }

    /* TODO: init Adapter Phương thức thanh toán */
    private void initPaidType() {
        int useMPOS = ((MyApp) getApplication()).getUser() != null ? ((MyApp) getApplication()).getUser().getUseMPOS() : 0;
        ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
        // lst.add(new KeyValuePairModel(0, "[Chọn hình thức thanh toán]"));

        // Nếu được phép thanh toán mPOS
        if (useMPOS > 0)
            lst.add(new KeyValuePairModel(2, "mPOS"));
        else {
            lst.add(new KeyValuePairModel(1, "Thanh toán tiền mặt"));
            lst.add(new KeyValuePairModel(3, "Thanh toán online HiFPT/Members"));
        }
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this,
                R.layout.my_spinner_style, lst, Gravity.LEFT);
        spPaidType.setAdapter(adapter);
        int rememberMPOS = 0;
        int position = 0;
        try {
            String ref = Common.loadPreference(this,
                    Constants.SHARE_PRE_PAID_TYPE,
                    Constants.SHARE_PRE_PAID_MPOS);
            if (ref != null && !ref.equals("")) {
                chbRemember.setChecked(true);
                rememberMPOS = Integer.valueOf(ref);
                if (rememberMPOS > 0 && useMPOS > 0)
                    position = 2;
                else
                    position = 1;
            }
        } catch (Exception e) {

            e.printStackTrace();
            position = 1;
        }
        spPaidType.setSelection(position, true);
    }

    /* TODO: load SBI từ API */
    public void loadData(ArrayList<SBIModel> list) {
        if (mRegister != null) {
            ArrayList<KeyValuePairModel> lstSBIADSL, lstSBIIPTV;
            lstSBIIPTV = new ArrayList<KeyValuePairModel>();
            lstSBIADSL = new ArrayList<KeyValuePairModel>();
            lstSBIADSL.add(new KeyValuePairModel("0", "--Chọn SBI--"));
            lstSBIIPTV.add(new KeyValuePairModel("0", "--Chọn SBI--"));
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    SBIModel item = list.get(i);
                    // Co dk internet moi them sbi
                    if (mRegister.getPromotionID() != 0)
                        if (item.getType() == 1 || item.getType() == 2)// SBI
                            // ADSL
                            // hoac
                            // FTTH
                            lstSBIADSL.add(new KeyValuePairModel(list.get(i)
                                    .getSBI(), list.get(i).getSBI()));
                    // Khong check SBI IPTV
                    if (!mRegister.getIPTVPackage().trim().equals(""))
                        if (item.getType() == 8)// SBI IPTV
                            lstSBIIPTV.add(new KeyValuePairModel(list.get(i)
                                    .getSBI(), list.get(i).getSBI()));

                }
            }
            spSBIInternet.setAdapter(new KeyValuePairAdapter(this,
                    R.layout.my_spinner_style, lstSBIADSL, Gravity.LEFT));
            spSBIIPTV.setAdapter(new KeyValuePairAdapter(this,
                    R.layout.my_spinner_style, lstSBIIPTV, Gravity.LEFT));
        }
    }

    /**
     * @TODO: Kiểm tra tình trạng thu tiền trước khi thanh toán.
     */
    private void checkForDeposit() {
        if (mRegister != null) {
            String sbiInternet = "", sbiIPTV = "";
            sbiInternet = spSBIInternet.getAdapter() != null ? ((KeyValuePairModel) spSBIInternet
                    .getSelectedItem()).getsID() : "0";
            sbiIPTV = spSBIIPTV.getAdapter() != null ? ((KeyValuePairModel) spSBIIPTV
                    .getSelectedItem()).getsID() : "0";
            new CheckForDeposit(this, mRegister, sbiInternet, sbiIPTV);
        } else {
            Common.alertDialog("TTKH không tồn tại.", this);
        }
    }

    public void loadResultCheckForDeposit(List<UpdResultModel> lst) {
        if (lst != null && lst.size() > 0) {
            if (lst.get(0).getResultID() > 0)
                callTPBankmPOSApp(lst.get(0).getMessage());
            else
                Common.alertDialog(lst.get(0).getResult(), this);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
        }
    }

    /* TODO: Kiem tra truoc khi thanh toan */
    private boolean checkForUpdate() {
        if (mRegister.getPromotionID() > 0) {
//			if (sbiInternet.equals("0") || sbiInternet.equals("")) {
//				Common.alertDialog("Chưa chọn số SBI Internet", this);
//				return false;
//				// Dang ky Internet Only nhung ko co SBi
//			}
        } else {
            if (mRegister.getTotal() <= 0) {
                Common.alertDialog(
                        "Tổng tiền bằng 0! Vui lòng cập nhật lại TTKH!",
                        this);
                return false;
                // Dang ky Internet Only nhung ko co SBi
            }
        }
        int paymentType = spPaidType.getAdapter() != null ? ((KeyValuePairModel) spPaidType
                .getSelectedItem()).getID() : 0;
        // Chua chon phuong thuc thanh toan
        if (paymentType == 0) {
            Common.alertDialog("Vui lòng chọn phương thức thanh toán!", this);
            return false;
        }

        // Kiem tra da cai dat ung dung mPOS (thanh toan bang mPOS).
        if (paymentType == 2) {
            String packageName = "com.fpt.tpb.tpbankmpos";
            if (!isPackageInstalled(packageName)) {
                try {
                    String Message = "Hiện tại ứng dụng thanh toán TPBank mPOS chưa được cài đặt. Bạn có muốn cài đặt không?";
                    AlertDialog.Builder builder = null;
                    Dialog dialog = null;
                    builder = new AlertDialog.Builder(this);
                    builder.setTitle("Thông báo")
                            .setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("Có",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            String packageName = "com.fpt.tpb.tpbankmpos";
                                            Uri uri = Uri
                                                    .parse("market://details?id="
                                                            + packageName);
                                            Intent goToMarket = new Intent(
                                                    Intent.ACTION_VIEW, uri);
                                            try {
                                                startActivity(goToMarket);
                                            } catch (ActivityNotFoundException e) {

                                                startActivity(new Intent(
                                                        Intent.ACTION_VIEW,
                                                        Uri.parse("http://play.google.com/store/apps/details?id="
                                                                + packageName)));
                                            }
                                        }
                                    })
                            .setNegativeButton("Không",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    dialog = builder.create();
                    dialog.show();
                    return false;
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    /********************************* THANH TOÁN TIỀN MẶT GỬI SMS *************************************/
    /* TODO: Xac nhan truoc khi thanh toan */
    private void comfirmToUpdate() {
        String Message = "Hệ thống sẽ gửi tin nhắn đến số "
                + lblPhoneNumber.getText().toString()
                + ". Bạn có muốn cập nhật?";
        AlertDialog.Builder builder = null;
        Dialog dialog = null;
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Xác nhận cập nhật")
                .setMessage(Message)
                .setCancelable(false)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deposit();
                    }
                })
                .setNegativeButton("Không",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        dialog = builder.create();
        dialog.show();
    }

    /* TODO: Thanh toan Tien mat */
    private void deposit() {
        try {
            String sbiADSL = "", sbiIPTV = "";
            KeyValuePairModel sbiInterNetModel = ((KeyValuePairModel) spSBIInternet
                    .getSelectedItem());
            if (sbiInterNetModel != null) {
                sbiADSL = sbiInterNetModel.getsID();
            }

            KeyValuePairModel sbiIPTVModel = ((KeyValuePairModel) spSBIIPTV
                    .getSelectedItem());
            if (sbiIPTVModel != null) {
                sbiIPTV = sbiIPTVModel.getsID();
            }

            String sbiInternet = sbiADSL;
            int paymentType = spPaidType.getAdapter() != null ? ((KeyValuePairModel) spPaidType
                    .getSelectedItem()).getID() : 0;
            new UpdateDeposit(this, mRegister.getRegCode(), sbiInternet, sbiIPTV,
                    mRegister.getTotal(), mRegister.getID(), linkImage, paymentType);
        } catch (Exception e) {

        }

    }

    /**************************************** MPOS *************************************/
    private void showToast(String mess) {
        try {
            if (toast == null) {
                toast = Toast.makeText(this, mess, Toast.LENGTH_LONG);
            } else {
                toast.setText(mess);
            }
            toast.show();
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
    }

    /* TODO: Kiểm tra ứng dụng TPBank mPOS cài đặt hay chưa */
    private boolean isPackageInstalled(String packagename) {
        PackageManager pm = this.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (NameNotFoundException e) {

            return false;
        }
    }

    private int step = 0;

    /* TODO: Thanh toan MPOS */
    private void callTPBankmPOSApp(String paymentDesc) {
        logs.clear();
        mPOSLogModel log = new mPOSLogModel();
        step = 0;
        log.setStep(step);
        log.setStepDesc(TAG_STEP_0);
        log.setClientDate(Calendar.getInstance().getTime());
        log.setClientID(clientIDIdentity(mRegister.getRegCode()));
        try {
            if (Common.isNullOrEmpty(paymentDesc))
                paymentDesc = getString(R.string.MPOS_PAYMENT_DESC);

            log.setReferenceID(mRegister.getRegCode());
            log.setAmount(mRegister.getTotal());
            log.setSEND_MPOS_EMAIL(mRegister.getEmail());
            log.setSEND_MPOS_PHONE_NUMBER(mRegister.getPhone_1());
            log.setSEND_MPOS_CUSNAME(mRegister.getFullName());
            log.setMPOS_PAYMENT_DESCRIPTION(paymentDesc);

            Hashtable<String, String> hash = new Hashtable<String, String>();
            hash.put(CallMposController.MPOS_CREDENTIALKEY,
                    Constants.SHARE_PRE_MPOS_CREDENTIAL_KEY);
            hash.put(CallMposController.MPOS_CREDENTIALPWD,
                    Constants.SHARE_PRE_MPOS_CREDENTIAL_PWD);
            hash.put(CallMposController.MPOS_REF_KEY, mRegister.getRegCode());
            hash.put(CallMposController.MPOS_AMOUNT,
                    String.valueOf(mRegister.getTotal()));// mRegister.getTotal()
            hash.put(CallMposController.MPOS_EMAIL, mRegister.getEmail());
            hash.put(CallMposController.MPOS_CUSNAME, mRegister.getFullName());
            hash.put(CallMposController.MPOS_PHONE_NUMBER,
                    mRegister.getPhone_1());
            hash.put(CallMposController.MPOS_PAYMENT_DESCRIPTION, paymentDesc);
            //
            // ///////////////////////////////////////////////////////////////////////////////////
            // 02/10/2014 Add check transaction if existed param
            hash.put(CallMposController.MPOS_FLG_CHECK_TRANSACTION_EXISTED, "1");
            // ///////////////////////////////////////////////////////////////////////////////////
            // “1”: cho phép sửa thông tin giao dịch trên mPOS; “2”: không cho
            // phép sửa thông tin giao dịch trên mPOS
            hash.put(CallMposController.MPOS_PAYMENT_STEP, "");
            // ///////////////////////////////////////////////////////////////////////////////////
            // “0”: thanh toán bằng thẻ; “1” thanh toán bằng phương thức chuyển
            // khoản
            hash.put(CallMposController.MPOS_PAYMENT_METHOD,/*
             * cb_transfer.isChecked
             * ()?"1":"0"
             */"0");

            hash.put(CallMposController.MPOS_TRANSACTION_KEY, "");
            logs.add(log);
            myController.CallPaymentScreen(false, hash);// false: cho phép thanh
            // toán cùng lúc nhiều
            // PĐK, true: thanh toán
            // tuần tự
            // updateLogs();
        } catch (NameNotFoundException ea) {
            // mpos hasn't been installed

            log.setResult_ErrDesc(ea.getMessage());
            ea.printStackTrace();
            logs.add(log);
            updateLogs();
            Common.alertDialog(ea.getMessage(), this);
        } catch (Exception e) {

            log.setResult_ErrDesc(e.getMessage());
            e.printStackTrace();
            logs.add(log);
            updateLogs();
            Common.alertDialog(e.getMessage(), this);
        }
    }

    /* TODO: mPOS TimeOut */
    @Override
    public void onMposTimeout(String referenceKey, int code) {
        // TODO Auto-generated method stub
        /*
         * EditText et_sts = (EditText)findViewById(R.id.editText1);
         * et_sts.setText(
         * "Handle status: Success. Error with: Code 106. Content: Processing timeout. Please try again! "
         * );
         */
        try {
            mPOSLogModel log = new mPOSLogModel();
            if (logs != null && logs.size() > 0) {
                log.copy(logs.get(0));
            }
            step++;
            log.setStep(step);
            log.setStepDesc(String.valueOf(step) + ". " + "MposTimeout");
            log.setResult_ErrDesc("mPOS TimeOut:" + referenceKey + "("
                    + String.valueOf(code) + ")");
            log.setResult_ErrCode(String.valueOf(code));
            log.setClientDate(Calendar.getInstance().getTime());
            log.setClientID(clientIDIdentity(mRegister.getRegCode()));
            logs.add(log);
            updateLogs();
            if (referenceKey == null)
                referenceKey = "";
            showToast("MposTimeout: " + referenceKey);
        } catch (Exception e) {
            // TODO: handle exception
            // Common.alertDialog(e.getMessage(), DepositActivity.this);

            showToast(e.getMessage());
        }

    }

    @Override
    public void onMposCanceled(String referenceKey, String amount) {
        // TODO Auto-generated method stub
        try {
            mPOSLogModel log = new mPOSLogModel();
            if (logs != null && logs.size() > 0) {
                log.copy(logs.get(0));
            }
            step++;
            log.setStep(step);
            log.setRegCode("-9999999");
            log.setResult_ErrDesc("mPOS Cancel: referenceKey:" + referenceKey
                    + ", amount:" + amount);
            log.setStepDesc(String.valueOf(step) + ". " + "MposCanceled");
            log.setClientDate(Calendar.getInstance().getTime());
            log.setClientID(clientIDIdentity(mRegister.getRegCode()));
            logs.add(log);
            updateLogs();
            showToast("Bạn đã hủy thanh toán mPOS.");
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
            showToast(e.getMessage());
        }

    }

    @Override
    public void onMposError(String referenceKey, String terminalId,
                            String merchantId, String cardHolderName, String maskedCardNumber,
                            String referenceNbr, String responseCode, String transactionKey,
                            int errCode, String amount, String signatureBase64,
                            String transactionDate, String paymentType) {
        // TODO Auto-generated method stub
        try {
            mPOSLogModel log = new mPOSLogModel();
            if (logs != null && logs.size() > 0) {
                log.copy(logs.get(0));
            }
            step++;
            log.setStep(step);
            log.setClientDate(Calendar.getInstance().getTime());
            log.setReferenceID(referenceKey);
            log.setResult_TerminalId(terminalId);
            log.setResult_CardHolderName(cardHolderName);
            log.setResult_MaskedCardNumber(maskedCardNumber);
            log.setResult_ReferenceNbr(referenceNbr);
            log.setResult_ResponseCode(responseCode);
            log.setResult_TransactionKey(transactionKey);
            try {
                log.setClientID(clientIDIdentity(mRegister.getRegCode()));
                log.setAmount(Integer.valueOf(amount));
            } catch (Exception e) {

                log.setAmount(0);
                e.printStackTrace();
            }
            log.setPaymentType(paymentType);
            log.setTransactionDate(transactionDate);
            //

            String text = "";
            if (errCode == ErrorHandler.MPOS_ERR_PAYMENT_APP) {
                text = getString(R.string.MPOS_ERR_PAYMENT_APP_100);
            } else if (errCode == ErrorHandler.MPOS_ERR_PAYMENT_SYS) {
                text = getString(R.string.MPOS_ERR_PAYMENT_SYS_101);
            } else if (errCode == ErrorHandler.MPOS_ERR_RECEIVED_FAIL) {
                text = getString(R.string.MPOS_ERR_RECEIVED_FAIL_102);
            } else if (errCode == ErrorHandler.MPOS_ERR_AUTH_FAIL) {
                text = getString(R.string.MPOS_ERR_AUTH_FAIL_103);
            } else if (errCode == ErrorHandler.MPOS_ERR_WRONG_PAYMENTINFO) {
                text = getString(R.string.MPOS_ERR_WRONG_PAYMENTINFO_104);
            } else if (errCode == ErrorHandler.MPOS_ERR_WRONG_REF_ID) {
                text = getString(R.string.MPOS_ERR_WRONG_REF_ID_1041);
            } else if (errCode == ErrorHandler.MPOS_ERR_WRONG_AMOUNT) {
                text = getString(R.string.MPOS_ERR_WRONG_AMOUNT_1042);
            } else if (errCode == ErrorHandler.MPOS_ERR_WRONG_TRANSACTION_KEY) {
                text = getString(R.string.MPOS_ERR_WRONG_TRANSACTION_KEY);
            } else if (errCode == ErrorHandler.MPOS_ERR_TRANSACTION_NOT_EXIST) {
                text = getString(R.string.MPOS_ERR_TRANSACTION_NOT_EXIST);
            } else if (errCode == ErrorHandler.MPOS_ERR_PAYMENTYPE_NOT_SUPPORTED) {
                text = getString(R.string.MPOS_ERR_PAYMENTYPE_NOT_SUPPORTED);
            } else if (errCode == ErrorHandler.MPOS_ERR_BANK_SYS) {
                text = getString(R.string.MPOS_ERR_BANK_SYS_105);
                /*
                 * try { byte[] decodedByte = Base64.decode(signatureBase64, 0);
                 * Bitmap t = BitmapFactory.decodeByteArray(decodedByte, 0,
                 * decodedByte.length); ImageView imgv =
                 * (ImageView)findViewById(R.id.imgsign);
                 * imgv.setImageBitmap(t); imgv.setVisibility(View.VISIBLE); }
                 * catch (Exception e) { // TODO: handle exception }
                 */
            } else if (errCode == ErrorHandler.MPOS_ERR_TIME_OUT) {
                text = getString(R.string.MPOS_ERR_TIME_OUT_106);
            } else if (errCode == ErrorHandler.MPOS_ERR_WRONG_EMAIL) {
                text = getString(R.string.MPOS_ERR_WRONG_EMAIL_107);
            } else if (errCode == ErrorHandler.MPOS_ERR_WRONG_CUSNAME) {
                text = getString(R.string.MPOS_ERR_WRONG_CUSNAME_108);
            } else if (errCode == ErrorHandler.MPOS_ERR_WRONG_PHONENBR) {
                text = getString(R.string.MPOS_ERR_WRONG_PHONENBR_109);
            }

            log.setResult_ErrCode(String.valueOf(errCode));
            log.setResult_ErrDesc(text);
            log.setStepDesc(String.valueOf(step) + ". " + "MposError");
            logs.add(log);
            updateLogs();
            showToast("Thanh toán thất bại: " + text);
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
            // Common.alertDialog(e.getMessage(), DepositActivity.this);
            showToast(e.getMessage());
        }

    }

    @Override
    public void onMposSuccess(String referenceKey, String terminalId,
                              String merchantId, String cardHolderName, String maskedCardNumber,
                              String referenceNbr, String responseCode, String approvedCode,
                              String transactionKey, String amount, String signatureBase64,
                              String transactionDate, boolean transactionAlreadyPaid,
                              String paymentType, String voidedDate) {
        showToast("Thanh toán thành công");
        try {
            mPOSLogModel log = new mPOSLogModel();
            if (logs != null && logs.size() > 0) {
                log.copy(logs.get(0));
            }
            step++;
            log.setStep(step);
            log.setClientDate(Calendar.getInstance().getTime());
            log.setReferenceID(referenceKey);
            log.setResult_TerminalId(terminalId);
            log.setResult_MerchantId(merchantId);
            log.setResult_CardHolderName(cardHolderName);
            log.setResult_MaskedCardNumber(maskedCardNumber);
            log.setResult_ReferenceNbr(referenceNbr);
            log.setResult_ResponseCode(responseCode);
            log.setResult_ApprovedCode(approvedCode);
            log.setResult_TransactionKey(transactionKey);
            try {
                log.setClientID(clientIDIdentity(mRegister.getRegCode()));
                log.setAmount(Integer.valueOf(amount));
            } catch (Exception e) {
                // TODO: handle exception

                log.setAmount(0);
                e.printStackTrace();
            }

            log.setStatus(1);
            if (transactionAlreadyPaid == false)
                log.setStepDesc(TAG_STEP_1);
            else
                log.setStepDesc(TAG_STEP_1_1);
            log.setPaymentType(paymentType);
            log.setTransactionAlreadyPaid(transactionAlreadyPaid == true ? 1
                    : 0);
            log.setTransactionDate(transactionDate);
            logs.add(log);
        } catch (Exception e) {
            // TODO: handle exception

            showToast(e.getMessage());
        }
        depositMPOS();
        updateLogs();
    }

    /* TODO: Cap nhat Log */
    private void updateLogs() {
        String UserName = ((MyApp) this.getApplication()).getUserName();
        try {
            if (logs != null && logs.size() > 0) {
                for (mPOSLogModel item : logs) {
                    item.setUserName(UserName);
                    item.setRegCode(mRegister.getRegCode());
                }
                // Insert Client DB
                mPOSLogTable table = new mPOSLogTable(this);
                table.add(logs);
                // Update Server DB
                WriteLogMPos task = new WriteLogMPos(this, logs);
                task.execute();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat")
    public String clientIDIdentity(String RegCode) {
        try {
            String regC = RegCode;
            if (regC == null)
                regC = "";
            String miliTime = new SimpleDateFormat("yyyyMMddHHmmss.SSSS")
                    .format(Calendar.getInstance().getTime());
            return (regC + "_" + miliTime);
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }

        return null;
    }

    /* TODO: Thanh toan PDK sau khi thanh toan MPOS thanh cong */
    private void depositMPOS() {
        String sbiADSL = ((KeyValuePairModel) spSBIInternet.getSelectedItem())
                .getsID();
        String sbiIPTV = ((KeyValuePairModel) spSBIIPTV.getSelectedItem())
                .getsID();
        String sbiInternet = sbiADSL;
        new UpdateDepositmPOS(this, mRegister, sbiInternet, sbiIPTV);
    }

    // TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        myController.doOnstart();
        // Get an Analytics tracker to report app starts and uncaught exceptions
        // etc.
        Common.reportActivityStart(this, this);
    }

    // TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        // Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
