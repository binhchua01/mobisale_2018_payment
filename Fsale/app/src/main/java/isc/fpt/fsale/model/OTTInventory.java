package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OTTInventory {
    @SerializedName("HTMLDetail")
    @Expose
    private String HTMLDetail;

    public String getHTMLDetail() {
        return HTMLDetail;
    }

    public void setHTMLDetail(String HTMLDetail) {
        this.HTMLDetail = HTMLDetail;
    }
}
