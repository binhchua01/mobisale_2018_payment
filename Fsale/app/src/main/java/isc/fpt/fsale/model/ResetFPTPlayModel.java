package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResetFPTPlayModel implements Parcelable {
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("ResultID")
    @Expose
    private Integer resultID;

    public ResetFPTPlayModel(String message, Integer resultID) {
        this.message = message;
        this.resultID = resultID;
    }

    public ResetFPTPlayModel() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getResultID() {
        return resultID;
    }

    public void setResultID(Integer resultID) {
        this.resultID = resultID;
    }

    public static Creator<ResetFPTPlayModel> getCREATOR() {
        return CREATOR;
    }

    protected ResetFPTPlayModel(Parcel in) {
        message = in.readString();
        if (in.readByte() == 0) {
            resultID = null;
        } else {
            resultID = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        if (resultID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(resultID);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResetFPTPlayModel> CREATOR = new Creator<ResetFPTPlayModel>() {
        @Override
        public ResetFPTPlayModel createFromParcel(Parcel in) {
            return new ResetFPTPlayModel(in);
        }

        @Override
        public ResetFPTPlayModel[] newArray(int size) {
            return new ResetFPTPlayModel[size];
        }
    };
}
