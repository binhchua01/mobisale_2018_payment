package isc.fpt.fsale.model;

import java.util.ArrayList;

public class Office365Model {

	public Office365Model() {
		// TODO Auto-generated constructor stub
	}
	
	private String EmailAdmin;
	private String DomainName;
	private String TechName;
	private String TechPhoneNumber;
	private String TechEmail;
	private String ListPackage ;
	private ArrayList<PackageModel> ArrayListPackage ;
	
	public String getEmailAdmin() {
		return EmailAdmin;
	}
	public void setEmailAdmin(String emailAdmin) {
		EmailAdmin = emailAdmin;
	}
	public String getDomainName() {
		return DomainName;
	}
	public void setDomainName(String domainName) {
		DomainName = domainName;
	}
	public String getTechName() {
		return TechName;
	}
	public void setTechName(String techName) {
		TechName = techName;
	}
	public String getTechPhoneNumber() {
		return TechPhoneNumber;
	}
	public void setTechPhoneNumber(String techPhoneNumber) {
		TechPhoneNumber = techPhoneNumber;
	}
	public String getTechEmail() {
		return TechEmail;
	}
	public void setTechEmail(String techEmail) {
		TechEmail = techEmail;
	}
	public String getListPackage() {
		return ListPackage;
	}
	public void setListPackage(String listPackage) {
		ListPackage = listPackage;
	}
	public ArrayList<PackageModel> getArrayListPackage() {
		return ArrayListPackage;
	}
	public void setArrayListPackage(ArrayList<PackageModel> arrayListPackage) {
		ArrayListPackage = arrayListPackage;
	}
	
}
