package isc.fpt.fsale.ui.fragment;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import isc.fpt.fsale.R;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;
import isc.fpt.fsale.utils.MyApp.TrackerName;
import isc.fpt.fsale.action.ResetPassword;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
// màn hình đổi mật khẩu
public class ResetPasswordDialog extends DialogFragment{
	
	private EditText txtPasswordOld, txtPasswordNew, txtConfirmPasswordNew;
	private Button btnUpdate;
	private ImageButton btnClose;	
	private Context mContext;
	private DialogFragment frmResetPassword;

	public ResetPasswordDialog(){ }

	@SuppressLint("ValidFragment")
	public ResetPasswordDialog(Context context){
		this.mContext = context;
		
	}
	
    /** The system calls this to get the DialogFragment's layout, regardless
        of whether it's being displayed as a dialog or an embedded fragment. */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
    	  View view = inflater.inflate(R.layout.dialog_reset_password, container);
    	  txtPasswordNew = (EditText) view.findViewById(R.id.txt_password_new);
    	  txtPasswordOld = (EditText) view.findViewById(R.id.txt_password_old);
    	  txtConfirmPasswordNew = (EditText) view.findViewById(R.id.txt_confirm_password_new);
    	  btnUpdate = (Button)view.findViewById(R.id.btn_update);	
    	  btnClose = (ImageButton)view.findViewById(R.id.btn_close); 
    	  this.frmResetPassword = this;
    	  
    	  btnUpdate.setOnClickListener(new View.OnClickListener() {  			
  			@Override
  			public void onClick(View v) { 
				String passold, passnew, confirmpass;
				passold = txtPasswordOld.getText().toString().trim();
				passnew = txtPasswordNew.getText().toString().trim();
				confirmpass = txtConfirmPasswordNew.getText().toString().trim();
				if(CheckValue(passold, passnew, confirmpass))
				{
					new ResetPassword(mContext,passold,passnew,frmResetPassword);
					onClickTracker("Reset password");
				}
  			}
  		  });
    	  
    	  btnClose.setOnClickListener(new View.OnClickListener() {  			
    			@Override
    			public void onClick(View v) {
    				getDialog().dismiss();
    			}
    	  });
          getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
          return view;
    }
  
    /** The system calls this only when creating the layout in a dialog. */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Common.reportActivityCreate(getActivity().getApplication(), getString(R.string.lbl_screen_name_dialog_reset_password));
        return dialog;
    }
    
    // Kiểm tra giá trị
    private Boolean CheckValue(String passold, String passnew, String confirmpass)
    {
    	Boolean flag = true;
    	try{
    		// Kiểm tra pass cũ
    		if(passold == null || passold.equals(""))
    		{
    			txtPasswordOld.setFocusable(true);
    			txtPasswordOld.requestFocus();
    			txtPasswordOld.setError(mContext.getResources().getString(R.string.msg_password_old_empty));
    			flag = false;
    		}
    		if(passnew == null || passnew.equals(""))
    		{
    			txtPasswordNew.setFocusable(true);
    			txtPasswordNew.requestFocus();
    			txtPasswordNew.setError(mContext.getResources().getString(R.string.msg_password_new_empty));
    			flag = false;
    		}
    		
    		if(confirmpass == null || confirmpass.equals(""))
    		{
    			txtConfirmPasswordNew.setFocusable(true);
    			txtConfirmPasswordNew.requestFocus();
    			txtConfirmPasswordNew.setError(mContext.getResources().getString(R.string.msg_confirm_password_new_empty));
    			flag = false;
    		}
    		
    		if(!passnew.equals(confirmpass))
    		{
    			txtPasswordNew.setFocusable(true);
    			txtPasswordNew.requestFocus();
    			txtPasswordNew.setError(mContext.getResources().getString(R.string.msg_password_new_error));
    			txtConfirmPasswordNew.setError(mContext.getResources().getString(R.string.msg_password_new_error));
    			flag = false;
    		}
    	}
    	catch(Exception ex)
    	{
    		flag = false;
    	}
    	return flag;
    }
    private void onClickTracker(String lable){
		 try {
			// Get tracker.
			 Tracker t = ((MyApp) getActivity().getApplication()).getTracker(
			     TrackerName.APP_TRACKER);
			 // Set screen name.
			 t.setScreenName(getString(R.string.lbl_screen_name_dialog_reset_password)+".onClick()");
			 // Send a screen view.
			 t.send(new HitBuilders.ScreenViewBuilder().build());

			 // This event will also be sent with the most recently set screen name.
			 // Build and send an Event.
			 t.send(new HitBuilders.EventBuilder()
			     .setCategory("RESET_PASSWORD")
			     .setAction("onClick")
			     .setLabel(lable)
			     .build());

			 // Clear the screen name field when we're done.
			 t.setScreenName(null);
		} catch (Exception e) {

			Log.i("RESET_PASSWORD New Tracker: ",e.getMessage());
		}
		
	 }
}