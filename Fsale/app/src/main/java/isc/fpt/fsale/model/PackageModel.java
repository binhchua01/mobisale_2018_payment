package isc.fpt.fsale.model;

import java.lang.reflect.Field;

import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class PackageModel implements Parcelable {

    private int PackageID;
    private String PackageName;
    private int Price;
    private int Monthly;
    private int Seat;
    private int SeatAdd;
    private int MonthlyAdd;
    private int PriceAdd;
    private int Status;

    public int getPackageID() {
        return PackageID;
    }

    public void setPackageID(int packageID) {
        PackageID = packageID;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public int getMonthly() {
        return Monthly;
    }

    public void setMonthly(int monthly) {
        Monthly = monthly;
    }

    public int getSeat() {
        return Seat;
    }

    public void setSeat(int seat) {
        Seat = seat;
    }

    public int getSeatAdd() {
        return SeatAdd;
    }

    public void setSeatAdd(int seatAdd) {
        SeatAdd = seatAdd;
    }

    public int getMonthlyAdd() {
        return MonthlyAdd;
    }

    public void setMonthlyAdd(int monthlyAdd) {
        MonthlyAdd = monthlyAdd;
    }

    public int getPriceAdd() {
        return PriceAdd;
    }

    public void setPriceAdd(int priceAdd) {
        PriceAdd = priceAdd;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }


    private PackageModel(Parcel in) {
        this.PackageID = in.readInt();
        this.PackageName = in.readString();
        this.Price = in.readInt();
        this.PriceAdd = in.readInt();
        this.Seat = in.readInt();
        this.SeatAdd = in.readInt();
        this.Monthly = in.readInt();
        this.MonthlyAdd = in.readInt();
        this.Status = in.readInt();
    }


    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(this.PackageID);
        out.writeString(this.PackageName);
        out.writeInt(this.Price);
        out.writeInt(this.PriceAdd);
        out.writeInt(this.Seat);
        out.writeInt(this.SeatAdd);
        out.writeInt(this.Monthly);
        out.writeInt(this.MonthlyAdd);
        out.writeInt(this.Status);
    }

    public static final Creator<PackageModel> CREATOR = new Creator<PackageModel>() {
        @Override
        public PackageModel createFromParcel(Parcel source) {
            return new PackageModel(source);
        }

        @Override
        public PackageModel[] newArray(int size) {
            return new PackageModel[size];
        }
    };

    public JSONObject toJSONObject() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("PackageID", this.getPackageID());
            jsonObj.put("PackageName", this.getPackageName());
            jsonObj.put("Price", this.getPrice());
            jsonObj.put("PriceAdd", this.getPriceAdd());
            jsonObj.put("Seat", this.getSeat());
            jsonObj.put("SeatAdd", this.getSeatAdd());
            jsonObj.put("Monthly", this.getMonthly());
            jsonObj.put("MonthlyAdd", this.getMonthlyAdd());
            jsonObj.put("Status", this.getStatus());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String toString() {
        JSONObject jsonObj = new JSONObject();
        try {
            Field[] fields = getClass().getDeclaredFields();
            for (Field f : fields) {
                f.setAccessible(true);
                jsonObj.put(f.getName(), f.get(this));
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return jsonObj.toString();
    }
}