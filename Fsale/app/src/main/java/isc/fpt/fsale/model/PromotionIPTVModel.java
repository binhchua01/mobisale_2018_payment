package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 7/18/2017.
 */
// model câu lệnh khuyến mãi IPTV
public class PromotionIPTVModel implements Parcelable {
    private int PromotionID;
    private String PromotionName;
    private int Type;
    private double Amount;
    private boolean ErrorService;

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int promotionID) {
        PromotionID = promotionID;
    }

    public String getPromotionName() {
        return PromotionName;
    }

    public void setPromotionName(String promotionName) {
        PromotionName = promotionName;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public boolean isErrorService() {
        return ErrorService;
    }

    public void setErrorService(boolean errorService) {
        ErrorService = errorService;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeInt(PromotionID);
        parcel.writeString(PromotionName);
        parcel.writeDouble(Amount);
        parcel.writeInt(Type);
        parcel.writeByte((byte) (ErrorService ? 1 : 0));
    }

    protected PromotionIPTVModel(Parcel in) {
        PromotionID = in.readInt();
        PromotionName = in.readString();
        Amount = in.readDouble();
        Type = in.readInt();
        ErrorService = in.readByte() != 0;
    }

    public static final Creator<PromotionIPTVModel> CREATOR = new Creator<PromotionIPTVModel>() {
        @Override
        public PromotionIPTVModel createFromParcel(Parcel in) {
            return new PromotionIPTVModel(in);
        }

        @Override
        public PromotionIPTVModel[] newArray(int size) {
            return new PromotionIPTVModel[size];
        }
    };

}
