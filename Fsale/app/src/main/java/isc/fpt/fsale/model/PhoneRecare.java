package isc.fpt.fsale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhoneRecare {
    @SerializedName("isRecare")
    @Expose
    private int isRecare;

    @SerializedName("message")
    @Expose
    private String message;

    public PhoneRecare(int isRecare, String message) {
        this.isRecare = isRecare;
        this.message = message;
    }

    public int getIsRecare() {
        return isRecare;
    }

    public void setIsRecare(int isRecare) {
        this.isRecare = isRecare;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
