package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.NewVersionDialog;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.DialogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

// API check version cho ứng dụng
public class CheckVersion implements AsyncTaskCompleteListener<String> {
    public static String LINK = null;
    private Context mContext;
    private FragmentActivity activity;
    private DialogFragment aboutDialog;
    public static Dialog dialogRequiredUpdate;


    public CheckVersion(Context mContext, String[] arrParams, FragmentActivity activity) {
        this.mContext = mContext;
        this.activity = activity;
        this.aboutDialog = null;
        String[] params = new String[]{"DeviceIMEI", "CurrentVersion", "AppType", "Platform"};
        String message = mContext.getResources().getString(R.string.msg_pd_checkversion);
        String GET_VERSION = "GetVersion";
        CallServiceTask service = new CallServiceTask(mContext, GET_VERSION, params, arrParams,
                Services.JSON_POST, message, CheckVersion.this);
        service.execute();
    }

    private void handleGetVersionResult(String json) {
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            try {
                String TAG_GET_VERSION_METHOD_POST_RESULT = "GetVersionMethodPostResult";
                jsObj = getJsonObject(json).getJSONObject(TAG_GET_VERSION_METHOD_POST_RESULT);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (jsObj != null) {
                try {
                    String TAG_VERSION = "Version";
                    String version = jsObj.getString(TAG_VERSION);
                    String TAG_LINK = "Link";
                    String link = jsObj.getString(TAG_LINK);
                    String TAG_ERROR = "ErrorService";
                    String error = jsObj.getString(TAG_ERROR);
                    if (jsObj.isNull(TAG_ERROR) || error.equals("")) {
                        if ((!version.equals("null")) && (!link.equals("null"))) {
                            LINK = jsObj.getString(TAG_LINK);
                            if (this.activity != null) {
                                dialogRequiredUpdate = DialogUtils.dialogUpdateVersion(mContext,
                                        jsObj.getString(TAG_LINK));
                                if (dialogRequiredUpdate != null && !dialogRequiredUpdate.isShowing()) {
                                    dialogRequiredUpdate.show();
                                }
                            }
                            if (this.aboutDialog != null) {
                                NewVersionDialog versionDialog = new NewVersionDialog(
                                        mContext, jsObj.getString(TAG_LINK),
                                        this.aboutDialog);
                                versionDialog.setCancelable(false);
                                Common.showFragmentDialog(aboutDialog.getActivity().getSupportFragmentManager(),
                                        versionDialog, "fragment_new_version_dialog");
                            }
                        } else {
                            //kết nối API kiểm tra IMEI
                            new CheckSIMIMEI(mContext, new String[]{
                                    DeviceInfo.SIM_IMEI, DeviceInfo.DEVICE_IMEI,
                                    DeviceInfo.ANDROID_OS_VERSION,
                                    DeviceInfo.MODEL_NUMBER});
                        }
                    } else {
                        Common.alertDialog("Lỗi WS:" + error, mContext);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
            }
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            return JSONParsing.getJsonObj(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onTaskComplete(String result) {
        handleGetVersionResult(result);
    }

}
