package isc.fpt.fsale.model;

public class CEMCodeResultObjectModel {
	
	private int ID ; 
	private String Result ; 
	private int ResultID ;
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getResult() {
		return Result;
	}
	public void setResult(String result) {
		Result = result;
	}
	public int getResultID() {
		return ResultID;
	}
	public void setResultID(int resultID) {
		ResultID = resultID;
	}
	
	public CEMCodeResultObjectModel (int ID , String Result , int ResultID)
	{
		this.ID = ID ; 
		this.Result = Result ; 
		this.ResultID = ResultID;
	}
	
	public CEMCodeResultObjectModel()
	{}
}
