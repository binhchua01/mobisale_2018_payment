package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListObjectHDBoxAction;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class ListObjectHDBoxActivity extends BaseActivity implements OnItemClickListener {

    private Context mContext;
    private ListView lvObjectList;

    // Tim kiem
    private ImageView btnFind;
    private EditText txtSearchValue;
    public Spinner SpSearchType;
    private String SearchType;


    // Phan trang
    public String PageSize = "1;1", ParamName;
    public int PAGE_COUNT = 1;
    public int iCurrentPage = 1;
    public Spinner SpPage;
    private Button btnPre, btnNext;

    public ListObjectHDBoxActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.menu_list_object);
        MyApp.setCurrentActivity(this);
        mContext = this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), "ListObjectHDBoxActivity");
        setContentView(R.layout.activity_iptv);

        if (DeviceInfo.ANDROID_SDK_VERSION >= 11) {
            try {
                android.app.ActionBar actionBar = getActionBar();
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.main_color_blue));
                actionBar.setTitle(R.string.menu_list_object);
            } catch (Exception e) {

                e.printStackTrace();
            }
        }

        // Tim kiem
        txtSearchValue = (EditText) findViewById(R.id.txt_search_value);
        btnFind = (ImageView) findViewById(R.id.btn_find);
        SpSearchType = (Spinner) findViewById(R.id.sp_search_type);

        lvObjectList = (ListView) findViewById(R.id.lv_object_list);
        lvObjectList.setOnItemClickListener(this);
        //Phan trang
        SpPage = (Spinner) this.findViewById(R.id.sp_page_num);
        btnPre = (Button) this.findViewById(R.id.btn_previous);
        btnNext = (Button) this.findViewById(R.id.btn_next);
        //
        loadSearchType();
        //

        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtSearchValue.getText() == null || txtSearchValue.getText().toString().trim().equals("")) {
                    Common.alertDialog(mContext.getResources().getString(R.string.msg_search_value_empty), mContext);
                    return;
                }
                new GetListObjectHDBoxAction(mContext, txtSearchValue.getText().toString(), SearchType, String.valueOf(iCurrentPage));
            }
        });

        SpSearchType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                SearchType = selectedItem.getsID();
                if (selectedItem.getsID().equals("4") || selectedItem.getsID().equals("5")) {
                    txtSearchValue.setText("");
                    txtSearchValue.setEnabled(false);
                    new GetListObjectHDBoxAction(mContext, "", selectedItem.getsID(), "0");
                } else
                    txtSearchValue.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


        //
        SpPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() != iCurrentPage) {
                    iCurrentPage = selectedItem.getID();
                    SearchOption();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        btnPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iCurrentPage--;
                CheckEnable();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iCurrentPage++;
                CheckEnable();
            }
        });
        Common.hideSoftKeyboard(mContext);
        new GetListObjectHDBoxAction(mContext, txtSearchValue.getText().toString(), "1", String.valueOf(iCurrentPage));


    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    // Load dữ liệu loại tìm kiếm
    private void loadSearchType() {
        ArrayList<KeyValuePairModel> lstSearchType = new ArrayList<>();
        lstSearchType.add(new KeyValuePairModel("1", "Số HD"));
        lstSearchType.add(new KeyValuePairModel("2", "Tên KH"));
        lstSearchType.add(new KeyValuePairModel("3", "Phone"));
        lstSearchType.add(new KeyValuePairModel("4", "Chưa đăng ký"));
        lstSearchType.add(new KeyValuePairModel("5", "Đã đăng ký"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstSearchType, Gravity.LEFT);
        SpSearchType.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        if (Common.isNetworkAvailable(this.mContext)) {
            //UpdateHDBoxDialog dialog = new UpdateHDBoxDialog(mContext, selectedItem, fm);
            //Common.showFragmentDialog(fm, dialog, "fragment_update_hdbox_dialog");
        } else {
            String s = "Vui lòng kết nối Internet!";
            Toast toast = Toast.makeText(mContext, s, Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    private void CheckEnable() {

        if (PAGE_COUNT == 1) {
            btnPre.setEnabled(false);
            btnNext.setEnabled(false);
        } else {
            if (iCurrentPage >= PAGE_COUNT) {
                btnNext.setEnabled(false);
                btnPre.setEnabled(true);
                if (iCurrentPage == PAGE_COUNT) {
                    SearchOption();

                }
            } else if (iCurrentPage <= 1) {
                btnPre.setEnabled(false);
                btnNext.setEnabled(true);
                if (iCurrentPage == 1) {
                    SearchOption();

                }
            } else {
                btnPre.setEnabled(true);
                btnNext.setEnabled(true);
                SearchOption();

            }
        }
    }

    private void SearchOption() {
        KeyValuePairModel selectedItem = (KeyValuePairModel) SpSearchType.getSelectedItem();
        SearchType = selectedItem.getsID();
        if (SearchType.equals("4") || SearchType.equals("5"))
            new GetListObjectHDBoxAction(mContext, "", SearchType, String.valueOf(iCurrentPage));
        else
            new GetListObjectHDBoxAction(mContext, txtSearchValue.getText().toString(), SearchType, String.valueOf(iCurrentPage));
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }
}
