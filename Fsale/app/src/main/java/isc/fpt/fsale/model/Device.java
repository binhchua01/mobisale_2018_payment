package isc.fpt.fsale.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Device implements Parcelable {
    /*
     * Dùng cho API GetListEquipment
     * */
    private int DeviceID;//giống như CodeID, đều là ID của thiết bị
    private String DeviceName;
    /*
     * Dùng cho API GetListPriceEquipment
     * */
    private int CustomerStatus;
    private int EFStatus;
    private int NumMax;
    private int NumMin;
    private double Price;
    private int PriceID;
    private String PriceName;
    private String PriceText;
    private int ServiceCode;
    private int Number;
    private int ReturnStatus;
    private boolean isSelected = false;

    /*
     * Dùng cho API GetRegistrationDetail
     * */
    private int PromotionDeviceID;
    private String PromotionDeviceText;

    protected Device(Parcel in) {
        DeviceID = in.readInt();
        DeviceName = in.readString();
        CustomerStatus = in.readInt();
        EFStatus = in.readInt();
        NumMax = in.readInt();
        NumMin = in.readInt();
        Price = in.readDouble();
        PriceID = in.readInt();
        PriceName = in.readString();
        PriceText = in.readString();
        ServiceCode = in.readInt();
        Number = in.readInt();
        isSelected = in.readByte() != 0;
        PromotionDeviceID = in.readInt();
        PromotionDeviceText = in.readString();
        ReturnStatus = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(DeviceID);
        dest.writeString(DeviceName);
        dest.writeInt(CustomerStatus);
        dest.writeInt(EFStatus);
        dest.writeInt(NumMax);
        dest.writeInt(NumMin);
        dest.writeDouble(Price);
        dest.writeInt(PriceID);
        dest.writeString(PriceName);
        dest.writeString(PriceText);
        dest.writeInt(ServiceCode);
        dest.writeInt(Number);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeInt(PromotionDeviceID);
        dest.writeString(PromotionDeviceText);
        dest.writeInt(ReturnStatus);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Device> CREATOR = new Creator<Device>() {
        @Override
        public Device createFromParcel(Parcel in) {
            return new Device(in);
        }

        @Override
        public Device[] newArray(int size) {
            return new Device[size];
        }
    };

    public int getCustomerStatus() {
        return CustomerStatus;
    }

    public void setCustomerStatus(int customerStatus) {
        CustomerStatus = customerStatus;
    }

    public int getEFStatus() {
        return EFStatus;
    }

    public void setEFStatus(int EFStatus) {
        this.EFStatus = EFStatus;
    }

    public int getNumMax() {
        return NumMax;
    }

    public void setNumMax(int numMax) {
        NumMax = numMax;
    }

    public int getNumMin() {
        return NumMin;
    }

    public void setNumMin(int numMin) {
        NumMin = numMin;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getPriceID() {
        return PriceID;
    }

    public void setPriceID(int priceID) {
        PriceID = priceID;
    }

    public String getPriceName() {
        return PriceName;
    }

    public void setPriceName(String priceName) {
        PriceName = priceName;
    }

    public String getPriceText() {
        return PriceText;
    }

    public void setPriceText(String priceText) {
        PriceText = priceText;
    }

    public int getServiceCode() {
        return ServiceCode;
    }

    public void setServiceCode(int serviceCode) {
        ServiceCode = serviceCode;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(int deviceID) {
        DeviceID = deviceID;
    }

    public String getDeviceName() {
        return DeviceName;
    }

    public void setDeviceName(String deviceName) {
        DeviceName = deviceName;
    }

    public int getPromotionDeviceID() {
        return PromotionDeviceID;
    }

    public void setPromotionDeviceID(int promotionDeviceID) {
        PromotionDeviceID = promotionDeviceID;
    }

    public String getPromotionDeviceText() {
        return PromotionDeviceText;
    }

    public void setPromotionDeviceText(String promotionDeviceText) {
        PromotionDeviceText = promotionDeviceText;
    }

    public int getReturnStatus() {
        return ReturnStatus;
    }

    public void setReturnStatus(int returnStatus) {
        ReturnStatus = returnStatus;
    }

    public JSONObject toJSONObjectRegister() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("DeviceID", getDeviceID());
            jsonObj.put("DeviceName", getDeviceName());
//            jsonObj.put("DevicePrice", getPrice());
            jsonObj.put("Price", getPrice());
            jsonObj.put("Number", getNumber());
            jsonObj.put("PriceID", getPriceID());
            jsonObj.put("PriceText", getPriceText());
            jsonObj.put("EFStatus", getEFStatus());
            jsonObj.put("NumMin", getNumMin());
            jsonObj.put("NumMax", getNumMax());
            jsonObj.put("ReturnStatus", getReturnStatus());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    public JSONObject toJSONObjectRegisterV2() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("DeviceID", getDeviceID());
            jsonObj.put("DeviceName", getDeviceName());
            jsonObj.put("EFStatus", getEFStatus());
            jsonObj.put("Number", getNumber());
            jsonObj.put("Price", getPrice());
            jsonObj.put("PriceID", getPriceID());
            jsonObj.put("PriceText", getPriceText());
            jsonObj.put("PromotionDeviceID", getPromotionDeviceID());
            jsonObj.put("PromotionDeviceText", getPromotionDeviceText());
            jsonObj.put("ServiceCode", getServiceCode());
            jsonObj.put("NumMin", getNumMin());
            jsonObj.put("NumMax", getNumMax());
            jsonObj.put("ReturnStatus", getReturnStatus());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    public JSONObject toJsonObjectTotal() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("DeviceID", getDeviceID());
            jsonObject.put("DeviceName", getDeviceName());
            jsonObject.put("Number", getNumber());
            jsonObject.put("Price", getPrice());
            jsonObject.put("PriceID", getPriceID());
            jsonObject.put("EFStatus", getEFStatus());
            jsonObject.put("ServiceCode", getServiceCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}