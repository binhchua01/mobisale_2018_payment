package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;

/**
 * Created by HCM.TUANTT14 on 3/20/2018.
 *
 * update by haulc3 on 2019-12-10
 */

public class TestNotification implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public TestNotification(Context mContext) {
        this.mContext = mContext;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Title", "TEST NOTIFICATION");
            jsonObject.put("Message", "SALE TEST NOTIFICATION");
            jsonObject.put("Category", "TEST");
            jsonObject.put("Model", "");
            jsonObject.put("EndPointType", "2");
            jsonObject.put("EndpointAddress", DeviceInfo.DEVICE_IMEI);
            jsonObject.put("KeysJSON", "");
            jsonObject.put("Source", "0");
            jsonObject.put("Type", "0");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(
                R.string.msg_pd_push_test_notification);
        String TEST_PUSH_NOTIFICATION = "PushNotificationTest";
        CallServiceTask service = new CallServiceTask(mContext, TEST_PUSH_NOTIFICATION, jsonObject,
                Services.JSON_POST_OBJECT, message, TestNotification.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj.has(Constants.RESPONSE_RESULT)) {
                    try {
                        jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                        String TAG_ERROR_CODE = "ErrorCode";
                        if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                            String TAG_LIST_OBJECT = "ListObject";
                            if (jsObj.has(TAG_LIST_OBJECT)) {
                                JSONArray jsonObject = jsObj.getJSONArray(TAG_LIST_OBJECT);
                                JSONObject resultJson = (JSONObject) jsonObject.get(0);
                                String TAG_RESULT_ID = "ResultID";
                                if (resultJson.has(TAG_RESULT_ID)) {
                                    if (resultJson.getInt(TAG_RESULT_ID) > 0) {
                                        Common.alertDialog("Gửi thông báo thành công.", mContext);
                                    }
                                }
                            }
                        } else {
                            String message = "Lỗi ServiceType.";
                            String TAG_ERROR = "Error";
                            if (jsObj.has(TAG_ERROR))
                                message = jsObj.getString(TAG_ERROR);
                            Common.alertDialog(message, mContext);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
