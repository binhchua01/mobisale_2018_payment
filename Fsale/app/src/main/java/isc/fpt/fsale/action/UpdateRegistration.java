package isc.fpt.fsale.action;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import org.json.JSONArray;
import org.json.JSONObject;
import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//API cập nhật pdk vào hệ thống
public class UpdateRegistration implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private int regID = 0;

    UpdateRegistration(Context mContext, JSONObject jsonObjParam) {
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.msg_pd_update);
        String UPDATE_REGISTRATION = "UpdateRegistrationV2";
        CallServiceTask service = new CallServiceTask(mContext, UPDATE_REGISTRATION, jsonObjParam,
                Services.JSON_POST_OBJECT, message, UpdateRegistration.this);
        service.execute();
    }

    public void handleUpdateRegistration(String json) {
        if (Common.jsonObjectValidate(json)) {
            try {
                JSONObject jsObj = getJsonObject(json);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    String result = "";
                    String TAG_ERROR_CODE = "ErrorCode";
                    if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {//Check service Error
                        String TAG_RESULT_LIST = "ListObject";
                        JSONArray resultArray = jsObj.getJSONArray(TAG_RESULT_LIST);
                        for (int i = 0; i < resultArray.length(); i++) {
                            JSONObject item = resultArray.getJSONObject(i);
                            String TAG_RESULT_ID = "ResultID";
                            if (item.has(TAG_RESULT_ID))
                                regID = Integer.parseInt(item.getString(TAG_RESULT_ID));
                            String TAG_RESULT = "Result";
                            if (item.has(TAG_RESULT))
                                result = item.getString(TAG_RESULT);
                        }
                        if (regID > 0) {
                            new AlertDialog.Builder(mContext)
                                    .setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(result)
                                    .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            new GetRegistrationDetail(mContext, Constants.USERNAME, regID);
                                            dialog.cancel();
                                        }
                                    })
                                    .setCancelable(false).create().show();
                        } else {
                            Common.alertDialog(result, mContext);
                        }
                    } else {
                        String TAG_ERROR = "Error";
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else{
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            return JSONParsing.getJsonObj(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onTaskComplete(String result) {
        handleUpdateRegistration(result);
    }
}
