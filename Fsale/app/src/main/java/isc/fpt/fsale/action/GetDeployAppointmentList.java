package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.DeployAppointmentListActivity;

import isc.fpt.fsale.model.DeployAppointmentModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

/*
 * @Description: 	Get list value contract deposit
 * @author: 		DuHK
 * @create date: 	06/08/2015
 * API lấy danh sách hẹn triển khai
 */

public class GetDeployAppointmentList implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public GetDeployAppointmentList(Context mContext, String UserName, String RegCode) {
        this.mContext = mContext;
        String[] paramNames = {"UserName", "RegCode"};
        String[] paramValues = {UserName, RegCode};
        String message = "Đang lấy dữ liệu...";
        String METHOD_NAME = "GetDeployAppointmentList";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames,
                paramValues, Services.JSON_POST, message, GetDeployAppointmentList.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            boolean isError = false;
            List<DeployAppointmentModel> lst = null;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<DeployAppointmentModel> resultObject =
                        new WSObjectsModel<>(jsObj, DeployAppointmentModel.class);
                if (resultObject.getErrorCode() == 0) {//OK not Error
                    lst = resultObject.getListObject();
                } else {//ServiceType Error
                    isError = true;
                    Common.alertDialog(resultObject.getError(), mContext);
                }
                if (!isError)
                    loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadData(List<DeployAppointmentModel> lst) {
        if (mContext.getClass().getSimpleName().equals(DeployAppointmentListActivity.class.getSimpleName())) {
            DeployAppointmentListActivity activity = (DeployAppointmentListActivity) mContext;
            activity.LoadData(lst);
        }
    }
}