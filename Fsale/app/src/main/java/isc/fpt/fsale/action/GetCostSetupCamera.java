package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.SellMoreCameraModel;
import isc.fpt.fsale.ui.fpt_camera.model.RegisterFptCameraModel;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.ui.fpt_camera.fragment.FptCameraService;
import isc.fpt.fsale.ui.fragment.FragmentFptCamera;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetCostSetupCamera implements AsyncTaskCompleteListener<String> {
    private FptCameraService mFptCameraService;
    private FragmentFptCamera mFragmentFptCamera;
    private Context mContext;

    //bán mới
    public GetCostSetupCamera(Context mContext, FptCameraService mFptCameraService, RegisterFptCameraModel object) {
        this.mFptCameraService = mFptCameraService;
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.msg_progress_get_price_set_up_camera);
        String GET_COST_SETUP_CAMERA = "GetCostSetupCamera";
        CallServiceTask service = new CallServiceTask(mContext, GET_COST_SETUP_CAMERA,
                object.toJsonObjectCostSetupCamera(), Services.JSON_POST_OBJECT, message, GetCostSetupCamera.this);
        service.execute();
    }

    //bán thêm
    public GetCostSetupCamera(Context mContext, FragmentFptCamera mFragmentFptCamera, SellMoreCameraModel object) {
        this.mFragmentFptCamera = mFragmentFptCamera;
        this.mContext = mContext;
        String message = mContext.getResources().getString(R.string.msg_progress_get_price_set_up_camera);
        String GET_COST_SETUP_CAMERA = "GetCostSetupCamera";
        CallServiceTask service = new CallServiceTask(mContext, GET_COST_SETUP_CAMERA, object.toJsonObjectCostSetupCamera(),
                Services.JSON_POST_OBJECT, message, GetCostSetupCamera.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                WSObjectsModel<SetupDetail> resultObject = new WSObjectsModel<>(jsObj, SetupDetail.class);
                List<SetupDetail> lst = resultObject.getListObject();
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), mContext);
                } else {
                    //callback to activity
                    if (lst.size() == 0) return;
                    if (mFragmentFptCamera != null) {
                        mFragmentFptCamera.loadInfoSetupDetail(lst);
                    } else {
                        mFptCameraService.loadInfoSetupDetail(lst);
                    }
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
