package isc.fpt.fsale.action;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.device_price_list.DevicePrice;
import isc.fpt.fsale.activity.device_price_list.DevicePriceListActivity;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.ui.fpt_camera.camera_price_list.DevicePriceListCameraActivity;
import isc.fpt.fsale.ui.fpt_camera.fragment.FptCameraService;
import isc.fpt.fsale.ui.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 18,October,2019
 */
public class GetListPriceEquipmentCamera implements AsyncTaskCompleteListener<String> {
    private Context mContext;

    public GetListPriceEquipmentCamera(Context mContext, Device mDevice, String mClassName) {
        this.mContext = mContext;
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(mDevice.getDeviceID());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("LocationID", Constants.LOCATION_ID);
            jsonObject.put("CodeIDs", jsonArray);
            jsonObject.put("CustomerStatus",
                    mClassName.equals(FptCameraService.class.getSimpleName()) ? 0 : 1 /* 0 - bán mới , 1  - bán thêm */);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = mContext.getResources().getString(R.string.msg_pd_get_device_price_list);
        String GET_DEVICE_PRICE_LIST = "GetListPriceEquipmentCamera";
        CallServiceTask service = new CallServiceTask(mContext, GET_DEVICE_PRICE_LIST,
                jsonObject, Services.JSON_POST_OBJECT, message, GetListPriceEquipmentCamera.this);
        service.execute();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(String result) {
        try {
            List<DevicePrice> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                String LIST_OBJECT = "ListObject";
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                if (jsObj.getInt("ErrorCode") == 0) {// OK not Error
                    JSONArray jsonArray = jsObj.getJSONArray(LIST_OBJECT);
                    for(int i = 0; i < jsonArray.length(); i++){
                        lst.add(new Gson().fromJson(jsonArray.get(i).toString(), DevicePrice.class));
                    }
                } else {
                    Common.alertDialog(jsObj.getString("Error"), mContext);
                }

                if (lst.size() > 0) {
                    if (mContext != null){
                        ((DevicePriceListCameraActivity) mContext).loadDeviceList(lst);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
