package isc.fpt.fsale.model;

import java.lang.reflect.Field;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class ReportSurveyManagerTotalModel  implements Parcelable {
	
	private String RowNumber;
	private String SaleName;
	private String TotalObject;
	private String CabPlus;
	private String ObjectCabPlus;
	private String CabSub;
	private String ObjectCabSub;
	private Integer TotalPage;
	private Integer CurrentPage;
	private Integer TotalRow;
	
	public String getRowNumber() {
		return RowNumber;
	}

	public void setRowNumber(String rowNumber) {
		this.RowNumber = rowNumber;
	}

	public String getSaleName() {
		return SaleName;
	}

	public void setSaleName(String SaleName) {
		this.SaleName = SaleName;
	}
	
	public String getTotalObject() {
		return TotalObject;
	}

	public void setTotalObject(String TotalObject) {
		this.TotalObject = TotalObject;
	}
	
	public String getCabPlus() {
		return CabPlus;
	}

	public void setCabPlus(String CabPlus) {
		this.CabPlus = CabPlus;
	}
	
	public String getObjectCabPlus() {
		return ObjectCabPlus;
	}

	public void setObjectCabPlus(String ObjectCabPlus) {
		this.ObjectCabPlus = ObjectCabPlus;
	}
	
	public String getCabSub() {
		return CabSub;
	}

	public void setCabSub(String CabSub) {
		this.CabSub = CabSub;
	}
	
	public String getObjectCabSub() {
		return ObjectCabSub;
	}

	public void setObjectCabSub(String ObjectCabSub) {
		this.ObjectCabSub = ObjectCabSub;
	}
	
	public int getTotalPage() {
		return TotalPage;
	}

	public void setTotalPage(int totalPage) {
		TotalPage = totalPage;
	}

	public int getCurrentPage() {
		return CurrentPage;
	}

	public void setCurrentPage(int currentPage) {
		CurrentPage = currentPage;
	}

	public int getTotalRow() {
		return TotalRow;
	}

	public void setTotalRow(int totalRow) {
		TotalRow = totalRow;
	}
	
	public ReportSurveyManagerTotalModel(String RowNumber,
			String SaleName,
			String TotalObject,
			String CabPlus,
			String ObjectCabPlus,
			String CabSub,
			String ObjectCabSub,int _TotalPage,int _CurrentPage,int _TotalRow)
	{
		this.RowNumber = RowNumber;
		this.SaleName = SaleName;
		this.TotalObject = TotalObject;
		this.CabPlus = CabPlus;
		this.ObjectCabPlus = ObjectCabPlus;
		this.CabSub = CabSub;
		this.ObjectCabSub = ObjectCabSub;
		this.TotalPage=_TotalPage;
		this.CurrentPage=_CurrentPage;
		this.TotalRow=_TotalRow;
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel pc, int flags) {
		// TODO Auto-generated method stub
		pc.writeString(RowNumber);
		pc.writeString(SaleName);
		pc.writeString(TotalObject);
		pc.writeString(CabPlus);
		pc.writeString(ObjectCabPlus);
		pc.writeString(CabSub);
		pc.writeString(ObjectCabSub);
		pc.writeInt(TotalPage);
		pc.writeInt(CurrentPage);
		pc.writeInt(TotalRow);
		
	}
	
	public ReportSurveyManagerTotalModel(Parcel source){
		
		RowNumber = source.readString();
		SaleName = source.readString();
		TotalObject = source.readString();
		CabPlus = source.readString();
		ObjectCabPlus = source.readString();
		CabSub = source.readString();
		ObjectCabSub = source.readString();
		TotalPage = source.readInt();
		CurrentPage = source.readInt();
		TotalRow = source.readInt();
	}
	
	/** Static field used to regenerate object, individually or as arrays */
	public static final Creator<ReportSurveyManagerTotalModel> CREATOR = new Creator<ReportSurveyManagerTotalModel>() {
		@Override     
		public ReportSurveyManagerTotalModel createFromParcel(Parcel source) {
	             return new ReportSurveyManagerTotalModel(source);
		}
		@Override
		public ReportSurveyManagerTotalModel[] newArray(int size) {
             return new ReportSurveyManagerTotalModel[size];
        }
	};
	
	public ReportSurveyManagerTotalModel(){
		
	}
	 public static ReportSurveyManagerTotalModel Parse(JSONObject json){  
		 ReportSurveyManagerTotalModel object = new ReportSurveyManagerTotalModel();
	    	try {
	    		Field[] fields = object.getClass().getDeclaredFields();
	    		 for (Field field : fields) {
	    			 String propertiesName = field.getName();
	    			 if(json.has(propertiesName)){
	    				 field.setAccessible(true);
	    				 if(field.getType().getSimpleName().equals("String"))    					
	    					 field.set(object, json.getString(propertiesName));    					 
	    				 else if(field.getType().getSimpleName().equals("Integer"))
	    					 field.set(object, json.getInt(propertiesName));
	    			 }
	    		 }
	    		 return object;
			} catch (Exception e) {
				//String message = e.getMessage();

				e.printStackTrace();
			}
	    	return null;
	    }
	    //////////////////////////////////////////////
	   
	    
	    @Override
	    public String toString() {
	    	 StringBuilder sb = new StringBuilder();
	    	 Field[] fields = getClass().getDeclaredFields();    	
	    	  for (Field f : fields) {
	    	    sb.append(f.getName());
	    	    sb.append(": ");
	    	    try {
	    	    	sb.append("[");
					sb.append(f.get(this));
					sb.append("]");
				} catch (Exception e) {
					// TODO: handle exception

				}
	    	    
	    	    sb.append(", ");
	    	    sb.append('\n');
	    	  }
	    	  return sb.toString();
	    }

}
