package isc.fpt.fsale.ui.fragment;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

public class InvestiageDialog extends DialogFragment{

	
	private ImageButton btnClose;	
	private Context mContext;
	private RegistrationDetailModel modelDetail;
	private TextView txtRegCode, txtCustomer, txtAddress;//,txtOutdoor,txtListPoint,txtIndoor;
	private Spinner spCableOutDoor,spCableInDoor,spModem,spMap;
	private KeyValuePairAdapter adapter;

	public InvestiageDialog(){}

	@SuppressLint("ValidFragment")
	public InvestiageDialog(Context mContext,RegistrationDetailModel modelDetail) {
		// TODO Auto-generated constructor stub		
		this.modelDetail = modelDetail;
		this.mContext = mContext;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	    // Inflate the layout to use as dialog or embedded fragment
		  
		  View view = inflater.inflate(R.layout.dialog_investigate, container);
		  btnClose = (ImageButton)view.findViewById(R.id.btn_close);
		  txtRegCode = (TextView)view.findViewById(R.id.txt_registration_id);
		  txtCustomer = (TextView)view.findViewById(R.id.txt_customer_name);
		  txtAddress = (TextView)view.findViewById(R.id.txt_address);		  
		 /* txtOutdoor = (TextView)view.findViewById(R.id.txt_outdoor_long);
		  txtIndoor = (TextView)view.findViewById(R.id.txt_indoor_long);
		  txtListPoint = (TextView)view.findViewById(R.id.txt_list_point);*/
		  spModem = (Spinner)view.findViewById(R.id.sp_modem);
		  spCableOutDoor = (Spinner)view.findViewById(R.id.sp_outdoor_cable);
		  spCableInDoor = (Spinner)view.findViewById(R.id.sp_indoor_cable);
		  spMap = (Spinner)view.findViewById(R.id.sp_map_code);
		  
    	 // txtRegCode.setText(modelDetail)
		  txtCustomer.setText(modelDetail.getContact());
		  txtAddress.setText(modelDetail.getAddress());
		  txtRegCode.setText(modelDetail.getRegCode());
		  
		  setCableList();	
		  setModemList();
		  setMapCodeList();
				  
		  btnClose.setOnClickListener(new View.OnClickListener() {  			
				@Override
				public void onClick(View v) {
					openGallery();
					//getDialog().dismiss();
					//Read more at http://www.airpair.com/android/android-image-picker-select-gallery-images#KUFGY3qSI1FAKWiq.99
				}
    	  });
		  return view;
	}
	
	/** The system calls this only when creating the layout in a dialog. */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {	   
	    Dialog dialog = super.onCreateDialog(savedInstanceState);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    return dialog;
	}
	
	
	
	public void setModemList(){
		ArrayList<KeyValuePairModel> lstModem = new ArrayList<>();
		lstModem.add(new KeyValuePairModel("0", "Không lấy modem"));
		lstModem.add(new KeyValuePairModel("1", "Modem thuê"));
		lstModem.add(new KeyValuePairModel("2", "Modem tặng"));
		lstModem.add(new KeyValuePairModel("3", "Modem bán"));
		
		adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstModem, Gravity.LEFT);
		spModem.setAdapter(adapter);
	}

	public void setCableList(){
		ArrayList<KeyValuePairModel> lstCable = new ArrayList<>();
		lstCable.add(new KeyValuePairModel("0", "[ Vui lòng chọn loại ]"));
		lstCable.add(new KeyValuePairModel("1", "0.5 mm"));
		lstCable.add(new KeyValuePairModel("2", "FO 1 core"));
		lstCable.add(new KeyValuePairModel("3", "FO 2 core"));
		lstCable.add(new KeyValuePairModel("4", "FO 4 core"));
		lstCable.add(new KeyValuePairModel("5", "FO 8 core"));
		lstCable.add(new KeyValuePairModel("6", "FO 16 core"));
		lstCable.add(new KeyValuePairModel("7", "RG6"));
		
		adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstCable, Gravity.LEFT);
		spCableInDoor.setAdapter(adapter);
		spCableOutDoor.setAdapter(adapter);
	}
	
	public void setMapCodeList(){
		ArrayList<KeyValuePairModel> lstMap = new ArrayList<>();
		lstMap.add(new KeyValuePairModel("M001: M001", "M001: M001"));
		lstMap.add(new KeyValuePairModel("M002: M002", "M002: M002"));
		lstMap.add(new KeyValuePairModel("M003: M003", "M003: M003"));
		lstMap.add(new KeyValuePairModel("M004: M004", "M004: M004"));
		lstMap.add(new KeyValuePairModel("M005: M005", "M005: M005"));
		lstMap.add(new KeyValuePairModel("M006: M006", "M006: M006"));
		lstMap.add(new KeyValuePairModel("M007: M007", "M007: M007"));
		lstMap.add(new KeyValuePairModel("M008: M008", "M008: M008"));
		lstMap.add(new KeyValuePairModel("M009: M009", "M009: M009"));
		lstMap.add(new KeyValuePairModel("M0010: M0010", "M0010: M0010"));
		lstMap.add(new KeyValuePairModel("M0011: M0011", "M0011: M0011"));
		lstMap.add(new KeyValuePairModel("M0012: M0012", "M0012: M0012"));		
		
		adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstMap, Gravity.LEFT);
		spMap.setAdapter(adapter);
	}
	
    private void openGallery()
    {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }
    
    

	

}
