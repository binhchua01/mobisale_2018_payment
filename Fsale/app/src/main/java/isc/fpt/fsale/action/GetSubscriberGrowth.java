package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.SubscriberGrowthFragment;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.support.v4.app.FragmentManager;

public class GetSubscriberGrowth implements AsyncTaskCompleteListener<String>{
	
	private Context mContext;
	private FragmentManager fm;
	
	private final String GET_SUBSCRIBERGROWTH = "GetSubscriberGrowth";	
	private final String TAG_SUBSCRIBERGROWTH_RESULT = "GetSubscriberGrowthResult";
	private final String TAG_ERROR = "ErrorService";
	private final String TAG_COUNT_OBJECT = "CountObject";	
	private final String TAG_NORM = "Norm";
	private final String TAG_TURN_OVER = "Turnover";
	private final String TAG_TURN_OVER_OBJECT = "TurnoverObject";
	
	
	
	public GetSubscriberGrowth(Context mContext, String userName,FragmentManager fm)  {
		// TODO Auto-generated constructor stub
		this.fm = fm;
		this.mContext = mContext;
		String params = Services.getParams(new String[]{userName});
		//call service
		String message = mContext.getResources().getString(R.string.msg_pd_get_subscriber_growth);
		CallServiceTask service = new CallServiceTask(mContext,GET_SUBSCRIBERGROWTH, params, Services.GET, message, GetSubscriberGrowth.this);
		service.execute();
	}
	
	public void handleSubcriberGrowth(String json)
	{
		if(json != null && Common.jsonObjectValidate(json)){			
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			bindData(jsObj);			
		}
		else Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	}
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {
				jsArr = jsObj.getJSONArray(TAG_SUBSCRIBERGROWTH_RESULT);				
				int l = jsArr.length();
				if(l>0)
				{					
					String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
					if(error.equals("null")){
						JSONObject iObj = jsArr.getJSONObject(0);
						String countObject = iObj.getString(TAG_COUNT_OBJECT);
						String Norm = iObj.getString(TAG_NORM);
						String Turnover = iObj.getString(TAG_TURN_OVER);
						String TurnoverObject = iObj.getString(TAG_TURN_OVER_OBJECT);
						SubscriberGrowthFragment dialog = new SubscriberGrowthFragment(mContext,countObject,Norm,Turnover,TurnoverObject);						
						Common.showFragmentDialog(fm, dialog, "fragment_about_us_dialog");
						
					}
					else{
						Common.alertDialog("Lỗi WS:" + error, mContext);					
					}										
				}
				else
				{
					Common.alertDialog("Không tìm thấy dữ liệu", mContext);
				}
					
		} catch (Exception e) {

			e.printStackTrace();
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
					+"-" + GET_SUBSCRIBERGROWTH, mContext);
		}
	 }

	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub		
		handleSubcriberGrowth(result);
	}
	

}
