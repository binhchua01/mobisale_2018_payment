package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by haulc3 on 20,July,2019
 */
public class ServiceListExtraOtt implements Parcelable {
    @SerializedName("PromoCommand")
    @Expose
    private PromoCommand promoCommand;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;

    public ServiceListExtraOtt(PromoCommand promoCommand, Integer quantity) {
        this.promoCommand = promoCommand;
        this.quantity = quantity;
    }

    public ServiceListExtraOtt(Parcel in) {
        promoCommand = in.readParcelable(PromoCommand.class.getClassLoader());
        if (in.readByte() == 0) {
            quantity = null;
        } else {
            quantity = in.readInt();
        }
    }

    public static final Creator<ServiceListExtraOtt> CREATOR = new Creator<ServiceListExtraOtt>() {
        @Override
        public ServiceListExtraOtt createFromParcel(Parcel in) {
            return new ServiceListExtraOtt(in);
        }

        @Override
        public ServiceListExtraOtt[] newArray(int size) {
            return new ServiceListExtraOtt[size];
        }
    };

    public PromoCommand getPromoCommand() {
        return promoCommand;
    }

    public void setPromoCommand(PromoCommand promoCommand) {
        this.promoCommand = promoCommand;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(promoCommand, flags);
        if (quantity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(quantity);
        }
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("PromoCommand", getPromoCommand().toJsonObject());
            jsonObject.put("Quantity", getQuantity());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
