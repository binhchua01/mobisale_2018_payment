package isc.fpt.fsale.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckVersion;
import isc.fpt.fsale.action.Login;
import isc.fpt.fsale.ui.fragment.AboutUsDialog;
import isc.fpt.fsale.ui.fragment.HelpDialog;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.EnCrypt;
import isc.fpt.fsale.utils.MyApp;

import static isc.fpt.fsale.action.CheckVersion.dialogRequiredUpdate;

//màn hình ĐĂNG NHẬP
public class LoginActivity extends FragmentActivity implements ActivityCompat.OnRequestPermissionsResultCallback {
    private Button btnLogin;
    private EditText txtUserName, txtPass;
    public Context mContext;
    private FragmentManager fm;
    private CheckBox cbShowPwd;

    // hàm khởi tạo giao diện màn hình login
    private void InitView() {
        Common.setupUI(this, this.findViewById(android.R.id.content));
        fm = getSupportFragmentManager();
        txtUserName = (EditText) this.findViewById(R.id.txt_username);
        txtPass = (EditText) this.findViewById(R.id.txt_password);
        //control Hiển thị mật khẩu
        cbShowPwd = (CheckBox) findViewById(R.id.cb_show_hide_password);
        cbShowPwd.setChecked(false);
        //control ĐĂNG NHẬP
        btnLogin = (Button) this.findViewById(R.id.btn_login);
        //control Thông tin
        TextView tileAbout = (TextView) this.findViewById(R.id.tile_about);
        //control Hướng dẫn
        TextView tileHelp = (TextView) this.findViewById(R.id.tile_help);
        if (!Common.checkStatusNotification(mContext)) {
            Common.alertDialog("Ứng dụng đang bị khóa chức năng thông báo," +
                    " bạn cần vào phần cài đặt, đến phần ứng dụng để mở chức năng thông báo.", mContext);
        }
        //khởi tạo sự kiện nhấn nút ĐĂNG NHẬP
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideSoftKeyboard(LoginActivity.this);
                try {
                    if ((!Common.isEmpty(txtUserName)) && (!Common.isEmpty(txtPass))) {
                        String userName = txtUserName.getText().toString().trim();
                        String pass = txtPass.getText().toString().trim();
                        pass = EnCrypt.SHA512Encrypt(pass);
                        String simImei = DeviceInfo.SIM_IMEI;
                        String deviceImei = DeviceInfo.DEVICE_IMEI;
                        //Kết nối API đăng nhập hệ thống
                        new Login(mContext, new String[]{userName, pass, simImei, deviceImei});
                    } else {
                        Common.alertDialog(getResources().getString(R.string.msg_username_pass_blank), mContext);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //khởi tạo sự kiện nhấn nút Thông tin
        tileAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // kiểm tra context không null
                    if (mContext != null) {
                        AboutUsDialog aboutDialog = new AboutUsDialog(mContext);
                        Common.showFragmentDialog(fm, aboutDialog, "fragment_about_us_dialog");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        // khởi tạo sự kiện nhấn nút Hướng dẫn
        tileHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    HelpDialog helpDialog = new HelpDialog(mContext);
                    Common.showFragmentDialog(fm, helpDialog, "fragment_about_us_dialog");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        //khởi tạo sự kiện hiện mật khẩu từ checkbox Hiển thị mật khẩu
        cbShowPwd.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (!cbShowPwd.isChecked()) {
                    // show password
                    txtPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    txtPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        txtPass.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnLogin.performClick();
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_welcome_activity));
        setContentView(R.layout.login_activity);
        this.mContext = LoginActivity.this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
                    || mContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
            } else {
                InitView();
                getDataFromIntent();
                checkVersion();
            }
        } else {
            InitView();
            getDataFromIntent();
            checkVersion();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void checkVersion() {
        Bundle bun = getIntent().getExtras();
        boolean isLogOut = ((MyApp) this.getApplication()).getIsLogOut();
        if (!isLogOut) {
            //khởi tạo đối tượng thông tin thiết bị
            new DeviceInfo(mContext);
            //get current version
            String sCurrentVersion = Common.GetAppVersion(mContext);
            Constants.DEVICE_IMEI = DeviceInfo.DEVICE_IMEI;
            // Kết nối API kiểm tra version
            String APP_TYPE = "6";
            String ANDROID_PLATFORM = "1";
            new CheckVersion(this, new String[]{DeviceInfo.DEVICE_IMEI, sCurrentVersion,
                    APP_TYPE, ANDROID_PLATFORM}, this);
        } else {
            ((MyApp) this.getApplication()).setIsLogIn(false);
            txtUserName.setText(Constants.USERNAME);
            if (bun != null && bun.getBoolean("EXIT")) {
                finish();
            }
        }
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            String headerError = intent.getStringExtra("HEADER_ERROR");
            if (headerError != null && !headerError.trim().equals("")) {
                Common.alertDialog(headerError, this);
                try {
                    ArrayList<CallServiceTask> threads = ((MyApp) getApplication()).getSyncThreads();
                    if (threads != null) {
                        for (CallServiceTask callServiceTask : threads) {
                            if (callServiceTask != null)
                                callServiceTask.cancel(true);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //sự kiên bấm nút back cứng từ hệ thống
    @Override
    public void onBackPressed() {
        Common.quitApp(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);

        //check dialog update is showing
        if (dialogRequiredUpdate != null && dialogRequiredUpdate.isShowing()) {
            dialogRequiredUpdate.dismiss();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 2) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                InitView();
                getDataFromIntent();
                checkVersion();
            } else {
                AlertDialog.Builder builder;
                Dialog dialog;
                builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Không chạy được chương trình do không đủ quyền!")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        System.exit(2);
                                    }
                                });
                dialog = builder.create();
                dialog.show();
            }
        }
    }

    public void bindUsername(String username) {
        txtUserName.setText(username);
    }
}