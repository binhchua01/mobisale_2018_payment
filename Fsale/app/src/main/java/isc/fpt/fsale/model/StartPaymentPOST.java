package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

public class StartPaymentPOST implements Parcelable {
    //Declares variables
    private String saleMan;
    private String regCode;
    private String contract;
    private int paidType;
    private int total;

    //Constructor
    public StartPaymentPOST(){}

    protected StartPaymentPOST(Parcel in) {
        saleMan = in.readString();
        regCode = in.readString();
        contract = in.readString();
        paidType = in.readInt();
        total = in.readInt();
    }

    public static final Creator<StartPaymentPOST> CREATOR = new Creator<StartPaymentPOST>() {
        @Override
        public StartPaymentPOST createFromParcel(Parcel in) {
            return new StartPaymentPOST(in);
        }

        @Override
        public StartPaymentPOST[] newArray(int size) {
            return new StartPaymentPOST[size];
        }
    };

    //Getters and Setters
    public String getSaleMan() {
        return saleMan;
    }

    public void setSaleMan(String saleMan) {
        this.saleMan = saleMan;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public int getPaidType() {
        return paidType;
    }

    public void setPaidType(int paidType) {
        this.paidType = paidType;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(saleMan);
        parcel.writeString(regCode);
        parcel.writeString(contract);
        parcel.writeInt(paidType);
        parcel.writeInt(total);
    }
}
