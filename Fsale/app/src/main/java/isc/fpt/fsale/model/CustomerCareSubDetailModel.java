package isc.fpt.fsale.model;

public class CustomerCareSubDetailModel {
	private String code;
	private String csat;
	private String note;
	private String confirm_code;
	private String time;
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	private String point;
	
	public CustomerCareSubDetailModel(){
		
	}
	
	public CustomerCareSubDetailModel(String code, String csat, String note, String confirm_code, String time) {
		super();
		this.code = code;
		this.csat = csat;
		this.note = note;
		this.confirm_code = confirm_code;
		this.time = time;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCsat() {
		return csat;
	}
	public void setCsat(String csat) {
		this.csat = csat;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getConfirm_code() {
		return confirm_code;
	}
	public void setConfirm_code(String confirm_code) {
		this.confirm_code = confirm_code;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
}
