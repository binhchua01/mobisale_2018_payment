package isc.fpt.fsale.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetIPTVPrice;
import isc.fpt.fsale.action.GetIPTVTotal;
import isc.fpt.fsale.action.GetPromotionIPTVList;
import isc.fpt.fsale.activity.PromotionIPTVFilterActivity;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.IPTVPriceModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.PromotionIPTVModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;

import static isc.fpt.fsale.activity.PromotionIPTVFilterActivity.TAG_PROMOTION_TYPE_BOX;

public class FragmentRegisterContractIPTV extends Fragment implements OnCheckedChangeListener {
    private Context mContext;

    private RadioButton radRequestSetup, radRequestDrillWall;
    public Spinner spFormDeployment, spPackage;

    private LinearLayout frmExtra, frmExtraTitle, frmDacsacHD;
    public TextView lblBoxCount;
    private TextView lblPLCCount, lblSTBReturn, lblChargeTimes, lblAmountFimStandard,
            lblAmountFimPlus, lblAmountFimHot, lblAmountKPlus, lblAmountDacSacHD, lblAmountVTVCab,
            lblFimStandardChargeTimes, lblFimStandardMonthCount, lblFimPlusChargeTimes, lblFimPlusMonthCount,
            lblFimHotChargeTimes, lblFimHotMonthCount, lblKPlusChargeTimes, lblKPlusMonthCount,
            lblDacSacHDChargeTimes, lblDacSacHDMonthCount, lblVTVCabChargeTimes, lblVTVCabMonthCount,
            tvFoxy2ChargeTime, tvFoxy2PrepaidMonth, tvFoxy4ChargeTime, tvFoxy4PrepaidMonth, tvFoxy2Amount, tvFoxy4Amount;
    public TextView lblAmountPromotionFirstBox, lblAmountPromotionBoxOrder;
    public EditText txtPromotionBox1, txtPromotionBox2;
    public ArrayList<PromotionIPTVModel> mListPromotionIPTVBox1;
    public ArrayList<PromotionIPTVModel> mListPromotionIPTVBox2;
    public PromotionIPTVModel promotionIPTVModelBox1, promotionIPTVModelBox2;
    private ImageView imgBoxPlus, imgBoxLess, imgPLCPlus, imgPLCLess, imgSTBReturnPlus, imgSTBReturnLess,
            imgChargeTimesPlus, imgChargeTimesLess, imgNavigationExtra, imgFimStandardChargeTimesPlus,
            imgFimStandardChargeTimesLess, imgFimStandardMonthCountPlus, imgFimStandardMonthCountLess,
            imgFimPlusChargeTimesPlus, imgFimPlusChargeTimesLess, imgFimPlusMonthCountPlus, imgFimPlusMonthCountLess,
            imgFimHotChargeTimesPlus, imgFimHotChargeTimesLess, imgFimHotMonthCountPlus, imgFimHotMonthCountLess,
            imgKPlusChargeTimesPlus, imgKPlusChargeTimesLess, imgKPlusMonthCountPlus, imgKPlusMonthCountLess,
            imgDacSacHDChargeTimesPlus, imgDacSacHDChargeTimesLess, imgDacSacHDMonthCountPlus, imgDacSacHDMonthCountLess,
            imgVTVCabChargeTimesPlus, imgVTVCabChargeTimesLess, imgVTVCabMonthCountPlus, imgVTVCabMonthCountLess,
            imgFoxy2ChargeTimeLess, imgFoxy2ChargeTimePlus, imgFoxy2PrepaidMonthLess, imgFoxy2PrepaidMonthPlus,
            imgFoxy4ChargeTimeLess, imgFoxy4ChargeTimePlus, imgFoxy4PrepaidMonthLess, imgFoxy4PrepaidMonthPlus;
    private CheckBox chbFimPlus, chbFimHot, chbKPlus, chbDacSacHD, chbVTVCab, chbFimStandard, cbFoxy2, cbFoxy4;
    private RegistrationDetailModel mRegister;
    public boolean isInternetOnly = false;
    private int customerType = 0;
    private String contract = "", regCode = "", phone1 = "";
    public static int isBind = 0;

    public static FragmentRegisterContractIPTV newInstance() {
        FragmentRegisterContractIPTV fragment = new FragmentRegisterContractIPTV();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentRegisterContractIPTV() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_contract_iptv, container, false);
        try {
            Common.setupUI(getActivity(), rootView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mContext = getActivity();
        getRegisterFromActivity();
        initView(rootView);
        initEvent();
        loadFormDeployment();
        loadPackage();
        loadDataToView();
        getIPTVPriceBoxFirst();
        return rootView;
    }

    private void initView(View view) {
        radRequestSetup = (RadioButton) view.findViewById(R.id.rad_request_setup_yes);
        radRequestDrillWall = (RadioButton) view.findViewById(R.id.rad_request_drill_the_wall_yes);
        spFormDeployment = (Spinner) view.findViewById(R.id.sp_form_deployment);

        /*Thiet bi dang ky*/
        imgBoxLess = (ImageView) view.findViewById(R.id.img_box_less);
        imgBoxPlus = (ImageView) view.findViewById(R.id.img_box_plus);
        lblBoxCount = (TextView) view.findViewById(R.id.lbl_box_count);

        imgPLCLess = (ImageView) view.findViewById(R.id.img_plc_less);
        imgPLCPlus = (ImageView) view.findViewById(R.id.img_plc_plus);
        lblPLCCount = (TextView) view.findViewById(R.id.lbl_plc_count);

        imgSTBReturnLess = (ImageView) view.findViewById(R.id.img_stb_return_less);
        imgSTBReturnPlus = (ImageView) view.findViewById(R.id.img_stb_return_plus);
        lblSTBReturn = (TextView) view.findViewById(R.id.lbl_stb_return_count);
        spPackage = (Spinner) view.findViewById(R.id.sp_package);
        txtPromotionBox1 = (EditText) view.findViewById(R.id.txt_promotion_box1);
        lblAmountPromotionFirstBox = (TextView) view.findViewById(R.id.lbl_promotion_first_box_amount);
        txtPromotionBox2 = (EditText) view.findViewById(R.id.txt_promotion_box2);
        lblAmountPromotionBoxOrder = (TextView) view.findViewById(R.id.lbl_promotion_box_order_amount);
        imgChargeTimesLess = (ImageView) view.findViewById(R.id.img_charge_times_less);
        imgChargeTimesPlus = (ImageView) view.findViewById(R.id.img_charge_times_plus);
        lblChargeTimes = (TextView) view.findViewById(R.id.lbl_charge_times);
        /*======= Extra =====*/

        /*Fim co ban*/
        chbFimStandard = (CheckBox) view.findViewById(R.id.chb_ip_tv_fim_standard);
        lblFimStandardChargeTimes = (TextView) view.findViewById(R.id.lbl_ip_tv_fim_standard_charge_times);
        lblFimStandardMonthCount = (TextView) view.findViewById(R.id.lbl_ip_tv_fim_standard_month_count);
        lblAmountFimStandard = (TextView) view.findViewById(R.id.lbl_ip_tv_fim_standard_amount);
        imgFimStandardChargeTimesLess = (ImageView) view.findViewById(R.id.img_ip_tv_fim_standard_charge_time_less);
        imgFimStandardChargeTimesPlus = (ImageView) view.findViewById(R.id.img_ip_tv_fim_standard_charge_time_plus);
        imgFimStandardMonthCountLess = (ImageView) view.findViewById(R.id.img_ip_tv_fim_standard_month_count_less);
        imgFimStandardMonthCountPlus = (ImageView) view.findViewById(R.id.img_ip_tv_fim_standard_month_count_plus);

        /*Fim+*/
        chbFimPlus = (CheckBox) view.findViewById(R.id.chb_fim_plus);
        imgFimPlusChargeTimesLess = (ImageView) view.findViewById(R.id.img_fim_plus_charge_times_less);
        imgFimPlusChargeTimesPlus = (ImageView) view.findViewById(R.id.img_fim_plus_charge_times_plus);
        imgFimPlusMonthCountLess = (ImageView) view.findViewById(R.id.img_fim_plus_month_count_less);
        imgFimPlusMonthCountPlus = (ImageView) view.findViewById(R.id.img_fim_plus_month_count_plus);
        lblAmountFimPlus = (TextView) view.findViewById(R.id.lbl_fim_plus_amount);
        lblFimPlusChargeTimes = (TextView) view.findViewById(R.id.lbl_fim_plus_charge_times);
        lblFimPlusMonthCount = (TextView) view.findViewById(R.id.lbl_fim_plus_month_count);

        /*Fim hot*/
        chbFimHot = (CheckBox) view.findViewById(R.id.chb_fim_hot);
        imgFimHotChargeTimesLess = (ImageView) view.findViewById(R.id.img_fim_hot_charge_times_less);
        imgFimHotChargeTimesPlus = (ImageView) view.findViewById(R.id.img_fim_hot_charge_times_plus);
        imgFimHotMonthCountLess = (ImageView) view.findViewById(R.id.img_fim_hot_month_count_less);
        imgFimHotMonthCountPlus = (ImageView) view.findViewById(R.id.img_fim_hot_month_count_plus);
        lblAmountFimHot = (TextView) view.findViewById(R.id.lbl_fim_hot_amount);
        lblFimHotChargeTimes = (TextView) view.findViewById(R.id.lbl_fim_hot_charge_times);
        lblFimHotMonthCount = (TextView) view.findViewById(R.id.lbl_fim_hot_month_count);

        /*K Plus*/
        chbKPlus = (CheckBox) view.findViewById(R.id.chb_k_plus);
        imgKPlusChargeTimesLess = (ImageView) view.findViewById(R.id.img_k_plus_charge_times_less);
        imgKPlusChargeTimesPlus = (ImageView) view.findViewById(R.id.img_k_plus_charge_times_plus);
        imgKPlusMonthCountLess = (ImageView) view.findViewById(R.id.img_k_plus_month_count_less);
        imgKPlusMonthCountPlus = (ImageView) view.findViewById(R.id.img_k_plus_month_count_plus);
        lblAmountKPlus = (TextView) view.findViewById(R.id.lbl_k_plus_amount);
        lblKPlusChargeTimes = (TextView) view.findViewById(R.id.lbl_k_plus_charge_times);
        lblKPlusMonthCount = (TextView) view.findViewById(R.id.lbl_k_plus_month_count);

        /*Dac sac HD*/
        frmDacsacHD = view.findViewById(R.id.dac_sac_hd);
        frmDacsacHD.setVisibility(View.GONE);
        chbDacSacHD = (CheckBox) view.findViewById(R.id.chb_dac_sac_hd);
        imgDacSacHDChargeTimesLess = (ImageView) view.findViewById(R.id.img_dac_sac_hd_charge_times_less);
        imgDacSacHDChargeTimesPlus = (ImageView) view.findViewById(R.id.img_dac_sac_hd_charge_times_plus);
        imgDacSacHDMonthCountLess = (ImageView) view.findViewById(R.id.img_dac_sac_hd_month_count_less);
        imgDacSacHDMonthCountPlus = (ImageView) view.findViewById(R.id.img_dac_sac_hd_month_count_plus);
        lblAmountDacSacHD = (TextView) view.findViewById(R.id.lbl_dac_sac_hd_amount);
        lblDacSacHDChargeTimes = (TextView) view.findViewById(R.id.lbl_dac_sac_hd_charge_times);
        lblDacSacHDMonthCount = (TextView) view.findViewById(R.id.lbl_dac_sac_hd_month_count);

        /*VTV Cab*/
        chbVTVCab = (CheckBox) view.findViewById(R.id.chb_vtv_cab_hd);
        imgVTVCabChargeTimesLess = (ImageView) view.findViewById(R.id.img_vtv_cab_charge_times_less);
        imgVTVCabChargeTimesPlus = (ImageView) view.findViewById(R.id.img_vtv_cab_charge_times_plus);
        imgVTVCabMonthCountLess = (ImageView) view.findViewById(R.id.img_vtv_cab_month_count_less);
        imgVTVCabMonthCountPlus = (ImageView) view.findViewById(R.id.img_vtv_cab_month_count_plus);
        lblAmountVTVCab = (TextView) view.findViewById(R.id.lbl_vtv_cab_amount);
        lblVTVCabChargeTimes = (TextView) view.findViewById(R.id.lbl_vtv_cab_charge_times);
        lblVTVCabMonthCount = (TextView) view.findViewById(R.id.lbl_vtv_cab_month_count);

        //Navigation goi extra
        frmExtra = (LinearLayout) view.findViewById(R.id.frm_extra);
        frmExtraTitle = (LinearLayout) view.findViewById(R.id.frm_extra_title);
        imgNavigationExtra = (ImageView) view.findViewById(R.id.img_navigation_drop_down);

        cbFoxy2 = (CheckBox) view.findViewById(R.id.chb_iptv_foxy_2);
        imgFoxy2ChargeTimeLess = (ImageView) view.findViewById(R.id.img_btn_iptv_foxy_2_charge_times_less);
        imgFoxy2ChargeTimePlus = (ImageView) view.findViewById(R.id.img_btn_iptv_foxy_2_charge_times_plus);
        imgFoxy2PrepaidMonthLess = (ImageView) view.findViewById(R.id.img_btn_iptv_foxy_2_month_count_pless);
        imgFoxy2PrepaidMonthPlus = (ImageView) view.findViewById(R.id.img_btn_iptv_foxy_2_month_count_plus);
        tvFoxy2ChargeTime = (TextView) view.findViewById(R.id.txt_iptv_foxy_2_charge_times);
        tvFoxy2PrepaidMonth = (TextView) view.findViewById(R.id.txt_iptv_foxy_2_month_count);
        tvFoxy2Amount = (TextView) view.findViewById(R.id.lbl_iptv_foxy_2_amount);

        cbFoxy4 = (CheckBox) view.findViewById(R.id.chb_iptv_foxy_4);
        imgFoxy4ChargeTimeLess = (ImageView) view.findViewById(R.id.img_btn_iptv_foxy_4_charge_times_less);
        imgFoxy4ChargeTimePlus = (ImageView) view.findViewById(R.id.img_btn_iptv_foxy_4_charge_times_plus);
        imgFoxy4PrepaidMonthLess = (ImageView) view.findViewById(R.id.img_btn_iptv_foxy_4_month_count_pless);
        imgFoxy4PrepaidMonthPlus = (ImageView) view.findViewById(R.id.img_btn_iptv_foxy_4_month_count_plus);
        tvFoxy4ChargeTime = (TextView) view.findViewById(R.id.txt_iptv_foxy_4_charge_times);
        tvFoxy4PrepaidMonth = (TextView) view.findViewById(R.id.txt_iptv_foxy_4_month_count);
        tvFoxy4Amount = (TextView) view.findViewById(R.id.lbl_iptv_foxy_4_amount);
    }

    /**
     * Lấy thông tin PĐk từ Activity trước khi thao tác
     */
    private void getRegisterFromActivity() {
        if (mContext != null && mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
            mRegister = ((RegisterContractActivity) mContext).getRegister();
            ObjectDetailModel mObject = ((RegisterContractActivity) mContext).getObject();
            contract = mRegister != null ? mRegister.getContract() : mObject.getContract();
            //haulc3 updated 2019/05/14
            regCode = mRegister != null ? mRegister.getRegCode() : "";
            phone1 = mRegister != null ? mRegister.getPhone_1() : mObject.getPhone_1();
        }
    }

    /**
     * Update view
     */
    private void loadDataToView() {
        try {
            if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
                if (((RegisterContractActivity) mContext).getObject() != null) {
                    if (((RegisterContractActivity) mContext).getObject().getServiceType() == 0) { //Internet Only
                        lblBoxCount.setText("1");
                        isInternetOnly = true;
                        txtPromotionBox1.setEnabled(true);
                    } else {
                        isInternetOnly = false;
                        txtPromotionBox1.setEnabled(false);
                    }
                }
            }
            if (mRegister != null && !mRegister.getRegCode().equals("")) {
                if (mRegister.getContractServiceType() == 0) {
                    isInternetOnly = true;
                    txtPromotionBox1.setEnabled(true);
                } else {
                    isInternetOnly = false;
                    txtPromotionBox1.setEnabled(false);
                }
                radRequestDrillWall.setChecked(mRegister.getIPTVRequestDrillWall() > 0);
                radRequestSetup.setChecked(mRegister.getIPTVRequestSetUp() > 0);
                lblBoxCount.setText(String.valueOf(mRegister.getIPTVBoxCount()));
                lblPLCCount.setText(String.valueOf(mRegister.getIPTVPLCCount()));
                lblSTBReturn.setText(String.valueOf(mRegister.getIPTVReturnSTBCount()));

                boolean enableExtra = false;
                //Phim cơ bản
                if (mRegister.getIPTVFimPlusStdChargeTimes() > 0) {
                    chbFimStandard.setChecked(true);
                    enableFimStandard(true);
                    enableExtra = true;
                    lblFimStandardChargeTimes.setText(String.valueOf(mRegister.getIPTVFimPlusStdChargeTimes()));
                    lblFimStandardMonthCount.setText(String.valueOf(mRegister.getIPTVFimPlusStdPrepaidMonth()));
                } else {
                    chbFimStandard.setChecked(false);
                    enableFimStandard(false);
                }

                //Phim cao cấp
                if (mRegister.getIPTVFimPlusChargeTimes() > 0) {
                    chbFimPlus.setChecked(true);
                    enableFimPlus(true);
                    enableExtra = true;
                    lblFimPlusChargeTimes.setText(String.valueOf(mRegister.getIPTVFimPlusChargeTimes()));
                    lblFimPlusMonthCount.setText(String.valueOf(mRegister.getIPTVFimPlusPrepaidMonth()));
                } else {
                    chbFimPlus.setChecked(false);
                    enableFimPlus(false);
                }

                //phim hot
                if (mRegister.getIPTVFimHotChargeTimes() > 0) {
                    chbFimHot.setChecked(true);
                    enableFimHot(true);
                    enableExtra = true;
                    lblFimHotChargeTimes.setText(String.valueOf(mRegister.getIPTVFimHotChargeTimes()));
                    lblFimHotMonthCount.setText(String.valueOf(mRegister.getIPTVFimHotPrepaidMonth()));
                } else {
                    chbFimHot.setChecked(false);
                    enableFimHot(false);
                }

                //K+
                if (mRegister.getIPTVKPlusChargeTimes() > 0) {
                    chbKPlus.setChecked(true);
                    enableKPlus(true);
                    enableExtra = true;
                    lblKPlusChargeTimes.setText(String.valueOf(mRegister.getIPTVKPlusChargeTimes()));
                    lblKPlusMonthCount.setText(String.valueOf(mRegister.getIPTVKPlusPrepaidMonth()));
                } else {
                    chbKPlus.setChecked(false);
                    enableKPlus(false);
                }

                //Đặc sắc HD
                if (mRegister.getIPTVVTCChargeTimes() > 0) {
                    chbDacSacHD.setChecked(true);
                    enableDacSacHD(true);
                    enableExtra = true;
                    lblDacSacHDChargeTimes.setText(String.valueOf(mRegister.getIPTVVTCChargeTimes()));
                    lblDacSacHDMonthCount.setText(String.valueOf(mRegister.getIPTVVTCPrepaidMonth()));
                } else {
                    chbDacSacHD.setChecked(false);
                    enableDacSacHD(false);
                }

                if (mRegister.getIPTVVTVChargeTimes() > 0) {
                    chbVTVCab.setChecked(true);
                    enableVTVCab(true);
                    enableExtra = true;
                    lblVTVCabChargeTimes.setText(String.valueOf(mRegister.getIPTVVTVChargeTimes()));
                    lblVTVCabMonthCount.setText(String.valueOf(mRegister.getIPTVVTVPrepaidMonth()));
                } else {
                    chbVTVCab.setChecked(false);
                    enableVTVCab(false);
                }

                tvFoxy2ChargeTime.setText(String.valueOf(mRegister.getFoxy2ChargeTimes()));
                tvFoxy2PrepaidMonth.setText(String.valueOf(mRegister.getFoxy2PrepaidMonth()));
                tvFoxy4ChargeTime.setText(String.valueOf(mRegister.getFoxy4ChargeTimes()));
                tvFoxy4PrepaidMonth.setText(String.valueOf(mRegister.getFoxy4PrepaidMonth()));
                if (mRegister.getFoxy2ChargeTimes() != 0 || mRegister.getFoxy2PrepaidMonth() != 0) {
                    cbFoxy2.setChecked(true);
                }
                if (mRegister.getFoxy4ChargeTimes() != 0 || mRegister.getFoxy4PrepaidMonth() != 0) {
                    cbFoxy4.setChecked(true);
                }

                if (!enableExtra) {
                    frmExtra.setVisibility(View.GONE);
                    imgNavigationExtra.setImageResource(R.drawable.ic_navigation_drop_up);
                }
            } else {
                lblBoxCount.setText(String.valueOf(0));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hình thức triển khai
     */
    private void loadFormDeployment() {
        try {
            ArrayList<KeyValuePairModel> lstCusSolvency = new ArrayList<>();
            lstCusSolvency.add(new KeyValuePairModel(4, "Thêm Box"));
            spFormDeployment.setAdapter(new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstCusSolvency, Gravity.CENTER));
            if (mRegister != null && !mRegister.getIPTVPackage().trim().equals("")) {
                spFormDeployment.setSelection(Common.getIndex(spFormDeployment, mRegister.getIPTVStatus()), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gói dịch vụ
     */
    private void loadPackage() {
        try {
            ArrayList<KeyValuePairModel> lst = new ArrayList<>();
            lst.add(new KeyValuePairModel(0, "--Chọn gói dịch vụ--", ""));
            lst.add(new KeyValuePairModel(76, "Truyền Hình FPT", "VOD HD"));
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lst, Gravity.CENTER);
            spPackage.setAdapter(adapter);
            if (mRegister != null && !Common.isNullOrEmpty(mRegister.getRegCode())) {
                for (int i = 0; i < lst.size(); i++) {
                    KeyValuePairModel selectedItem = lst.get(i);

                    if (selectedItem.getHint().equalsIgnoreCase(mRegister.getIPTVPackage())) {
                        spPackage.setSelection(i, true);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Goi API Lấy DS CLMK Box 1
     */
    private void getPromotionFirstBox() {
        try {
            KeyValuePairModel packageItem = (KeyValuePairModel) spPackage.getSelectedItem();
            if (packageItem.getID() != 0) {
                new GetPromotionIPTVList(getActivity(), FragmentRegisterContractIPTV.this, packageItem.getHint(),
                        1, this.customerType, contract, regCode);//1: box 1, 2: box 2 trở đi
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gọi API CLKM Box 2 trở đi
     */
    private void getPromotionOrderBox() {
        KeyValuePairModel packageItem = (KeyValuePairModel) spPackage.getSelectedItem();
        if (packageItem.getID() != 0) {
            new GetPromotionIPTVList(getActivity(), FragmentRegisterContractIPTV.this, packageItem.getHint(),
                    2, this.customerType, contract, regCode);//1: box 1, 2: box 2 trở đi
        }
    }

    /**
     * Lấy giá các gói Extra BOX 1
     */

    private void getIPTVPriceBoxFirst() {
        int IPTVPromotionType = promotionIPTVModelBox1 != null ? promotionIPTVModelBox1.getType() : -1;
        int IPTVPromotionID = promotionIPTVModelBox1 != null ? promotionIPTVModelBox1.getPromotionID() : 0;
        new GetIPTVPrice(mContext, this, contract, regCode, IPTVPromotionType, IPTVPromotionID);
    }

    /**
     * Lấy giá các gói Extra BOX 2
     */

    private void getIPTVPriceBoxOrder() {
        int IPTVPromotionTypeBoxOrder = promotionIPTVModelBox2 != null ? promotionIPTVModelBox2.getType() : -1;
        int IPTVPromotionIDBoxOrder = promotionIPTVModelBox2 != null ? promotionIPTVModelBox2.getPromotionID() : 0;
        new GetIPTVPrice(mContext, this, contract, regCode, IPTVPromotionTypeBoxOrder, IPTVPromotionIDBoxOrder);
    }

    // khởi tạo danh sách clkm iptv box 1
    public void initIPTVPromotionIPTVBox1(ArrayList<PromotionIPTVModel> lst) {
        this.mListPromotionIPTVBox1 = lst;
        if (this.mListPromotionIPTVBox1.size() > 0) {
            try {
                if (mRegister != null) {
                    PromotionIPTVModel promotionIPTVModel = Common.getItemPromotionIPTVBox(lst, 1, mRegister);
                    promotionIPTVModelBox1 = promotionIPTVModel;
                    if (promotionIPTVModel != null) {
                        txtPromotionBox1.setText(promotionIPTVModel.getPromotionName());
                        lblAmountPromotionFirstBox.setText(
                                String.format(getString(R.string.txt_unit_price), Common.formatNumber(promotionIPTVModel.getAmount())));
                        lblAmountPromotionFirstBox.setVisibility(View.VISIBLE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // khởi tạo danh sách clkm iptv box 2
    public void initIPTVPromotionIPTVBox2(ArrayList<PromotionIPTVModel> lst) {
        this.mListPromotionIPTVBox2 = lst;
        if (this.mListPromotionIPTVBox2.size() > 0) {
            try {
                if (mRegister != null) {
                    PromotionIPTVModel promotionIPTVModel = Common.getItemPromotionIPTVBox(lst, 2, mRegister);
                    if (promotionIPTVModel != null) {
                        promotionIPTVModelBox2 = promotionIPTVModel;
                        txtPromotionBox2.setText(promotionIPTVModel.getPromotionName());
                        lblAmountPromotionBoxOrder.setText(String.format(getString(R.string.txt_unit_price),
                                Common.formatNumber(promotionIPTVModel.getAmount())));
                        lblAmountPromotionBoxOrder.setVisibility(View.VISIBLE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * TODO: Update giá các gói Extra trên giao diện
     */
    public void loadExtraAmount(List<IPTVPriceModel> lst) {
        if (lst != null && lst.size() > 0) {
            try {
                IPTVPriceModel item = lst.get(0);
                lblAmountFimHot.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFimHot())));
                lblAmountFimPlus.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFimPlus())));
                lblAmountKPlus.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getKPlus())));
                lblAmountVTVCab.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getVTVCap())));
                lblAmountDacSacHD.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getDacSacHD())));
                lblAmountFimStandard.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFimPlusStd())));
                tvFoxy2Amount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFoxy2())));
                tvFoxy4Amount.setText(String.format(getString(R.string.txt_unit_price),
                        Common.formatNumber(item.getFoxy4())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Goi API Tinh tổng tiền IPTV
     */
    public void getIPTVTotal(Context mContext) {
        int IPTVPromotionType = promotionIPTVModelBox1 != null ? promotionIPTVModelBox1.getType() : -1;
        int IPTVPromotionTypeBoxOrder = promotionIPTVModelBox2 != null ? promotionIPTVModelBox2.getType() : -1;
        mRegister = updateRegisterFromView(mRegister);
        new GetIPTVTotal(mContext, mRegister, phone1, contract, regCode, IPTVPromotionType, IPTVPromotionTypeBoxOrder);
    }


    /**
     * Dùng khởi tạo đối tượng PĐK
     */
    private RegistrationDetailModel updateRegisterFromView(RegistrationDetailModel register) {
        try {
            if (register == null)
                register = new RegistrationDetailModel();
            int boxCount = Integer.valueOf(lblBoxCount.getText().toString());
            /*Neu co dang ky IPTV(Box count > 0) thi cap nhat thong tin dk IPTV cho PDK*/
            if (boxCount > 0) {
                register.setIPTVRequestDrillWall(radRequestDrillWall.isChecked() ? 1 : 0);
                register.setIPTVRequestSetUp(radRequestSetup.isChecked() ? 1 : 0);
                if (spFormDeployment.getAdapter() != null)
                    register.setIPTVStatus(((KeyValuePairModel) spFormDeployment.getSelectedItem()).getID());
                register.setIPTVBoxCount(Integer.valueOf(lblBoxCount.getText().toString()));
                register.setIPTVPLCCount(Integer.valueOf(lblPLCCount.getText().toString()));
                register.setIPTVReturnSTBCount(Integer.valueOf(lblSTBReturn.getText().toString()));
                register.setIPTVPackage(((KeyValuePairModel) spPackage.getSelectedItem()).getHint());
                if (promotionIPTVModelBox1 != null) {
                    register.setIPTVPromotionID(promotionIPTVModelBox1.getPromotionID());
                    register.setIPTVPromotionType(promotionIPTVModelBox1.getType());
                    register.setIPTVPromotionDesc(promotionIPTVModelBox1.getPromotionName());
                } else {
                    register.setIPTVPromotionID(0);
                    register.setIPTVPromotionType(-1);
                    register.setIPTVPromotionDesc("");
                }
                if (promotionIPTVModelBox2 != null) {
                    register.setIPTVPromotionIDBoxOther(promotionIPTVModelBox2.getPromotionID());
                    register.setIPTVPromotionTypeBoxOrder(promotionIPTVModelBox2.getType());
                    register.setIPTVPromotionBoxOrderDesc(promotionIPTVModelBox2.getPromotionName());
                } else {
                    register.setIPTVPromotionIDBoxOther(0);
                    register.setIPTVPromotionTypeBoxOrder(-1);
                    register.setIPTVPromotionBoxOrderDesc("");
                }
                // số lần tính cước mặc định = 1
                register.setIPTVChargeTimes(1);

                /* phim co ban*/
                register.setIPTVFimPlusStdChargeTimes(Integer.valueOf(lblFimStandardChargeTimes.getText().toString()));
                register.setIPTVFimPlusStdPrepaidMonth(Integer.parseInt(lblFimStandardMonthCount.getText().toString()));

                register.setIPTVFimPlusChargeTimes(Integer.valueOf(lblFimPlusChargeTimes.getText().toString()));
                register.setIPTVFimPlusPrepaidMonth(Integer.valueOf(lblFimPlusMonthCount.getText().toString()));
                register.setIPTVFimHotChargeTimes(Integer.valueOf(lblFimHotChargeTimes.getText().toString()));
                register.setIPTVFimHotPrepaidMonth(Integer.valueOf(lblFimHotMonthCount.getText().toString()));
                register.setIPTVKPlusChargeTimes(Integer.valueOf(lblKPlusChargeTimes.getText().toString()));
                register.setIPTVKPlusPrepaidMonth(Integer.valueOf(lblKPlusMonthCount.getText().toString()));
                register.setIPTVVTCChargeTimes(Integer.valueOf(lblDacSacHDChargeTimes.getText().toString()));
                register.setIPTVVTCPrepaidMonth(Integer.valueOf(lblDacSacHDMonthCount.getText().toString()));
                register.setIPTVVTVChargeTimes(Integer.valueOf(lblVTVCabChargeTimes.getText().toString()));
                register.setIPTVVTVPrepaidMonth(Integer.valueOf(lblVTVCabMonthCount.getText().toString()));
            } else {/*Khong dang ky IPTV*/
                register.setIPTVRequestDrillWall(0);
                register.setIPTVRequestSetUp(0);
                register.setIPTVStatus(0);
                register.setIPTVBoxCount(0);
                register.setIPTVPLCCount(0);
                register.setIPTVReturnSTBCount(0);
                register.setIPTVPackage("");
                register.setIPTVPromotionID(0);
                register.setIPTVPromotionType(-1);
                register.setIPTVPromotionDesc("");
                register.setIPTVPromotionIDBoxOther(0);
                register.setIPTVPromotionTypeBoxOrder(-1);
                register.setIPTVChargeTimes(0);
                register.setIPTVFimPlusStdChargeTimes(0);
                register.setIPTVFimPlusStdPrepaidMonth(0);
                register.setIPTVFimPlusChargeTimes(0);
                register.setIPTVFimPlusPrepaidMonth(0);
                register.setIPTVFimHotChargeTimes(0);
                register.setIPTVFimHotPrepaidMonth(0);
                register.setIPTVKPlusChargeTimes(0);
                register.setIPTVKPlusPrepaidMonth(0);
                register.setIPTVVTCChargeTimes(0);
                register.setIPTVVTCPrepaidMonth(0);
                register.setIPTVVTVChargeTimes(0);
                register.setIPTVVTVPrepaidMonth(0);
            }

            register.setFoxy2ChargeTimes(Integer.parseInt(tvFoxy2ChargeTime.getText().toString()));
            register.setFoxy2PrepaidMonth(Integer.parseInt(tvFoxy2PrepaidMonth.getText().toString()));
            register.setFoxy4ChargeTimes(Integer.parseInt(tvFoxy4ChargeTime.getText().toString()));
            register.setFoxy4PrepaidMonth(Integer.parseInt(tvFoxy4PrepaidMonth.getText().toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return register;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            if (requestCode == 1) {
                if (data.hasExtra(TAG_PROMOTION_TYPE_BOX)) {
                    PromotionIPTVModel item = data.getParcelableExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_ITEM);
                    int typeIPTVBox = data.getIntExtra(TAG_PROMOTION_TYPE_BOX, 0);
                    if (typeIPTVBox == 1) {
                        if (item != null) {
                            promotionIPTVModelBox1 = item;
                            txtPromotionBox1.setText(item.getPromotionName());
                            FragmentRegisterContractIPTV.isBind = 1;
                            lblAmountPromotionFirstBox.setText(
                                    String.format(getString(R.string.txt_unit_price), Common.formatNumber(item.getAmount()))
                            );
                            lblAmountPromotionFirstBox.setVisibility(View.VISIBLE);
                        }
                    }
                    if (typeIPTVBox == 2) {
                        if (item != null) {
                            promotionIPTVModelBox2 = item;
                            txtPromotionBox2.setText(item.getPromotionName());
                            FragmentRegisterContractIPTV.isBind = 1;
                            lblAmountPromotionBoxOrder.setText(
                                    String.format(getString(R.string.txt_unit_price), Common.formatNumber(item.getAmount()))
                            );
                            lblAmountPromotionBoxOrder.setVisibility(View.VISIBLE);
                        }
                    }

                }
            }
        }
    }

    public void updateRegister() {
        try {
            if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
                RegisterContractActivity activity = (RegisterContractActivity) mContext;
                RegistrationDetailModel register = activity.getRegister();
                register = updateRegisterFromView(register);
                activity.setRegister(register);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * TODO: Bật/tắt gói Extra
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.chb_ip_tv_fim_standard:
                enableFimStandard(isChecked);
                if (isChecked)
                    lblFimStandardChargeTimes.setText("1");
                break;
            case R.id.chb_fim_plus:
                enableFimPlus(isChecked);
                if (isChecked)
                    lblFimPlusChargeTimes.setText("1");
                break;
            case R.id.chb_fim_hot:
                enableFimHot(isChecked);
                if (isChecked)
                    lblFimHotChargeTimes.setText("1");
                break;
            case R.id.chb_k_plus:
                enableKPlus(isChecked);
                if (isChecked)
                    lblKPlusChargeTimes.setText("1");
                break;
            case R.id.chb_dac_sac_hd:
                enableDacSacHD(isChecked);
                if (isChecked)
                    lblDacSacHDChargeTimes.setText("1");
                break;
            case R.id.chb_vtv_cab_hd:
                enableVTVCab(isChecked);
                if (isChecked)
                    lblVTVCabChargeTimes.setText("1");
                break;
        }
    }

    /**
     * TODO: Dong mo goi extra khi click
     */
    private void dropDownNavigation(LinearLayout frm, ImageView img) {
        try {
            if (frm.getVisibility() == View.VISIBLE) {
                frm.setVisibility(View.GONE);
                img.setImageResource(R.drawable.ic_navigation_drop_up);
            } else {
                frm.setVisibility(View.VISIBLE);
                img.setImageResource(R.drawable.ic_navigation_drop_down);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * TODO: init event for control UI
     */
    private void initEvent() {
        try {
            lblBoxCount.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        int boxCount;
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                        if (boxCount == 0) {
                            enableIPTV(false);
                        } else {
                            enableIPTV(true);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            frmExtraTitle.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dropDownNavigation(frmExtra, imgNavigationExtra);
                }
            });

            /*Box*/
            imgBoxLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblBoxCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblBoxCount.setText(String.valueOf(count));
                    }

                    //binhnp2 edit
                    /*Xoa CLMK box 2 nếu chi dk 1 box*/
                    if (isInternetOnly) {
                        if (count == 1) {
                            promotionIPTVModelBox2 = null;
                            txtPromotionBox2.setText(null);
                            lblAmountPromotionBoxOrder.setText("");
                            lblAmountPromotionBoxOrder.setVisibility(View.GONE);
                            txtPromotionBox2.setEnabled(false);
                        }
                    } else {
                        if (count == 0) {
                            promotionIPTVModelBox2 = null;
                            txtPromotionBox2.setText(null);
                            lblAmountPromotionBoxOrder.setText("");
                            lblAmountPromotionBoxOrder.setVisibility(View.GONE);
                            txtPromotionBox2.setEnabled(false);
                        }
                    }

                }
            });

            imgBoxPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblBoxCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblBoxCount.setText(String.valueOf(count));
                    //binhnp2
                    /*Load CLMK Box 2 neu dk 2 box tro len*/
                    if (isInternetOnly) {
                        if (count == 2) {
                            getPromotionOrderBox();
                            txtPromotionBox2.setEnabled(true);
                        }
                    } else {
                        if (count == 1)
                            getPromotionOrderBox();
                    }
                }
            });

            /*PLC*/
            imgPLCLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblPLCCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblPLCCount.setText(String.valueOf(count));
                    }
                }
            });

            imgPLCPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblPLCCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblPLCCount.setText(String.valueOf(count));
                }
            });

            /*STB Return*/
            imgSTBReturnLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblSTBReturn.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblSTBReturn.setText(String.valueOf(count));
                    }
                }
            });

            imgSTBReturnPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblSTBReturn.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblSTBReturn.setText(String.valueOf(count));
                }
            });

            /*Charge times*/
            imgChargeTimesLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblChargeTimes.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgChargeTimesPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblSTBReturn.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblChargeTimes.setText(String.valueOf(count));
                }
            });
            /*====================== Extra =========================*/
            /*Fim cơ bản*/
            imgFimStandardChargeTimesLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimStandardChargeTimes.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblFimStandardChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgFimStandardChargeTimesPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblFimStandardChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblFimStandardChargeTimes.setText(String.valueOf(count));
                }
            });

            imgFimStandardMonthCountLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimStandardMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblFimStandardMonthCount.setText(String.valueOf(count));
                    }
                }
            });

            imgFimStandardMonthCountPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimStandardMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblFimStandardMonthCount.setText(String.valueOf(count));
                }
            });
            /*Fim+*/
            imgFimPlusChargeTimesLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimPlusChargeTimes.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblFimPlusChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgFimPlusChargeTimesPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblFimPlusChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblFimPlusChargeTimes.setText(String.valueOf(count));
                }
            });

            imgFimPlusMonthCountLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimPlusMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblFimPlusMonthCount.setText(String.valueOf(count));
                    }
                }
            });

            imgFimPlusMonthCountPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimPlusMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblFimPlusMonthCount.setText(String.valueOf(count));
                }
            });
            /*Fim hot*/
            imgFimHotChargeTimesLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimHotChargeTimes.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblFimHotChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgFimHotChargeTimesPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblFimHotChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblFimHotChargeTimes.setText(String.valueOf(count));
                }
            });

            imgFimHotMonthCountLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimHotMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblFimHotMonthCount.setText(String.valueOf(count));
                    }
                }
            });

            imgFimHotMonthCountPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimHotMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblFimHotMonthCount.setText(String.valueOf(count));
                }
            });
            /*K+*/
            imgKPlusChargeTimesLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblKPlusChargeTimes.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblKPlusChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgKPlusChargeTimesPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblKPlusChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                    if (count < boxCount)
                        count++;
                    lblKPlusChargeTimes.setText(String.valueOf(count));
                }
            });

            imgKPlusMonthCountLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblKPlusMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblKPlusMonthCount.setText(String.valueOf(count));
                    }
                }
            });

            imgKPlusMonthCountPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblKPlusMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblKPlusMonthCount.setText(String.valueOf(count));
                }
            });
            /*Dac sac HD*/
            imgDacSacHDChargeTimesLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblDacSacHDChargeTimes.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblDacSacHDChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgDacSacHDChargeTimesPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblDacSacHDChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblDacSacHDChargeTimes.setText(String.valueOf(count));
                }
            });

            imgDacSacHDMonthCountLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblDacSacHDMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblDacSacHDMonthCount.setText(String.valueOf(count));
                    }
                }
            });

            imgDacSacHDMonthCountPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblDacSacHDMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblDacSacHDMonthCount.setText(String.valueOf(count));
                }
            });
            /*VTV Cab*/
            imgVTVCabChargeTimesLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblVTVCabChargeTimes.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblVTVCabChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgVTVCabChargeTimesPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblVTVCabChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblVTVCabChargeTimes.setText(String.valueOf(count));
                }
            });

            imgVTVCabMonthCountLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblVTVCabMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblVTVCabMonthCount.setText(String.valueOf(count));
                    }
                }
            });
            imgVTVCabMonthCountPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblVTVCabMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblVTVCabMonthCount.setText(String.valueOf(count));
                }
            });

            /*Check Box*/
            chbFimPlus.setOnCheckedChangeListener(this);
            chbFimHot.setOnCheckedChangeListener(this);
            chbKPlus.setOnCheckedChangeListener(this);
            chbVTVCab.setOnCheckedChangeListener(this);
            chbDacSacHD.setOnCheckedChangeListener(this);
            chbFimStandard.setOnCheckedChangeListener(this);
            spPackage.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position > 0) {
                        if (isInternetOnly) {
                            getPromotionFirstBox();
                            int boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                            if (boxCount >= 2)
                                getPromotionOrderBox(); //binhnp2
                        } else {
                            getPromotionOrderBox();
                        }
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            txtPromotionBox1.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Common.hideSoftKeyboard(txtPromotionBox1);
                    showFilterActivity(true, 1);
                }
            });
            txtPromotionBox1.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() > 0) {
                        // lấy giá gói IPTV Extra box 1
                        getIPTVPriceBoxFirst();
                    } else {
                        lblAmountPromotionFirstBox.setText(null);
                        lblAmountPromotionFirstBox.setVisibility(View.GONE);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            txtPromotionBox2.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Common.hideSoftKeyboard(txtPromotionBox2);
                    showFilterActivity(true, 2);
                }
            });
            txtPromotionBox2.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() > 0) {
                        // lấy giá gói IPTV Extra box 2 nếu box 1 không kích hoạt
                        if (txtPromotionBox1.getText().length() == 0) {
                            getIPTVPriceBoxOrder();
                        }
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            //foxy 2
            imgFoxy2ChargeTimeLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!cbFoxy2.isChecked()) {
                        return;
                    }
                    int quantity = Integer.parseInt(tvFoxy2ChargeTime.getText().toString());
                    if (quantity == 1) {
                        return;
                    }
                    quantity--;
                    tvFoxy2ChargeTime.setText(String.valueOf(quantity));
                }
            });

            imgFoxy2ChargeTimePlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!cbFoxy2.isChecked()) {
                        return;
                    }
                    int quantity = Integer.parseInt(tvFoxy2ChargeTime.getText().toString());
                    int maxQuantity = Integer.valueOf(lblBoxCount.getText().toString());
                    if (quantity < maxQuantity) {
                        quantity++;
                    }
                    tvFoxy2ChargeTime.setText(String.valueOf(quantity));
                }
            });

            imgFoxy2PrepaidMonthLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!cbFoxy2.isChecked()) {
                        return;
                    }
                    int quantity = Integer.parseInt(tvFoxy2PrepaidMonth.getText().toString());
                    if (quantity == 0) {
                        return;
                    }
                    quantity--;
                    tvFoxy2PrepaidMonth.setText(String.valueOf(quantity));
                }
            });

            imgFoxy2PrepaidMonthPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!cbFoxy2.isChecked()) {
                        return;
                    }
                    int quantity = Integer.parseInt(tvFoxy2PrepaidMonth.getText().toString());
                    quantity++;
                    tvFoxy2PrepaidMonth.setText(String.valueOf(quantity));
                }
            });

            //foxy4
            imgFoxy4ChargeTimeLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!cbFoxy4.isChecked()) {
                        return;
                    }
                    int quantity = Integer.parseInt(tvFoxy4ChargeTime.getText().toString());
                    if (quantity == 1) {
                        return;
                    }
                    quantity--;
                    tvFoxy4ChargeTime.setText(String.valueOf(quantity));
                }
            });

            imgFoxy4ChargeTimePlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!cbFoxy4.isChecked()) {
                        return;
                    }
                    int quantity = Integer.parseInt(tvFoxy4ChargeTime.getText().toString());
                    int maxQuantity = Integer.valueOf(lblBoxCount.getText().toString());
                    if (quantity < maxQuantity) {
                        quantity++;
                    }
                    tvFoxy4ChargeTime.setText(String.valueOf(quantity));
                }
            });

            imgFoxy4PrepaidMonthLess.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!cbFoxy4.isChecked()) {
                        return;
                    }
                    int quantity = Integer.parseInt(tvFoxy4PrepaidMonth.getText().toString());
                    if (quantity == 0) {
                        return;
                    }
                    quantity--;
                    tvFoxy4PrepaidMonth.setText(String.valueOf(quantity));
                }
            });

            imgFoxy4PrepaidMonthPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!cbFoxy4.isChecked()) {
                        return;
                    }
                    int quantity = Integer.parseInt(tvFoxy4PrepaidMonth.getText().toString());
                    quantity++;
                    tvFoxy4PrepaidMonth.setText(String.valueOf(quantity));
                }
            });

            cbFoxy2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean isChecked = cbFoxy2.isChecked();
                    if (isChecked) {
                        tvFoxy2ChargeTime.setText(String.valueOf(1));
                        tvFoxy2PrepaidMonth.setText(String.valueOf(1));

                        cbFoxy4.setChecked(false);
                        tvFoxy4ChargeTime.setText(String.valueOf(0));
                        tvFoxy4PrepaidMonth.setText(String.valueOf(0));
                    } else {
                        tvFoxy2ChargeTime.setText(String.valueOf(0));
                        tvFoxy2PrepaidMonth.setText(String.valueOf(0));
                    }
                }
            });

            cbFoxy4.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean isChecked = cbFoxy4.isChecked();
                    if (isChecked) {
                        tvFoxy4ChargeTime.setText(String.valueOf(1));
                        tvFoxy4PrepaidMonth.setText(String.valueOf(1));

                        cbFoxy2.setChecked(false);
                        tvFoxy2ChargeTime.setText(String.valueOf(0));
                        tvFoxy2PrepaidMonth.setText(String.valueOf(0));
                    } else {
                        tvFoxy4ChargeTime.setText(String.valueOf(0));
                        tvFoxy4PrepaidMonth.setText(String.valueOf(0));
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // hiển thị màn hình lọc clkm IPTV
    private void showFilterActivity(boolean promotionIPTV, int box) {
        try {
            if (promotionIPTV) {
                if (mListPromotionIPTVBox1 != null && box == 1) {
                    Intent intent = new Intent(getActivity(), PromotionIPTVFilterActivity.class);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_TYPE_BOX, box);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_LIST, mListPromotionIPTVBox1);
                    startActivityForResult(intent, 1);
                }
                if (mListPromotionIPTVBox2 != null && box == 2) {
                    Intent intent = new Intent(getActivity(), PromotionIPTVFilterActivity.class);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_TYPE_BOX, box);
                    intent.putExtra(PromotionIPTVFilterActivity.TAG_PROMOTION_LIST, mListPromotionIPTVBox2);
                    startActivityForResult(intent, 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * TODO: On/Off Fim Cơ bản
     */
    private void enableFimStandard(boolean enable) {
        if (enable) {
            if (chbFimStandard.isChecked()) {
                imgFimStandardChargeTimesLess.setClickable(enable);
                imgFimStandardChargeTimesPlus.setClickable(enable);
                imgFimStandardMonthCountLess.setClickable(enable);
                imgFimStandardMonthCountPlus.setClickable(enable);
            }
        } else {
            lblFimStandardChargeTimes.setText(String.valueOf(0));
            lblFimStandardMonthCount.setText(String.valueOf(0));
            imgFimStandardChargeTimesLess.setClickable(enable);
            imgFimStandardChargeTimesPlus.setClickable(enable);
            imgFimStandardMonthCountLess.setClickable(enable);
            imgFimStandardMonthCountPlus.setClickable(enable);
        }
    }

    /**
     * TODO: On/Off Fim+
     */
    private void enableFimPlus(boolean enable) {
        if (enable) {
            if (chbFimPlus.isChecked()) {
                imgFimPlusChargeTimesLess.setClickable(enable);
                imgFimPlusChargeTimesPlus.setClickable(enable);
                imgFimPlusMonthCountLess.setClickable(enable);
                imgFimPlusMonthCountPlus.setClickable(enable);
            }
        } else {
            lblFimPlusChargeTimes.setText(String.valueOf(0));
            lblFimPlusMonthCount.setText(String.valueOf(0));
            imgFimPlusChargeTimesLess.setClickable(enable);
            imgFimPlusChargeTimesPlus.setClickable(enable);
            imgFimPlusMonthCountLess.setClickable(enable);
            imgFimPlusMonthCountPlus.setClickable(enable);
        }
    }

    /**
     * TODO: On/Off Fim Hot
     */
    private void enableFimHot(boolean enable) {
        if (enable) {
            if (chbFimHot.isChecked()) {
                imgFimHotChargeTimesLess.setClickable(enable);
                imgFimHotChargeTimesPlus.setClickable(enable);
                imgFimHotMonthCountLess.setClickable(enable);
                imgFimHotMonthCountPlus.setClickable(enable);
            }
        } else {
            lblFimHotChargeTimes.setText(String.valueOf(0));
            lblFimHotMonthCount.setText(String.valueOf(0));
            imgFimHotChargeTimesLess.setClickable(enable);
            imgFimHotChargeTimesPlus.setClickable(enable);
            imgFimHotMonthCountLess.setClickable(enable);
            imgFimHotMonthCountPlus.setClickable(enable);
        }
    }

    /**
     * TODO: On/Off K+
     */
    private void enableKPlus(boolean enable) {
        if (enable) {
            if (chbKPlus.isChecked()) {
                imgKPlusChargeTimesLess.setClickable(enable);
                imgKPlusChargeTimesPlus.setClickable(enable);
                imgKPlusMonthCountLess.setClickable(enable);
                imgKPlusMonthCountPlus.setClickable(enable);
            }
        } else {
            lblKPlusChargeTimes.setText(String.valueOf(0));
            lblKPlusMonthCount.setText(String.valueOf(0));
            imgKPlusChargeTimesLess.setClickable(enable);
            imgKPlusChargeTimesPlus.setClickable(enable);
            imgKPlusMonthCountLess.setClickable(enable);
            imgKPlusMonthCountPlus.setClickable(enable);
        }
    }


    /**
     * TODO: On/Off Dac sac HD
     */
    private void enableDacSacHD(boolean enable) {
        if (enable) {
            if (chbDacSacHD.isChecked()) {
                imgDacSacHDChargeTimesLess.setClickable(enable);
                imgDacSacHDChargeTimesPlus.setClickable(enable);
                imgDacSacHDMonthCountLess.setClickable(enable);
                imgDacSacHDMonthCountPlus.setClickable(enable);
            }
        } else {
            lblDacSacHDChargeTimes.setText(String.valueOf(0));
            lblDacSacHDMonthCount.setText(String.valueOf(0));
            imgDacSacHDChargeTimesLess.setClickable(enable);
            imgDacSacHDChargeTimesPlus.setClickable(enable);
            imgDacSacHDMonthCountLess.setClickable(enable);
            imgDacSacHDMonthCountPlus.setClickable(enable);
        }
    }

    /**
     * TODO: On/Off VTV Cab
     */
    private void enableVTVCab(boolean enable) {
        if (enable) {
            if (chbVTVCab.isChecked()) {
                imgVTVCabChargeTimesLess.setClickable(enable);
                imgVTVCabChargeTimesPlus.setClickable(enable);
                imgVTVCabMonthCountLess.setClickable(enable);
                imgVTVCabMonthCountPlus.setClickable(enable);
            }
        } else {
            lblVTVCabChargeTimes.setText(String.valueOf(0));
            lblVTVCabMonthCount.setText(String.valueOf(0));
            imgVTVCabChargeTimesLess.setClickable(enable);
            imgVTVCabChargeTimesPlus.setClickable(enable);
            imgVTVCabMonthCountLess.setClickable(enable);
            imgVTVCabMonthCountPlus.setClickable(enable);
        }
    }


    /**
     * TODO: On/Off IPTV (khi giảm Box = 0 - OFF, Box > 0 - ON)
     */
    private void enableIPTV(boolean enabled) {
        try {
            lblPLCCount.setEnabled(enabled);
            lblSTBReturn.setEnabled(enabled);
            lblChargeTimes.setEnabled(enabled);
            spPackage.setEnabled(enabled);
            if (isInternetOnly) {
                txtPromotionBox1.setEnabled(enabled);
            }
            txtPromotionBox2.setEnabled(enabled);
            enableDacSacHD(enabled);
            chbDacSacHD.setEnabled(enabled);
            enableFimHot(enabled);
            chbFimHot.setEnabled(enabled);
            enableFimPlus(enabled);
            chbFimPlus.setEnabled(enabled);
            enableFimStandard(enabled);
            chbFimStandard.setEnabled(enabled);
            enableKPlus(enabled);
            chbKPlus.setEnabled(enabled);
            enableVTVCab(enabled);
            chbVTVCab.setEnabled(enabled);
//            cbFoxy2.setEnabled(enabled);
//            cbFoxy4.setEnabled(enabled);

            if (!enabled) {
                chbFimStandard.setChecked(false);
                chbDacSacHD.setChecked(false);
                chbFimHot.setChecked(false);
                chbFimPlus.setChecked(false);
                chbKPlus.setChecked(false);
                chbVTVCab.setChecked(false);
                spPackage.setSelection(0, true);
                if (isInternetOnly) {
                    txtPromotionBox1.setText(null);
                    promotionIPTVModelBox1 = null;
                }
                lblAmountPromotionBoxOrder.setText("");
                lblAmountPromotionBoxOrder.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}