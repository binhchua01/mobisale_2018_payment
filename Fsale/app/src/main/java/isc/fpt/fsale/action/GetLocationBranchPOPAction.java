package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.ui.fragment.BookPortADSL;
import isc.fpt.fsale.ui.fragment.BookPortFTTH;
import isc.fpt.fsale.ui.fragment.BookPort_FTTH_New;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;
import android.support.v4.app.FragmentManager;

public class GetLocationBranchPOPAction implements AsyncTaskCompleteListener<String> {
    private final String GET_POP_LOCATION = "GetLocationBranchPOP";
    private Context mContext;
    private String registerNumber, Id;
    private FragmentManager fm;
    private RowBookPortModel selectedItem;
    private RegistrationDetailModel modelRegister;
    private Location currentLocationDevice;
    private Location locationMarker;

    public GetLocationBranchPOPAction(FragmentManager fm, Context mContext,
                                      RowBookPortModel selectedItem, String registerNumber,
                                      String Id, RegistrationDetailModel modelRegister) {
        this.fm = fm;
        this.mContext = mContext;
        this.registerNumber = registerNumber;
        this.Id = Id;
        this.selectedItem = selectedItem;
        this.modelRegister = modelRegister;
        String TypeService = String.valueOf(this.selectedItem.GetTypeService());
        String[] params = new String[]{this.selectedItem.getTDName(), TypeService};
        String[] paramNames = new String[]{"TDName", "Type"};
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(this.mContext, GET_POP_LOCATION,
                paramNames, params, Services.JSON_POST, message, GetLocationBranchPOPAction.this);
        service.execute();
    }

    public GetLocationBranchPOPAction(FragmentManager fm, Context mContext, RowBookPortModel selectedItem,
                                      String registerNumber, String Id, RegistrationDetailModel modelRegister,
                                      Location currentLocationDevice, Location locationMarker) {
        this.fm = fm;
        this.mContext = mContext;
        this.registerNumber = registerNumber;
        this.Id = Id;
        this.selectedItem = selectedItem;
        this.modelRegister = modelRegister;
        this.currentLocationDevice = currentLocationDevice;
        this.locationMarker = locationMarker;
        String TypeService = String.valueOf(this.selectedItem.GetTypeService());
        String[] params = new String[]{this.selectedItem.getTDName(), TypeService};
        String[] paramNames = new String[]{"TDName", "Type"};
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(this.mContext, GET_POP_LOCATION, paramNames, params,
                Services.JSON_POST, message, GetLocationBranchPOPAction.this);
        service.execute();
    }

    public void handleGetDistrictsResult(String json) {
        if (Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if (jsObj != null) {
                bindData(jsObj);
            }
        }
    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {
            String TAG_GET_POP_lOCATION_RESULT = "GetLocationBranchPOPResult";
            jsArr = jsObj.getJSONArray(TAG_GET_POP_lOCATION_RESULT);
            int l = jsArr.length();
            if (l > 0) {
                String error = jsArr.getJSONObject(0).getString("ErrorService");
                if (error.equals("null")) {
                    JSONObject iObj = jsArr.getJSONObject(0);

                    selectedItem.setBranchID(iObj.getInt("BranchID"));
                    selectedItem.setLocationID(iObj.getInt("LocationID"));
                    selectedItem.setTDID(iObj.getInt("TDID"));

                    selectedItem.SetLocationName(iObj.getString("LocationName"));
                    selectedItem.SetNameBranch(iObj.getString("NameBranch"));
                    selectedItem.SetPopName(iObj.getString("PopName"));

                    BookPortFTTH dialogFTTH = new BookPortFTTH(mContext, selectedItem, this.registerNumber,
                            this.Id, this.modelRegister, this.currentLocationDevice, this.locationMarker);

                    BookPort_FTTH_New dialogFTTH_New = new BookPort_FTTH_New(mContext, selectedItem,
                            this.registerNumber, this.Id, this.modelRegister, this.currentLocationDevice, this.locationMarker);

                    BookPortADSL dialogADSL = new BookPortADSL(mContext, selectedItem, this.registerNumber, this.Id,
                            this.modelRegister, this.currentLocationDevice, this.locationMarker);

                    // Kiểm tra TD có thuộc vùng miền của sale không
                    if (iObj.getString("LocationID").equals(Constants.PORT_LOCATION_ID)) {

                        if (selectedItem.GetTypeService() == 0) {
                            Common.showFragmentDialog(fm, dialogADSL, "fragment_bookPort_dialog");
                        } else {
                            if (selectedItem.GetTypeService() == 1) {
                                Common.showFragmentDialog(fm, dialogFTTH, "fragment_bookPort_dialog");
                            } else {
                                Common.showFragmentDialog(fm, dialogFTTH_New, "fragment_bookPort_dialog");
                            }
                        }
                    } else
                        Common.alertDialog(mContext.getResources()
                                .getString(R.string.msg_permission_access_denied_td), mContext);
                } else {
                    Common.alertDialog("Lỗi WS: " + error, mContext);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        handleGetDistrictsResult(result);
    }
}
