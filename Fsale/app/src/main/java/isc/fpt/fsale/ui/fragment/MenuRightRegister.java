package isc.fpt.fsale.ui.fragment;

import isc.fpt.fsale.action.GetListRegistration;
import isc.fpt.fsale.activity.ListObjectActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegistrationListActivity;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.ui.choose_service.ChooseServiceActivity;
import isc.fpt.fsale.utils.Constants;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MenuRightRegister extends ListFragment {
    private static final int TAG_GROUP = 0;
    private RightMenuListItem itemCreateRegister;
    private RightMenuListItem itemListRegister;
    private RightMenuListItem itemSearchObject;
    private Context mContext;
    private RegistrationDetailModel mModel = null;

    public MenuRightRegister() {
    }

    @SuppressLint("ValidFragment")
    public MenuRightRegister(RegistrationDetailModel model) {
        mModel = model;
    }

    @SuppressLint("InflateParams")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_menu, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mContext = getActivity();
        RightMenuListAdapter adapter = new RightMenuListAdapter(this.mContext);
        if (mModel == null || mModel.getID() > 0) {
            RightMenuListItem itemGroup = new RightMenuListItem(getString(R.string.menu_list_registration), -1);
            itemCreateRegister = new RightMenuListItem(getString(R.string.menu_create_registration), R.drawable.ic_menu_create_registration);
            itemListRegister = new RightMenuListItem(getString(R.string.lbl_register_list), R.drawable.ic_menu_list_registration);
            itemSearchObject = new RightMenuListItem(getString(R.string.menu_search_object), R.drawable.ic_find);
            adapter.add(itemGroup);
            adapter.add(itemCreateRegister);
            adapter.add(itemListRegister);
        }
        //add menu items
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView lv, View v, int position, long id) {
        Constants.SLIDING_MENU.toggle();
        RightMenuListItem item = (RightMenuListItem) getListAdapter().getItem(position);
        if (item != null) {
            if (item == itemCreateRegister) {
                if (!getActivity().getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
                    Intent intent = new Intent(mContext, ChooseServiceActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(intent);
                }
            } else if (item == itemSearchObject) {
                if (!getActivity().getClass().getSimpleName().equals(ListObjectActivity.class.getSimpleName())) {
                    Intent intent = new Intent(mContext, ListObjectActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getActivity().startActivity(intent);
                }
            } else if (item == itemListRegister) {
                if (!getActivity().getClass().getSimpleName()
                        .equals(RegistrationListActivity.class.getSimpleName())) {
                    new GetListRegistration(mContext, Constants.USERNAME, 1,
                            0, "", false);
                }
            }
        }
    }

    /*
     * MODEL: RightMenuListItem
     */
    public class RightMenuListItem {
        public String tag;
        int iconRes;

        RightMenuListItem(String tag, int iconRes) {
            this.tag = tag;
            this.iconRes = iconRes;
        }
    }

    /*
     * ADAPTER: RightMenuListAdapter
     */
    public class RightMenuListAdapter extends ArrayAdapter<RightMenuListItem> {
        RightMenuListAdapter(Context context) {
            super(context, 0);
        }

        @SuppressLint("InflateParams")
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_menu, null);
            }

            TextView title = (TextView) convertView.findViewById(R.id.item_title);
            title.setText(getItem(position).tag);
            ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
            if (getItem(position).iconRes != -1) {
                icon.setImageResource(getItem(position).iconRes);
            }
            if (position == TAG_GROUP) {
                title.setTypeface(null, Typeface.BOLD);
                title.setTextSize(15);
                title.setTextColor(getResources().getColor(R.color.text_header_slide));
                title.setPadding(0, 10, 0, 10);
                // set background for header slide
                convertView.setBackgroundColor(getResources().getColor(R.color.bg_header_slide));
            } else {
                title.setPadding(0, 20, 0, 20);
            }
            return convertView;
        }
    }
}