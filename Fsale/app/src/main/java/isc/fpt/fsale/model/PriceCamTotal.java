package isc.fpt.fsale.model;

/**
 * Created by haulc3 on 07,January,2019
 */
public class PriceCamTotal {
    private int Total;

    public PriceCamTotal(int total) {
        Total = total;
    }

    public PriceCamTotal() {
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }
}
