package isc.fpt.fsale.action;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.CategoryServiceList;
import isc.fpt.fsale.model.LocalType;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.activity.local_type.LocalTypeActivity;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

/**
 * Created by haulc3 on 19,July,2019
 */
public class GetListLocalType implements AsyncTaskCompleteListener<String> {
    private LocalTypeActivity activity;

    public GetListLocalType(LocalTypeActivity activity, List<CategoryServiceList> mList) {
        this.activity = activity;
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        for (CategoryServiceList item : mList) {
            jsonArray.put(item.getCategoryServiceID());
        }
        try {
            jsonObject.put("UserName", Constants.USERNAME);
            jsonObject.put("ListCategoryService", jsonArray);
            String message = activity.getResources().getString(R.string.txt_message_get_list_local_type);
            String GET_LIST_LOCAL_TYPE = "GetListLocalType_V2";
            CallServiceTask service = new CallServiceTask(activity, GET_LIST_LOCAL_TYPE, jsonObject,
                    Services.JSON_POST_OBJECT, message, GetListLocalType.this);
            service.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            List<LocalType> lst = new ArrayList<>();
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                JSONArray jsonArray = jsObj.getJSONArray(Constants.LIST_OBJECT);
                for (int i = 0; i < jsonArray.length(); i++) {
                    lst.add(new Gson().fromJson(jsonArray.get(i).toString(), LocalType.class));
                }
                //callback to activity
                String ERROR_CODE = "ErrorCode";
                String ERROR = "Error";
                if (Integer.parseInt(String.valueOf(jsObj.get(ERROR_CODE))) < 0) {
                    Common.alertDialog(String.valueOf(jsObj.get(ERROR)), activity);
                } else {
                    activity.loadLocalType(lst);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}