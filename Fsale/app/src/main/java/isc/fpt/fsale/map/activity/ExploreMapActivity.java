package isc.fpt.fsale.map.activity;

import isc.fpt.fsale.activity.BaseActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.action.GetRouteTask;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.map.utils.MapConstants;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;


import com.slidingmenu.lib.SlidingMenu;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

// màn hình Bản đồ khảo sát
public class ExploreMapActivity extends BaseActivity implements OnMapReadyCallback {
    private Button bntImg, btnDrawingPath;
    private Context mContext;
    private GoogleMap mMap;
    private LatLng latLngTDName;
    private LatLng latLngCustomer;
    private String TDName, IndoorCable, IndoorLog, OutdoorCable, OutdoorLog;
    private TextView txtIndoorCable, txtIndoorLog, txtOutdoorCable, txtOutdoorLog;
    private LinearLayout frmInfo;
    private RegistrationDetailModel modelDetail;
    private static boolean flag_manual_mode = false;
    private Button btnManualMode;
    private List<Polyline> lstPolyline;
    private LatLng mLastPointPolyline = null;
    private LinearLayout frmUndoRedo;
    private Button btnUndo;
    private Button btnScreenShot;
    private Bitmap bitmap;
    private Marker mkrCustomer;

    public ExploreMapActivity() {
        super(R.string.lbl_MapView);
        MyApp.setCurrentActivity(this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_explore_map_activity));
        getSlidingMenu().setSlidingEnabled(false);
        setContentView(R.layout.explore_map_layout);

        this.mContext = this;
        lstPolyline = new ArrayList<>();

        txtIndoorCable = (TextView) findViewById(R.id.txt_indoor_cable);
        txtIndoorLog = (TextView) findViewById(R.id.txt_indoor_long);
        txtOutdoorCable = (TextView) findViewById(R.id.txt_outdoor_cable);
        txtOutdoorLog = (TextView) findViewById(R.id.txt_outdoor_long);

        frmInfo = (LinearLayout) findViewById(R.id.frm_info);
        frmInfo.setAlpha((float) 0.8);

        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                modelDetail = myIntent.getParcelableExtra(Constants.MODEL_REGISTER);
                IndoorCable = myIntent.getStringExtra("IndoorCable");
                IndoorLog = myIntent.getStringExtra("IndoorLog");
                OutdoorCable = myIntent.getStringExtra("OutdoorCable");
                OutdoorLog = myIntent.getStringExtra("OutdoorLog");
                TDName = modelDetail.getTDName();
                txtIndoorCable.setText(IndoorCable);
                txtIndoorLog.setText(" (" + IndoorLog + " m ) ");
                txtOutdoorCable.setText(OutdoorCable);
                txtOutdoorLog.setText(" (" + OutdoorLog + " m ) ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Set map
        ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMapAsync(this);
        //Create events
        createEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void createEvents() {
        bntImg = (Button) this.findViewById(R.id.btn_map_mode);
        bntImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                captureScreen();
                Common.alertDialog("Chụp hình thành công !", mContext);
            }
        });

        btnDrawingPath = (Button) this.findViewById(R.id.btn_drawing_path);
        btnDrawingPath.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                DrawingPath();
            }
        });
        //Button vẽ thủ công
        frmUndoRedo = (LinearLayout) this.findViewById(R.id.frm_undo_redo);
        if (!flag_manual_mode)
            frmUndoRedo.setVisibility(View.GONE);
        btnUndo = (Button) this.findViewById(R.id.btn_undo);
        btnUndo.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                if (lstPolyline.size() > 0) {
                    Polyline curPolyline = lstPolyline.get(lstPolyline.size() - 1);//Lay duong thang sau cung
                    mLastPointPolyline = curPolyline.getPoints().get(0);//Lay diem dau cua duong thang
                    curPolyline.remove();//Xoa duong thang
                    lstPolyline.remove(lstPolyline.size() - 1);//Xoa trong danh sách

                } else {
                    btnUndo.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_undo_empty));
                }
                flag_manual_mode = true;//Neu đã đến VT khách hàng mà back lại
            }
        });

        btnManualMode = (Button) this.findViewById(R.id.btn_drawing_manual_mode);
        btnManualMode.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                flag_manual_mode = !flag_manual_mode;
                //Chuyen sang che do ve thu cong
                try {
                    if (mLastPointPolyline == null || mLastPointPolyline == latLngTDName) {
                        mMap.clear();
                        addCusMarker();
                        addTDMarker();

                    }
                    if (flag_manual_mode) {
                        btnManualMode.setBackgroundDrawable(getResources()
                                .getDrawable(R.drawable.shape_mapmode_button_select));
                        frmUndoRedo.setVisibility(View.VISIBLE);
                    } else {
                        btnManualMode.setBackgroundDrawable(getResources()
                                .getDrawable(R.drawable.shape_map_mode_button));
                        frmUndoRedo.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        //ScreenShot
        btnScreenShot = (Button) findViewById(R.id.btn_screen_shot);
        btnScreenShot.setVisibility(View.GONE);
        btnScreenShot.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                captureScreen();
            }
        });

    }

    private void setEventsShowOnMap() {
        try {
            String latlng = modelDetail.getLatlng();
            if (latlng != null)
                latlng = latlng.replace("(", "").replace(")", "");
            assert latlng != null;
            String[] LatlngTD = latlng.split(",");
            if (LatlngTD.length > 1) {
                double LatlngX = Double.parseDouble(LatlngTD[0]);
                double LatlngY = Double.parseDouble(LatlngTD[1]);
                latLngTDName = new LatLng(LatlngX, LatlngY);
                addTDMarker();
            }

        } catch (Exception e) {

            e.printStackTrace();
        }


        // lay vi tri hien tai
        LatLng currentLocation;
        GPSTracker gpsTracker = new GPSTracker(mContext);
        if (gpsTracker.getLocation() != null) {
            currentLocation = new LatLng(GPSTracker.location.getLatitude(), GPSTracker.location.getLongitude());
            latLngCustomer = currentLocation;
            addCusMarker();
        }

        //Su kien tren Map
        mMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {

            @Override
            public boolean onMyLocationButtonClick() {
                LatLng latLng;
                GPSTracker gpsTracker = new GPSTracker(mContext);
                if (gpsTracker.getLocation() != null) {
                    latLng = new LatLng(GPSTracker.location.getLatitude(), GPSTracker.location.getLongitude());
                    latLngCustomer = latLng;
                    addCusMarker();
                }
                return false;
            }
        });
        mMap.setOnMarkerDragListener(new OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                if (marker != null) {
                    String title = marker.getTitle();
                    // Vị trí TD
                    if (title != null && title.equals(mContext.getResources().getString(R.string.lbl_ability_pay)))
                        latLngTDName = marker.getPosition();
                    else {
                        // Vị trí của khách hàng
                        latLngCustomer = marker.getPosition();
                    }
                }
            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }
        });

        //Vẽ thủ công
        mMap.setOnMapClickListener(new OnMapClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onMapClick(LatLng touchLatlng) {
                if (flag_manual_mode) {
                    if (mLastPointPolyline == null)
                        mLastPointPolyline = latLngTDName;
                    if (latLngTDName != null && touchLatlng != null) {
                        Polyline line = mMap.addPolyline(
                                new PolylineOptions().add(mLastPointPolyline,
                                        touchLatlng).width(5).color(Color.DKGRAY));
                        lstPolyline.add(line);
                        mLastPointPolyline = touchLatlng;
                        btnUndo.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_undo));
                    }
                }
            }
        });

        //Vẽ Polyline cuoi cung từ điểm hiện tại đến Vị trí nhà khách hàng
        mMap.setOnMarkerClickListener(new OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker != null) {
                    String title = marker.getTitle();
                    // Vị trí nhà khách hàng
                    if (title != null && !title.equals(mContext.getResources().getString(R.string.lbl_ability_pay))) {
                        if (flag_manual_mode && mLastPointPolyline != null) {
                            Polyline line = mMap.addPolyline(
                                    new PolylineOptions().add(mLastPointPolyline,
                                            marker.getPosition()).width(5).color(Color.DKGRAY));
                            lstPolyline.add(line);
                            mLastPointPolyline = marker.getPosition();
                            flag_manual_mode = false;
                        }
                    }
                }
                return false;
            }
        });
        //Drawing Path
        DrawingPath();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else if (itemId == R.id.action_right) {
            toggle(SlidingMenu.RIGHT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @SuppressWarnings("deprecation")
    public void DrawingPath() {
        try {
            lstPolyline.clear();
            mLastPointPolyline = null;
            flag_manual_mode = false;
            btnManualMode.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_map_mode_button));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // ve duong di giua 2 diem
        if (latLngTDName != null && latLngCustomer != null) {
            GetRouteTask getRoute = new GetRouteTask(
                    ExploreMapActivity.this, latLngTDName, latLngCustomer, TDName);
            getRoute.execute();
        }
    }

    public Marker addMarker(LatLng latlng, String title, int dr, boolean isDraggable) {
        Marker marker = null;
        try {
            marker = MapCommon.addMarkerOnMap(title, latlng, dr, isDraggable);
            marker.showInfoWindow();
            MapCommon.animeToLocation(latlng);
            if (!flag_manual_mode) {
                MapCommon.setMapZoom(18);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return marker;
    }

    private void addCusMarker() {
        if (latLngCustomer != null) {
            if (mkrCustomer != null)
                mkrCustomer.remove();
            mkrCustomer = addMarker(latLngCustomer, mContext.getString(R.string.title_cuslocation), R.drawable.icon_home, true);
        }
    }

    private void addTDMarker() {
        if (latLngTDName != null) {
            addMarker(latLngTDName, mContext.getString(R.string.title_TDlocation), R.drawable.cuslocation_black, true);
        }
    }

    public void captureScreen() {
        SnapshotReadyCallback callback = new SnapshotReadyCallback() {
            @SuppressLint("WorldReadableFiles")
            @SuppressWarnings("deprecation")
            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                bitmap = snapshot;
                OutputStream os;
                String filePath = System.currentTimeMillis() + ".png";

                try {
                    os = openFileOutput(filePath, MODE_WORLD_READABLE);
                    // Write the string to the file
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, os);
                    os.flush();
                    os.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    filePath = "";
                } catch (IOException e) {
                    e.printStackTrace();
                    filePath = "";
                }
                openShareImageDialog(filePath);
            }
        };
        mMap.snapshot(callback);
    }

    public void openShareImageDialog(String filePath) {
        File file = this.getFileStreamPath(filePath);

        if (!filePath.equals("")) {
            final ContentValues values = new ContentValues(2);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
            values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
            final Uri contentUriFile = getContentResolver()
                    .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            final Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/jpeg");
            intent.putExtra(Intent.EXTRA_STREAM, contentUriFile);
            startActivity(Intent.createChooser(intent, "Share Image"));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            MapConstants.MAP = mMap;
            setEventsShowOnMap();
        }
    }
}



	
		
			
		
		
		
