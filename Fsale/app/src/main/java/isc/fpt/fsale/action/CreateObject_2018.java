package isc.fpt.fsale.action;

import android.content.Context;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.activity.CreateContractActivity;
import isc.fpt.fsale.activity.PayContractActivity;
import isc.fpt.fsale.model.CreateObject_2018POST;
import isc.fpt.fsale.model.CreateObject_2018Result;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Common.checkLifeActivity;

// API tạo hợp đồng thanh toán online bán mới
public class CreateObject_2018 implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private CreateObject_2018POST createObject_2018POST;
    private RegistrationDetailModel mRegister;

    public CreateObject_2018(Context context, CreateObject_2018POST mObject, RegistrationDetailModel mRegister) {
        this.mContext = context;
        this.createObject_2018POST = mObject;
        this.mRegister = mRegister;
        //Declares variables
        String[] arrParamName = new String[]{"UserName", "RegCode", "Total", "ImageSignature"};
        String[] arrParamValue = new String[]{mObject.getUserName(), mObject.getRegCode(),
                mObject.getTotal(), mObject.getImageSignature()};
        String message = "Đang tạo hợp đồng...";
        String POST_METHOD_NAME = "CreateObject_2018";
        CallServiceTask service = new CallServiceTask(mContext, POST_METHOD_NAME, arrParamName,
                arrParamValue, Services.JSON_POST, message, CreateObject_2018.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<CreateObject_2018Result> objList;
            if (Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                jsObj = jsObj.getJSONObject("CreateObject_2018Result");
                WSObjectsModel<CreateObject_2018Result> resultObject =
                        new WSObjectsModel<>(jsObj, CreateObject_2018Result.class);
                if (resultObject.getErrorCode() == 0) {
                    objList = resultObject.getArrayListObject();
                    if (objList.size() > 0) {
                        if (objList.get(0).getResultID() >= 0) {
                            Intent intent = new Intent(mContext, PayContractActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("CREATE_OBJECT_2018_POST", createObject_2018POST);
                            intent.putExtra(Constants.MODEL_REGISTER, mRegister);
                            mContext.startActivity(intent);
                            if (mContext.getClass().getSimpleName().equals(CreateContractActivity.class.getSimpleName())) {
                                CreateContractActivity activity = (CreateContractActivity) mContext;
                                if (checkLifeActivity(mContext)) {
                                    activity.finish();
                                }
                            }
                        } else {
                            Common.alertDialog(objList.get(0).getResult(), mContext);
                        }
                    } else {
                        Common.alertDialog(resultObject.getError(), mContext);
                    }
                } else {
                    Common.alertDialog(resultObject.getError(), mContext);
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }
}