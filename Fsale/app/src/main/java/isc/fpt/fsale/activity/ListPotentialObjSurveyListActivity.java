package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPotentialObjSurveyList;
import isc.fpt.fsale.ui.fragment.FragmentPotentialObjSurveyList;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.PotentialObjSurveyModel;
import isc.fpt.fsale.model.PotentialObjSurveyValueModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.view.Menu;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class ListPotentialObjSurveyListActivity extends BaseActivity implements OnTabChangeListener {
    public final String TAG_TAB_ITEM = "POTENTIAL_SURVEY_TAP_";

    public static FragmentManager fragmentManager;
    private FragmentTabHost mTabHost;
    public int currentPos = 0;
    private PotentialObjModel mPotentialObj;
    private String support;
    @SuppressLint("UseSparseArrays")
    private HashMap<Integer, List<PotentialObjSurveyModel>> hashMap = new HashMap<>();

    public ListPotentialObjSurveyListActivity() {
        super(R.string.lbl_screen_name_potential_obj_survey);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_potential_obj_survey));
        setContentView(R.layout.list_potentail_obj_survey);
        fragmentManager = getSupportFragmentManager();
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.real_tab_content);
        mTabHost.setOnTabChangedListener(this);
        getDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            this.mPotentialObj = intent.getParcelableExtra(Constants.POTENTIAL_OBJECT);
            this.support = intent.getStringExtra(PotentialObjDetailActivity.TAG_SUPPORTER);
            if (mPotentialObj != null)
                getData(mPotentialObj.getID(), 1);
        }
    }

    public PotentialObjModel getPotentialObj() {
        return mPotentialObj;
    }

    public String getSupport() {
        return support;
    }

    public void setSupport(String support) {
        this.support = support;
    }

    private void getData(int PotentialObjID, int PageNumber) {
        new GetPotentialObjSurveyList(this, PotentialObjID, 0, "", PageNumber);
    }

    public void loadData(List<PotentialObjSurveyModel> lst) {
        if (lst != null && lst.size() > 0) {
            try {
                mTabHost.clearAllTabs();
                int row = 1;
                for (int i = 0; i < lst.size(); i++) {
                    PotentialObjSurveyModel item = lst.get(i);
                    item.setRowNumber(row++);
                    if (!hashMap.containsKey(item.getCategory())) {
                        List<PotentialObjSurveyModel> list = new ArrayList<>();
                        list.add(item);
                        hashMap.put(item.getCategory(), list);
                    } else {
                        hashMap.get(item.getCategory()).add(item);
                    }
                }
                initTab();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Common.alertDialog("Không có dữ liệu", this);
        }

    }

    @SuppressLint("ResourceAsColor")
    private void initTab() {
        try {
            Map<Integer, List<PotentialObjSurveyModel>> map = new TreeMap<>(hashMap);
            for (int key : map.keySet()) {
                String tabName = "";
                if (hashMap.get(key).size() > 0)
                    tabName = hashMap.get(key).get(0).getCategoryName();
                TabSpec tab = mTabHost.newTabSpec(TAG_TAB_ITEM + key);
                tab.setIndicator(tabName);
                mTabHost.addTab(tab, FragmentPotentialObjSurveyList.class, null);
            }
            for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
                mTabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.color.main_color_light);
                TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
                tv.setTextColor(Color.WHITE);
            }
            mTabHost.setCurrentTab(0);
            mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.color.theme_color);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<PotentialObjSurveyModel> getSurveyListAllTab() {
        List<PotentialObjSurveyModel> lst = new ArrayList<>();
        for (int key : hashMap.keySet()) {
            try {
                String tabSpec = TAG_TAB_ITEM + key;
                if (getFragment(tabSpec) != null)
                    lst.addAll(getFragment(tabSpec).getSurveyList());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lst;
    }

    public List<PotentialObjSurveyModel> getSurveySubList(String tagName) {
        try {
            int key = Integer.valueOf(tagName.substring(tagName.length() - 1));
            return hashMap.get(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public FragmentPotentialObjSurveyList getFragment(String TAB_NAME) {
        try {
            return (FragmentPotentialObjSurveyList) getSupportFragmentManager().findFragmentByTag(TAB_NAME);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateCount(String tabName) {
        try {
            int selected = 0, total;
            if (getSurveySubList(tabName) != null) {
                total = getSurveySubList(tabName).size();
                for (PotentialObjSurveyModel item : getSurveySubList(tabName)) {
                    if (item.getValues() != null) {
                        for (PotentialObjSurveyValueModel subItem : item.getValues())
                            if (subItem.getSelected() > 0)
                                selected++;
                    }
                }
                String s = getSurveySubList(tabName).get(0).getCategoryName() + "\n";
                s += selected + "/" + total;
                int key = Integer.valueOf(tabName.substring(tabName.length() - 1));
                TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(key).findViewById(android.R.id.title);
                tv.setText(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Common.reportActivityStop(this, this);
    }

    @Override
    public void onTabChanged(String tabId) {
        int nextPos = this.mTabHost.getCurrentTab();
        if (preventTabHost())
            currentPos = nextPos;

        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            mTabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.color.main_color_light);
        }
        mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab()).setBackgroundResource(R.color.theme_color);
    }

    private boolean preventTabHost() {
        return false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;
        Dialog dialog;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.msg_confirm_to_back))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.lbl_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.lbl_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
