package isc.fpt.fsale.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danh32.fontify.Button;
import com.danh32.fontify.EditText;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.camera319.GetCamera2Total;
import isc.fpt.fsale.action.camera319.GetCostSetupCamerav2;
import isc.fpt.fsale.action.maxy.GetMaxyTotal;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.camera319.PromoCloud;
import isc.fpt.fsale.ui.adapter.CameraDetailContractAdapter;
import isc.fpt.fsale.ui.adapter.CloudDetailContractAdapter;
import isc.fpt.fsale.ui.callback.OnItemClickListener;
import isc.fpt.fsale.ui.callback.OnItemClickListenerV4;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_package.CameraOrNetPackageActivity;
import isc.fpt.fsale.ui.fpt_camera.camera_detail.camera_type.CameraOrNetTypeActivity;
import isc.fpt.fsale.ui.fpt_camera.cloud_detail.netorcamera.cloudtype.CloudTypeV2Activity;
import isc.fpt.fsale.ui.fpt_camera.model.CameraDetail;
import isc.fpt.fsale.ui.fpt_camera.model.CloudDetail;
import isc.fpt.fsale.ui.fpt_camera.model.ObjectCameraOfNet;
import isc.fpt.fsale.ui.fpt_camera.model.SetupDetail;
import isc.fpt.fsale.ui.maxytv.model.MaxyBox;
import isc.fpt.fsale.ui.maxytv.model.MaxyGeneral;
import isc.fpt.fsale.ui.maxytv.model.ObjectMaxy;
import isc.fpt.fsale.ui.promo_cloud.PromoCloudActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import static isc.fpt.fsale.utils.Constants.CAMERA_DETAIL;
import static isc.fpt.fsale.utils.Constants.CLASS_NAME;
import static isc.fpt.fsale.utils.Constants.CLOUD_DETAIL;
import static isc.fpt.fsale.utils.Constants.CUSTYPE;
import static isc.fpt.fsale.utils.Constants.LIST_CAMERA_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.LIST_CLOUD_DETAIL_SELECTED;
import static isc.fpt.fsale.utils.Constants.NUM_CLOUD;
import static isc.fpt.fsale.utils.Constants.OBJECT_CAMERA_DETAIL;
import static isc.fpt.fsale.utils.Constants.OBJECT_PROMOTION_CLOUD_DETAIL;
import static isc.fpt.fsale.utils.Constants.POSITION;
import static isc.fpt.fsale.utils.Constants.REGTYPE;

public class FragmentRegisterContractCamera extends Fragment implements OnItemClickListenerV4<Object, Integer, Integer>, OnItemClickListener<Boolean> {
    private Context mContext;
    private final int regType = 1;
    private String mContract;
    private int isCusType;
    public RegistrationDetailModel mRegister = new RegistrationDetailModel();

    //Key Result
    private final int CODE_CAMERA_DETAIL = 1;
    private final int CODE_CAMERA_PACKAGE = 3;
    private final int CODE_CLOUD_DETAIL = 2;
    private final int CODE_CLOUD_PROMOTION = 4;

    //Model View
    public List<CameraDetail> mListCameraDetail;
    public List<CloudDetail> mListCloudDetail;
    public PromoCloud mPromotionCloud;
    public SetupDetail mSetupDetail;
    private CameraDetailContractAdapter aCameraAdapter;
    private CloudDetailContractAdapter aCloudAdapter;
    private Button bAddCamera, bAddCloud;
    private EditText bAddPromotionCloud;

    public static FragmentRegisterContractCamera newInstance() {
        FragmentRegisterContractCamera fragment = new FragmentRegisterContractCamera();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_contract_camera,
                container, false);
        try {
            Common.setupUI(getActivity(), view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mContext = getActivity();
        getRegisterFromActivity();
        initView(view);
        initEvent();
        return view;
    }

    private void initEvent() {

        bAddCamera.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(LIST_CAMERA_DETAIL_SELECTED,
                    (ArrayList<? extends Parcelable>) mListCameraDetail);
            bundle.putInt(CUSTYPE, isCusType);
            bundle.putInt(REGTYPE, regType);
            bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractCamera.class.getSimpleName());
            Intent intent = new Intent(getContext(), CameraOrNetTypeActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_CAMERA_DETAIL);
        });

        bAddCloud.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(LIST_CLOUD_DETAIL_SELECTED,
                    (ArrayList<? extends Parcelable>) mListCloudDetail);
            bundle.putInt(CUSTYPE, isCusType);
            bundle.putInt(REGTYPE, regType);
            bundle.putString(Constants.CLASS_NAME, FragmentRegisterContractCamera.class.getSimpleName());
            Intent intent = new Intent(getContext(), CloudTypeV2Activity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, CODE_CLOUD_DETAIL);
        });

        if (mListCloudDetail.size() == 0)
            setupPromotionCloud(false);
    }

    private void initView(View view) {
        RecyclerView rCameraDetail = view.findViewById(R.id.list_camera_detail);
        RecyclerView rCloudDetail = view.findViewById(R.id.list_cloud_camera_detail);
        bAddCamera = view.findViewById(R.id.btn_add_camera);
        bAddCloud = view.findViewById(R.id.btn_add_cloud_service);
        bAddPromotionCloud = view.findViewById(R.id.txt_promotion_cloud);

        if (mRegister.getObjectCameraOfNet() == null) {
            mRegister.setObjectCameraOfNet(new ObjectCameraOfNet());
        }
        //Camera detail
        mListCameraDetail = new ArrayList<>();
        mListCameraDetail = mRegister.getObjectCameraOfNet().getCameraDetail() != null ?
                mRegister.getObjectCameraOfNet().getCameraDetail() :
                new ArrayList<>();
        aCameraAdapter = new CameraDetailContractAdapter(getActivity(),
                mListCameraDetail, this, this);
        rCameraDetail.setAdapter(aCameraAdapter);

        //Cloud detail
        mListCloudDetail = new ArrayList<>();
        mListCloudDetail = mRegister.getObjectCameraOfNet().getCloudDetail() != null ?
                mRegister.getObjectCameraOfNet().getCloudDetail() :
                new ArrayList<>();
        aCloudAdapter = new CloudDetailContractAdapter(getActivity(),
                mListCloudDetail, this, this);
        rCloudDetail.setAdapter(aCloudAdapter);

        //Promotion cloud
        if (mRegister.getObjectCameraOfNet().getPromotionDetail() != null) {
            mPromotionCloud = mRegister.getObjectCameraOfNet().getPromotionDetail();
            bAddPromotionCloud.setText(mPromotionCloud.getPromoName());
            setupPromotionCloud(true);
        } else {
            mPromotionCloud = new PromoCloud();
            setupPromotionCloud(true);
        }

        //Setup Detail
        if (mRegister.getObjectCameraOfNet().getSetupDetail() != null) {
            mSetupDetail = mRegister.getObjectCameraOfNet().getSetupDetail();
        } else {
            mSetupDetail = new SetupDetail();
        }
    }

    //0: Internet Only
    //2 : Internet + IPTV
    //3: FPT Playbox
    //5: Camera OTT
    //6: Extra OTT
    private void getRegisterFromActivity() {
        if (mContext != null && mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
            mRegister = ((RegisterContractActivity) mContext).getRegister() == null ? new RegistrationDetailModel()
                    : ((RegisterContractActivity) mContext).getRegister();
            ObjectDetailModel mObject = ((RegisterContractActivity) mContext).getObject();
            if (mObject != null) {
                mContract = mObject.getContract();
                isCusType = mObject.getCusType();
                mRegister.setCusType(isCusType);
                mRegister.setRegType(1);
            } else {
                if (mRegister != null) {
                    mContract = mRegister.getContract();
                    isCusType = mRegister.getCusType();
                }
            }
        }
    }

    public void updateRegister() {
        try {
            if (mContext.getClass().getSimpleName()
                    .equals(RegisterContractActivity.class.getSimpleName())) {
                RegisterContractActivity activity = (RegisterContractActivity) mContext;
                RegistrationDetailModel register = activity.getRegister();
                register.setObjectCameraOfNet(new ObjectCameraOfNet(mListCameraDetail,
                        mListCloudDetail,
                        mSetupDetail,
                        mPromotionCloud,
                        0));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case CODE_CAMERA_DETAIL:
                    mListCameraDetail.clear();
                    List<CameraDetail> mListCamera = data.getParcelableArrayListExtra(OBJECT_CAMERA_DETAIL);
                    if (mListCamera != null) {
                        mListCameraDetail.addAll(mListCamera);
                        aCameraAdapter.notifyDataChanged(mListCameraDetail);
                    }
                    break;
                case CODE_CAMERA_PACKAGE:
                    CameraDetail mCameraDetail = data.getParcelableExtra(OBJECT_CAMERA_DETAIL);
                    if (mCameraDetail != null) {
                        int pos = data.getIntExtra(POSITION, -1);
                        mListCameraDetail.set(pos, mCameraDetail);
                        aCameraAdapter.notifyDataChanged(mListCameraDetail);
                    }
                    getCostSetupCamera();
                    break;
                case CODE_CLOUD_DETAIL:
                    CloudDetail mCloudDetail = data.getParcelableExtra(CLOUD_DETAIL);
                    if (mCloudDetail != null) {
                        for (CloudDetail item : mListCloudDetail) {
                            if (item.getPackID() == mCloudDetail.getPackID()) {
                                Common.alertDialog(getString(R.string.lbl_coincides_with_clould), mContext);
                                return;
                            }
                        }
                        mListCloudDetail.add(mCloudDetail);
                        aCloudAdapter.notifyDataChanged(mListCloudDetail);
                    }
                    clearPromotionCloud();
                    if (mListCloudDetail.size() != 0) {
                        setupPromotionCloud(true);
                    }
                    break;
                case CODE_CLOUD_PROMOTION:
                    PromoCloud mDetail = data.getParcelableExtra(OBJECT_PROMOTION_CLOUD_DETAIL);
                    if (mDetail != null) {
                        mPromotionCloud = mDetail;
                        bAddPromotionCloud.setText(mDetail.getPromoName());
                    }
                    break;
            }
        }
    }

    private RegistrationDetailModel updateRegisterFromView(RegistrationDetailModel register) {
        try {
            if (register == null)
                register = new RegistrationDetailModel();
            if (register.getObjectCameraOfNet() == null)
                register.setObjectCameraOfNet(new ObjectCameraOfNet());
            register.setRegType(1);

            if (mListCameraDetail.size() > 0) {
                register.getObjectCameraOfNet().setCameraDetail(mListCameraDetail);
            } else {
                register.getObjectCameraOfNet().setCameraDetail(new ArrayList<>());
            }

            if (mListCloudDetail.size() > 0) {
                register.getObjectCameraOfNet().setCloudDetail(mListCloudDetail);
            } else {
                register.getObjectCameraOfNet().setCloudDetail(new ArrayList<>());
            }

            if (mPromotionCloud.getPromoID() != 0) {
                register.getObjectCameraOfNet().setPromotionDetail(mPromotionCloud);
            } else {
                register.getObjectCameraOfNet().setPromotionDetail(new PromoCloud());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return register;
    }

    public void getCameraOfNetTotal(Context mContext) {
        mRegister = updateRegisterFromView(mRegister);
        if (mListCameraDetail.size() > 0) {
            for (CameraDetail item : mListCameraDetail) {
                if (item.getPackID() == 0) {
                    Common.alertDialog(getString(R.string.text_select_promotion_camera_of_net),
                            getActivity());
                    return;
                }
            }
        }

        else {
            if (mListCloudDetail.size() > 0) {
                Common.alertDialog(getString(R.string.text_select_camera_of_net),
                        getActivity());
                return;
            }
        }

        if (mListCameraDetail.size() > 0 && mListCloudDetail.size() < 0) {
            Common.alertDialog(getString(R.string.text_select_cloud_of_net),
                    getActivity());
            return;
        }

        new GetCamera2Total(mContext, mRegister, this);
    }

    private void setupPromotionCloud(boolean b) {
        bAddPromotionCloud.setOnClickListener(view -> {
            if (b) {
                Bundle bundle = new Bundle();
                bundle.putInt(CUSTYPE, isCusType);
                bundle.putInt(REGTYPE, regType);
                bundle.putParcelableArrayList(CLOUD_DETAIL,
                        (ArrayList<? extends Parcelable>) mListCloudDetail);
                bundle.putString(Constants.CLASS_NAME,
                        FragmentRegisterContractCamera.class.getSimpleName());
                Intent intent = new Intent(getContext(), PromoCloudActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_CLOUD_PROMOTION);
            } else {
                Common.alertDialog(getString(R.string.msg_select_cloud_promotion), getContext());
            }
        });
    }

    private void getCostSetupCamera() {
        new GetCostSetupCamerav2(getContext(),
                FragmentRegisterContractCamera.this, mRegister);
    }

    public void loadInfoSetupDetail(List<SetupDetail> lst) {
        mRegister.getObjectCameraOfNet().setSetupDetail(lst.get(0));
        mSetupDetail = mRegister.getObjectCameraOfNet().getSetupDetail();
        mSetupDetail.setType(1);//1 - triển khai , 0 - giao tại quầy
        //set min set up detail
        mSetupDetail.setQty(getCameraCount());
    }

    private void clearPromotionCloud() {
        mPromotionCloud = new PromoCloud();
        bAddPromotionCloud.getText().clear();
    }

    private int getCameraCount() {
        if (mListCameraDetail == null || mListCameraDetail.size() == 0) {
            return 0;
        }
        int cameraCount = 0;
        for (CameraDetail item : mListCameraDetail) {
            cameraCount += item.getQty();
        }
        return cameraCount;
    }

    @Override
    public void onItemClick(Boolean is) {
        if (is) {
            mSetupDetail.setQty(getCameraCount());
        }
    }

    @Override
    public void onItemClickV4(Object mObject, Integer position, Integer type) {
        Bundle bundle = new Bundle();
        Intent intent;
        switch (type) {
            case 1:
                CameraDetail mCameraDetail = (CameraDetail) mObject;
                bundle.putParcelable(CAMERA_DETAIL, mCameraDetail);
                bundle.putInt(POSITION, position);
                bundle.putInt(CUSTYPE, isCusType);
                bundle.putInt(REGTYPE, regType);
                bundle.putString(CLASS_NAME, FragmentRegisterContractCamera.class.getSimpleName());
                intent = new Intent(getContext(), CameraOrNetPackageActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, CODE_CAMERA_PACKAGE);
                break;
            case 3:
                CloudDetail mCloudRemove = (CloudDetail) mObject;
                mListCloudDetail.remove(mCloudRemove);
                aCloudAdapter.notifyDataChanged(mListCloudDetail);
                if (mListCloudDetail.size() == 0) {
                    setupPromotionCloud(false);
                }
                clearPromotionCloud();
                break;
            case 4:
            case 5:
                clearPromotionCloud();
                break;
        }
    }

}
