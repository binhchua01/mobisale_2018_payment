package isc.fpt.fsale.ui.fragment;

import isc.fpt.fsale.R;
import isc.fpt.fsale.utils.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.StringTokenizer;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.EditText;

import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

// màn hình chọn ngày tháng
public class DatePickerReportDialog extends DialogFragment {
    private int mDay, mMonth, mYear;
    private android.app.DatePickerDialog dialog;
    private Calendar mMaxDate, mMinDate, mCurrentDate;
    private String mTitle;
    private OnDateSetListener myDateListener;
    private EditText txtValue, txtValue1;
    private String dateSelected;
    private String valuetxt, valuetxt1;
    private int setSelectedTime;
    private boolean checkYearOld;

    public DatePickerReportDialog() {
        super();
    }

    @SuppressLint("ValidFragment")
    public DatePickerReportDialog(OnDateSetListener dateListener,
                                  Calendar minDate, Calendar maxDate, String dialogTitle) {
        this.mMaxDate = maxDate;
        this.mMinDate = minDate;
        this.mTitle = dialogTitle;
        this.myDateListener = dateListener;
    }

    @SuppressLint("ValidFragment")
    public DatePickerReportDialog(OnDateSetListener dateListener,
                                  Calendar minDate, Calendar maxDate, String dialogTitle,
                                  EditText txtValue) {
        this.mMaxDate = maxDate;
        this.mMinDate = minDate;
        this.mTitle = dialogTitle;
        this.myDateListener = dateListener;
        this.txtValue = txtValue;
    }

    @SuppressLint("ValidFragment")
    public DatePickerReportDialog(OnDateSetListener dateListener,
                                  Calendar currentDate, Calendar minDate, Calendar maxDate,
                                  String dialogTitle, EditText txtValue) {
        this.mMaxDate = maxDate;
        this.mMinDate = minDate;
        this.mTitle = dialogTitle;
        this.myDateListener = dateListener;
        this.txtValue = txtValue;
        this.mCurrentDate = currentDate;
    }

    @SuppressLint("ValidFragment")
    public DatePickerReportDialog(OnDateSetListener dateListener,
                                  Calendar currentDate, Calendar minDate, Calendar maxDate,
                                  String dialogTitle, EditText txtValue, boolean checkYearOld) {
        this.mMaxDate = maxDate;
        this.mMinDate = minDate;
        this.mTitle = dialogTitle;
        this.myDateListener = dateListener;
        this.txtValue = txtValue;
        this.mCurrentDate = currentDate;
        this.checkYearOld = checkYearOld;
    }

    @SuppressLint("ValidFragment")
    public DatePickerReportDialog(OnDateSetListener dateListener,
                                  Calendar currentDate, Calendar minDate, Calendar maxDate,
                                  String dialogTitle, EditText txtValue, int selectedTime) {
        this.mMaxDate = maxDate;
        this.mMinDate = minDate;
        this.mTitle = dialogTitle;
        this.myDateListener = dateListener;
        this.txtValue = txtValue;
        this.mCurrentDate = currentDate;
        this.setSelectedTime = selectedTime;
    }

    @SuppressLint("ValidFragment")
    public DatePickerReportDialog(OnDateSetListener dateListener,
                                  Calendar currentDate, Calendar minDate, Calendar maxDate,
                                  String dialogTitle, EditText txtValue, EditText txtValue1) {
        this.mMaxDate = maxDate;
        this.mMinDate = minDate;
        this.mTitle = dialogTitle;
        this.myDateListener = dateListener;
        this.txtValue = txtValue;
        this.txtValue1 = txtValue1;
        this.mCurrentDate = currentDate;
    }

    @SuppressWarnings("unused")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Tuan set thoi gian chon cho dialog chon thoi gian 23032017
        String textDate = null;
        if (txtValue != null)
            textDate = txtValue.getText().toString();
        // Use the current date as the default date in the picker
        Calendar c = new GregorianCalendar();// Calendar.getInstance();
        if (mCurrentDate != null)
            c = mCurrentDate;
        mYear = c.get(YEAR);
        mMonth = c.get(MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        if (textDate != null && textDate.length() > 0 && setSelectedTime == 1) {
            int[] dateSelected = new int[3];
            StringTokenizer strToken = new StringTokenizer(textDate, "/");
            int index = 0;
            while (strToken.hasMoreTokens()) {
                String temp = strToken.nextToken();
                if (index == 2)
                    dateSelected[index] = Integer.valueOf(temp);
                else {
                    temp = temp.charAt(0) == '0' ? temp.substring(1, 2) : temp.substring(0, 2);
                    dateSelected[index] = Integer.valueOf(temp);
                }
                index++;
            }
            mYear = dateSelected[2];
            mMonth = dateSelected[1] - 1;
            mDay = dateSelected[0];
        }
        // Create a new instance of DatePickerDialog and return it
        if (myDateListener == null)
            initDatelistener();
        dialog = new android.app.DatePickerDialog(getActivity(), R.style.CustomTheme,
                myDateListener, mYear, mMonth, mDay);
        setMinDate();
        setMaxDate();
        setTitle();
        if (txtValue != null)
            valuetxt = txtValue.getText().toString();
        if (txtValue1 != null)
            valuetxt1 = txtValue1.getText().toString();
        return dialog;
    }

    public long calculateDays(String startDate, String endDate)
            throws ParseException {
        SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy");
        Date sDate = simpleDate.parse(startDate);
        Date eDate = simpleDate.parse(endDate);
        Calendar cal3 = Calendar.getInstance();
        cal3.setTime(sDate);
        Calendar cal4 = Calendar.getInstance();
        cal4.setTime(eDate);
        return daysBetween(cal3, cal4);
    }

    // tính số ngày giữa 2 ngày
    public long daysBetween(Calendar startDate, Calendar endDate) {
        Calendar date = (Calendar) startDate.clone();
        long daysBetween = 0;
        while (date.before(endDate)) {
            date.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public String getDateFromDatePicker(DatePicker datePicker) {
        String date = "";
        String day = datePicker.getDayOfMonth() <= 9 ? "0"
                + datePicker.getDayOfMonth() : "" + datePicker.getDayOfMonth();
        String month = (datePicker.getMonth() + 1) <= 9 ? "0"
                + (datePicker.getMonth() + 1) : ""
                + (datePicker.getMonth() + 1);
        int year = datePicker.getYear();
        date = date.concat(day + "/" + month + "/" + String.valueOf(year));
        return date;
    }

    public EditText getEditText() {
        return this.txtValue;
    }

    private void setMinDate() {
        if (dialog != null) {
            if (mMinDate != null)
                dialog.getDatePicker().setMinDate(
                        mMinDate.getTime().getTime() - 1000);

        }
    }

    private void setMaxDate() {
        if (dialog != null) {
            if (mMaxDate != null) {
                dialog.getDatePicker().setMaxDate(mMaxDate.getTime().getTime());
            }
        }
    }

    private void setTitle() {
        try {
            if (dialog != null) {
                dialog.setTitle(mTitle);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    private void initDatelistener() {
        myDateListener = new OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {

            }
        };
    }

    // kiểm tra lớn hơn 15 tuổi thì cho tạo pdk mới
    public int getYearOld(String dateSelected) throws ParseException {
        SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy");
        Date fDate = simpleDate.parse(dateSelected);
        Date lDate = new Date();
        Calendar a = getCalendar(fDate);
        Calendar b = getCalendar(lDate);
        int yearOld = b.get(YEAR) - a.get(YEAR);
        if (a.get(MONTH) > b.get(MONTH) ||
                (a.get(MONTH) == b.get(MONTH) && a.get(DATE) > b.get(DATE))) {
            yearOld--;
        }
        return yearOld;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    @Override
    public void onDismiss(DialogInterface d) {
        try {
            this.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        String dateFrom = "";
        dateSelected = getDateFromDatePicker(dialog.getDatePicker());
        if (this.txtValue != null && this.txtValue1 != null) {
            long days = 0;
            if (this.mTitle.equals("TỪ NGÀY")) {
                dateFrom = this.txtValue1.getText().toString();
                txtValue.setText(dateSelected);
                try {
                    days = calculateDays(dateSelected, dateFrom);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    e.printStackTrace();
                }
            } else {
                dateFrom = this.txtValue.getText().toString();
                txtValue1.setText(dateSelected);
                try {
                    days = calculateDays(dateFrom, dateSelected);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    e.printStackTrace();
                }
            }
            if (days > 30) {
                if (valuetxt != "") {
                    txtValue.setText(valuetxt);
                } else {
                    txtValue1.setText(valuetxt1);
                }
                Common.alertDialog(
                        "Bạn chọn thông tin từ ngày, đến ngày quá 30 ngày!",
                        getActivity());
            }
        } else {
            if (checkYearOld) {
                try {
//                    if (getYearOld(dateSelected) < 18) {
//                        Common.alertDialog(
//                                "Khách hàng chưa đủ 18 tuổi để tạo TTKH!",
//                                getActivity());
//                        txtValue.setText("");
//                    }
                    if (getYearOld(dateSelected) < 15) {
                        Common.alertDialog(
                                "Khách hàng chưa đủ 15 tuổi để tạo TTKH!",
                                getActivity());
                        txtValue.setText("");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                if (this.txtValue != null) {
                    this.txtValue.setText(dateSelected);
                } else if (this.txtValue1 != null) {
                    this.txtValue1.setText(dateSelected);
                }
            }
        }
    }

}
