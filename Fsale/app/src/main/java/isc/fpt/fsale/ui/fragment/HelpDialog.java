package isc.fpt.fsale.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import isc.fpt.fsale.R;

/**
 * DIALOG FRAGMENT: HelpDialog
 *
 * @Description:
 * @author: vandn, on 21/08/2013
 */
@SuppressLint("ResourceAsColor")
// màn hình hướng dẫn sử dụng
public class HelpDialog extends DialogFragment {
    private LinearLayout dialogScreen;

    public HelpDialog() {
    }

    @SuppressLint("ValidFragment")
    public HelpDialog(Context mContext) {
        //this.mContext = mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_help, container);
        dialogScreen = (LinearLayout) view.findViewById(R.id.frm_help_dialog);
        dialogScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        return view;
    }

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }
}
